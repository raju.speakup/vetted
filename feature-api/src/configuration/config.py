# ------------------------------------------------------------------------------
# MONGO
# ------------------------------------------------------------------------------

USERNAME = "mongo-admin"
PASSWORD = "mongo-pass"
HOST = "localhost"
DB_NAME = "vetted"


# ------------------------------------------------------------------------------
# APP
# ------------------------------------------------------------------------------

DEFAULT_IP = '0.0.0.0'
SECRETE_KEY = '12345'
DATE_FORMAT = '%d/%m/%Y %I:%M %p'
COMPANY_COLLECTION = 'companies'
USER_COLLECTION = 'users'
PERMISSION_COLLECTION = 'permissions'
EMPLOYEE_COLLECTION = 'employees'
TOKEN_EXPIRE_TIME = 15
REFRESH_EXPIRE_TIME = 15
EMAIL = "raju.speakup@gmail.com"
PWD = "Raj@12345t"
SMTP_URL = 'smtp.gmail.com'
SMTP_PORT = 587


# ------------------------------------------------------------------------------
# ROLES
# ------------------------------------------------------------------------------


SUPER_ADMIN_ROLES = [
    'create_company',
    'edit_company',
    'delete_company',
    'list_company',
    'create_admin',
    'update_admin',
    'delete_admin',
    'list_admin'
]

ADMIN_ROLES = [
    'create_employee',
    'create_profile',
    'edit_employee',
    'list_employee',
    'delete_employee',
    'invite_employee'
]

EMPLOYEE_ROLES = [
    'create_profile',
    'edit_profile',
    'list_profile',
    'delete_profile'
]