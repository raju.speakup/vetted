import json
import sys
from pymongo import MongoClient as pyclient

from configuration.config import (
    DB_NAME,
    HOST,
    PASSWORD,
    USERNAME
)


class MongoClient:
    """MongoClient."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        mongo_url = "".join(
            [
                'mongodb://',
                USERNAME,
                ':',
                PASSWORD,
                '@',
                HOST
            ]
        )

        self.client = pyclient(mongo_url)
        self.database = self.client[DB_NAME]

    def insert_one(self, collection_name, data):
        """Insert One."""
        try:
            data = json.loads(data)
        except ValueError as err:
            message = err.args[0] + ' - ERROR IN - ' + collection_name
            sys.exit(message)
        return self.database[collection_name].insert_one(
            data)

    def find_one(self, collection_name, data):
        """Find One."""
        return self.database[collection_name].find_one(data)

    def find_one_projection(self, collection_name, data, projection):
        """Find One."""
        return self.database[collection_name].find_one(data, projection)

    def find_selected_field(self, collection_name, data, projection):
        """Find Selected."""
        docs = []
        for doc in self.database[collection_name].find(data, projection):
            doc.pop('_id')
            docs.append(doc)
        return docs

    def find_selected_field_all(self, collection_name, data, projection, skip=None, limit=None, sortdata='_id'):
        """Find Selected All."""
        skip = 0 if skip is None or not type(skip) is int else skip
        limit = 5 if limit is None or not type(limit) is int else limit
        docs = []
        for doc in self.database[collection_name].find(data, projection).skip(skip).limit(limit).sort(sortdata):
            doc.pop('_id')
            docs.append(doc)
        return docs, self.database[collection_name].find(data, projection).count()

    def find_all(self, collection_name, data,
                 skip=None, limit=None, sortdata=list()):
        """Find All."""
        skip = 0 if skip is None or not type(skip) is int else skip
        limit = 5 if limit is None or not type(limit) is int else limit
        if type(data) == list:
            return self.database[collection_name].find(
                data[0], data[1]).skip(skip).limit(limit).sort(sortdata),\
                self.database[collection_name].find(data[0], data[1]).count()
        return self.database[collection_name].find(
                data).skip(skip).limit(limit).sort(sortdata),\
                self.database[collection_name].find(data).count()

    def count_docs(self, collection_name, data):
        """Count Docs"""

        return self.database[collection_name].find(
            data).count()

    def update_one(self, collection_name, data):
        """Find One."""
        return self.database[collection_name].update_one(
            data[0], data[1], upsert=True)

    def update_many(self, collection_name, data):
        """Find Many."""
        return self.database[collection_name].update_many(data[0], data[1])

    def delete_many(self, collection_name, data):
        """Delete Many."""
        return self.database[collection_name].delete_many(data)

    def delete_one(self, collection_name, data):
        return self.database[collection_name].delete_one(data)

    def list_all(self, collection_name, data, skip, limit, sort=list()):
        """Find All."""
        docs = []
        for doc in self.database[collection_name].find(
                data).skip(skip).limit(limit).sort(sort):
            doc.pop('_id')
            docs.append(doc)

        return docs

    def find_unique_names(self, collection_name, field, query):
        return self.database[collection_name].distinct(field, query)




