#!/usr/bin/env python
# -*- coding: utf-8 -*-
from constants.custom_field_error import (
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND,
    HTTP_403_FORBIDDEN
)
from commons.mongo_services import MongoClient
from commons.json_utils import to_json
from configuration.config import (
    PERMISSION_COLLECTION,
    USER_COLLECTION
)
from functools import wraps
from jsonschema import ValidationError

from flask_jwt_extended import (
    get_jwt_claims
)

from pymongo.errors import PyMongoError
connection = MongoClient()


def validate_user_action(action):
    """Validate app user for the invoked service. action needs to be passed to the service"""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_claims = get_jwt_claims()
                user_id = user_claims.get('user_id')
                try:
                    role_object = connection.find_one_projection(
                        collection_name=USER_COLLECTION,
                        data={
                            "user_id": user_id
                        },
                        projection={
                            "role": 1
                        }
                    )
                except PyMongoError as err:
                    return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
                if not role_object:
                    return to_json({"message": "user_id : {0} does not exist".format(user_id)},
                                   is_error=True), HTTP_404_NOT_FOUND
                role = role_object.get('role')
                try:
                    action_object = connection.find_one_projection(
                        collection_name=PERMISSION_COLLECTION,
                        data={
                            "role": role
                        },
                        projection={
                            "actions": 1
                        }
                    )
                except PyMongoError as err:
                    return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

                if not action_object:
                    return to_json({"message": "role : {0} does not exist".format(role)},
                                   is_error=True), HTTP_404_NOT_FOUND

                actions = action_object.get('actions')
                if action not in actions:
                    return to_json(
                        {
                            "message": "User does not have access to perform this action"
                        }, is_error=True
                    ), HTTP_403_FORBIDDEN

            except ValidationError as err:
                return to_json({"message": err.args[0]}, is_error=True), HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator


