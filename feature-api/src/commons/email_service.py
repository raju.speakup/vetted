import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from configuration.config import (
    EMAIL,
    PWD,
    SMTP_URL,
    SMTP_PORT
)
from commons.celery_factory_obj import celery

class EmailService:

    def __init__(self):
        self.conn = smtplib.SMTP(SMTP_URL, SMTP_PORT)
        self.conn.starttls()
        self.conn.login(EMAIL, PWD)

    def send_email(self, to_address, subject, body):
        msg = MIMEMultipart()
        msg['From'] = EMAIL
        msg['To'] = to_address
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        text = msg.as_string()
        self.conn.sendmail(EMAIL, to_address, text)
        self.conn.quit()

@celery.task
def email_sender(to_address, subject, body):
    email_obj = EmailService()
    email_obj.send_email(to_address, subject, body)