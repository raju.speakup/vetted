from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import (
    HTTP_200_OK
)


from math import log, ceil
from itertools import islice

class PrimeNumberService:

    def find_primes(self, limit):
        nums = [True] * (limit + 1)
        nums[0] = nums[1] = False

        for (i, is_prime) in enumerate(nums):
            if is_prime:
                yield i
                for n in range(i * i, limit + 1, i):
                    nums[n] = False

    def upper_bound_for_p_n(self, n):
        if n < 6:
            return 100
        return ceil(n * (log(n) + log(log(n))))

    def find_n_prime(self, n):
        primes = self.find_primes(self.upper_bound_for_p_n(n))
        return to_json({"message": next(islice(primes, n - 1, n))}), HTTP_200_OK
