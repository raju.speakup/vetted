from commons.json_validator import Schema, Prop
# \d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)

def create_user():
    prop = {
        "email": Prop().string().max(255).min(1).build(),
        "company_id": Prop().string().max(1000).min(1).build(),

    }
    return Schema().keys(prop).required(["email", "company_id"]).build()



def update_user():
    prop = {
        "email": Prop().string().max(255).min(1).build(),
        "password": Prop().string().max(1000).min(1).build(),
        "company_id": Prop().string().max(1000).min(1).build(),
        "role": Prop().string().max(255).min(1).enum(['admin', 'employee']).build(),
    }
    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "user_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["user_id"]).build()


def list_schema():
    prop = {
        "company_id": Prop().string().max(1000).min(1).build(),
        "limit": Prop().number().max(5).min(1).build(),
        "offset": Prop().number().max(5).min(1).build(),
    }

    return Schema().keys(prop).required(["company_id"]).build()


def bulk_delete_schema():
    prop = {
        "user_ids": Prop().array().build()
    }

    return Schema().keys(prop).required(["user_ids"]).build()

