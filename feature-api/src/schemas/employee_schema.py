from commons.json_validator import Schema, Prop
# \d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)

def create_employee():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "country": Prop().string().max(1000).min(1).build(),
        "designation": Prop().string().max(1000).min(1).build(),
        "email": Prop().string().max(1000).min(1).build(),
    }
    return Schema().keys(prop).required(["name", "country", "designation"]).build()


def update_employee():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "country": Prop().string().max(1000).min(1).build(),
        "designation": Prop().string().max(1000).min(1).build(),
    }
    return Schema().keys(prop).required([]).build()


def invite_employee():
    prop = {
        "email": Prop().string().max(255).min(1).build(),
        "company_id": Prop().string().max(255).min(1).build()
    }
    return Schema().keys(prop).required(["email", "company_id"]).build()


def id_path_schema():
    prop = {
        "user_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["user_id"]).build()

def number_schema():
    prop = {
        "number": Prop().string().max(50).min(1).build()
    }

    return Schema().keys(prop).required(["number"]).build()

def list_schema():
    prop = {
        "limit": Prop().number().max(5).min(1).build(),
        "offset": Prop().number().max(5).min(1).build(),
    }

    return Schema().keys(prop).required([]).build()


def bulk_delete_schema():
    prop = {
        "user_ids": Prop().array().build()
    }

    return Schema().keys(prop).required(["user_ids"]).build()

