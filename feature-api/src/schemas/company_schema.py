from commons.json_validator import Schema, Prop
# \d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)

def create_company():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "description":Prop().string().max(1000).min(1).build(),
        "type": Prop().string().max(255).min(1).enum(['finance', 'e-commerce', 'banking']).build(),
    }
    return Schema().keys(prop).required(["name", "description", "type"]).build()


def update_company():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "description":Prop().string().max(1000).min(1).build(),
        "type": Prop().string().max(255).min(1).enum(['finance', 'e-commerce', 'banking']).build(),
    }
    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "company_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["company_id"]).build()


def search_schema():
    prop = {
        "type": Prop().string().max(100).min(1).enum(['data_ingestion', 'ai_pipeline']).build(),
        "limit": Prop().number().max(5).min(1).build(),
        "offset": Prop().number().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required(["type"]).build()


def list_schema():
    prop = {
        "limit": Prop().number().max(5).min(1).build(),
        "offset": Prop().number().max(5).min(1).build(),
    }

    return Schema().keys(prop).required([]).build()


def bulk_delete_schema():
    prop = {
        "company_ids": Prop().array().build()
    }

    return Schema().keys(prop).required(["company_ids"]).build()

