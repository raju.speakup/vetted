# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND,
    HTTP_201_CREATED,
    HTTP_406_NOT_ACCEPTABLE
)
from configuration.config import (
    DATE_FORMAT,
    USER_COLLECTION
)
import json


# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError


class UserService:
    """User Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None

    def create_user(self, collection_name, data):
        try:
            name = data.get('email')
            status, resp = self.check_name_exists(collection_name=collection_name, name=name)
            if status:
                return resp

            self.connection.insert_one(collection_name=collection_name,
                                       data=json.dumps(data))

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return to_json({"message": "User created !"}), HTTP_201_CREATED

    def update_user(self, collection_name, user_id, data):
        try:
            name = data.get('name')
            if name:
                status, resp = self.check_name_exists(collection_name=collection_name, name=name)
                if status:
                    return resp
            return_object = self.connection.find_one(
                collection_name=collection_name,
                data={
                    'user_id': user_id
                }
            )

            if not return_object:
                return to_json({"message": "user_id : {0}  does not exist".format(user_id)}, is_error=True), HTTP_404_NOT_FOUND
            else:
                self.connection.update_one(
                    collection_name=collection_name,
                    data=(
                        {
                            "user_id": user_id
                        },
                        {
                            "$set": data
                        }
                    )
                )
            return to_json(data), HTTP_200_OK

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

    def delete_user(self, user_id):
        try:
            return_object = self.connection.find_one(
                collection_name=USER_COLLECTION,
                data={
                    "user_id": user_id
                }
            )
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json(
                {
                    "message": "user_id : {0}  does not exist".format(user_id)
                }, is_error=True
            ), HTTP_404_NOT_FOUND

        else:
            self.connection.delete_one(
                collection_name=USER_COLLECTION,
                data={
                    "user_id": user_id
                }
            )

        return to_json({"message": "User deleted"}), HTTP_200_OK

    def bulk_delete_users(self, user_ids):
        try:
            response_dict = dict()
            for user_id in user_ids:
                return_object = self.connection.find_one(
                                        collection_name=USER_COLLECTION,
                                        data={
                                            "user_id": user_id
                                        }
                                    )
                if not return_object:
                    response_dict[user_id] = "user_id does not exist"
                else:
                    self.connection.delete_one(
                        collection_name=USER_COLLECTION,
                        data={
                            "user_id": user_id
                        }
                    )
                    response_dict[user_id] = "user_id deleted"

            return to_json(response_dict), HTTP_200_OK
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

    def get_user(self, collection_name, user_id):
        try:
            return_object = self.connection.find_one(
                collection_name=collection_name,
                data={
                    "user_id": user_id
                }
            )
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "user_id : {0} does not exist".format(user_id)}, is_error=True), HTTP_404_NOT_FOUND
        if return_object:
            return_object.pop("_id")
        return to_json(return_object), HTTP_200_OK

    def list_all_users(self, collection_name, data, offset, limit):
        try:
            return_object = self.connection.list_all(
                collection_name=collection_name,
                data=data,
                skip=offset,
                limit=limit,
                sort='_id'

            )
            count = self.connection.count_docs(
                collection_name=collection_name,
                data={}
            )
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(return_object, list_count=count), HTTP_200_OK

    def search_schedule(self, collection_name, keyword, offset, limit):
        try:
            query = self.search_query(keyword)

            return_object = self.connection.list_all(
                collection_name=collection_name,
                data=query,
                skip=offset,
                limit=limit,
            )
            count = self.connection.count_docs(
                collection_name=collection_name,
                data=query
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(return_object, list_count=count), HTTP_200_OK

    def check_name_exists(self, collection_name, name):
        try:
            if name in self.connection.find_unique_names(
                    collection_name=collection_name,
                    field='email',
                    query={}
            ):
                return True, (to_json(
                    {
                        "message": "user '{}' already exists.".format(name)
                    },
                    is_error=True
                ), HTTP_406_NOT_ACCEPTABLE)
            return False, ()
        except PyMongoError as err:
            return True, (to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR)

    @staticmethod
    def search_query(keyword):
        return {
                "$or": [
                        {'name': {'$regex': keyword, "$options": "-i"}},
                        {'description': {'$regex': keyword, "$options": "-i"}},
                        {'type': {'$regex': keyword, "$options": "-i"}}
                ]
        }