# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND,
    HTTP_201_CREATED,
    HTTP_406_NOT_ACCEPTABLE
)
from configuration.config import (
    DATE_FORMAT,
    USER_COLLECTION,
    EMPLOYEE_COLLECTION
)
import json


# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError


class EmployeeService:
    """Employee Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None

    def create_employee(self, collection_name, data):
        try:
            self.connection.insert_one(collection_name=collection_name,
                                       data=json.dumps(data))

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return to_json({"message": "Profile created !"}), HTTP_201_CREATED

    def update_employee(self, collection_name, user_id, data):
        try:

            return_object = self.connection.find_one(
                collection_name=USER_COLLECTION,
                data={
                    'user_id': user_id
                }
            )

            if not return_object:
                return to_json({"message": "user_id : {0}  does not exist".format(user_id)}, is_error=True), HTTP_404_NOT_FOUND
            else:
                self.connection.update_one(
                    collection_name=collection_name,
                    data=(
                        {
                            "email": return_object.get('email')
                        },
                        {
                            "$set": data
                        }
                    )
                )
            return to_json(data), HTTP_200_OK

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

    def delete_employee(self, user_id):
        try:
            return_object = self.connection.find_one(
                collection_name=USER_COLLECTION,
                data={
                    "user_id": user_id
                }
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json(
                {
                    "message": "user_id : {0}  does not exist".format(user_id)
                }, is_error=True
            ), HTTP_404_NOT_FOUND

        else:
            self.connection.delete_one(
                collection_name=EMPLOYEE_COLLECTION,
                data={
                    "user_id": return_object.get('email')
                }
            )

        return to_json({"message": "User deleted"}), HTTP_200_OK

    def get_employee(self, collection_name, user_id):
        try:
            return_object = self.connection.find_one(
                collection_name=USER_COLLECTION,
                data={
                    "user_id": user_id
                }
            )
            if not return_object:
                return to_json({"message": "user_id : {0} does not exist".format(user_id)},
                               is_error=True), HTTP_404_NOT_FOUND
            else:
                profile_object = self.connection.find_one(
                    collection_name=collection_name,
                    data={
                        "email": return_object.get('email')
                    }
                )
                if profile_object:
                    profile_object.pop("_id")
                return to_json(profile_object), HTTP_200_OK

        except PyMongoError as err:
            raise
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR


