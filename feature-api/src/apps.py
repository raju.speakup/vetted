#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS

from configuration.config import DEFAULT_IP, SECRETE_KEY
from resources.company import Company
from resources.company_edit_delete import CompanyEditDelete
from resources.admin import Admin
from resources.admin_edit_delete import AdminEditDelete
from resources.admin_bulk import AdminBulk
from resources.employee import Employee
from resources.employee_edit_delete import EmployeeEditDelete
from resources.employee_bulk import EmployeeBulk
from resources.employee_invite import EmployeeInvite
from resources.profile import Profile
from resources.profile_edit_delete import ProfileEditDelete
from resources.find_prime import Prime
from resources.login import (
    User,
    UserLogin,
    SecretKey,
    Refresh
)

from flask_jwt_extended import (
    JWTManager,
    get_raw_jwt,
    jwt_required,
    jwt_refresh_token_required
)

from commons.mongo_services import MongoClient

app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="Dag Schedule Management Service",
    resourcePath="/Clients"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)
jwt._set_error_handler_callbacks(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
blacklist = set()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """check_if_token_in_blacklist."""
    jti = decrypted_token['jti']
    return jti in blacklist


@jwt.user_claims_loader
def add_claims_to_access_token(user_id):
    """add_claims_to_access_token.

    Connect to mongo authorization service
    and get permission information of user
    """
    connection = MongoClient()
    role_object = connection.find_one_projection(
        collection_name="users",
        data={
            "user_id": user_id
        },
        projection={
            "role": 1
        }
    )
    if role_object:
        return {
            'user_id': user_id,
            'role': role_object.get('role')
       }


class   LogOutAccess(Resource):
    """Logout."""
    @jwt_required
    def delete(self):
        """Delete token."""
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return jsonify({"msg": "Successfully Removed Access Token"})


class LogOutRefresh(Resource):
    """Logout."""

    @jwt_refresh_token_required
    def delete(self):
        """delete."""
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return jsonify({"msg": "Successfully Removed Refresh Token"})


# ------------------------------------------------------------------------------
# Company services
# ------------------------------------------------------------------------------

# http://server/api/v1/dag/companies
api.add_resource(
     Company, '/api/v1/companies')

# http://server/api/v1/dag/companies/{company_id}
api.add_resource(
     CompanyEditDelete, '/api/v1/companies/<string:company_id>')

# ------------------------------------------------------------------------------
# Prime Number services
# ------------------------------------------------------------------------------

api.add_resource(
     Prime, '/api/v1/prime/<string:number>')



# ------------------------------------------------------------------------------
# Admin services
# ------------------------------------------------------------------------------

# http://server/api/v1/dag/companies
api.add_resource(
     Admin, '/api/v1/admins')

# http://server/api/v1/dag/companies/{company_id}
api.add_resource(
     AdminEditDelete, '/api/v1/admins/<string:user_id>')

api.add_resource(
     AdminBulk, '/api/v1/admins/list/<string:company_id>')

# ------------------------------------------------------------------------------
# Employee services
# ------------------------------------------------------------------------------

# http://server/api/v1/dag/companies
api.add_resource(
     Employee, '/api/v1/employees')

# http://server/api/v1/dag/companies/{company_id}
api.add_resource(
     EmployeeEditDelete, '/api/v1/employees/<string:user_id>')

api.add_resource(
     EmployeeBulk, '/api/v1/employees/list/<string:company_id>')


api.add_resource(
     EmployeeInvite, '/api/v1/employees/invite')


# ------------------------------------------------------------------------------
# Profile services
# ------------------------------------------------------------------------------

# http://server/api/v1/dag/companies
api.add_resource(
     Profile, '/api/v1/profiles')

# http://server/api/v1/dag/companies/{company_id}
api.add_resource(
     ProfileEditDelete, '/api/v1/profiles/<string:user_id>')
# ------------------------------------------------------------------------------
# Authentication services
# ------------------------------------------------------------------------------

# http://server/api/v1/login
api.add_resource(UserLogin, '/api/v1/login')

api.add_resource(User, '/api/v1/user')

# http://server/api/v1/refresh
api.add_resource(Refresh, '/api/v1/refresh_access_token')

# http://server/api/v1/logoutaccess
api.add_resource(LogOutAccess, '/api/v1/logoutaccess')

# http://server/api/v1/logoutrefresh
api.add_resource(LogOutRefresh, '/api/v1/logoutrefresh')

# http://server/api/v1/secretekey
api.add_resource(SecretKey, '/api/v1/secretekey')

if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8091, debug=True, threaded=True)
