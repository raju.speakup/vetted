#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class CreateUser:
    """Create User fields."""

    resource_fields = {
        'email': fields.String,
        'password': fields.String,
        'role': fields.String,
        'company_id': fields.String
    }
    swagger_metadata = {
        "email": {
            "required": True
        },
        "password": {
            "required": True
        },
        "company_id": {
            "required": True
        },
        "role": {
            "enum": ['admin', 'employee'],
            "required": True
        }
    }


@swagger.model
class UpdateUser:
    """Update User fields."""

    resource_fields = {
        'email': fields.String,
        'password': fields.String,
        'role': fields.String,
        'company_id': fields.String
    }
    swagger_metadata = {
        "email": {
            "required": True
        },
        "password": {
            "required": True
        },
        "company_id": {
            "required": True
        },
        "role": {
            "enum": ['admin', 'employee'],
            "required": True
        }
    }