#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class CreateCompany:
    """Create Company fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'type': fields.String,
    }
    swagger_metadata = {
        "name": {
            "required": True
        },
        "description": {
            "required": True
        },
        "type": {
            "enum": ['finance', 'e-commerce', 'banking'],
            "required": True
        }
    }


@swagger.model
class UpdateCompany:
    """Update Company fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'type': fields.String
    }

    swagger_metadata = {
        "name": {
            "required": True
        },
        "description": {
            "required": True
        },
        "type": {
            "enum": ['finance', 'e-commerce', 'banking'],
            "required": True
        }
    }

