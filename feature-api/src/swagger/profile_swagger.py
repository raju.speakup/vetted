#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class CreateProfile:
    """Create employee fields."""

    resource_fields = {
        'name': fields.String,
        'country': fields.String,
        'designation': fields.String,
    }
    swagger_metadata = {
        "name": {
            "required": True
        },
        "country": {
            "required": True
        },
        "designation": {
            "required": True
        }
    }
@swagger.model
class UpdateProfile:
    """Update Profile."""

    resource_fields = {
        'name': fields.String,
        'country': fields.String,
        'designation': fields.String,
    }
    swagger_metadata = {
        "name": {
            "required": True
        },
        "country": {
            "required": True
        },
        "designation": {
            "required": True
        }
    }


