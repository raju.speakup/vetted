#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class Login:
    """Login fields."""

    resource_fields = {
        'email': 'fields.String',
        'password': 'fields.String'
    }

