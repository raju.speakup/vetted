#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from configuration.config import (
    DATE_FORMAT,
    PERMISSION_COLLECTION,
    USER_COLLECTION,
    SUPER_ADMIN_ROLES,
    ADMIN_ROLES,
    EMPLOYEE_ROLES
)
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
)
# ------------------------------------------------------------------------------
# PYTHON FUNCTIONS
# ------------------------------------------------------------------------------
import uuid
import json
import bcrypt
import datetime


class SuperAdmin:
    """SuperAdmin.
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None

    def create_super_admin(self):
        """ Create Super Admin """

        data = {
                'email': 'superadmin@vetted.com',
                'password': 'superadmin1+',
                'is_active': True,
                'role': 'super_admin'

        }
        try:
            return_object = self.connection.find_one(
                collection_name=USER_COLLECTION,
                data={
                    "email": data['email'],
                    "isActive": True
                }
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            password_hash = bcrypt.hashpw(
                data.get(
                    'password').encode('utf-8'),
                bcrypt.gensalt())
            user_id = str(uuid.uuid4())
            data.update(
                {
                    'user_id': user_id,
                    'company_id': '*',
                    'password': password_hash.decode('utf-8'),
                    "created_by": '007',
                    "updated_by": '007',
                    "created_at": datetime.datetime.now().strftime(DATE_FORMAT),
                    "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
                }
            )
            try:
                self.connection.insert_one(
                    collection_name=USER_COLLECTION,
                    data=json.dumps(data))
            except PyMongoError as err:
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        print("Super Admin Created !")
        return data, HTTP_200_OK


    def create_roles(self, role, actions):
        try:
            self.connection.insert_one(
                collection_name=PERMISSION_COLLECTION,
                data=json.dumps(
                    {
                        "role": role,
                        "actions": actions,
                    }
                )
            )
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        print("Roles Created !")


sa_obj = SuperAdmin()
sa_obj.create_super_admin()
sa_obj.create_roles('super_admin', SUPER_ADMIN_ROLES)
sa_obj.create_roles('admin', ADMIN_ROLES)
sa_obj.create_roles('employee', EMPLOYEE_ROLES)



