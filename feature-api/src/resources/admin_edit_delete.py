#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from configuration.config import (
    DATE_FORMAT,
    COMPANY_COLLECTION,
    USER_COLLECTION
)
from services.users import UserService
from swagger.user_swagger import (
    UpdateUser,
    CreateUser
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.user_schema import (
    update_user,
    id_path_schema
)
#------------------------------------------------------------------------------


class AdminEditDelete(Resource):
    """Admin Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateUser.__name__,
                "paramType": "body",
                "required": True,
                "description": "PUT body"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The user_id to be updated"
            }
        ],
        notes='Admin Update',
        nickname='Admin Update')
    @validate_json
    @validate_schema(update_user())
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='update_admin')
    def put(self, user_id):
        """Edit admin details"""
        data = json.loads(request.data.decode('utf-8'))
        creator_user_id = get_jwt_claims().get('user_id')
        data.update({
            "updated_by": creator_user_id,
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })
        output = self.service.update_user(
            collection_name=USER_COLLECTION,
            user_id=user_id,
            data=data
        )
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The user_id to be fetched"
            }

        ],
        notes='Get admin ',
        nickname='Get admin')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='list_admin')
    def get(self, user_id):
        """Get admin"""
        output = self.service.get_user(
            collection_name=COMPANY_COLLECTION,
            user_id=user_id
        )

        return output

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The user_id to be deleted"
            }

        ],
        notes='Delete user',
        nickname='Delete user')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='delete_admin')
    def delete(self, user_id):
        """Delete user"""
        output = self.service.delete_user(
            user_id=user_id
        )
        return output

