#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from configuration.config import (
    DATE_FORMAT,
    COMPANY_COLLECTION,
    EMPLOYEE_COLLECTION
)
from services.employees import EmployeeService
from swagger.profile_swagger import (
    CreateProfile,
    UpdateProfile
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.employee_schema import (
    update_employee,
    id_path_schema
)
#------------------------------------------------------------------------------


class ProfileEditDelete(Resource):
    """Profile Edit Delete Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = EmployeeService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateProfile.__name__,
                "paramType": "body",
                "required": True,
                "description": "PUT body"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The user_id to be updated"
            }
        ],
        notes='Employee Update',
        nickname='Employee Update')
    @validate_json
    @validate_schema(update_employee())
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='edit_profile')
    def put(self, user_id):
        """Edit employee details"""
        data = json.loads(request.data.decode('utf-8'))
        creator_user_id = get_jwt_claims().get('user_id')
        data.update({
            "updated_by": creator_user_id,
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })
        output = self.service.update_employee(
            collection_name=EMPLOYEE_COLLECTION,
            user_id=user_id,
            data=data
        )
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The user_id to be fetched"
            }

        ],
        notes='Get profile ',
        nickname='Get profile')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='list_profile')
    def get(self, user_id):
        """Get employee"""
        output = self.service.get_employee(
            collection_name=EMPLOYEE_COLLECTION,
            user_id=user_id
        )

        return output

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The user_id to be deleted"
            }

        ],
        notes='Employee user',
        nickname='Employee user')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='delete_profile')
    def delete(self, user_id):
        """Employee user"""
        output = self.service.delete_employee(
            user_id=user_id
        )
        return output

