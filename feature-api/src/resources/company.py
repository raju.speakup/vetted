#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from configuration.config import (
    DATE_FORMAT,
    COMPANY_COLLECTION
)
from services.companies import CompanyService
from swagger.company_swagger import (
    CreateCompany,
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.company_schema import (
    create_company,
    bulk_delete_schema,
    list_schema
)
#------------------------------------------------------------------------------


class Company(Resource):
    """Company Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = CompanyService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateCompany.__name__,
                "paramType": "body",
                "required": True,
                "description": "POST body"
            }
        ],
        notes='Company create',
        nickname='Company create')
    @validate_json
    @validate_schema(create_company())
    @jwt_required
    @validate_user_action(action='create_company')
    def post(self):
        """Create company"""
        print (request.data.decode('utf-8'))
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data
        company_id = str(uuid.uuid4())
        creator_user_id = get_jwt_claims().get('user_id')
        new_doc.update({
            "company_id": company_id,
            "created_by": creator_user_id,
            "updated_by": creator_user_id,
            "created_at": datetime.datetime.now().strftime(DATE_FORMAT),
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })

        output = self.service.create_company(
            collection_name=COMPANY_COLLECTION,
            data=new_doc
        )
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            }

        ],
        notes='List all companies',
        nickname='List all companies')
    @validate_json
    @validate_schema(list_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='list_company')
    def get(self):
        """Get list of all companies"""
        limit = int(request.args.get('limit', 5))
        offset = int(request.args.get('offset', 0))
        output = self.service.list_all_companies(
            collection_name=COMPANY_COLLECTION,
            offset=offset,
            limit=limit
        )
        return output


    @swagger.operation(
        parameters=[
            {
                "name": "company_id",
                "dataType": 'list',
                "paramType": "body",
                "required": True,
                "description": "The list of company_ids to be deleted"
            }

        ],
        notes='Delete companies',
        nickname='Delete companies')
    @validate_json
    @validate_schema(bulk_delete_schema())
    @jwt_required
    @validate_user_action(action='delete_company')
    def delete(self):
        """Bulk delete companies"""
        data = json.loads(request.data.decode('utf-8'))
        ids = data.get('company_ids')
        output = self.service.bulk_delete_company(
            company_ids=ids
        )
        return output
