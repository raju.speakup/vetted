#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from configuration.config import (
    DATE_FORMAT,
    COMPANY_COLLECTION
)
from services.companies import CompanyService
from swagger.company_swagger import (
    CreateCompany,
    UpdateCompany
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.company_schema import (
    create_company,
    update_company,
    id_path_schema
)
#------------------------------------------------------------------------------


class CompanyEditDelete(Resource):
    """Company Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = CompanyService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateCompany.__name__,
                "paramType": "body",
                "required": True,
                "description": "PUT body"
            },
            {
                "name": "company_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The company_id to be updated"
            }
        ],
        notes='Company Update',
        nickname='Company Update')
    @validate_json
    @validate_schema(update_company())
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='edit_company')
    def put(self, company_id):
        """Edit Company details"""
        data = json.loads(request.data.decode('utf-8'))
        print(data)
        creator_user_id = get_jwt_claims().get('user_id')
        data.update({
            "updated_by": creator_user_id,
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })
        output = self.service.update_company(
            collection_name=COMPANY_COLLECTION,
            company_id=company_id,
            data=data
        )
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "company_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The company_id to be fetched"
            }

        ],
        notes='Get company ',
        nickname='Get company')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='list_company')
    def get(self, company_id):
        """Get company"""
        output = self.service.get_company(
            collection_name=COMPANY_COLLECTION,
            company_id=company_id
        )

        return output

    @swagger.operation(
        parameters=[
            {
                "name": "company_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The company_id to be deleted"
            }

        ],
        notes='Delete company',
        nickname='Delete company')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='delete_company')
    def delete(self, company_id):
        """Delete company"""
        output = self.service.delete_company(
            company_id=company_id
        )
        return output

