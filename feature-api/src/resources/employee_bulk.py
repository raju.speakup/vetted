#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from configuration.config import (
    DATE_FORMAT,
    USER_COLLECTION
)
from services.users import UserService
from swagger.user_swagger import (
    CreateUser,
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.user_schema import (
    create_user,
    bulk_delete_schema,
    list_schema
)
#------------------------------------------------------------------------------


class EmployeeBulk(Resource):
    """EmployeeBulk Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "company_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The company_id to be searched"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            }

        ],
        notes='List all employees',
        nickname='List all employees')
    @validate_json
    @validate_schema(list_schema(), is_path=True)
    @jwt_required
    @validate_user_action(action='list_employee')
    def get(self, company_id):
        """Get list of all employees"""
        limit = int(request.args.get('limit', 5))
        offset = int(request.args.get('offset', 0))
        output = self.service.list_all_users(
            collection_name=USER_COLLECTION,
            data={
                'role': 'employee',
                'company_id': company_id
            },
            offset=offset,
            limit=limit
        )
        return output

