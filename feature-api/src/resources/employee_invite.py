#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import bcrypt
import datetime
from random import randint

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from commons.email_service import EmailService, email_sender
from configuration.config import (
    DATE_FORMAT,
    USER_COLLECTION,
    EMPLOYEE_COLLECTION
)
from services.users import UserService
from swagger.employee_swagger import (
    CreateEmployee,
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.employee_schema import (
    invite_employee
)
#------------------------------------------------------------------------------


class EmployeeInvite(Resource):
    """Employee Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()
        self.email_service = EmailService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateEmployee.__name__,
                "paramType": "body",
                "required": True,
                "description": "POST body"
            }
        ],
        notes='Employee invite',
        nickname='Employee invite')
    @validate_json
    @validate_schema(invite_employee())
    @jwt_required
    @validate_user_action(action='invite_employee')
    def post(self):
        """Invite employee"""
        print(request.data.decode('utf-8'))
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data
        email = data.get('email')
        user_id = str(uuid.uuid4())
        creator_user_id = get_jwt_claims().get('user_id')
        pwd = str(randint(1000, 9999))
        password_hash = bcrypt.hashpw(
            pwd.encode('utf-8'),
            bcrypt.gensalt()
        )
        new_doc.update({
            "user_id": user_id,
            "is_active": False,
            "role": "employee",
            "password": password_hash.decode('utf-8'),
            "created_by": creator_user_id,
            "updated_by": creator_user_id,
            "created_at": datetime.datetime.now().strftime(DATE_FORMAT),
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })

        output = self.service.create_user(
            collection_name=USER_COLLECTION,
            data=new_doc
        )
        if output[-1] == 201:
            email_sender.delay(
                email,
                'Invitation to login',
                'Username : {} ; Password : {} '.format(email, pwd)
            )
        return output
