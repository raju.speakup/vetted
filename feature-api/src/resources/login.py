#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource, reqparse
from flask import request, jsonify
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from configuration.config import (
    TOKEN_EXPIRE_TIME,
    REFRESH_EXPIRE_TIME,
    USER_COLLECTION
)

from constants.custom_field_error import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_401_UNAUTHORIZED,
    HTTP_403_FORBIDDEN,
    HTTP_404_NOT_FOUND,
    HTTP_400_BAD_REQUEST
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.login_swagger import Login
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_raw_jwt,
    get_jwt_claims,
    get_jwt_identity,
    jwt_required,
    jwt_refresh_token_required
)
from commons.json_utils import to_json, to_list_json
# ------------------------------------------------------------------------------
# PYTHON FUNCTIONS
# ------------------------------------------------------------------------------
import datetime
import bcrypt


class UserLogin(Resource):
    """UserLogin.

    Login mechanism.
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": Login.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='User Login',
        nickname='User Login')
    def post(self):
        """ User Login"""
        parser = reqparse.RequestParser()
        parser.add_argument(
            'email', help='This field cannot be blank', required=True)
        parser.add_argument(
            'password', help='This field cannot be blank', required=True)
        data = parser.parse_args()

        if len(data.get('password')) == 0:
            return to_json(
                {
                    "message": "Wrong email or password !"
                }, is_error=True), HTTP_401_UNAUTHORIZED

        try:
            return_object = self.connection.find_one(
                collection_name="users",
                data={
                    "email": data.get('email'),

                }
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if return_object:
            if not bcrypt.checkpw(
                    data.get('password').encode('utf-8'),
                    return_object['password'].encode('utf-8')
            ):
                return to_json(
                    {
                        "message": "Wrong email or password !"
                    }, is_error=True), HTTP_401_UNAUTHORIZED

            return_object.pop('_id')
            if 'password' in return_object.keys():
                return_object.pop('password')

            data.update(return_object)
            print(data)
            expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
            referesh_expires = datetime.timedelta(minutes=REFRESH_EXPIRE_TIME)
            accesstoken = create_access_token(
                identity=data['user_id'], expires_delta=expires)
            refreshtoken = create_refresh_token(
                identity=data['user_id'], expires_delta=referesh_expires)

            from apps import add_claims_to_access_token
            add_claims_to_access_token(return_object)
            data.update({'accessToken': accesstoken,
                         'accesstokenExpTime': TOKEN_EXPIRE_TIME,
                         'refreshToken': refreshtoken,
                         'refreshTokenExpTime': REFRESH_EXPIRE_TIME,
                         })
            if 'password' in data.keys():
                data.pop('password')
        else:
            return to_json(
                {
                    "message": "Wrong email or password !"
                }, is_error=True), HTTP_401_UNAUTHORIZED

        return data, HTTP_200_OK


class SecretKey(Resource):
    """SecretKey."""

    @swagger.operation(
        parameters=[],
        notes='Secrete Get',
        nickname='Secrete')
    def get(self):
        """Get."""
        import os
        import binascii
        key = binascii.hexlify(os.urandom(24))
        return {
            'secreteKey': str(key)
        }


class Refresh(Resource):
    """Refresh Token."""

    @jwt_refresh_token_required
    def post(self):
        """Post."""
        data = {}
        current_user = get_jwt_identity()
        expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
        refresh_expires = datetime.timedelta(minutes=REFRESH_EXPIRE_TIME)
        data.update({
            'accessToken': str(create_access_token(identity=current_user, expires_delta=expires)),
            'refreshToken': str(create_refresh_token(identity=current_user, expires_delta=refresh_expires))
        })
        return jsonify(data)


class User(Resource):
    """Get User."""

    @jwt_required
    def get(self):
        """Get."""
        try:
            connection = MongoClient()
            user_claims = get_jwt_claims()
            user_id = user_claims.get('user_id')
            try:
                role_object = connection.find_one_projection(
                    collection_name=USER_COLLECTION,
                    data={
                        "user_id": user_id
                    },
                    projection={
                        "role": 1
                    }
                )
            except PyMongoError as err:
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
            if not role_object:
                return to_json({"message": "user_id : {0} does not exist".format(user_id)},
                               is_error=True), HTTP_404_NOT_FOUND
            role = role_object.get('role')
            return to_json(role), HTTP_200_OK
        except Exception as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_400_BAD_REQUEST