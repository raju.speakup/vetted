#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import bcrypt
import datetime
from random import randint

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from configuration.config import (
    DATE_FORMAT,
    EMPLOYEE_COLLECTION,
    USER_COLLECTION
)
from services.employees import EmployeeService
from services.users import UserService
from swagger.profile_swagger import (
    CreateProfile,
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.employee_schema import (
    create_employee,
)
#------------------------------------------------------------------------------


class Profile(Resource):
    """Profile Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = EmployeeService()
        self.user_service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateProfile.__name__,
                "paramType": "body",
                "required": True,
                "description": "POST body"
            }
        ],
        notes='Employee create',
        nickname='Employee create')
    @validate_json
    @validate_schema(create_employee())
    @jwt_required
    @validate_user_action(action='create_profile')
    def post(self):
        """Create profile"""
        print(request.data.decode('utf-8'))
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data
        creator_user_id = get_jwt_claims().get('user_id')
        new_doc.update({
            "created_by": creator_user_id,
            "updated_by": creator_user_id,
            "created_at": datetime.datetime.now().strftime(DATE_FORMAT),
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })

        output = self.service.create_employee(
            collection_name=EMPLOYEE_COLLECTION,
            data=new_doc
        )
        self.user_service.update_user(
            collection_name=USER_COLLECTION,
            user_id=creator_user_id,
            data={
                'is_active': True
            }
        )

        return output

