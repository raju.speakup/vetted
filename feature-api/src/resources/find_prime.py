#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import bcrypt
import datetime
from random import randint

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from commons.prime_number import PrimeNumberService
# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.employee_schema import (
    number_schema
)
#------------------------------------------------------------------------------


class Prime(Resource):
    """Employee Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.service = PrimeNumberService()

    @swagger.operation(
        parameters=[
            {
                "name": "number",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The company_id to be fetched"
            }

        ],
        notes='Get company ',
        nickname='Get company')
    @validate_json
    @validate_schema(number_schema(), is_path=True)
    @jwt_required
    def get(self, number):
        """Get company"""
        output = self.service.find_n_prime(int(number))

        return output