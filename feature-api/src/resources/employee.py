#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims
)
from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import bcrypt
import datetime
from random import randint

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import MongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from commons.role_validator import validate_user_action
from commons.email_service import EmailService, email_sender
from configuration.config import (
    DATE_FORMAT,
    USER_COLLECTION
)
from services.users import UserService
from swagger.user_swagger import (
    CreateUser,
)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.user_schema import (
    create_user,
    bulk_delete_schema,
    list_schema
)
#------------------------------------------------------------------------------


class Employee(Resource):
    """Employee Resource."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = MongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()
        self.email_service = EmailService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateUser.__name__,
                "paramType": "body",
                "required": True,
                "description": "POST body"
            }
        ],
        notes='Employee create',
        nickname='Employee create')
    @validate_json
    @validate_schema(create_user())
    @jwt_required
    @validate_user_action(action='create_employee')
    def post(self):
        """Create employee"""
        print(request.data.decode('utf-8'))
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data
        email = data.get('email')
        user_id = str(uuid.uuid4())
        creator_user_id = get_jwt_claims().get('user_id')
        pwd = str(randint(1000, 9999))
        password_hash = bcrypt.hashpw(
            pwd.encode('utf-8'),
            bcrypt.gensalt()
        )
        new_doc.update({
            "user_id": user_id,
            "is_active": True,
            "role": "employee",
            "password": password_hash.decode('utf-8'),
            "created_by": creator_user_id,
            "updated_by": creator_user_id,
            "created_at": datetime.datetime.now().strftime(DATE_FORMAT),
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })

        output = self.service.create_user(
            collection_name=USER_COLLECTION,
            data=new_doc
        )
        if output[-1] == 201:
            email_sender.delay(
                email,
                'Invitation to login',
                'Username : {} ; Password : {} '.format(email, pwd)
            )
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'list',
                "paramType": "body",
                "required": True,
                "description": "The list of company_ids to be deleted"
            }

        ],
        notes='Delete employees',
        nickname='Delete employees')
    @validate_json
    @validate_schema(bulk_delete_schema())
    @jwt_required
    @validate_user_action(action='delete_employee')
    def delete(self):
        """Bulk delete employee"""
        data = json.loads(request.data.decode('utf-8'))
        ids = data.get('user_ids')
        output = self.service.bulk_delete_users(
            user_ids=ids
        )
        return output
