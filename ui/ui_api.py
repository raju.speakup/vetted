from flask import Flask, request, redirect, url_for, render_template, send_from_directory, jsonify
from flask import Flask, session

app = Flask(__name__)

API_SERVER = '104.198.172.7'

@app.route('/')
def login():
    return render_template('login.html', API_SERVER=API_SERVER)

@app.route('/super_admin')
def super_admin():
    return render_template('super_admin_index.html', API_SERVER=API_SERVER)

@app.route('/admin')
def admin():
    return render_template('admin_index.html', API_SERVER=API_SERVER)

@app.route('/employee')
def employee():
    return render_template('employee_index.html', API_SERVER=API_SERVER)

@app.route('/super_admin/prime')
def super_admin_prime():
    return render_template('super_prime_number.html', API_SERVER=API_SERVER)

@app.route('/admin/prime')
def admin_prime():
    return render_template('admin_prime_number.html', API_SERVER=API_SERVER)

@app.route('/employee/prime')
def employee_prime():
    return render_template('emp_prime_number.html', API_SERVER=API_SERVER)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=8080)

