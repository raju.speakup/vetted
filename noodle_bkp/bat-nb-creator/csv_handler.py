import sys
import csv


class CsvHandler:

    def __init__(self, fname):
        self.fname = fname

    def read_csv(self):
        try:
            rows = list()
            with open(self.fname, encoding='utf-8-sig') as file_obj:
                reader=csv.DictReader(file_obj)
                for row in reader:
                    rows.append(row)
            return rows
        except Exception as err:
            message = '{} - Unable to read csv file - {}'.format(self.fname)
            sys.exit(message)

    def write_csv(self, row_list):
        try:
            fieldnames = self.get_headers()
            print("Row list {}".format(row_list))
            with open(self.fname, 'w') as file_obj:
                writer = csv.DictWriter(file_obj, fieldnames=fieldnames, dialect='excel')
                writer.writeheader()
                writer.writerows(row_list)
        except Exception as err:
            message = '{} - Unable to write csv file - {}'.format(self.fname)
            sys.exit(message)

    def get_headers(self):
        with open(self.fname, 'r') as file_obj:
            reader=csv.DictReader(file_obj)
            return reader.fieldnames



obj = CsvHandler('/Users/c_rajkumar.t/Documents/nb_test.csv')

for row in obj.read_csv():
    print (row)

obj.write_csv([{'type': 'data_wrangling', 'description': 'Test Notebook', 'Notebook_path': '/test/test.pynb', 'status': 'Success', 'Name': 'Test'}])