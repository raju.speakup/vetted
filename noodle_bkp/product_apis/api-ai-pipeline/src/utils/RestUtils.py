from src.utils.RestClient import RestClient


def rest_client_get(token, url, request_headers=None ):
    rest_client = None
    if token is None:
        rest_client = RestClient()
    else:
        rest_client = RestClient(token, request_headers)
    response = rest_client.get(url)
    return response


def rest_client_post(token, url, payload,request_headers=None):
    rest_client = None
    if token is None:
        rest_client = RestClient()
    else:
        rest_client = RestClient(token,request_headers)
    response = rest_client.post(url, payload)
    return response


def rest_client_put(token, url, payload, request_headers=None):
    rest_client = None
    if token is None:
        rest_client = RestClient()
    else:
        rest_client = RestClient(token,request_headers)
    response = rest_client.put(url, payload)
    return response


def rest_client_delete(token, url, payload=None,request_headers=None):
    rest_client = None
    if token is None:
        rest_client = RestClient()
    else:
        rest_client = RestClient(token,request_headers)
    response = rest_client.delete(url, payload)
    return response