import requests
HEADER_ACCEPT_KEY = 'Accept'
HEADER_ACCEPT_VALUE = 'application/json'
HEADER_CONTENT_TYPE_KEY = 'Content-Type'
HEADER_CONTENT_TYPE_VALUE = 'application/json'
HEADER_AUTHORIZATION_KEY = "Authorization"
HEADER_AUTHORIZATION_VALUE = "Bearer "


class RestClient:
    response = None
    headers = None
    TIMEOUT = 60

    def __init__(self, token=None,request_headers=None):
        self.headers = {}
        self.response = None
        self.headers[HEADER_ACCEPT_KEY] = HEADER_ACCEPT_VALUE
        self.headers[HEADER_CONTENT_TYPE_KEY] = HEADER_CONTENT_TYPE_VALUE

        if token is not None:
            self.headers[HEADER_AUTHORIZATION_KEY] = token

        if request_headers is not None:
           for key in request_headers.keys():
               self.headers[key] = request_headers[key]

    def get(self, url):
        self.response = None

        try:
            self.response = requests.get(url, headers=self.headers, timeout=self.TIMEOUT)
            # or timeout = (Connection_timeout, Read_Timeout)
            # self.response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            err_msg = ''
            for err in e.args:
                err_msg = err_msg + err
            return err_msg
        except requests.exceptions.ConnectionError as e:
            # In the event of a network problem such as DNS failures, refused connection, etc
            return None
        except requests.exceptions.ConnectTimeout as e:
            # The request timed out while trying to connect to the remote server.
            return None
        except requests.exceptions.ReadTimeout as e:
            return None
        except requests.exceptions.RequestException as e:
            return None
        return self.response

    def post(self, url, payload):
        self.response = None;
        self.response = requests.post(url, payload, headers=self.headers)
        return self.response

    def put(self, url, payload):
        self.response = None

        if payload is None:
            self.response = requests.put(url, headers=self.headers)
        else:
            self.response = requests.put(url, payload, headers=self.headers)
        return self.response

    def delete(self, url, payload):
        self.response = None
        self.response = requests.delete(url, payload, headers=self.headers)
        return self.response

'''
response = requests.get(url, allow_redirects=False)
'''