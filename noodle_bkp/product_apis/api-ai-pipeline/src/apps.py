from flask import Flask, request, g
from flask_restful import Api
from flask_restful_swagger import swagger
from src.resources.nodes import NodeResource
from src.resources.nodes_list import NodeListResource

from src.resources.kgraphs import KGraphResource
from src.resources.kgraphs_list import KGraphListResource

from src.resources.ai_dag import AIDagResource
from src.resources.ai_dag_list import AIDagListResource
from src.resources.ai_dag_bulk import AIDagBulkResource
from src.resources.ai_notebook_node import AINotebookNode
from src.resources.notebook_params import NotebookParamsResource
from src.resources.notebook_run_details import NotebookRunResource

from src.resources.ai_dag_run import AIDagRunResource
from src.resources.ai_dag_test import AIDagTestResource
from src.resources.ai_dag_commit import AIDagCommitResource
from src.resources.ai_dag_runs_details import AIDagRunsDetailsResource
from src.resources.ai_dag_node_status import AIDagNodeStatusResource
from src.resources.ai_dag_node_update import AIDagNodeCommitResource

from flask_cors import CORS
from src.utils.constants import con
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
import re


app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    description="Docs for ai pipeline apis",
    produces=["application/json"],
    api_spec_url='/api/spec',
    resourcePath="/ai-pipeline"
)
SECRETE_KEY = 'some-difficult-value'
# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['PROPAGATE_EXCEPTIONS'] = True
jwt = JWTManager(app)


@app.before_request
def validate_auth():
    if (not "/admin/_refresh" == request.path) and (not re.compile('/api/spec*').match(request.path)):
        check_auth()


#@jwt_required
def check_auth():
    print("jwt token validated successfully")
    g.client_id = request.headers.get('clientid')
    g.app_id = request.headers.get('appid')
    print("serving request for client_id {} and app_id {}".format(request.headers.get('clientid'), request.headers.get('appid')))


api.add_resource(KGraphResource, '/k-graphs/<string:kg_id>')
api.add_resource(KGraphListResource, '/k-graphs')

api.add_resource(NodeResource, '/nodes/<string:node_id>')
api.add_resource(NodeListResource, '/nodes')

api.add_resource(AIDagResource, '/ai-pipeline/<string:ai_dag_id>')
api.add_resource(AIDagListResource, '/ai-pipeline')
api.add_resource(AIDagBulkResource, '/ai-pipeline/_bulk')
api.add_resource(AINotebookNode, '/ai-pipeline/notebooks/<string:nb_type>')
api.add_resource(NotebookParamsResource, '/ai-pipeline/nb-params/<string:nb_id>')
api.add_resource(NotebookRunResource, '/ai-pipeline/node-run/<string:nb_id>/<string:r_id>')

api.add_resource(AIDagRunResource, '/ai-pipeline/dag/run/<string:dag_id>')
api.add_resource(AIDagTestResource, '/ai-pipeline/dag/test')
api.add_resource(AIDagRunsDetailsResource, '/ai-pipeline/dag/<string:dag_id>/runs')
api.add_resource(AIDagCommitResource, '/ai-pipeline/commit/<string:dag_id>')
api.add_resource(AIDagNodeStatusResource, '/ai-pipeline/node/status/<string:dag_id>/<string:run_id>')
api.add_resource(AIDagNodeCommitResource, '/ai-pipeline/<string:dag_id>/node/<string:node_id>')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=con.API_PORT, debug=True)
