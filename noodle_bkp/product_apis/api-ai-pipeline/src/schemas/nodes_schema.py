from src.utils.json_validator import Schema, Prop


def featurebook_schema():
    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "metadata": Prop().object().build()
    }

    return Schema().keys(prop).required(["name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "node_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required([]).build()