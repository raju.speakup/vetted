from src.utils.json_validator import Schema, Prop


def kgraph_schema():

    prop = {
        "id": Prop().string().max(255).min(1).build(),
        "name": Prop().string().max(255).min(1).build(),
        "type": Prop().string().max(255).min(0).build(),
        "parent": Prop().string().max(255).min(0).build(),
        "children": Prop().array().build(),
        "child": Prop().object().build(),
    }

    return Schema().keys(prop).required(["name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "kg_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required([]).build()