from src.utils.json_validator import Schema, Prop


def ai_dag_schema():

    node_prop = {
        "id": Prop().string().max(255).min(1).build(),
        "name": Prop().string().max(255).min(1).build(),
        "type": Prop().string().max(255).min(1).build(),
        "description": Prop().string().max(1000).build(),
        "metadata": Prop().object().build(),
        "ui_metadata": Prop().object().max(255).min(0).build(),
        "parents": Prop().array().max(255).min(0).build()
    }

    items = { "type": "object", "properties":  node_prop}

    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "nodes": Prop().array().items(items).build(),
        "clientid": Prop().string().max(255).min(1).build(),
        "appid": Prop().string().max(255).min(1).build(),
        "current_version": Prop().string().max(255).min(1).build(),
        "versions": Prop().array().array().build()
    }

    return Schema().keys(prop).required(["name","nodes"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(125).min(0).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "ai_dag_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["ai_dag_id"]).build()