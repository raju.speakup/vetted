from src.utils.config import conf
from src.utils.json_utils import to_json, to_list_json
from src.utils.mongo_db import db_client
import os
import requests
from src.utils.nlogger import logger


class AICommitService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.db_client = db_client

    def create_commit(self, dag_id, commit):
        return_object, count = self.db_client.find(self.db_name, 'ai_dag_commits',
                                            query={"dag_id": dag_id,
                                                   "version_number": commit['version_number']}
                                            )
        if return_object:
            return to_json({'message': 'version number already exist'}, is_error=True)
        id = self.db_client.insert_one(self.db_name, "ai_dag_commits", commit).inserted_id
        output, count = self.db_client.find_with_projection(self.db_name, "ai_dag_commits",
                                                       query={"dag_id": dag_id},
                                                       projection={"version_number": 1, "_id": 0},
                                                      limit=100)
        versions = []
        for ver in output:
            versions.append(ver['version_number'])

        updated_nodes = {
            "nodes": commit['dag']['nodes'],
            "current_version": commit["version_number"],
            "versions": versions
        }
        output = self.db_client.find_one_and_update(self.db_name, "ai_dags", {'ai_dag_id': commit['dag_id']},
                                                 {'$set': updated_nodes})
        del commit['_id']
        return to_json(commit)

    def update_node(self,dag_id, node_id, version_number,updated_node):
        output = self.db_client.find_one_and_update(self.db_name, "ai_dags", {'ai_dag_id': dag_id, 'nodes.id': node_id},
                                                 {'$set': {"nodes.$.metadata.inputs": updated_node}})

        update_version = self.db_client.find_one_and_update(self.db_name, "ai_dag_commits",
                                                            {'dag_id': dag_id,
                                                             'dag.nodes.id': node_id,
                                                             'version_number': version_number},
                                                            {'$set': {"dag.nodes.$.metadata.inputs": updated_node}})

        del output['_id']

        return to_json(output)

    def list_commit(self, dag_id, limit, offset, version_number=None):
        query1 = {'dag_id': dag_id}
        if version_number is not None:
            query1 = {'dag_id': dag_id, 'version_number': version_number}

        result, count = self.db_client.find_with_projection(self.db_name, "ai_dag_commits", query=query1,
                                                            projection={
                                                                "dag_id": 1, "version_number": 1,
                                                                "message": 1, "committed_datetime": 1,
                                                                "author": 1, "dag": 1
                                                            },
                                                            offset=offset, limit=limit)
        output = []
        for commit in result:
            del commit['_id']
            output.append(commit)
            
        #op = output[0] if output else {}

        return to_list_json(output, list_count=count)

    def get_commit(self, dag_id, version_number):
        result, count = self.db_client.find(self.db_name, "ai_dag_commits", query={'dag_id': dag_id,
                                                                                   'version_number': version_number})
        output = {}
        for commit in result:
            del commit['_id']
            output = commit

        return output

    def initial_commit(self, dag_doc):
        id = self.db_client.insert_one(self.db_name, "ai_dag_commits", dag_doc).inserted_id
        del dag_doc['_id']
        return to_json(dag_doc)