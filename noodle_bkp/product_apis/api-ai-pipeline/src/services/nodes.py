from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json


class NodeService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client

    def list_nodes(self, limit, offset):
        result = self.client.find(self.db_name, "nodes", offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_json(output)

    def get_node(self, id):
        output =self.client.find(self.db_name, "nodes", {'node_id': id})
        for service in output:
            output = {
                        'node_id': service['node_id'],
                        'name': service['name'],
                        'description': service['description'],
                        'metadata': service['metadata']
                      }
        return to_json(output)

    def delete_node(self, id):
        result = self.client.find_one_and_delete(self.db_name, "nodes", {'node_id': id})
        return to_json(result)

    def create_node(self, app):
        id = self.client.insert_one(self.db_name, "nodes", app).inserted_id
        del app['_id']
        return to_json(app)