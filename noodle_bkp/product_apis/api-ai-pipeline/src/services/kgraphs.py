from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json


class KGraphService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client

    def list_kgraphs(self, limit, offset):
        result = self.client.find(self.db_name, "kgraphs", offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_json(output)

    def get_kgraph(self, id):
        output, count =self.client.find(self.db_name, "kgraphs", {'kg_id': id})
        for service in output:
            output = service
            del output['_id']
        return to_json(output)

    def delete_kgraph(self, id):
        result = self.client.find_one_and_delete(self.db_name, "kgraphs", {'kg_id': id})
        return to_json(result)

    def create_kgraph(self, app):
        id = self.client.insert_one(self.db_name, "kgraphs", app).inserted_id
        del app['_id']
        return to_json(app)

    def update_kgraph(self, doc_id, data):
        id = self.client.replace_one(db_name = self.db_name, collection_name = "kgraphs",query={"kg_id":doc_id},data=data).modified_count
        del data['_id']
        return to_json(data)
