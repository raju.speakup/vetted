from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime
from flask import jsonify
from src.utils.postgres_db import PostgresDBClient
import pandas as pd


class AIDatasetService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_datasets'
        self.mongo_client = db_client
        self.dag_doc = None

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query, filter_query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query, query]}


    def list_datasets(self, source_id, source_type, limit=10, offset=0, keyword=""):
        #self.refresh_datasets(source_id, source_type)
        filter_query = {'source_id': source_id}
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword),filter_query)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query, offset=offset, limit=limit)

        output = []
        for doc in result:
            del doc['_id']
            output.append(doc)

        return to_json(output)

    def get_dataset(self, dataset_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'dataset_id': dataset_id})
        for service in output:
            del service['_id']
            output = service

        self.dag_doc = output

        return to_json(output)

    def get_target_db_connection(self,source_id, source_type):
        pg_db_client = None
        try:
            if source_type == 'data_store':
                return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                              {"datastore_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            elif source_type == 'data_cartridge':
                return_object, count = self.mongo_client.find(self.db_name, "di_data_cartridges",
                                                              {"cartridge_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            else:
                return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                              {"datamart_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']

            return_object, count = self.mongo_client.find(self.db_name,"di_connections",{"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                db_name = return_object['db_name']
                host = return_object['host']
                port = return_object['port']
                user = return_object['user_name']
                password = return_object['password']

                pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client
        except Exception as e:
            print(e)
            return False

    def get_table_schema(self, source_id, source_type, table_name):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        cur = pg_conn.cursor

        #query = """select column_name, data_type from information_schema.columns where table_name = '""" + table_name + """'"""

        query = "   WITH pkey_list as (SELECT c.column_name \
                    FROM information_schema.key_column_usage AS c \
                    LEFT JOIN information_schema.table_constraints AS t \
                    ON t.constraint_name = c.constraint_name \
                    WHERE t.table_name = '{}' AND t.constraint_type = 'PRIMARY KEY') \
                    select distinct cc.column_name, cc.data_type, cc.is_nullable, \
                    case when cc.column_name  in (select column_name from pkey_list) then 'YES' \
                    else 'NO' end as primary_key \
                    from information_schema.columns cc\
                    where cc.table_name = '{}' ".format(table_name, table_name)

        cur.execute(query)
        row = cur.fetchone()
        schema = []
        while row is not None:
            schema.append(row)
            row = cur.fetchone()

        return schema

    def filter_data(self, source_id, dataset_id, source_type, filter_conditions):
        results = []
        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                     {"dataset_id": dataset_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            table_name = return_object['table_name']
            column_details = return_object['columns']
            columns = [column[0] for column in column_details]

            where_query_conditions = ''
            for filter in filter_conditions:
                where_query_conditions += "{} {} {} and ".format(filter['field_name'], filter['field_op'],
                                                                 filter['field_value'])
            where_query_conditions += '1 = 1'

            query = 'select * from {} where {}'.format(table_name, where_query_conditions)
            pg_conn = self.get_target_db_connection(source_id, source_type)

            cur = pg_conn.cursor
            cur.execute(query)

            for row in cur.fetchall():
                results.append(dict(zip(columns, row)))

        return to_json(results)

    def preview_table_records(self, source_id, table_name, source_type, limit=10):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        query = "SELECT * FROM  {} LIMIT {}".format(table_name,limit)

        data = pd.read_sql(query, pg_conn.connection)

        results = data.to_json(orient='records', date_format='iso')
        return json.loads(results)

    def refresh_dataset_metrics(self, source_id, source_type, client_id, app_id):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={"source_id":source_id, "source_type": source_type,
                                                                                          "clientid": client_id, "appid": app_id}, limit=10000)
        output = []
        for doc in result:
            del doc['_id']
            dataset_id = doc['dataset_id']
            metrics = self.get_dataset_metrics(dataset_id, pg_conn)
            new_metrics = {
                'metrics': metrics
            }

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                  query={'dataset_id': dataset_id},
                                                  update={'$set': new_metrics})

            output.append(new_metrics)

        return to_json(output)

    def get_dataset_info(self, dataset_id):
        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                      {"dataset_id": dataset_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            table_name = return_object['table_name']
            source_id = return_object['source_id']
        return table_name, source_id

