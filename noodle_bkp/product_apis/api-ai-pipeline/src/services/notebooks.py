from src.utils.mongo_db import db_client
import json
from bson import json_util
from pymongo.errors import PyMongoError
from src.utils.json_utils import to_json, to_list_json
from flask import g


class NotebookService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and": [{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and": [{'client_id': client_id}, {'app_id': app_id}, query]}

    def list_notebooks(self, nb_type, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))

        result = []
        count = 0
        if nb_type == 'DW':
            result, count = self.client.find_with_projection(self.db_name, "datawbooks", query,
                                                             projection={"dw_id": 1, "name": 1,"notebook_file_path": 1},
                                                             offset=offset, limit=limit)
        elif nb_type == 'FB':
            result, count = self.client.find_with_projection(self.db_name, "featurebooks", query,
                                                             projection={"fb_id": 1, "name": 1,"notebook_file_path": 1},
                                                             offset=offset, limit=limit)
        elif nb_type == 'SD':
            result, count = self.client.find_with_projection(self.db_name, "signald_books", query,
                                                             projection={"sd_id": 1, "name": 1,"notebook_file_path": 1},
                                                             offset=offset, limit=limit)
        elif nb_type == 'MT':
            result, count = self.client.find_with_projection(self.db_name, "modelbooks", query,
                                                             projection={"mb_id": 1, "name": 1,"notebook_file_path": 1},
                                                             offset=offset, limit=limit)
        else:
            return to_json({"message": "Invalid Notebook Type"}, is_error=True)

        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)
