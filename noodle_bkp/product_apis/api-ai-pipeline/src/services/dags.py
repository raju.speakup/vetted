from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json
from flask import g

class DAGService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.collection_name = "dags"

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query]}

    def list_dags(self, limit, offset, keyword):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result = self.client.find(self.db_name, self.collection_name, query=query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_json(output)

    def get_dag(self, id):
        output =self.client.find(self.db_name, self.collection_name, {'dag_id': id})
        for service in output:
            output = {
                        'dag_id': service['dag_id'],
                        'name': service['name'],
                        'description': service['description'],
                        'nodes': service['nodes']
                      }
        return to_json(output)

    def delete_dag(self, id):
        result = self.client.find_one_and_delete(self.db_name, self.collection_name, {'dag_id': id})
        return to_json(result)

    def create_dag(self, app):
        id = self.client.insert_one(self.db_name, self.collection_name, app).inserted_id
        del app['_id']
        return to_json(app)

    def update_dag(self, doc_id, data):
        output = self.client.find_one_and_update(db_name=self.db_name, collection_name = self.collection_name, query={'dag_id': doc_id},update= {'$set':data})
        del output['_id']
        return to_json(data)
