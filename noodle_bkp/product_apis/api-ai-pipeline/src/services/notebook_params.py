from src.utils.mongo_db import db_client
import json
from bson import json_util
from pymongo.errors import PyMongoError
from src.utils.json_utils import to_json, to_list_json
from flask import g


class NotebookParamsService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client

    def get_nb_params(self, nb_id):
        result, count = self.client.find_latest(self.db_name, "nbruns",{"entity_id": nb_id},
                                                projection={'status': 0, 'error_message': 0, 'created_at': 0,
                                                            'updated_at': 0, 'created_by': 0, 'updated_by': 0},
                                                sort_field='version')

        for service in result:
            del service['_id']
            output = service

        return to_json(output)

    def get_nb_run(self, nb_id, r_id):
        result, count = self.client.find_latest(self.db_name, "nbruns",{"entity_id": nb_id, "r_id": r_id},
                                                projection={'status': 0, 'error_message': 0, 'created_at': 0,
                                                            'updated_at': 0, 'created_by': 0, 'updated_by': 0},
                                                sort_field='version')

        for service in result:
            del service['_id']
            output = service

        return to_json(output)
