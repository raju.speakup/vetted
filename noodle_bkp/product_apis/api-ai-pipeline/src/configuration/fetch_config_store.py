#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fetch all confidencial data from config store
    Functionality:
        - If any manupulations needed in environment setting
        this is place to do it

"""
# ------------------------------------------------------------------------------
# IMPORT SECTION
# ------------------------------------------------------------------------------

from configuration.config import Config

# ------------------------------------------------------------------------------
# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES USING CONFIG STORE
# ------------------------------------------------------------------------------

conf = Config()

MONGO_SERVER = conf.get_v('MONGO_SERVER')
MONGO_USER = conf.get_v("MONGO_USER")
MONGO_PASSWD = conf.get_v("MONGO_PASSWD")
DB_URL = conf.get_v("DB_URL")
DB_NAME = conf.get_v("DB_NAME")
