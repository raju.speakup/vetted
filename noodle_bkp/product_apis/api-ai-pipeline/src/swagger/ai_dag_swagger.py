from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class BulkDelete:
    resource_fields = {
        'ids': fields.List(fields.String)
    }

@swagger.model
class UIMetaData:
    resource_fields = {
        "x": fields.Integer,
        "y": fields.Integer
    }

@swagger.model
class InputAndOutput:
    resource_fields = {
        "type": fields.String,
        "key": fields.String,
        "value":fields.String
    }

@swagger.model
@swagger.nested(
    input=InputAndOutput.__name__,
    output=InputAndOutput.__name__
)
class MetaData:
    resource_fields = {
        "input": fields.List(fields.Nested(InputAndOutput.resource_fields)),
        "output": fields.List(fields.Nested(InputAndOutput.resource_fields))
    }

@swagger.model
@swagger.nested(
    ui_metadata=UIMetaData.__name__,
    metadata=MetaData.__name__
)
class Node:
    resource_fields = {
        "id" : fields.String,
        "name" : fields.String,
        "type" : fields.String,
        "parents" : fields.List(fields.String),
        "metadata": fields.Nested(MetaData.__name__),
        "ui_metadata" : fields.Nested(UIMetaData.__name__)
    }

@swagger.model
@swagger.nested(
    ui_metadata=UIMetaData.__name__,
    metadata=MetaData.__name__
)
class CreateNode:
    resource_fields = {
        "id" : fields.String,
        "name" : fields.String,
        "description": fields.String,
        "notebook_file_path": fields.String,
        "nb_id": fields.String,
        "nb_version": fields.String,
        "nb_name": fields.String,
        "type" : fields.String,
        "parents" : fields.List(fields.String),
        "metadata": fields.Nested(MetaData.__name__),
        "ui_metadata" : fields.Nested(UIMetaData.__name__)
    }

@swagger.model
@swagger.nested(
    input=InputAndOutput.__name__
)
class InputNodeData:
    resource_fields = {
        "input": fields.List(fields.Nested(InputAndOutput.resource_fields))
    }
@swagger.model
@swagger.nested(
    nodes=CreateNode.__name__
)
class CreateAIDagSwaggerModel:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'nodes':fields.List(fields.Nested(CreateNode.resource_fields))
    }

@swagger.model
@swagger.nested(
    nodes=Node.__name__
)
class AIDagSwaggerModel:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'nodes':fields.List(fields.Nested(Node.resource_fields))
    }