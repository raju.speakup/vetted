from flask_restful import Resource
from flask import request

from src.services.dag_run import AIDagRunService

from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger


class AIDagRunsDetailsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.run_service = AIDagRunService(db_name)

    @swagger.operation(
        parameters=[{
            "name": "dag_id",
            "dataType": 'string',
            "paramType": "path",
            "required": True,
            "description": "dag ID"

        }],
        nickname="get AI Dag Run Details Resource",
        notes="Get AI Dag Run Details Resource by dag Id"
    )
    def get(self, dag_id):
        output = self.run_service.get_runs_details(dag_id)
        return to_json(output)
