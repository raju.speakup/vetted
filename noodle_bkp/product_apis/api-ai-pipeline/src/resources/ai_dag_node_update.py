# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.ai_commit_service import AICommitService
from flask import request, g
import json
import datetime
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger
from swagger.ai_dag_swagger import InputNodeData

class AIDagNodeCommitResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.commit_service = AICommitService(db_name)

    @swagger.operation(
        nickname='Update ai-dag node',
        notes='Update ai-dag node by Id ',
        parameters=[
            {
                "name": "ai_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "ai dag ID"

            },
            {
                "name": "node_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "node_id"

            },
            {
                "name": "parameters",
                "dataType": InputNodeData.__name__,
                "paramType": "body",
                "required": True,
                "description": "ai dag ID"

            }
        ]
    )
    def put(self, dag_id, node_id):
        data = json.loads(request.data.decode('utf-8'))
        version_number = data['version_number']
        input_params = data['inputs']

        output = self.commit_service.update_node(dag_id, node_id, version_number, input_params)
        return output

    #revert
    def post(self, dag_id):
        version_number = request.args.get('revert_to_version_number')
        commit = self.commit_service.get_commit(dag_id, version_number)

        message = request.args.get("message", "")
        version_number = request.args.get("version_number", "v0")
        author = request.args.get("user_id", "unknown")

        commit['version_number'] = version_number
        commit['author'] = author
        commit['updated_by'] = request.headers.get("userName", "unknown")
        commit['committed_datetime'] = str(datetime.datetime.utcnow())
        commit['message'] = message

        self.commit_service.create_commit(commit)
        return to_json(commit)

    #list
    def get(self, dag_id):
        limit = request.args.get("limit", "10")
        offset = request.args.get("offset", "0")
        version_number = request.args.get("version_number", None)
        history = self.commit_service.list_commit(dag_id, int(limit), int(offset), version_number)
        return history
