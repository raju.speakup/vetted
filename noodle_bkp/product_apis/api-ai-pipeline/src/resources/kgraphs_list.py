# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
import uuid
import json
from flask import request
import datetime

from src.services.kgraphs import KGraphService
from src.utils.json_validator import validate_schema
from src.schemas.kgraph_schema import kgraph_schema, list_arg_schema
from configuration.fetch_config_store import DB_NAME, DB_URL
from bat_auth_sdk import role_validator


class KGraphListResource(Resource):

    def __init__(self):
        self.service = KGraphService(DB_NAME)
        role_validator.init(DB_URL, DB_NAME)

    #@role_validator.validate_app_user()
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        nodes = self.service.list_kgraphs(limit, offset)
        return nodes

    #@role_validator.validate_app_user()
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
                'kg_id': str(uuid.uuid4()),
                'name': str(data['name']),
                'type': str(data['type']),
                'parent': str(data['parent']),
                'children': data['children'],
                'created_at': str(datetime.datetime.utcnow()),
                'created_by': str('unknown'),
                'updated_at': str(datetime.datetime.utcnow()),
                'updated_by': str('unknown')
        }

        node = self.service.create_kgraph(data_ext)
        return node
