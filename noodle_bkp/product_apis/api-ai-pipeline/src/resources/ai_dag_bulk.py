# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.ai_dag import AIDagService
from src.utils.config import conf
from flask import request
import json
import datetime
from flask_restful_swagger import  swagger
from swagger.ai_dag_swagger import BulkDelete

class AIDagBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = AIDagService(db_name)

    @swagger.operation(
        nickname='Delete AI Dag',
        notes='Delete AI Dag by Ids',
        parameters=[
            {
                "name": "di_dag_ids",
                "dataType": BulkDelete.__name__,
                "paramType": "body",
                "required": True,
                "description": "di dag IDs"

            }
        ]
    )
    def delete(self):
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        datasets = self.service.bulk_delete_dags(ids)
        return datasets
