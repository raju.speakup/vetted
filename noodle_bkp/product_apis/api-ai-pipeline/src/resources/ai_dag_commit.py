# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.ai_commit_service import AICommitService
from flask import request, g
import json
import datetime
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger
from swagger.ai_dag_swagger import AIDagSwaggerModel

class AIDagCommitResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.commit_service = AICommitService(db_name)

    @swagger.operation(
        notes='Update Dag',
        nickname='Update Dag',
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag Id"

            },
            {
                "name": "parameters",
                "dataType": AIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "version_number",
                "dataType": 'string',
                "paramType": "query",
                "description": ""
            },
            {
                "name": "message",
                "dataType": 'string',
                "paramType": "query",
                "required": True,
                "description": "message"

            }
        ]
    )
    def put(self, dag_id):
        data = json.loads(request.data.decode('utf-8'))

        message = request.args.get("message", "")
        version_number = request.args.get("version_number", "v0")
        author = request.headers.get("userName", "unknown")

        item = {}
        item['dag_id'] = dag_id
        item['version_number'] = version_number
        item['author'] = author
        item['updated_by'] = author
        item['committed_datetime'] = str(datetime.datetime.utcnow())
        item['message'] = message
        item['dag'] = data

        if 'appid' in data:
            item['appid'] = str(data['appid'])
        else:
            item['appid'] = str(g.app_id)

        if 'clientid' in data:
            item['clientid'] = str(data['clientid'])
        else:
            item['clientid'] = str(g.client_id)

        self.commit_service.create_commit(dag_id, item)
        return to_json(item)

    #revert
    def post(self, dag_id):
        version_number = request.args.get('revert_to_version_number')
        commit = self.commit_service.get_commit(dag_id, version_number)

        message = request.args.get("message", "")
        version_number = request.args.get("version_number", "v0")
        author = request.args.get("user_id", "unknown")

        commit['version_number'] = version_number
        commit['author'] = author
        commit['committed_datetime'] = str(datetime.datetime.utcnow())
        commit['message'] = message

        self.commit_service.create_commit(commit)
        return to_json(commit)

    #list
    @swagger.operation(
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "dag ID"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "version_number",
                "dataType": 'int',
                "paramType": "query",
                "description": ""
            }
        ],
        nickname="get AI Dag Commit Resource",
        notes="Get AI Dag Commit Resource by dag Id"
    )
    def get(self, dag_id):
        limit = request.args.get("limit", "10")
        offset = request.args.get("offset", "0")
        version_number = request.args.get("version_number", None)
        history = self.commit_service.list_commit(dag_id, int(limit), int(offset), version_number)
        return history
