import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
from src.utils.json_validator import validate_schema

from src.schemas.dags_schema import list_arg_schema, ai_dag_schema

from bat_auth_sdk import role_validator
from src.services.ai_dag import AIDagService
from src.services.ai_commit_service import AICommitService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.ai_dag_swagger import CreateAIDagSwaggerModel

class AIDagListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = AIDagService(db_name)
        self.commit_service = AICommitService(db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ],
        nickname="get AI Dag List Resource",
        notes="Get AI Dag List Resource"
    )
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        output = self.service.list_dag(limit, offset, keyword)
        return output

    @swagger.operation(
        nickname='Create ai-dag',
        notes='Create ai-dag',
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateAIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": "ai dag ID"

            }
        ]
    )
    #@validate_schema(ai_dag_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        new_doc = data

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)

        new_doc['ai_dag_id'] = str(uuid.uuid4())
        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['created_by'] = request.headers.get("userName", "unknown")
        new_doc['updated_at'] = str(datetime.datetime.utcnow())
        new_doc['current_version'] = 'v0'
        new_doc['versions'] = ['v0']

        commit_doc = {
            "dag_id": new_doc['ai_dag_id'],
            "version_number": "v0",
            "author": request.headers.get('userName', 'unknown'),
            "updated_by": request.headers.get("userName", "unknown"),
            "committed_datetime": str(datetime.datetime.utcnow()),
            "message": "initial commit",
            "dag": new_doc

        }

        output = self.service.create_dag(new_doc)
        commit = self.commit_service.initial_commit(commit_doc)
        return output
