# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request, g

from src.utils.json_utils import to_json
from src.services.notebooks import NotebookService
from src.utils.config import conf
from  flask_restful_swagger import swagger

class AINotebookNode(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.notebook_service = NotebookService(db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "nb_type",
                "dataType": "string",
                "paramType": "path",
                "description": "NB Type"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ],
        nickname="get AINotebookNode",
        notes="Get AINotebookNode"
    )
    def get(self, nb_type):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")

        notebook_list = self.notebook_service.list_notebooks(nb_type, limit, offset, keyword)

        return notebook_list
