from flask_restful import Resource
from src.services.dag_run import AIDagRunService
from src.services.dag_run import DagRun

from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger

class AIDagNodeStatusResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.run_service = AIDagRunService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "dag ID"
            },
            {
                "name": "run_id",
                "dataType": 'string',
                "paramType": "path",
                "description": "run id"
            }
        ],
        nickname="get AI Dag Node Resource",
        notes="Get AI Dag Node Resource by dag Id"
    )
    #@validate_schema(id_path_schema(), is_path=True)
    def get(self, dag_id, run_id):
        dag_status = []
        nodes = self.run_service.get_dag_nodes(dag_id)
        if not nodes:
            return to_json({"message": "this pipeline is no longer available"}, is_error=True)
        for node in nodes:
            node_status = {}
            status = self.run_service.get_node_status(node['id'], run_id)
            node_status.update({"node_id": node['id'],
                                "status": status}
                              )
            dag_status.append(node_status)
        return to_json(dag_status)
