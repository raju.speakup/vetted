# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.kgraphs import KGraphService
from src.utils.json_validator import validate_schema
from src.schemas.kgraph_schema import id_path_schema
from configuration.fetch_config_store import DB_NAME, DB_URL
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger

class KGraphResource(Resource):

    def __init__(self):
        self.service = KGraphService(DB_NAME)
        role_validator.init(DB_URL, DB_NAME)

    @swagger.operation(
        parameters=[
            {
                "name": "kg_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "kg_id"
            }
        ],
        nickname="get KGraph Resource",
        notes="Get KGraph Resource by KGraph Id"
    )
    #@role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, kg_id):
        node = self.service.get_kgraph(kg_id)
        return node

    #@role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, kg_id):
        node = self.service.delete_kgraph(kg_id)
        return node
