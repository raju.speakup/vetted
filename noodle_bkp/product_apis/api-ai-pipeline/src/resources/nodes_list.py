# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request
import datetime

from src.services.nodes import NodeService
from src.utils.json_validator import validate_schema
from src.schemas.nodes_schema import featurebook_schema, list_arg_schema
from configuration.fetch_config_store import DB_NAME, DB_URL
from bat_auth_sdk import role_validator

class NodeListResource(Resource):

    def __init__(self):
        self.service = NodeService(DB_NAME)
        role_validator.init(DB_URL, DB_NAME)

    @role_validator.validate_app_user()
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        nodes = self.service.list_nodes(limit, offset)
        return nodes

    @role_validator.validate_app_user()
    @validate_schema(featurebook_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
            'node_id': str(uuid.uuid4()),
            'name': str(data['name']),
            'description': str(data['description']),
            'metadata': dict(data['metadata']),
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str('unknown'),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        }

        node = self.service.create_node(data_ext)
        return node
