import datetime
import uuid
from flask_restful import Resource
from flask import request

import json
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json
from src.services.node_status import NodeStatus
from src.services.dag_run import DagRun
from src.services.ai_datasets import AIDatasetService
from src.services.dag_run import AIDagRunService
from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.ai_dag_swagger import CreateAIDagSwaggerModel


class AIDagTestResource(Resource):
    """DataSet Schema ."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.dataset_service = AIDatasetService(db_name)
        self.service = AIDagRunService(db_name)
        self.run_service = DagRun()

    @swagger.operation(
        nickname='Create dag test',
        notes='Create dag test',
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateAIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": "ai dag ID"

            }
        ]
    )
    #@validate_schema(id_path_schema(), is_path=True)
    def post(self):
        dag = json.loads(request.data.decode('utf-8'))

        run_id = str(uuid.uuid4())
        dag_id = str(uuid.uuid4())
        responses = []
        nodes = dag['nodes']
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata'],
                "notebook_file_path": node['notebook_file_path'],
                "nb_id": node['nb_id'],
                "nb_version": node['nb_version'],
                "nb_name": node['nb_name'],
            }
            )
            if node['id'] == nodes[0]['id']:
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.save_test_dag(node_doc)
            responses.append(resp)
        output = self.run_service.run_dag(run_id, dag)
        if output is True:
            #limit = request.args.get("limit", 100)
            #table_name, source_id = self.dataset_service.get_dataset_info(dataset_id)
            #preview = self.dataset_service.preview_table_records(source_id, table_name, 'data_mart', limit)
            #return to_json(preview)
            return to_json({"message": "AI pipeline is tested successfully"}, is_error=False)
        else:
            return to_json({"message": output}, is_error=True)




