# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.nodes import NodeService
from src.utils.json_validator import validate_schema
from src.schemas.nodes_schema import id_path_schema
from configuration.fetch_config_store import DB_NAME, DB_URL
from bat_auth_sdk import role_validator


class NodeResource(Resource):

    def __init__(self):
        self.service = NodeService(DB_NAME)
        role_validator.init(DB_URL, DB_NAME)

    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, node_id):
        node = self.service.get_node(node_id)
        return node

    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, node_id):
        node = self.service.delete_node(node_id)
        return node
