import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.services.ai_dag import AIDagService
from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import id_path_schema
from src.services.node_status import NodeStatus

from bat_auth_sdk import role_validator
from src.utils.config import conf


class AIDagScheduleResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = AIDagService(db_name)

    @validate_schema(id_path_schema(), is_path=True)
    def get(self, ai_dag_id):
        dag = self.service.get_dag(ai_dag_id)
        run_id = str(uuid.uuid4())
        responses = []
        nodes = dag['data']['nodes']
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": ai_dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata']
            }
            )
            if node['type'] in ("data_store", "data_cartridge", "data_mart"):
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.schedule_dag(node_doc)
            responses.append(resp)

        return responses
