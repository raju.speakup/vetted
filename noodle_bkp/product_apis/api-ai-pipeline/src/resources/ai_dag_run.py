import datetime
import uuid

from flask_restful import Resource
from flask import request
import requests

import json
from src.services.dag_run import AIDagRunService
from src.services.ai_dag import AIDagService
from src.utils.json_validator import validate_schema
from src.services.node_status import NodeStatus
from src.services.dag_run import DagRun

from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_jwt_extended import jwt_required
from celery_tasks import celery
from flask import request, g

class AIDagRunResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = AIDagRunService(db_name)
        self.dag_service = AIDagService(db_name)
        self.run_service = DagRun()
        self.sign = conf.get_v('AI_CELERY_SIGN')

    @jwt_required
    #@validate_schema(id_path_schema(), is_path=True)
    def post(self, dag_id):

        payload = json.loads(request.data.decode('utf-8'))
        if 'current_version' in payload.keys():
            if payload['current_version'] != '':
                run_version = payload['current_version']
            else:
                run_version = 'current_version'
        else:
            run_version = 'current_version'

        dag = self.dag_service.get_dag_version(dag_id, run_version)

        #dag = self.dag_service.get_dag(dag_id)['data']

        if 'current_version' in dag.keys():
            version = dag['current_version']
        else:
            version = 'NA'

        run_id = str(uuid.uuid4())
        responses = []
        #nodes = self.service.get_dag_nodes(dag_id)
        nodes = dag['nodes']
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata'],
                "notebook_file_path": node['notebook_file_path'],
                "nb_id": node['nb_id'],
                "nb_version": node['nb_version'],
                "nb_name": node['nb_name'],
            }
            )
            if node['id'] == nodes[0]['id']:
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.save_test_dag(node_doc)
            responses.append(resp)
        start_time = str(datetime.datetime.now())

        if self.service.check_dag_running(dag_id):
            return to_json({"message": "Already running"}, is_error=True)

        self.service.set_run_doc(run_id, dag_id, start_time, version, dag, g.app_id, g.client_id)
        self.service.set_dag_run(dag_id)
        sig = celery.signature(self.sign)
        sig.apply_async(
            args=[run_id, dag_id, dag, start_time, version],
            task_id=run_id,
            queue='ai_queue_1'
        )

        sch_details = {"dag_id": dag_id, "run_id": run_id}
        return to_json(sch_details)

