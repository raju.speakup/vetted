import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.ai_dag import AIDagService
from src.utils.json_validator import validate_schema
from src.schemas.dags_schema import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.ai_dag_swagger import AIDagSwaggerModel

class AIDagResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = AIDagService(db_name)

    @swagger.operation(
        parameters=[ {
                "name": "ai_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "AI dag ID"

            }],
        nickname="get AI Dag Resource",
        notes="Get AI Dag Resource by dag Id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, ai_dag_id):
        output = self.service.get_dag(ai_dag_id)
        return output

    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, ai_dag_id):
        output = self.service.delete_dag(ai_dag_id)
        return output

    @swagger.operation(
        nickname='Update ai-dag',
        notes='Update ai-dag by Id ',
        parameters=[
            {
                "name": "ai_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "ai dag ID"

            },
            {
                "name": "parameters",
                "dataType": AIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": "ai dag ID"

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, ai_dag_id):

        data = json.loads(request.data.decode('utf-8'))
        data['updated_at'] = str(datetime.datetime.utcnow())
        data['updated_by'] = request.headers.get("userName", "unknown")

        new_doc = data

        output = self.service.update_dag(ai_dag_id, new_doc)
        return output
