# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request
import datetime

from src.services.notebook_params import NotebookParamsService
from src.configuration.fetch_config_store import DB_NAME, DB_URL
from bat_auth_sdk import role_validator


class NotebookRunResource(Resource):

    def __init__(self):
        self.service = NotebookParamsService(DB_NAME)
        role_validator.init(DB_URL, DB_NAME)

    #@role_validator.validate_app_user()
    def get(self, nb_id, r_id):
        output = self.service.get_nb_run(nb_id, r_id)
        return output

