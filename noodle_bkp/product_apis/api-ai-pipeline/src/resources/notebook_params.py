# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request
import datetime

from src.services.notebook_params import NotebookParamsService
from src.configuration.fetch_config_store import DB_NAME, DB_URL
from bat_auth_sdk import role_validator


class NotebookParamsResource(Resource):

    def __init__(self):
        self.service = NotebookParamsService(DB_NAME)
        role_validator.init(DB_URL, DB_NAME)

    @swagger.operation(
        parameters=[
            {
                "name": "nb_id",
                "dataType": "string",
                "paramType": "path",
                "description": "NB Id"
            }
        ],
        nickname="get Notebook Params Resource",
        notes="Get Notebook Params Resource"
    )
    #@role_validator.validate_app_user()
    def get(self, nb_id):
        output = self.service.get_nb_params(nb_id)
        return output

