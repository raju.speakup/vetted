# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.connection import ConnectionService
from src.utils.json_validator import validate_schema
from src.schemas.connection_schema import test_connection_schema
from src.utils.role_validator import validate_user
from src.utils.config import conf
from flask import request
import json
import datetime
import psycopg2
from flask import jsonify
from src.utils.json_utils import to_json
from  flask_restful_swagger import swagger
from swagger.connection_swagger import Connection

class TestConnectionResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = ConnectionService(db_name)

    @swagger.operation(
        nickname="Test connection",
        notes="Test connection",
        parameters=[
            {
                "name": "parameter",
                "dataType": Connection.__name__,
                "required": True,
                "paramType": "body",
                "description": ""
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    @validate_schema(test_connection_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))
        db_name = data['db_name']
        host = data['host']
        port = data['port']
        user = data['user_name']
        password = data['password']
        url = "dbname={} user={} password={} host={} port={}".format(db_name, user, password, host, port)

        try:
            connection = psycopg2.connect(url)
        except psycopg2.Error as e:
            return to_json({"message": "Test connection failed"}, is_error=True)

        if connection:
            return to_json({"message": "Test connection is successful"}, is_error=False)
