# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request
import datetime
from src.utils.nlogger import logger
from src.services.dataset import DatasetService
from src.utils.config import conf


class DatasetBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatasetService(db_name)

    def put(self):
        data = json.loads(request.data.decode('utf-8'))
        action = request.args.get("action", 'get')
        logger.log().info("performing {}".format(action))
        if action == 'delete':
            ids = data['ids']
            delete_response = self.service.delete_datasets(ids)
            return delete_response

        return []