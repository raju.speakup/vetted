# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request, g
import datetime

from src.utils.config import conf
from src.services.datastore import DatastoreService
from src.utils.json_validator import validate_schema
from src.schemas.datastore_schema import datastore_schema, list_arg_schema


class DatastoreListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.store_service = DatastoreService(db_name)

    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        datastores = self.store_service.list_datastore(limit, offset, keyword)
        return datastores

    @validate_schema(datastore_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
            'ds_id': str(uuid.uuid4()),
            'name': str(data['name']),
            'description': str(data['description']),
            'datastore_type': str(data['datastore_type']),
            'owner': str(data['owner']),
            'website_url': str(data['website_url']),
            'licensing_type': str(data['licensing_type']),
            'is_paid': str(data['is_paid']),
            'ingestion_type': str(data['ingestion_type']),
            'ingestion_details': data['ingestion_details'],
            'target_details': data['target_details'],
            'metadata': data['metadata'],
            'status': str(data['status']),
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str('unknown'),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        }

        if 'app_id' in data:
            data_ext['app_id'] = str(data['app_id'])
        else:
            data_ext['app_id'] = str(g.app_id)

        if 'client_id' in data:
            data_ext['client_id'] = str(data['client_id'])
        else:
            data_ext['client_id'] = str(g.client_id)

        datastore = self.store_service.create_datastore(data_ext)
        return datastore
