# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.dataset import DatasetService
from src.utils.json_validator import validate_schema
from src.schemas.dataset_schema import id_path_schema
from src.utils.config import conf
from flask import request
import json
import datetime


class DatasetResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatasetService(db_name)

    @validate_schema(id_path_schema(), is_path=True)
    def get(self, datastore_id, dataset_id):
        dataset = self.service.get_dataset(datastore_id, dataset_id)
        return dataset

    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, datastore_id, dataset_id):
        dataset = self.service.delete_dataset(datastore_id, dataset_id)
        return dataset

    @validate_schema(id_path_schema(), is_path=True)
    def put(self, datastore_id, dataset_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_dataset(data, datastore_id, dataset_id)
        return update_response
