# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request, g
import datetime

from src.services.connection import ConnectionService
from src.utils.json_validator import validate_schema
from src.schemas.connection_schema import connection_schema, list_arg_schema
from src.utils.config import conf
from src.utils.role_validator import validate_user
from src.utils.json_utils import to_json
from  flask_restful_swagger import swagger
from swagger.connection_swagger import Connection

class ConnectionListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = ConnectionService(db_name)

    @swagger.operation(
        nickname="Get Connection lists",
        notes="Get List of connection",
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        connections = self.service.list_connection(limit, offset, keyword)
        return connections

    @swagger.operation(
        nickname="create connection",
        notes="Create connection",
        parameters=[
            {
                "name":"parameter",
                "dataType": Connection.__name__,
                "required": True,
                "paramType": "body",
                "description": ""
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    @validate_schema(connection_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        data.update({
            'conn_id': str(uuid.uuid4()),
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str('unknown'),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        })

        if 'appid' in data:
            data['appid'] = str(data['appid'])
        else:
            data['appid'] = str(g.app_id)

        if 'clientid' in data:
            data['clientid'] = str(data['clientid'])
        else:
            data['clientid'] = str(g.client_id)

        connection = self.service.create_connection(data)
        if not connection:
            return to_json({"message": "A connection with same details already exist"}, is_error=True)
        else:
            del connection['data']['_id']
            del connection['data']['password']
            return connection, 201
