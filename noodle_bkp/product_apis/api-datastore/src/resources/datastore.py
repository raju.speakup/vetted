# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.datastore import DatastoreService
from src.utils.json_validator import validate_schema
from src.schemas.datastore_schema import id_path_schema
from src.utils.config import conf
from flask import request
import json
import datetime


class DatastoreResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatastoreService(db_name)

    @validate_schema(id_path_schema(), is_path=True)
    def get(self, ds_id):
        datastore = self.service.get_datastore(ds_id)
        return datastore

    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, ds_id):
        datastore = self.service.delete_datastore(ds_id)
        return datastore

    @validate_schema(id_path_schema(), is_path=True)
    def put(self, ds_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_datastore(data, ds_id)
        return update_response
