# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
import uuid
import json
from flask import request, g
import datetime

from src.services.dataset import DatasetService
from src.utils.json_validator import validate_schema
from src.schemas.dataset_schema import dataset_schema, list_arg_schema
from src.utils.config import conf


class DatasetListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatasetService(db_name)

    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self, datastore_id):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        datasets = self.service.list_dataset(datastore_id, limit, offset, keyword)
        return datasets

    @validate_schema(dataset_schema())
    def post(self, datastore_id):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
            'ds_id': str(uuid.uuid4()),
            'name': str(data['name']),
            'datastore_id': str(datastore_id),
            'description': str(data['description']),
            'status': str(data['status']),
            'db_source_table_name': str(data['db_source_table_name']),
            'db_target_table_name': str(data['db_target_table_name']),
            'estimated_record_count': str(data['estimated_record_count']),
            'estimated_record_size': data['estimated_record_size'],
            'sample_records': data['sample_records'],
            'schema': str(data['schema']),
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str('unknown'),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        }

        if 'app_id' in data:
            data_ext['app_id'] = str(data['app_id'])
        else:
            data_ext['app_id'] = str(g.app_id)

        if 'client_id' in data:
            data_ext['client_id'] = str(data['client_id'])
        else:
            data_ext['client_id'] = str(g.client_id)

        dataset = self.service.create_dataset(data_ext)
        return dataset
