# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.connection import ConnectionService
from src.utils.role_validator import validate_user
from src.utils.config import conf
from flask import request
import json
import datetime
from  flask_restful_swagger import swagger
from swagger.connection_swagger import DeleteConnections


class ConnectionBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = ConnectionService(db_name)

    @swagger.operation(
        nickname="Delete connection",
        notes="Delete connection",
        parameters=[
            {
                "name": "parameter",
                "dataType": DeleteConnections.__name__,
                "required": True,
                "paramType": "body",
                "description": ""
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    def delete(self):
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        connection = self.service.bulk_delete_connection(ids)
        return connection

    @validate_user.validate_user(roles=['super-admin', 'admin'])
    def put(self, conn_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_connection(data, conn_id)
        del update_response['data']['password']
        return update_response
