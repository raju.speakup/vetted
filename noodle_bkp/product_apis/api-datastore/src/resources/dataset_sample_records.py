# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.dataset_stats import DatasetStatsService
from src.utils.json_validator import validate_schema
from src.schemas.dataset_schema import id_path_schema
from src.utils.config import conf
from flask import request

class DatasetSampleRecordsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatasetStatsService(db_name)

    def get(self, datastore_id, dataset_id):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        records = self.service.list_dataset_records(datastore_id, dataset_id, limit, offset)
        return records