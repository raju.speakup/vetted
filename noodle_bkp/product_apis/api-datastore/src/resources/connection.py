# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.connection import ConnectionService
from src.utils.json_validator import validate_schema
from src.schemas.connection_schema import id_path_schema
from src.utils.role_validator import validate_user
from src.utils.config import conf
from flask import request
import json
import datetime
from flask_restful_swagger import  swagger
from swagger.connection_swagger import  Connection

class ConnectionResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = ConnectionService(db_name)

    @swagger.operation(
        nickname='Get Connection',
        notes='Get Connection by ID',
        parameters=[
            {
                "name":"connection Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "connection ID"
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, conn_id):
        connection = self.service.get_connection(conn_id)
        return connection

    @swagger.operation(
        nickname='Delete Connection',
        notes='Delete Connection by ID',
        parameters=[
            {
                "name": "connection Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "connection ID"
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, conn_id):
        connection = self.service.delete_connection(conn_id)
        return connection

    @swagger.operation(
        nickname='Delete Connection',
        notes='Delete Connection by ID',
        parameters=[
            {
                "name": "connection Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "connection ID"
            },
            {
                "name": "Data",
                "dataType": Connection.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ]
    )
    @validate_user.validate_user(roles=['super-admin', 'admin'])
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, conn_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_connection(data, conn_id)
        if 'password' in update_response.keys():
           del update_response['data']['password']
        return update_response
