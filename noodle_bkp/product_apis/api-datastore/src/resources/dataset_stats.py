# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.dataset_stats import DatasetStatsService
from src.utils.json_validator import validate_schema
from src.schemas.dataset_schema import id_path_schema
from src.utils.config import conf


class DatasetStatsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatasetStatsService(db_name)

    @validate_schema(id_path_schema(), is_path=True)
    def get(self, datastore_id, dataset_id):
        stats = self.service.list_dataset_stats(datastore_id, dataset_id)
        return stats