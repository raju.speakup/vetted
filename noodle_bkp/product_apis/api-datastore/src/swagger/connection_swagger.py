from flask_restful import  fields
from flask_restful_swagger import swagger

@swagger.model
class Connection:
    resource_fields= {
        "name": fields.String,
        "description": fields.String,
        "host": fields.String,
        "user_name": fields.String,
        "password": fields.String,
        "port": fields.String,
        "store_type": fields.String,
        "db_name": fields.String,
        "db_type": fields.String,
        "schema_name": fields.String
    }

@swagger.model
class DeleteConnections:
    resource_fields = {
        "ids": fields.List(fields.String)
    }