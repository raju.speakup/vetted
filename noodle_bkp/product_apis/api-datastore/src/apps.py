import re

from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger

from src.resources.datastore import DatastoreResource
from src.resources.datastore_list import DatastoreListResource
from src.resources.datastore_bulk import DatastoreBulkResource

from src.resources.dataset import DatasetResource
from src.resources.dataset_list import DatasetListResource
from src.resources.dataset_bulk import DatasetBulkResource
from src.resources.dataset_stats import DatasetStatsResource
from src.resources.dataset_sample_records import DatasetSampleRecordsResource

from src.resources.connection import ConnectionResource
from src.resources.connection_list import ConnectionListResource
from src.resources.test_connection import TestConnectionResource
from src.resources.connection_bulk import ConnectionBulkResource

from flask_cors import CORS
from src.utils.constants import con
from src.resources.admin import AdminResource
from src.utils.nlogger import logger
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask import request, g

app = Flask(__name__)
CORS(app)
SECRETE_KEY = 'some-difficult-value'

@app.before_first_request
def global_init():
    logger.log().info("started initialising")

    logger.log().info("completed initialising")

@app.before_request
def validate_auth():
    if (not "/admin/_refresh" == request.path) and (not re.compile('/api/spec*').match(request.path)):
        check_auth()


@jwt_required
def check_auth():
    print("jwt token validated successfully")
    g.client_id = request.headers.get('clientid')
    g.app_id = request.headers.get('appid')
    print("serving request for client_id {} and app_id {}".format(request.headers.get('clientid'), request.headers.get('appid')))


api = swagger.docs(
    Api(app), apiVersion='0.1',
    basePath="http://localhost:5000/api/v0/",
    api_spec_url='/api/spec',
    description="Docs for Data Store Layer API's",
    resourcePath="/datastore")

api.add_resource(DatasetBulkResource, '/datastores/<string:datastore_id>/datasets/_bulk')
api.add_resource(DatasetResource, '/datastores/<string:datastore_id>/datasets/<string:dataset_id>')
api.add_resource(DatasetListResource, '/datastores/<string:datastore_id>/datasets')
api.add_resource(DatasetStatsResource, '/datastores/<string:datastore_id>/datasets/<string:dataset_id>/stats')
api.add_resource(DatasetSampleRecordsResource, '/datastores/<string:datastore_id>/datasets/<string:dataset_id>/records')

api.add_resource(DatastoreBulkResource, '/datastores/_bulk')
api.add_resource(DatastoreResource, '/datastores/<string:ds_id>')
api.add_resource(DatastoreListResource, '/datastores')

api.add_resource(ConnectionResource, '/connections/<string:conn_id>')
api.add_resource(ConnectionListResource, '/connections')
api.add_resource(TestConnectionResource, '/test_connection')
api.add_resource(ConnectionBulkResource, '/connections/_bulk')

api.add_resource(AdminResource, '/admin/_refresh')

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['PROPAGATE_EXCEPTIONS'] = True

jwt = JWTManager(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=con.API_PORT, debug=True)