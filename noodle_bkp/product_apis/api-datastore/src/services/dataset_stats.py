from src.utils.mongo_db import db_client
#import pymssql
import pandas as pd
import json


class DatasetStatsService:

    def __init__(self, mongo_db_name):
        self.mongo_db_name = mongo_db_name
        self.mongo_client = db_client

    def list_dataset_stats(self, datastore_id, dataset_id):
        result = self.get_target_db_connection(datastore_id, dataset_id)

        if result['connection'] is None:
            return {'error':result['error']}

        db_target_host, db_target_port, db_target_name, db_target_schema_name, db_table, db_username, db_password = result['db_details']

        mssql_connector = result['connection']

        query = "SELECT TOP 1000 * FROM [{}].[{}].[{}]".format(db_target_name, db_target_schema_name, db_table)
        df = pd.read_sql(query, mssql_connector)

        stats_result = df.describe(include='all').to_json()
        stats_result = json.loads(stats_result)
        results = []
        for key, value in stats_result.items():
            stat = dict()
            stat.setdefault("columnName", key)
            stat.setdefault("stats", value)
            results.append(stat)

        return results

    def list_dataset_records(self, datastore_id, dataset_id, limit, offset):
        result = self.get_target_db_connection(datastore_id, dataset_id)

        if result['connection'] is None:
            return {'error':result['error']}

        db_target_host, db_target_port, db_target_name, db_target_schema_name, db_table, db_username, db_password = result['db_details']

        mssql_connector = result['connection']

        query = "SELECT * FROM [{}].[{}].[{}] ORDER BY 1 OFFSET {} ROWS FETCH NEXT {} ROWS ONLY".format(db_target_name, db_target_schema_name, db_table, offset, limit)
        df = pd.read_sql(query, mssql_connector)

        records = df.to_json(orient='records')
        records = json.loads(records)
        return records

    def is_all_db_fields_exist(self, datastore, dataset):

        error = ""

        if 'target_details' not in datastore['data']:
            error += "target_details is missing;"
            db_target_details = {}
        else:
            db_target_details = datastore['data']['target_details']

        if 'db_target_name' not in db_target_details:
            error += "db_target_name is missing;"

        if 'db_target_host' not in db_target_details:
            error += "db_target_host is missing;"

        if 'db_target_name' not in db_target_details:
            error += "db_target_name is missing;"

        if 'db_target_port' not in db_target_details:
            error += "db_target_port is missing;"

        if 'db_target_username' not in db_target_details:
            error += "db_target_username is missing;"

        if 'db_target_password' not in db_target_details:
            error += "db_target_password is missing;"

        if 'db_target_schema_name' not in db_target_details:
            error += "db_target_schema_name is missing;"

        if 'db_target_table_name' not in dataset['data']:
            error += "db_target_table_name is missing;"

        return error == "", error

    def get_metadata(self, datastore_id, dataset_id):
        try:
            result = self.get_target_db_connection(datastore_id, dataset_id)

            if result['connection'] is None:
                return {'error':result['error']}

            db_target_host, db_target_port, db_target_name, db_target_schema_name, db_table, db_username, db_password = result['db_details']

            mssql_connector = result['connection']

            total_columns_result = pd.read_sql("SELECT count(*) FROM INFORMATION_SCHEMA.Columns where TABLE_NAME='{}'".format(db_table), mssql_connector)
            total_size_result = pd.read_sql("exec sp_spaceused {}".format(db_table), mssql_connector)

            metadata = {
                'total_colmns': str(total_columns_result.iloc[0,0]),
                'total_rows': total_size_result.iloc[0,1],
                'total_size': float("{0:.2f}".format(float(total_size_result.iloc[0,3].split(" ")[0]) / 1024))
             }
            return {'metadata': metadata}
        except Exception as e:
            return {'metadata': {'error': str(e)}}

    def get_target_db_connection(self, datastore_id, dataset_id):
        from src.services.datastore import DatastoreService
        from src.services.dataset import DatasetService
        dataset_service = DatasetService(self.mongo_db_name)
        datastore_service = DatastoreService(self.mongo_db_name)
        datastore = datastore_service.get_datastore(datastore_id)
        dataset = dataset_service.get_dataset(datastore_id, dataset_id)

        isValid, error = self.is_all_db_fields_exist(datastore, dataset)
        if(isValid == False):
            return {'error': error, 'connection': None}

        db_target_details = datastore['data']['target_details']

        db_target_name = db_target_details['db_target_name']
        db_target_host = db_target_details['db_target_host']
        db_target_port = db_target_details['db_target_port']
        db_username = db_target_details['db_target_username']
        db_password = db_target_details['db_target_password']

        db_target_schema_name = db_target_details['db_target_schema_name']
        db_table = dataset['data']['db_target_table_name']

        mssql_connector = pymssql.connect(db_target_host, db_username, db_password, db_target_name)

        return {'connection': mssql_connector, 'db_details': (db_target_host, db_target_port, db_target_name, db_target_schema_name, db_table, db_username, db_password)}