from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from src.services.dataset import DatasetService
from flask import g


class DatastoreService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.dataset_service = DatasetService(db_name)

    def search_query(self, keyword):
        return {"$or":[{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query]}

    def list_datastore(self, limit, offset, keyword):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.client.find(self.db_name, "datastores", query=query, offset=offset, limit=limit)

        output = []
        ds_ids = []
        for service in result:
            del service['_id']
            ds_ids.append(service['ds_id'])
            output.append(service)

        datasets_output = self.dataset_service.list_dataset2(ds_ids, 1000, 0, "")
        datasets = datasets_output['data']
        for dataset in datasets:
            for datastore in output:
                if dataset['datastore_id'] == datastore['ds_id']:
                    if 'datasets' not in datastore:
                        datastore['datasets']=[]
                    datastore['datasets'].append(dataset)

        return to_list_json(output, list_count=count)

    def compute_stats(self, datasets):
        total_size = total_rows = 0

        for dataset in datasets:
            if 'metadata' in dataset:
                if 'total_size' in dataset['metadata']:
                    total_size += int(dataset['metadata']['total_size'])

                if 'total_rows' in dataset['metadata']:
                    total_rows += int(dataset['metadata']['total_rows'])

        return total_rows, total_size

    def get_datastore(self, id):
        output, count =self.client.find(self.db_name, "datastores", {'ds_id': id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_datastore(self, newObj, id):
        output = self.client.find_one_and_update(self.db_name, "datastores", {'ds_id': id}, {'$set':newObj})
        del output['_id']
        return to_json(output)

    def delete_datastore(self, id):
        result = self.client.find_one_and_delete(self.db_name, "datastores", {'ds_id': id})
        return to_json(result)

    def delete_datastores(self, ids):
        results = []
        for id in ids:
            result = self.client.find_one_and_delete(self.db_name, "datastores", {'ds_id': id})
            results.append(result)
        return results

    def create_datastore(self, app):
        id = self.client.insert_one(self.db_name, "datastores", app).inserted_id
        del  app['_id']
        return to_json(app)