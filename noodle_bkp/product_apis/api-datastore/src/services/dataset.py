from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g


class DatasetService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client

    def search_query(self, keyword):
        return {"$or":[{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query, query1):
        if query is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query, query1]}

    def list_dataset(self, id, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword), {'datastore_id': id})
        result, count = self.client.find(self.db_name, "datasets", query=query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def list_dataset2(self, ids, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword), {'datastore_id': { '$in': ids}} )
        result, count = self.client.find(self.db_name, "datasets", query=query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_dataset(self, datastore_id, id):
        output, count =self.client.find(self.db_name, "datasets", {'ds_id': id, 'datastore_id': datastore_id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_dataset(self, newObj, datastore_id, id):
        output = self.client.find_one_and_update(self.db_name, "datasets", {'ds_id': id, 'datastore_id': datastore_id}, {'$set':newObj})
        del output['_id']
        return to_json(output)

    def delete_dataset(self, datastore_id, id):
        result = self.client.find_one_and_delete(self.db_name, "datasets", {'ds_id': id, 'datastore_id': datastore_id})
        return to_json(result)

    def delete_datasets(self, ids):
        results = []
        for id in ids:
            result = self.client.find_one_and_delete(self.db_name, "datasets", {'ds_id': id})
            results.append(result)
        return results

    def create_dataset(self, app):
        id = self.client.insert_one(self.db_name, "datasets", app).inserted_id
        del  app['_id']
        return to_json(app)