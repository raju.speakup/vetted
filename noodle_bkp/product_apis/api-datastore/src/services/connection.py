from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from src.services.dataset_stats import DatasetStatsService
from flask import g


class ConnectionService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword,  "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and": [{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and": [{'client_id': client_id}, {'app_id': app_id}, query]}

    def list_connection(self, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.client.find(self.db_name, "di_connections", query=query, offset=offset, limit=limit)
        output = []
        stats_service = DatasetStatsService(self.db_name)
        for service in result:
            del service['_id']
            #metadata = stats_service.get_metadata(service['datastore_id'], service['ds_id'])
            #service.update(metadata)
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_connection(self, conn_id):
        output, count = self.client.find(self.db_name, "di_connections", {'conn_id': conn_id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_connection(self, new_delta_doc, conn_id):
        output = self.client.find_one_and_update(self.db_name, "di_connections", {'conn_id': conn_id}, {'$set': new_delta_doc})
        if output:
            del output['_id']
        return to_json(output)

    def delete_connection(self, conn_id):
        assigned, count = self.client.find(self.db_name, "clients", query={"connections.conn_id": conn_id})
        if assigned:
            return to_json({"message": "Can't delete a connection which is assigned to a client"}, is_error=True)
        else:
            result = self.client.find_one_and_delete(self.db_name, "di_connections", {'conn_id': conn_id})
        return to_json(result)

    def bulk_delete_connection(self, ids):
        results = []
        for id in ids:
            assigned, count = self.client.find(self.db_name, "clients", {"connections.conn_id": id})
            if assigned:
                pass
            else:
                result = self.client.find_one_and_delete(self.db_name, "di_connections", {'conn_id': id})
                results.append(result)
        return to_json(results)

    def create_connection(self, dag_doc):
        return_object, count = self.client.find(self.db_name, "di_connections", {
            "host": dag_doc['host'],
            "user_name": dag_doc['user_name'],
            "password": dag_doc['password'],
            "port": dag_doc['port'],
            "source_type": dag_doc['source_type'],
            "db_name": dag_doc['db_name'],
            "db_type": dag_doc['db_type'],
            "name": dag_doc['name']
        })
        if len(return_object) != 0:
            return False

        id = self.client.insert_one(self.db_name, "di_connections", dag_doc).inserted_id
        return to_json(dag_doc)
