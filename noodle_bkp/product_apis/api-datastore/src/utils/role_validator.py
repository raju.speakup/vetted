from flask_jwt_extended import get_jwt_claims
from src.utils.json_utils import to_json
from src.utils.mongo_db import db_client
from functools import wraps
from jsonschema import ValidationError


class ValidationUtil:

    def __init__(self, db_name):
        self.db_name = db_name
        self.mongo_client = db_client

    def validate_user(self, roles):
        """Validate user with the required role."""
        def decorator(f):
            @wraps(f)
            def wrapper(*args, **kw):
                try:
                    user_claims = get_jwt_claims()
                    user_role = user_claims['userRole']
                    if user_role not in roles:
                        return to_json({"message": "User does not have required role to for this action"}, is_error=True)
                    if 'clientid' in kw.keys() and user_role == 'manager':
                        user_id = user_claims['user_id']
                        return_object = self.mongo_client.find_one(
                            collectionname="users",
                            data={"$and": [
                                {
                                    "user_id": user_id
                                },
                                {
                                    "clients": {
                                        "$elemMatch": {
                                            "clientid": kw['client_id']
                                        }
                                    }
                                }]}
                        )
                        if not return_object:
                            return to_json({"message": "Manager does not belong to this client"},
                                           is_error=True)

                except ValidationError as err:
                    return to_json({"message": err.args[0]}, is_error=True)
                return f(*args, **kw)
            return wrapper
        return decorator


validate_user = ValidationUtil('bat_db')
