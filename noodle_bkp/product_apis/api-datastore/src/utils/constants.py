class Const(object):
    MONGO_HOST = "localhost"
    MONGO_PORT = 27017
    API_PORT = 8022
    MONGO_SERVER = 'UBUNTUMONGODEV01:27017'
    MONGO_DB_NAME = 'NoodleApp'
    MONGO_DB_COLLECTION = 'catalog_store'
    MONGO_USER = 'mongo-admin'
    MONGO_PASSWD = 'noodleadmin'
    S3_BUCKET_NAME = "noodleai-postman-collections"
    NOTEBOOK_URL = "http://192.168.2.142:8006/user/test/notebooks/beast/books/"
    NOTEBOOK_API_URL = "/user/test/api/contents/beast/books/"
    MONGO_SERVER_URL = "".join(['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])

con = Const()