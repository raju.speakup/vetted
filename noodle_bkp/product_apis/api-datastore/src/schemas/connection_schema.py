from src.utils.json_validator import Schema, Prop


def connection_schema():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "host": Prop().string().max(1000).min(1).build(),
        "description":Prop().string().max(1000).min(1).build(),
        "user_name": Prop().string().max(255).min(1).build(),
        "password": Prop().string().max(255).min(1).build(),
        "port": Prop().string().max(255).min(1).build(),
        "source_type": Prop().string().max(255).min(1).enum(['database','csv', 'pickle']).build(),
        "db_name": Prop().string().max(255).min(1).build(),
        "db_type": Prop().string().max(255).min(1).build(),
        "schema_name": Prop().string().max(255).min(3).build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build()
    }

    return Schema().keys(prop).required(["name", "host", "user_name", "password", "port", "source_type",
                                         "db_name", "db_type"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "conn_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["conn_id"]).build()


def test_connection_schema():
    prop = {
        "host": Prop().string().max(100).min(1).build(),
        "db_name": Prop().string().max(100).min(1).build(),
        "user_name": Prop().string().max(100).min(1).build(),
        "password": Prop().string().max(100).min(1).build(),
        "port": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["host", "db_name", "user_name", "password", "port"]).build()
