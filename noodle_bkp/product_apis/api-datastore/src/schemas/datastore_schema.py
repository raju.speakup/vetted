from src.utils.json_validator import Schema, Prop


def datastore_schema():
    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "datastore_type": Prop().string().max(255).min(0).build(),
        "owner": Prop().string().max(255).min(0).build(),
        "website_url": Prop().string().max(255).min(0).build(),
        "licensing_type": Prop().string().max(255).min(0).build(),
        "is_paid": Prop().string().max(255).min(0).build(),
        "ingestion_type": Prop().string().max(255).min(0).build(),
        "ingestion_details": Prop().object().build(),
        "target_details": Prop().object().build(),
        "metadata": Prop().object().build(),
        "status": Prop().string().max(255).min(0).build(),
        "app_id": Prop().string().max(255).min(0).build(),
        "client_id": Prop().string().max(255).min(0).build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build()
    }

    return Schema().keys(prop).required(["name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()

def id_path_schema():
    prop = {
        "ds_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required([]).build()