from src.utils.json_validator import Schema, Prop


def dataset_schema():
    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "status": Prop().string().max(255).min(0).build(),
        "db_source_table_name": Prop().string().max(255).min(0).build(),
        "db_target_table_name": Prop().string().max(255).min(0).build(),
        "estimated_record_count": Prop().string().max(255).min(0).build(),
        "estimated_record_size": Prop().string().max(255).min(0).build(),
        "sample_records": Prop().object().build(),
        "schema": Prop().object().build(),
        "app_id": Prop().string().max(255).min(0).build(),
        "client_id": Prop().string().max(255).min(0).build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build()
    }

    return Schema().keys(prop).required(["name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()

def id_path_schema():
    prop = {
        "dataset_id": Prop().string().max(100).min(1).build(),
        "datastore_id": Prop().string().max(255).min(1).build()
    }

    return Schema().keys(prop).required([]).build()