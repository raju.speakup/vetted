#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Create database collection entries and generate schema for validation."""

import json
import os

from genson import SchemaBuilder
from commons.mongo_services import NoodleMongoClient

# Path declaration
collections = os.path.join(
    os.getcwd(), 'configuration\mongo_collections')
schemas = os.path.join(os.getcwd(), 'configuration\collection_schemas')

for filename in os.listdir(collections):
    # Open file and read content
    file = open(
        os.path.join(
            collections, filename), 'r')
    content = json.loads(
        json.dumps(file.read()))
    # Create mongodb instance and start pushing collections
    _dbinstance = NoodleMongoClient()
    _dbinstance.insert_one(
        filename.split('.')[0], content) if not type(
        json.loads(content)) == list else _dbinstance.insert_many(
        filename.split('.')[0], content)

    # # Create schema builder and generate schema
    # _builder = SchemaBuilder()
    # _builder.add_schema(
    #     json.loads(content)) if not type(json.loads(
    #         content)) == list else _builder.add_schema(
    #     json.loads(content)[0])

    # # Open a file in schema folder and start storing schema
    # schema_store = schemas + '/' + filename.split('.')[0] + '.py'
    # with open(
    #         str(schema_store), "w") as file_write:
    #     file_write.write('schema' + '=' +
    #                      str(
    #                          json.loads(_builder.to_json(indent=4))))
