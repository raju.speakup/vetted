#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User Search API
    REST EndPoints:
        get:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger


# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_list_json, to_json
from services.users import UserService

from flask_jwt_extended import jwt_required


class UserMetrics(Resource):
    """User Search"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User Id"
            }
        ],
        notes='User Search',
        nickname='User Search')
    @jwt_required
    def get(self, user_id):
        output = []

        clients = self.service.get_user_clients(user_id)
        if clients:
            for client_id in clients:
                module_details = self.connection.find_selected_field(
                    collectionname="modules",
                    data={

                    },
                    projection={
                        'name': 1, 'type': 1, 'module_id': 1
                    }
                )

                client_details = self.connection.find_selected_field(
                    collectionname="clients",
                    data={
                        "clientid": client_id,
                    },
                    projection={
                        'clientName': 1
                    }
                )
                if client_details:
                    client_name = client_details[0]['clientName']

                    modules = self.service.get_user_modules( user_id, client_id)
                    details = []
                    if modules == ['*']:
                        for module in module_details:
                            all_apps, no_of_apps = self.connection.find_all_projected(
                                collectionname="apps",
                                data={"module_id": module["module_id"], "client_id": client_id, "isActive": True},
                                projection={"appid": 1, "name": 1}

                            )

                            module.update({"apps": all_apps})
                            details.append(module)
                        output.append({
                            "clientid": client_id,
                            "clientName": client_name,
                            "modules": details
                        })

                    else:
                        for module in modules:
                            all_apps, no_of_apps = self.connection.find_all_projected(
                                collectionname="apps",
                                data={"module_id": module, "client_id": client_id, "isActive": True},
                                projection={"appid": 1, "name": 1}

                            )
                            module_name = [md['name'] for md in module_details if md['module_id'] == module][0]
                            module_type = [md['type'] for md in module_details if md['module_id'] == module][0]

                            app_list, no_of_users = self.connection.find_all_projected(
                                collectionname="permissions",
                                data=
                                {
                                    "entityType": {"$in": ["app", '*']},
                                    "user_id": user_id,
                                    "isActive": True,
                                    "clientid": {"$in": [client_id, '*']},

                                },
                                projection={"entityId": 1}

                            )
                            if app_list == '*':
                                apps = all_apps
                            else:
                                #apps = [app for app in all_apps if app['appid'] in app_list]  -- needs to change after apps
                                apps = all_apps

                            details.append({
                                'name': module_name,
                                'module_id': module,
                                'type': module_type,
                                'apps': apps
                            })
                        output.append({
                            "clientid": client_id,
                            "clientName": client_name,
                            "modules": details
                        })

            return to_list_json(output, list_count=len(output))
        else:
            return to_json({"message": "no client is assigned to this user"}, is_error=True)


