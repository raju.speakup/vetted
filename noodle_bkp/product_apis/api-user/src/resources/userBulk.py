#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User Bulk API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from flask_jwt_extended import jwt_required

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_json
from commons.decorators import validate_user
from services.users import UserService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.user_swagger import BulkUserDelete, BulkUserUpdate


class UserBulk(Resource):
    """UserBulk."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": BulkUserDelete.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='User Bulk Delete',
        nickname='User Bulk Delete')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    def delete(self):
        """User bulk delete."""

        data = json.loads(request.data)
        user_ids = data['ids']
        delete_response = self.service.bulk_delete_users(user_ids)
        return to_json({"deleted users": delete_response})

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": BulkUserUpdate.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='User Bulk Disable',
        nickname='User Bulk Disable')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    def put(self):
        """User bulk Disable."""

        data = json.loads(request.data)
        user_ids = data['ids']
        disable_response = self.service.bulk_disable_users(user_ids)
        return to_json({"disabled users": disable_response})
