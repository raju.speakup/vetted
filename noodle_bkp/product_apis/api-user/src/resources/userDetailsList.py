#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from flask_jwt_extended import jwt_required

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_list_json
from commons.code_snippets import get_list_user
from commons.decorators import validate_user
from services.users import UserService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.user_swagger import CreateUser


class UserList(Resource):
    """UserList."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='User List',
        nickname='Users')
    @jwt_required
    def get(self):
        """User List."""
        result = get_list_user(self, "users", {
                          "userRole": {"$ne": "super-admin"},
                          "isActive": True}, request)

        return to_list_json(result[0].get('data'), list_count=result[0].get('resultCount'))


    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes=' User Create',
        nickname='User Create')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    def post(self):
        """Create User."""
        data = json.loads(request.data)
        output = self.service.create_user(data)
        return output
