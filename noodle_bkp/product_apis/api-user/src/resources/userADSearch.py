#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User AD Search API
    REST EndPoints:
        get:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_list_json
from flask_jwt_extended import jwt_required
from configuration.layer_fetch_environ import LDAP_URL
from commons.ldap import get_users_first_and_last_name


class UserADSearch(Resource):
    """User AD Search"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "filter_str",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User Name"
            }
        ],
        notes='User AD Search',
        nickname='User AD Search')
    @jwt_required
    def get(self, filter_str):
        users = get_users_first_and_last_name(LDAP_URL, filter_str)
        return to_list_json(users, list_count=len(users))



