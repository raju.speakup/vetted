#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User Search API
    REST EndPoints:
        get:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_list_json
from commons.code_snippets import get_list_user
from flask_jwt_extended import jwt_required


class UserSearch(Resource):
    """User Search"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "user_name",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User Name"
            }
        ],
        notes='User Search',
        nickname='User Search')
    @jwt_required
    def get(self, user_name):
        result = get_list_user(self, "users", {
            "userName": {'$regex': user_name, "$options": 'i'},
            "userRole": {"$nin": ["super-admin"]},
            "isActive": True}, request)
        return to_list_json(result[0].get('data'), list_count=result[0].get('resultCount'))


