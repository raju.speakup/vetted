#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from services.users import UserService
from commons.decorators import validate_json, validate_schema, validate_user, compare_user_role

from flask_jwt_extended import jwt_required
# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.user_swagger import UpdateUser


class User(Resource):
    """User."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = UserService()

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='User Get',
        nickname='User')
    @jwt_required
    def get(self, user_id):
        """User Get."""
        output = self.service.get_user(user_id)
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='User Delete',
        nickname='UserDelete')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    @compare_user_role()
    def delete(self, user_id):
        """User Delete.

        This is soft delete for user - Adding is active = False"
        """
        output = self.service.delete_user(user_id)
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }

        ],
        notes='User Update',
        nickname='User Update')
    @validate_json
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    @compare_user_role()
    def put(self, user_id):
        """Users Update."""
        data = json.loads(request.data)
        output = self.service.update_user(user_id, data)
        return output

