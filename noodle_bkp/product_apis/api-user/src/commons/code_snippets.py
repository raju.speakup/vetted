#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.util import return_result, get_url_args, get_sorting_list
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_406_NOT_ACCEPTABLE)
from pymongo.errors import PyMongoError
import sys
import os
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from flask_jwt_extended import get_jwt_claims
from string import Template
from configuration.layer_fetch_environ import (
    CREATE_TEMPLATE_PATH, MODIFY_TEMPLATE_PATH,DELETE_TEMPLATE_PATH,
    SMTP_URL, FROM_ADDRESS, SMTP_PORT, SMTP_PASSWORD
    )
from celery_tasks import celery

def get_list(self, collectionname, querydata, request):
    """Get list data."""
    sys.path.insert(0, os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))))
    from logger.logger import logger
    data = list()
    try:
        limit, offset, gen_sort_key, projection = get_url_args(request)
    except ValueError as err:
        logger.info(err)
        return return_result(
            data,
            "Wrong Query Parameters",
            "HTTP_406_NOT_ACCEPTABLE"), HTTP_406_NOT_ACCEPTABLE
    sort_dict = get_sorting_list(gen_sort_key)
    try:
        return_object, result_count = self.connection.find_all(
            collectionname=collectionname,
            data=querydata,
            skip=offset,
            limit=limit,
            sortdata=sort_dict
        )
        logger.info(querydata)
    except PyMongoError as err:
        logger.info(err)
        return return_result(
            data,
            err.args[0],
            HTTP_500_INTERNAL_SERVER_ERROR), HTTP_500_INTERNAL_SERVER_ERROR
    if return_object:
        for entry in return_object:
            if entry:
                entry.pop("_id")
                data.append(entry)

    return return_result(
        data,
        result_count,
        self.error,
        self.error_code), HTTP_200_OK


def get_list_user(self, collectionname, querydata, request):
    """Get list data."""
    sys.path.insert(0, os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))))
    from logger.logger import logger
    data = list()
    try:
        limit, offset, gen_sort_key, projection = get_url_args(request)
    except ValueError as err:
        logger.info(err)
        return return_result(
            data,
            "Wrong Query Parameters",
            "HTTP_406_NOT_ACCEPTABLE"), HTTP_406_NOT_ACCEPTABLE
    sort_dict = get_sorting_list(gen_sort_key)
    try:
        return_object, result_count = self.connection.find_all(
            collectionname=collectionname,
            data=querydata,
            skip=offset,
            limit=limit,
            sortdata=sort_dict
        )
        logger.info(querydata)
    except PyMongoError as err:
        logger.info(err)
        return return_result(
            data,
            err.args[0],
            HTTP_500_INTERNAL_SERVER_ERROR), HTTP_500_INTERNAL_SERVER_ERROR
    if return_object:
        for entry in return_object:
            if entry:
                entry.pop("_id")
                if "password" in entry.keys():
                    entry.pop("password")
                data.append(entry)

    return return_result(
        data,
        result_count,
        self.error,
        self.error_code), HTTP_200_OK


def add_audit_log(self, collection_name, audit_data):
    """Add audit Logs"""
    sys.path.insert(0, os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))))
    from logger.logger import logger
    try:
        self.connection.insert_one(
            collectionname=collection_name,
            data=json.dumps(audit_data)
        )
        logger.info(audit_data)
    except PyMongoError as err:
        logger.info(err)
        return return_result(
            audit_data,
            err.args[0],
            HTTP_500_INTERNAL_SERVER_ERROR), HTTP_500_INTERNAL_SERVER_ERROR


def read_template(filename):
    """
    Returns a Template object comprising the contents of the
    file specified by filename.
    """
    file_name = os.path.join(os.getcwd(), filename)
    with open(file_name, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)


def get_logged_in_email(self):
    logged_in_email = str()
    from logger.logger import logger
    user_claims = get_jwt_claims()
    logged_in_user = user_claims['user_id']
    try:
        return_object = self.connection.find_selected_field(
            collectionname="users",
            data={
                "user_id": logged_in_user
            },
            projection={"email": 1}
        )
        logger.info(return_object[0]['email'])
    except PyMongoError as err:
        logger.error(err)
        return return_result(
            logged_in_user,
            err.args[0],
            HTTP_500_INTERNAL_SERVER_ERROR
        ), HTTP_500_INTERNAL_SERVER_ERROR
    if return_object:
        logged_in_email = return_object[0]['email']

    return logged_in_email


@celery.task
def send_email(to_address, msg_type, logged_in_email):
    """Send Email Notification"""
    sys.path.insert(0, os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))))
    from_address = FROM_ADDRESS
    to_address = to_address + ';' + logged_in_email
    msg = MIMEMultipart()
    msg['From'] = from_address
    msg['To'] = to_address
    msg['Subject'] = "BEAST Notification"
    if msg_type == 'create':
        message_template = read_template(CREATE_TEMPLATE_PATH)
    elif msg_type == 'modify':
        message_template = read_template(MODIFY_TEMPLATE_PATH)
    elif msg_type == 'delete':
        message_template = read_template(DELETE_TEMPLATE_PATH)

    body = message_template.substitute(USER_NAME='User')
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(SMTP_URL, SMTP_PORT)
    server.starttls()
    #server.login(from_address, SMTP_PASSWORD)
    text = msg.as_string()
    server.sendmail(from_address, to_address, text)
    server.quit()


def get_projected_list(self, collectionname, querydata, projected, request):
    """Get list data."""
    sys.path.insert(0, os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))))
    from logger.logger import logger
    data = list()
    try:
        limit, offset, gen_sort_key, projection = get_url_args(request)
    except ValueError as err:
        logger.info(err)
        return return_result(
            data,
            "Wrong Query Parameters",
            "HTTP_406_NOT_ACCEPTABLE"), HTTP_406_NOT_ACCEPTABLE
    sort_dict = get_sorting_list(gen_sort_key)
    try:
        return_object, result_count = self.connection.find_selected_field_all(
            collectionname=collectionname,
            data=querydata,
            projection=projected,
            skip=offset,
            limit=limit,
            sortdata=sort_dict
        )
        logger.info(querydata)
    except PyMongoError as err:
        logger.info(err)
        return return_result(
            data,
            err.args[0],
            HTTP_500_INTERNAL_SERVER_ERROR), HTTP_500_INTERNAL_SERVER_ERROR
    if return_object:
        for entry in return_object:
            if entry:
                data.append(entry)

    return return_result(
        data,
        result_count,
        self.error,
        self.error_code), HTTP_200_OK