#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS

from resources.health import Health
from resources.debug import ConfigStore, CheckDbStatus, ServiceStatus

from resources.userDetailsId import User
from resources.userDetailsList import UserList
from resources.userBulk import UserBulk
from resources.userSearch import UserSearch
from resources.userADSearch import UserADSearch
from resources.userMetrics import UserMetrics

from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG

from celery import Celery

from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

from commons.mongo_services import NoodleMongoClient

from configuration.layer_fetch_environ import SECRETE_KEY

app = Flask(__name__)
CORS(app)

api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="User Management Service",
    resourcePath="/Users"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
jwt = JWTManager(app)

app.config['PROPAGATE_EXCEPTIONS'] = True
# ------------------------------------------------------------------------------
# Health services
# ------------------------------------------------------------------------------

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

# http://server/api/v1/debug/config_store
api.add_resource(
    ConfigStore,
    '/api/v1/debug/config_store')

# http://server/api/v1/debug/check_db_status
api.add_resource(
    CheckDbStatus,
    '/api/v1/debug/check_db_status')

# http://server/api/v1/debug/service_status
api.add_resource(
    ServiceStatus,
    '/api/v1/debug/service_status')

# ------------------------------------------------------------------------------
# User Management services
# ------------------------------------------------------------------------------

# # http://server/api/v1/{client_id}/users
# api.add_resource(
#     UserClientsList,
#     '/api/v1/user/<string:user_id>/clients')
#
# # http://server/api/v1/client/{client_id}
# api.add_resource(
#     UserClient,
#     '/api/v1/user/<string:user_id>/client/<string:client_id>')
#
# # http://server/api/v1/user/{user_id}/clients/_bulk
# api.add_resource(
#     UserClientsBulk,
#     '/api/v1/user/<string:user_id>/clients/_bulk')

# http://server/api/v1/users
api.add_resource(
     UserList,
     '/api/v1/users')

# http://server/api/v1/users/_bulk
api.add_resource(
     UserBulk,
     '/api/v1/user/_bulk')

# http://server/api/v1/user_name/{user_name}/all
api.add_resource(
     UserSearch,
     '/api/v1/user_name/<string:user_name>/all')

# http://server/api/v1/user_name/{user_name}/all
api.add_resource(
     UserADSearch,
     '/api/v1/ad-search/<string:filter_str>')


# http://server/api/v1/clients/users/{user_id}
api.add_resource(
     User,
     '/api/v1/users/<string:user_id>')


# http://server/api/v1/clients/users/{user_id}/metrics
api.add_resource(
     UserMetrics,
     '/api/v1/users/<string:user_id>/metrics')

# #  http://server/api/v1/user/{user_id}/clients/{client_id}/app/{app_id}
# api.add_resource(
#     UserApp, '/api/v1/user/<string:user_id>/clients/<string:client_id>/app/<string:app_id>')
#
# #  http://server/api/v1/user/{user_id}/clients/{client_id}/apps
# api.add_resource(
#     UserAppsList, '/api/v1/user/<string:user_id>/clients/<string:client_id>/apps')
#
# #  http://server/api/v1/user/{user_id}/clients/{client_id}/apps/_bulk
# api.add_resource(
#     UserAppsBulk, '/api/v1/user/<string:user_id>/clients/<string:client_id>/apps/_bulk')


if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8017, debug=True)
