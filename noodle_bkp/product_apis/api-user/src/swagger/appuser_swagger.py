#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateAppUser:
    """CreateApp fields."""

    resource_fields = {
        'permission': fields.String,
    }
    swagger_metadata = {
        "permission": {
            "enum": ['read', 'write']
        }
    }


@swagger.model
class UpdateAppUser:
    """UpdateApp fields."""

    resource_fields = {
        'permission': fields.String,
        "isActive": fields.Boolean

    }
    swagger_metadata = {
        "permission": {
            "enum": ['read', 'write']
        }
    }

@swagger.model
class BulkUserApp:
    """BulkUser App fields."""

    resource_fields = {
        'ids': fields.List
    }