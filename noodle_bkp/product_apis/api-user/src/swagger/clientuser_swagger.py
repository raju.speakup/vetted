#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class UpdateClientUser:
    """UpdateClientUser fields."""

    resource_fields = {
        'clientRole': fields.String,
        'isActive': fields.Boolean,

    }
    swagger_metadata = {
        "clientRole": {
            "enum": ['manager', 'user']
        }
    }

@swagger.model
class CreateClientUser:
    """CreateClientUser fields."""

    resource_fields = {
        'clientRole': fields.String,
    }
    swagger_metadata = {
        "clientRole": {
            "enum": ['manager', 'user']
        }
    }

@swagger.model
class BulkUserClient:
    """BulkUser Client fields."""

    resource_fields = {
        'ids': fields.List
    }