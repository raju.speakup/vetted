#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateUser:
    """CreateUser fields."""

    resource_fields = {
        'userName': fields.String,
        'password': fields.String,
        'email': fields.String,
        'firstName': fields.String,
        'lastName': fields.String,
        'type': fields.String,
        'userRole': fields.String
    }
    swagger_metadata = {
        "userRole": {
            "enum": ['super-admin', 'admin', 'manager', 'user', 'guest']
        },
        "type": {
            "enum": ['AD', 'DB']
        }
    }



@swagger.model
class UpdateUser:
    """UpdateUser fields."""

    resource_fields = {
        'userName': fields.String,
        'firstName': fields.String,
        'lastName': fields.String,
        'type': fields.String,
        'userRole': fields.String,
        'isActive': fields.Boolean,
        'password': fields.String,
        'isEnabled': fields.Boolean
    }
    swagger_metadata = {
        "userRole": {
            "enum": ['super-admin', 'admin', 'manager', 'user', 'guest']
        },
        "type": {
            "enum": ['AD', 'DB']
        }
    }

@swagger.model
class BulkUserDelete:
    """BulkUser fields."""

    resource_fields = {
        'ids': fields.List
    }

@swagger.model
class BulkUserUpdate:
    """BulkUser fields."""

    resource_fields = {
        'ids': fields.List
    }