user_schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "required": ["userName", "email", "type", "userRole"],
    "properties": {
        "users": {
            "description": "user schema",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "userName": {"type": "string"},
                    "password": {"type": "string"},
                    "email ": {"type": "string"},
                    "firstName": {"type": "string"},
                    "lastName": {"type": "string"},
                    "type": {"type": "string", "enum": ["AD", "DB"]},
                    "userRole": {"type": "string"},
                    "created_at": {"type": "string"},
                },
                "required": ["userName", "email", "type", "userRole", "firstName", "lastName"]
            },
        },
    },
}
