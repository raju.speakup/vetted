# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.json_utils import to_json, to_list_json
from commons.code_snippets import add_audit_log, get_projected_list, send_email, get_logged_in_email
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_203_NON_AUTHORITATIVE_INFORMATION,
    HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN, HTTP_201_CREATED)
from logger.logger import logger
import datetime
from flask_jwt_extended import get_jwt_claims
from flask import jsonify, request
from configuration.layer_fetch_environ import LDAP_URL
from commons.ldap import authenticate, search, get_user_name
# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import bcrypt
import datetime


class UserService:
    """User Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def bulk_delete_apps(self, user_id, client_id, app_ids):
        results = []
        for app_id in app_ids:
            try:
                result = self.connection.update_one(
                    collectionname="permissions",
                    data=({
                              "clientid": client_id,
                              "entityId": app_id,
                              "entityType": "app",
                              "user_id": user_id
                          }, {
                              "$set": {"isActive": False}
                          }))
                results.append(app_id)
                logger.info(app_id)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
        audit_data = {"user_id": user_id,
                      "action": "user app bulk delete",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)
        return results

    def bulk_disable_users(self, user_ids):
        results = []
        for user_id in user_ids:
            try:
                result = self.connection.update_one(
                    collectionname="users",
                    data=({
                              "user_id": user_id

                          }, {
                              "$set": {"isEnabled": False,
                                       "updated_at": str(datetime.datetime.now())
                                       }
                          }))
                results.append(user_id)
                logger.debug(user_id)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
        audit_data = {"user_id": user_id,
                      "action": "user bulk disable",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)
        return results

    def bulk_delete_clients(self, user_id, client_ids):
        results = []
        for client_id in client_ids:
            try:
                result = self.connection.update_one(
                collectionname="users",
                data=({
                          "user_id": user_id,
                          "clients": {
                              "$elemMatch": {
                                  "clientid": client_id
                              }
                          }
                      }, {
                          "$set": {"clients.$.isActive": False}
                      }))
                results.append(client_id)
                logger.info(client_id)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
        audit_data = {"user_id": user_id,
                      "action": "User Clients Bulk delete",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)
        return results

    def bulk_delete_users(self, user_ids):
        results = []
        for user_id in user_ids:
            try:
                result = self.connection.update_one(
                    collectionname="users",
                    data=({
                              "user_id": user_id

                          }, {
                              "$set": {"isActive": False,
                                       "updated_at": str(datetime.datetime.now())
                                       }
                          }))
                result = self.connection.update_many(
                    collectionname="permissions",
                    data=({
                              "user_id": user_id

                          }, {
                              "$set": {"isActive": False
                                       }
                          }))
                results.append(user_id)
                logger.debug(user_id)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
        audit_data = {"user_id": user_id,
                      "action": "user bulk delete",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)
        return results

    def get_user_clients(self, user_id):
        role = get_projected_list(self, "users",
                                        {
                                            "user_id": user_id
                                        }, {"userRole": 1}

                                    , request)
        result = get_projected_list(self, "users",
                                    {"$and": [
                                        {
                                            "user_id": user_id
                                        },
                                        {
                                            "clients": {
                                                "$elemMatch": {
                                                    "isActive": True
                                                }
                                            }

                                        }

                                    ]}, {"clients.clientid": 1}

                                    , request)
        if result[0].get('data'):
            clients = result[0].get('data')[0]['clients']
            client_list = [client['clientid'] for client in clients]
            return client_list
        elif role[0].get('data'):
            if role[0].get('data')[0]['userRole'] in ['super-admin', 'guest']:
                results = get_projected_list(self, "clients", {"isActive": True}, {"clientid": 1}, request)
                clients = results[0].get('data')
                client_list = [client['clientid'] for client in clients]
                return client_list

        else:
            return []

    def get_user_apps(self, user_id, client_id):
        role = get_projected_list(self, "users",
                                  {
                                      "user_id": user_id
                                  }, {"userRole": 1}

                                  , request)
        result = get_projected_list(self, "permissions",

                                    {
                                        "user_id": user_id,
                                        "clientid": client_id,
                                        "entityType": 'app'
                                    }

                                    , {"entityId": 1}

                                    , request)

        if role[0].get('data')[0]['userRole'] in ['super-admin','guest']:
            return ['*']
        else:
            apps = result[0].get('data')
            apps_list = [app['entityId'] for app in apps]

            return apps_list

    def get_user_modules(self, user_id, client_id):
        role = get_projected_list(self, "users",
                                  {
                                      "user_id": user_id
                                  }, {"userRole": 1}

                                  , request)
        result = get_projected_list(self, "permissions",

                                    {
                                        "user_id": user_id,
                                        "clientid": client_id,
                                        "entityType": 'module'
                                    }

                                    , {"entityId": 1}

                                    , request)

        if role[0].get('data')[0]['userRole'] in ['super-admin','guest']:
            return ['*']
        else:
            modules = result[0].get('data')
            modules_list = [module['entityId'] for module in modules]

            return modules_list

    def create_user(self, data):
        if data.get('type') not in ['AD', 'DB']:
            return to_json({"message": "{0} is not accepted as Type".format(data.get('type'))}, is_error=True)
        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"email": data['email'], "isActive": True}
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            if data['type'] == "AD":
                if 'ldap_url' in data.keys():
                    ldap_url = data.get('ldap_url')
                else:
                    ldap_url = LDAP_URL
                result, message = search(
                    ldap_url, data.get('email'))
                if not result:
                    return to_json({"message": "user is not available in AD"}, is_error=True), HTTP_403_FORBIDDEN
                first_name, last_name = get_user_name(LDAP_URL, data.get('email'))
            if 'password' in data.keys():
                password_hash = bcrypt.hashpw(
                    data.get(
                        'password').encode('utf-8'),
                    bcrypt.gensalt())
            userid = str(uuid.uuid4())
            if data['type'] == "AD":
                data.update(
                    {
                        'user_id': userid,
                        'isActive': True,
                        'created_at': str(datetime.datetime.now()),
                        'isEnabled': True,
                        'firstName': first_name,
                        'lastName': last_name,
                        'updated_at': str(datetime.datetime.now()),
                        'created_by': request.headers.get("userName", "unknown"),
                        'updated_by': request.headers.get("userName", "unknown")
                    })
            else:
                data.update(
                    {
                        'user_id': userid,
                        'isActive': True,
                        'isEnabled': True,
                        'password': password_hash.decode('utf-8'),
                        'created_at': str(datetime.datetime.now()),
                        'updated_at': str(datetime.datetime.now()),
                        'created_by': request.headers.get("userName", "unknown"),
                        'updated_by': request.headers.get("userName", "unknown")
                    })

            try:
                self.connection.insert_one(
                    collectionname="users",
                    data=json.dumps(data))
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
                logger.info(data)
            if "password" in data.keys():
                data.pop('password')
            try:
                permission = {
                    'user_id': userid
                }
                if data['userRole'] == 'admin':
                    permission.update({
                        "entityId": "*",
                        "entityType": "administrative",
                        "permission": "*",
                        "clientid": "*",
                        "isActive": True
                    })
                    self.connection.insert_one(
                        collectionname="permissions",
                        data=json.dumps(permission))
                logger.info(permission)
            except PyMongoError as err:
                logger.error(err)
                logger.info(data)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

            audit_data = {"user_id": data['user_id'],
                          "action": "user created",
                          "time": str(datetime.datetime.now())}
            add_audit_log(self, 'auditLogs', audit_data)
            logged_in_email = get_logged_in_email(self)
            # send_email.delay(data.get('email'), 'create', logged_in_email)
            return to_json(data), HTTP_201_CREATED
        else:
            return to_json({"message": "User Exist"}, is_error=True), HTTP_203_NON_AUTHORITATIVE_INFORMATION

    def get_user(self, user_id):
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"user_id": user_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "User {0} not found".format(user_id)}, is_error=True), HTTP_404_NOT_FOUND
        if return_object:
            return_object.pop("_id")
            if "password" in return_object.keys():
                return_object.pop("password")
            return to_json(return_object)

    def delete_user(self, user_id):
        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"user_id": user_id})
            logger.info(user_id)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "{0} User do not exists".format(user_id)}, is_error=True)
        else:
            result = self.connection.update_one(
                collectionname="users",
                data=({
                          "user_id": user_id

                      }, {
                          "$set": {"isActive": False,
                                   "updated_at": str(datetime.datetime.now())
                                   }
                      }))
            output = self.connection.update_many(
                collectionname="permissions",
                data=({
                          "user_id": user_id

                      }, {
                          "$set": {"isActive": False
                                   }
                      }))
        audit_data = {"user_id": user_id,
                      "action": "user delete",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)
        return to_json({"message": "User has been deleted"})

    def update_user(self, user_id, data):
        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"user_id": user_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return to_json({"message": "{0} User do not exists".format(user_id)}, is_error=True)
        else:
            if 'isActive' in data.keys():
                data.update({"isActive": bool(data['isActive'])})
            if 'password' in data.keys():
                password_hash = bcrypt.hashpw(
                    data.get('password').encode('utf-8'),
                    bcrypt.gensalt())
                data.update({"password":password_hash.decode('utf-8')})
            data.update({"updated_at": str(datetime.datetime.now())})
            self.connection.update_one(
                collectionname="users",
                data=({"user_id": user_id}, {
                    "$set": data}))
        audit_data = {"user_id": user_id,
                      "action": "user update",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)
        return_object.pop('_id')
        if 'password' in return_object.keys():
            return_object.pop('password')
        return_object.update(data)
        return to_json(return_object)
