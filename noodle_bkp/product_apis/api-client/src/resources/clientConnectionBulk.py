#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client Cartridge Bulk API
    REST EndPoints:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from commons.json_utils import to_json
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED,
    HTTP_203_NON_AUTHORITATIVE_INFORMATION, HTTP_403_FORBIDDEN, HTTP_200_OK )
from commons.decorators import validate_user
from services.clients import ClientService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.client_connection_swagger import BulkConnectionAssign, BulkConnectionDelete


class ClientConnectionsBulk(Resource):
    """UserCartridgeBulk."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "parameters",
                "dataType": BulkConnectionAssign.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client Connections Bulk Assign',
        nickname='Client Connections Bulk Assign')
    @jwt_required
    def post(self, client_id):
        """Client  Connections Bulk Assign."""

        data = json.loads(request.data)
        connections = data.get('connections')
        assign_response = self.service.bulk_assign_connections(client_id, connections)
        return assign_response, HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "parameters",
                "dataType": BulkConnectionDelete.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client Connection Bulk Delete',
        nickname='Client Connection Bulk Delete')
    def delete(self, client_id):
        """Client  Connection Bulk Delete."""

        data = json.loads(request.data)
        connections = data['ids']
        delete_response = self.service.bulk_delete_connections(client_id, connections)
        return delete_response, HTTP_200_OK



