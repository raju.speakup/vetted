#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client User Bulk API
    REST EndPoints:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema, validate_user
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED,
    HTTP_203_NON_AUTHORITATIVE_INFORMATION, HTTP_403_FORBIDDEN, HTTP_200_OK )
from commons.decorators import validate_user
from services.clients import ClientService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.client_user_swagger import BulkUserAssign, BulkUserDelete



class ClientUsersBulk(Resource):
    """UserClientBulk."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "parameters",
                "dataType": BulkUserAssign.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client User Bulk Assign',
        nickname='Client User Bulk Assign')
    @jwt_required
    @validate_user(['super-admin', 'admin', 'manager'])
    def post(self, client_id):
        """Client  User Bulk Assign."""

        data = json.loads(request.data)
        users = data.get('users')
        assign_response = self.service.bulk_assign_users(client_id, users)
        return return_result_final(
            json.dumps([{"assigned users": assign_response}]),
            1,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "parameters",
                "dataType": BulkUserDelete.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client User Bulk Assign',
        nickname='Client User Bulk Assign')
    @jwt_required
    @validate_user(['super-admin', 'admin', 'manager'])
    def delete(self, client_id):
        """Client  User Bulk Assign."""

        data = json.loads(request.data)
        users = data.get('ids')
        delete_response = self.service.bulk_delete_users(client_id, users)
        return return_result_final(
            json.dumps([{"deleted users": delete_response}]),
            1,
            self.error,
            self.error_code), HTTP_200_OK

