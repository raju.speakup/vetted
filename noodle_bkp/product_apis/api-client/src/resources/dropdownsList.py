#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask import request
from flask_jwt_extended import jwt_required

from commons.util import return_result_final
from commons.code_snippets import get_list
from commons.mongo_services import NoodleMongoClient


class DropdownList(Resource):

    def __init__(self):
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @jwt_required
    def get(self):
        type = request.args.get("type", "")
        result = get_list(self, "dropdowns", {"type": type}, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('resultCount'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]