#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list
from commons.decorators import validate_json, validate_schema, validate_user
from commons.json_utils import to_json
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED, HTTP_200_OK)
from swagger.app_swagger import CreateApp

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientdetails import schema



class AppsList(Resource):
    """Apps List."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Apps List',
        nickname='Apps')
    @jwt_required
    def get(self):
        """Apps List."""
        result = get_list(self, "apps", {
                          "isActive": True}, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('resultCount'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateApp.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='App Create',
        nickname='App Create')
    @validate_json
    @validate_schema(schema)
    @jwt_required
    @validate_user(['super-admin', 'admin', 'manager'])
    def post(self):
        """App Create."""
        data = json.loads(request.data)
        data.update(
            {"appid": str(uuid.uuid4()),
             "isActive": True})
        logger.info(data)

        # return_object = self.connection.find_one("apps", {
        #     "name": data['name']
        # })
        # if return_object:
        #     return to_json({"message": "app with provided name already exist"}, is_error=True)

        try:
            exists = self.connection.find_selected_field(collectionname="apps",
                data={ "client_id": data["client_id"], "module_id": data["module_id"], "name": data["name"]},projection={"name": 1})
            if len(exists) > 0:
                return to_json({"message": "App Name already exists"}, is_error=True), HTTP_200_OK
            self.connection.insert_one(collectionname="apps",
                                       data=json.dumps(data))
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            data,
            1,
            self.error,
            self.error_code
        ), HTTP_201_CREATED
