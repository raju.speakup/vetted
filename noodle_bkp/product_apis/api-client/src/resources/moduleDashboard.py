#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Module Metrics API
    REST EndPoints:
        get:

"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from services.modules import ModuleService
from flask_jwt_extended import jwt_required


class ModuleDashboard(Resource):
    """Module Metrics"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ModuleService()

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "module_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Module ID"
            }
        ],
        notes='Module Dashboard Get',
        nickname='Module Dashboard Get')
    @jwt_required
    def get(self, client_id, module_id):
        """Module Dashboard Get"""
        output = self.service.get_module_dashboard(client_id, module_id)
        return output



