#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User Search API
    REST EndPoints:
        get:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.code_snippets import get_list
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import HTTP_200_OK
from flask_jwt_extended import jwt_required


class ClientSearch(Resource):
    """Client Search"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_name",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client Name"
            }
        ],
        notes='Client Search',
        nickname='Client Search')
    @jwt_required
    def get(self, client_name):
        """Client Search """
        result = get_list(self, "clients", {
            "clientName": {'$regex': client_name, "$options": 'i'},
            "isActive": True}, request)

        return to_list_json(result[0].get('data'), list_count=result[0].get('resultCount')), HTTP_200_OK


