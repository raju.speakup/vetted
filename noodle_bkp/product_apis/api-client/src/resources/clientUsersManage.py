#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client User Bulk API
    REST EndPoints:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema, validate_user
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED,
    HTTP_203_NON_AUTHORITATIVE_INFORMATION, HTTP_403_FORBIDDEN, HTTP_200_OK)
from services.clients import ClientService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.client_user_swagger import BulkUser


class ClientUsersManage(Resource):
    """UserClientBulk."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "parameters",
                "dataType": BulkUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client User Bulk Operation',
        nickname='Client User Bulk Operation')
    @jwt_required
    @validate_user(['super-admin', 'admin', 'manager'])
    def post(self, client_id):
        """Client  User Bulk Operation."""

        data = json.loads(request.data)
        assigned_users = data.get('assigned_users')
        unassigned_users = data.get('unassigned_users')
        assigned_modules = data.get('assigned_modules')
        unassigned_modules = data.get('unassigned_modules')

        assigned_response_users = self.service.bulk_assign_users(client_id, assigned_users)
        unassigned_response_users = self.service.bulk_delete_users(client_id, unassigned_users)
        assigned_response_modules = self.service.bulk_assign_modules(client_id, assigned_modules)
        unassigned_response_modules = self.service.bulk_delete_modules(client_id, unassigned_modules)

        return return_result_final(
            json.dumps([{"assigned users": assigned_response_users,
                         "unassigned users": unassigned_response_users,
                         "assigned_modules": assigned_response_modules,
                         "unassigned_modules": unassigned_response_modules}]),
            1,
            self.error,
            self.error_code), HTTP_200_OK


