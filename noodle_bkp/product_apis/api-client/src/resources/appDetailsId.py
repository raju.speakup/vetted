#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema, validate_user
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.app_swagger import UpdateApp




class App(Resource):
    """App."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='App Get',
        nickname='App Get')
    @jwt_required
    def get(self, app_id):
        """App Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="apps",
                data={"appid": app_id})
        except PyMongoError as err:
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                0,
                "{0} app do not exists".format(app_id),
                "APP_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        if return_object:
            return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data.decode('utf-8')),
            1,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='App Delete',
        nickname='App Delete')
    @jwt_required
    @validate_user(['super-admin', 'admin', 'manager'])
    def delete(self, app_id):
        """App Delete """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="apps",
                data={"appid": app_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                0,
                "{0} app do not exists".format(app_id),
                "APP_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="apps",
                data=({"appid": app_id}, {
                    "$set": {"isActive": False}}))
        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            1,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateApp.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }

        ],
        notes='App Update',
        nickname='App Update')
    @validate_json
    @jwt_required
    @validate_user(['super-admin', 'admin', 'manager'])
    def put(self, app_id):
        """App Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="apps",
                data={"appid": app_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                0,
                "{0} app do not exists".format(app_id),
                "APP_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="apps",
                data=({"appid": app_id}, {
                    "$set": data}))
        return return_result_final(
            data,
            1,
            self.error,
            self.error_code), HTTP_200_OK



