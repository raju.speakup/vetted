#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
import datetime
from logger.logger import logger

from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_projected_list
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientdetails import schema
from swagger.client_swagger import UpdateClient


class ClientUsersList(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Client Users List',
        nickname='Client Users List')
    @jwt_required
    def get(self, client_id):
        """Client Users List Get."""
        result = get_projected_list(self, "users",
                                   {
                                       "clients": {
                                           "$elemMatch": {
                                               "clientid": client_id
                                           }
                                       }
                                   },
                                   {
                                       "user_id": 1, "clients.$.clientRole": 1, 'userName': 1
                                    }, request)

        modules_list = self.connection.find_selected_field(collectionname="modules",
                                                           data={},
                                                           projection={"module_id": 1})
        modules_list = [module['module_id'] for module in modules_list]
        users = []
        for user in result[0].get('data'):
            if user["clients"][0]["clientRole"] == 'manager':
                user.update({
                    "modules": modules_list
                })
            else:
                modules = self.connection.find_selected_field(collectionname="permissions",
                                                           data={"user_id": user["user_id"],
                                                                 "clientid": client_id,
                                                                 "isActive": True,
                                                                 "entityType": "module"},
                                                           projection={"entityId": 1})
                modules = [module['entityId'] for module in modules]
                user.update({
                    "modules": modules
                })
            users.append(user)

        return return_result_final(
            users,
            result[0].get('resultCount'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]
