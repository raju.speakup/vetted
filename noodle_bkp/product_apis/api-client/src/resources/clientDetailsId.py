#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json

from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json, validate_schema, validate_user
from services.clients import ClientService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientdetails import schema
from swagger.client_swagger import UpdateClient


class Client(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client Get',
        nickname='Client')
    @jwt_required
    def get(self, client_id):
        """Client Get."""
        output = self.service.get_client(client_id)
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client Delete',
        nickname='ClientDelete')
    @jwt_required
    @validate_user(['super-admin', 'admin'])
    def delete(self, client_id):
        """Client Delete.

        This is soft delete for client - Adding is active = False"
        """
        output = self.service.delete_client(client_id)
        return output

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateClient.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }

        ],
        notes='Client Update',
        nickname='Client Update')
    @validate_json
    @validate_schema(schema)
    @jwt_required
    @validate_user(['super-admin', 'admin'])
    def put(self, client_id):
        """Client Update."""
        data = json.loads(request.data)
        output = self.service.update_client(client_id, data)
        return output
