#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request, jsonify
from logger.logger import logger
from flask_jwt_extended import jwt_required

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list
from commons.json_utils import to_json, to_list_json
from commons.decorators import validate_json, validate_schema, validate_user
from services.clients import ClientService
from constants.custom_field_error import HTTP_200_OK

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientdetails import schema
from swagger.client_swagger import CreateClient


class ClientList(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Client List',
        nickname='Clients')
    @jwt_required
    def get(self):
        """Client List."""
        result = get_list(self, "clients", {"isActive": True}, request)
        return to_list_json(result[0].get('data'), list_count=result[0].get('resultCount')), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateClient.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client Create',
        nickname='Client Create')
    @validate_json
    @validate_schema(schema)
    @jwt_required
    @validate_user(['super-admin', 'admin'])
    def post(self):
        """Client Create."""
        data = json.loads(request.data)

        output = self.service.create_client(data)
        return output
