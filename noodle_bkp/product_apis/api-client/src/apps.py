#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS
from resources.health import Health
from resources.debug import ConfigStore, CheckDbStatus, ServiceStatus
from resources.clientDetailsId import Client
from resources.clientDetailsList import ClientList
from resources.clientMetrics import ClientMetrics
from resources.clientUserBulk import ClientUsersBulk
from resources.clientBulk import ClientBulk
from resources.clientSearch import ClientSearch
from resources.clientUsersManage import ClientUsersManage
from resources.clientUsersList import ClientUsersList
from resources.clientCartridgesList import ClientCartridgesList
from resources.clientCartridgeBulk import ClientCartridgesBulk
from resources.clientConnectionsList import ClientConnectionsList
from resources.clientConnectionBulk import ClientConnectionsBulk
from resources.dropdownsList import DropdownList
from resources.appDetailsId import App
from resources.appDetailsList import AppsList
from resources.modulesDetailsList import ModulesList
from resources.clientDashboard import ClientDashboard
from resources.moduleDashboard import ModuleDashboard
from resources.appMetrics import AppMetrics
from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG, SECRETE_KEY
from flask_jwt_extended import JWTManager


app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="Client Management Service",
    resourcePath="/Clients"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
jwt = JWTManager(app)

app.config['PROPAGATE_EXCEPTIONS'] = True
# ------------------------------------------------------------------------------
# Health services
# ------------------------------------------------------------------------------

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

# http://server/api/v1/debug/config_store
api.add_resource(
    ConfigStore,
    '/api/v1/debug/config_store')

# http://server/api/v1/debug/check_db_status
api.add_resource(
    CheckDbStatus,
    '/api/v1/debug/check_db_status')

# http://server/api/v1/debug/service_status
api.add_resource(
    ServiceStatus,
    '/api/v1/debug/service_status')

# http://server/api/v1/dropdowns
api.add_resource(
    DropdownList,
    '/api/v1/dropdowns')

# ------------------------------------------------------------------------------
# Client Management services
# ------------------------------------------------------------------------------

# # http://server/api/v1/clients
api.add_resource(
    ClientList,
    '/api/v1/clients')

# # http://server/api/v1/clients/_bulk
api.add_resource(
    ClientBulk,
    '/api/v1/clients/_bulk')

# http://server/api/v1/clients/{client_id}
api.add_resource(
    Client,
    '/api/v1/clients/<string:client_id>')

# http://server/api/v1/clients/search/{client_name}
api.add_resource(
    ClientSearch,
    '/api/v1/clients/search/<string:client_name>')

# http://server/api/v1/clients/{client_id}/users/_bulk
api.add_resource(
    ClientUsersBulk,
    '/api/v1/clients/<string:client_id>/users/_bulk')

# http://server/api/v1/clients/{client_id}/users/manage
api.add_resource(
    ClientUsersManage,
    '/api/v1/clients/<string:client_id>/users/manage')

# http://server/api/v1/clients/{client_id}/users/_bulk
api.add_resource(
    ClientCartridgesBulk,
    '/api/v1/clients/<string:client_id>/cartridges/_bulk')

# http://server/api/v1/clients/{client_id}/users
api.add_resource(
    ClientUsersList,
    '/api/v1/clients/<string:client_id>/users')

# http://server/api/v1/clients/{client_id}/cartridges
api.add_resource(
    ClientCartridgesList,
    '/api/v1/clients/<string:client_id>/cartridges')


# http://server/api/v1/clients/{client_id}/connections/_bulk
api.add_resource(
    ClientConnectionsBulk,
    '/api/v1/clients/<string:client_id>/connections/_bulk')

# http://server/api/v1/clients/{client_id}/cartridges
api.add_resource(
    ClientConnectionsList,
    '/api/v1/clients/<string:client_id>/connections')


# http://server/api/v1/clients/metrics
api.add_resource(
    ClientMetrics,
    '/api/v1/clients/metrics')

# http://server/api/v1/app/{app_id}
api.add_resource(
     App, '/api/v1/app/<string:app_id>')

# http://server/api/v1/apps
api.add_resource(
     AppsList, '/api/v1/apps')

# http://server/api/v1/modules
api.add_resource(
    ModulesList, '/api/v1/modules')

# http://server/api/v1/clients/{client_id}/dashboard
api.add_resource(
    ClientDashboard, '/api/v1/clients/<string:client_id>/dashboard')

# http://server/api/v1/clients/{client_id}/modules/{module_id}/dashboard
api.add_resource(
    ModuleDashboard, '/api/v1/clients/<string:client_id>/modules/<string:module_id>/dashboard')

# http://server/api/v1/clients/{client_id}/modules/{module_id}/dashboard
api.add_resource(
    AppMetrics, '/api/v1/clients/<string:client_id>/modules/<string:module_id>/apps/<string:app_id>/metrics')

if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8021, debug=True)
