# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR
# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError


class ModuleService:
    """Module Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def get_module_dashboard(self, client_id, module_id):
        module_metrics = {}
        try:
            users, no_of_users = self.connection.find_all_projected(
                collectionname="permissions",
                data=
                {
                    "entityType": {"$in": ["module", '*']},
                    "entityId": {"$in": [module_id, '*']},
                    "isActive": True,
                    "clientid": {"$in": [client_id, '*']},

                },
                projection={"user_id": 1}

            )
            apps, no_of_apps = self.connection.find_all_projected(
                collectionname="apps",
                data=
                {
                    "module_id": module_id,
                    "isActive": True,
                    "client_id": client_id

                },
                projection={"appid": 1, "name": 1}

            )
            datastore_count = self.connection.count_docs(
                collectionname="di_datastores",
                data=
                {
                    "module_id": module_id,
                    "clientid": client_id
                })
            datamart_count = self.connection.count_docs(
                collectionname="di_datamarts",
                data=
                {
                    "module_id": module_id,
                    "clientid": client_id
                })
            datacart_count = self.connection.count_docs(
                collectionname="di_data_cartridges",
                data=
                {
                    "module_id": module_id,
                    "clientid": client_id
                })

            apps_list = [app['appid'] for app in apps]
            app_by_user = []
            for app in apps_list:
                metrics = self.get_app_metrics(client_id, module_id, app)[0]['data']
                no_of_objects = metrics['teaching'] + metrics['learning'] + metrics['thinking']
                app_users = self.get_module_app_users(client_id, module_id, app, no_of_objects)
                app_name = [a['name'] for a in apps if a['appid'] == app][0]
                app_users.update({"name": app_name})
                app_by_user.append(app_users)

            users_list = [user['user_id'] for user in users]

            user_roles, no_of_users = self.connection.find_all_projected(
                collectionname="users",
                data=
                {
                    "user_id": {"$in": users_list},
                    "isActive": True

                },
                projection={"user_id": 1, "userRole": 1}

            )
            no_of_manager_roles = sum(user['userRole'] == 'manager' for user in user_roles)
            no_of_user_roles = sum(user['userRole'] == 'user' for user in user_roles)
            no_of_guest_roles = sum(user['userRole'] == 'guest' for user in user_roles)

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        ds_count = self.connection.count_docs(collectionname="di_datastores", data={"appid": {"$in": apps_list}})
        dm_count = self.connection.count_docs(collectionname="di_datamarts", data={"appid": {"$in": apps_list}})
        dc_count = self.connection.count_docs(collectionname="di_data_cartridges", data={"appid": {"$in": apps_list}})
        di_dags_count = self.connection.count_docs(collectionname="di_dags", data={"appid": {"$in": apps_list}})
        de_count = self.connection.count_docs(collectionname="data_explorers", data={"appid": {"$in": apps_list}})
        nb_dw_count = self.connection.count_docs(collectionname="datawbooks", data={"app_id": {"$in": apps_list}})
        nb_fb_count = self.connection.count_docs(collectionname="featurebooks", data={"app_id": {"$in": apps_list}})
        nb_mt_count = self.connection.count_docs(collectionname="modelbooks", data={"app_id": {"$in": apps_list}})
        nb_sd_count = self.connection.count_docs(collectionname="signald_books", data={"app_id": {"$in": apps_list}})
        ai_dags_count = self.connection.count_docs(collectionname="ai_dags", data={"appid": {"$in": apps_list}})

        total_ds_rows = sum(self.connection.get_sum(collectionname="di_datastores",
                                                    data={"appid":{"$in": apps_list}},
                                                    sum={'total': {'$sum': '$metrics.no_of_rows'}}))
        total_dc_rows = sum(self.connection.get_sum(collectionname="di_data_cartridges",
                                                    data={"appid":{"$in": apps_list}},
                                                    sum={'total': {'$sum': '$metrics.no_of_rows'}}))
        total_dm_rows = sum(self.connection.get_sum(collectionname="di_datamarts",
                                                    data={"appid":{"$in": apps_list}},
                                                    sum={'total': {'$sum': '$metrics.no_of_rows'}}))

        total_ds_size = sum(self.connection.get_sum(collectionname="di_datastores",
                                                    data={"appid":{"$in": apps_list}},
                                                    sum={'total': {'$sum': '$metrics.size'}}))
        total_dc_size = sum(self.connection.get_sum(collectionname="di_data_cartridges",
                                                    data={"appid":{"$in": apps_list}},
                                                    sum={'total': {'$sum': '$metrics.size'}}))
        total_dm_size = sum(self.connection.get_sum(collectionname="di_datamarts",
                                                    data={"appid":{"$in": apps_list}},
                                                    sum={'total': {'$sum': '$metrics.size'}}))

        total_ds_datasets = sum(self.connection.get_sum(collectionname="di_datastores",
                                                        data={"appid":{"$in": apps_list}},
                                                        sum={'total': {'$sum': '$metrics.no_of_datasets'}}))
        total_dc_datasets = sum(self.connection.get_sum(collectionname="di_data_cartridges",
                                                        data={"appid":{"$in": apps_list}},
                                                        sum={'total': {'$sum': '$metrics.no_of_datasets'}}))
        total_dm_datasets = sum(self.connection.get_sum(collectionname="di_datamarts",
                                                        data={"appid":{"$in": apps_list}},
                                                        sum={'total': {'$sum': '$metrics.no_of_datasets'}}))

        module_metrics.update({
                               "user_details": {
                                    "no_of_users": no_of_users,
                                    "user_roles": {
                                        "managers": no_of_manager_roles,
                                        "users": no_of_user_roles,
                                        "guests": no_of_guest_roles
                                    }
                                },
                               "app_details": {
                                   "apps": apps,
                                   "no_of_apps": no_of_apps,
                                   "app_users": app_by_user
                                               },
                               "objects": {
                                   "datastore_count": datastore_count,
                                   "datamart_count": datamart_count,
                                   "datacart_count": datacart_count
                               },
                               "total_rows": (total_ds_rows + total_dm_rows + total_dc_rows),
                               "total_size": (total_ds_size + total_dm_size + total_dc_size),
                               "total_datasets": (total_ds_datasets + total_dm_datasets + total_dc_datasets),
                               "teaching": {'Data Store': ds_count,
                                            'Data Mart': dm_count,
                                            'Data Cartridge': dc_count,
                                            'Data Ingestion': di_dags_count,
                                            'Data Explorer': de_count
                                             },
                               "learning": {'Data Wrangling': nb_dw_count,
                                            'Signal Detection': nb_sd_count,
                                            'Model Training': nb_mt_count,
                                            'Feature Engineering': nb_fb_count,
                                             },
                               "thinking": {'AI Pipeline': ai_dags_count
                                             }

        })
        return to_json(module_metrics), HTTP_200_OK

    @staticmethod
    def get_module_app_users(client_id, module_id, app_id, no_of_objects):
        app_user ={"appid": app_id,
                   "no_of_objects": no_of_objects}
        return app_user

    def get_app_metrics(self, client_id, module_id, app_id):
        ds_count = self.connection.count_docs(collectionname="di_datastores", data={"appid": app_id})
        dm_count = self.connection.count_docs(collectionname="di_datamarts", data={"appid": app_id})
        dc_count = self.connection.count_docs(collectionname="di_data_cartridges", data={"appid": app_id})
        di_dags_count = self.connection.count_docs(collectionname="di_dags", data={"appid": app_id})
        de_count = self.connection.count_docs(collectionname="data_explorers", data={"appid": app_id})

        nb_dw_count = self.connection.count_docs(collectionname="datawbooks", data={"app_id": app_id})
        nb_fb_count = self.connection.count_docs(collectionname="featurebooks", data={"app_id": app_id})
        nb_mt_count = self.connection.count_docs(collectionname="modelbooks", data={"app_id": app_id})
        nb_sd_count = self.connection.count_docs(collectionname="signald_books", data={"app_id": app_id})

        ai_dags_count = self.connection.count_docs(collectionname="ai_dags", data={"appid": app_id})

        app_metrics = {
            'teaching': ds_count + dm_count + dc_count + di_dags_count + de_count,
            'learning': nb_dw_count + nb_fb_count + nb_mt_count + nb_sd_count,
            'thinking': ai_dags_count
        }

        return to_json(app_metrics), HTTP_200_OK

