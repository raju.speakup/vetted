# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final, get_url_args, get_sorting_list
from commons.json_utils import to_json, to_list_json
from commons.decorators import validate_json, validate_schema, validate_user, compare_user_role
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED)
from logger.logger import logger
import datetime
import json
import uuid
from flask import request
# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError


class ClientService:
    """User Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def bulk_assign_users(self, client_id, users):
        results = []
        for user in users:
            try:
                if user['clientRole'] == 'guest':
                    return to_json({"message": "guest is not a valid role"}, is_error=True)
                return_object = self.connection.find_one(
                    collectionname="users",
                    data={
                            "user_id": user['user_id'],
                            "isActive": True
                        }
                )
                if not return_object:
                    return to_json({"message": "user does not exist"}, is_error=True)
                role = return_object['userRole']
                permission = {
                    'clientid': client_id,
                    'user_id': user['user_id']
                }
                modules_list = self.connection.find_selected_field(collectionname="modules",
                                                                   data={},
                                                                   projection={"module_id": 1})
                change_object = self.connection.find_one(
                    collectionname="users",
                    data={"$and": [
                        {
                            "user_id": user['user_id']
                        },
                        {
                            "clients": {
                                "$elemMatch": {
                                    "clientid": client_id,
                                    "clientRole" : {"$ne": user['clientRole']}
                                }
                            }
                        }]}
                )
                if change_object:
                    self.connection.update_one(
                        collectionname="users",
                        data=(
                            {
                                "user_id": user['user_id']
                            },
                            {
                                "$pull": {
                                    "clients": {
                                        "clientid": client_id
                                    }
                                }
                            }))
                    return_object = self.connection.delete_many(
                        collectionname="permissions",
                        data={
                            "clientid": client_id,
                            "user_id": user['user_id']
                        }
                    )

                return_object = self.connection.find_one(
                    collectionname="users",
                    data={"$and": [
                        {
                            "user_id": user['user_id']
                        },
                        {
                            "clients": {
                                "$elemMatch": {
                                    "clientid": client_id
                                }
                            }
                        }]}
                )
                if not return_object:
                    self.connection.update_one(
                        collectionname="users",
                        data=({
                                  "user_id": user['user_id']
                              },
                              {
                                  "$push": {
                                      "clients": {
                                          "clientid": client_id,
                                          "clientRole": user['clientRole'],
                                          "isActive": True
                                      }
                                  }
                              }

                        ))
                    if role == 'guest':
                        return_object = self.connection.delete_many(
                            collectionname="permissions",
                            data={
                                "user_id": user['user_id']
                            }
                        )
                    self.connection.update_one(
                        collectionname="users",
                        data=({"user_id": user['user_id']}, {
                            "$set": {"userRole": user['clientRole']}}))

                    if user['clientRole'] == 'manager':
                        permission.update({
                            "entityId": "*",
                            "entityType": "module",
                            "permission": "*",
                            "isActive": True
                        })
                        self.connection.insert_one(
                            collectionname="permissions",
                            data=json.dumps(permission))

                        app_permission ={
                            'clientid': client_id,
                            'user_id': user['user_id'],
                            "entityId": "*",
                            "entityType": "app",
                            "permission": "*",
                            "isActive": True
                        }
                        self.connection.insert_one(
                            collectionname="permissions",
                            data=json.dumps(app_permission))
                        logger.info(permission)
                    # elif user['clientRole'] == 'user':
                    #     for module in modules_list:
                    #         permission.update({
                    #             "entityId": module['module_id'],
                    #             "entityType": "module",
                    #             "permission": "*",
                    #             "isActive": True
                    #         })
                    #         self.connection.insert_one(
                    #             collectionname="permissions",
                    #             data=json.dumps(permission))
                    #         logger.info(permission)
                    results.append(user['user_id'])
                    logger.info(user['user_id'])
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return results

    def bulk_client_delete(self, client_ids):
        results = []
        for client_id in client_ids:
            try:
                result = self.connection.update_one(
                    collectionname="clients",
                    data=({
                              "clientid": client_id

                          }, {
                              "$set": {"isActive": False, "updated_at": str(datetime.datetime.now())}
                          }))
                results.append(client_id)
                logger.debug(client_id)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return to_json(results)

    def bulk_assign_cartridges(self, client_id, cartridges):
        results = []
        for cartridge in cartridges:
            try:
                return_object = self.connection.find_one(
                    collectionname="clients",
                    data={"$and": [
                        {
                            "clientid": client_id
                        },
                        {
                            "cartridges": {
                                "$elemMatch": {
                                    "cartridge_id": cartridge['cartridge_id']
                                }
                            }
                        }]}
                )
                if not return_object:
                    self.connection.update_one(
                        collectionname="clients",
                        data=({
                                  "clientid": client_id
                              },
                              {
                                  "$push": {
                                      "cartridges": {
                                          "cartridge_id": cartridge['cartridge_id'],
                                          "cartridgeName": cartridge['cartridgeName'],
                                          "isActive": True
                                      }
                                  }
                              }

                        ))
                    results.append(cartridge['cartridge_id'])
                    logger.info(cartridge['cartridge_id'])
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return to_json(results)

    def bulk_delete_cartridges(self, client_id, cartridges):
        results = []
        for cartridge in cartridges:
            try:
                self.connection.update_one(
                    collectionname="clients",
                    data=(
                        {
                            "clientid": client_id
                        },
                        {
                            "$pull": {
                                "cartridges": {
                                    "cartridge_id": cartridge
                                }
                            }
                        }))
                results.append(cartridge)
                logger.info(cartridge)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return to_json(results)

    def bulk_delete_users(self, client_id, users):
        results = []
        for user in users:
            try:
                result = self.connection.update_one(
                    collectionname="users",
                    data=(
                        {
                            "user_id": user
                        },
                        {
                            "$pull": {
                                "clients": {
                                    "clientid": client_id
                                }
                            }
                        }))
                return_object = self.connection.delete_many(
                    collectionname="permissions",
                    data={
                        "clientid": client_id,
                        "user_id": user
                    }
                )
                results.append(user)
                logger.debug(user)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return results

    def bulk_delete_apps(self, client_id, user_apps):
        results = []
        for user in user_apps:
            try:
                user_id = user['user_id']
                apps = user['apps']
                modified_apps = {"user_id": user_id, "apps": []}
                for app in apps:
                    return_object = self.connection.delete_many(
                        collectionname="permissions",
                        data={
                            "clientid": client_id,
                            "user_id": user_id,
                            "entityId": app
                        }
                    )
                    modified_apps["apps"].append(app)
                results.append(modified_apps)
                logger.debug(user)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return results

    def bulk_delete_modules(self, client_id, user_modules):
        results = []
        for user in user_modules:
            try:
                user_id = user['user_id']
                modules = user['modules']
                modified_modules = {"user_id": user_id, "modules": []}
                for module in modules:
                    return_object = self.connection.delete_many(
                        collectionname="permissions",
                        data={
                            "clientid": client_id,
                            "user_id": user_id,
                            "entityId": module,
                            "entityType": "module"
                        }
                    )
                    all_apps, no_of_apps = self.connection.find_all_projected(
                        collectionname="apps",
                        data={"module_id": module, "client_id": client_id},
                        projection={"appid": 1, "name": 1})
                    for app in all_apps:
                        return_object = self.connection.delete_many(
                            collectionname="permissions",
                            data={
                                "clientid": client_id,
                                "user_id": user_id,
                                "entityId": app['appid'],
                                "entityType": "app"
                            }
                        )
                    modified_modules["modules"].append(module)
                results.append(modified_modules)
                logger.debug(user)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return results

    def bulk_assign_apps(self, client_id, user_apps):
        results = []
        for user in user_apps:
            try:
                user_id = user['user_id']
                apps = user['apps']
                modified_apps = {"user_id": user_id, "apps": []}
                for app in apps:
                    return_object = self.connection.find_one(
                        collectionname="permissions",
                        data={
                                "user_id": user_id,
                                "clientid": client_id,
                                "entityId": app
                            }
                    )
                    if not return_object:
                        permission = {
                            'clientid': client_id,
                            'user_id': user_id,
                            "entityId": app,
                            "entityType": "app",
                            "permission": "*",
                            "isActive": True}
                        return_object = self.connection.insert_one(
                            collectionname="permissions",
                            data=json.dumps(permission)

                        )
                        modified_apps["apps"].append(app)
                results.append(modified_apps)
                logger.debug(user)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return results

    def bulk_assign_modules(self, client_id, user_modules):
        results = []
        for user in user_modules:
            try:
                user_id = user['user_id']
                modules = user['modules']
                modified_modules = {"user_id": user_id, "modules": []}
                for module in modules:
                    return_object = self.connection.find_one(
                        collectionname="permissions",
                        data={
                                "user_id": user_id,
                                "clientid": client_id,
                                "entityId": module,
                                "entityType": "module"
                            }
                    )
                    if not return_object:
                        permission = {
                            'clientid': client_id,
                            'user_id': user_id,
                            "entityId": module,
                            "entityType": "module",
                            "permission": "*",
                            "isActive": True}
                        return_object = self.connection.insert_one(
                            collectionname="permissions",
                            data=json.dumps(permission)

                        )
                        all_apps, no_of_apps = self.connection.find_all_projected(
                            collectionname="apps",
                            data={"module_id": module, "client_id": client_id},
                            projection={"appid": 1, "name": 1})
                        for app in all_apps:
                            app_permission = {
                                'clientid': client_id,
                                'user_id': user_id,
                                "entityId": app['appid'],
                                "entityType": "app",
                                "permission": "*",
                                "isActive": True}
                            return_object = self.connection.insert_one(
                                collectionname="permissions",
                                data=json.dumps(app_permission))

                        modified_modules["modules"].append(module)
                results.append(modified_modules)
                logger.debug(user)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        return results

    def bulk_assign_connections(self, client_id, connections):
        results = {"successful": [], "failed": []}
        for connection in connections:
            try:
                change_object = self.connection.find_one(
                    collectionname="clients",
                    data=
                        {
                            "connections": {
                                "$elemMatch": {
                                    "conn_id": connection['conn_id'],
                                    "source_type": {"$ne": connection['source_type']}
                                }
                            }
                        }
                )
                if change_object:
                    self.connection.update_one(
                        collectionname="clients",
                        data=(
                            {
                                "clientid": client_id
                            },
                            {
                                "$pull": {
                                    "connections": {
                                        "conn_id": connection['conn_id']
                                    }
                                }
                            }))

                return_object = self.connection.find_one(
                    collectionname="clients",
                    data=
                    {
                        "connections": {
                            "$elemMatch": {
                                "conn_id": connection['conn_id']
                            }
                        }
                    }
                )
                if return_object:
                    continue

                if not return_object:
                    self.connection.update_one(
                        collectionname="clients",
                        data=({
                                  "clientid": client_id
                              },
                              {
                                  "$push": {
                                      "connections": {
                                          "conn_id": connection['conn_id'],
                                          "connectionName": connection['connectionName'],
                                          "source_type": connection['source_type'],
                                          "isActive": True
                                      }
                                  }
                              }

                        ))
                    self.connection.update_one(
                        collectionname="di_connections",
                        data=({"conn_id": connection['conn_id']}, {
                            "$set": {"assigned": True}}))
                    results['successful'].append(connection['conn_id'])
                    logger.info(connection['conn_id'])
                else:
                    self.connection.update_one(
                        collectionname="clients",
                        data=({
                                "clientid": client_id,
                                "connections": {
                                      "$elemMatch": {
                                          "conn_id": connection['conn_id']
                                      }
                                  }

                              },
                              {
                                  "$set": {
                                          "connections.$.conn_id": connection['conn_id'],
                                          "connections.$.connectionName": connection['connectionName'],
                                          "connections.$.source_type": connection['source_type'],
                                          "isActive": True
                                      }
                                  }
                            ))
                    results['successful'].append(connection['conn_id'])
                    logger.info(connection['conn_id'])
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        failed = [con['conn_id'] for con in connections if con['conn_id'] not in results['successful']]
        results['failed'] = failed
        return to_json(results)

    def bulk_delete_connections(self, client_id, connections):
        results = {"successful": [], "failed": []}
        for connection in connections:
            try:
                return_object = self.connection.find_one(
                    collectionname="di_datamarts",
                    data={"conn_id": connection}
                )
                if return_object:
                    continue
                return_object = self.connection.find_one(
                    collectionname="di_datastores",
                    data={"conn_id": connection}
                )
                if return_object:
                    continue
                return_object = self.connection.find_one(
                    collectionname="di_data_cartridges",
                    data={"conn_id": connection}
                )
                if return_object:
                    continue

                self.connection.update_one(
                    collectionname="clients",
                    data=(
                        {
                            "clientid": client_id
                        },
                        {
                            "$pull": {
                                "connections": {
                                    "conn_id": connection
                                }
                            }
                        }))
                self.connection.update_one(
                    collectionname="di_connections",
                    data=({"conn_id": connection}, {
                        "$set": {"assigned": False}}))
                results['successful'].append(connection)
                logger.info(connection)
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        failed = [con for con in connections if con not in results['successful']]
        results['failed'] = failed
        return to_json(results)

    def get_client_connections(self, client_id, source_type):
        output = None

        result = self.connection.find_selected_field("clients",
                                    {
                                        "clientid": client_id
                                    },
                                    {
                                        "clientid": 1, 'connections.conn_id': 1, 'connections.connectionName': 1,
                                        'connections.source_type': 1
                                    })
        if not result:
            return to_json({"message": "No connection assigned to {}".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND

        if 'connections' in result[0].keys():
            if source_type:
                conns = [con for con in result[0]['connections'] if con['source_type'] == source_type]

                output = {
                    'clientid': client_id,
                    'connections': conns
                }
            else:
                conns = [con for con in result[0]['connections']]
                output = {
                    'clientid': client_id,
                    'connections': conns
                }
        if output:
            return to_list_json(output, list_count=len(conns)), HTTP_200_OK
        else:
            return to_json({"message": "No connection assigned to {}".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND

    def get_client_cartridges(self, client_id):

        result = self.connection.find_selected_field("clients",
                                                     {
                                                         "clientid": client_id
                                                     },
                                                     {
                                                         "clientid": 1, 'cartridges.cartridge_id': 1,
                                                         'cartridges.cartridgeName': 1
                                                     })
        if not result:
            return to_json({"message": "No cartridges assigned to {}".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND

        if 'cartridges' in result[0].keys():
                carts = [cart for cart in result[0]['cartridges']]
                output = {
                    'clientid': client_id,
                    'cartridges': carts
                }
        if output:
            return to_list_json(output, list_count=len(carts)), HTTP_200_OK
        else:
            return to_json({"message": "No cartridge assigned to {}".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND

    def update_client(self, client_id, data):
        try:
            return_object = self.connection.find_one(
                collectionname="clients",
                data={"clientid": client_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return to_json({"message": "{0} client do not exists".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND
        else:
            data.update({"updated_at": str(datetime.datetime.now())})
            data.update({"updated_by": request.headers.get("userName", "unknown")})
            self.connection.update_one(
                collectionname="clients",
                data=({"clientid": client_id}, {
                    "$set": data}))
        return to_json(data), HTTP_200_OK

    def delete_client(self, client_id):
        try:
            return_object = self.connection.find_one(
                collectionname="clients",
                data={"clientid": client_id})
            logger.info(client_id)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "{0} client do not exists".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clients",
                data=({"clientid": client_id}, {
                    "$set": {"isActive": False}}))

        return to_json({"message": "Soft Delete Applied"}), HTTP_200_OK

    def get_client(self, client_id):
        try:
            return_object = self.connection.find_one(
                collectionname="clients",
                data={"clientid": client_id})
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "{0} client do not exists".format(client_id)}, is_error=True), HTTP_404_NOT_FOUND
        if return_object:
            return_object.pop("_id")
        return to_json(return_object), HTTP_200_OK

    def create_client(self, data):
        return_object = self.connection.find_one("clients", {
            "clientName": data['clientName']
        })
        if return_object:
            return to_json({"message": "client with provided name already exist"}, is_error=True)
        try:
            modules_list = self.connection.find_selected_field(collectionname="modules",
                                                            data={},
                                                            projection={"module_id": 1, "name": 1, "type": 1})
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if len(modules_list) >= 10:
            data.update(
                {"clientid": str(uuid.uuid4()),
                 "isActive": True,
                 "modules": modules_list,
                 "created_at": str(datetime.datetime.now()),
                 "updated_at": str(datetime.datetime.now())})
            logger.info(data)
            try:
                self.connection.insert_one(collectionname="clients",
                                           data=json.dumps(data))
            except PyMongoError as err:
                logger.error(err)
                return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
            return to_json(data), HTTP_201_CREATED
        else:
            return to_json({"message": "All 10 modules not found"}, is_error=True), HTTP_404_NOT_FOUND

    def get_client_metrics(self, request):
        client_metrics = []
        limit, offset, gen_sort_key, projection = get_url_args(request)
        sort_dict = get_sorting_list(gen_sort_key)
        try:
            keyword = str(request.args.get('keyword',''))
        except ValueError:
            raise
        try:
            clients_list, count = self.connection.find_selected_field_all(collectionname="clients",
                                                                   data={"isActive": True,
                                                                         "clientName": {'$regex': keyword,
                                                                                        "$options": 'i'}},
                                                                   projection={"clientid": 1}, limit=limit,
                                                                   skip=offset,
                                                                   sortdata=sort_dict)
            for client in clients_list:
                client_id = client['clientid']
                data = {}
                manager_count = self.connection.count_docs(
                    collectionname="users",
                    data=
                    {
                        "clients":
                            {
                                "$elemMatch":
                                    {
                                        "clientid": client_id,
                                        "isActive": True,
                                        "clientRole": 'manager'
                                    }
                            }
                    }

                )

                user_count = self.connection.count_docs(
                    collectionname="users",
                    data=
                    {
                        "clients":
                            {
                                "$elemMatch":
                                    {
                                        "clientid": client_id,
                                        "isActive": True,
                                        "clientRole": 'user'
                                    }
                            }
                    }

                )

                client_details = self.connection.find_selected_field(
                    collectionname="clients",
                    data={
                        "clientid": client_id,
                    },
                    projection={
                        'description': 1,
                        'updated_at': 1,
                        'clientName': 1
                    }
                )
                if client_details:
                    if 'description' in client_details[0].keys():
                        description = client_details[0]['description']
                    else:
                        description = ''
                    if 'updated_at' in client_details[0].keys():
                        updated_at = client_details[0]['updated_at']
                    else:
                        updated_at = ''
                    if 'clientName' in client_details[0].keys():
                        client_name = client_details[0]['clientName']
                    else:
                        client_name = ''
                else:
                    description = ''
                    updated_at = ''
                    client_name = ''
                total_users = manager_count + user_count

                data.update({
                    'client_name': client_name,
                    'clientid': client_id,
                    'description': description,
                    'total_users': total_users,
                    'managers': manager_count,
                    'users': user_count,
                    'updated_at': updated_at
                })
                client_metrics.append(data)
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(client_metrics, list_count=count), HTTP_200_OK

    def get_client_dashboard(self, client_id):
        client_metrics = {}
        users, count = self.connection.find_all_projected(
            collectionname="users",
            data=
            {
                "clients":
                    {
                        "$elemMatch":
                            {
                                "clientid": client_id,
                                "isActive": True,
                            }
                    }
            },
            projection={"firstName": 1, "lastName": 1, "userRole": 1, "email": 1, "user_id": 1}

        )
        try:
            modules_list = self.connection.find_selected_field(collectionname="modules",
                                                               data={},
                                                               projection={"module_id": 1, "name": 1})
            modules = []
            for module in modules_list:
                data = {}
                apps_count = self.connection.count_docs(
                    collectionname="apps",
                    data=
                    {
                        "client_id": client_id,
                        "isActive": True,
                        "module_id": module['module_id']
                    }

                )

                data.update({
                    'module_id': module['module_id'],
                    'name': module['name'],
                    'total_apps': apps_count
                })
                modules.append(data)
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        user_id = request.headers.get('userid')

        last_login = self.connection.find_latest_selected_field(
            collectionname="auditLogs",
            data={
                "user_id": user_id,
                "action": 'login'
            },
            projection={
                'time': 1
            }, sort='time', limit=1)

        print(last_login)

        if last_login:
            login_time = last_login[0]['time']
        else:
            login_time = ''

        client_details = self.connection.find_selected_field(
            collectionname="clients",
            data={
                "clientid": client_id,
            },
            projection={
                'updated_at': 1,
                'clientName': 1,
                'stakeholders': 1,
                'address': 1,
                'phone': 1
            }
        )
        client_metrics.update(client_details[0])
        client_metrics.update({
                               "users": users,
                               "modules": modules,
                               "last_login": login_time
                              })

        return to_list_json(client_metrics, list_count=len(modules_list)), HTTP_200_OK

