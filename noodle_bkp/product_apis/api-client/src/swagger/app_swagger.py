#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateApp:
    """Create App fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'module_id': fields.String,
        'client_id': fields.String
    }


@swagger.model
class UpdateApp:
    """Update App fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String
    }
