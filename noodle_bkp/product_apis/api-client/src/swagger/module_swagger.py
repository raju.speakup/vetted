#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateModule:
    """Create App fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'type': fields.String,
    }


@swagger.model
class UpdateModule:
    """Update App fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'type': fields.String,
    }
