#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class ConnectionAssign:
    """bulk user assign fields"""
    resource_fields = {
        'conn_id': fields.String,
        'connectionName': fields.String,
        'source_type': fields.String
    }

@swagger.model
@swagger.nested(
    cartridges=ConnectionAssign.__name__
)
class BulkConnectionAssign:
    """Bulk Connection assign field"""
    resource_fields = {
        'connections': fields.List(fields.Nested(ConnectionAssign.resource_fields))
    }


@swagger.model
class BulkConnectionDelete:
    """Bulk Connection delete field"""
    resource_fields = {
        'ids': fields.List
    }