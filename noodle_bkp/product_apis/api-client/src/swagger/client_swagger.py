#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateClient:
    """CreateClient fields."""

    resource_fields = {
        'clientName': fields.String,
        'repository': fields.String,
        'description': fields.String,
        # 'contactName': 'fields.String',
        # 'contactEmail': 'fields.String',
        # 'address1': 'fields.String',
        # 'address2': 'fields.String',
        # 'city': 'fields.String',
        # 'stateCode': 'fields.String',
        # "zip": 'fields.String',
        # 'preferredDateFormat': 'fields.String',
        # 'timeZone': 'fields.String',
        # 'onBoardedBy': 'fields.String',
    }


@swagger.model
class UpdateClient:
    """UpdateClient fields."""

    resource_fields = {
        'clientName': fields.String,
        'repository': fields.String,
        'description': fields.String,
        'isActive': fields.Boolean
        # 'clientShortName': 'fields.String',
        # 'contactName': 'fields.String',
        # 'contactEmail': 'fields.String',
        # 'address1': 'fields.String',
        # 'address2': 'fields.String',
        # 'city': 'fields.String',
        # 'stateCode': 'fields.String',
        # "zip": 'fields.String',
        # 'preferredDateFormat': 'fields.String',
        # 'timeZone': 'fields.String',
        # 'onBoardedBy': 'fields.String'
    }


@swagger.model
class BulkClient:
    resource_fields = {
        'ids': fields.List
    }
