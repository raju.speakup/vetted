#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class UserAssign:
    """bulk user assign fields"""
    resource_fields = {
        'user_id': fields.String,
        'clientRole': fields.String
    }
    swagger_metadata = {
        "clientRole": {
            "enum": ['manager', 'user']
        }
    }

@swagger.model
@swagger.nested(
    users=UserAssign.__name__
)
class BulkUserAssign:
    """Bulk user assign field"""
    resource_fields = {
        'users': fields.List(fields.Nested(UserAssign.resource_fields))
    }

@swagger.model
class BulkUserDelete:
    resource_fields = {
        'ids': fields.List
    }


@swagger.model
class ModuleAssign:
    """bulk user assign fields"""
    resource_fields = {
        'user_id': fields.String,
        'modules': fields.List
    }

@swagger.model
class ModuleUnassign:
    """bulk user assign fields"""
    resource_fields = {
        'user_id': fields.String,
        'modules': fields.List
    }

@swagger.model
@swagger.nested(
    assigned_users=UserAssign.__name__ ,
    assigned_apps=ModuleAssign.__name__ ,
    unassigned_apps=ModuleUnassign.__name__

)
class BulkUser:
    """Bulk user operation field"""
    resource_fields = {
        'assigned_users': fields.List(fields.Nested(UserAssign.resource_fields)),
        'unassigned_users': fields.List,
        'assigned_modules': fields.List(fields.Nested(ModuleAssign.resource_fields)),
        'unassigned_modules': fields.List(fields.Nested(ModuleUnassign.resource_fields))
    }
