#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class CartridgeAssign:
    """bulk user assign fields"""
    resource_fields = {
        'cartridge_id': fields.String,
        'cartridgeName': fields.String
    }

@swagger.model
@swagger.nested(
    cartridges=CartridgeAssign.__name__
)
class BulkCartridgeAssign:
    """Bulk cartridge assign field"""
    resource_fields = {
        'cartridges': fields.List(fields.Nested(CartridgeAssign.resource_fields))
    }


@swagger.model
class BulkCartridgeDelete:
    """Bulk cartridge delete field"""
    resource_fields = {
        'ids': fields.List
    }