from os import getenv
import requests
import json
import os

def version():
    return "1.0.0"

entity_id = None


def init(entity_id):
    entity_id = entity_id


def set_iparam(field_name, field_value):
    try:
        if "IS_NB_EXECUTOR" in os.environ:
            if os.environ["IS_NB_EXECUTOR"] == 'TRUE':
                print("In notebook executor mode so not setting variable")
                return None

        url = "http://192.168.2.142:8817/nb-runs/params/"+entity_id
        payload = "{\n\t\"input_params\": {\""+field_name+"\":\""+field_value+"\"}\n}"
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
            'postman-token': "2bfd15a9-79ad-2421-cf6b-8710fc125072"
        }

        response = requests.request("PUT", url, data=payload, headers=headers)
    except Exception as e:
        return e


def set_oparam(field_name, field_value):
    try:
        if "IS_NB_EXECUTOR" in os.environ:
            if os.environ["IS_NB_EXECUTOR"] == 'TRUE':
                print("In notebook executor mode so not setting variable")
                return None

        url = "http://192.168.2.142:8817/nb-runs/params/"+entity_id
        payload = "{\n\t\"output_params\": {\""+field_name+"\":\""+field_value+"\"}\n}"
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
            'postman-token': "2bfd15a9-79ad-2421-cf6b-8710fc125072"
        }

        response = requests.request("PUT", url, data=payload, headers=headers)
    except Exception as e:
        return e


def get_iparam(field_name):
    url = "http://192.168.2.142:8817/nb-runs/params/"+entity_id

    headers = {
        'cache-control': "no-cache",
        'postman-token': "f1d16276-d986-55bb-5b80-b89891ddbd6f"
    }

    response = requests.request("GET", url, headers=headers)
    json_obj = json.loads(response.text)
    return json_obj['data']['input_params'][field_name]


def get_oparam(field_name):
    url = "http://192.168.2.142:8817/nb-runs/params/"+entity_id

    headers = {
        'cache-control': "no-cache",
        'postman-token': "f1d16276-d986-55bb-5b80-b89891ddbd6f"
    }

    response = requests.request("GET", url, headers=headers)
    json_obj = json.loads(response.text)
    return json_obj['data']['output_params'][field_name]