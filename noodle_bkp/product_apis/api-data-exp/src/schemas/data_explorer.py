from src.utils.json_validator import Schema, Prop


def datamart_schema():
    metrics_prop = {
        "rows": Prop().number().max(255).min(1).build(),
        "datasets": Prop().string().max(255).min(1).build(),
        "size": Prop().number().max(255).min(1).build(),
    }
    items = {"type": "object", "properties": metrics_prop}

    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "conn_id": Prop().string().max(255).min(1).build(),
        "source_type": Prop().string().max(255).min(1).build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build(),
        "metrics": Prop().object().properties(items).build()
    }

    return Schema().keys(prop).required(["name", "source_type", "conn_id"]).build()


def dataexplorer_schema():
    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "datamart_id": Prop().string().max(255).min(0).build(),
        "datamart_name": Prop().string().max(255).min(0).build()
    }

    return Schema().keys(prop).required(["name", "datamart_id", "datamart_name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()


def fetch_data_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "datalimit": Prop().string().max(5).min(1).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "ids": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["ids"]).build()


def id_stats_schema():
    prop = {
        "datamart_id": Prop().string().max(100).min(1).build(),
        "source_type": Prop().string().max(100).min(1).build(),
    }

    return Schema().keys(prop).required(["source_id", "source_type"]).build()


def create_table_schema():
    column_prop = {
        "column_name": Prop().string().max(255).min(1).build(),
        "data_type": Prop().string().max(255).min(1).enum(["INT", "data_cartridge", "join",
                                                      "filter", "data_mart", "update"]).build(),
        "allow_null": Prop().array().max(255).min(1).enum('ALLOW NULL', 'NOT NULL').build()
    }

    column_schema = {"type": "object", "properties": column_prop}
    prop = {
        "table_name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "columns": Prop().array().items(column_schema).build(),
        "primary_keys": Prop().array().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build()
    }