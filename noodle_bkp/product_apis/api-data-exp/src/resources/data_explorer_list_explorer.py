import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
from src.utils.json_validator import validate_schema

from src.schemas.data_explorer import (
    list_arg_schema,
    dataexplorer_schema,
    id_path_schema
)

from src.services.http_code import (
    HTTP_status_200,
    HTTP_status_201,
    HTTP_status_404
)

from bat_auth_sdk import role_validator
from src.services.data_explorer import DataExplorerService
from src.services.di_datamart import DataIngestionDataMartService
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.data_explorer_swagger import DataExplorerPostSwaggerModel, DataExplorerDeleteSwaggerModel

class DataExplorerListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataExplorerService(db_name)
        self.datasetservice = DataIngestionDatasetService(db_name)
        self.datamartservice = DataIngestionDataMartService(db_name)

    @swagger.operation(
        nickname='Get DataExplorer',
        notes='Get DataExplorer list',
        parameters=[
            {
                "name": "limit",
                "dataType": "int",
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": "int",
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": "string",
                "paramType": "query",
                "description": "Takes a keyword to search"
            }]
    )
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        """ Get Data Explorer List """
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        output = self.service.list_data_explorer(limit, offset, keyword)
        response_code = HTTP_status_200
        return output, response_code

    @swagger.operation(
        nickname="Create DataExplorer",
        notes="Create DataExplorer",
        parameters=[
            {
                "name":"parameters",
                "dataType": DataExplorerPostSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ]
    )
    @validate_schema(dataexplorer_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data

        datamart_id = data['datamart_id']

        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_at'] = str(datetime.datetime.utcnow())
        new_doc['created_by'] = request.headers.get("userName", "unknown")

        datamart = self.datamartservice.get_datamart(datamart_id)['data']
        datasets = self.datasetservice.list_datasets(datamart_id, "data_mart", 10000, 0)
        count_datasets = datasets['meta']['total_count']
        new_doc['metrics'] = {
            "no_of_charts": 0,
            "advanceEDA": 0,
            "no_of_datasets": count_datasets
        }
        new_doc['datasets'] = datamart['datasets']

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)
        output = self.service.create_data_explorer(new_doc)
        response_code = HTTP_status_201

        return output, response_code

    @swagger.operation(
        nickname="Delete DataExplorer",
        notes="Bulk Delete by ids",
        parameters=[
            {
                "name":"parameters",
                "dataType": DataExplorerDeleteSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ]
    )
    #@validate_schema(id_path_schema(), is_path=True)
    def delete(self):
        "Bulk Delete Data Explorer"
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        try:
            datastores = self.service.bulk_delete_data_explorer(ids)
            response_code = HTTP_status_200
        except:
            datastores = ''
            response_code = HTTP_status_404

        return datastores, response_code
