import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json

from src.schemas.data_explorer import fetch_data_schema
from src.services.http_code import HTTP_status_200

from bat_auth_sdk import role_validator
from src.services.di_datasets import DataIngestionDatasetService
from src.services.de_charts import DataExplorerChartService
from src.utils.config import conf
from flask_restful_swagger import swagger

class DataExplorerDatasetFetchData(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        self.chart_service = DataExplorerChartService(db_name)

    @swagger.operation(
        nickname="Get Dataset",
        notes="Get Dataset",
        parameters=[
            {
                "name":"source_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Source ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            }
        ]
    )
    @validate_schema(fetch_data_schema(), is_arg=True)
    def get(self, source_id, dataset_id):
        datalimit = request.args.get("datalimit", 500)

        output = self.service.fetch_data(source_id,'data_mart',dataset_id, datalimit)
        response_code = HTTP_status_200

        return output, response_code
