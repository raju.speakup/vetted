import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json

from src.schemas.data_explorer import list_arg_schema
from src.services.http_code import (
    HTTP_status_200,
    HTTP_status_404
)
from bat_auth_sdk import role_validator
from src.services.di_datasets import DataIngestionDatasetService
from src.services.de_charts import DataExplorerChartService
from src.utils.config import conf
from flask_restful_swagger import swagger

class DataExplorerDatasetFetchCharts(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        self.chart_service = DataExplorerChartService(db_name)

    @swagger.operation(
        nickname="Fetch charts",
        notes="Dataset Fetch charts",
        parameters=[
            {
                "name": "explorer_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Explorer ID"
            },
            {
                "name": "source_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Source ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ]
    )
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self, explorer_id, source_id, dataset_id):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")

        output = self.chart_service.list_charts(explorer_id, source_id, dataset_id, limit, offset, keyword)
        response_code = HTTP_status_200

        return output, response_code
