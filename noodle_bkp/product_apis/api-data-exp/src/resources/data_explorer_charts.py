import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
#from src.utils.json_validator import validate_schema

#from src.schemas.di_datamart import list_arg_schema, datamart_schema
from src.services.http_code import (
    HTTP_status_200,
    HTTP_status_201,
    HTTP_status_500
)

from bat_auth_sdk import role_validator
from src.services.de_charts import DataExplorerChartService
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.config import conf
from flask_restful_swagger import  swagger
from swagger.data_explorer_charts_swagger import Charts, UpdateCharts
from swagger.data_explorer_swagger import DataExplorerDeleteSwaggerModel

class DataExplorerChartResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.chart_service = DataExplorerChartService(db_name)
        self.dataset_service = DataIngestionDatasetService(db_name)

    @swagger.operation(
        nickname="Create chart",
        notes="Create Data Explorer Chart",
        parameters=[
            {
                "name":"parameter",
                "dataType": Charts.__name__,
                "required": True,
                "paramType": "body",
                "description": ""
            }
        ]
    )
    #@validate_schema(datamart_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        source_id = data['source_id']
        dataset_id = data['dataset_id']
        chart_type = data["chartType"]

        new_doc = data

        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_at'] = str(datetime.datetime.utcnow())
        new_doc['created_by'] = request.headers.get("userName", "unknown")

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)

        if 'datalimit' in data:
            datalimit = data['datalimit']
        else:
            datalimit = 500

        metrics = data["metrics"]

        chartdata ={}
        if chart_type == "barchart":
            chartdata = self.dataset_service.fetch_bar_chart_data(source_id, 'data_mart', dataset_id, metrics, datalimit)
        elif chart_type == "linechart":
            chartdata = self.dataset_service.fetch_line_chart_data(source_id, 'datamart', dataset_id,metrics, datalimit, is_scatter= False)
        elif chart_type == "scatterchart":
            chartdata = self.dataset_service.fetch_line_chart_data(source_id, 'datamart', dataset_id,metrics, datalimit, is_scatter= True)

        output = self.chart_service.create_chart(new_doc)
        output['chartdata'] = chartdata
        response_code = HTTP_status_201

        return output, response_code

    @swagger.operation(
        nickname="Create chart",
        notes="Create Data Explorer Chart",
        parameters=[
            {
                "name": "parameter",
                "dataType": UpdateCharts.__name__,
                "required": True,
                "paramType": "body",
                "description": ""
            }
        ]
    )
    def put(self):
        data = json.loads(request.data.decode('utf-8'))

        source_id = data['source_id']
        dataset_id = data['dataset_id']
        chart_type = data["chartType"]

        new_doc = data

        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_by'] = request.headers.get("userName", "unknown")

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)

        if 'datalimit' in data:
            datalimit = data['datalimit']
        else:
            datalimit = 500

        metrics = data["metrics"]

        chartdata = {}
        if chart_type == "barchart":
            chartdata = self.dataset_service.fetch_bar_chart_data(source_id, 'datamart', dataset_id, metrics, datalimit)
        elif chart_type == "linechart":
            chartdata = self.dataset_service.fetch_line_chart_data(source_id, 'datamart', dataset_id,metrics, datalimit, is_scatter= False)
        elif chart_type == "scatterchart":
            chartdata = self.dataset_service.fetch_line_chart_data(source_id, 'datamart', dataset_id,metrics, datalimit, is_scatter= True)

        output = self.chart_service.update_chart(new_doc)
        output['chartdata'] = chartdata
        response_code = HTTP_status_200
        return output, response_code

    @swagger.operation(
        nickname="Delete DataExplorer",
        notes="Bulk Delete by ids",
        parameters=[
            {
                "name": "parameters",
                "dataType": DataExplorerDeleteSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ]
    )
    #@validate_schema(id_path_schema(), is_path=True)
    def delete(self):
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        deleted = self.chart_service.bulk_delete_charts(ids)
        response_code = HTTP_status_200
        return deleted, response_code
