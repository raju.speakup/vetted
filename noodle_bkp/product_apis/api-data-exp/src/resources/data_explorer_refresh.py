import datetime
import uuid

from flask_restful import Resource
from src.services.http_code import HTTP_status_204
from flask import request

import json
from src.utils.json_validator import validate_schema

#from src.schemas.di_dataset import refresh_datasets_schema

from bat_auth_sdk import role_validator
from src.services.data_explorer import DataExplorerService
from src.utils.config import conf


class DataExplorerRefreshResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataExplorerService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    #@role_validator.validate_app_user()
    def get(self):
        output = self.service.refresh_dataexplorers()
        response_code = HTTP_status_204
        return output, response_code
