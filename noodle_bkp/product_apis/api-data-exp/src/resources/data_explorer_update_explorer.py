import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json

#from src.schemas.di_dataset import list_arg_schema, dataset_schema
from src.services.http_code import (
    HTTP_status_200,
    HTTP_status_404
)
from bat_auth_sdk import role_validator
from src.services.data_explorer import DataExplorerService
from src.services.di_datamart import DataIngestionDataMartService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.data_explorer_swagger import DataExplorerUpdate

class DataExplorerUpdateResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataExplorerService(db_name)
        self.datamartservice = DataIngestionDataMartService(db_name)

    @swagger.operation(
        nickname='Get DataExplorer',
        notes='Get DataExplorer list',
        parameters=[
            {
                "name": "explorer_id",
                "dataType": "string",
                "required": True,
                "paramType": "path",
                "description": "Explorer Id"
            },
            {
                "name": "parameter",
                "dataType": DataExplorerUpdate.__name__,
                "required": True,
                "paramType": "body",
                "description": ""
            }]
    )
    #@validate_schema(id_path_schema(), is_path=True)
    def put(self, explorer_id):
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data
        new_doc['updated_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_by'] = request.headers.get("userName", "unknown")

        if 'datamart_id' in data:
            datamart_id = data['datamart_id']
            datamart = self.datamartservice.get_datamart(datamart_id)['data']
            new_doc['datasets'] = datamart['datasets']
            output = self.service.update_data_explorer(explorer_id, new_doc)
            response_code = HTTP_status_200

        return output, response_code
