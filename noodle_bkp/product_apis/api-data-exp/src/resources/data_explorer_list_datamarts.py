from flask_restful import Resource
from flask import request

from src.utils.json_validator import validate_schema

from src.schemas.data_explorer import list_arg_schema
from bat_auth_sdk import role_validator

from src.services.di_datamart import DataIngestionDataMartService
from src.utils.config import conf
from src.services.http_code import HTTP_status_200
from flask_restful_swagger import swagger


class DatamartListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDataMartService(db_name)

    @swagger.operation(
        nickname="DataMart list",
        notes="Get DataMart list",
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ]
    )
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        output = self.service.list_datamart(limit, offset, keyword)
        response_code = HTTP_status_200

        return output, response_code
