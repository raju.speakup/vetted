from pymongo import MongoClient, ReturnDocument, ASCENDING
from pymongo.errors import ConnectionFailure
from src.utils.constants import Const
from src.utils.nlogger import logger
from src.utils.config import conf


class MongoDBClient:
    def __init__(self):
        self.url = conf.get_v('db.url')
        logger.log().info("connecting to " + self.url)
        self.client = MongoClient(self.url, maxPoolSize=10, minPoolSize=0,
                                  serverSelectionTimeoutMS=30000, connectTimeoutMS=20000, maxIdleTimeMS=3000,
                                  appname="config_apis")

    def find(self, db_name, collection_name, query={}, offset=0, limit=1, sort_field = "_id"):
        db = self.client[db_name]

        if db is None:
            raise ConnectionError(self.error)

        results = []
        db_results = db[collection_name].find(query).sort(sort_field, ASCENDING).skip(int(offset)).limit(int(limit))
        results_count = db_results.count()
        for result in db_results:
            results.append(result)
        self.client.close()
        return results, results_count

    def find_with_projection(self, db_name, collection_name, query={}, projection={}, offset=0, limit=1, sort_field = "_id"):
        db = self.client[db_name]

        if db is None:
            raise ConnectionError(self.error)

        results = []
        db_results = db[collection_name].find(query, projection).sort(sort_field, ASCENDING).skip(int(offset)).limit(int(limit))
        results_count = db_results.count()
        for result in db_results:
            results.append(result)
        self.client.close()
        return results, results_count

    def find_with_cursor(self, db_name, collection_name, query={}):
        db = self.client[db_name]

        if db is None:
            raise ConnectionError(self.error)

        return db[collection_name].find(query)

    def insert_one(self, db_name, collection_name, data):
        db = self.client[db_name]
        if db is None:
            raise ConnectionError(self.error)
        return db[collection_name].insert_one(data)

    def find_one_and_update(self, db_name, collection_name, query, update):
        db = self.client[db_name]
        if db is None:
            raise ConnectionError(self.error)
        return db[collection_name].find_one_and_update(query, update, return_document=ReturnDocument.AFTER)

    def find_one_and_delete(self, db_name, collection_name, query):
        db = self.client[db_name]
        if db is None:
            raise ConnectionError(self.error)
        return db[collection_name].find_one_and_delete(query, projection={'_id': False})

    def close(self):
        return self.client.close()

    def is_connected(self):
        try:
            self.client.admin.command('ismaster')
            return True
        except ConnectionFailure as e:
            return False

    def reconnect(self):
        url = conf.get_v('db.url')
        print("reconnecting to " + url)
        self.client = MongoClient(url, maxPoolSize=10, minPoolSize=0,
                                  serverSelectionTimeoutMS=30000, connectTimeoutMS=20000, maxIdleTimeMS=3000,
                                  appname="config_apis")


db_client = MongoDBClient()
