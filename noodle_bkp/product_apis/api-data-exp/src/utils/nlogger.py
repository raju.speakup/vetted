import logging


class NLogger():

    def __init__(self):
        logging.basicConfig(filename='api-data-ingestion-pipeline.log', level=logging.DEBUG)

    def log(self):
        return logging

logger = NLogger()