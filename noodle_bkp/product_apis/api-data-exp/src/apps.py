from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger

from src.resources.data_explorer_list_datamarts import DatamartListResource
from src.resources.data_explorer_list_datasets import DataExplorerDatasetListResource
from src.resources.data_explorer_list_explorer import DataExplorerListResource
from src.resources.data_explorer_get_data import DataExplorerDatasetFetchData
from src.resources.data_explorer_get_charts import DataExplorerDatasetFetchCharts
from src.resources.data_explorer_charts import DataExplorerChartResource
from src.resources.data_explorer_update_explorer import DataExplorerUpdateResource
from src.resources.data_explorer_refresh import DataExplorerRefreshResource
from flask_cors import CORS
from src.utils.constants import con
from src.utils.nlogger import logger
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask import request, g
import re


app = Flask(__name__)
CORS(app)
SECRETE_KEY = 'some-difficult-value'


@app.before_first_request
def global_init():
    logger.log().info("started initialising")

    logger.log().info("completed initialising")


@app.before_request
def validate_auth():
    if (not "/admin/_refresh" == request.path) and (not re.compile('/api/spec*').match(request.path)):
        check_auth()


#@validate_auth()
@jwt_required
def check_auth():
    print("jwt token validated successfully")
    g.client_id = request.headers.get('clientid')
    g.app_id = request.headers.get('appid')
    print("serving request for client_id {} and app_id {}".format(request.headers.get('clientid'), request.headers.get('appid')))


api = swagger.docs(
    Api(app), apiVersion='0.1',
    basePath="http://localhost:5000/api/v0/",
    api_spec_url='/api/spec',
    description="Docs for Data Explorer Layer API's",
    resourcePath="/dataExp"

)

api.add_resource(DatamartListResource, '/data-explorer/datamarts')
api.add_resource(DataExplorerDatasetListResource, '/data-explorer/<string:datamart_id>')
api.add_resource(DataExplorerListResource, '/data-explorers')
api.add_resource(DataExplorerDatasetFetchData, '/data-explorer/data/source-id/<string:source_id>/dataset-id/<string:dataset_id>')
api.add_resource(DataExplorerDatasetFetchCharts, '/data-explorer/charts/explorer-id/<string:explorer_id>/source-id/<string:source_id>/dataset-id/<string:dataset_id>')

api.add_resource(DataExplorerChartResource, '/data-explorer/charts')
api.add_resource(DataExplorerUpdateResource, '/data-explorer/explorer-id/<string:explorer_id>')

api.add_resource(DataExplorerRefreshResource, '/data-explorer/refresh')


# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['PROPAGATE_EXCEPTIONS'] = True

jwt = JWTManager(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=con.API_PORT, debug=True)