from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime
from src.utils.postgres_db import PostgresDBClient


class DataIngestionDataMartService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_datamarts'
        self.mongo_client = db_client
        self.dag_doc = None

    def create_table(self, new_table_doc):
        id = self.mongo_client.insert_one(self.db_name, self.collection_name, new_table_doc).inserted_id
        del new_table_doc['_id']
        return to_json(new_table_doc)

    def get_target_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, query]}

    def list_datamart(self, limit, offset, keyword=""):
        #self.refresh_datamarts()
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.mongo_client.find_with_projection(self.db_name, self.collection_name, query=query,
                                                               projection= {'datamart_id':1, "name":1, 'conn_id':1},
                                                               offset=offset,limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_datamart(self, datamart_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'datamart_id': datamart_id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_datamart(self, new_delta_doc, datamart_id):
        output = self.mongo_client.find_one_and_update(self.db_name, self.collection_name, {'datamart_id': datamart_id}, {'$set': new_delta_doc})
        del output['_id']
        return to_json(output)

    def delete_datamart(self, datamart_id):
        result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'datamart_id': datamart_id})
        return result

    def bulk_delete_datamart(self, ids):
        results = []
        for id in ids:
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'datamart_id': id})
            results.append(result)
        return results

    def create_datamart(self, datamart_doc):
        datamart_id = str(uuid.uuid4())
        pg_conn = self.get_target_db_connection(datamart_doc['conn_id'])

        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                      {"conn_id": datamart_doc['conn_id']})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys() :
                schema_name = 'public'
                db_name = return_object['db_name']
            else:
                schema_name = return_object['schema_name']
                db_name = return_object['db_name']

        query = "SELECT table_name FROM information_schema.tables WHERE table_schema='{}'".format(schema_name)
        cur = pg_conn.cursor
        cur.execute(query)
        rows = cur.fetchall()
        tables = []
        for row in rows:
            tables.append(row[0])

        query = "SELECT pg_size_pretty(pg_database_size('{}'))".format(db_name)
        cur.execute(query)
        db_size = cur.fetchone()
        db_size = db_size[0]

        query = "SELECT CAST(SUM(n_live_tup) as INTEGER) as total_rows FROM pg_stat_user_tables " \
                "where schemaname = '{}'".format(schema_name)
        cur.execute(query)
        no_of_rows = cur.fetchone()[0]

        datamart_doc.update(
            {
                'datamart_id': datamart_id,
                'created_at': str(datetime.datetime.now()),
                'tables': tables,
                'metrics': {
                    "no_of_rows": no_of_rows,
                    "no_of_tables": len(tables),
                    "size": db_size
                }
            })
        id = self.mongo_client.insert_one(self.db_name, self.collection_name, datamart_doc).inserted_id
        del datamart_doc['_id']
        return to_json(datamart_doc)

    def get_datamart_stats(self, datamart_id, conn_id):
        output = {}
        try:
            pg_conn = self.get_target_db_connection(conn_id)
            if not pg_conn:
                return to_json({"message": "connection could not be established"}, is_error=True)

            return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                          {"conn_id": conn_id})
            if return_object:
                if not 'schema_name' in return_object[0].keys():
                    schema_name = 'public'
                    db_name = return_object[0]['db_name']
                else:
                    schema_name = return_object[0]['schema_name']
                    db_name = return_object[0]['db_name']

            return_object, ds_count = self.mongo_client.find_with_projection(self.db_name, "di_datasets",
                                                          {"source_id": datamart_id}, {'table_name': 1})

            cur = pg_conn.cursor
            query = "SELECT table_name FROM information_schema.tables WHERE table_schema='{}'".format(schema_name)
            cur.execute(query)
            rows = cur.fetchall()
            tables = []
            for row in rows:
                tables.append(row[0])

            query = "SELECT pg_size_pretty(pg_database_size('{}'))".format(db_name)
            cur.execute(query)
            db_size = cur.fetchone()
            db_size = db_size[0]

            query = "SELECT CAST(SUM(n_live_tup) as INTEGER) as total_rows FROM pg_stat_user_tables " \
                    "where schemaname = '{}'".format(schema_name)
            cur.execute(query)
            no_of_rows = cur.fetchone()[0]

            output.update({
                "tables": tables,
                "metrics": {
                    "no_of_rows": no_of_rows,
                    "no_of_tables": ds_count,
                    "size": db_size
                }
            })

            return output
        except Exception as e:
            print(e)
            return to_json(e)

    def refresh_datamarts(self):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={}, limit=10000)
        output = []
        for doc in result:
            del doc['_id']
            datamart_id = doc['datamart_id']
            conn_id = doc['conn_id']
            metrics = self.get_datamart_stats(datamart_id, conn_id)

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                           query={'datamart_id': datamart_id},
                                                           update={'$set': metrics})

            output.append(metrics)

        return to_json(output)