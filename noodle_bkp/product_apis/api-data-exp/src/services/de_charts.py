from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.postgres_db import PostgresDBClient
from pymongo import ReturnDocument


class DataExplorerChartService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'de_charts'
        self.mongo_client = db_client
        self.dag_doc = None
        self.dataset_service = DataIngestionDatasetService(db_name)

    def create_chart(self, new_doc):
        new_doc['chart_id'] = str(uuid.uuid4())
        id = self.mongo_client.insert_one(self.db_name, self.collection_name, new_doc).inserted_id
        del new_doc['_id']
        return to_json(new_doc)

    def update_chart(self, new_doc):
        chart_id = new_doc['chart_id']
        new_doc = self.mongo_client.find_one_and_update(self.db_name, self.collection_name,
                                                        query = {"chart_id": chart_id},
                                                        update={'$set': new_doc})
        del new_doc['_id']
        return to_json(new_doc)

    def get_target_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client

    def search_query(self, keyword):
        return {"$or": [{'chartType': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query, filter_query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query, query]}

    def list_charts(self,explorer_id, source_id, dataset_id, limit, offset, keyword= ""):
        filter_query = {'explorer_id': explorer_id, 'source_id':source_id, 'dataset_id': dataset_id}
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword),filter_query)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query,
                                                               offset=offset,
                                                               limit=limit)
        output = []
        for service in result:
            del service['_id']
            source_id = service['source_id']
            dataset_id = service['dataset_id']
            metrics = service['metrics']
            if 'datalimit' in service:
                datalimit = service['datalimit']
            else:
                datalimit = 500

            if service['chartType'] == "barchart":
                chartdata = self.dataset_service.fetch_bar_chart_data(source_id, 'data_mart', dataset_id,
                                                                      metrics, datalimit)
                service['chartdata'] = chartdata
            elif service['chartType'] == "linechart":
                chartdata = self.dataset_service.fetch_line_chart_data(source_id, 'data_mart', dataset_id,
                                                                      metrics, datalimit, is_scatter= False)
                service['chartdata'] = chartdata
            elif service['chartType'] == "scatterchart":
                chartdata = self.dataset_service.fetch_line_chart_data(source_id, 'data_mart', dataset_id,
                                                                      metrics, datalimit, is_scatter= True)
                service['chartdata'] = chartdata

            output.append(service)

        return to_list_json(output, list_count=count)

    def get_charts_count(self, explorer_id, limit, offset, keyword= ""):
        filter_query = {'explorer_id': explorer_id}
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword), filter_query)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query,
                                               offset=offset,
                                               limit=limit)
        return count

    def bulk_delete_charts(self, ids):
        results = []
        for id in ids:
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'chart_id': id})
            results.append(id)
        return results