from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.postgres_db import PostgresDBClient
from pymongo import ReturnDocument


class DataExplorerEDAService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'eda_books'
        self.mongo_client = db_client
        self.dag_doc = None
        self.dataset_service = DataIngestionDatasetService(db_name)

    def search_query(self, keyword):
        return {"$or": [{'chartType': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query, filter_query):
        if query is None:
            return {"$and": [{'client_id': client_id}, {'app_id': app_id}, filter_query]}
        return {"$and": [{'client_id': client_id}, {'app_id': app_id}, filter_query, query]}

    def get_books_count(self, explorer_id, limit, offset):
        filter_query = {'explorer_id': explorer_id}
        query = self.find_query(g.client_id, g.app_id, None, filter_query)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query,
                                               offset=offset, limit=limit)
        return count