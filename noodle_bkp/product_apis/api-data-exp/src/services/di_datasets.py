from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime
import pandas as pd
from flask import jsonify
from src.utils.postgres_db import PostgresDBClient
import copy


class DataIngestionDatasetService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_datasets'
        self.mongo_client = db_client
        self.dag_doc = None

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query, filter_query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query, query]}

    def create_dataset(self, dataset_doc):

        dataset_id = str(uuid.uuid4())

        pg_conn = self.get_target_db_connection(dataset_doc['source_id'], dataset_doc['source_type'])
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        cur = pg_conn.cursor

        query = """select column_name, data_type from information_schema.columns where table_name = '""" + dataset_doc[
            'table_name'] + """'"""

        cur.execute(query)
        row = cur.fetchone()
        columns = []
        while row is not None:
            columns.append(tuple(row))
            row = cur.fetchone()

        dataset_doc.update(
            {
                'dataset_id': dataset_id,
                'created_at': str(datetime.datetime.now()),
                'columns': columns,
                'metrics': {
                    "no_of_rows": 0,
                    "no_of_columns": 0,
                    "size": "0 kb"
                }
            })

        id = self.mongo_client.insert_one(self.db_name, self.collection_name, dataset_doc).inserted_id
        del dataset_doc['_id']
        return to_json(dataset_doc)

    def delete_dataset(self, dataset_id):
        result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'dataset_id': dataset_id})
        return to_json(result)

    def list_datasets(self, source_id, source_type, limit, offset, keyword=''):
        filter_query = {'source_id': source_id}
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword),filter_query)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query, offset=offset, limit=limit)

        output = {
            "data": [],
            "meta": {
                "total_count": count
            },
            "error": {}
        }
        if count ==0:
            output['error'] = "either {0} does not exist or is empty".format(source_type)
        for doc in result:
            del doc['_id']
            output['data'].append(doc)

        return output

    def get_dataset(self, dataset_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'dataset_id': dataset_id})
        for service in output:
            del service['_id']
            output = service

        self.dag_doc = output

        return to_json(output)

    def update_dataset(self, dataset_id, new_delta_doc):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                       query={'dataset_id': dataset_id}, update={'$set': new_delta_doc})
        return to_json(output)

    def list_dataset_records(self, source_id, dataset_id, source_type, limit):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                      {"dataset_id": dataset_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            table_name = return_object['table_name']
            column_details = return_object['columns']
            columns = [column[0] for column in column_details]

            query = "SELECT * FROM  {} LIMIT {}".format(table_name,limit)
            cur = pg_conn.cursor
            cur.execute(query)
            results = []
            for row in cur.fetchall():
                results.append(dict(zip(columns, row)))

            return to_json(results)

    def get_target_db_connection(self,source_id, source_type):
        pg_db_client = None
        try:
            if source_type == 'data_store':
                return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                              {"datastore_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            elif source_type == 'data_cartridge':
                return_object, count = self.mongo_client.find(self.db_name, "di_cartridges",
                                                              {"cartridge_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            else:
                return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                              {"datamart_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            return_object, count = self.mongo_client.find(self.db_name,"di_connections",{"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                db_name = return_object['db_name']
                host = return_object['host']
                port = return_object['port']
                user = return_object['user_name']
                password = return_object['password']

                pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client
        except Exception as e:
            print(e)
            return False

    def get_dataset_stats(self, source_id, dataset_id, source_type):
        output = None
        try:
            pg_conn = self.get_target_db_connection(source_id, source_type)
            if not pg_conn:
                return to_json({"message": "connection could not be established"}, is_error=True)

            return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                          {"dataset_id": dataset_id})

            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                table_name = return_object['table_name']
                column_deatils = return_object['columns']
                columns = [column[0] for column in column_deatils]

                count_query = "SELECT COUNT(1) FROM  {}".format(table_name)
                size_query = "SELECT pg_size_pretty(pg_total_relation_size('{}'));".format(table_name)
                cur = pg_conn.cursor
                cur.execute(count_query)
                count = cur.fetchone()

                cur.execute(size_query)
                size = cur.fetchone()

                output ={

                    'no_of_columns': len(columns),
                    'no_of_rows': count[0],
                    'size': size[0]

                }

            return output
        except Exception as e:
            print(e)
            return to_json(e)

    def bulk_delete_dataset(self, ids):
        results = []
        for id in ids:
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'dataset_id': id})
            results.append(result)
        return results

    def refresh_datasets(self, source_id, source_type):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={}, limit=10000)

        output = []
        for doc in result:
            del doc['_id']
            dataset_id = doc['dataset_id']
            metrics = self.get_dataset_stats(source_id, dataset_id, source_type)
            new_metrics = {
                'metrics': metrics
            }

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                  query={'dataset_id': dataset_id},
                                                  update={'$set': new_metrics})

            output.append(new_metrics)

        return to_json(output)

    def fetch_data(self, source_id, source_type, dataset_id, datalimit):
        #get schema
        schema = self.get_schema_name(source_id, source_type)

        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        dataset = self.get_dataset(dataset_id)
        table_name = '"{0}"'.format(dataset["data"]["table_name"])
        cols = dataset["data"]["columns"]

        columns = []
        for col in cols:
            columns.append('"{0}"'.format(col[0]))

        col_str = ','.join(columns)

        try:
            query = "select {0} from {1}.{2} limit {3}".format(col_str, schema, table_name, datalimit)
            data = pd.read_sql(query, pg_conn.connection)
            data= json.loads(data.to_json(orient='records', date_format='iso'))
            return to_list_json(data, list_count=len(data))
        except Exception as e:
            return to_list_json(str(e), is_error= True)

    def fetch_bar_chart_data(self, source_id, source_type, dataset_id, metrics, datalimit):
        #get schema
        schema = self.get_schema_name(source_id, source_type)

        agg = metrics["agg"]
        x_axis = []
        for axis in metrics["x-axis"]:
            x_axis.append('"{0}"'.format(axis))

        group_by =[]
        if "group_by" in metrics:
            for group in metrics["group_by"]:
                group_by.append('"{0}"'.format(group))

        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        dataset = self.get_dataset(dataset_id)
        table_name = '"{0}"'.format(dataset["data"]["table_name"])

        agg_select_col = []
        origin_y_cols = []
        for metric in agg:
            if "agg_func" in metric:
                agg_select_col.append('CAST({0}("{1}") as float) as {0}_{1}'.format(metric["agg_func"].lower(), metric["col_name"]))
                origin_y_cols.append('MIN({0}_{1})'.format(metric["agg_func"], metric["col_name"]))
            else:
                group_by.append('"{0}"'.format(metric["col_name"]))
                agg_select_col.append('"{0}"'.format(metric["col_name"]))
                origin_y_cols.append('MIN("{0}")'.format(metric['col_name']))

        select_col = ','.join(x_axis + agg_select_col)
        group_by_col = ','.join(x_axis + group_by)
        origin_y_cols = ','.join(origin_y_cols)

        query = 'select {0} from {1}.{2} GROUP BY {3} ORDER BY {4} asc limit {5} '.format(select_col, schema, table_name, group_by_col, x_axis[0], datalimit)
        origin_query = 'select  MIN({0}) as x, LEAST({1}) as y from ({2}) as T1'.format(x_axis[0], origin_y_cols, query)
        response = {
            "data": [],
            "error": {},
            "metadata": {
                "query": query,
                "origin": {
                }
            },
        }
        try:
            data = pd.read_sql(query, pg_conn.connection)
            data = json.loads(data.to_json(orient='records', date_format='iso'))
            count = len(data)
            response["data"] = data
            response["metadata"]["total_count"] = count

            origin_cord = pd.read_sql(origin_query, pg_conn.connection)
            origin_cord = json.loads(origin_cord.to_json(orient='records', date_format='iso'))[0]
            response['metadata']['origin'] = origin_cord

        except Exception as e:
            response["error"] = str(e)

        return response

    def fetch_line_chart_data(self, source_id, source_type, dataset_id, metrics, datalimit, is_scatter):
        #get schema
        schema = self.get_schema_name(source_id, source_type)

        agg = metrics["agg"]
        x_axis = []
        x_axis_col =[]
        for axis in metrics["x-axis"]:
            x_axis_col.append(axis)
            x_axis.append('"{0}"'.format(axis))

        group_by =[]
        if "group_by" in metrics:
            for group in metrics["group_by"]:
                group_by.append('"{0}"'.format(group))

        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        dataset = self.get_dataset(dataset_id)
        table_name = '"{0}"'.format(dataset["data"]["table_name"])

        agg_select_col = []
        agg_col_name = []
        origin_y_cols = []
        for metric in agg:
            if "agg_func" in metric:
                agg_select_col.append('CAST({0}("{1}") as float) as {0}_{2}'.format(metric["agg_func"].lower(),
                                    metric["col_name"], metric["col_name"].lower()))
                agg_col_name.append("{0}_{1}".format(metric["agg_func"].lower(), metric["col_name"].lower()))
                origin_y_cols.append('MIN({0}_{1})'.format(metric["agg_func"].lower(), metric["col_name"].lower()))
            else:
                group_by.append('"{0}"'.format(metric['col_name']))
                agg_select_col.append('"{0}"'.format(metric["col_name"]))
                agg_col_name.append("{0}".format(metric["col_name"]))
                origin_y_cols.append('MIN("{0}")'.format(metric['col_name']))

        select_col = ','.join(x_axis + agg_select_col)
        group_by_col = ','.join(x_axis + group_by)
        origin_y_cols = ','.join(origin_y_cols)

        query = "select {0} from {1}.{2} GROUP BY {3} ORDER BY {4} asc limit {5}".format(select_col, schema, table_name,
                                                                                    group_by_col, x_axis[0], datalimit)
        origin_query = 'select  MIN({0}) as x, LEAST({1}) as y from ({2}) as t1'.format(x_axis[0], origin_y_cols, query)

        response = {
            "data": [],
            "error": {},
            "metadata": {
                "query": query,
                "origin": {}
            }
        }

        try:
            data = pd.read_sql(query, pg_conn.connection)
            data = json.loads(data.to_json(orient='records', date_format='iso'))

            count = len(data)
            response["metadata"]["total_count"] = count

            for chart_y in (agg_col_name):
                chart_data = {
                    "id": chart_y,
                    "data": []
                }
                if is_scatter is True:
                    id_count =0
                    for row in data:
                        chart_data["data"].append({
                            "id": id_count,
                            "x": row[x_axis_col[0]],
                            "y": row[chart_y]
                        })
                        id_count +=1
                else:
                    for row in data:
                        chart_data["data"].append({
                            "x": row[x_axis_col[0]],
                            "y": row[chart_y]
                        })
                response["data"].append(chart_data)

            origin_cord = pd.read_sql(origin_query, pg_conn.connection)
            origin_cord = json.loads(origin_cord.to_json(orient='records', date_format='iso'))[0]
            response['metadata']['origin'] = origin_cord

        except Exception as e:
            response["error"] = str(e)

        return response


    def get_schema_name(self, source_id, source_type):
        schema = 'public'  # default
        try:
            if source_type == 'data_store':
                return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                              {"datastore_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            elif source_type == 'data_cartridge':
                return_object, count = self.mongo_client.find(self.db_name, "di_data_cartridges",
                                                              {"cartridge_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            else:
                return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                              {"datamart_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']

            return_object, count = self.mongo_client.find(self.db_name, "di_connections", {"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                schema = return_object['schema_name']

            return schema
        except Exception as e:
            print(e)
            return schema


dag_service = DataIngestionDatasetService('bat_db')