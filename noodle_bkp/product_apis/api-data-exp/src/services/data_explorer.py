from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime

from src.utils.mongo_db import db_client
from src.utils.postgres_db import PostgresDBClient
from src.services.de_charts import DataExplorerChartService
from src.services.eda_charts import DataExplorerEDAService
from src.services.di_datasets import DataIngestionDatasetService


class DataExplorerService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'data_explorers'
        self.mongo_client = db_client
        self.chart_service = DataExplorerChartService(db_name)
        self.books_service = DataExplorerEDAService(db_name)
        self.dataset_service = DataIngestionDatasetService(db_name)
        self.dag_doc = None

    def create_data_explorer(self, new_doc):
        new_doc['explorer_id'] = str(uuid.uuid4())
        id = self.mongo_client.insert_one(self.db_name, self.collection_name, new_doc).inserted_id
        del new_doc['_id']
        return to_json(new_doc)

    def update_data_explorer(self, explorer_id, new_doc):
        output = self.mongo_client.find_one_and_update(self.db_name, self.collection_name, {'explorer_id': explorer_id},
                                                 {'$set': new_doc})
        if output:
            del output['_id']
        return to_json(output)

    def get_target_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, query]}

    def list_data_explorer(self, limit, offset, keyword=""):
        #self.refresh_datamarts()
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query,
                                                               offset=offset,
                                                               limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_data_explorer(self, explorer_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'explorer_id': explorer_id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def bulk_delete_datamart(self, ids):
        results = []
        for id in ids:
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'explorer_id': id})
            results.append(result)
        return results
                    
    def bulk_delete_data_explorer(self, ids):
        results = []
        for id in ids:
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'explorer_id': id})
            results.append(id)
        return to_json(results)

    def refresh_dataexplorers(self):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={}, limit=10000)
        output = {}
        for doc in result:
            del doc['_id']
            explorer_id = doc['explorer_id']
            metrics = self.get_data_explorer_metrics(doc)
            new_metrics = {
                'metrics': metrics
            }
            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                    query={'explorer_id': explorer_id},
                                                    update={'$set': new_metrics})
            #output.append(metrics)
        return to_json(output)

    def get_data_explorer_metrics(self, doc):
        metrics = {}
        metrics['advanceEDA'] = 1
        datamart_id = doc['datamart_id']

        # Get dataset counts
        dataset = self.dataset_service.list_datasets(datamart_id, 'data_mart', 10000, 0)
        metrics['no_of_datasets'] = dataset['meta']['total_count']

        # Get charts count
        explorer_id = doc['explorer_id']
        charts_count = self.chart_service.get_charts_count(explorer_id, 10000, 0, '')
        metrics['no_of_charts'] = charts_count

        books_counts = self.books_service.get_books_count(explorer_id, 10000, 0)
        metrics['advanceEDA'] = books_counts
        return metrics
