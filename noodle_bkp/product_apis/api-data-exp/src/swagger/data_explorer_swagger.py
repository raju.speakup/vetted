from flask_restful_swagger import swagger
from flask_restful import fields

@swagger.model
class DataExplorerPostSwaggerModel:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'datamart_id': fields.String,
        'datamart_name': fields.String
    }

@swagger.model
class DataExplorerDeleteSwaggerModel:
    resource_fields = {
        'ids': fields.List(fields.String)
    }

@swagger.model
class DataExplorerUpdate:
    resource_fields = {
        "name": fields.String,
        "description": fields.String,
        "datamart_id": fields.String
    }