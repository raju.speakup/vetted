from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class DataGridNode:
    resource_fields = {
        "x": fields.Float,
        "y": fields.Float,
        "w": fields.Float,
        "h": fields.Float,
        "static": fields.Boolean
    }


@swagger.model
class Metrics:
    resource_fields= {
            "agg": fields.List(fields.Raw),
            "group_by": fields.List(fields.Raw),
            "x-axis": fields.List(fields.String)
        }

@swagger.model
@swagger.nested(
    dataGridNode=DataGridNode.__name__,
    metrics=Metrics.__name__
)
class Charts:
    resource_fields = {
        "chartType": fields.String,
        "explorer_id": fields.String,
        "dataset_id": fields.String,
        "source_id": fields.String,
        "dataGridNode": fields.Nested(DataGridNode.__name__),
        "metrics": fields.Nested(Metrics.__name__)
    }

@swagger.model
class UpdateCharts:
    resource_fields = {
        "chartType": fields.String,
        "chart_id": fields.String,
        "dataset_id": fields.String,
        "source_id": fields.String,
        "dataGridNode": fields.Nested(DataGridNode.__name__),
        "metrics": fields.Nested(Metrics.__name__)
    }