CELERY_BROKER_URL = 'amqp://'
CELERY_RESULT_BACKEND = 'amqp://'
CELERY_IMPORTS = (
    'api-data-ingestion.src.resources.di_dag_run',
    'api-user.src.commons.code_snippets'
)