from config_store import config_store_client as conf_client
from src.utils.nlogger import logger


class Config():

    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='api-datastore')
        logger.log().info("load config completed")

    def get_v(self, key):
        return self.cs.get(key)

    def reload(self):
        self.cs.load(ip='*', service_name='api-datastore')

conf = Config()


class CeleryConfig:
    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='celery-worker-config')

    def get_v(self, key):
        return self.cs.get(key)

    def reload(self):
        self.cs.load(ip='*', service_name='celery-worker-config')

celery_conf = CeleryConfig()
