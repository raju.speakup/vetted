from src.services.di_dag import dag_service
from src.services.node_status import NodeStatus
import datetime
import uuid
from src.utils.postgres_db import PostgresDBClient


class DagExecutor:

    def __init__(self):
        self.dag_service = dag_service
        self.postgress_db_service = PostgresDBClient('bat_data_ingestion' ,'batuser', 'Noodle1234' ,'192.168.10.59', "5432")

    def execute(self):
        node = self.dag_service.get_next_scheduled_node()
        node_type = node['type']
        dag_id, node_id = self.node_ids(node)

        self.dag_service.update_node_status(dag_id, node_id, NodeStatus.InProgress)
        self.dag_service.update_node_field(dag_id, node_id, 'start_time', str(datetime.datetime.now()))

        if 'DS' == node_type or 'DC' == node_type:
            self.execute_data_node(node)
        elif 'JOIN' == node_type:
            self.execute_join_node(node)
        elif 'UPDATE' == node_type:
            self.execute_update_node(node)
        elif 'FILTER' == node_type:
            self.execute_filter_node(node)
        elif 'DM' == node_type:
            self.execute_datamart_node(node)

        self.dag_service.update_node_field(dag_id, node_id, 'end_time', str(datetime.datetime.now()))
        self.dag_service.update_node_status(dag_id, node_id, NodeStatus.Complete)

        self.dag_service.update_child_node_status(dag_id, node_id, NodeStatus.Schedule, check_parent_status=True)

    # node execution for datastore and data cartridge
    def execute_data_node(self, node):
        dag_id, node_id = self.node_ids(node)

        self.dag_service.update_node_field(dag_id, node_id, 'tracking_data', node['metadata'])
        print("{} execution completed.".format(node['node_id']))

    def execute_join_node(self, node):
        dag_id, node_id = self.node_ids(node)
        dag_doc = self.dag_service.get_dag(dag_id)
        parents = self.dag_service.get_parent_node(dag_doc['data'], node_id)

        parent1_id, parent2_id = tuple(parents)
        parent1 = self.dag_service.get_sch_node_details(parent1_id)
        parent2 = self.dag_service.get_sch_node_details(parent2_id)

        table_name_1 = parent1['tracking_data']['table_name']
        table_name_2 = parent2['tracking_data']['table_name']

        join_columns = node['metadata']['join_columns']
        proj_data = node['metadata']['projection_columns']

        projection_columns = ''
        for key in proj_data.keys():
            cols = [key + '.' + i for i in proj_data[key]]
            projection_columns = projection_columns + ','.join(cols) + ','
        projection_columns = projection_columns[:-1]

        query = 'select {} from {} t1 inner join {} t2 on ('.format(projection_columns, table_name_1, table_name_2)

        for key in join_columns.keys():
            query = query + 't1.'+key + ' = ' + 't2.'+join_columns[key] + ' and '
        query = query + '1 = 1)'

        temp_table_name = 't' + uuid.uuid4().hex

        join_query = "create table {} as {}".format(temp_table_name, query)

        self.postgress_db_service.cursor.execute(join_query)
        self.postgress_db_service.connection.commit()
        self.dag_service.update_node_field(dag_id, node_id, "tracking_data", {'table_name': temp_table_name})

    def execute_update_node(self, node):
        dag_id, node_id = self.node_ids(node)
        dag_doc = self.dag_service.get_dag(dag_id)
        parents = self.dag_service.get_parent_node(dag_doc['data'], node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(parent_id)

        table_name_1 = parent1['tracking_data']['table_name']

        update_conditions = node['metadata']['update_conditions']
        where_conditions = node['metadata']['where_conditions']

        update_query_conditions = ''
        for update_doc in update_conditions:
            update_query_conditions += "{} {} '{}',".format(update_doc['field_name'],update_doc['field_op'],update_doc['field_value'])
        update_query_conditions = update_query_conditions[:-1]

        where_query_conditions = ''
        for where_doc in where_conditions:
            where_query_conditions += "{} {} {} and ".format(where_doc['field_name'],where_doc['field_op'],where_doc['field_value'])
        where_query_conditions += '1 = 1'

        query = 'update {} set {} where {}'.format(table_name_1, update_query_conditions, where_query_conditions)
        self.postgress_db_service.cursor.execute(query)
        self.postgress_db_service.connection.commit()
        self.dag_service.update_node_field(dag_id, node_id, "tracking_data", {'table_name': table_name_1})

    def execute_filter_node(self, node):
        dag_id, node_id = self.node_ids(node)

        dag_doc = self.dag_service.get_dag(dag_id)
        parents = self.dag_service.get_parent_node(dag_doc['data'], node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(parent_id)
        table_name_1 = parent1['tracking_data']['table_name']

        where_conditions = node['metadata']['where_conditions']

        where_query_conditions = ''
        for where_doc in where_conditions:
            where_query_conditions += "{} {} {} and ".format(where_doc['field_name'], where_doc['field_op'],
                                                             where_doc['field_value'])
        where_query_conditions += '1 = 1'

        query = 'select * from {} where {}'.format(table_name_1, where_query_conditions)

        temp_table_name = 't' + uuid.uuid4().hex

        filter_query = "create table {} as {}".format(temp_table_name, query)
        self.postgress_db_service.cursor.execute(filter_query)
        self.postgress_db_service.connection.commit()

        self.dag_service.update_node_field(dag_id, node_id, "tracking_data", {'table_name': temp_table_name})

    def node_ids(self, node):
        dag_id = node['dag_id']
        node_id = node['node_id']
        return dag_id, node_id

    def execute_datamart_node(self, node):
        dag_id, node_id = self.node_ids(node)

        dag_doc = self.dag_service.get_dag(dag_id)
        parents = self.dag_service.get_parent_node(dag_doc['data'], node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(parent_id)

        table_name_1 = parent1['tracking_data']['table_name']

        table_name = node['metadata']['table_name']

        new_table_query = "create table {} as select * from {}".format(table_name, table_name_1)

        self.postgress_db_service.cursor.execute(new_table_query)
        self.postgress_db_service.connection.commit()

        self.dag_service.update_node_field(dag_id, node_id, "tracking_data", {'table_name': table_name})

    def execute_sql_node(self, node):
        dag_id, node_id = self.node_ids(node)

        dag_doc = self.dag_service.get_dag(dag_id)
        parents = self.dag_service.get_parent_node(dag_doc['data'], node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(parent_id)

        table_name_1 = parent1['tracking_data']['table_name']

        table_name = node['metadata']['table_name']

        sql_query = node['sql_query']

        self.postgress_db_service.cursor.execute(sql_query)
        self.postgress_db_service.connection.commit()

        self.dag_service.update_node_field(dag_id, node_id, "tracking_data", {'table_name': table_name})


de = DagExecutor()
de.execute()
