from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import psycopg2
import psycopg2.extras
import json
import datetime
from flask import jsonify
from src.utils.postgres_db import PostgresDBClient


class DataIngestionNodeService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_datasets'
        self.mongo_client = db_client
        self.dag_doc = None

    def get_join_node(self, dataset_1, dataset_2):
        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                      {"dataset_id": {"$in": [dataset_1, dataset_2]}}, limit=2)

        ds1_cols = [node['columns'] for node in return_object if node['dataset_id'] == dataset_1][0]
        ds2_cols = [node['columns'] for node in return_object if node['dataset_id'] == dataset_2][0]

        ds1_cols = [ds[0] for ds in ds1_cols]
        ds2_cols = [ds[0] for ds in ds2_cols]

        common_cols = list(set(ds1_cols).intersection(ds2_cols))

        total_cols = {}
        total_cols.update({"t1": ds1_cols})
        total_cols.update({"t2": ds2_cols})

        output = {
            'common_cols': common_cols,
            'total_cols': total_cols
        }
        return output

    def get_target_db_connection(self,source_id, source_type):
        pg_db_client = None
        try:
            if source_type == 'data_store':
                return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                              {"datastore_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            elif source_type == 'data_cartridge':
                return_object, count = self.mongo_client.find(self.db_name, "di_data_cartridges",
                                                              {"cartridge_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            else:
                return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                              {"datamart_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']

            return_object, count = self.mongo_client.find(self.db_name,"di_connections",{"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                db_name = return_object['db_name']
                host = return_object['host']
                port = return_object['port']
                user = return_object['user_name']
                password = return_object['password']

                pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client
        except Exception as e:
            print(e)
            return False

    def get_sql_node(self, dataset_id):
        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                      {"dataset_id": dataset_id})
        if not return_object:
            return to_json({"message": "dataset doesn't exist"}, is_error=True)
        else:
            source_id = return_object[0]['source_id']
            source_type = return_object[0]['source_type']

        if source_type == 'data_mart':
            return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                          {"datamart_id":  source_id })
            if not return_object:
                return to_json({"message": "datamart doesn't exist"}, is_error=True)
            conn_id = return_object[0]['conn_id']

        if source_type == 'data_cartridge':
            return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                          {"cartridge_id":  source_id })
            if not return_object:
                return to_json({"message": "data cartridge doesn't exist"}, is_error=True)
            conn_id = return_object[0]['conn_id']

        if source_type == 'data_store':
            return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                          {"datastore_id":  source_id })
            if not return_object:
                return to_json({"message": "data store doesn't exist"}, is_error=True)
            conn_id = return_object[0]['conn_id']

        pg_conn = self.get_source_db_connection(conn_id)

        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                      {"conn_id": conn_id})

        if return_object:
            if not 'schema_name' in return_object[0].keys():
                schema_name = 'public'
            else:
                schema_name = return_object[0]['schema_name']

        query = "SELECT  p.proname \
                FROM    pg_catalog.pg_namespace n \
                JOIN    pg_catalog.pg_proc p \
                ON      p.pronamespace = n.oid \
                WHERE   n.nspname = '{}';".format(schema_name)

        cur = pg_conn.cursor
        cur.execute(query)
        rows = cur.fetchall()
        stored_procs_defs = []
        for row in rows:
            sp_def_query = "select pg_get_functiondef('{}'::regproc);".format(row[0])
            cur.execute(sp_def_query)
            sp_def = cur.fetchall()
            stored_procs_defs.append({
                "name": row[0],
                "definition": sp_def[0][0]
            })
        return to_json(stored_procs_defs)

    def get_source_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client

    def get_node_status(self, node_id, run_id):
        status, count = self.mongo_client.find_with_projection(self.db_name, "di_dag_test", {"node_id": node_id,
                                                                                             "run_id": run_id},
                                                               projection={"status": 1, "tracking_data": 1})
        for service in status:
            del service['_id']
            status = service

        return status

    def get_node_data(self, node_id, run_id):
        data, count = self.mongo_client.find_with_projection(self.db_name, "di_dag_test", {"node_id": node_id,
                                                                                           "run_id": run_id},
                                                             projection={"tracking_data": 1})
        schema, preview = [], []
        for service in data:
            del service['_id']
            data = service
        if data:
            schema = data['tracking_data']['table_schema']
            preview = data['tracking_data']['table_data']

        return schema, preview
