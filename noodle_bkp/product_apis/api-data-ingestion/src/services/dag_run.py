from src.services.node_status import NodeStatus
import datetime
import uuid
from src.utils.postgres_db import PostgresDBClient
from src.utils.config import conf, celery_conf
from src.utils.json_utils import to_json
from src.utils.mongo_db import db_client
from src.services.run_status import RunStatus
import psycopg2
from src.services.QBuilder import BuildQuery
from src.services.sql_parser import SQLParser
import pandas as pd
import json


class DagRun:

    def __init__(self, pg_meta):
        self.db_name = conf.get_v('db.name')
        self.mongo_client = db_client
        self.pg_meta = pg_meta
        self.postgress_db_service = PostgresDBClient(pg_meta['db_name'], pg_meta['user'], pg_meta['password'],
                                                     pg_meta['host'], pg_meta['port'])
        self.builder = BuildQuery()

    def run_dag(self, run_id, dag_doc):
        de = DagExecutor(self.pg_meta)
        while True:
            return_object, count = self.mongo_client.find(self.db_name, "di_dag_test", {"run_id": run_id,
                                                                                        "status": {"$ne": "C"}}
                                                          )
            failed_node, count = self.mongo_client.find(self.db_name, "di_dag_test", {"run_id": run_id,
                                                                                      "status": "F"})

            if failed_node:
                return "DAG testing failed"

            if return_object:
                output = de.execute(run_id, dag_doc)
                if output is not True:
                    return output

            else:
                print("no more node to execute")
                return True

    def remove_temp_data(self, run_id):
        return_object, count = self.mongo_client.find_with_projection(self.db_name, "di_dag_test",
                                                 {"run_id": run_id,
                                                  "type": {"$in": ["JOIN", "UPDATE", "FILTER", "DS", "DC"]}
                                                 },
                                                 projection={"temp_data": 1},
                                                 limit=100
                                                                      )

        temp_tables =[]
        for service in return_object:
            del service['_id']
            temp_tables.append(service)

        for temp_table in temp_tables:
            if 'temp_data' in temp_table.keys():
                table_name = temp_table['temp_data']['table_name']
                query = self.builder.drop_temp_table_query(table_name, 'postgres')

                self.postgress_db_service.cursor.execute(query)
                self.postgress_db_service.connection.commit()

    def get_runs_details(self, dag_id):
        output, count = self.mongo_client.find(self.db_name, "di_dag_runs", {"dag_id": dag_id}, limit=100)

        results = []
        for doc in output:
            del doc['_id']
            results.append(doc)

        return results

    def add_dag_run(self, dag_id, run_id, start_time, end_time, version, status, scheduled='NA'):
        run_doc = {
            "dag_id": dag_id,
            "run_id": run_id,
            "start_time": start_time,
            "end_time": end_time,
            "status": status,
            "version": version,
            "scheduled": scheduled
        }
        res = self.mongo_client.insert_one(self.db_name, "di_dag_runs", run_doc)
        del run_doc['_id']
        return to_json(run_doc)


class DagExecutor:

    def __init__(self, pg_meta):
        db_name = conf.get_v('db.name')
        self.pg_db = conf.get_v('POSTGRES_DB')
        self.dag_service = DataIngestionDagRunService(db_name)
        self.db_meta = pg_meta
        self.schema_name = pg_meta['schema_name']
        self.postgress_db_service = PostgresDBClient(pg_meta['db_name'], pg_meta['user'], pg_meta['password'],
                                                     pg_meta['host'], pg_meta['port'])
        self.builder = BuildQuery()

    def execute(self, run_id, dag_doc):
        self.dag_doc = dag_doc
        node = self.dag_service.get_next_scheduled_node(run_id)
        node_type = node['type']
        dag_id, node_id = self.node_ids(node)
        self.dag_service.update_node_status(run_id, node_id, NodeStatus.InProgress)
        self.dag_service.update_node_field(run_id, node_id, 'start_time', str(datetime.datetime.now()))

        if 'DS' == node_type or 'DC' == node_type:
            status = self.execute_data_node(node, node_type)
        elif 'JOIN' == node_type:
            status = self.execute_join_node(dag_doc, node)
        elif 'TRANSFORM' == node_type:
            status = self.execute_update_node(dag_doc, node)
        elif 'FILTER' == node_type:
            status = self.execute_filter_node(dag_doc, node)
        elif 'COL_FILTER' == node_type:
            status = self.execute_column_filter_node(dag_doc, node)
        elif 'AGGREGATE' == node_type:
            status = self.execute_aggregate_node(dag_doc, node)
        elif 'DM' == node_type:
            status = self.execute_datamart_node(dag_doc, node)
        elif 'SQL' == node_type:
            status = self.execute_sql_node(dag_doc, node)

        if status is True:
            self.dag_service.update_node_field(run_id, node_id, 'end_time', str(datetime.datetime.now()))
            self.dag_service.update_node_status(run_id, node_id, NodeStatus.Complete)

            self.dag_service.update_child_node_status(dag_doc, run_id, node_id, NodeStatus.Schedule, check_parent_status=True)
            return True
        else:
            return status

    # node execution for datastore and data cartridge
    def execute_data_node(self, node, node_type):
        dag_id, node_id = self.node_ids(node)

        dataset_id = node['metadata']['dataset_id']
        db_meta = self.dag_service.get_db_details_from_dataset(dataset_id, node_type)
        if not db_meta:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            return 'dataset {} is not available'.format(dataset_id)
        output = self.dag_service.compare_db_meta(db_meta, self.db_meta)
        if output:
            table_name = self.dag_service.get_table_name(dataset_id)
            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, table_name, self.schema_name)
            temp_table_data_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': temp_table_data_schema,
                                                'target_schema': temp_table_data_schema})
        else:
            if db_meta['host'] == self.pg_db:
                db_meta['host'] = '127.0.0.1'
            table_name, columns = self.dag_service.get_table_schema(dataset_id)
            temp_table_name = 't' + uuid.uuid4().hex
            query = self.builder.remote_data_transfer(table_name, columns, temp_table_name, db_meta)
            try:
                self.postgress_db_service.cursor.execute(query)
                self.postgress_db_service.connection.commit()

            except psycopg2.Error as e:
                self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
                print(e)
                return str(e)
            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, temp_table_name, self.schema_name)
            temp_table_data_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, temp_table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': temp_table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': temp_table_data_schema,
                                                'target_schema': temp_table_data_schema})
            self.dag_service.update_node_field(node['run_id'], node_id, 'temp_data', {"table_name": temp_table_name})

        print("{} execution completed.".format(node['node_id']))
        return True

    def execute_join_node(self, dag_doc, node):

        dag_id, node_id = self.node_ids(node)

        parents = self.dag_service.get_parent_node(dag_doc, node_id)

        parent1_id, parent2_id = tuple(parents)
        parent1 = self.dag_service.get_sch_node_details(node['run_id'], parent1_id)
        parent2 = self.dag_service.get_sch_node_details(node['run_id'], parent2_id)

        table_name_1 = parent1['tracking_data']['table_name']
        table_name_2 = parent2['tracking_data']['table_name']

        parent_1_data_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_1)
        parent_2_data_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_2)

        source_schema = []
        source_schema.extend(parent_1_data_schema)
        source_schema.extend(parent_2_data_schema)
        source_schema = self.dag_service.remove_duplicate(source_schema)

        query = self.builder.join_query_build(table_name_1, table_name_2, node['metadata'], 'postgres')

        temp_table_name = 't' + uuid.uuid4().hex

        join_query = "create table {} as {}".format(temp_table_name, query)
        try:
            self.postgress_db_service.cursor.execute(join_query)
            self.postgress_db_service.connection.commit()
            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, temp_table_name, self.schema_name)
            temp_table_data_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, temp_table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': temp_table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': source_schema,
                                                'target_schema': temp_table_data_schema})
            self.dag_service.update_node_field(node['run_id'], node_id, "temp_data", {'table_name': temp_table_name})
            print("{} execution completed.".format(node['node_id']))
            return True
        except psycopg2.Error as e:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            print(e)
            return str(e)

    def execute_update_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)
        parents = self.dag_service.get_parent_node(dag_doc, node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(node['run_id'], parent_id)
        table_name_1 = parent1['tracking_data']['table_name']
        source_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_1)

        rules = node['metadata']['rules']
        for rule in rules:
            query = self.builder.transform_query_build(table_name_1, rule, 'postgres')
            try:
                self.postgress_db_service.cursor.execute(query)
                self.postgress_db_service.connection.commit()

            except psycopg2.Error as e:
                self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
                print(e)
                return str(e)
        temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, table_name_1, self.schema_name)
        target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_1)
        self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                           {'table_name': table_name_1,
                                            'table_data': temp_table_data,
                                            'source_schema': source_schema,
                                            'target_schema': target_schema})
        print("{} execution completed.".format(node['node_id']))
        return True

    def execute_aggregate_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        parents = self.dag_service.get_parent_node(dag_doc, node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(node['run_id'], parent_id)
        table_name_1 = parent1['tracking_data']['table_name']
        source_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_1)

        query = self.builder.aggregate_on_columns(table_name_1, node['metadata'], 'postgres')

        temp_table_name = 't' + uuid.uuid4().hex

        filter_query = "create table {} as {}".format(temp_table_name, query)
        try:
            self.postgress_db_service.cursor.execute(filter_query)
            self.postgress_db_service.connection.commit()

            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, temp_table_name, self.schema_name)
            target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, temp_table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': temp_table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': source_schema,
                                                'target_schema': target_schema})
            self.dag_service.update_node_field(node['run_id'], node_id, "temp_data", {'table_name': temp_table_name})
            print("{} execution completed.".format(node['node_id']))
            return True
        except psycopg2.Error as e:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            return str(e)


    def execute_column_filter_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        parents = self.dag_service.get_parent_node(dag_doc, node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(node['run_id'], parent_id)
        table_name_1 = parent1['tracking_data']['table_name']
        source_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_1)

        query = self.builder.filter_column(table_name_1, node['metadata'], 'postgres')

        temp_table_name = 't' + uuid.uuid4().hex

        filter_query = "create table {} as {}".format(temp_table_name, query)
        try:
            self.postgress_db_service.cursor.execute(filter_query)
            self.postgress_db_service.connection.commit()

            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, temp_table_name, self.schema_name)
            target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, temp_table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': temp_table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': source_schema,
                                                'target_schema': target_schema})
            self.dag_service.update_node_field(node['run_id'], node_id, "temp_data", {'table_name': temp_table_name})
            print("{} execution completed.".format(node['node_id']))
            return True
        except psycopg2.Error as e:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            return str(e)

    def execute_filter_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        parents = self.dag_service.get_parent_node(dag_doc, node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(node['run_id'], parent_id)
        table_name_1 = parent1['tracking_data']['table_name']
        source_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name_1)

        query = self.builder.filter_query_build(table_name_1, node['metadata'], 'postgres')

        temp_table_name = 't' + uuid.uuid4().hex

        filter_query = "create table {} as {}".format(temp_table_name, query)
        try:
            self.postgress_db_service.cursor.execute(filter_query)
            self.postgress_db_service.connection.commit()

            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, temp_table_name, self.schema_name)
            target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, temp_table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': temp_table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': source_schema,
                                                'target_schema': target_schema})
            self.dag_service.update_node_field(node['run_id'], node_id, "temp_data", {'table_name': temp_table_name})
            print("{} execution completed.".format(node['node_id']))
            return True
        except psycopg2.Error as e:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            return str(e)

    def node_ids(self, node):
        dag_id = node['dag_id']
        node_id = node['node_id']
        return dag_id, node_id

    def execute_datamart_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        parents = self.dag_service.get_parent_node(dag_doc, node_id)
        parent_id = parents[0]

        parent1 = self.dag_service.get_sch_node_details(node['run_id'], parent_id)

        parent_table_name = parent1['tracking_data']['table_name']
        source_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, parent_table_name)
        # need to fetch from dataset_id
        dataset_id = node['metadata']['dataset_id']
        if 'auto_create' in node['metadata'].keys():
            auto_create = node['metadata']['auto_create']
        else:
            auto_create = False

        if auto_create:
            table_name = node['metadata']['target_mart_table']
            source_id = node['metadata']['datamart_id']
            dataset_doc = {
                  "name": table_name,
                  "description": "dynamic dataset creation",
                  "source_type": "data_mart",
                  "source_id": source_id,
                  "table_name": table_name
            }

            mart_schema = self.dag_service.get_datamart_table_schema(self.postgress_db_service, parent_table_name)
            create_mart_query = self.builder.create_datamart_query(table_name, mart_schema, 'postgres')
            try:
                self.postgress_db_service.cursor.execute(create_mart_query)
                self.postgress_db_service.connection.commit()
                output = self.dag_service.create_dataset(self.postgress_db_service, dataset_doc)
                if output != 'success':
                    return output
                target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name)
            except psycopg2.Error as e:
                self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
                return str(e)
            query = self.builder.get_cols_list_query(table_name, 'postgres')

        else:
            table_name = self.dag_service.get_table_name(dataset_id)
            target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name)

            if not self.dag_service.compare_schema(source_schema, target_schema):
                self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
                return "source and target schema doesn't match"

            query = self.builder.get_cols_list_query(table_name, 'postgres')

        self.postgress_db_service.cursor.execute(query)
        row = self.postgress_db_service.cursor.fetchone()
        col_items = []
        while row is not None:
            col_items.append(row[0])
            row = self.postgress_db_service.cursor.fetchone()
        truncate_mart_query = self.builder.truncate_table(table_name)
        data_mart_query = self.builder.copy_table_data_query(table_name, parent_table_name, col_items, 'postgres')

        try:
            self.postgress_db_service.cursor.execute(truncate_mart_query)
            self.postgress_db_service.connection.commit()
            self.postgress_db_service.cursor.execute(data_mart_query)
            self.postgress_db_service.connection.commit()
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {"table_name": table_name,
                                                "source_schema": source_schema,
                                                "target_schema": target_schema})
            print("{} execution completed.".format(node['node_id']))
            return True
        except psycopg2.Error as e:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            return str(e)

    def execute_sql_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        parents = self.dag_service.get_parent_node(dag_doc, node_id)
        parent_id = parents[0]

        parent = self.dag_service.get_sch_node_details(node['run_id'], parent_id)

        table_name = parent['tracking_data']['table_name']
        source_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name)

        # need to fetch from dataset_id
        type = node['metadata']['type']
        sql = node['metadata']['sql']
        if type == 'query':
            sql_query = node['sql']
        else:
            sql_query = 'EXEC ' + node['sql']

        try:
            self.postgress_db_service.cursor.execute(sql_query)
            self.postgress_db_service.connection.commit()
            temp_table_data = self.dag_service.get_table_data(self.postgress_db_service, table_name, self.schema_name)
            target_schema = self.dag_service.get_temp_table_schema(self.postgress_db_service, table_name)
            self.dag_service.update_node_field(node['run_id'], node_id, "tracking_data",
                                               {'table_name': table_name,
                                                'table_data': temp_table_data,
                                                'source_schema': source_schema,
                                                'target_schema': target_schema})

            print("{} execution completed.".format(node['node_id']))
            return True
        except psycopg2.Error as e:
            self.dag_service.update_node_status(node['run_id'], node_id, NodeStatus.Failed)
            return str(e)


class DataIngestionDagRunService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_dag_test'
        self.mongo_client = db_client
        self.dag_doc = None
        self.sql_parse_service = SQLParser()

    def save_test_dag(self, sch_doc):
        res = self.mongo_client.insert_one(self.db_name, "di_dag_test", sch_doc)
        del sch_doc['_id']
        return to_json(sch_doc)

    def get_scheduled_dag(self, dag_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                            query={'dag_id': dag_id, "is_exec": 1})
        for doc in res:
            del doc['_id']
        return res

    def get_parent_node(self, dag_doc, node_id):
        parents = [node['parents'] for node in dag_doc['nodes'] if node['id'] == node_id]
        return parents[0]

    def get_child_node(self, dag_doc, node_id):
        children = [node['id'] for node in dag_doc['nodes'] if node_id in node['parents']]
        return children

    def get_node_status(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['status']

    def get_exec_order(self, dag_doc):
        exec_nodes = [(node['id'], node['order']) for node in dag_doc['nodes'] if node['is_exec'] == 1]
        if len(exec_nodes) > 1:
            node_order = sorted([exec_nodes],key=lambda x: x[1])
        else:
            node_order = [exec_nodes[0][0]]
        return node_order

    def get_node_metadata(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['metadata']

    def update_node_status(self, run_id, node_id, new_status):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name="di_dag_test",
                                                       query={'run_id': run_id, "node_id": node_id},
                                                       update={'$set': {"status":new_status }})

    def update_node_field(self, run_id, node_id, field_name, field_value):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name="di_dag_test",
                                                       query={'run_id': run_id, "node_id": node_id},
                                                       update={'$set': {field_name: field_value }})

    def get_node_type(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['type']

    def get_node_details(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res

    def update_child_node_status(self,dag_doc, run_id, node_id, status, check_parent_status):
        child_node_ids = self.get_child_node(dag_doc, node_id)

        for child_node_id in child_node_ids:

            #child = self.get_sch_node_details(dag_id, child_node_id)

            parent_ids = self.get_parent_node(dag_doc, child_node_id)

            for parent_id in parent_ids:
                parent = self.get_sch_node_details(run_id, parent_id)
                if 'C' != parent['status']:
                    return None

            self.update_node_status(run_id, child_node_id, 'S')

    def get_next_scheduled_node(self, run_id):
        node, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                            query={'status': 'S', 'run_id': run_id}, limit=1)
        for doc in node:
            del doc['_id']
        return node[0]

    def exec_node(self, di_dag_id, node_id, node_type):
        pass

    def get_sch_node_details(self, run_id, node_ids):
        node, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_test",
                                             query={'node_id': node_ids,
                                                    'run_id': run_id}, limit=1)
        for doc in node:
            del doc['_id']
        return node[0]

    def get_target_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client

    def get_table_name(self, dataset_id):
        node, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name="di_datasets",
                                             query={'dataset_id': dataset_id},
                                             projection={"table_name": 1})
        if len(node) !=0:
            for doc in node:
                del doc['_id']
            return node[0]['table_name']

    def get_table_schema(self, dataset_id):
        node, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name="di_datasets",
                                             query={'dataset_id': dataset_id},
                                             projection={"table_name": 1, "columns": 1})
        if len(node) !=0:
            for doc in node:
                del doc['_id']
            return node[0]['table_name'], node[0]['columns']

    def get_dag_nodes(self, dag_id):
        nodes, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name="di_dags",
                                                             query={'di_dag_id': dag_id},
                                                            projection={"nodes": 1})
        if not nodes:
            return []
        if len(nodes) != 0:
            for doc in nodes:
                del doc['_id']
            return nodes[0]['nodes']

    def set_dag_run(self, dag_id):
        dag_run_doc = dict()
        dag_run_doc['dag_id'] = dag_id
        dag_run_doc['start_time'] = datetime.datetime.now()
        res = self.mongo_client.insert_one(
                        db_name=self.db_name,
                        collection_name='di_dag_running',
                        data=dag_run_doc
                        )

    def unset_dag_run(self, dag_id):
        res = self.mongo_client.find_one_and_delete(
                        db_name=self.db_name,
                        collection_name='di_dag_running',
                        query={'dag_id' : dag_id}
                        )

        result, count = self.mongo_client.find(
            db_name=self.db_name,
            collection_name='di_dag_running',
            query={'dag_id': dag_id}
        )

    def check_dag_running(self, dag_id):

        result, count = self.mongo_client.find(
                        db_name=self.db_name,
                        collection_name='di_dag_running',
                        query={'dag_id' : dag_id}
                        )
        return bool(count)

    def set_run_doc(self, run_id, dag_id, start_time, version, dag, pg_meta, app_id, client_id):
        run_doc = locals()
        name = dag.get('name')
        desc = dag.get('description')
        del run_doc['dag']
        del run_doc['pg_meta']
        del run_doc['self']
        msg = "Queuing DI pipeline {}. DAG_ID : {}. RUN_ID : {}. QUEUE : {} ".format(dag['name'], dag_id, run_id,
                                                                                     'di_queue_1')
        run_doc.update(
            {
                'type': 'data_ingestion',
                'meta': {},
                'percentage': 0,
                'log': [msg],
                'source': 'adhoc',
                'schedule': 'NA',
                'name': name,
                'description': desc,
                'duration': '0 seconds',
                'status': RunStatus().InProgress,
                'end_time': datetime.datetime.now().strftime(celery_conf.get_v('DATE_FORMAT'))

            }
        )
        self.mongo_client.insert_one(
            db_name=self.db_name,
            collection_name=celery_conf.get_v('RUN_STATUS_COLLECTION'),
            data=run_doc
        )

    def update_run_log(self, run_id, msg):
        self.mongo_client.update_one(
            db_name=self.db_name,
            collectionname=celery_conf.get_v('RUN_STATUS_COLLECTION'),
            data=[
                {
                    'run_id': run_id
                },
                {
                    '$push': {
                        'log': msg
                    }
                }
            ]
        )

    def get_db_details_from_dataset(self, dataset_id, type ='DM'):
        return_object, count = self.mongo_client.find_with_projection(db_name=self.db_name,
                                                                      collection_name="di_datasets",
                                                                      query={'dataset_id': dataset_id},
                                                                      projection={"source_id": 1}

        )
        for service in return_object:
            del service['_id']
            return_object = service
        if not return_object:
            return False
        if return_object:
            source_id = return_object['source_id']

        if type == 'DM':
            return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                          {"datamart_id": source_id})
        elif type == 'DC':
            return_object, count = self.mongo_client.find(self.db_name, "di_data_cartridges",
                                                          {"cartridge_id": source_id})
        elif type == 'DS':
            return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                          {"datastore_id": source_id})

        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            conn_id = return_object['conn_id']

        return_object, count = self.mongo_client.find(self.db_name, "di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            output = {
                "db_name":return_object['db_name'],
                "host": return_object['host'],
                "port": return_object['port'],
                "user": return_object['user_name'],
                "password": return_object['password'],
                "schema_name": return_object['schema_name']
            }
            return output
        else:
            return False

    def get_db_details_from_dag_id(self, dag_id, version='current_version'):
        if version == 'current_version':
            return_object, count = self.mongo_client.find_with_projection(db_name=self.db_name,
                                                                          collection_name="di_dags",
                                                                          query={'di_dag_id': dag_id},
                                                                          projection={"nodes": 1}

                                                                          )
            for service in return_object:
                del service['_id']
                return_object = service
            if return_object:
                nodes = return_object['nodes']
            else:
                return False

            dataset_id = nodes[-1]['metadata']['dataset_id']
            pg_meta = self.get_db_details_from_dataset(dataset_id)

            return pg_meta
        else:
            return_object, count = self.mongo_client.find_with_projection(db_name=self.db_name,
                                                                          collection_name="di_dag_commits",
                                                                          query={'dag_id': dag_id,
                                                                                 'version_number': version},
                                                                          projection={"dag.nodes": 1}

                                                                          )
            for service in return_object:
                del service['_id']
                return_object = service
            if return_object:
                nodes = return_object['dag']['nodes']
            else:
                return False

            dataset_id = nodes[-1]['metadata']['dataset_id']
            pg_meta = self.get_db_details_from_dataset(dataset_id)

            return pg_meta

    def get_runs_details(self, dag_id):
        output, count = self.mongo_client.find_latest(self.db_name, "di_dag_runs", {"dag_id": dag_id},
                                                      projection={'_id': 0},
                                                      limit=100)

        results = []
        for doc in output:
            dag_id = doc['dag_id']
            run_id = doc['run_id']

            percent_complete = self.get_dag_completion(dag_id, run_id)
            doc.update({"percent_complete": percent_complete})
            results.append(doc)

        return results

    def get_dag_completion(self, dag_id, run_id):
        completed = self.mongo_client.count_docs(self.db_name, collectionname="di_dag_test",
                                                 data={
                                                        "dag_id": dag_id,
                                                        "run_id": run_id,
                                                        "status": 'C'
                                                      }
                                                )
        total = self.mongo_client.count_docs(self.db_name, collectionname="di_dag_test",
                                             data={
                                                     "dag_id": dag_id,
                                                     "run_id": run_id
                                                  }
                                             )
        if total == 0:
            total = 1
        return round(float(completed/total)*100, 2)

    @staticmethod
    def compare_db_meta(db_meta_1, db_meta_2):
        sharedKeys = set(db_meta_1.keys()).intersection(db_meta_2.keys())
        for key in sharedKeys:
            if db_meta_1[key] != db_meta_2[key]:
                return False
        return True

    @staticmethod
    def get_table_data(pg_client, table_name, schema):
        query = 'select * from {}.{} limit 20'.format(schema, table_name)
        data = pd.read_sql(query, pg_client.connection)
        results = data.to_json(orient='records', date_format='iso')

        return json.loads(results)

    @staticmethod
    def get_temp_table_schema(pg_client, table_name):
        schema_query = "   WITH pkey_list as (SELECT c.column_name \
                       FROM information_schema.key_column_usage AS c \
                       LEFT JOIN information_schema.table_constraints AS t \
                       ON t.constraint_name = c.constraint_name \
                       WHERE t.table_name = '{}' AND t.constraint_type = 'PRIMARY KEY') \
                       select distinct cc.column_name, cc.data_type, \
                       case when cc.column_name  in (select column_name from pkey_list) then 'YES' \
                       else 'NO' end as primary_key, cc.is_nullable \
                       from information_schema.columns cc\
                       where cc.table_name = '{}' ".format(table_name, table_name)
        schema = []
        try:
            pg_client.cursor.execute(schema_query)
            row = pg_client.cursor.fetchone()
            while row is not None:
                schema.append(row)
                row = pg_client.cursor.fetchone()
        except psycopg2.Error as e:
            print(e)

        return schema

    @staticmethod
    def compare_schema(source_schema, target_schema):
        if len(source_schema) != len(target_schema):
            return False
        for i, j in zip(source_schema, target_schema):
            if not (i[0] == j[0]) or not (i[1] == j[1]):
                return False
        return True

    @staticmethod
    def get_datamart_table_schema(pg_client, table_name):
        schema_query = "   WITH pkey_list as (SELECT c.column_name \
                           FROM information_schema.key_column_usage AS c \
                           LEFT JOIN information_schema.table_constraints AS t \
                           ON t.constraint_name = c.constraint_name \
                           WHERE t.table_name = '{}' AND t.constraint_type = 'PRIMARY KEY') \
                           select distinct cc.column_name, cc.data_type, cc.character_maximum_length, \
                           case when cc.column_name  in (select column_name from pkey_list) then 'YES' \
                           else 'NO' end as primary_key, cc.is_nullable \
                           from information_schema.columns cc\
                           where cc.table_name = '{}' ".format(table_name, table_name)
        schema = []
        try:
            pg_client.cursor.execute(schema_query)
            row = pg_client.cursor.fetchone()
            while row is not None:
                schema.append(row)
                row = pg_client.cursor.fetchone()
        except psycopg2.Error as e:
            print(e)

        return schema

    def create_dataset(self, pg_conn, dataset_doc):
        schema = self.get_datamart_schema(dataset_doc['source_id'])
        if schema:
            schema_name = schema
        else:
            schema_name = 'public'

        output, count = self.mongo_client.find(self.db_name, "di_datamarts", {'datamart_id': dataset_doc['source_id']})
        result = {}
        for service in output:
            result = service
            del result['_id']
        client_id = result['clientid']
        app_id = result['appid']

        output, count = self.mongo_client.find(self.db_name, "di_datasets", {"name": dataset_doc['name'],
                                                                             "appid": app_id,
                                                                             "clientid": client_id})
        if output:
            return "dataset name already exists"

        dataset_id = str(uuid.uuid4())

        cur = pg_conn.cursor

        table_name = str.lower(dataset_doc['table_name'])
        query = "select column_name, data_type from information_schema.columns where table_schema = '{0}' " \
                "and table_name = '{1}'".format(schema_name, table_name)
        try:
            cur.execute(query)
            row = cur.fetchone()
        except psycopg2.Error as e:
            return str(e)
        columns = []
        while row is not None:
            columns.append(tuple(row))
            row = cur.fetchone()

        count_query = "SELECT COUNT(1) FROM {}.{}".format(schema_name, dataset_doc['table_name'])
        size_query = "SELECT pg_total_relation_size('{}.{}');".format(schema_name, dataset_doc['table_name'])
        cur = pg_conn.cursor
        try:
            cur.execute(count_query)
            count = cur.fetchone()

            cur.execute(size_query)
            size = cur.fetchone()
        except psycopg2.Error as e:
            return str(e)

        dataset_doc.update(
            {
                'dataset_id': dataset_id,
                'created_at': str(datetime.datetime.now()),
                'created_by': g.user_name,
                'columns': columns,
                'clientid': client_id,
                'appid': app_id,
                'metrics': {
                    "no_of_rows": count[0],
                    "no_of_columns": len(columns),
                    "size": size[0]
                }
            })

        id = self.mongo_client.insert_one(self.db_name, self.collection_name, dataset_doc).inserted_id
        del dataset_doc['_id']

        self.mongo_client.update_one(
            self.db_name,
            collectionname='di_datamarts',
            data=({
                      'datamart_id': dataset_doc['source_id']
                  },
                  {
                      "$push": {
                          "datasets": {
                              "dataset_id": dataset_id,
                              "name": dataset_doc['name'],
                              "table_name": dataset_doc['table_name']
                          }
                      }
                  }

            ))
        return 'success'

    def get_datamart_schema(self, source_id):
        schema = 'public'  # default
        try:
            return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                          {"datamart_id": source_id})
            for service in return_object:
                del service['_id']
                return_object = service
            if return_object:
                conn_id = return_object['conn_id']

            return_object, count = self.mongo_client.find(self.db_name, "di_connections", {"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                schema = return_object['schema_name']

            return schema
        except Exception as e:
            print(e)
            return schema

    def get_source_target(self, json_data):
        output = []
        nodes = json_data['nodes']
        for node in nodes:
            parents = node['parents']

            for p in parents:
                inserted_nodes = [out['node_id'] for out in output]
                if p not in inserted_nodes:
                    try:
                        parent_node = [node for node in nodes if node['id'] == p][0]
                    except:
                        return to_json({"message":"parent node {} is not present".format(p)}, is_error=True)
                    status = self.push_schema(output, parent_node)
                    if not status:
                        return to_json({"message": "source and target schema doesn't match",
                                        "schema": output}, is_error=True)
            status = self.push_schema(output, node)
            if status != 'pushed':
                return to_json({"message": status,
                                "schema": output}, is_error=True)

        return to_json(output)

    def push_schema(self, output, node):
        parents = node['parents']
        node_type = node['type']
        node_id = node['id']
        source_schema = []
        target_schema = []
        if node_type == 'DS' or node_type == 'DC':
            dataset_id = node['metadata']['dataset_id']
            try:
                table_name, table_schema = self.get_table_schema(dataset_id)
            except:
                return "dataset {} is not present".format(dataset_id)
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": table_schema})
            return 'pushed'
        elif node_type == 'JOIN':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)

            projections = node['metadata']['projection_columns']
            target_cols = projections['t1']
            target_cols.extend(projections['t2'])

            target_schema.extend([col for col in source_schema if col[0] in target_cols])
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            return 'pushed'
        elif node_type == 'FILTER':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)

            target_schema.extend(source_schema)
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            return 'pushed'
        elif node_type == 'COL_FILTER':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)

            filter_cols = node['metadata']['selected_columns']
            target_schema.extend([col for col in source_schema if col[0] in filter_cols])
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            return 'pushed'
        elif node_type == 'TRANSFORM':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)

            target_schema.extend(source_schema)

            # derived_cols = node['metadata']['derived_cols']
            # if derived_cols:
            #     target_schema.extend([derived_cols['column_name'], derived_cols['data_type']])

            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            return 'pushed'
        elif node_type == 'SQL':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)
            if node['metadata']['type'] == 'query':
                sql_query = node['metadata']['sql']
                query_columns = self.sql_parse_service.parse_sql_columns(sql_query)
                query_tables = self.sql_parse_service.parse_sql_tables(sql_query)

                for col in query_columns:
                    if col not in [sr_col[0] for sr_col in source_schema]:
                        return '{} column used in the query is not part of source schema'.format(col)


            target_schema.extend(source_schema)
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            return 'pushed'
        elif node_type == 'AGGREGATE':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)

            seleted_columns = node['metadata']['select_columns']
            agg_columns = node['metadata']['columns_to_aggregate']
            agg_cols = [a['agg_func'] for a in agg_columns]
            total_cols = seleted_columns.extend(agg_cols)

            target_schema.extend([col for col in source_schema if col[0] in total_cols])
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            return 'pushed'

        elif node_type == 'DM':
            for p in parents:
                parent_schema = [out['target_schema'] for out in output if out['node_id'] == p][0]
                source_schema.extend(parent_schema)
            source_schema = self.remove_duplicate(source_schema)

            dataset_id = node['metadata']['dataset_id']
            try:
                table_name, table_schema = self.get_table_schema(dataset_id)
            except:
                return "dataset {} is not present".format(dataset_id)
            target_schema.extend(table_schema)
            output.append({"node_id": node_id, "node_type": node_type, "source_schema": source_schema,
                           "target_schema": target_schema})
            if parents:
                is_equal = self.compare_schema(sorted(source_schema), sorted(target_schema))
                if is_equal:
                    return 'pushed'
                else:
                    return "source and target schema doesn't match"
            return 'pushed'

    @staticmethod
    def remove_duplicate(duplicate):
        final_list = []
        for num in duplicate:
            if num not in final_list:
                final_list.append(num)
        return final_list
