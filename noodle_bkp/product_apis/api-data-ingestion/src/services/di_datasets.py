from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
import psycopg2
import psycopg2.extras
import json
import datetime
from src.utils.postgres_db import PostgresDBClient
import pandas as pd
from src.services.QBuilder import BuildQuery
from src.services.di_datamart import DataIngestionDataMartService
from src.services.audit import AuditService


class DataIngestionDatasetService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_datasets'
        self.mongo_client = db_client
        self.dag_doc = None
        self.builder = BuildQuery()
        self.datamart_service = DataIngestionDataMartService(db_name)
        self.audit_service = AuditService(db_name)

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query, filter_query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, filter_query, query]}

    def create_dataset(self, dataset_doc):
        pg_conn = self.get_target_db_connection(dataset_doc['source_id'], dataset_doc['source_type'])
        schema = self.get_schema_name(dataset_doc['source_id'], dataset_doc['source_type'])
        if schema:
            schema_name = schema
        else:
            schema_name = 'public'
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True), 200

        if dataset_doc['source_type'] == 'data_mart':
            if dataset_doc['new_table_schema'] and not dataset_doc['is_columnar']:
                status = self.create_new_db_table(dataset_doc['source_id'], dataset_doc['new_table_schema'])
                if status is not True:
                    return to_json({"message": status}, is_error=True), 200
            elif dataset_doc['new_table_schema'] and dataset_doc['is_columnar']:
                status = self.create_new_columnar_table(dataset_doc['new_table_schema'], pg_conn)
                if status is not True:
                    return to_json({"message": status}, is_error=True), 200
            elif not dataset_doc['new_table_schema'] and dataset_doc['is_columnar']:
                status = self.create_existing_columnar_table(dataset_doc, pg_conn)
                if status is not True:
                    return to_json({"message": status}, is_error=True), 200

        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'name': dataset_doc['name'],
                                                       "appid": dataset_doc['appid'],
                                                        "clientid":dataset_doc['clientid']})
        if output:
            return to_json({"message": "dataset name already exists"}, is_error=True), 200

        dataset_id = str(uuid.uuid4())

        cur = pg_conn.cursor

        table_name = str.lower(dataset_doc['table_name'])
        query = "select column_name, data_type from information_schema.columns where table_schema = '{0}' " \
                "and table_name = '{1}'".format(schema_name, table_name)
        try:
            cur.execute(query)
            row = cur.fetchone()
        except psycopg2.Error as e:
            return to_json({"message": str(e)}, is_error=True), 200
        columns = []
        while row is not None:
            columns.append(tuple(row))
            row = cur.fetchone()

        count_query = "SELECT COUNT(1) FROM {}.{}".format(schema_name, dataset_doc['table_name'])
        size_query = "SELECT pg_total_relation_size('{}.{}');".format(schema_name, dataset_doc['table_name'])
        cur = pg_conn.cursor
        try:
            cur.execute(count_query)
            count = cur.fetchone()

            cur.execute(size_query)
            size = cur.fetchone()
        except psycopg2.Error as e:
            return to_json({"message": str(e)}, is_error=True), 200

        dataset_doc.update(
            {
                'dataset_id': dataset_id,
                'created_at': str(datetime.datetime.now()),
                'created_by': g.user_name,
                'columns': columns,
                'metrics': {
                    "no_of_rows": count[0],
                    "no_of_columns": len(columns),
                    "size": size[0]
                }
            })

        id = self.mongo_client.insert_one(self.db_name, self.collection_name, dataset_doc).inserted_id
        del dataset_doc['_id']

        if dataset_doc['source_type'] == 'data_store':
            collection_name = 'di_datastores'
            field_name = 'datastore_id'
        elif dataset_doc['source_type'] == 'data_cartridge':
            collection_name = 'di_data_cartridges'
            field_name = 'cartridge_id'
        else:
            collection_name = 'di_datamarts'
            field_name = 'datamart_id'

        self.mongo_client.update_one(
            self.db_name,
            collectionname=collection_name,
            data=({
                      field_name: dataset_doc['source_id']
                  },
                  {
                      "$push": {
                          "datasets": {
                              "dataset_id": dataset_id,
                              "name": dataset_doc['name'],
                              "table_name": dataset_doc['table_name']
                          }
                      }
                  }

            ))
        audit_data = {"action": "new dataset {} created.".format(dataset_doc['name'])}
        self.audit_service.add_audit_log(audit_data)
        return to_json(dataset_doc), 201

    def delete_dataset(self, dataset_id):
        found = self.mongo_client.find(self.db_name, 'di_dag_commits',
                                       {
                                           'dag.nodes.metadata.dataset_id': id
                                       })
        if found:
            return to_json({"message": "this dataset is being used in a pipeline."}, is_error=True)
        result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'dataset_id': dataset_id})
        return to_json(result)

    def list_datasets(self, source_id, source_type, limit=100, offset=0, keyword=""):
        #self.refresh_datasets(source_id, source_type)
        filter_query = {'source_id': source_id}
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword),filter_query)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query, offset=offset, limit=limit)

        output = []
        for doc in result:
            del doc['_id']
            output.append(doc)

        return to_list_json(output, list_count=count)

    def get_dataset(self, dataset_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'dataset_id': dataset_id})
        for service in output:
            del service['_id']
            output = service

        self.dag_doc = output

        return to_json(output)

    def update_dataset(self, dataset_id, new_delta_doc):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                       query={'dataset_id': dataset_id}, update={'$set': new_delta_doc})
        return to_json(output)

    def list_dataset_records(self, source_id, dataset_id, source_type, limit):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                      {"dataset_id": dataset_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            table_name = return_object['table_name']
            output = self.preview_table_records(source_id, table_name, source_type, False, limit)
            return output

    def get_target_db_connection(self,source_id, source_type):
        pg_db_client = None
        try:
            if source_type == 'data_store':
                return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                              {"datastore_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            elif source_type == 'data_cartridge':
                return_object, count = self.mongo_client.find(self.db_name, "di_data_cartridges",
                                                              {"cartridge_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            else:
                return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                              {"datamart_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']

            return_object, count = self.mongo_client.find(self.db_name,"di_connections",{"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                db_name = return_object['db_name']
                host = return_object['host']
                port = return_object['port']
                user = return_object['user_name']
                password = return_object['password']

                pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client
        except Exception as e:
            print(e)
            return False

    def get_dataset_stats(self, source_id, dataset_id, source_type):
        output = None
        try:
            pg_conn = self.get_target_db_connection(source_id, source_type)
            if not pg_conn:
                return to_json({"message": "connection could not be established"}, is_error=True)

            return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                          {"dataset_id": dataset_id})

            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                table_name = return_object['table_name']
                column_deatils = return_object['columns']
                columns = [column[0] for column in column_deatils]
                schema = self.get_schema_name(source_id, source_type)

                count_query = "SELECT COUNT(1) FROM {0}.{1}".format(schema, table_name)

                size_query = "SELECT pg_total_relation_size('{0}.{1}')".format(schema, table_name)

                cur = pg_conn.cursor
                cur.execute(count_query)
                count = cur.fetchone()

                cur.execute(size_query)
                size = cur.fetchone()

                output ={

                    'no_of_columns': len(columns),
                    'no_of_rows': count[0],
                    'size': size[0]

                }

            return output
        except Exception as e:
            print(e)
            return to_json(e)

    def bulk_delete_dataset(self, source_id, source_type, ids):
        results = {'success': [], 'failed': []}
        if source_type == 'data_store':
            collection_name = 'di_datastores'
            field_name = 'datastore_id'
        elif source_type == 'data_cartridge':
            collection_name = 'di_data_cartridges'
            field_name = 'cartridge_id'
        else:
            collection_name = 'di_datamarts'
            field_name = 'datamart_id'
        found_in_pipeline= []
        for id in ids:
            found, count = self.mongo_client.find(self.db_name, 'di_dag_commits',
                                           {
                                               'dag.nodes.metadata.dataset_id': id
                                           })
            if found:
                found_in_pipeline.append(id)
                continue
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name,
                                                           {'dataset_id': id,
                                                            'source_id': source_id,
                                                            'source_type': source_type
                                                            })
            self.mongo_client.update_one(
                self.db_name,
                collectionname=collection_name,
                data=({
                          field_name: source_id
                      },
                      {
                          "$pull": {
                              "datasets": {
                                  "dataset_id": id
                              }
                          }
                      }

                ),)

            results['success'].append(id)
        results['failed'] = found_in_pipeline
        return results

    def refresh_datasets(self, source_id, source_type, client_id, app_id):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={"source_id":source_id,
                                                                                          "source_type": source_type,
                                                                                          "clientid": client_id,
                                                                                          "appid": app_id}, limit=10000)
        output = []
        for doc in result:
            del doc['_id']
            dataset_id = doc['dataset_id']
            metrics = self.get_dataset_stats(source_id, dataset_id, source_type)
            new_metrics = {
                'metrics': metrics
            }

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                  query={'dataset_id': dataset_id},
                                                  update={'$set': new_metrics})

            output.append(new_metrics)

        return to_json(output)

    def get_table_schema(self, source_id, source_type, table_name):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        cur = pg_conn.cursor

        query = "   WITH pkey_list as (SELECT c.column_name \
                    FROM information_schema.key_column_usage AS c \
                    LEFT JOIN information_schema.table_constraints AS t \
                    ON t.constraint_name = c.constraint_name \
                    WHERE t.table_name = '{}' AND t.constraint_type = 'PRIMARY KEY') \
                    select distinct cc.column_name, cc.data_type, \
                    case when cc.column_name  in (select column_name from pkey_list) then 'YES' \
                    else 'NO' end as primary_key, cc.is_nullable \
                    from information_schema.columns cc\
                    where cc.table_name = '{}' ".format(table_name, table_name)
        try:
            cur.execute(query)
            row = cur.fetchone()
            schema = []
            while row is not None:
                schema.append(row)
                row = cur.fetchone()
        except psycopg2.Error as e:
            print(e)
            return {}

        return schema

    def get_dataset_schema(self, source_id, source_type, dataset_id):
        table_name, id = self.get_dataset_info(dataset_id)
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        cur = pg_conn.cursor

        query = "   WITH pkey_list as (SELECT c.column_name \
                    FROM information_schema.key_column_usage AS c \
                    LEFT JOIN information_schema.table_constraints AS t \
                    ON t.constraint_name = c.constraint_name \
                    WHERE t.table_name = '{}' AND t.constraint_type = 'PRIMARY KEY') \
                    select distinct cc.column_name, cc.data_type, cc.is_nullable, \
                    case when cc.column_name  in (select column_name from pkey_list) then 'YES' \
                    else 'NO' end as primary_key \
                    from information_schema.columns cc\
                    where cc.table_name = '{}' ".format(table_name, table_name)

        cur.execute(query)
        row = cur.fetchone()
        schema = []
        while row is not None:
            schema.append(row)
            row = cur.fetchone()

        return schema

    def filter_data(self, source_id, dataset_id, source_type, filter_conditions):
        results = []
        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                     {"dataset_id": dataset_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            table_name = return_object['table_name']
            column_details = return_object['columns']
            columns = [column[0] for column in column_details]

            where_query_conditions = ''
            for filter in filter_conditions:
                where_query_conditions += "{} {} {} and ".format(filter['field_name'], filter['field_op'],
                                                                 filter['field_value'])
            where_query_conditions += '1 = 1'

            schema = self.get_schema_name(source_id, source_type)
            query = 'select * from {0}.{1} where {2}'.format(schema, table_name, where_query_conditions)
            pg_conn = self.get_target_db_connection(source_id, source_type)

            cur = pg_conn.cursor
            cur.execute(query)

            for row in cur.fetchall():
                results.append(dict(zip(columns, row)))

        return to_json(results)

    def preview_table_records(self, source_id, table_name, source_type, filter_conditions, limit):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        schema = self.get_schema_name(source_id, source_type)
        if schema:
            schema_name = schema
        else:
            schema_name = 'public'
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        if filter_conditions:
            filter_conditions = self.builder.jq_build('postgres', filter_conditions)
            query = "SELECT * FROM  {}.{} WHERE {} LIMIT {}".format(schema_name,table_name, filter_conditions, limit)
        else:
            query = "SELECT * FROM  {}.{} LIMIT {}".format(schema_name,table_name, limit)
        try:
            data = pd.read_sql(query, pg_conn.connection)
        except:
            return {}

        results = data.to_json(orient='records', date_format='iso')
        return json.loads(results)

    def get_dataset_metrics(self, dataset_id, pg_conn):
        output = None
        try:
            return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                          {"dataset_id": dataset_id})

            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                table_name = return_object['table_name']
                column_details = return_object['columns']
                columns = [column[0] for column in column_details]
                count_query = "SELECT COUNT(1) FROM  {}".format(table_name)

                build_size_query = ""
                for column in columns:
                    build_size_query += "pg_column_size('" + column + "') + "
                build_size_query = build_size_query[0:-3]
                size_query = "SELECT sum({0}) from {1};".format(build_size_query, table_name)

                cur = pg_conn.cursor
                cur.execute(count_query)
                count = cur.fetchone()

                cur.execute(size_query)
                size = cur.fetchone()

                output ={

                    'no_of_columns': len(columns),
                    'no_of_rows': count[0],
                    'size': size[0]

                }

            return output
        except Exception as e:
            print(e)
            return to_json(e)

    def refresh_dataset_metrics(self, source_id, source_type, client_id, app_id):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={"source_id":source_id, "source_type": source_type,
                                                                                          "clientid": client_id, "appid": app_id}, limit=10000)
        output = []
        for doc in result:
            del doc['_id']
            dataset_id = doc['dataset_id']
            metrics = self.get_dataset_metrics(dataset_id, pg_conn)
            new_metrics = {
                'metrics': metrics
            }

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                  query={'dataset_id': dataset_id},
                                                  update={'$set': new_metrics})

            output.append(new_metrics)

        return to_json(output)

    def get_dataset_info(self, dataset_id):
        return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                      {"dataset_id": dataset_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            table_name = return_object['table_name']
            source_id = return_object['source_id']
        return table_name, source_id

    def refresh_datasets_with_conn(self, source_id, source_type, client_id, app_id, conn_id, pg_conn):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={"source_id":source_id,
                                                                                          "source_type": source_type,
                                                                                          "clientid": client_id,
                                                                                          "appid": app_id}, limit=10000)
        output = []
        for doc in result:
            del doc['_id']
            dataset_id = doc['dataset_id']
            metrics = self.get_dataset_stats_with_conn(source_id, dataset_id, source_type, conn_id, pg_conn)
            new_metrics = {
                'metrics': metrics
            }

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                  query={'dataset_id': dataset_id},
                                                  update={'$set': new_metrics})

            output.append(new_metrics)

        return to_json(output)

    def refresh_dataset_cols(self, source_id, source_type, client_id, app_id, conn_id, pg_conn):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={"source_id":source_id,
                                                                                          "source_type": source_type,
                                                                                          "clientid": client_id,
                                                                                          "appid": app_id}, limit=10000)
        for doc in result:
            del doc['_id']
            dataset_id = doc['dataset_id']
            columns = self.get_dataset_cols(source_id, dataset_id, source_type, conn_id, pg_conn)
            update_cols = {
                "columns": columns
            }
            self.mongo_client.find_one_and_update(db_name=self.db_name,
                                                  collection_name=self.collection_name,
                                                  query={'dataset_id': dataset_id},
                                                  update={'$set': update_cols})
        return True

    def get_dataset_stats_with_conn(self, source_id, dataset_id, source_type, conn_id, pg_conn):
        output = None
        try:
            return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                          {"dataset_id": dataset_id})

            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                table_name = return_object['table_name']
                column_details = return_object['columns']
                columns = [column[0] for column in column_details]
                schema = self.get_schema_name(source_id, source_type)

                count_query = "SELECT COUNT(1) FROM  {0}.{1}".format(schema, table_name)
                size_query = "SELECT pg_total_relation_size('{0}.{1}')".format(schema, table_name)

                try:
                    cur = pg_conn.cursor
                    cur.execute(count_query)
                    count = cur.fetchone()
                    cur.execute(size_query)
                    size = cur.fetchone()
                except psycopg2.Error as e:
                    pg_conn.connection.rollback()
                    print(e)
                    output = {
                        'no_of_columns': 0,
                        'no_of_rows': 0,
                        'size': 0
                    }
                    return output
                output = {
                    'no_of_columns': len(columns),
                    'no_of_rows': count[0],
                    'size': size[0]
                }

            return output
        except Exception as e:
            print(e)
            return to_json(e)

    def get_dataset_cols(self, source_id, dataset_id, source_type, conn_id, pg_conn):
        output = None
        try:
            return_object, count = self.mongo_client.find(self.db_name, "di_datasets",
                                                          {"dataset_id": dataset_id})

            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                table_name = return_object['table_name']
                schema = self.get_schema_name(source_id, source_type)
                query = "select column_name, data_type from information_schema.columns where " \
                        " table_schema = '{0}' AND table_name = '{1}'".format(schema, table_name)

                cur = pg_conn.cursor
                cur.execute(query)
                row = cur.fetchone()
                columns = []
                while row is not None:
                    columns.append(tuple(row))
                    row = cur.fetchone()

            return columns
        except Exception as e:
            print(e)
            return to_json(e)

    def to_columnar(self, pg_conn, table_name, columns):
        #Check if columnar table exists
        #LOGIC

        new_table = table_name + "_col"
        new_col_str = ""
        targeted_col = []
        for col in columns:
            new_col_str += col[0] + " " + col[1]
            targeted_col.append(col[0])
        targeted_col = ','.join(targeted_col)

        create_table_query = "CREATE FOREIGN TABLE {0}({1}) SERVER cstore_server ' \
                       'OPTIONS(compression 'pglz');".format(new_table, new_col_str)
        migrate_data_query = "insert into {0}({1}) select * from {2}".format(new_table, targeted_col, table_name)

        try:
            cur = pg_conn.cursor
            cur.execute(create_table_query)
            cur.execute(migrate_data_query)
            cur.commit()
        except Exception as e:
            print(e)
            return to_json(e)
        return True

    def preview_dataset_records(self, source_id, dataset_id, source_type, filter_conditions, limit):
        pg_conn = self.get_target_db_connection(source_id, source_type)
        schema = self.get_schema_name(source_id, source_type)
        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        table_name, id = self.get_dataset_info(dataset_id)
        if filter_conditions:
            filter_conditions = self.builder.jq_build('postgres', filter_conditions)
            query = "SELECT * FROM  {}.{} WHERE {} LIMIT {}".format(schema, table_name, filter_conditions, limit)
        else:
            query = "SELECT * FROM  {}.{} LIMIT {}".format(schema, table_name, limit)

        data = pd.read_sql(query, pg_conn.connection)

        results = data.to_json(orient='records', date_format='iso')
        return json.loads(results)

    def create_new_db_table(self, source_id, data):
        table_name = str(data['table_name']).lower()

        if ' ' in table_name:
            return to_json({"message": "table name can't contain spaces"}, is_error=True)

        query = self.builder.create_table_query_build(table_name, data, 'postgres')

        return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                      {"datamart_id": source_id})

        if not return_object:
            return "provided datamart does not exist"

        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            conn_id = return_object['conn_id']

        pg_conn = self.datamart_service.get_target_db_connection(conn_id)

        if not pg_conn:
            return "connection could not be established"
        try:
            cur = pg_conn.cursor
            cur.execute(query)

            pg_conn.connection.commit()
            self.datamart_service.refresh_table_list(source_id)
            return True
        except Exception as e:
            return str(e)

    def create_new_columnar_table(self, data, conn):
        table_name = str.lower(data['table_name'])
        query = self.builder.create_new_columnar_table_query(table_name, data, 'postgres')
        try:
            cur = conn.cursor
            cur.execute(query)

            conn.connection.commit()
            return True
        except Exception as e:
            return str(e)

    def get_schema_name(self, source_id, source_type):
        schema = 'public'  # default
        try:
            if source_type == 'data_store':
                return_object, count = self.mongo_client.find(self.db_name, "di_datastores",
                                                              {"datastore_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            elif source_type == 'data_cartridge':
                return_object, count = self.mongo_client.find(self.db_name, "di_data_cartridges",
                                                              {"cartridge_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']
            else:
                return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                              {"datamart_id": source_id})
                for service in return_object:
                    del service['_id']
                    return_object = service
                if return_object:
                    conn_id = return_object['conn_id']

            return_object, count = self.mongo_client.find(self.db_name,"di_connections",{"conn_id": conn_id})
            for service in return_object:
                del service['_id']
                return_object = service

            if return_object:
                schema = return_object['schema_name']

            return schema
        except Exception as e:
            print(e)
            return schema

    def create_existing_columnar_table(self, data, conn):
        old_table_name = str.lower(data['table_name'])
        table_name = 'col_'+old_table_name

        columns_query = self.builder.get_cols_schema_list_query(old_table_name, 'postgres')

        cur = conn.cursor

        try:
            cur.execute(columns_query)
            row = cur.fetchone()
            columns = []
            while row is not None:
                columns.append(tuple(row))
                row = cur.fetchone()
        except Exception as e:
            return str(e)
        cols_list = [col[0] for col in columns]
        create_query = self.builder.create_existing_columnar_table_query(table_name, columns, 'postgres')
        copy_data_query = self.builder.copy_table_data_query(table_name, old_table_name, cols_list, 'postgres')

        try:
            cur.execute(create_query)
            conn.connection.commit()
            cur.execute(copy_data_query)
            conn.connection.commit()
            return True
        except Exception as e:
            return str(e)


#dag_service = DataIngestionDatasetService('bat_db')
