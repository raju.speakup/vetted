from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
from pymongo.errors import PyMongoError
import json
import datetime


class AuditService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'auditLogs'
        self.mongo_client = db_client
        self.dag_doc = None

    def add_audit_log(self, audit_data):
        """Add audit Logs"""
        audit_data.update({"user_name": (g.user_name if g.user_name else '-'),
                           "userId": (g.user_id if g.user_id else '-'),
                           "time": str(datetime.datetime.now()),
                           "client_id": (g.client_id if g.client_id else '-'),
                           "app_id": (g.app_id if g.app_id else '-')
                           })
        try:
            self.mongo_client.insert_one(self.db_name,
                                         self.collection_name,
                                         data=audit_data
                                         )
        except PyMongoError as err:
            return to_json({"message": str(err)}, is_error=True)
