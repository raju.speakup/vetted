from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
from src.utils.postgres_db import PostgresDBClient
from flask import g
import psycopg2
import uuid
import datetime
from src.services.audit import AuditService
from src.services.QBuilder import BuildQuery

class DatastoreService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.collection_name = 'di_datastores'
        self.audit_service = AuditService(db_name)
        self.builder = BuildQuery()

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, query]}

    def list_datastore(self, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.client.find(self.db_name, "di_datastores", query=query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            if('tables' in service):
                del service['tables']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_datastore(self, datastore_id):
        output, count = self.client.find(self.db_name, "di_datastores", {'datastore_id': datastore_id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_datastore(self, datastore_id, new_delta_doc):
        output = self.client.find_one_and_update(self.db_name, "di_datastores", {'datastore_id': datastore_id},
                                                 {'$set': new_delta_doc})
        if output:
            del output['_id']
        return to_json(output)

    def delete_datastore(self, datastore_id):
        result = self.client.find_one_and_delete(self.db_name, "di_datastores", {'datastore_id': datastore_id})
        return result

    def bulk_delete_datastore(self, ids):
        results = []
        for id in ids:
            removed_datasets = self.client.delete_multiple(self.db_name, "di_datasets", {"source_id": id})
            result = self.client.find_one_and_delete(self.db_name, "di_datastores", {'datastore_id': id})
            results.append(id)
        return results

    def create_datastore(self, datastore_doc):
        output, count = self.client.find(self.db_name, self.collection_name, {'name': datastore_doc['name'],
                                                       "appid": datastore_doc['appid'],
                                                        "clientid":datastore_doc['clientid']})
        if output:
            return to_json({"message": "datastore name already exists"}, is_error=True)

        datastore_id = str(uuid.uuid4())
        pg_conn = self.get_target_db_connection(datastore_doc['conn_id'])

        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        return_object, count = self.client.find(self.db_name, "di_connections",
                                                      {"conn_id": datastore_doc['conn_id']})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys():
                schema_name = 'public'
                db_name = return_object['db_name']
            else:
                schema_name = return_object['schema_name']
                db_name = return_object['db_name']

        query = self.builder.get_table_view_query_build(schema_name, 'postgres')
        cur = pg_conn.cursor
        cur.execute(query)
        rows = cur.fetchall()
        tables = []
        for row in rows:
            tables.append(row[0])

        datastore_doc.update(
            {
                'datastore_id': datastore_id,
                'created_at': str(datetime.datetime.now()),
                'created_by': g.user_name,
                'tables': tables,
                'datasets': [],
                'metrics': {
                    "no_of_rows": 0,
                    "no_of_tables": 0,
                    "size": 0
                }
            })

        id = self.client.insert_one(self.db_name, "di_datastores", datastore_doc).inserted_id

        audit_data = {"action": "new datastore {} created.".format(datastore_doc['name'])}
        self.audit_service.add_audit_log(audit_data)
        del datastore_doc['_id']
        return to_json(datastore_doc)

    def get_table_list(self,conn_id):
        return_object, count = self.client.find(self.db_name,"di_connections",{"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']
            schema_name = return_object['schema_name']

            table_list = []
            try:
                pg_db_client = PostgresDBClient(db_name, user, password, host, port)
                cur = pg_db_client.cursor

                query = self.builder.get_table_view_query_build(schema_name, 'postgres')
                cur.execute(query)
                rows = cur.fetchall()
                for row in rows:
                    table_list.append(row[0])

                cur.close()

            except Exception as e:
                print(e)

            return table_list

    def get_dataset_list(self, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.client.find_with_projection(self.db_name, self.collection_name, query=query,
                                                         projection={'name': '', 'datastore_id': '', 'datasets': '',
                                                                     'conn_id': ''},
                                                         offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)
        return to_list_json(output, list_count=count)

    def refresh_table_list(self, source_id):
        result, count = self.client.find(self.db_name, self.collection_name, query={"datastore_id": source_id})

        conn_id = result[0]['conn_id']
        pg_conn = self.get_target_db_connection(conn_id)

        return_object, count = self.client.find(self.db_name, "di_connections",
                                                      {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys():
                schema_name = 'public'
            else:
                schema_name = return_object['schema_name']

        tables = []
        if pg_conn:
            cur = pg_conn.cursor
            query = self.builder.get_table_view_query_build(schema_name, 'postgres')
            cur.execute(query)
            rows = cur.fetchall()

            for row in rows:
                tables.append(row[0])

        table_doc = {"tables": tables}
        output = self.client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                       query={'datastore_id': source_id},
                                                       update={'$set': table_doc})

        if output:
            del output['_id']
        return to_json(output)

    def get_target_db_connection(self, conn_id):
        return_object, count = self.client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']
            try:
                pg_db_client = PostgresDBClient(db_name, user, password, host, port)
            except psycopg2.Error as e:
                print(e)
                return False

            return pg_db_client
        else:
            return False

    def refresh_table_list_with_conn(self, source_id, conn_id, pg_conn):

        return_object, count = self.client.find(self.db_name, "di_connections",
                                                      {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys():
                schema_name = 'public'
            else:
                schema_name = return_object['schema_name']

        tables = []
        if pg_conn:
            cur = pg_conn.cursor
            query = self.builder.get_table_view_query_build(schema_name, 'postgres')
            try:
                cur.execute(query)
                rows = cur.fetchall()
                for row in rows:
                    tables.append(row[0])
            except psycopg2.Error as e:
                pg_conn.connection.rollback()
                print(e)
                table_doc = {"tables": []}

        table_doc = {"tables": tables}
        output = self.client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                 query={'datastore_id': source_id},
                                                 update={'$set': table_doc})

        if output:
            del output['_id']
        return to_json(output)
