from src.utils.json_utils import to_json, to_list_json
from flask import g
from src.utils.mongo_db import db_client
import uuid
import psycopg2
import psycopg2.extras
import datetime
from src.utils.postgres_db import PostgresDBClient
from src.services.audit import AuditService
from src.services.QBuilder import BuildQuery


class DataIngestionDataMartService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'di_datamarts'
        self.mongo_client = db_client
        self.dag_doc = None
        self.audit_service = AuditService(db_name)
        self.builder = BuildQuery()

    def create_table(self, new_table_doc):
        id = self.mongo_client.insert_one(self.db_name, "di_datasets", new_table_doc).inserted_id

        self.mongo_client.update_one(self.db_name,collectionname="di_datamarts",
                                    data=({"datamart_id": new_table_doc["source_id"]},
                  {
                      "$push": {
                          "datasets": {
                              "dataset_id": new_table_doc["dataset_id"],
                              "name": new_table_doc['name']
                          }
                      }
                  }

            ))
        del new_table_doc['_id']
        return to_json(new_table_doc)

    def get_target_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            try:
                pg_db_client = PostgresDBClient(db_name, user, password, host, port)
            except psycopg2.Error as e:
                print(e)
                return False

            return pg_db_client

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and": [{'clientid': client_id}, {'appid': app_id}]}
        return {"$and": [{'clientid': client_id}, {'appid': app_id}, query]}

    def list_datamart(self, limit, offset, keyword=""):
        #self.refresh_datamarts()
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_datamart(self, datamart_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'datamart_id': datamart_id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_datamart(self, datamart_id, new_delta_doc):
        output = self.mongo_client.find_one_and_update(self.db_name, self.collection_name, {'datamart_id': datamart_id}, {'$set': new_delta_doc})
        if output:
            del output['_id']
        return to_json(output)

    def delete_datamart(self, datamart_id):
        result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'datamart_id': datamart_id})
        return result

    def bulk_delete_datamart(self, ids):
        results = []
        for id in ids:
            removed_datasets = self.mongo_client.delete_multiple(self.db_name, "di_datasets", {"source_id": id})
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'datamart_id': id})
            results.append(id)
        return results

    def create_datamart(self, datamart_doc):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'name': datamart_doc['name'],
                                                       "appid": datamart_doc['appid'],
                                                        "clientid":datamart_doc['clientid']})
        if output:
            return to_json({"message": "datamart name already exists"}, is_error=True)

        datamart_id = str(uuid.uuid4())
        pg_conn = self.get_target_db_connection(datamart_doc['conn_id'])

        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)

        return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                      {"conn_id": datamart_doc['conn_id'] })
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys() :
                schema_name = 'public'
                db_name = return_object['db_name']
            else:
                schema_name = return_object['schema_name']
                db_name = return_object['db_name']
        query = self.builder.get_table_view_query_build(schema_name, 'postgres')
        cur = pg_conn.cursor
        cur.execute(query)
        rows = cur.fetchall()
        tables = []
        for row in rows:
            tables.append(row[0])

        datamart_doc.update(
            {
                'datamart_id': datamart_id,
                'created_at': str(datetime.datetime.now()),
                'created_by': g.user_name,
                'tables': tables,
                'datasets':[],
                'metrics': {
                    "no_of_rows": 0,
                    "no_of_tables": 0,
                    "size": 0
                }
            })
        id = self.mongo_client.insert_one(self.db_name, self.collection_name, datamart_doc).inserted_id

        audit_data = {"action": "new datamart {} created.".format(datamart_doc['name'])}
        self.audit_service.add_audit_log(audit_data)
        del datamart_doc['_id']
        return to_json(datamart_doc)

    def get_datamart_stats(self, datamart_id, conn_id):
        output = {}
        try:
            pg_conn = self.get_target_db_connection(conn_id)
            if not pg_conn:
                return to_json({"message": "connection could not be established"}, is_error=True)

            return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                          {"conn_id": conn_id})
            if not return_object:
                return to_json({"message": "connection could not be found"}, is_error=True)

            if return_object:
                if not 'schema_name' in return_object[0].keys():
                    schema_name = 'public'
                    db_name = return_object[0]['db_name']
                else:
                    schema_name = return_object[0]['schema_name']
                    db_name = return_object[0]['db_name']

            return_object, ds_count = self.mongo_client.find_with_projection(self.db_name, "di_datasets",
                                                          {"source_id": datamart_id}, {'table_name': 1})

            cur = pg_conn.cursor

            query = self.builder.get_table_view_query_build(schema_name, 'postgres')
            cur.execute(query)
            rows = cur.fetchall()
            tables = []
            for row in rows:
                tables.append(row[0])

            query = self.builder.total_rows_query(schema_name, 'postgres')
            cur.execute(query)
            db_size = cur.fetchone()
            db_size = db_size[0]

            query = self.builder.total_size_query(db_name, 'postgres')
            cur.execute(query)
            no_of_rows = cur.fetchone()[0]

            output.update({
                "tables": tables,
                "metrics": {
                    "no_of_rows": no_of_rows,
                    "no_of_tables": ds_count,
                    "size": db_size
                }
            })

            return output
        except Exception as e:
            print(e)
            return to_json(e)

    def refresh_datamarts(self):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={}, limit=10000)
        output = []
        for doc in result:
            del doc['_id']
            datamart_id = doc['datamart_id']
            conn_id = doc['conn_id']
            metrics = self.get_datamart_stats(datamart_id, conn_id)

            self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                           query={'datamart_id': datamart_id},
                                                           update={'$set': metrics})

            output.append(metrics)

        return to_json(output)

    def get_dataset_list(self, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.mongo_client.find_with_projection(self.db_name, self.collection_name, query=query,
                                                         projection={'name': '', 'datamart_id': '', 'datasets': '',
                                                                     'conn_id': ''},
                                                         offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)
        return to_list_json(output, list_count=count)

    def refresh_table_list(self, source_id):
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query={"datamart_id": source_id})

        conn_id = result[0]['conn_id']
        pg_conn = self.get_target_db_connection(conn_id)

        return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                      {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys():
                schema_name = 'public'
            else:
                schema_name = return_object['schema_name']

        tables = []
        if pg_conn:
            cur = pg_conn.cursor
            query = self.builder.get_table_view_query_build(schema_name, 'postgres')
            cur.execute(query)
            rows = cur.fetchall()

            for row in rows:
                tables.append(row[0])

        table_doc = {"tables": tables}
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,
                                                       query={'datamart_id': source_id},
                                                       update={'$set': table_doc})

        if output:
            del output['_id']
        return to_json(output)

    def refresh_table_list_with_conn(self, source_id, conn_id, pg_conn):

        return_object, count = self.mongo_client.find(self.db_name, "di_connections",
                                                      {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            if not 'schema_name' in return_object.keys():
                schema_name = 'public'
            else:
                schema_name = return_object['schema_name']

        tables = []
        if pg_conn:
            cur = pg_conn.cursor
            query = self.builder.get_table_view_query_build(schema_name, 'postgres')
            try:
                cur.execute(query)
                rows = cur.fetchall()
                for row in rows:
                    tables.append(row[0])

            except psycopg2.Error as e:
                pg_conn.connection.rollback()
                print(e)
                table_doc = {"tables": []}

        table_doc = {"tables": tables}
        output = self.mongo_client.find_one_and_update(db_name=self.db_name,
                                                       collection_name=self.collection_name,
                                                       query={'datamart_id': source_id},
                                                       update={'$set': table_doc})

        if output:
            del output['_id']
        return to_json(output)
