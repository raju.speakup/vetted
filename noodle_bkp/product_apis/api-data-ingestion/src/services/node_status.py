class NodeStatus():
    New = "N"
    Complete = "C"
    InProgress = "P"
    Schedule = "S"
    Failed = "F"
