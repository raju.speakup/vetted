from src.utils.config import conf
from src.utils.json_utils import to_json, to_list_json
from src.utils.mongo_db import db_client
import os
import requests
from src.utils.nlogger import logger
from src.services.audit import AuditService
import datetime


class DICommitService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.db_client = db_client
        self.audit_service = AuditService(db_name)

    def create_commit(self, dag_id, commit):
        return_object, count = self.db_client.find(self.db_name, 'di_dag_commits',
                                            query={"dag_id": dag_id,
                                                   "version_number": commit['version_number'],
                                                   "appid": commit['appid'],
                                                    "clientid":commit['clientid'] }
                                            )
        if return_object:
            return to_json({'message': 'version number already exist'}, is_error=True)
        id = self.db_client.insert_one(self.db_name, "di_dag_commits", commit).inserted_id
        output, count = self.db_client.find_with_projection(self.db_name, "di_dag_commits",
                                                            query={"dag_id": dag_id},
                                                            projection={"version_number": 1, "_id": 0},
                                                            limit=100)
        versions = []
        for ver in output:
            versions.append(ver['version_number'])

        no_of_ds = 0
        no_of_dc = 0
        no_of_dm = 0

        for node in commit['dag']['nodes']:
            if node['type'] == 'DS':
                no_of_ds = no_of_ds + 1
            elif node['type'] == 'DM':
                no_of_dm = no_of_dm + 1
            elif node['type'] == 'DC':
                no_of_dc = no_of_dc + 1

        updated_nodes = {
            "nodes": commit['dag']['nodes'],
            "current_version": commit["version_number"],
            "versions": versions,
            "stats": {"no_of_datastores": no_of_ds,
                      "no_of_datamarts": no_of_dm,
                      "no_of_data_cartridges": no_of_dc}
        }

        output = self.db_client.find_one_and_update(self.db_name, "di_dags", {'di_dag_id': commit['dag_id']},
                                                 {'$set': updated_nodes})

        audit_data = {"action": "new version {} added for di pipeline {}".format(commit["version_number"], dag_id)}
        self.audit_service.add_audit_log(audit_data)
        del commit['_id']
        return to_json(commit)

    def list_commit(self, dag_id, limit, offset, version_number=None):
        query1 = {'dag_id': dag_id}
        if version_number is not None:
            query1 = {'dag_id': dag_id, 'version_number': version_number}

        result, count = self.db_client.find_with_projection(self.db_name, "di_dag_commits", query=query1,
                                                            projection={
                                                                "dag_id": 1, "version_number": 1,
                                                                "message": 1, "committed_datetime": 1,
                                                                "author": 1
                                                            },
                                                            offset=offset, limit=limit)
        output = []
        for commit in result:
            del commit['_id']
            output.append(commit)

        return to_list_json(output, list_count=count)

    def get_commit(self, dag_id, version_number):
        result, count = self.db_client.find(self.db_name, "di_dag_commits", query={'dag_id': dag_id,
                                                                               'version_number': version_number})
        output = {}
        for commit in result:
            del commit['_id']
            output = commit

        return output

    def initial_commit(self,dag_doc):
        id = self.db_client.insert_one(self.db_name, "di_dag_commits", dag_doc).inserted_id
        del dag_doc['_id']
        return to_json(dag_doc)
