from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger

from src.resources.di_dag import DataIngestionDagResource
from src.resources.di_dag_list import DataIngestionDagListResource
from src.resources.di_dag_exec import DataIngestionDagExecResource
from src.resources.di_dag_schedule import DataIngestionDagScheduleResource
from src.resources.di_dag_run import DataIngestionDagRunResource
from src.resources.di_dag_validate import DataIngestionDagValidateResource
from src.resources.di_dag_test import DIDagTestResource
from src.resources.di_dag_runs_details import DIDagRunsDetailsResource
from src.resources.di_dag_bulk import DIDagBulkResource

from src.resources.di_dataset_list import DataIngestionDatasetListResource
from src.resources.di_dataset import DataIngestionDatasetResource
from src.resources.di_dataset_stats import DatasetSampleRecordsResource
from src.resources.di_dataset_filter import DatasetFilterResource
from src.resources.di_dataset_bulk import DatasetBulkResource
from src.resources.di_dataset_refresh import DatasetRefreshResource
from src.resources.di_dataset_schema import DatasetSchemaResource

from src.resources.di_datamart_list import DataIngestionDatamartListResource
from src.resources.di_datamart import DataIngestionDatamartResource
from src.resources.di_datamart_create_table import CreateTableResource
from src.resources.di_datamart_bulk import DatamartBulkResource
from src.resources.di_datamart_refresh import DatamartRefreshResource

from src.resources.di_data_cartridge_list import DataIngestionDataCartridgeListResource
from src.resources.di_data_cartridge import DataIngestionDataCartridgeResource
from src.resources.di_data_cartridge_bulk import DataCartridgeBulkResource
from src.resources.di_data_cartridge_refresh import DataCartridgeRefreshResource

from src.resources.di_datastore_list import DataIngestionDataStoreListResource
from src.resources.di_datastore import DataIngestionDatastoreResource
from src.resources.di_datastore_bulk import DatastoreBulkResource

from src.resources.di_dag_commit_list import DIDagCommitListResource
from src.resources.di_dag_commit import DIDagCommitResource
from src.resources.di_refresh_metrics import MetricsRefreshResource


from src.resources.di_node_join import DataIngestionJoinNodeResource
from src.resources.di_node_sql import DataIngestionSQLNodeResource
from src.resources.di_dag_node_status import DataIngestionDagNodeStatusResource
from src.resources.di_interim_node import DataIngestionInterimNode

from src.resources.di_data_node import DataIngestionDataNode


from flask_cors import CORS
from src.utils.constants import con
from src.resources.admin import AdminResource
from src.utils.nlogger import logger
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask import request, g
from bat_auth_sdk import role_validator
import re

app = Flask(__name__)
CORS(app)
SECRETE_KEY = 'some-difficult-value'


@app.before_first_request
def global_init():
    logger.log().info("started initialising")

    logger.log().info("completed initialising")


@app.before_request
def validate_auth():
    if (not "/admin/_refresh" == request.path) and (not re.compile('/api/spec*').match(request.path)):
        check_auth()


@jwt_required
def check_auth():
    print("jwt token validated successfully")
    g.client_id = request.headers.get('clientid')
    g.app_id = request.headers.get('appid')
    g.user_name = request.headers.get('userName')
    g.user_id = request.headers.get('userid')
    print("serving request for client_id {} and app_id {}".format(request.headers.get('clientid'), request.headers.get('appid')))


api = swagger.docs(
    Api(app),
    apiVersion='0.1',
    basePath="http://localhost:5000/api/v0/",
    api_spec_url='/api/spec',
    description="docs for Teaching Layer API's",
    resourcePath="/dataIngestion")

api.add_resource(DataIngestionDagResource, '/di-dags/<string:di_dag_id>')
api.add_resource(DataIngestionDagExecResource, '/di-dags/<string:di_dag_id>/_exec')
api.add_resource(DataIngestionDagListResource, '/di-dags')
api.add_resource(DataIngestionDagScheduleResource, '/di-dags/<string:di_dag_id>/_sch')
api.add_resource(DIDagBulkResource, '/di-dags/_bulk')
api.add_resource(DataIngestionInterimNode, '/di-dags/nodes/<string:node_id>/<string:run_id>/data')

api.add_resource(DataIngestionDatasetResource, '/datasets/<string:dataset_id>')
api.add_resource(DataIngestionDatasetListResource, '/datasets/<string:source_id>/<string:source_type>')
api.add_resource(DatasetSampleRecordsResource, '/datasets/<string:source_id>/<string:dataset_id>/<string:source_type>/samples')
api.add_resource(DatasetFilterResource,'/datasets/<string:source_id>/<string:dataset_id>/<string:source_type>/filter')
api.add_resource(DatasetBulkResource, '/datasets/<string:source_id>/<string:source_type>/_bulk')
api.add_resource(DatasetRefreshResource, '/datasets/refresh/<string:source_id>/<string:source_type>')
api.add_resource(DatasetSchemaResource, '/datasets/<string:source_id>/<string:source_type>/<string:table_name>/schema')

api.add_resource(DataIngestionDatamartResource, '/datamarts/<string:datamart_id>')
api.add_resource(DataIngestionDatamartListResource, '/datamarts')
api.add_resource(CreateTableResource, '/datamarts/<string:datamart_id>/create_table')
api.add_resource(DatamartBulkResource, '/datamarts/_bulk')
api.add_resource(DatamartRefreshResource, '/datamarts/refresh')


api.add_resource(DataIngestionDataCartridgeResource, '/data-cartridges/<string:cartridge_id>')
api.add_resource(DataIngestionDataCartridgeListResource, '/data-cartridges')
api.add_resource(DataCartridgeBulkResource, '/data-cartridges/_bulk')
api.add_resource(DataCartridgeRefreshResource, '/data-cartridges/refresh')

api.add_resource(DataIngestionDataStoreListResource, '/datastores')
api.add_resource(DataIngestionDatastoreResource, '/datastores/<string:datastore_id>')
api.add_resource(DatastoreBulkResource, '/datastores/_bulk')


api.add_resource(DataIngestionJoinNodeResource, '/di-pipeline/nodes/join/<string:dataset1_id>/<string:dataset2_id>')
api.add_resource(DataIngestionSQLNodeResource, '/di-pipeline/nodes/sql/<string:dataset_id>')
api.add_resource(DataIngestionDataNode, '/dataingestion/node/data/<string:source_type>')
api.add_resource(DataIngestionDagNodeStatusResource, '/dataingestion/node/status/<string:dag_id>/<string:run_id>')

api.add_resource(DataIngestionDagRunResource, '/di-pipeline/dag/run/<string:dag_id>')
api.add_resource(DataIngestionDagValidateResource, '/di-pipeline/dag/validate')
api.add_resource(DIDagTestResource, '/di-pipeline/dag/<string:dataset_id>/test')
api.add_resource(DIDagRunsDetailsResource, '/di-pipeline/dag/<string:dag_id>/runs')

api.add_resource(DIDagCommitListResource, '/di-pipeline/commit/<string:dag_id>')
api.add_resource(DIDagCommitResource, '/di-pipeline/get_commit/<string:dag_id>')

api.add_resource(MetricsRefreshResource, '/metrics/refresh/<string:source_type>')

api.add_resource(AdminResource, '/admin/_refresh')

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['PROPAGATE_EXCEPTIONS'] = True

jwt = JWTManager(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=con.API_PORT, debug=True)