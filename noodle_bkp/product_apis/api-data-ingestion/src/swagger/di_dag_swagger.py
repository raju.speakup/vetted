from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class UIMetaData:
    resource_fields = {
        "x": fields.Integer,
        "y": fields.Integer
    }

@swagger.model
class ProjectionColumns:
    resource_fields = {
        "t1" : fields.List(fields.String),
        "t2" : fields.List(fields.String)
    }

@swagger.model
@swagger.nested(
    projection_columns=ProjectionColumns.__name__
)
class JoinNode:
    resource_fields = {
        "join_columns" : fields.List(fields.Raw),
        "projection_columns" : fields.Nested(ProjectionColumns.__name__)
    }

@swagger.model
class FilterNode:
    resource_fields = {
        "where_conditions" : fields.Raw
    }

@swagger.model
class DataNode:
    resource_fields = {
        "dataset_id": fields.String,
        "table_name": fields.String
    }

@swagger.model
class TransformNode:
    resource_fields = {
        "rules": fields.List(fields.Raw)
    }

@swagger.model
@swagger.nested(
    ui_metadata=UIMetaData.__name__
)
class Node:
    resource_fields = {
        "id" : fields.String,
        "name" : fields.String,
        "type" : fields.String,
        "parents" : fields.List(fields.String),
        "metadata": fields.Raw,
        "ui_metadata" : fields.Nested(UIMetaData.__name__)
    }

    swagger_metadata = {
        "metadata": {
            "enum": [JoinNode.__name__, DataNode.__name__, TransformNode.__name__, FilterNode.__name__]
        }
    }

@swagger.model
@swagger.nested(
    nodes=Node.__name__
)
class DIDagSwaggerModel:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'nodes':fields.List(fields.Nested(Node.resource_fields))
    }