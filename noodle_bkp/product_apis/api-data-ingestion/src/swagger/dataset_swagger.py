from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class DataSetSwaggerModel:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'ds_type': fields.String,
        'conn_id': fields.String,
        'table_name': fields.String
    }

@swagger.model
class BulkDelete:
    resource_fields = {
        'ids': fields.List(fields.String)
    }

@swagger.model
class Columns:
    resource_fields = {
        'column_name': fields.String,
        'data_type': fields.String,
        'allow_null': fields.String
    }

@swagger.model
@swagger.nested(
    columns=Columns.__name__
)
class TableSchema:
    resource_fields = {
        'table_name': fields.String,
        'description': fields.String,
        'columns': fields.Nested(Columns.__name__),
        'primary_keys': fields.List(fields.String)
    }

@swagger.model
@swagger.nested(
    new_table_schema=TableSchema.__name__
)
class DataSetPostSwaggerModel:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'source_type': fields.String,
        'source_id': fields.String,
        'table_name': fields.String,
        'is_columnar': fields.Boolean,
        'new_table_schema': fields.Nested(TableSchema.__name__)
    }

@swagger.model
class DataSetFilterSwaggerModel:
    resource_fields = {
        'filter_conditions': fields.List(fields.Raw)
    }

@swagger.model
class Rules:
    resource_fields = {
        "field": fields.String,
        "operator": fields.String,
        "value": fields.String,
        "nodeName": fields.String,
    }
@swagger.model
@swagger.nested(
    rules=Rules.__name__
)
class FilterConditions:
    resource_fields = {
        "combinator": fields.String,
        "nodeName": fields.String,
        "rules": fields.List(fields.Nested(Rules.__name__))
    }

@swagger.model
@swagger.nested(
    filter_conditions=FilterConditions.__name__
)
class DataSetSchema:
    resource_fields = {
        "filter_conditions": fields.Nested(FilterConditions.__name__)
    }