from flask_restful_swagger import swagger
from flask_restful import fields

@swagger.model
class CreateData:
    resource_fields = {
            "name": fields.String,
            "description": fields.String,
            "source_type": fields.String,
            "conn_id": fields.String,
        }

@swagger.model
class UpdateData:
    resource_fields = {
            "name": fields.String,
            "description": fields.String
        }