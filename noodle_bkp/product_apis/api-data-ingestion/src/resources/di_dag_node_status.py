from flask_restful import Resource
from src.services.di_node import DataIngestionNodeService
from src.services.dag_run import DataIngestionDagRunService
from src.services.dag_run import DagRun

from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger

class DataIngestionDagNodeStatusResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionNodeService(db_name)
        self.run_service = DataIngestionDagRunService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get Node status',
        notes="Get Node status by Dag ID and run ID",
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "dag Id"

            },
            {
                "name": "run_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "run Id"

            }
        ]
    )
    @role_validator.validate_app_user()
    #@validate_schema(id_path_schema(), is_path=True)
    def get(self, dag_id, run_id):
        dag_status = []
        nodes = self.run_service.get_dag_nodes(dag_id)
        if not nodes:
            return to_json({"message": "this pipeline is no longer available"}, is_error=True)
        for node in nodes:
            node_status = {}
            status = self.service.get_node_status(node['id'], run_id)
            if 'tracking_data' in status.keys():
                node_status.update({"node_id": node['id'],
                                    "status": status["status"],
                                    "preview": status["tracking_data"]["table_data"]
                                               if 'table_data' in status["tracking_data"].keys()
                                               else [],
                                    "schema": status["tracking_data"]["target_schema"]
                                               if 'target_schema' in status["tracking_data"].keys()
                                               else []}
                                  )
            else:
                node_status.update({"node_id": node['id'],
                                    "status": status["status"],
                                    "preview": [],
                                    "schema": []}
                                   )
            dag_status.append(node_status)
        return to_json(dag_status)
