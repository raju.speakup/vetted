# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_commit_service import DICommitService
from flask import request, g
import json
import datetime
from src.utils.config import conf
from src.utils.json_utils import to_json
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger
from swagger.di_dag_swagger import DIDagSwaggerModel

class DIDagCommitListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.commit_service = DICommitService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Update Dag',
        notes="Update Dag by ID",
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag Id"

            },
            {
                "name": "parameters",
                "dataType": DIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    def put(self, dag_id):
        data = json.loads(request.data.decode('utf-8'))

        message = request.args.get("message", "")
        version_number = request.args.get("version_number")
        author = request.headers.get('userName','unknown')

        item = {}
        item['dag_id'] = dag_id
        item['version_number'] = version_number
        item['author'] = author
        item['committed_datetime'] = str(datetime.datetime.utcnow())
        item['message'] = message
        item['dag'] = data

        if 'appid' in data:
            item['appid'] = str(data['appid'])
        else:
            item['appid'] = str(g.app_id)

        if 'clientid' in data:
            item['clientid'] = str(data['clientid'])
        else:
            item['clientid'] = str(g.client_id)

        self.commit_service.create_commit(dag_id, item)
        return to_json(item)

    #revert
    @role_validator.validate_app_user()
    def post(self, dag_id):
        version_number = request.args.get('revert_to_version_number')
        commit = self.commit_service.get_commit(dag_id, version_number)

        message = request.args.get("message", "")
        version_number = request.args.get("version_number", "v0")
        author = request.headers.get('userName','unknown')

        commit['version_number'] = version_number
        commit['author'] = author
        commit['committed_datetime'] = str(datetime.datetime.utcnow())
        commit['message'] = message
        commit['clientid'] = g.client_id
        commit['appid'] = g.app_id

        self.commit_service.create_commit(dag_id, commit)
        return to_json(commit)

    #list
    @swagger.operation(
        nickname='Get Dag',
        notes="Get Dag by ID",
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag Id"

            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "version_number",
                "dataType": 'int',
                "paramType": "query",
                "description": ""
            }
        ]
    )
    @role_validator.validate_app_user()
    def get(self, dag_id):
        limit = request.args.get("limit", "10")
        offset = request.args.get("offset", "0")
        version_number = request.args.get("version_number", None)
        history = self.commit_service.list_commit(dag_id, int(limit), int(offset), version_number)
        return history
