import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_dag import DataIngestionDagService
from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger


class DataIngestionDagExecResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDagService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='get exec di-dag',
        notes='Get exec  di-dag by Id ',
        parameters=[
            {
                "name": "di_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, di_dag_id):
        docs = self.service.get_dag(di_dag_id)

        node_exec_order = self.service.get_exec_order(docs['data'])

        for entry in node_exec_order:

            parents = self.service.get_parent_node(docs['data'],entry)
            for parent in parents:
                par_status=self.service.get_node_status(di_dag_id, parent)
                if par_status not in ('na','completed'):
                    print('parent node is still running')

            status = self.service.set_node_status(di_dag_id, entry, 'scheduled')
            node_type = self.service.get_node_type(di_dag_id, entry)

            output = self.service.exec_node(di_dag_id, entry, node_type)

            status = self.service.set_node_status(di_dag_id, entry, 'in-progress')



            children = self.service.get_child_node(docs['data'],entry)
            status = self.service.get_node_status(di_dag_id, entry)



        return node_exec_order
