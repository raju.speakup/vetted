import datetime
import uuid
from flask_restful import Resource
from flask import request

import json
from src.services.di_datasets import DataIngestionDatasetService
from src.services.dag_run import DataIngestionDagRunService
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json
from src.schemas.di_dataset import id_path_schema
from src.services.node_status import NodeStatus
from src.services.dag_run import DagRun

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.di_dag_swagger import DIDagSwaggerModel


class DIDagTestResource(Resource):
    """DataSet Schema ."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.dataset_service = DataIngestionDatasetService(db_name)
        self.service = DataIngestionDagRunService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='DagTest',
        notes="DagTest by dataset_id ID",
        parameters=[
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "dataset Id"

            },
            {
                "name": "parameters",
                "dataType": DIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    #@validate_schema(id_path_schema(), is_path=True)
    def post(self, dataset_id):
        dag = json.loads(request.data.decode('utf-8'))

        pg_meta = self.service.get_db_details_from_dataset(dataset_id)
        if pg_meta:
            run_service = DagRun(pg_meta)
        else:
            return to_json({"message": "postgres server used in this pipeline does not exist"}, is_error=True)

        if 'current_verion' in dag.keys():
            version = dag['current_verion']
        else:
            version = 'NA'

        run_id = str(uuid.uuid4())
        dag_id = str(uuid.uuid4())
        responses = []
        nodes = dag['nodes']
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata']
            }
            )
            if node['type'] in ("DS", "DC", "DM"):
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.save_test_dag(node_doc)
            responses.append(resp)
        start_time = str(datetime.datetime.now())
        output = run_service.run_dag(run_id, dag)
        end_time = str(datetime.datetime.now())
        if output is True:
            run_service.remove_temp_data(run_id)
            run_service.add_dag_run(dag_id, run_id, start_time, end_time, version, status='C')
            limit = request.args.get("limit", 100)
            table_name, source_id = self.dataset_service.get_dataset_info(dataset_id)
            preview = self.dataset_service.preview_table_records(source_id, table_name, 'data_mart', False, limit)
            return to_json(preview)
        else:
            run_service.remove_temp_data(run_id)
            run_service.add_dag_run(dag_id, run_id, start_time, end_time, version, status='F')
            return to_json({"message": output}, is_error=True)




