# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request, g

from src.utils.json_utils import to_json
from src.services.di_datasets import DataIngestionDatasetService
from src.services.di_datastore import DatastoreService
from src.services.di_datamart import DataIngestionDataMartService
from src.services.di_data_cartridge import DataIngestionDataCartridgeService

from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import refresh_schema
from src.utils.config import conf

from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger

class DataIngestionDataNode(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.datastore_service = DatastoreService(db_name)
        self.datamart_service = DataIngestionDataMartService(db_name)
        self.data_cartridge_service = DataIngestionDataCartridgeService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "source type"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ],
        notes='Get DataNode List with limits and offset',
        nickname='Get DataNode')
    @role_validator.validate_app_user()
    @validate_schema(refresh_schema(), is_path=True)
    def get(self, source_type):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")

        dataset_list = {}
        if source_type == 'data_store':
            dataset_list = self.datastore_service.get_dataset_list(limit, offset, keyword)
        elif source_type == 'data_mart':
            dataset_list = self.datamart_service.get_dataset_list(limit, offset, keyword)
        elif source_type == 'data_cartridge':
            dataset_list = self.data_cartridge_service.get_dataset_list(limit, offset, keyword)
        else:
            return to_json({"message": "incorrect source_type, allowed: [data_store, data_mart, data_cartridge]"},
                           is_error=True)

        return dataset_list