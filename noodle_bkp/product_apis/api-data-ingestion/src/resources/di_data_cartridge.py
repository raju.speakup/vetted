import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_data_cartridge import DataIngestionDataCartridgeService
from src.utils.json_validator import validate_schema
from src.schemas.di_data_cartridge import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from  flask_restful_swagger import swagger
from  swagger.data_swagger import UpdateData

class DataIngestionDataCartridgeResource(Resource):
    """DataCartridge."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDataCartridgeService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get DataCartridge',
        notes='Get DataCartridge',
        parameters=[
            {
                "name": "cartridge_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "cartridge Id"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, cartridge_id):
        output = self.service.get_data_cartridge(cartridge_id)
        return output

    @swagger.operation(
        nickname='Delete DataCartridge',
        notes='Delete DataCartridge',
        parameters=[
            {
                "name": "cartridge_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "cartridge Id"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, cartridge_id):
        output = self.service.delete__data_cartridge(cartridge_id)
        return output

    @swagger.operation(
        nickname='Update DataCartridge',
        notes='Update DataCartridge by Id ',
        parameters=[
            {
                "name": "cartridge_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Cartridge ID"

            },
            {
                "name": "parameter",
                "dataType": UpdateData.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, cartridge_id):

        data = json.loads(request.data.decode('utf-8'))
        updated_by = request.headers.get('userName')
        data['updated_at'] = str(datetime.datetime.utcnow())
        data['updated_by'] = 'unknown'
        if updated_by:
            data['updated_by'] = updated_by

        new_doc = data

        output = self.service.update_data_cartridge(cartridge_id, new_doc)
        return output

