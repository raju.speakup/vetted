# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.json_validator import validate_schema
from src.schemas.di_datamart import id_path_schema
from src.utils.config import conf
from flask import request
import json
import datetime
from bat_auth_sdk import role_validator


class DatasetBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @role_validator.validate_app_user()
    #@validate_schema(id_path_schema(), is_path=True)
    def delete(self, source_id, source_type):
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        datasets = self.service.bulk_delete_dataset(source_id, source_type, ids)
        return datasets
