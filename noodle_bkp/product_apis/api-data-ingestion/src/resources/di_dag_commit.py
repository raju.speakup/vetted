# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_commit_service import DICommitService
from flask import request, g
import json
import datetime
from src.utils.config import conf
from src.utils.json_utils import to_json
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger

class DIDagCommitResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.commit_service = DICommitService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get Dag',
        notes="Get Dag by ID",
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag Id"

            },
            {
                "name": "version_number",
                "dataType": 'int',
                "paramType": "query",
                "description": ""
            }
        ]
    )
    @role_validator.validate_app_user()
    def get(self, dag_id):
        version_number = request.args.get("version_number", None)
        output = self.commit_service.get_commit(dag_id, version_number)

        return to_json(output)
