import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.json_validator import validate_schema
from src.schemas.di_dataset import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.dataset_swagger import DataSetSwaggerModel

class DataIngestionDatasetResource(Resource):
    """DataSet."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='get dataset',
        notes='Get dataset by Id ',
        parameters=[
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, dataset_id):
        output = self.service.get_dataset(dataset_id)
        return output

    @swagger.operation(
        nickname='Delete',
        notes='Delete dataset by Id ',
        parameters=[
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, dataset_id):
        output = self.service.delete_dataset(dataset_id)
        return output

    @swagger.operation(
        nickname='Update',
        notes='Update dataset by Id ',
        parameters=[
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"

            },{
                "name": "dataset",
                "dataType":  DataSetSwaggerModel.__name__,
                "paramType": 'body',
                "required": True,
                "description": "Dataset"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, dataset_id):

        data = json.loads(request.data.decode('utf-8'))
        updated_by = request.headers.get('userName')
        data['updated_at'] = str(datetime.datetime.utcnow())
        data['updated_by'] = 'unknown'
        if updated_by:
            data['updated_by'] = updated_by

        new_doc = data

        output = self.service.update_dataset(dataset_id, new_doc)
        return output

