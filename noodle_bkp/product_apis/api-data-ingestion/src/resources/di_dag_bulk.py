import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
from src.utils.json_validator import validate_schema

from src.schemas.di_pipeline import delete_schema

from bat_auth_sdk import role_validator
from src.services.di_dag import DataIngestionDagService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.dataset_swagger import BulkDelete

class DIDagBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDagService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Delete DI Dag',
        notes='Delete DI Dag by Id ',
        parameters=[
            {
                "name": "di_dag_ids",
                "dataType": BulkDelete.__name__,
                "paramType": "body",
                "required": True,
                "description": "di dag IDs"

            }
        ]
    )
    # @role_validator.validate_app_user()
    @validate_schema(delete_schema())
    def delete(self):
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        output = self.service.bulk_delete_dag(ids)
        return output

