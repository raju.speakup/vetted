import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.services.dag_run import DataIngestionDagRunService
from src.services.di_dag import DataIngestionDagService
from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import id_path_schema
from src.services.node_status import NodeStatus
from src.services.dag_run import DagRun
from flask import request, g
from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from celery_tasks import celery
from src.services.audit import AuditService

class DataIngestionDagRunResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDagRunService(db_name)
        self.dag_service = DataIngestionDagService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)
        self.sign = conf.get_v('DI_CELERY_SIGN')
        self.audit_service = AuditService(db_name)

    @role_validator.validate_app_user()
    #@validate_schema(id_path_schema(), is_path=True)
    def post(self, dag_id):
        payload = json.loads(request.data.decode('utf-8'))
        if 'current_version' in payload.keys():
            if payload['current_version'] != '':
                run_version = payload['current_version']
            else:
                run_version = 'current_version'
        else:
            run_version = 'current_version'

        dag = self.dag_service.get_dag_version(dag_id, run_version)

        pg_meta = self.service.get_db_details_from_dag_id(dag_id, run_version)
        if pg_meta:
            run_service = DagRun(pg_meta)
        else:
            return to_json({"message": "postgres server used in this pipeline does not exist"}, is_error=True)

        if run_version == 'current_version':
            version = dag['current_version']
        else:
            version = run_version

        run_id = str(uuid.uuid4())
        responses = []
        #nodes = self.service.get_dag_nodes(dag_id)
        nodes = dag['nodes']
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata']
            }
            )
            if node['type'] in ("DS", "DC", "DM"):
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.save_test_dag(node_doc)
            responses.append(resp)
        start_time = str(datetime.datetime.now())
        # output = self.run_service.run_dag(run_id, dag)

        if self.service.check_dag_running(dag_id):
            return to_json({"message": "Already running"}, is_error=True)

        self.service.set_run_doc(run_id, dag_id, start_time, version, dag, pg_meta, g.app_id, g.client_id)
        self.service.set_dag_run(dag_id)

        sig = celery.signature(self.sign)
        sig.apply_async(
            args=[run_id, dag_id, dag, start_time, version, pg_meta],
            task_id=run_id,
            queue='di_queue_1'
        )

        audit_data = {'action': 'DI pipeline {} is triggered for an ad-hoc run'.format(dag['name'])}
        self.audit_service.add_audit_log(audit_data)

        sch_details = {"dag_id": dag_id, "run_id": run_id}
        return to_json(sch_details)


