import datetime
import uuid

from flask_restful import Resource
from flask import request, g

from src.utils.json_utils import to_json
from src.utils.json_validator import validate_schema

from src.schemas.di_pipeline import refresh_schema
from src.utils.mongo_db import db_client

from bat_auth_sdk import role_validator
from src.services.di_datasets import DataIngestionDatasetService
from src.services.di_datastore import DatastoreService
from src.services.di_datamart import DataIngestionDataMartService
from src.services.di_data_cartridge import DataIngestionDataCartridgeService
from src.services.audit import AuditService

from src.utils.config import conf
from flask_restful_swagger import swagger

class MetricsRefreshResource(Resource):

    def __init__(self):
        self.db_name = conf.get_v('db.name')
        self.dataset_service = DataIngestionDatasetService(self.db_name)
        self.datastore_service = DatastoreService(self.db_name)
        self.datamart_service = DataIngestionDataMartService(self.db_name)
        self.data_cartridge_service = DataIngestionDataCartridgeService(self.db_name)
        self.mongo_client = db_client
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, self.db_name)
        self.audit_service = AuditService(self.db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "source type"

            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ],
        notes='Get Metrics List with limits and offset',
        nickname='Get Metrics')
    @role_validator.validate_app_user()
    @validate_schema(refresh_schema(), is_path=True)
    def get(self, source_type):
        limit = request.args.get("limit", 10000)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")

        return_json = {}
        if source_type == 'data_store':
            distinct_conns = self.mongo_client.distinct(self.db_name, "di_datastores", key='conn_id',
                                                        query={'appid':g.app_id, 'clientid': g.client_id})
            for conn in distinct_conns:
                pg_conn = self.datastore_service.get_target_db_connection(conn)
                if not pg_conn:
                    continue
                source_list, count = self.mongo_client.find_with_projection(self.db_name, "di_datastores",
                                                                            query={"conn_id": conn},
                                                                            projection={"datastore_id": 1},
                                                                            limit=100)
                for source in source_list:
                    if 'datastore_id' in source.keys():
                        source_id = source['datastore_id']
                    else:
                        continue
                    output = self.dataset_service.refresh_datasets_with_conn(source_id, source_type, g.client_id,
                                                                             g.app_id, conn, pg_conn)

                    try:
                        self.dataset_service.refresh_dataset_cols(source_id, source_type, g.client_id,
                                                                             g.app_id, conn, pg_conn)
                    except Exception as e:
                        print(e)

                    no_of_datasets = len(output['data'])
                    no_of_rows = 0
                    size = 0
                    if output['data']:
                        for metric in output['data']:
                            no_of_rows += metric['metrics']['no_of_rows']
                            size += metric['metrics']['size']
                    metrics = {
                        "metrics": {
                            "no_of_rows": no_of_rows,
                            "no_of_datasets": no_of_datasets,
                            "size": size
                        }
                    }
                    self.datastore_service.update_datastore(source_id, metrics)
                    self.datastore_service.refresh_table_list_with_conn(source_id, conn, pg_conn)

        elif source_type == 'data_mart':
            distinct_conns = self.mongo_client.distinct(self.db_name, "di_datamarts", key='conn_id', query={})
            for conn in distinct_conns:
                pg_conn = self.datamart_service.get_target_db_connection(conn)
                if not pg_conn:
                    continue
                source_list, count = self.mongo_client.find_with_projection(self.db_name, "di_datamarts",
                                                                            query={"conn_id": conn},
                                                                            projection={"datamart_id": 1},
                                                                            limit=100)
                for source in source_list:
                    source_id = source['datamart_id']
                    output = self.dataset_service.refresh_datasets_with_conn(source_id, source_type, g.client_id,
                                                                             g.app_id, conn, pg_conn)

                    try:
                        self.dataset_service.refresh_dataset_cols(source_id, source_type, g.client_id,
                                                                             g.app_id, conn, pg_conn)
                    except Exception as e:
                        print(e)

                    no_of_datasets = len(output['data'])
                    no_of_rows = 0
                    size = 0
                    if output['data']:
                        for metric in output['data']:
                            no_of_rows += metric['metrics']['no_of_rows']
                            size += metric['metrics']['size']
                    metrics = {
                        "metrics": {
                            "no_of_rows": no_of_rows,
                            "no_of_datasets": no_of_datasets,
                            "size": size
                        }
                    }
                    self.datamart_service.update_datamart(source_id, metrics)
                    self.datamart_service.refresh_table_list_with_conn(source_id, conn, pg_conn)
        elif source_type == 'data_cartridge':
            distinct_conns = self.mongo_client.distinct(self.db_name, "di_data_cartridges", key='conn_id', query={})
            for conn in distinct_conns:
                pg_conn = self.data_cartridge_service.get_target_db_connection(conn)
                if not pg_conn:
                    continue
                source_list, count = self.mongo_client.find_with_projection(self.db_name, "di_data_cartridges",
                                                                            query={"conn_id": conn},
                                                                            projection={"cartridge_id": 1},
                                                                            limit=100)
                for source in source_list:
                    source_id = source['cartridge_id']
                    output = self.dataset_service.refresh_datasets_with_conn(source_id, source_type, g.client_id,
                                                                             g.app_id, conn, pg_conn)
                    try:
                        self.dataset_service.refresh_dataset_cols(source_id, source_type, g.client_id,
                                                                             g.app_id, conn, pg_conn)
                    except Exception as e:
                        print(e)

                    no_of_datasets = len(output['data'])
                    no_of_rows = 0
                    size = 0
                    if output['data']:
                        for metric in output['data']:
                            no_of_rows += metric['metrics']['no_of_rows']
                            size += metric['metrics']['size']
                    metrics = {
                        "metrics": {
                            "no_of_rows": no_of_rows,
                            "no_of_datasets": no_of_datasets,
                            "size": size
                        }
                    }
                    self.data_cartridge_service.update_data_cartridge(source_id, metrics)
                    self.data_cartridge_service.refresh_table_list_with_conn(source_id, conn, pg_conn)

        else:
            return to_json({"message": "incorrect source_type, allowed: [data_store, data_mart, data_cartridge]"},
                           is_error=True)

        audit_data = {'action': 'Metrics refresh complete'}
        self.audit_service.add_audit_log(audit_data)

        return to_json(return_json)
