# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request, g

from src.utils.json_utils import to_json
from src.services.di_node import DataIngestionNodeService

from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import refresh_schema
from src.utils.config import conf

from bat_auth_sdk import role_validator


class DataIngestionInterimNode(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionNodeService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @role_validator.validate_app_user()
    def get(self, node_id, run_id):
        schema, preview = self.service.get_node_data(node_id, run_id)
        output = {'schema': schema,
                  'preview': preview}
        return to_json(output)
