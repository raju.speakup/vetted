import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json

from src.schemas.di_dataset import list_arg_schema, dataset_schema

from bat_auth_sdk import role_validator
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.dataset_swagger import DataSetPostSwaggerModel

class DataIngestionDatasetListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='get dataset',
        notes='Get dataset by Id and Type',
        parameters=[
            {
                "name": "source_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            },{
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset Type"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ])
    @role_validator.validate_app_user()
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self, source_id, source_type):
        limit = request.args.get("limit", 100)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")

        output = self.service.list_datasets(source_id, source_type, limit, offset, keyword)
        return output

    @swagger.operation(
        nickname='Post dataset',
        notes='Post dataset by Id and Type',
        parameters=[
            {
                "name": "source_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            },
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset Type"
            },
            {
                "name": "parameters",
                "dataType": DataSetPostSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": "Dataset Type"
            }
        ])
    @role_validator.validate_app_user()
    @validate_schema(dataset_schema())
    def post(self, source_id, source_type):
        data = json.loads(request.data.decode('utf-8'))

        new_doc = data

        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_at'] = str(datetime.datetime.utcnow())

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)

        output, status_code = self.service.create_dataset(new_doc)
        return output, status_code
