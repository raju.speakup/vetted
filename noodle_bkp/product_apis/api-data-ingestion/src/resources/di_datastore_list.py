# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request, g
import datetime

from src.services.di_datastore import DatastoreService
from src.utils.json_validator import validate_schema
from src.schemas.di_datastore_schema import datastore_schema, list_arg_schema
from src.utils.config import conf
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger
from swagger.data_swagger import CreateData

class DataIngestionDataStoreListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DatastoreService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get DataStoreList',
        notes='Get DataStoreList',
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ])
    @role_validator.validate_app_user()
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 100)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        datastore_list = self.service.list_datastore(limit, offset, keyword)
        return datastore_list

    @swagger.operation(
        nickname='Create DataStore Resource',
        notes='Create DataStore Resource',
        parameters=[
            {
                "name": "parameter",
                "dataType": CreateData.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(datastore_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        if 'appid' in data:
            data['appid'] = str(data['appid'])
        else:
            data['appid'] = str(g.app_id)

        if 'clientid' in data:
            data['clientid'] = str(data['clientid'])
        else:
            data['clientid'] = str(g.client_id)

        output = self.service.create_datastore(data)
        if 'tables' in output['data']:
            del output['data']['tables']
            del output['data']['datasets']
        return output, 201

