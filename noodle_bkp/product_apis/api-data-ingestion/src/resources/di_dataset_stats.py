# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json
from src.schemas.di_dataset import id_stats_schema
from src.utils.config import conf
from flask import request
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger

class DatasetSampleRecordsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "source_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "source_id"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "dataset_id"
            },
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "source_type"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            }
        ],
        notes='Get Dataset samples with limits and offset',
        nickname='Get Dataset samples')
    @role_validator.validate_app_user()
    @validate_schema(id_stats_schema(), is_path=True)
    def get(self, source_id, dataset_id, source_type):
        """ get dataset sample records"""
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        records = self.service.list_dataset_records(source_id, dataset_id, source_type, limit)
        return to_json(records)

    @swagger.operation(
        parameters=[
            {
                "name": "source_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "source_id"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "dataset_id"
            },
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "source_type"
            }
        ],
        notes='Post Dataset samples with limits and offset',
        nickname='Post Dataset samples')
    @role_validator.validate_app_user()
    @validate_schema(id_stats_schema(), is_path=True)
    def post(self, source_id, dataset_id, source_type):
        """ get dataset stats"""
        output = self.service.get_dataset_stats(source_id, dataset_id, source_type)

        return to_json(output)
