import datetime
import uuid

from flask_restful import Resource
from flask import request, g

import json
from src.utils.json_validator import validate_schema

from src.schemas.di_datamart import list_arg_schema, datamart_schema

from bat_auth_sdk import role_validator
from src.services.di_datamart import DataIngestionDataMartService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.data_swagger import CreateData

class DataIngestionDatamartListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDataMartService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ],
        notes='Get Datamart List with limits and offset',
        nickname='Get Datamart')
    @role_validator.validate_app_user()
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 100)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        output = self.service.list_datamart(limit, offset, keyword)
        return output

    @swagger.operation(
        nickname='Create datamart',
        notes='Create Datamart',
        parameters=[
            {
                "name": "parameter",
                "dataType": CreateData.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(datamart_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        new_doc = data

        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_at'] = str(datetime.datetime.utcnow())

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)

        output = self.service.create_datamart(new_doc)
        return output, 201
