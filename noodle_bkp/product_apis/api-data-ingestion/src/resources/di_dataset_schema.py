import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json
from src.schemas.di_dataset import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.dataset_swagger import DataSetSchema

class DatasetSchemaResource(Resource):
    """DataSet Schema ."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Create Dataset Schema',
        notes='Create Dataset Schema Resource',
        parameters=[
            {
                "name": "source_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Source Id"
            },
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Source Type"
            },
            {
                "name": "table_name",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Table name"
            },
            {
                "name": "parameter",
                "dataType": DataSetSchema.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ]
    )
    #@role_validator.validate_app_user()
    #@validate_schema(id_path_schema(), is_path=True)
    def post(self, source_id, source_type, table_name):
        data = json.loads(request.data.decode('utf-8'))
        filters = data['filter_conditions']

        limit = request.args.get("limit", 200)
        output = {}
        schema = self.service.get_table_schema(source_id, source_type, table_name)
        if not schema:
            return to_json({"message": "table doesn't exist"}, is_error=True)
        preview = self.service.preview_table_records(source_id, table_name, source_type, filters, limit)
        output.update({"schema": schema,
                       "preview": preview
                       })
        return output


