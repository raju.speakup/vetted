import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_datastore import DatastoreService
from src.utils.json_validator import validate_schema
from src.schemas.di_datastore_schema import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.data_swagger import UpdateData

class DataIngestionDatastoreResource(Resource):
    """DataStore."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.service = DatastoreService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get DataStore by ID',
        notes='Get DataStore by ID',
        parameters=[
            {
                "name": "datastore_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Datastore ID"

            }
        ])
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, datastore_id):
        output = self.service.get_datastore(datastore_id)
        return output

    @swagger.operation(
        nickname='delete datastore by id',
        notes='Delete datastore by Id ',
        parameters=[
            {
                "name": "datastore_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "datastore Id"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, datastore_id):
        output = self.service.delete_datastore(datastore_id)
        return output

    @swagger.operation(
        nickname='Update DatastoreResource',
        notes='Update DatastoreResource by Id ',
        parameters=[
            {
                "name": "datastore_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Datamart ID"

            },
            {
                "name": "parameter",
                "dataType": UpdateData.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, datastore_id):

        data = json.loads(request.data.decode('utf-8'))
        updated_by = request.headers.get('userName')
        data['updated_at'] = str(datetime.datetime.utcnow())
        data['updated_by'] = 'unknown'
        if updated_by:
            data['updated_by'] = updated_by

        new_doc = data

        output = self.service.update_datastore(datastore_id, new_doc)
        return output

