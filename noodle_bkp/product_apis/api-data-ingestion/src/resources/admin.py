# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
import uuid
import json
from flask import request
import logging
import time

from src.utils.constants import Const
from src.utils.config import conf
from src.utils.mongo_db import db_client
from src.utils.nlogger import logger


class AdminResource(Resource):

    def get(self):
        logger.log().info("refresh completed at " + time.asctime())
        conf.reload()
        db_client.reconnect()
        response = {'status': 'success',
                    'db_name': db_client.url,
                    'is_db_connected': db_client.is_connected(),
                    'log_name': logger.log().getLogger().name,
                    'log_level': logger.log().getLogger().getEffectiveLevel()}
        return response
