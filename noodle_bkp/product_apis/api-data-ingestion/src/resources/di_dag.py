import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_dag import DataIngestionDagService
from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.di_dag_swagger import DIDagSwaggerModel

class DataIngestionDagResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)
        self.service = DataIngestionDagService(db_name)


    @swagger.operation(
        nickname='get di-dag',
        notes='Get di-dag by Id ',
        parameters = [
            {
                "name": "di_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag ID"

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, di_dag_id):
        """ Get Dag By Dag Id"""
        output = self.service.get_dag(di_dag_id)
        return output


    @swagger.operation(
        nickname='delete di-dag',
        notes='Delete di-dag by Id ',
        parameters=[
            {
                "name": "di_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag Id"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, di_dag_id):
        """ Delete Dag By Dag Id"""
        output = self.service.delete_dag(di_dag_id)
        return output

    @swagger.operation(
        nickname='Update Dag di-dag',
        notes="Update Dag by Dag ID",
        parameters=[
            {
                "name": "di_dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag Id"

            },
            {
                "name": "parameters",
                "dataType": DIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, di_dag_id):
        """ Update Dag By Dag Id """
        data = json.loads(request.data.decode('utf-8'))
        updated_by = request.headers.get('userName')
        data['updated_at'] = str(datetime.datetime.utcnow())
        data['updated_by'] = 'unknown'
        if updated_by:
            data['updated_by'] = updated_by

        new_doc = data

        no_of_ds = 0
        no_of_dc = 0
        no_of_dm = 0
        for node in new_doc['nodes']:
            if node['type'] == 'DS':
                no_of_ds = no_of_ds + 1
            elif node['type'] == 'DM':
                no_of_dm = no_of_dm + 1
            elif node['type'] == 'DC':
                no_of_dc = no_of_dc + 1

        new_doc['stats'] = {"no_of_datastores": no_of_ds,
                            "no_of_datamarts": no_of_dm,
                            "no_of_data_cartridges": no_of_dc}

        output = self.service.update_dag(di_dag_id, new_doc)
        return output
