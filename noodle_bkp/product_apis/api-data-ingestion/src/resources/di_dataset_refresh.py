import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.utils.json_validator import validate_schema

from src.schemas.di_dataset import refresh_datasets_schema

from bat_auth_sdk import role_validator
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.config import conf
from flask_restful_swagger import swagger

class DatasetRefreshResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Refresh Dataset',
        notes='Refresh Dataset',
        parameters=[{
                "name": "source_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "source_id"
            },
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "source_type"
            }]
    )
    @role_validator.validate_app_user()
    @validate_schema(refresh_datasets_schema(), is_path=True)
    def get(self, source_id, source_type):
        output = self.service.refresh_datasets(source_id, source_type)
        return output
