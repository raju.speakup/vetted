from flask_restful import Resource
from flask import request

from src.services.dag_run import DataIngestionDagRunService

from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger

class DIDagRunsDetailsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.dag_service = DataIngestionDagRunService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='get Dag run details',
        notes='Get Dag run details by Id ',
        parameters=[
            {
                "name": "dag_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "dag ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    def get(self, dag_id):
        output = self.dag_service.get_runs_details(dag_id)
        return to_json(output)
