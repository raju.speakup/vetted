# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_datamart import DataIngestionDataMartService
from src.utils.json_validator import validate_schema
from src.schemas.di_datamart import id_path_schema
from src.utils.config import conf
from flask import request
import json
import datetime
from bat_auth_sdk import role_validator
from  flask_restful_swagger import swagger
from swagger.dataset_swagger import BulkDelete

class DatamartBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDataMartService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Delete Datamart',
        notes='Bulk Delete Datamart',
        parameters=[
            {
                "name": "di_dag_ids",
                "dataType": BulkDelete.__name__,
                "paramType": "body",
                "required": True,
                "description": "di dag IDs"

            }
        ]
    )
    @role_validator.validate_app_user()
    #@validate_schema(id_path_schema(), is_path=True)
    def delete(self):
        data = json.loads(request.data.decode('utf-8'))
        ids = data['ids']
        datamart = self.service.bulk_delete_datamart(ids)
        return datamart
