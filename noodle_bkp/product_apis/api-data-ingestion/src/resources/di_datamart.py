import datetime

from flask_restful import Resource
from flask import request

import json
from src.services.di_datamart import DataIngestionDataMartService
from src.utils.json_validator import validate_schema
from src.schemas.di_datamart import id_path_schema

from bat_auth_sdk import role_validator
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.data_swagger import UpdateData

class DataIngestionDatamartResource(Resource):
    """DataSet."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDataMartService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get datamart',
        notes='Get Datamart by Id ',
        parameters=[
            {
                "name": "datamart_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Datamart ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, datamart_id):
        output = self.service.get_datamart(datamart_id)
        return output

    @swagger.operation(
        nickname='Delete datamart',
        notes='Delete Datamart by Id ',
        parameters=[
            {
                "name": "datamart_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Datamart ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, datamart_id):
        output = self.service.delete_datamart(datamart_id)
        return output

    @swagger.operation(
        nickname='Update datamart',
        notes='Update Datamart by Id ',
        parameters=[
            {
                "name": "datamart_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Datamart ID"

            },
            {
                "name": "parameter",
                "dataType": UpdateData.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, datamart_id):

        data = json.loads(request.data.decode('utf-8'))
        updated_by = request.headers.get('userName')
        data['updated_at'] = str(datetime.datetime.utcnow())
        data['updated_by'] = 'unknown'
        if updated_by:
            data['updated_by'] = updated_by

        new_doc = data

        output = self.service.update_datamart(datamart_id, new_doc)
        return output

