import datetime
import uuid

from flask_restful import Resource
from flask import request,g

import json
from src.utils.json_validator import validate_schema

from src.schemas.di_pipeline import list_arg_schema, dag_schema, delete_schema

from bat_auth_sdk import role_validator
from src.services.di_dag import DataIngestionDagService
from src.services.di_commit_service import DICommitService
from src.utils.config import conf
from flask_restful_swagger import swagger
from swagger.di_dag_swagger import DIDagSwaggerModel


class DataIngestionDagListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionDagService(db_name)
        self.commit_service = DICommitService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ],
        notes='Get DI Dag List with limits and offset',
        nickname='Get DI Dag')
    @role_validator.validate_app_user()
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        """ Get all list of Dag's"""
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        output = self.service.list_dag(limit, offset, keyword)
        return output

    @swagger.operation(
        notes='Create Dag',
        nickname='Create Dag',
        parameters=[{
                "name": "parameters",
                "dataType": DIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }]
    )
    @role_validator.validate_app_user()
    @validate_schema(dag_schema())
    def post(self):
        " Create Dag"
        data = json.loads(request.data.decode('utf-8'))

        new_doc = data

        if 'appid' in data:
            new_doc['appid'] = str(data['appid'])
        else:
            new_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['clientid'] = str(data['clientid'])
        else:
            new_doc['clientid'] = str(g.client_id)

        no_of_ds = 0
        no_of_dc = 0
        no_of_dm = 0

        for node in new_doc['nodes']:
            if node['type'] == 'DS':
                no_of_ds = no_of_ds + 1
            elif node['type'] == 'DM':
                no_of_dm = no_of_dm + 1
            elif node['type'] == 'DC':
                no_of_dc = no_of_dc + 1

        new_doc['stats'] = {"no_of_datastores": no_of_ds,
                            "no_of_datamarts": no_of_dm,
                            "no_of_data_cartridges": no_of_dc}

        new_doc['di_dag_id'] = str(uuid.uuid4())
        new_doc['created_at'] = str(datetime.datetime.utcnow())
        new_doc['created_by'] = request.headers.get('userName','unknown')
        new_doc['updated_at'] = str(datetime.datetime.utcnow())
        new_doc['updated_by'] = request.headers.get('userName', 'unknown')
        new_doc['current_version'] = 'v0'
        new_doc['versions'] = ['v0']

        commit_doc ={
            "dag_id": new_doc['di_dag_id'],
            "version_number": "v0",
            "author": request.headers.get('userName', 'unknown'),
            "updated_by": request.headers.get('userName', 'unknown'),
            "committed_datetime": str(datetime.datetime.utcnow()),
            "message": "initial commit",
            "dag": new_doc

        }

        output, status_code = self.service.create_dag(new_doc)
        commit = self.commit_service.initial_commit(commit_doc)
        return output, status_code
