import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json

from src.schemas.di_dataset import list_arg_schema, dataset_schema

from bat_auth_sdk import role_validator
from src.services.di_node import DataIngestionNodeService
from src.utils.config import conf
from flask_restful_swagger import swagger

class DataIngestionJoinNodeResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataIngestionNodeService(db_name)
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)

    @swagger.operation(
        nickname='Get JoinNodeResource',
        notes="Get JoinNodeResource by dataset's id ",
        parameters=[
            {
                "name": "dataset1_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag ID"

            },
            {
                "name": "dataset2_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "di dag ID"

            }
        ]
    )
    @role_validator.validate_app_user()
    #@validate_schema(list_arg_schema(), is_arg=True)
    def get(self, dataset1_id, dataset2_id):
        output = self.service.get_join_node(dataset1_id, dataset2_id)
        return to_json(output)