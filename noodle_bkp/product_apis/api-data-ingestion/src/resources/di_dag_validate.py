import datetime
import uuid

from flask_restful import Resource
from flask import request

import json
from src.utils.json_validator import validate_schema
from src.schemas.di_pipeline import id_path_schema
from src.services.node_status import NodeStatus

from bat_auth_sdk import role_validator
from src.utils.config import conf
from src.utils.json_utils import to_json
from flask_restful_swagger import swagger
from swagger.di_dag_swagger import DIDagSwaggerModel
from src.services.dag_run import DataIngestionDagRunService

class DataIngestionDagValidateResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, db_name)
        self.service = DataIngestionDagRunService(db_name)

    @swagger.operation(
        nickname='DagValidateResource',
        notes="DagValidateResource",
        parameters=[
            {
                "name": "parameters",
                "dataType": DIDagSwaggerModel.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    def post(self):
        dag = json.loads(request.data.decode('utf-8'))

        # responses = []
        # nodes = dag['nodes']
        #
        # if nodes[-1]['type'] != 'DM':
        #     return to_json({"message": "Data Mart should be the last node in a pipeline"}, is_error=True)
        # for node in nodes:
        #     if node['type'] == 'JOIN' and len(node['parents']) != 2:
        #         return to_json({"message": "Join node should have two parents"}, is_error=True)
        #     if node['type'] in ['FILTER','TRANSFORM'] and len(node['parents']) != 1:
        #         return to_json({"message": "Filter and Transform node should be have only one parent"}, is_error=True)
        #
        # return to_json({"message": "pipeline is valid"}, is_error=False)

        output = self.service.get_source_target(dag)
        return output
