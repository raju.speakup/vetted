# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_datamart import DataIngestionDataMartService
from src.utils.json_validator import validate_schema
from src.utils.json_utils import to_json
from src.schemas.di_dataset import id_stats_schema
from src.services.QBuilder import BuildQuery
from src.utils.mongo_db import db_client
from src.utils.config import conf
from flask import request,g
import json
import uuid
import datetime
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger
from swagger.dataset_swagger import TableSchema

class CreateTableResource(Resource):

    def __init__(self):
        self.db_name = conf.get_v('db.name')
        self.service = DataIngestionDataMartService(self.db_name)
        self.mongo_client = db_client
        self.builder = BuildQuery()
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, self.db_name)

    @swagger.operation(
        nickname='Create datamart table',
        notes='Create Datamart Table',
        parameters=[
            {
                "name": "parameter",
                "dataType": TableSchema.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @role_validator.validate_app_user()
    #@validate_schema(id_stats_schema(), is_path=True)
    def post(self, datamart_id):
        """ create  table"""
        dataset_id = str(uuid.uuid4())

        data = json.loads(request.data.decode('utf-8'))
        created_by = request.headers.get('userName')
        table_name = str(data['table_name']).lower()
        description = data['description']
        columns = data['columns']
        if created_by == None:
            created_by = 'unknown'

        if ' ' in table_name:
            return to_json({"message": "table name can't contain spaces"}, is_error= True)

        query = self.builder.create_table_query_build(table_name, data, 'postgres')

        return_object, count = self.mongo_client.find(self.db_name, "di_datamarts",
                                                      {"datamart_id": datamart_id})

        if not return_object:
            return to_json({"message": "provided datamart does not exist"}, is_error=True), 404

        for service in return_object:
            del service['_id']
            return_object = service
        if return_object:
            conn_id = return_object['conn_id']

        pg_conn = self.service.get_target_db_connection(conn_id)

        if not pg_conn:
            return to_json({"message": "connection could not be established"}, is_error=True)
        try:
            cur = pg_conn.cursor
            cur.execute(query)

            pg_conn.connection.commit()
            self.service.refresh_table_list(datamart_id)
        except Exception as e:
            return to_json({"message": str(e)}, is_error=True)

        new_table_doc ={
            'name': table_name,
            'description': description,
            'source_type': 'data_mart',
            'source_id': datamart_id,
            'table_name': table_name,
            'created_at': str(datetime.datetime.now()),
            'created_by': created_by,
            'columns': [(column['column_name'], column['data_type']) for column in columns],
            'dataset_id': dataset_id,
            'metrics': {
                    "no_of_rows": 0,
                    "no_of_columns": 0,
                    "size": "0 kb"
                }
        }

        if 'appid' in data:
            new_table_doc['appid'] = str(data['appid'])
        else:
            new_table_doc['appid'] = str(g.app_id)

        if 'clientid' in data:
            new_table_doc['clientid'] = str(data['clientid'])
        else:
            new_table_doc['clientid'] = str(g.client_id)

        output = self.service.create_table(new_table_doc)

        return to_json(output), 201
