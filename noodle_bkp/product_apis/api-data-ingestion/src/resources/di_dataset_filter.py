# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.di_datasets import DataIngestionDatasetService
from src.utils.json_validator import validate_schema
from src.schemas.di_dataset import id_stats_schema
from src.utils.json_utils import to_json
from src.utils.mongo_db import db_client
from src.utils.config import conf
from flask import request
import json
from bat_auth_sdk import role_validator
from flask_restful_swagger import swagger
from swagger.dataset_swagger import DataSetFilterSwaggerModel

class DatasetFilterResource(Resource):

    def __init__(self):
        self.db_name = conf.get_v('db.name')
        self.service = DataIngestionDatasetService(self.db_name)
        self.mongo_client = db_client
        db_url = conf.get_v('db.url')
        role_validator.init(db_url, self.db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "source_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "source_id"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "required": True,
                "paramType": "path",
                "description": "dataset_id"
            },
            {
                "name": "source_type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "source_type"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "parameter",
                "dataType": DataSetFilterSwaggerModel.__name__,
                'paramType': "body",
                "description":""
            }
        ],
        notes='Post Dataset filter',
        nickname='post dataset filter'
    )
    #@role_validator.validate_app_user()
    @validate_schema(id_stats_schema(), is_path=True)
    def post(self, source_id, dataset_id, source_type):
        """ dataset filter """

        data = json.loads(request.data.decode('utf-8'))
        filters = data['filter_conditions']

        limit = request.args.get("limit", 20)
        output = {}
        schema = self.service.get_dataset_schema(source_id, source_type, dataset_id)
        preview = self.service.preview_dataset_records(source_id, dataset_id, source_type, filters, limit)

        output.update({"preview": preview,
                       "schema": schema
                       })
        return to_json(output)
