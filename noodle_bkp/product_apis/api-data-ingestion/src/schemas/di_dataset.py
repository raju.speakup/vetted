from src.utils.json_validator import Schema, Prop


def dataset_schema():

    metrics_prop = {
        "rows": Prop().number().max(255).min(1).build(),
        "columns": Prop().string().max(255).min(1).build(),
        "size": Prop().number().max(255).min(1).build(),
    }
    items = {"type": "object", "properties": metrics_prop}

    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "source_type": Prop().string().max(255).min(1).build(),
        "source_id": Prop().string().max(255).min(1).build(),
        "table_name": Prop().string().max(255).min(1).build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build(),
        "metrics": Prop().object().properties(items).build(),
        "is_columnar": Prop().boolean().build(),
        "new_table_schema": Prop().object().build()
    }

    return Schema().keys(prop).required(["name", "source_type", "source_id", "table_name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "dataset_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["dataset_id"]).build()


def id_stats_schema():
    prop = {
        "source_id": Prop().string().max(100).min(1).build(),
        "dataset_id": Prop().string().max(100).min(1).build(),
        "source_type": Prop().string().max(100).min(1).build(),
    }

    return Schema().keys(prop).required(["source_id", "dataset_id", "source_type"]).build()


def refresh_datasets_schema():
    prop = {
        "source_id": Prop().string().max(100).min(1).build(),
        "source_type": Prop().string().max(100).min(1).build(),
    }

    return Schema().keys(prop).required(["source_id", "source_type"]).build()