from src.utils.json_validator import Schema, Prop


def datastore_schema():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "description": Prop().string().max(1000).build(),
        "conn_id": Prop().string().max(255).min(0).build(),
        "source_type": Prop().string().max(255).min(1).build(),
    }

    return Schema().keys(prop).required(["name", "description", "conn_id"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "datastore_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["datastore_id"]).build()
