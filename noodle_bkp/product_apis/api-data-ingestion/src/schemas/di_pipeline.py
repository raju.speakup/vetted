from src.utils.json_validator import Schema, Prop


def dag_schema():
    node_prop = {
        "id": Prop().string().max(255).min(1).build(),
        "name": Prop().string().max(255).min(1).build(),
        "type": Prop().string().max(255).min(1).enum(["DS", "DC", "JOIN",
                                                      "FILTER", "DM", "TRANSFORM", "SQL",
                                                      "COL_FILTER", "AGGREGATE"]).build(),
        "parents": Prop().array().max(255).min(1).build(),
        "order": Prop().number().max(255).min(1).build(),
        "description": Prop().string().max(1000).min(0).build(),
        "metadata": Prop().object().build(),
        "ui_metadata": Prop().object().build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build()
    }

    node_schema = {"type": "object", "properties": node_prop}
    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "status": Prop().string().max(255).min(0).build(),
        "nodes": Prop().array().items(node_schema).build(),
        "appid": Prop().string().max(255).min(0).build(),
        "clientid": Prop().string().max(255).min(0).build(),
        "current_version": Prop().string().max(255).min(0).build(),
        "versions": Prop().array().array().build()
    }

    return Schema().keys(prop).required(['name', 'nodes']).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "di_dag_id": Prop().string().max(100).min(1).build(),
    }

    return Schema().keys(prop).required([]).build()


def refresh_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build(),
        "source_type": Prop().string().max(100).min(1).build()
    }
    
    return Schema().keys(prop).required(["source_type"]).build()


def delete_schema():
    prop = {
        "ids": Prop().array().build(),
    }

    return Schema().keys(prop).required(["ids"]).build()
