from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify, request
from logger.logger import logger
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")

class GetConnectionsEngagement(Resource):
    """
    This class calls methods to create a new
    dataset for a cartridge
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to get physical connections for a particular client',
        nickname='GET'
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def get(self):
        """
        Get physical connections for a client
        Returns: Connections params

        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            clientid = request.headers.get("clientid")
            appid = request.headers.get("appid")
            connections = queries.getEngagementConnections(clientid,appid)
            logger.info(connections)
            return jsonify ({'data':connections})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})