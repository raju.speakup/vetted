from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
import pymssql
from logger.logger import logger

class CartridgeDatasetStatsUpdate(Resource):



    @swagger.operation(
        notes='This method is used to retrieve the '
              'data from target table',
        nickname='GET',
        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            }]
    )

    def get(self, cartridgeId):
        """
        Fetches data from target table
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except pymssql.InternalError as e:
            logger.info(e)
            return jsonify(
                {'Error': {
                    'Message': e.args[0]}})
        except pymssql.OperationalError  as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            data = queries.updateCartridgeStatistics(cartridgeId)
            logger.info(data)
            return jsonify({'data': data})
        except pymssql.ProgrammingError  as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        except pymssql.OperationalError  as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        except pymssql.DataError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': e}})