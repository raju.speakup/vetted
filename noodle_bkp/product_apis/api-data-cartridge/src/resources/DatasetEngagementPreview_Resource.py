from flask_restful import Resource, reqparse
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")


class PreviewEngagementDataset(Resource):
   """
   This class calls methods to retrieve the cartridge type
   """

   def __init__(self):
       role_validator.init(DB_URL, MONGO_DB)

   @swagger.operation(
       notes='This method is used to retrieve the '
             'data from target table',
       nickname='GET',
       parameters=[
           {
               "name": "connectionId",
               "description": "The connectionid is a unique "
                              "identifier of each connection",
               "required": True,
               "allowMultiple": False,
               "dataType": "String",
               "paramType": "path"
           },
           {
               "name": "datasetTable",
               "description": "The DatasetTable is a unique table name of each Cartridge's Set",
               "required": True,
               "allowMultiple": False,
               "dataType": "String",
               "paramType": "path"
           }]
   )
   @jwt_required
   @role_validator.validate_app_user()
   def get(self,connectionId,datasetTable):
       """
       Fetches data from target table
       :return: Success or failure message based on data fetched
       """
       parser = reqparse.RequestParser()
       parser.add_argument('offset', type=int)
       parser.add_argument('limit', type=int)

       args = parser.parse_args()
       offset = args['offset']
       limit = args['limit']

       try:
           queries = Queries()
           logger.info(queries)
       except pymssql.InternalError as e:
           logger.info(e)
           return jsonify(
               {'Error': {
                   'Message': e.args[0]}})
       except pymssql.OperationalError  as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       except Exception as e:
           logger.info(e)
           return jsonify({'Error': {
                   'Message': [e.args[0]]}})
       try:
           dataset = queries.previewEngagementDatasetTable(connectionId,datasetTable,offset,limit)
           logger.info(dataset)
           return (dataset)
       except pymssql.ProgrammingError  as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message':[e.args[0]]}})
       except pymssql.OperationalError  as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       except pymssql.DataError as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       except Exception as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e]}})