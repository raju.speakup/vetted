from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc
from logger.logger import logger

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")

class Cartridge(Resource):
    """
    This class calls methods to create a new cartridge
    """

    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to create new data cartridges',
        nickname='POST',

        parameters=[
            {
                "name": "body",
                "description": "{  }",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def post(self):
        """
        Creates a new data cartridge
        Returns: Data inserted if successful

        """

        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            cartridge = queries.createDataCartridge()
            logger.info(cartridge)
            return jsonify ({'data':cartridge})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})



    @swagger.operation(
        notes='This method is used to get all the cartridges ',
        nickname='GET',
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def get(self):
        """

        Fetches the list of cartridges

        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            limit = request.args.get("limit", 10)
            offset = request.args.get("offset", 0)


            cartridgesList, cartridgeCount = queries.allCartridges(limit, offset)
            logger.info(cartridgesList)
            return jsonify({'data': cartridgesList, 'cartridgeCount':cartridgeCount})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

    @swagger.operation(
            notes='This method is used to delete Cartridge',
            nickname='DELETE',
        parameters=[
            {
                "name": "body",
                "description": "{  }",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def delete(self):
            """
            Deletes a cartridge
            :return: Success or failure message based on data fetched
            """
            try:
                queries = Queries()
                logger.info(queries)
            except Exception as e:
                logger.info(e)
                return jsonify({'Error': {
                    'Message': [e.args[0]]}})
            try:
                delCartridge = queries.deleteCartridge()
                logger.info(delCartridge)
                return jsonify({'data': delCartridge})
            except Exception as e:
                logger.info(e)
                return jsonify({'Error': {
                    'Message': [e.args[0]]}})