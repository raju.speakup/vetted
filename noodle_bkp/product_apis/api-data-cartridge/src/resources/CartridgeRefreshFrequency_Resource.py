from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger

class CartridgeRefreshFrequency(Resource):
    """
    This class calls methods to fetch refresh frequency
    of the data cartridges
    """
    @swagger.operation(
        notes='This method is used to retrieve the '
              'type of data cartridge refresh frequency'
              ' to be used',
        nickname='GET'
    )
    def get(self):
        """
        Fetches cartridge refresh frequency
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            cartridgeRefreshFreq = queries.cartridgeRefreshFrequency()
            logger.info(cartridgeRefreshFreq)
            return jsonify({'data' : cartridgeRefreshFreq})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})