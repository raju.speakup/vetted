from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger


class CartridgeType(Resource):
    """
    TThis class calls methods to retrieve the cartridge type
    """
    @swagger.operation(
        notes='This method is used to retrieve the '
              'type of data cartridge'
              ' to be used',
        nickname='GET'
    )
    def get(self):
        """
        Fetches cartridge type
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            cartridgeType = queries.dataCartridgeType()
            logger.info(cartridgeType)
            return jsonify({'data' : cartridgeType})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})