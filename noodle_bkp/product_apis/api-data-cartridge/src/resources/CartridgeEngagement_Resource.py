from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
from flask import request
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")

class CartridgeEngagement(Resource):
    """
    This class calls methods to create a new engagement among app_id,customer_id,dataset_id and cartridge_id
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)


    @swagger.operation(
        notes='This method is used to create new data engagement',
        nickname='POST',

        parameters=[
            {
                "name": "body",
                "description": "{  }",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def post(self):
        """
        Creates a new data cartridge
        Returns: Data inserted if successful

        """

        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            clientid = request.headers.get("clientid")
            appid = request.headers.get("appid")
            engagement = queries.createEngagementCartridge(appid,clientid)
            logger.info(engagement)
            return jsonify ({'data':engagement})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

    @swagger.operation(
        notes='This method is used to get all the cartridges ',
        nickname='GET',
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def get(self):
        """

        Fetches the list of cartridges

        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            limit = request.args.get("limit", 10)
            offset = request.args.get("offset", 0)
            clientid = request.headers.get("clientid")
            appid = request.headers.get("appid")

            cartridgesList, cartridgeCount = queries.showEngagementClientCartridges(limit, offset, clientid, appid)
            logger.info(cartridgesList)
            return jsonify({'data': cartridgesList, 'cartridgeCount': cartridgeCount})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})