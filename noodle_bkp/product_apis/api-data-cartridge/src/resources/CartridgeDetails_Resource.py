from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")


class CartridgeDatasetDetails(Resource):
    """
    This class calls methods to get details for a specific cartridgeName
    including the columns and statistics of it
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to provide details of '
              'each CartridgeName and the Datasets '
              'including descriptions of rules applied on it',
        nickname='GET',

        parameters =[
        {
            "name": "cartridgeId",
            "description": "The CartridgeName is a unique "
                           "identifier of each Cartridge",
            "required": True,
            "allowMultiple": False,
            "dataType": "String",
            "paramType": "path"
        },
        {
            "name": "datasetId",
            "description": "The DataSetName is a unique identifier of each Cartridge's Set",
            "required": True,
            "allowMultiple": False,
            "dataType": "String",
            "paramType": "path"
        }]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def get(self, cartridgeId, datasetId):
        """
        Gets column names and types, statistics of the dataset
        and some basic column statistics
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            cartridgeDetailsScreen = queries.cartridgeDetails(cartridgeId, datasetId)
            logger.info(cartridgeDetailsScreen)
            return jsonify ({'data':cartridgeDetailsScreen})

        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

    @swagger.operation(
        notes='This method is used to update dataset of a given cartridge',
        nickname='PUT',

        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            },
            {
                "name": "datasetId",
                "description": "The DataSetName is a unique identifier of each Cartridge's Set",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            },
            {
                "name": "body",
                "description": "Change basic details(datasetname/description) of a dataset",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def put(self, cartridgeId, datasetId):
        """
        Updates basic info in the dataset
        Args:
            cartridgeId: The unique identifier of a cartridge
            datasetId: the unique identifier of a dataset

        Returns: True if updated successfully

        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            updateDatasetDetails = queries.updateDataset(cartridgeId, datasetId)
            logger.info(updateDatasetDetails)
            return jsonify({'data': updateDatasetDetails})

        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

    @swagger.operation(
        notes='This method is used to delete dataset of a given cartridge',
        nickname='DELETE',

        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            },
            {
                "name": "datasetId",
                "description": "The DataSetName is a unique identifier of each Cartridge's Set",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            }]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def delete(self,cartridgeId, datasetId):
        """
        Delete a dataset under a data cartridge
        Args:
            cartridgeId: The unique identifier of a cartridge
            datasetId: the unique identifier of a dataset

        Returns: True if deleted successfully

        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            cartridgeDetailsScreen = queries.deleteDataset(cartridgeId, datasetId)
            logger.info(cartridgeDetailsScreen)
            return jsonify ({'data':cartridgeDetailsScreen})

        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})