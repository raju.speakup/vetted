from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")

class Dataset(Resource):
    """
    This class calls methods to create a new
    dataset for a cartridge
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to create new dataset for a particular cartridge',
        nickname='POST',

        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            },
            {
                "name": "body",
                "description": "{  }",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def post(self, cartridgeId):
        """
        Creates a new dataset
        Returns: Data inserted if successful

        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            dataset = queries.createDataSet(cartridgeId)
            logger.info(dataset)
            return jsonify ({'data':dataset})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})