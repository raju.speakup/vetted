from flask_restful import Resource, reqparse
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
import pymssql
from logger.logger import logger


class DBStatistics(Resource):
   """
   This class calls methods to retrieve the cartridge and dataset type
   """

   @swagger.operation(
       notes='This method is used to update the '
             'number of rows, columns and size of the target table',
       nickname='GET',
       parameters=[
           {
               "name": "cartridgeId",
               "description": "The CartridgeId is a unique "
                              "identifier of each Cartridge",
               "required": True,
               "allowMultiple": False,
               "dataType": "String",
               "paramType": "path"
           },
           {
               "name": "datasetId",
               "description": "The DataSetId is a unique identifier of each Cartridge's Set",
               "required": True,
               "allowMultiple": False,
               "dataType": "String",
               "paramType": "path"
           }]
   )

   def get(self,cartridgeId,datasetId):
       """
       Update the statistics of the target table
       :return: Success or failure message based on data fetched
       """

       try:
           queries = Queries()
           logger.info(queries)
       except pymssql.InternalError as e:
           logger.info(e)
           return jsonify(
               {'Error': {
                   'Message': e.args[0]}})
       except pymssql.OperationalError  as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       except Exception as e:
           logger.info(e)
           return jsonify({'Error': {
                   'Message': [e.args[0]]}})
       try:
           data = queries.updateDBStatistics(cartridgeId,datasetId)
           logger.info(data)
           return jsonify({'data': data})
       except pymssql.ProgrammingError  as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message':[e.args[0]]}})
       except pymssql.OperationalError  as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       except pymssql.DataError as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       except Exception as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e]}})