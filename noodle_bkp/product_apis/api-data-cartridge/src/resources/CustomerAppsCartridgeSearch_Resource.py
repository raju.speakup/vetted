from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")


class CustomerCartridgeSearch(Resource):
    """
    This class calls methods to get details
    for a specific CartridgeName
    based on certain search criteria
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to search all the data cartridges according  '
              'to the search made',
        nickname='GET',

        parameters=[
            {
                "name": "searchTerm",
                "description": "The search term to find "
                               "Cartridge availables",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def get(self, searchTerm):
        """
        Searches for different cartridges according to the search criteria
        :param json: for searchterm, refreshFrequency, cartridgeType
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            clientid = request.headers.get("clientid")
            appid = request.headers.get("appid")
            cartridgeCustomScreen = queries.customerCustomiseCartridgeInfo(searchTerm,clientid,appid)
            logger.info(cartridgeCustomScreen)
            return jsonify({'data': cartridgeCustomScreen})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
"""
    @swagger.operation(
        notes='This method is used to search all the data cartridges according  '
              'to the search made',
        nickname='POST',

        parameters =[
            {
                "name": "body",
                "description": "{ searchTerm : \"\"}",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def post(self,client_id,app_id):
        
        Searches for different cartridges according to the search criteria
        :param json: for searchterm, refreshFrequency, cartridgeType
        :return: Success or failure message based on data fetched

        try:
            queries = Queries()
        except Exception as e:
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            cartridgeCustomScreen = queries.customerCustomiseCartridgeInfo(client_id,app_id)
            return jsonify ({'data':cartridgeCustomScreen})
        except Exception as e:
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
"""