from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required
from config_store import config_store_client as csc

cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-cartridge')
MONGO_DB = cs.get("MONGO_DB")
DB_URL = cs.get("DB_URL")

class CartridgeDatasets(Resource):
    """
    This class calls methods to get datasets for a
    specific cartridgeName including basic
    statistics of it
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to get datasets, rows, '
              'consumers for each cartridge ',
        nickname='GET',

        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            }]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def get(self, cartridgeId):
        """
        Gets datasets, rows and active consumers for each cartridge
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            cartridgeDetailsScreenIntermediate = queries.intermediateDetailCartridges(cartridgeId)
            logger.info(cartridgeDetailsScreenIntermediate)
            return jsonify({'data': cartridgeDetailsScreenIntermediate})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})


    @swagger.operation(
        notes='This method is used to update any field'
              'in a data cartridge  ',
        nickname='PUT',

        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "Int",
                "paramType": "path"
            },
            {
                "name": "body",
                "description": "",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def put(self, cartridgeId):
        """
        Updates fields for a given data cartridge
        Returns: Data updated, if successful
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            #clientid = request.headers.get("clientid")
            #appid = request.headers.get("appid")
            updatedData = queries.updatedata_cartridge(cartridgeId)
            logger.info(updatedData)
            return jsonify({'data': updatedData})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

    @swagger.operation(
        notes='This method is used to delete Cartridge',
        nickname='DELETE',

        parameters=[
            {
                "name": "cartridgeId",
                "description": "The CartridgeName is a unique "
                               "identifier of each Cartridge",
                "required": True,
                "allowMultiple": False,
                "dataType": "String",
                "paramType": "path"
            }]
    )
    #@jwt_required
    #@role_validator.validate_app_user()
    def delete(self, cartridgeId):
        """
        Deletes Cartridge
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            delCartridge = queries.deleteCartridge(cartridgeId)
            logger.info(delCartridge)
            return jsonify({'data': delCartridge})
        except Exception as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})