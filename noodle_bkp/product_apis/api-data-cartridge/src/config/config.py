import os
from dotenv import load_dotenv

APP_ROOT = os.path.join(os.path.dirname(__file__), '..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)


MONGO_SERVER= os.getenv("MONGO_SERVER")
MONGO_DB= os.getenv("MONGO_DB")
MONGO_AUTHDB = os.getenv("MONGO_AUTHDB")
MONGO_USER= os.getenv("MONGO_USER")
MONGO_PASSWORD= os.getenv("MONGO_PASSWORD")
MONGO_COLLECTION=os.getenv("MONGO_COLLECTION")
AUTH_MECHANISM=os.getenv("AUTH_MECHANISM")

SERVER_NAME = os.getenv("SERVER_NAME")
DB_NAME = os.getenv("DB_NAME")
USERNAME = os.getenv("USERNAME")
PASSWORD = os.getenv("PASSWORD")
RAW_SCHEMA = os.getenv("RAW_SCHEMA")

HIVE_HOST_NAME=os.getenv("HIVE_HOST_NAME")
HIVE_DB_NAME=os.getenv("HIVE_DB_NAME")
HIVE_PORT=os.getenv("HIVE_PORT")
HIVE_AUTH_MECHANISM=os.getenv("HIVE_AUTH_MECHANISM")



