from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger
#from resources.CartridgeType_Resource import CartridgeType
#from resources.CartridgeRefreshFrequency_Resource import CartridgeRefreshFrequency
from resources.CartridgeDetails_Resource import CartridgeDatasetDetails
from resources.CartridgeSearch_Resource import CartridgeSearch
from resources.CartridgeDatasets_Resource import CartridgeDatasets
from resources.Cartridge_Resource import Cartridge
from resources.CreateNewDataset_Resource import Dataset
from resources.PreviewData import PreviewData
from resources.CartridgeUpdateStats import CartridgeDatasetStatsUpdate
from resources.UpdateDBStatistics_Resource import DBStatistics
from resources.CartridgeHealth_Resource import Health, ConfigParams
from flask_jwt_extended import JWTManager
from resources.CustomerAppsCartridge_Resource import CustomerCartridge
from resources.CustomerAppsCartridgeSearch_Resource import CustomerCartridgeSearch
from resources.CartridgeEngagement_Resource import CartridgeEngagement
from resources.DatasetEngagement_Resource import DatasetEngagement
from resources.CartridgeEngagementOne_Resource import ShowEngagementCartridge
from resources.DatasetEngagementPreview_Resource import PreviewEngagementDataset
from resources.GetConnectionEngagement_Resource import GetConnectionsEngagement
app = Flask(__name__)

api = swagger.docs(
    Api(app), apiVersion='0.1',
    basePath="http://localhost:5000/data-cartridge/api/v1/<Resource>",
    description="docs for data cartridge api")

# JWT settings
app.config['JWT_SECRET_KEY'] = 'some-difficult-value'
jwt = JWTManager(app)

app.config['PROPAGATE_EXCEPTIONS'] = True

#api.add_resource(CartridgeType,'/data-cartridge/api/v1/cartridges/_cartridgeTypes')
#api.add_resource(CartridgeRefreshFrequency,'/data-cartridge/api/v1/cartridges/_cartridgeRefreshFrequencies')
api.add_resource(CartridgeEngagement,'/data-cartridge/api/v1/di/cartridges')
api.add_resource(ShowEngagementCartridge,'/data-cartridge/api/v1/di/cartridges/<string:cartridgeId>')
#api.add_resource(DatasetEngagement,'/data-cartridge/api/v1/di/cartridges/<string:cartridgeId>/datasets')
#api.add_resource(PreviewEngagementDataset,'/data-cartridge/api/v1/di/cartridges/<string:connectionId>/dataset/<string:datasetTable>')
api.add_resource(GetConnectionsEngagement,'/data-cartridge/api/v1/di/dbConnections')
#######
api.add_resource(Cartridge,'/data-cartridge/api/v1/cartridges')
api.add_resource(CustomerCartridge,'/data-cartridge/api/v1/customer/cartridges')
api.add_resource(Dataset,'/data-cartridge/api/v1/cartridges/<string:cartridgeId>/datasets')
api.add_resource(CartridgeDatasets,'/data-cartridge/api/v1/customer/cartridges/<string:cartridgeId>')
api.add_resource(CartridgeDatasetDetails, '/data-cartridge/api/v1/customer/cartridges/<string:cartridgeId>/datasets/<string:datasetId>')
api.add_resource(CartridgeDatasetStatsUpdate, '/data-cartridge/api/v1/cartridges/<string:cartridgeId>/_updateCartridgeStatistics')
api.add_resource(CartridgeSearch,'/data-cartridge/api/v1/cartridges/_search/<string:searchTerm>')
api.add_resource(CustomerCartridgeSearch,'/data-cartridge/api/v1/customer/cartridges/_search/<string:searchTerm>')
api.add_resource(PreviewData,'/data-cartridge/api/v1/customer/cartridges/<string:cartridgeId>/datasets/<string:datasetId>/_previewData')
api.add_resource(DBStatistics,'/data-cartridge/api/v1/cartridges/<string:cartridgeId>/datasets/<string:datasetId>/_updateDatasetStatistics')
api.add_resource(Health,'/data-cartridge/api/v1/_healthStatus')
api.add_resource(ConfigParams,'/data-cartridge/api/v1/_configParams')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000,threaded='TRUE')