from flask import request,jsonify
import json
import jsonschema
from jsonschema import validate
import datetime
import uuid
from pymongo import MongoClient
import pymongo
from commons.utils import return_result
import pymssql
import psycopg2
import re
from collections import OrderedDict
from config_store import config_store_client as csc

# from config.config import (
   # SERVER_NAME,
   # DB_NAME,
   # USERNAME,
   # PASSWORD,
   # RAW_SCHEMA,
   # MONGO_SERVER,
   # MONGO_AUTHDB,
   #  MONGO_DB
   # MONGO_USER,
   # MONGO_PASSWORD,
   # AUTH_MECHANISM,
   # HIVE_HOST_NAME,
   # HIVE_PORT,
   # HIVE_DB_NAME,
   # HIVE_AUTH_MECHANISM
# )

class Queries:
    """
    Init constructor to instantiate the class
    """


    def __init__(self):
        #os.environ['CONF_STORE'] = '192.168.2.142:8811'
        #os.environ['CONF_ENV'] = 'dev'
        cs = csc.ConfigStore()
        cs.load(ip='*', service_name='api-data-cartridge')

        MONGO_SERVER = cs.get("MONGO_SERVER")
        MONGO_USER = cs.get("MONGO_USER")
        MONGO_PASSWORD = cs.get("MONGO_PASSWORD")
        MONGO_AUTHDB = cs.get("MONGO_AUTHDB")
        MONGO_DB = cs.get("MONGO_DB")
        AUTH_MECHANISM = cs.get("AUTH_MECHANISM")
        SERVER_NAME = cs.get("DB_SERVER")
        #DB_NAME = cs.get("DB_NAME")
        #RAW_SCHEMA =cs.get("DB_SCHEMA")
        USERNAME = cs.get("DB_USER")
        PASSWORD = cs.get("DB_PASSWORD")
        PG_DB_NAME = "bat_data_ingestion"
        PG_USER = "batuser"
        PG_HOST = "192.168.10.112"
        PG_DB_PASS = "Noodle1234+"
        #print(MONGO_DB)
        # connection_string="".join\
        #     (['mongodb://', MONGO_USER, ':',
        #       MONGO_PASSWORD, '@', MONGO_SERVER])
        self.config = csc.ConfigStore().load(ip='*', service_name='api-data-cartridge')
        self.client = MongoClient(MONGO_SERVER,
                             username=MONGO_USER,
                             password=MONGO_PASSWORD,
                             authSource=MONGO_AUTHDB,
                             authMechanism=AUTH_MECHANISM)
        self.conn = pymssql.connect(SERVER_NAME, USERNAME, PASSWORD)


    def dataCartridgeType(self):
        """
        Retrieves all cartridge types from the collection
        :return: List of all cartridge types
        """

        data = []
        db = self.client[self.config["MONGO_DB"]]
        # print(db)
        #print(self.config["MONGO_DB"])
        cartridgeType = db.dc_cartridge_type.find({}, {"_id": 0})

        for cartridges in cartridgeType:
            data.append(cartridges)

        print(data)
        self.client.close()
        return data

    def cartridgeRefreshFrequency(self):
        data = []
        db = self.client[self.config["MONGO_DB"]]
        cartridgeFrequency = db.dc_refresh_frequency.find({},
                                                {"_id": 0})

        for cartridges in cartridgeFrequency:
            data.append(cartridges)

        self.client.close()
        return data

    def cartridgeDetails(self, cartridge_id, dataset_id):
        """
        Retrieves details of a particular cartridge and dataset
        :param cartridgeName: Unique identifier of a cartridge
        :param DataSetName: Name of the dataset contained in
        the cartridge
        :return: Cartridge details of the input cartridge and dataset
        """
        cartridgeColumns = []

        db = self.client[self.config["MONGO_DB"]]

        dataCartridgeColumns = db.dc_columns.find({"cartridge_id":
            cartridge_id, "dataset_id": dataset_id}, {"_id": 0})

        for cartridgeCols in dataCartridgeColumns:
            cartridgeColumns.append(cartridgeCols)

        dataCartridgeStatistics = db.dc_columns.find(
            {"cartridge_id": cartridge_id, "dataset_id": dataset_id},
            {"_id": 0, "activeConsumers": 1,
             "numberOfRows": 1, "numberOfColumns":1,
             "diskSpaceOccupied":1})

        for cartridgeStats in dataCartridgeStatistics:
            cartridgeColumns[0]['stats'] = cartridgeStats

        self.client.close()
        return cartridgeColumns
    '''
    def customiseCartridgeInfo(self):
        """
        Retrieves cartridge details based on a search term
        and/or cartridge type and refresh frequency
        :return: Cartridge details based on search criteria
        """

        db = self.client[self.config["MONGO_DB"]]

        data = request.json
        cartridgeType = data['cartridgeType']
        refreshFrequency = data['refreshFrequency']
        searchTerm = data['searchTerm']

        if ( data['searchTerm'] == "NA" and
            data['cartridgeType'] == "NA" and
            data['refreshFrequency'] == "NA"):
            cartridgeListData = []
            db = self.client[self.config["MONGO_DB"]]
            dataCartridges = db.data_cartridge.find({},
                            {"_id": 0})

            for cartridge in dataCartridges:
                cartridgeListData.append(cartridge)

            self.client.close()
            return cartridgeListData

        elif ( data['searchTerm'] == "NA" ):
            if (data['cartridgeType'] == "NA"
                and data['refreshFrequency'] != "NA"):
                dataCartridgeDetails = []
                cartridgeDetails = db.data_cartridge.find({
                    "refreshFrequency" : refreshFrequency
                }, {"_id":0})
                for cartridges in cartridgeDetails:
                    dataCartridgeDetails.append(cartridges)

                self.client.close()
                return dataCartridgeDetails

            elif (data['cartridgeType'] != "NA" and
                data['refreshFrequency'] == "NA"):
                dataCartridgeDetails = []
                cartridgeDetails = db.data_cartridge.find({
                    "cartridgeType": cartridgeType
                }, {"_id": 0})
                for cartridges in cartridgeDetails:
                    dataCartridgeDetails.append(cartridges)

                self.client.close()
                return dataCartridgeDetails

            else:
                dataCartridgeDetails = []
                cartridgeDetails = db.data_cartridge.find({
                    "$and": [ { "cartridgeType" : cartridgeType },
                        { "refreshFrequency" : refreshFrequency}]},
                    {"_id":0})
                for cartridges in cartridgeDetails:
                    dataCartridgeDetails.append(cartridges)

                self.client.close()
                return dataCartridgeDetails

        elif ( data['searchTerm'] != "NA" ):
            if (data['cartridgeType'] == "NA" and
                data['refreshFrequency'] == "NA"):
                dataCartridgeDetails =[]
                cartridgeDetails = db.data_cartridge.find({
                    "$or" : [ { "cartridgeName": searchTerm},
                              {"description": searchTerm},
                              {"keyWords": searchTerm}
                    ]
                }, {"_id": 0})
                for cartridges in cartridgeDetails:
                    dataCartridgeDetails.append(cartridges)

                self.client.close()
                return dataCartridgeDetails

            elif (data['cartridgeType'] == "NA" and
                  data['refreshFrequency'] != "NA"):
                dataCartridgeDetails = []
                cartridgeDetails = db.data_cartridge.find({
                    "$and": [
                        {"$or": [{"cartridgeName": searchTerm},
                                 {"description": searchTerm},
                                 {"keyWords": searchTerm}]},
                    {"$or": [{"refreshFrequency": refreshFrequency}]}
                    ]
                }, {"_id": 0})
                for cartridges in cartridgeDetails:
                    dataCartridgeDetails.append(cartridges)

                self.client.close()
                return dataCartridgeDetails

            else:
                dataCartridgeDetails = []
                cartridgeDetails = db.data_cartridge.find({
                     "$and" : [
                         {"$or": [{"cartridgeName": searchTerm},
                            {"description": searchTerm},
                            {"keyWords": searchTerm}]},
                    {"$or": [{"cartridgeType" : cartridgeType}]}
                     ]
                }, {"_id": 0})
                for cartridges in cartridgeDetails:
                    dataCartridgeDetails.append(cartridges)

                self.client.close()
                return dataCartridgeDetails

        elif (data['searchTerm'] != "NA" and
              data['cartridgeType'] != "NA" and
              data['refreshFrequency'] != "NA"):
            dataCartridgeDetails = []
            cartridgeDetails = db.data_cartridge.find({
                "$or" : [
                    { "$or" : [{"cartridgeName" : searchTerm},
                            {"description": searchTerm},
                            {"keyWords": searchTerm}]},
                    { "$and" : [{"cartridgeType" : cartridgeType},
                    {"refreshFrequency": refreshFrequency}]}
                ]
            }, {"_id": 0})
            for cartridges in cartridgeDetails:
                dataCartridgeDetails.append(cartridges)

            self.client.close()
            return dataCartridgeDetails
'''

    def customiseCartridgeInfo(self,searchTerm):
        """
        Retrieves cartridge details based on a search term

        :return: Cartridge details based on search criteria
        """

        db = self.client[self.config["MONGO_DB"]]

        #data = request.json
        cartridges = []
        #searchTerm = data['searchTerm']

        getCartridges = db.data_cartridge.find(
            {"cartridgeName":
                 {"$regex" :searchTerm,"$options":"i"},
             "isActive":1},
            {"_id":0}
        )

        '''getDatasets = db.dc_columns.find(
            {"datasetName":
                 {"$regex":searchTerm},
             "isActive":1},
            {"_id":0,"cartridge_id":1}
        )
        for dataset in getDatasets:
            cartridges
            '''
        for cartridge in getCartridges:
            cartridges.append(cartridge)
        print(len(cartridges))
        self.client.close()
        return cartridges

    def intermediateDetailCartridges(self, cartridge_id):
        """
                Retrieves dataset names and some basic statistics
                of the input cartridge name
                :param cartridgeName: Unique identifier of a cartridge
                :return: Cartridge details based on input cartridge name
                """
        db = self.client[self.config["MONGO_DB"]]
        cartridgeDatasets = []

        datasetsCursor = db.data_cartridge.find({"cartridge_id": cartridge_id},
                                               {"_id": 0, "dataSets": 1,
                                                "cartridgeName": 1, "sourceDescription":1,
                                                "cartridge_id": 1, "numberOfEngagements": 1,
                                                "datasets": 1, "description": 1,
                                                "updated_at": 1, "source": 1,
                                                "totalRowNum": 1, "diskSpaceOccupied(MB)":1,
                                                "target": 1, "diskSpaceOccupied" : 1,
                                                "updated_by":1,"created_at":1,"created_by":1})

        for cartridgeSets in datasetsCursor:
            cartridgeDatasets.append(cartridgeSets)

            if ('datasets' in cartridgeDatasets[0].keys()):
                for i in range(len(cartridgeSets['datasets'])):
                    dataset_id = (cartridgeSets['datasets'][i]['dataset_id'])
                    datasetsStatsCursor = db.dc_columns.find(
                    {"dataset_id": dataset_id},
                    {"_id": 0, "activeConsumers": 1,
                     "numberOfRows": 1, "numberOfColumns":1,
                     "diskSpaceOccupied(MB)" : 1, "diskSpaceOccupied" : 1})

                    for cartridgeStats in datasetsStatsCursor:
                    # Deleted the below 3 lines
                    # cartridgeStatsInfo = []
                    # cartridgeStatsInfo.append(cartridgeStats)
                    # cartridgeDatasets[0]['datasets'][i]["stats"] = cartridgeStatsInfo

                    #added the below line
                        cartridgeDatasets[0]['datasets'][i]["stats"] = cartridgeStats

        self.client.close()
        return cartridgeDatasets

    def allCartridges(self, limit, offset):
        """
        Retrieves list of all cartridges
        Returns: Data if retrieval is successful

        """
        data = []
        db = self.client[self.config["MONGO_DB"]]

        cartridgeCount = db.data_cartridge.find({"isActive":1}, {"_id": 0}).count()
        getAllCartridges = db.data_cartridge.find({"isActive":1},{"_id":0})\
            .sort("cartridgeName",pymongo.DESCENDING).skip(int(offset)).limit(int(limit))

        for cartridges in getAllCartridges:
            data.append(cartridges)

        self.client.close()
        return data, cartridgeCount

    def createDataCartridge(self):
        """
        This method creates a new data cartridge
        based on the parameters received through the input JSON
        Returns: Data inserted if successful

        """

        now = datetime.datetime.utcnow()
        db = self.client[self.config["MONGO_DB"]]
        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        data = request.json
        cartridge_id = uuid.uuid4().hex
        db.data_cartridge.insert({"cartridge_id": cartridge_id,
                                 "cartridgeName": data["cartridgeName"],
                                 "description": data["cartridgeDescription"],
                                 #"cartridgeType": data["cartridgeType"],
                                 "isActive": data["isActive"],
                                 "lastDataRefresh": currentDate,
                                 "refreshFrequency": data["refreshFrequency"],
                                 "created_by": self.config["MONGO_USER"],
                                 "created_at": currentDate,
                                  "diskSpaceOccupied" : 0.0,
                                  "totalRowNum" : 0,
                                 "frequencyInterval" : data["frequencyInterval"],
                                 "sourceDescription" : data["sourceDescription"],
                                 "source" : data["source"],
                                 #"target" : data["target"]
                                 })

        db.data_cartridge.update({"cartridge_id" : cartridge_id},
                                {"$set": {"keywords": data["keywords"]}},
                                multi=True)

        self.client.close()
        return data

    def createDataSet(self, cartridge_id):
        """
        This method creates a new dataset
        based on the parameters received through
        the input JSON for a specific cartridge
        Returns: Data inserted if successful

        """
        now = datetime.datetime.utcnow()

        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        db = self.client[self.config["MONGO_DB"]]
        data = request.json

        dataset_id = uuid.uuid4().hex

        db.data_cartridge.update(
            {'cartridge_id': cartridge_id},
            {
                "$addToSet": {
                    "datasets": {
                        "$each": [{
                            "dataset_id": dataset_id,
                            "datasetId":dataset_id,
                            "datasetName": data["datasetName"],
                            "datasetDescription" : data["datasetDescription"]
                        }]
                    }
                },
                "$set": {
                    "lastDataRefresh": currentDate,
                    "updated_by": self.config["MONGO_USER"],
                    "updated_at": currentDate
                }
            }
        )

        db.dc_columns.insert({"cartridge_id": cartridge_id,
                             "dataset_id": dataset_id,
                              "datasetId":dataset_id,
                              "diskSpaceOccupied":0.0,
                              "numberOfRows":0,
                              "numberOfColumns":0,
                             "datasetName": data["datasetName"]
                             })

        for item in data["columns"]:
            db.dc_columns.update(
                {'cartridge_id': cartridge_id,'dataset_id':dataset_id},
                {
                    "$push": {
                        "columns": {
                            "$each": [{
                                "isPrimaryKey" : item["isPrimaryKey"],
                                "columnName": item["columnName"],
                                "columnDataType": item["columnDataType"],
                                "allowNulls" : item["allowNulls"],
                                "columnDescription" : item["columnDescription"]
                            }]
                        }
                    },
                    "$set": {
                        "created_by": self.config["MONGO_USER"],
                        "created_at": currentDate
                    }
                }
            )
        db.dc_columns.update({"dataset_id": dataset_id},
                {"$set": {"orderByColumnsSql": data["orderByColumnsSql"]}},
                                multi=True)

        #stripDatasetName = re.sub('\s+','',data["datasetName"])

        db.dc_target_db_details.insert({
            "cartridge_id" : cartridge_id,
            "dataset_id" : dataset_id,
            "datasetId" : dataset_id,
            "datasetName" : data["datasetName"],
            "targetSchema" : data["schemaName"],
            "targetServerType" : "SQL Server",
            "targetHost" : data["hostName"],
            "targetDatabase" : data["dbName"],
            "targetPort" : 1433,
            "targetTable" : data["targetTable"],
            "created_by" : self.config["MONGO_USER"],
            "created_at" : currentDate
         })

        self.client.close()
        return data

    def updatedata_cartridge(self, cartridge_id):
        """
        This method updates the collection based
        on new values in the json
        Args:
            cartridge_id: Unique identifier of the data cartridge

        Returns: Data if update is successful

        """
        now = datetime.datetime.utcnow()
        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

        updatedJson = []
        db = self.client[self.config["MONGO_DB"]]
        data = request.json
        for key, value in data.items():
            if (key == "keywords"):
                for i in data[key]:
                    db.data_cartridge.update(
                        {"cartridge_id": cartridge_id},
                        {   "$addToSet": {"keywords": i},
                            "$set" : {
                                "updated_at" : currentDate,
                                "updated_by" : self.config["MONGO_USER"]
                            }
                        }
                    )
            else:
                db.data_cartridge.update(
                    {'cartridge_id': cartridge_id},
                    {
                        "$set": {
                            key: value,
                            "updated_at": currentDate,
                            "updated_by" : self.config["MONGO_USER"]
                        }
                    }
                )



        getAllCartridges = db.data_cartridge.find(
            {
                "cartridge_id" : cartridge_id
            },
            {
                "_id" : 0
            }
        )

        for cartridges in getAllCartridges:
            updatedJson.append(cartridges)

        self.client.close()
        return updatedJson

    def updateDataset(self, cartridge_id, dataset_id):
        """
        This method updates the collection based
        on new values in the json
        Args:
            cartridge_id: Unique identifier of the data cartridge

        Returns: Data if update is successful

        """
        now = datetime.datetime.utcnow()
        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

        db = self.client[self.config["MONGO_DB"]]
        data = request.json
        for key, value in data.items():
            # Updating orderByColumnsSql
            if (key == "orderByColumnsSql"):
                db.dc_columns.update(
                        {
                            "cartridge_id": cartridge_id,
                            "dataset_id": dataset_id
                        },
                        {
                            "$addToSet": {
                                key : {
                                    "$each" : data[key]
                                }
                            },
                            "$set" : {
                                "updated_at" : currentDate,
                                "updated_by" : self.config["MONGO_USER"]
                            }
                        }
                )
            # Updating datasetName
            elif (key == "datasetName"):
                db.data_cartridge.update(
                    {
                        "cartridge_id": cartridge_id,
                        "datasets.dataset_id": dataset_id
                    },
                    {
                        "$set": {
                            "datasets.$." + key: value,
                            "updated_at": currentDate,
                            "updated_by": self.config["MONGO_USER"]
                        }
                    }
                )

                db.dc_target_db_details.update(
                    {
                        "cartridge_id": cartridge_id,
                        "dataset_id": dataset_id
                    },
                    {
                        "$set": {
                            key: value,
                            "updated_at": currentDate,
                            "updated_by" : self.config["MONGO_USER"]
                        }
                    }
                )

                db.dc_columns.update(
                    {
                        "cartridge_id": cartridge_id,
                        "dataset_id": dataset_id
                    },
                    {
                        "$set": {
                            key: value,
                            "updated_at": currentDate,
                            "updated_by" : self.config["MONGO_USER"]
                        }
                    }
                )


            # Updating Target DB Table info
            elif (key == "targetDB"):
                targetDBDetails = data[key]
                for k,v in targetDBDetails.items():
                    #print(k,v)
                    db.dc_target_db_details.update(
                        {
                            "cartridge_id" : cartridge_id,
                            "dataset_id" : dataset_id
                        },
                        {
                            "$set" : {
                                k : v,
                                "updated_at" : currentDate,
                                "updated_by" : self.config["MONGO_USER"]
                            }
                        }
                    )
                    db.data_cartridge.update(
                        {
                            "cartridge_id" : cartridge_id
                        },
                        {
                            "$set" : {
                                "lastDataRefresh" : currentDate,
                                "updated_at": currentDate,
                                "updated_by": self.config["MONGO_USER"]
                            }
                        }
                    )
            # Updating Existing Column Schema Info
            elif (key == "columns"):
                #print(len(data[key]))
                for i in range(len(data[key])):
                    # print (data[key][i])
                    # print(data[key][i]['oldColumnName'])
                    # print(data[key][i]['newColumnName'])
                    db.dc_columns.update(
                            {
                                "cartridge_id" : cartridge_id,
                                "dataset_id" : dataset_id,
                                "columns.columnName" : data[key][i]['oldColumnName']
                            },
                            {
                                "$set" : {
                                    "columns.$.allowNulls" : data[key][i]['allowNulls'],
                                    "columns.$.columnDataType" : data[key][i]['columnDataType'],
                                    "columns.$.columnDescription" : data[key][i]['columnDescription'],
                                    "columns.$.columnName" : data[key][i]['newColumnName'],
                                    "updated_at": currentDate,
                                    "updated_by": self.config["MONGO_USER"]
                                }
                            }
                    )
                    db.dc_columns.update(
                        {
                            "cartridge_id" : cartridge_id,
                            "dataset_id" : dataset_id,
                            "columns.columnName" : { "$ne" : data[key][i]['oldColumnName']}
                        },
                        {
                            "$addToSet" : {
                                "columns" : {
                                    "allowNulls": data[key][i]['allowNulls'],
                                    "columnDataType": data[key][i]['columnDataType'],
                                    "columnDescription": data[key][i]['columnDescription'],
                                    "columnName": data[key][i]['newColumnName']
                                }
                            }
                        }
                    )
            # Updating Dataset Description
            elif (key == "datasetDescription"):
                db.data_cartridge.update(
                    {
                        "cartridge_id": cartridge_id,
                        "datasets.dataset_id": dataset_id
                    },
                    {
                        "$set": {
                            "datasets.$." + key: value,
                            "updated_at": currentDate,
                            "updated_by" : self.config["MONGO_USER"]
                        }
                    }
                )
                db.dc_columns.update(
                    {
                        "cartridge_id": cartridge_id,
                        "dataset_id": dataset_id
                    },
                    {
                        "$set": {
                            key: value,
                            "updated_at": currentDate,
                            "updated_by": self.config["MONGO_USER"]
                        }
                    }
                )

        self.client.close()
        return data

    def previewData(self,cartridge_id,dataset_id,offset,limit):

        jsondata = []

        db = self.client[self.config["MONGO_DB"]]
        getTargetDB = db.dc_target_db_details.find(
            {
                "cartridge_id": cartridge_id,
                "dataset_id": dataset_id
            },
            {
                "_id": 0
            }
        )

        for dbDetails in getTargetDB:
            jsondata.append(dbDetails)

        tableName = jsondata[0]["targetTable"]
        dbName = jsondata[0]["targetDatabase"]
        schemaName = jsondata[0]["targetSchema"]

        getOrderByColumns = db.dc_columns.find(
            {
                "cartridge_id" : cartridge_id,
                "dataset_id" : dataset_id
            },
            {
                "_id":0,
                "orderByColumnsSql" : 1
            }
        )

        if(offset==None):
            offset=0

        if(limit==None):
            limit=100

        # If there is no order by columns
        if(len(getOrderByColumns[0]['orderByColumnsSql'][0]) == 0):
            self.cursor = self.conn.cursor()
            queryString = "select * from [" + dbName + "].[" + schemaName + "].[" + tableName + "] " \
                            "ORDER BY(SELECT NULL) OFFSET "+str(offset)+" ROWS FETCH NEXT "+str(limit)+" ROWS ONLY"
            self.cursor.execute(queryString)
            data = self.cursor.fetchall()
        else:
            for orderColumns in getOrderByColumns:
                columnOrder = ','.join(orderColumns["orderByColumnsSql"])

            self.cursor = self.conn.cursor()

            queryString = "select * from ["+dbName+"].["+schemaName+"].["+tableName+"]" \
                            " order by "+columnOrder+" OFFSET "+str(offset)+" ROWS FETCH NEXT "+str(limit)+" ROWS ONLY"
            self.cursor.execute(queryString)
            data = self.cursor.fetchall()

        self.cursor1 = self.conn.cursor()
        query_string1 = "SELECT column_name "\
                                "FROM "+dbName+".INFORMATION_SCHEMA.COLUMNS "\
                                "WHERE TABLE_NAME = N'%s'" % tableName
        self.cursor1.execute(query_string1)

        col_names_dict = self.cursor1.fetchall()
        finalData = []
        colNames = [i for (i,) in col_names_dict]
        for i in range(len(data)):
                finalData1 = OrderedDict({})
                for j in range(len(data[i])):
                    finalData1[colNames[j]] = (data[i][j])
                finalData.append(finalData1)
        self.conn.close()

        return (finalData)

    def updateCartridgeStatistics(self,cartridge_id):

        """
                Updates the number of rows,DiskSpaceUsed in a Cartridge
                Returns: Data if retrieval is successful

        """
        now = datetime.datetime.utcnow()

        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

        db = self.client[self.config["MONGO_DB"]]
        jsondata = []
        getAllDatasets = db.dc_columns.find(
            {
                "cartridge_id" : cartridge_id
            },
            {
                "_id":0,
                "diskSpaceOccupied":1,
                "numberOfRows":1,
                "cartridge_id":1,
                "dataset_id":1,
                "datasetName":1
            }
        )
        for stats in getAllDatasets:
            jsondata.append(stats)

        totalDiskSpace=0.0
        totalRowsNum=0

        for i in range(0,len(jsondata)):
            totalDiskSpace=totalDiskSpace+jsondata[i]['diskSpaceOccupied']
            totalRowsNum=totalRowsNum+jsondata[i]['numberOfRows']

        jsondata.append(totalRowsNum)
        jsondata.append(totalDiskSpace)

        db.data_cartridge.update(
            {
                "cartridge_id": cartridge_id
            },
            {
                "$set" : {
                    "diskSpaceOccupied" : totalDiskSpace,
                    "totalRowNum" : totalRowsNum,
                    "updated_at" : currentDate,
                    "updated_by" : self.config["MONGO_USER"]
                }
            }
         )


        self.conn.close()
        self.client.close()
        return jsondata

    def updateDBStatistics(self,cartridge_id,dataset_id):
            """
            Args:
                cartridge_id: The unique identifier of a cartridge
                dataset_id : The unique identifier of a dataset
            Returns: True, if cartridge and dataset is sucessfully found
            """
            now = datetime.datetime.utcnow()

            currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

            data = []
            db = self.client[self.config["MONGO_DB"]]

            getTargetTable = db.dc_target_db_details.find(
                {
                    "cartridge_id": cartridge_id,
                    "dataset_id" : dataset_id
                },
                {
                    "_id": 0,
                    "targetTable": 1,
                    "targetDatabase" : 1
                }
            )
            for cartridge in getTargetTable:
                data.append(cartridge)

            tableName = data[0]["targetTable"]
            dbName = data[0]["targetDatabase"]

            self.cursor = self.conn.cursor()
            spQueryString = "use "+dbName+" exec sp_spaceused "+tableName
            self.cursor.execute(spQueryString)
            jsondata = self.cursor.fetchall()

            totalRowNum = int(re.sub('\s+','',jsondata[0][1]))
            diskSpaceOccupied = int(re.sub('[^0-9]','',jsondata[0][3]))/1024

            queryString = "use "+dbName+" SELECT COUNT(*) AS numberOfColumns "\
                                 "FROM "+dbName+".INFORMATION_SCHEMA.COLUMNS "\
                                 "WHERE TABLE_NAME = N'%s'" % tableName

            self.cursor.execute(queryString)

            columnCount = self.cursor.fetchone()

            data.append(totalRowNum)
            data.append(diskSpaceOccupied)
            data.append(columnCount[0])

            db.dc_columns.update(
                {
                    "cartridge_id" : cartridge_id,
                    "dataset_id" : dataset_id
                },
                {
                    "$set" : {
                        "numberOfRows" : totalRowNum,
                        "diskSpaceOccupied" : diskSpaceOccupied,
                        "numberOfColumns" : columnCount[0],
                        "updated_by" : self.config["MONGO_USER"],
                        "updated_at" : currentDate
                    }
                }
            )

            self.conn.close()
            self.client.close()
            return data

    # def deleteCartridge(self,cartridge_id):
    #     """
    #     Args:
    #         cartridge_id: The unique identifier of a cartridge
    #     Returns: True, if cartridge is sucessfully deleted
    #     """
    #     data = []
    #     db = self.client[self.config["MONGO_DB"]]
    #
    #     getCartridgeName = db.data_cartridge.find(
    #         {
    #             "cartridge_id": cartridge_id
    #         },
    #         {
    #             "_id": 0,
    #             "cartridgeName": 1
    #         }
    #     )
    #     for cartridge in getCartridgeName:
    #         data.append(cartridge)
    #
    #     db.data_cartridge.delete_one(
    #         {
    #             "cartridge_id": cartridge_id
    #         }
    #     )
    #
    #     db.dc_columns.delete_one(
    #         {
    #             "cartridge_id" : cartridge_id
    #         }
    #     )
    #
    #
    #     db.dc_target_db_details.delete_one(
    #         {
    #             "cartridge_id" : cartridge_id
    #         }
    #     )
    #     self.client.close()
    #     return data


    def deleteDataset(self,cartridge_id,dataset_id):
       """
        Args:
            cartridge_id: The unique identifier for each cartridge
            dataset_id: The unique identifier for each dataset under a cartridge
        Returns: If sucessful, cartridge name along with the dataset name

        """
       now = datetime.datetime.utcnow()

       currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
       data = []
       db = self.client[self.config["MONGO_DB"]]


       # checkCartridgeId = db.data_cartridge.find_one(
       #      {
       #          "cartridge_id": cartridge_id
       #      }
       # )
       # checkDatasetId = db.data_cartridge.find_one(
       #     {
       #         "datasets.dataset_id": dataset_id
       #     }
       # )
       # if (cartridge_id != checkCartridgeId):
       #     return ("Invalid Cartridge ID %s" % cartridge_id)
       # if(dataset_id != checkDatasetId):
       #     return ("Invalid Dataset ID %s" % dataset_id)

       getCartridgeName = db.data_cartridge.find(
           {
               "cartridge_id": cartridge_id,
               "datasets.dataset_id": dataset_id
           },
           {
               "_id": 0,
               "cartridgeName": 1,
               "datasets.$.datasetName": 1
           }
       )
       for cartridge in getCartridgeName:
           data.append(cartridge)
           data.append(cartridge_id)
           data.append(dataset_id)

       db.data_cartridge.update(
           {
               "cartridge_id": cartridge_id
           },
           {
               "$pull": {
                   "datasets": {
                       "dataset_id": dataset_id
                   }
               },
               "$set": {
                   "updated_at": currentDate,
                   "updated_by": self.config["MONGO_USER"]
               }
           }
       )

       db.dc_columns.delete_one(
           {
               "dataset_id": dataset_id
           }
       )


       db.dc_target_db_details.delete_one(
           {
               "dataset_id": dataset_id
           }
       )
       self.client.close()
       return data

    def healthStatus(self):
        #print(self.config["DB_SCHEMA"])
        mongoHealthList = [
             {
                'MONGO_SERVER': self.config["MONGO_SERVER"],
                'MONGO_AUTHDB': self.config["MONGO_AUTHDB"],
                'MONGO_DB': self.config["MONGO_DB"],
                'MONGO_USER' : self.config["MONGO_USER"],
                'MONGO_AUTH_MECHANISM' : self.config["AUTH_MECHANISM"]
             }
        ]

        msSQLHealthList = [
            {
                'DB_SERVER_NAME' : self.config["DB_SERVER"],
                'DB_NAME' : self.config["DB_NAME"],
                'DB_USERNAME' : self.config["DB_USER"]
                #'DB_SCHEMA' : self.config["DB_SCHEMA"]
            }
        ]

        mongoHealthList.append(msSQLHealthList)

        return mongoHealthList

    def configParamStatus(self):
        #print(self.config["DB_SCHEMA"])
        configParamList = [
             {
                'MONGO_SERVER': self.config["MONGO_SERVER"],
                'MONGO_AUTHDB': self.config["MONGO_AUTHDB"],
                'MONGO_DB': self.config["MONGO_DB"],
                'MONGO_USER' : self.config["MONGO_USER"],
                'MONGO_AUTH_MECHANISM' : self.config["AUTH_MECHANISM"],
                'DB_SERVER_NAME': self.config["DB_SERVER"],
                'DB_NAME': self.config["DB_NAME"],
                'DB_USERNAME': self.config["DB_USER"]
             }
        ]

        return configParamList


    def deleteCartridge(self):
        """
        Deletes all cartridges specified by id in an array
        :return: Success message on delete
        """
        now = datetime.datetime.utcnow()

        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        db = self.client[self.config["MONGO_DB"]]
        data = request.json
        result = []
        for cartridge_id in data['ids']:
            checkCartridgeId = db.data_cartridge.find_one(
                {
                    "cartridge_id": cartridge_id
                },
                {
                    "_id":0
                }
            )
            result.append(checkCartridgeId)
            db.data_cartridge.update(
                {
                        "cartridge_id": cartridge_id
                },
                {
                    "$set":{
                        "isActive":0,
                        "updated_by":self.config["MONGO_USER"],
                        "updated_at":currentDate
                    }
                }
            )

            # db.dc_columns.delete_one(
            #         {
            #             "cartridge_id": cartridge_id
            #         }
            #     )
            #
            #
            # db.dc_target_db_details.delete_one(
            #         {
            #             "cartridge_id": cartridge_id
            #         }
            #     )
        self.client.close()
        return (result)

    def allCustomerCartridges(self, limit, offset, client_id,app_id):
        """
                Retrieves list of all cartridges
                Returns: Data if retrieval is successful

                """
        data = []
        finalCartridges=[]
        cartridgeCount =0
        db = self.client[self.config["MONGO_DB"]]

        customerCartridges=db.clients.find(
            {"clientid":client_id},
            {"_id":0,"cartridges":1}
        )

        #print(customerCartridges)
        for cartridge in customerCartridges:
            data.append(cartridge)
            if(data=="[{}]"):
                return None, cartridgeCount

        #print(data)
        cartridges=data[0]['cartridges']
        #print(cartridges)
        for i in cartridges:
            #print(i['cartridge_id'])
            globalCartridges=db.data_cartridge.find(
                {"cartridge_id":i['cartridge_id'],"isActive":1},
                {"_id":0}
            )
            #print(globalCartridges)

            for all in globalCartridges:
                finalCartridges.append(all)
            cartridgeCount=len(finalCartridges)

        self.client.close()
        return finalCartridges, cartridgeCount

    def customerCustomiseCartridgeInfo(self,searchTerm,client_id,app_id):
        """
        Retrieves cartridge details based on a search term

        :return: Cartridge details based on search criteria
        """

        db = self.client[self.config["MONGO_DB"]]

        #cartData = request.json
        data = []
        #searchTerm = cartData['searchTerm']
        custCartridge=[]
        finalCartridges= []

        customerCartridges = db.clients.find(
            {"clientid": client_id},
            {"_id": 0,"cartridges.cartridge_id":1}
        )

        for cartridge in customerCartridges:
            for i in cartridge['cartridges']:
                data.append(i['cartridge_id'])

        print(data)
        getAllCartridges=db.data_cartridge.find(
            {
                "cartridge_id":{ "$in" : data },
                "isActive":1
            },
            {
                "cartridgeName":1,
                "_id":0
            }
        )

        for cartridges in getAllCartridges:

            if(re.search(searchTerm,cartridges['cartridgeName'],re.IGNORECASE)):
                custCartridge.append(cartridges['cartridgeName'])
        print(custCartridge)
        AllCartridges=db.data_cartridge.find(
            {
                "cartridgeName":{ "$in" : custCartridge }
            },
            {
                "_id":0
            }
        )
        for cart in AllCartridges:
            finalCartridges.append(cart)

        self.client.close()
        return finalCartridges

    def createEngagementCartridge(self,appid,clientid):
        """
        This method creates a new data cartridge
        based on the parameters received through the input JSON
        Returns: Data inserted if successful

        """

        now = datetime.datetime.utcnow()
        db = self.client[self.config["MONGO_DB"]]
        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        data = request.json
        cartridge_id = uuid.uuid4().hex

        db.di_data_cartridge.insert({"cartridgee_id": cartridge_id,
                                 "name": data["cartridge_name"],
                                 "description": data["cartridge_description"],
                                 "conn_id" : data["conn_id"],
                                 "appid" : appid,
                                 "clientid" : clientid,
                                 "tables" : [],
                                 #"total_rows" : 0,
                                 #"total_disk_space" : 0,
                                 #"isActive" :1,
                                 #"cartridgeType": data["cartridgeType"],
                                 #"isActive": data["isActive"],
                                 #"lastDataRefresh": currentDate,
                                 #"refreshFrequency": data["refreshFrequency"],
                                 "created_by": self.config["MONGO_USER"],
                                 "created_at": currentDate,
                                 "updated_by": self.config["MONGO_USER"],
                                 "updated_at":currentDate

                                 })



        connId = data['conn_id']

        connectionParam = db.di_connections.find({"conn_id": connId},
                                                 {
                                                     "_id": 0,
                                                     "conn_id": 1,
                                                     "name": 1,
                                                     "db_name": 1,
                                                     "schema_name": 1,
                                                     "host": 1,
                                                     "user_name": 1,
                                                     "password": 1,
                                                     "port": 1,
                                                     "db_type": 1,
                                                     "source_type": 1
                                                 })
        for conParams in connectionParam:


            dbName = conParams['db_name']
            tableSchema = conParams['schema_name']
            user = conParams['user_name']
            host = conParams['host']
            passw = conParams['password']

            self.pgconn = psycopg2.connect(
                "dbname=" + dbName + " user=" + user + " host=" + host + " password=" + passw)

            self.cursor1 = self.pgconn.cursor()

            query_string1 = "SELECT table_name " \
                            "FROM " + dbName + ".information_schema.tables WHERE " \
                                               "table_type = 'BASE TABLE' AND table_schema= '" + tableSchema + "'"

            self.cursor1.execute(query_string1)

            self.cursor2 = self.pgconn.cursor()

            query_string2 = "SELECT pg_size_pretty(pg_database_size('{}'))".format(dbName)

            self.cursor2.execute(query_string2)

            diskSize = self.cursor2.fetchone()[0]

            self.cursor3 = self.pgconn.cursor()

            query_string3 = "SELECT CAST(SUM(n_live_tup) as INTEGER) as total_rows " \
                            "FROM pg_stat_user_tables " \
                            "where schemaname = '"+tableSchema+"'"

            self.cursor3.execute(query_string3)

            total_rows = self.cursor3.fetchone()[0]


            table_names_dict = self.cursor1.fetchall()
            # print(table_names_dict)
            tableNames = [i for (i,) in table_names_dict]
            print(tableNames)
            numTables = len(tableNames)
            #diconnSets[0]['tableList'] = tableNames
            db.di_data_cartridge.update({"cartridgee_id": cartridge_id},
                                     {"$set": {"tables": tableNames}},
                                     multi=True)

            db.di_data_cartridge.update(
                {'cartridgee_id': cartridge_id},
                {
                    "$set": {
                        "metrics": {
                            "no_of_tables" : numTables,
                            "no_of_rows": total_rows,
                            "size": diskSize

                        }
                    }

                }
            )


        self.client.close()
        return data

    def createEngagementDataSet(self, cartridge_id,clientid,appid):
        """
        This method creates a new dataset
        based on the parameters received through
        the input JSON for a specific cartridge
        Returns: Data inserted if successful

        """
        now = datetime.datetime.utcnow()

        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        db = self.client[self.config["MONGO_DB"]]
        data = request.json

        dataset_id = uuid.uuid4().hex

        #orig_dataset_id = data["orig_dataset_id"]
        connectionId = data["conn_id"]
        datasetTable = data["datasetTable"]

        connectionParam = db.di_connections.find({"conn_id": connectionId},
                                                 {
                                                     "_id": 0,
                                                     "conn_id": 1,
                                                     "name": 1,
                                                     "db_name": 1,
                                                     "schema_name": 1,
                                                     "host": 1,
                                                     "user_name": 1,
                                                     "password": 1,
                                                     "port": 1,
                                                     "db_type": 1,
                                                     "source_type": 1
                                                 })
        for conParams in connectionParam:

            dbName = conParams['db_name']
            tableSchema = conParams['schema_name']
            user = conParams['user_name']
            host = conParams['host']
            passw = conParams['password']


        self.pgconn = psycopg2.connect(
                "dbname=" + dbName + " user=" + user + " host=" + host + " password=" + passw)

        self.cursor1 = self.pgconn.cursor()

        query_string1 = "SELECT COUNT(*) AS TotalRows " \
                            "FROM " + dbName + "." + tableSchema + "." + datasetTable

        #query_string1 = "SELECT COUNT(*) AS TotalRows " \
        #                    "FROM " + dbName +  "." + datasetTable

        self.cursor1.execute(query_string1)

        self.cursor2 = self.pgconn.cursor()

        query_string2 = "SELECT COUNT(columns) AS TotalColumns "\
                            "FROM "+ dbName + ".information_schema.columns WHERE " \
                            "table_name='" +datasetTable +"' AND table_schema='"+ tableSchema+"'"

        self.cursor2.execute(query_string2)

        self.cursor3 = self.pgconn.cursor()

        query_string3 = "SELECT pg_relation_size(quote_ident(table_name)) as DiskSpace "\
                            "FROM "+ dbName + ".information_schema.tables WHERE "\
                            "table_name='" + datasetTable +"' AND table_schema='"+tableSchema+ "'"

        self.cursor3.execute(query_string3)

        countRows = self.cursor1.fetchone()[0]
        #print(countRows)
        countColumns = self.cursor2.fetchone()[0]
        #print(countColumns)
        diskSpaceSize = self.cursor3.fetchone()[0]
        #print(diskSpaceSize)


        '''metricsCusror = db.dc_columns.find(
            {
                "dataset_id" : orig_dataset_id
            },
            {
                "_id":0,
                "numberOfColumns":1,
                "numberOfRows":1,
                "diskSpaceOccupied" :1
            }
        )
        for metrics in metricsCusror:
            numRows = metrics['numberOfRows']
            numCols = metrics['numberOfColumns']
            diskSpaceSize = metrics['diskSpaceOccupied']
            #print(numRows)
            #print(numCols)
        '''

        db.di_data_cartridge.update(
            {'cartridgee_id': cartridge_id},
            {
                "$addToSet": {
                    "datasets": {
                        "$each": [{
                            "di_dataset_id": dataset_id,
                            "datasetName": data["datasetName"],
                            "datasetDescription" : data["datasetDescription"],
                            "datasetTable" : data["datasetTable"],
                            "numberOfRows" : countRows,
                            "numberOfColumns" : countColumns,
                            "diskSpaceOccupied" : diskSpaceSize
                        }]
                    }
                },
                "$set": {
                    #"lastDataRefresh": currentDate,
                    "updated_by": self.config["MONGO_USER"],
                    "updated_at": currentDate
                }
            }
        )

        db.di_datasets.insert({"cartridgee_id": cartridge_id,
                             "di_dataset_id": dataset_id,
                              #"datasetId":dataset_id,
                             "datasetName": data["datasetName"],
                             "datasetDescription" : data["datasetDescription"],
                             "datasetTable" : data["datasetTable"],
                             "clientid" : clientid,
                             "appid" : appid,
                             "source_type" : "data_cartridge",
                             "numberOfRows": countRows,
                             "numberOfColumns": countColumns,
                             "diskSpaceOccupied": diskSpaceSize,
                             "created_at" : currentDate,
                             "created_by" : self.config["MONGO_USER"]
                             })

        db.di_data_cartridge.update(
            {
                "cartridgee_id" : cartridge_id
            },
            {
                "$inc" :{
                    "total_rows" : countRows,
                    "total_disk_space" : diskSpaceSize
                },
                "$set" : {
                    "updated_by": self.config["MONGO_USER"],
                    "updated_at": currentDate
                }
            }
        )

        '''for item in data["columns"]:
            db.dm_dc_columns.update(
                {'cartridge_id': cartridge_id,'dataset_id':dataset_id},
                {
                    "$push": {
                        "columns": {
                            "$each": [{
                                #"isPrimaryKey" : item["isPrimaryKey"],
                                "columnName": item["columnName"],
                                #"columnDataType": item["columnDataType"],
                                #"allowNulls" : item["allowNulls"],
                                #"columnDescription" : item["columnDescription"]
                            }]
                        }
                    },
                    "$set": {
                        "created_by": self.config["MONGO_USER"],
                        "created_at": currentDate
                    }
                }
            )
        db.dm_dc_columns.update({"dataset_id": dataset_id},
                {"$set": {"orderByColumnsSql": data["orderByColumnsSql"]}},
                                multi=True)
                                '''

        #stripDatasetName = re.sub('\s+','',data["datasetName"])

        '''db.dm_dc_target_db_details.insert({
            "cartridge_id" : cartridge_id,
            "dataset_id" : dataset_id,
            "datasetId" : dataset_id,
            "datasetName" : data["datasetName"],
            "targetSchema" : data["schemaName"],
            "targetServerType" : "SQL Server",
            "targetHost" : data["hostName"],
            "targetDatabase" : data["dbName"],
            "targetPort" : 1433,
            "targetTable" : data["targetTable"],
            "created_by" : self.config["MONGO_USER"],
            "created_at" : currentDate
         })
         '''

        self.client.close()
        return data

    def showEngagementCartridge(self, cartridge_id, clientid, appid):
        """
                Retrieves dataset names and some basic statistics
                of the input cartridge name
                :param cartridgeName: Unique identifier of a cartridge
                :return: Cartridge details based on input cartridge name
                """
        db = self.client[self.config["MONGO_DB"]]
        cartridgeDatasets = []
        diconnSets = []

        '''getAssignedCartridges = db.clients.find(
            {
                "clientid" : clientid
            },
            {
                "_id":0,
                "cartridges" :1
            }
                                )'''

        datasetsCursor = db.di_data_cartridge.find({"cartridgee_id": cartridge_id},
                                               {"_id": 0,
                                                "conn_id" : 1,
                                                "name": 1,
                                                "cartridgee_id": 1,
                                                "datasets": 1, "description": 1,
                                                "tables" :1
                                                })

        for diCartridgeSets in datasetsCursor:
            diconnSets.append(diCartridgeSets)
            #connId = diCartridgeSets['conn_id']

            '''connectionParam = db.di_connections.find({"conn_id" : connId},
                                                 {
                                                     "_id":0,
                                                     "conn_id":1,
                                                     "name":1,
                                                     "db_name":1,
                                                     "schema_name":1,
                                                     "host":1,
                                                     "user_name":1,
                                                     "password":1,
                                                     "port":1,
                                                     "db_type":1,
                                                     "source_type":1
                                                 })
            for conParams in connectionParam:
                diconnSets[0]['connectionParams']=conParams

                dbName = conParams['db_name']
                tableSchema = conParams['schema_name']
                user = conParams['user_name']
                host = conParams['host']
                passw = conParams['password']

                self.pgconn = psycopg2.connect(
                    "dbname=" + dbName + " user=" + user + " host=" + host + " password=" + passw)

                self.cursor1 = self.pgconn.cursor()

                query_string1 = "SELECT table_name " \
                                "FROM " + dbName + ".information_schema.tables WHERE " \
                                "table_type = 'BASE TABLE' AND table_schema= '"+tableSchema+"'"

                self.cursor1.execute(query_string1)

                table_names_dict = self.cursor1.fetchall()
                # print(table_names_dict)
                tableNames = [i for (i,) in table_names_dict]
                #print(tableNames)
                diconnSets[0]['tableList']=tableNames'''

        #for cartridgeSets in getAssignedCartridges:
        #    cartridgeDatasets.append(cartridgeSets)
            #print(cartridgeDatasets[0]['cartridges'])
            '''phy_tables = []
            for i in range(len(cartridgeDatasets[0]['cartridges'])):
                cartId = (cartridgeDatasets[0]['cartridges'][i]['cartridge_id'])
                getDSTables = db.dc_target_db_details.find(
                    {
                        "cartridge_id" : cartId
                    },
                    {
                        "_id":0,
                        "targetTable" :1,
                        "dataset_id":1
                    }
                )

                for tables in getDSTables:
                    phy_tables.append(tables)

            diconnSets.append(phy_tables)
            '''

            '''#print(cartridgeSets['conn_id'])
            connection_id = cartridgeSets['conn_id']
            connectionCursor = db.di_connections.find(
                {
                    "conn_id" : connection_id
                },
                {
                    "_id":0,
                    "name":1,
                    "db_type":1,
                    "source_type":1,
                    "db_name" :1,
                    "schema_name":1,
                    "user_name" : 1,
                    "host" : 1,
                    "password" :1
                }
            )

            for connStats in connectionCursor:
                cartridgeDatasets[0]['conn_params'] = connStats

                dbName = connStats['db_name']
                tableSchema = connStats['schema_name']
                user = connStats['user_name']
                host = connStats['host']
                passw = connStats['password']

                self.pgconn = psycopg2.connect(
                    "dbname=" + dbName + " user=" + user + " host=" + host + " password=" + passw)

                self.cursor1 = self.pgconn.cursor()

                query_string1 = "SELECT table_name " \
                            "FROM " + dbName + ".information_schema.tables " \
                                               "WHERE table_schema = N'%s' AND table_type = 'BASE TABLE'" \
                                               % (tableSchema)
                self.cursor1.execute(query_string1)

                table_names_dict = self.cursor1.fetchall()
                #print(table_names_dict)
                tableNames = [i for (i,) in table_names_dict]
                print(tableNames)
                cartridgeDatasets[0]['tableList']=tableNames
                '''



        self.client.close()
        return diconnSets

    def showEngagementClientCartridges(self,limit,offset,clientid,appid):
        """
                Retrieves list of all cartridges on a givent client
                Returns: Data if retrieval is successful

                """
        data = []
        db = self.client[self.config["MONGO_DB"]]

        cartridgeCount = db.di_data_cartridge.find({"clientid": clientid}, {"_id": 0}).count()
        getAllCartridges = db.di_data_cartridge.find({"clientid": clientid}, {"_id": 0}) \
            .sort("cartridgeName", pymongo.DESCENDING).skip(int(offset)).limit(int(limit))

        for cartridges in getAllCartridges:
            data.append(cartridges)

        self.client.close()
        return data, cartridgeCount

    def previewEngagementDatasetTable(self,connectionId,datasetName,offset,limit):
        """

        :param connectionId: Get the particular connection id
        :param datasetName: Get the particular dataset table name
        :return: Previews top 100 records from the dataset table
        """
        db = self.client[self.config["MONGO_DB"]]

        connectionCursor = db.di_connections.find(
            {
                "conn_id": connectionId
            },
            {
                "_id": 0,
                "name": 1,
                "db_type": 1,
                "source_type": 1,
                "db_name": 1,
                "schema_name": 1,
                "user_name": 1,
                "host": 1,
                "password": 1
            }
        )

        for connStats in connectionCursor:
            #cartridgeDatasets[0]['conn_params'] = connStats

            dbName = connStats['db_name']
            tableSchema = connStats['schema_name']
            user = connStats['user_name']
            host = connStats['host']
            passw = connStats['password']

        self.pgconn = psycopg2.connect(
            "dbname=" + dbName + " user=" + user + " host=" + host + " password=" + passw)

        self.cursor = self.pgconn.cursor()

        query_string = "SELECT * FROM "+dbName+"."+tableSchema+"."+datasetName+" LIMIT 100"
        #query_string = "SELECT * FROM "+dbName+"."+datasetName+" LIMIT 100"
        self.cursor.execute(query_string)

        data = self.cursor.fetchall()

        self.cursor1 = self.pgconn.cursor()
        query_string1 = "SELECT column_name " \
                        "FROM " + dbName + ".information_schema.columns " \
                                           "WHERE table_name = N'%s'" % datasetName
        self.cursor1.execute(query_string1)

        col_names_dict = self.cursor1.fetchall()
        finalData = []
        colNames = [i for (i,) in col_names_dict]
        for i in range(len(data)):
            finalData1 = OrderedDict({})
            for j in range(len(data[i])):
                finalData1[colNames[j]] = (data[i][j])
            finalData.append(finalData1)
        self.conn.close()
        #print(finalData)
        return (finalData)

    def getEngagementConnections(self,clientid,appid):
        """

        :clientid: The client id of the customer
        :appid: The application id
        :return: Get the assigned connections
        """
        data = []
        db = self.client[self.config["MONGO_DB"]]

        getConnections = db.clients.find(
            {
                "clientid" : clientid
            },
            {
                "_id":0,
                "connections" : 1
            }
        )

        for conn in getConnections:
            for i in range(len(conn['connections'])):
                connId = (conn['connections'][i]['conn_id'])
                print(connId)
                getConnParams = db.di_connections.find(
                    {
                        "conn_id":connId,
                        "source_type" : "data_cartridge"
                    },
                    {
                        "_id":0,
                        "conn_id":1,
                        "user_name":1,
                        "name":1,
                        "host":1,
                        "password":1,
                        "db_name":1,
                        "source_type" : 1
                    }
                )
                for conns in getConnParams:
                    print(conns)
                    data.append(conns)


        self.client.close()
        return data