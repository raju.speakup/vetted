from bat_celery_sdk.celery_creator import MakeCelery
from celery.schedules import crontab
from workers.configuration.config import RunConfig
from workers.configuration.layer_fetch_environ import (
    RESULT_BACKEND,
    BROKER_URL
)


class CeleryObjFactory:
    def __init__(self, type):
        self.type = type
        self.run_conf = RunConfig(self.type)
        self.celery_class = MakeCelery(
            broker_url=BROKER_URL,
            result_backend=RESULT_BACKEND.format(self.run_conf.get_val('STATUS_DB')),
            prefetch=self.run_conf.get_val('PREFETCH'),
            status_table=self.run_conf.get_val('STATUS_TABLE'),
            imports=self.run_conf.get_val('IMPORTS')
        )
        self.celery = self.celery_class.create_object()

    def get_object(self):
        if self.type == 'beat':
            self.celery.conf.update(
                beat_schedule={
                    'test-celery': {
                        'task': self.run_conf.get_val('TASK'),
                        # Every 1 minute
                        'schedule': crontab(minute='*'),
                        'options': {'queue': self.run_conf.get_val('QUEUE')}
                    }
                }
            )
        return self.celery
