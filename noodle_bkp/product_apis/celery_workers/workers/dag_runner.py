from celery.schedules import crontab
from bat_celery_sdk.celery_creator import MakeCelery
from workers.utils.mongo_services import NoodleMongoClient
from workers.utils.task_due_checker import check_task_due
from workers.configuration.settings import (
    TYPE_MAPPING
)
from workers.celery_obj_factory import CeleryObjFactory
from workers.configuration.layer_fetch_environ import (
    COLLECTION_NAME
)
from workers.configuration.config import RunConfig
celery_beat = CeleryObjFactory(type='beat').get_object()

class DagRunner:
    def __init__(self):
        self.collection_name = COLLECTION_NAME
        self.client = NoodleMongoClient()

    def doc_generator(self):
        docs = self.client.get_all_docs(
                        collectionname=self.collection_name,
                        data={},
                        projection={
                            'start_date': 1,
                            'end_date': 1,
                            'interval':1,
                            'dag_id':1,
                            'type':1,
                            'queue':1,
                            'version':1,
                            'name': 1,
                            'description': 1,
                            'app_id': 1,
                            'client_id': 1
                        }
                )

        for doc in docs:
            yield doc

    def runner(self):
        for doc in self.doc_generator():
            print('Got Doc: {} from collection'.format(doc))
            print("Checking if dag is due...")
            print(doc)
            if check_task_due(doc):
                task_type = doc.get('type')
                dag_id = doc.get('dag_id')
                version = doc.get('version')
                queue = doc.get('queue', 'celery')
                interval = doc.get('interval')
                time = doc.get('start_date').split(' ', 1)[-1]
                app_id = doc.get('app_id')
                client_id = doc.get('client_id')
                schedule = '{} {}'.format(interval, time)
                print("dag_id : {} of type {} is due now..".format(dag_id, task_type))
                type_args = TYPE_MAPPING[task_type]
                celery = CeleryObjFactory(type=task_type).get_object()
                run_conf = RunConfig(task_type)
                type_args['obj'].run_dag(
                    celery_obj=celery,
                    dag_id=doc['dag_id'],
                    sign=run_conf.get_val('SIGNATURE'),
                    queue=queue,
                    version=version,
                    schedule=schedule,
                    app_id=app_id,
                    client_id=client_id
                )


@celery_beat.task
def run():
    obj = DagRunner()
    obj.runner()



