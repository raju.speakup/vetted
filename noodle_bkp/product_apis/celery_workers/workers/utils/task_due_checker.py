import datetime
from workers.configuration.layer_fetch_environ import DATE_FORMAT


def check_task_due(data):
    start_time = data['start_date']
    end_time = data['end_date']
    interval = data['interval']
    current_time = datetime.datetime.now()
    start_time_obj = str_to_datetime(start_time)
    end_time_obj = str_to_datetime(end_time)
    # Schedule has expired as current_time > end_time
    print("current_time : {} >=  start_time: {} --> {}".format(
                                    current_time,
                                    start_time,
                                    current_time >= start_time_obj
                                    )
                                )

    print("current_time : {} <=  end_time: {} --> {}".format(
                                    current_time,
                                    end_time,
                                    current_time <= end_time_obj
                                    )
                                )
    if start_time_obj <= current_time <= end_time_obj:
        print("current_minute : {} == start_minute : {} and current_hour : {} == start_hour : {}".format(
                                    current_time.minute,
                                    start_time_obj.minute,
                                    current_time.hour,
                                    start_time_obj.hour
                                    )
                                )
        if (current_time.minute == start_time_obj.minute and
                current_time.hour == start_time_obj.hour):
            return True
    return False


def str_to_datetime(date_string):
    return datetime.datetime.strptime(date_string, DATE_FORMAT)