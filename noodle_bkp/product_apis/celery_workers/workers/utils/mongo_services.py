from pymongo import MongoClient, ASCENDING

from workers.configuration.layer_fetch_environ import (
    DB_NAME, SERVER_NAME, PASSWORD, USERNAME, DEBUG)


class NoodleMongoClient:
    """NoodleMongoClient."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        mongo_url = "".join(
            ['mongodb://', USERNAME, ':',
             PASSWORD, '@', SERVER_NAME])

        if DEBUG:
            mongo_url = None
        self.client = MongoClient(mongo_url)
        self.database = self.client[DB_NAME]

    def find_one(self, collectionname, data):
        """Find One."""
        return self.database[collectionname].find_one(data)

    def get_all_docs(self, collectionname, data, projection):
        """Find Selected."""
        docs = []
        for doc in self.database[collectionname].find(data, projection):
            doc.pop('_id')
            docs.append(doc)
        return docs

    def count_docs(self, collectionname, data):
        """Count Docs"""

        return self.database[collectionname].find(
            data).count()

    def find_with_projection(self, collectionname, query={}, projection={}, offset=0, limit=1, sort_field = "_id"):
        results = []
        db_results = self.database[collectionname].find(query, projection).sort(sort_field, ASCENDING).skip(int(offset)).limit(int(limit))
        results_count = db_results.count()
        for result in db_results:
            results.append(result)
        self.client.close()
        return results, results_count



