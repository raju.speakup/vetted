import json
from config_store import config_store_client as conf_client

class Config:
    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='api-client')

    def get_v(self, key):
        return self.cs.get(key)

    def reload(self):
        self.cs.load(ip='*', service_name='api-client')


class CeleryConfig:
    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='celery-worker-config')

    def get_v(self, key):
        return self.cs.get(key)

    def reload(self):
        self.cs.load(ip='*', service_name='celery-worker-config')


class RunConfig:
    def __init__(self, type):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='celery-worker-config')
        self.type = type

    def get_v(self, key):
        return self.cs.get(key)

    def get_val(self, key):
        conf_key = self.dict_from_string('CONF_MAPPING')[self.type]
        conf_dict = self.dict_from_string(conf_key)
        return conf_dict.get(key)

    def dict_from_string(self, str):
        return json.loads(self.get_v(str))

    def reload(self):
        self.cs.load(ip='*', service_name='celery-worker-config')