from data_ingestion.di_run_trigger import DataIngestionDagRun
from ai_pipeline.ai_run_trigger import AIDagRun
from workers.configuration.config import RunConfig

data_ingestion_obj = DataIngestionDagRun()
ai_pipeline_obj = AIDagRun()

TYPE_MAPPING = {
    'data_ingestion': {
        'signature': RunConfig(type='data_ingestion').get_val('SIGNATURE'),
        'obj': data_ingestion_obj
        },
    'ai_pipeline': {
        'signature': RunConfig(type='ai_pipeline').get_val('SIGNATURE'),
        'obj': ai_pipeline_obj
    }
}
