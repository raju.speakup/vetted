import json


class BuildQuery:

    def __init__(self):
        pass

    def combine_condition(self, conditions, operator):
        combined_cond = ""
        if len(conditions) is None:
            return combined_cond
        elif len(conditions) ==1:
            combined_cond = conditions[0]
        else:
            combined_cond = conditions[0]
            for condition in conditions[1:]:
                combined_cond = " ".join([combined_cond,operator, condition])
            combined_cond = "(" + combined_cond + ")"

        return combined_cond

    def jq_build(self, db_type, json_data):
        data = json_data
        condition_str = ""
        if data:
            if 'combinator' in data:
                condition = []
                for rule in data["rules"]:
                    condition.append(self.jq_build(db_type, rule))
                    condition_str = self.combine_condition(condition, data["combinator"])
            else:
                if db_type == "postgres" or db_type == "mssql":
                    query = """"{0}" {1} '{2}'""".format(data["field"], data["operator"], data["value"])
                elif db_type == "hive":
                    query = "`{0}` {1} '{2}'".format(data["field"], data["operator"], data["value"])
                else:
                    query = """"{0}" {1} '{2}'""".format(data["field"], data["operator"], data["value"])
                return query
        return condition_str

    def transform_query_build(self, table_name, json_data, db_type='postgres'):
        update_conditions = json_data['update_conditions']
        where_conditions = json_data['where_conditions']

        if db_type == "postgres" or db_type == "mssql":
            update_query_conditions = ''
            for update_doc in update_conditions:
                update_query_conditions += "{} = '{}',".format(update_doc['field'], update_doc['new_value'])
            update_query_conditions = update_query_conditions[:-1]

            where_query_conditions = self.jq_build(db_type, where_conditions)
            query = 'update {} set {} where {}'.format(table_name, update_query_conditions, where_query_conditions)

            return query

    @staticmethod
    def join_query_build(table_1, table_2, json_data,schema, db_type='postgres'):
        join_conditions = json_data['join_columns']
        join_columns = {}
        for col in join_conditions:
            join_columns.update(col)
        proj_data = json_data['projection_columns']

        if db_type == "postgres" or db_type == "mssql":
            projection_columns = ''
            for key in proj_data.keys():
                cols = [key + '."' + i + '"' for i in proj_data[key]]
                if not (proj_data[key]):
                    projection_columns = projection_columns + ','.join(cols)
                else:
                    projection_columns = projection_columns + ','.join(cols) + ','

            projection_columns = projection_columns[:-1]
            query = 'select {} from {}.{} t1 inner join {}.{} t2 on ('.format(projection_columns, schema,
                                                                              table_1, schema, table_2)

            for key in join_columns.keys():
                query = query + 't1.' + key + ' = ' + 't2.' + join_columns[key] + ' and '
            query = query + '1 = 1)'

            query = query + ' limit (select count(1) from {}.{} )'.format(schema, table_1)
            return query

    @staticmethod
    def create_table_query_build(table_name, json_data, db_type='postgres'):
        columns = json_data['columns']
        primary_keys_data = json_data['primary_keys']

        if db_type == "postgres" or db_type == "mssql":
            column_details = ''
            for column in columns:
                column_details += "{} {} {} ,".format(column['column_name'], column['data_type'],
                                                      column['allow_null'])
            if len(primary_keys_data) == 1:
                primary_keys = str(tuple(primary_keys_data))
                primary_keys = (str(primary_keys.split(',')[0]) + ")").replace("'", "")
            else:
                primary_keys = str(tuple(primary_keys_data))
                primary_keys = primary_keys.replace("'", "")

            if len(primary_keys_data) == 0:
                query = 'CREATE TABLE  {} ({});'.format(table_name, column_details.rstrip(','))
            else:
                query = 'CREATE TABLE  {} ({} PRIMARY KEY {});'.format(table_name, column_details, primary_keys)

            return query

    @staticmethod
    def create_new_columnar_table_query(table_name, json_data, db_type='postgres'):
        columns = json_data['columns']
        primary_keys_data = json_data['primary_keys']

        if db_type == "postgres" or db_type == "mssql":
            column_details = ''
            for column in columns:
                column_details += "{} {} {} ,".format(column['column_name'], column['data_type'],
                                                      column['allow_null'])
            if len(primary_keys_data) == 1:
                primary_keys = str(tuple(primary_keys_data))
                primary_keys = (str(primary_keys.split(',')[0]) + ")").replace("'", "")
            else:
                primary_keys = str(tuple(primary_keys_data))
                primary_keys = primary_keys.replace("'", "")

            if len(primary_keys_data) == 0:
                query = "CREATE FOREIGN TABLE  {} ({}) SERVER cstore_server OPTIONS(compression 'pglz');".format(table_name, column_details.rstrip(','))
            else:
                query = "CREATE FOREIGN TABLE  {} ({} PRIMARY KEY {}) SERVER cstore_server OPTIONS(compression 'pglz');".format(table_name, column_details, primary_keys)

            return query

    @staticmethod
    def create_existing_columnar_table_query(table_name, columns, db_type='postgres'):

        if db_type == "postgres" or db_type == "mssql":
            column_details = ''
            for column in columns:
                column_details += "{} {}{},".format(column[0], column[1], '('+str(column[2])+')' if column[2] else '')

            query = "CREATE FOREIGN TABLE  {} ({}) SERVER cstore_server OPTIONS(compression 'pglz');".format(
                    table_name, column_details.rstrip(','))

            return query

    def filter_query_build(self, table_name, json_data,schema, db_type='postgres'):
        where_conditions = json_data['where_conditions']

        if db_type == "postgres" or db_type == "mssql":
            where_query_conditions = self.jq_build('postgres', where_conditions)
            query = 'select * from {}.{} where {}'.format(schema, table_name, where_query_conditions)

            return query

    def filter_column(self, table_name, json_data, db_type='postgres'):
        col_list = json_data['selected_columns']
        col_csv = ','.join('"' + col + '"' for col in col_list)

        if db_type == "postgres" or db_type == "mssql":
            query = 'select {0} from {1}'.format(col_csv, table_name)

            return query

    @staticmethod
    def copy_table_data_query(source, target, columns_list, schema, db_type='postgres'):
        columns = str(tuple(columns_list)).replace("'", "")

        if db_type == "postgres" or db_type == "mssql":
            query = " do $$ BEGIN IF EXISTS ( SELECT 1 FROM   information_schema.tables WHERE  table_name = '{0}') \
                                       THEN \
                                       INSERT INTO {4}.{0}{2} SELECT {3} from {4}.{1} ;\
                                       ELSE \
                                       CREATE TABLE {4}.{0} AS SELECT {3} FROM {4}.{1} ;\
                                       END IF ;\
                                       END; $$ LANGUAGE plpgsql;".format(source, target, columns,
                                                                         columns.replace(")", "").replace("(", ""),
                                                                         schema)
            return query

    @staticmethod
    def drop_temp_table_query(table_name, schema, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = " do $$ BEGIN IF EXISTS ( SELECT 1 FROM   information_schema.tables WHERE  table_name = '{0}') \
                                                       THEN \
                                                       DROP TABLE {1}.{0} ;\
                                                       END IF ;\
                                                       END; $$ LANGUAGE plpgsql;".format(table_name.replace("'", ""),schema )
            return query

    @staticmethod
    def get_cols_list_query(table_name, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = """select column_name from information_schema.columns where table_name = '""" + table_name + \
                    """'"""
            return query

    @staticmethod
    def get_cols_schema_list_query(table_name, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = """select column_name, data_type, character_maximum_length from information_schema.columns where 
            table_name = '""" + table_name + \
                    """'"""
            return query

    @staticmethod
    def truncate_table(table_name, schema, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = " TRUNCATE TABLE {}.{}".format(schema, table_name.replace("'", ""))
            return query

    @staticmethod
    def remote_data_transfer(remote_table, columns, local_table, db_meta, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":

            column_schema = ''
            for column in columns:
                column_schema += "{} {} ,".format(column[0], column[1])
            column_schema = column_schema.rstrip(',')

            query = "SELECT {0}.* INTO {1} FROM \
            dblink('dbname={2} port={3} host={4} user={5} password={6}','SELECT * FROM {7}.{8}') AS {0}({9});" \
                .format('tmp', local_table, db_meta['db_name'], db_meta['port'], db_meta['host'], db_meta['user'],
                        db_meta['password'], db_meta['schema_name'], remote_table, column_schema)
            return query

    @staticmethod
    def get_table_view_query_build(schema_name, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = "SELECT table_name FROM information_schema.tables WHERE table_schema='{0}'" \
                    "UNION \
                     SELECT table_name FROM information_schema.views WHERE table_schema='{0}'".format(schema_name)

            return query

    @staticmethod
    def total_rows_query(schema_name, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = "SELECT CAST(SUM(n_live_tup) as INTEGER) as total_rows FROM pg_stat_user_tables " \
                    "where schemaname = '{}'".format(schema_name)

            return query

    @staticmethod
    def total_size_query(db_name, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            query = "SELECT pg_database_size('{}')".format(db_name)

            return query

    @staticmethod
    def create_datamart_query(table_name, schema, schema_name, db_type='postgres'):
        if db_type == "postgres" or db_type == "mssql":
            column_details = ''
            for column in schema:
                column_details += "{} {}{},".format(column[0], column[1],
                                                    '(' + str(column[2]) + ')' if column[2] else '')

            query = "CREATE TABLE  {}{} ({}) ;".format(schema_name,
                table_name, column_details.rstrip(','))

            return query


'''
test = {
  "combinator": "OR",
  "nodeName": "1",
  "rules": [{
      "combinator": "AND",
      "nodeName": "1/1",
      "rules": [{
          "field": "firstName",
          "operator": "=",
          "value": "test",
          "nodeName": "1/1/1"
        },
        {
          "field": "age",
          "operator": ">",
          "value": "20",
          "nodeName": "1/1/2"
        }
      ]
    },
    {
      "combinator": "AND",
      "nodeName": "1/2",
      "rules": [{
          "field": "firstName",
          "operator": "=",
          "value": "test2",
          "nodeName": "1/2/1"
        },
        {
          "field": "age",
          "operator": "<",
          "value": "50",
          "nodeName": "1/2/2"
        }
      ]
    }
  ]
}
Reference
BQObject = BuildQuery()
print(BQObject.jq_build(json_data=json.loads(open("test").read())))
'''