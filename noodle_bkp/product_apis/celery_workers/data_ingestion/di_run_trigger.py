import uuid
from datetime import datetime
from data_ingestion.services.dag_run import DataIngestionDagRunService
from data_ingestion.services.di_dag import DataIngestionDagService
from data_ingestion.configuration.layer_fetch_environ import (
    DB_NAME,
    COLLECTION,
    DATE_FORMAT
)
from data_ingestion.services.node_status import NodeStatus
from data_ingestion.utils.mongo_db import MongoDBClient
from data_ingestion.services.run_status import RunStatus

class DataIngestionDagRun:

    def __init__(self):
        self.db_name = DB_NAME
        self.service = DataIngestionDagRunService(self.db_name)
        self.dag_service = DataIngestionDagService(self.db_name)
        self.mongo = MongoDBClient()

    def get_details(self, dag_id, run_version):
        dag = self.dag_service.get_dag_version(dag_id, run_version)
        pg_meta = self.service.get_db_details_from_dag_id(dag_id)
        run_id = str(uuid.uuid4())
        nodes = self.service.get_dag_nodes(dag_id)
        # nodes = dag['nodes']
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata']
            }
            )
            if node['type'] in ("DS", "DC", "DM"):
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.save_test_dag(node_doc)
        return dag, pg_meta, run_id

    def run_dag(self, celery_obj, dag_id, sign, queue, version, schedule, app_id, client_id):
        if self.service.check_dag_running(dag_id):
            print("dag_id : {} is already running".format(dag_id))
        else:
            sig = celery_obj.signature(sign)
            dag, pg_meta, run_id = self.get_details(dag_id, version)
            name = dag.get('name')
            desc = dag.get('description')
            self.service.set_dag_run(dag_id)
            start_time = datetime.now().strftime(DATE_FORMAT)
            msg = "Queuing DI pipeline {}. DAG_ID : {}. RUN_ID : {}. QUEUE : {} ".format(name, dag_id, run_id, queue)
            sig.apply_async(
                args=[run_id, dag_id, dag, start_time, version, pg_meta],
                queue=queue,
                task_id=run_id
            )
            self.mongo.insert_one(
                db_name=self.db_name,
                collection_name=COLLECTION,
                data={
                    'run_id': run_id,
                    'dag_id': dag_id,
                    'meta': {},
                    'percentage': 0,
                    'log': [msg],
                    'name': name,
                    'description': desc,
                    'source': 'scheduler',
                    'schedule': schedule,
                    'start_time': start_time,
                    'end_time': start_time,
                    'version': version,
                    'duration': '0 seconds',
                    'type': 'data_ingestion',
                    'status': RunStatus().InProgress,
                    'app_id': app_id,
                    'client_id': client_id
                }
            )