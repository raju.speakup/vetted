from datetime import datetime
from data_ingestion.services.dag_run import DataIngestionDagRunService
from data_ingestion.services.dag_run import DagRun
from data_ingestion.configuration.layer_fetch_environ import (
    DB_NAME,
    COLLECTION,
    DATE_FORMAT
)
from data_ingestion.utils.json_utils import to_json
from data_ingestion.utils.mongo_db import MongoDBClient
from data_ingestion.services.run_status import RunStatus
from workers.celery_obj_factory import CeleryObjFactory


celery = CeleryObjFactory(type='data_ingestion').get_object()
service = DataIngestionDagRunService(DB_NAME)
mongo = MongoDBClient()
run_doc = dict()
@celery.task
def dag_runner(run_id, dag_id, dag, start_time, version, pg_meta):
    try:
        run_service = DagRun(pg_meta)
        name = dag.get('name')
        msg = "DI pipeline {} received py the worker.".format(name)
        print(msg)
        service.update_run_log(run_id, msg)
        msg = "Running DI pipeline : {} ".format(name)
        print(msg)
        service.update_run_log(run_id, msg)
        output = run_service.run_dag(run_id, dag_id,dag)
        end_time = datetime.now().strftime(DATE_FORMAT)
        if output is True:
            run_service.remove_temp_data(run_id)
            run_service.add_dag_run(dag_id, run_id, start_time, end_time, version, status='C')
            run_doc.update(
                {
                    'status': RunStatus().Success,
                    'end_time': end_time,
                    'duration': service.get_duration(start_time, end_time)

                }
            )
            msg = "DI Pipeline : {} completed successfully".format(name)
            print(to_json({"message": msg}, is_error=False))
            service.update_run_log(run_id, msg)

        else:
            run_service.remove_temp_data(run_id)
            run_service.add_dag_run(dag_id, run_id, start_time, end_time, version, status='F')
            # return to_json({"message": output}, is_error=True)
            run_doc.update(
                {
                    'status': RunStatus().Failure,
                    'end_time': end_time,
                    'duration': service.get_duration(start_time, end_time)

                }
            )

            print(to_json({"message": output}, is_error=True))
            service.update_run_log(run_id, output)
    except Exception as err:
        end_time = datetime.now().strftime(DATE_FORMAT)
        run_doc.update(
            {
                'status': RunStatus().Failure,
                'end_time': end_time,
                'duration': service.get_duration(start_time, end_time)

            }
        )
        service.update_run_log(run_id, err.args[0])

    mongo.find_one_and_update(
        db_name=DB_NAME,
        collection_name=COLLECTION,
        query={
                'run_id': run_id
            },
        update={
                '$set': run_doc
            }
    )
    service.unset_dag_run(dag_id)
    msg = 'Postprocessing Complete.'
    print(msg)
    service.update_run_log(run_id, msg)



