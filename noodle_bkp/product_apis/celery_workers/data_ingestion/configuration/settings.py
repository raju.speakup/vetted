from data_ingestion.di_run_trigger import DataIngestionDagRun
from workers.configuration import celery_config

data_ingestion_obj = DataIngestionDagRun()

DATE_FORMAT = celery_config.get_v('DATE_FORMAT')
COLLECTION_NAME = celery_config.get_v('COLLECTION_NAME')

TYPE_MAPPING = {
    'data_ingestion': {
        'signature': celery_config.get_v('DATA_INGESTION_SIGNATURE'),
        'obj': data_ingestion_obj
        }
}
