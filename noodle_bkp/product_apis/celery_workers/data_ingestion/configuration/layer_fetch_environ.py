#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fetch all confidencial data from and environment file
    Functionality:
        - If any manupulations needed in environment setting
        this is place to do it

"""
# ------------------------------------------------------------------------------
# IMPORT SECTION
# ------------------------------------------------------------------------------

#import os

#from dotenv import load_dotenv
from ast import literal_eval
from data_ingestion.configuration.config import Config, CeleryConfig

# ------------------------------------------------------------------------------
# CONFIGURING ENVIRONMENT FILE AND FETCHING DATA
# ------------------------------------------------------------------------------

#APP_ROOT = os.path.join(os.path.dirname(__file__), '../..')
#dotenv_path = os.path.join(APP_ROOT, '.env')
#load_dotenv(dotenv_path)


# ------------------------------------------------------------------------------
# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES
# ------------------------------------------------------------------------------

#SERVER_NAME = os.getenv(os.getenv("MODE") + '_' + "SERVER_NAME")
#DB_NAME = os.getenv(os.getenv("MODE") + '_' + "DB_NAME")
#USERNAME = os.getenv(os.getenv("MODE") + '_' + "USERNAME")
#PASSWORD = os.getenv(os.getenv("MODE") + '_' + "PASSWORD")
#PORT = int(os.getenv(os.getenv("MODE") + '_' + "PORT"))
#SESSION_TIMEOUT = int(os.getenv(os.getenv("MODE") + '_' + "SESSION_TIMEOUT"))
#DEFAULT_IP = os.getenv("DEFAULT_IP")
#LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")
#MODE = os.getenv("MODE")
#DEBUG = True if os.getenv("MODE") == 'LOCAL' else False
#SECRETE_KEY = os.getenv("SECRET_KEY", default="some-difficult-value")
#TOKEN_EXPIRE_TIME = int(os.getenv("TOKEN_EXPIRE_TIME", default=15))
#REFRESH_EXPIRE_TIME = int(os.getenv("REFRESH_EXPIRE_TIME", default=120))
#LDAP_URL = os.getenv("LDAP_URL", default="localhost")

# ------------------------------------------------------------------------------
# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES USING CONFIG STORE
# ------------------------------------------------------------------------------

conf = Config()

DB_NAME = conf.get_v('db.name')
DB_URL = conf.get_v('db.url')

celery_config = CeleryConfig()

BROKER_URL = celery_config.get_v('BROKER_URL')
RESULT_BACKEND = celery_config.get_v('RESULT_BACKEND')
COLLECTION = celery_config.get_v('RUN_STATUS_COLLECTION')
DATE_FORMAT = celery_config.get_v('DATE_FORMAT')
INTERVAL = literal_eval(celery_config.get_v('INTERVAL'))
POSTGRES_DB = celery_config.get_v('POSTGRES_DB')