import psycopg2
import psycopg2.extras



class PostgresDBClient:
    def __init__(self, db_name, user, password, host, port ):
        self.url = "dbname={} user={} password={} host={} port={}".format(db_name, user, password, host, port)
        self.connection = psycopg2.connect(self.url)
        self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def execute_query(self, query):
        self.cursor.execute(query)


#pg_db_client = PostgresDBClient('postgres','postgres', 'postgres', 'localhost', '5432')
