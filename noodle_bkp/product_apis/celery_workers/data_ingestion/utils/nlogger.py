import logging


class NLogger():

    def __init__(self):
        logging.basicConfig(filename='celery-workers.log', level=logging.DEBUG)

    def log(self):
        return logging

logger = NLogger()