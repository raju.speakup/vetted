from data_ingestion.services.QBuilder import BuildQuery
from data_ingestion.utils.mongo_db import MongoDBClient
from workers.celery_obj_factory import CeleryObjFactory
celery = CeleryObjFactory(type='columnar').get_object()

class Columnar:
    def __init__(self):
        self.builder = BuildQuery()
        self.mongo = MongoDBClient()

    def create_existing_columnar_table(self, data, conn):
        old_table_name = str.lower(data['table_name'])
        table_name = 'col_'+old_table_name

        columns_query = self.builder.get_cols_schema_list_query(old_table_name, 'postgres')

        cur = conn.cursor

        try:
            cur.execute(columns_query)
            row = cur.fetchone()
            columns = []
            while row is not None:
                columns.append(tuple(row))
                row = cur.fetchone()
        except Exception as e:
            return str(e)
        cols_list = [col[0] for col in columns]
        create_query = self.builder.create_existing_columnar_table_query(table_name, columns, 'postgres')
        copy_data_query = self.builder.copy_table_data_query(table_name, old_table_name, cols_list, 'postgres')

        try:
            cur.execute(create_query)
            conn.connection.commit()
            cur.execute(copy_data_query)
            conn.connection.commit()
            return True
        except Exception as e:
            return str(e)


@celery.task
def run(data, conn):
    obj = Columnar()
    obj.create_existing_columnar_table(data, conn)