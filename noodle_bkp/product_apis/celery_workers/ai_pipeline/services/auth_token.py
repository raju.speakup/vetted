import json
from ai_pipeline.utils.RestUtils import rest_client_post
from ai_pipeline.configuration.layer_fetch_environ import (
    USERNAME,
    PASSWD,
    AUTH_URL,
    APP_ID,
    CLIENT_ID
)


class AuthToken:
    def __init__(self):
        self.url = AUTH_URL
        self.payload = dict()
        self.payload.update(
            {
                "email": USERNAME,
                "password": PASSWD
            }
        )

    def get_token(self):
        try:
            response = rest_client_post(token=None, url=self.url, payload=json.dumps(self.payload))
        except Exception as err:
            return False, err.args[0]
        status_code = response.status_code
        if status_code != 200:
            msg = "failed to get auth token"
            return False, msg
        json_data = json.loads(response.text)
        auth_token = json_data['accessToken']
        return True, 'Bearer {}'.format(auth_token)

    def get_request_headers(self):
        header = dict()
        header.update(
            {
                "clientid": CLIENT_ID,
                "appid": APP_ID,
                "Authorization": self.get_token()[1]

            }
        )

        return header