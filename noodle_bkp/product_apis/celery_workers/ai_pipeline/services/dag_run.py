import time
import uuid
import datetime
from workers.utils.task_due_checker import str_to_datetime
from ai_pipeline.utils.postgres_db import PostgresDBClient
from ai_pipeline.utils.json_utils import to_json
from ai_pipeline.services.node_status import NodeStatus
from ai_pipeline.utils.mongo_db import db_client
from ai_pipeline.utils.RestUtils import rest_client_post
from celery_docker.docker_utils import (
    create_container,
    create_nb_container
)
from ai_pipeline.services.auth_token import AuthToken
from ai_pipeline.configuration.layer_fetch_environ import (
    DB_NAME,
    COLLECTION,
    NB_RUN_URL,
    RETRY,
    SLEEP,
    INTERVAL
)
from pymongo import DESCENDING
import json


class DagRun:

    def __init__(self):
        self.db_name = DB_NAME
        self.mongo_client = db_client
        self.postgress_db_service = PostgresDBClient('bat_data_ingestion', 'batuser', 'Noodle1234', '192.168.10.59',
                                                     "5432")
        self.service = AIDagRunService(DB_NAME)

    def run_nb(self, nb_url, name, r_id):
        create_nb_container(nb_url, name, r_id)

    def run_dag(self, run_id, dag_id, dag_doc):
        de = DagExecutor()
        r_ids = list()
        while True:
            val_dict = dict()
            return_object, count = self.mongo_client.find(self.db_name, "ai_dag_test", {"run_id": run_id,
                                                                                        "status": {"$ne": "C"}}
                                                          )
            failed_node, count = self.mongo_client.find(self.db_name, "ai_dag_test", {"run_id": run_id,
                                                                                      "status": "F"})

            if failed_node:
                return "DAG testing failed", None

            if return_object:

                output, r_id, nb_id = de.execute(run_id, dag_doc)
                print("IN run dag: {},  {}, {} ".format(output, r_id, nb_id))
                val_dict.update(
                    {
                        'nb_id': nb_id,
                        'r_id': r_id
                    }
                )
                r_ids.append(val_dict)
                if output is not True:
                    return output, r_ids

            else:
                print("No more nodes to execute.")
                return True, r_ids
            self.service.update_dag_completion(run_id=run_id, dag_id=dag_id)

    def get_runs_details(self, dag_id):
        output, count = self.mongo_client.find(self.db_name, "ai_dag_runs", {"dag_id": dag_id}, limit=100)

        result = []
        for doc in output:
            del doc['_id']
            result.append(doc)

        return result

    def add_dag_run(self, dag_id, run_id, start_time, end_time, version, r_ids, status, scheduled='NA'):
        run_doc = {
            "dag_id": dag_id,
            "run_id": run_id,
            "start_time": start_time,
            "end_time": end_time,
            "status": status,
            "version": version,
            "scheduled": scheduled,
            "r_ids": r_ids
        }
        res = self.mongo_client.insert_one(self.db_name, "ai_dag_runs", run_doc)
        del run_doc['_id']
        return to_json(run_doc)


class DagExecutor:

    def __init__(self):
        db_name = DB_NAME
        self.nb_run_url = NB_RUN_URL
        self.dag_service = AIDagRunService(db_name)
        self.token_service = AuthToken()
        #self.postgress_db_service = PostgresDBClient('bat_data_ingestion' ,'batuser', 'Noodle1234' ,'192.168.10.59', "5432")

    def execute(self, run_id, dag_doc):
        self.dag_doc = dag_doc
        node = self.dag_service.get_next_scheduled_node(run_id)
        node_type = node['type']
        dag_id, node_id = self.node_ids(node)
        self.dag_service.update_node_status(run_id, node_id, NodeStatus.InProgress)
        self.dag_service.update_node_field(run_id, node_id, 'start_time', str(datetime.datetime.now()))

        msg = "Running node {}.".format(node['name'])
        self.dag_service.update_run_log(run_id, msg)
        n_start = str(datetime.datetime.now())

        if 'DW' == node_type:
            status, r_id, msg = self.execute_dw_node(dag_doc, node)
        elif 'SD' == node_type:
            status, r_id, msg = self.execute_sd_node(dag_doc, node)
        elif 'FE' == node_type:
            status, r_id, msg = self.execute_fe_node(dag_doc, node)
        elif 'MT' == node_type:
            status, r_id, msg = self.execute_mt_node(dag_doc, node)
        n_end = str(datetime.datetime.now())

        self.dag_service.update_run_log(run_id, msg)

        if status is True:
            self.dag_service.update_node_field(run_id, node_id, 'end_time', str(datetime.datetime.now()))
            self.dag_service.update_node_status(run_id, node_id, NodeStatus.Complete)
            self.dag_service.update_child_node_status(dag_doc, run_id, node_id, NodeStatus.Schedule, check_parent_status=True)
            msg = "Node {} completed. Time taken : {}".format(
                node['name'],
                self.dag_service.get_duration(n_start, n_end)
            )
            self.dag_service.update_run_log(run_id, msg)
            return True, r_id, node['nb_id']

        else:
            msg = "Node {} failed.".format(node['name'])
            self.dag_service.update_run_log(run_id, msg)
            return status, r_id, node['nb_id']



    # node execution for datastore and data cartridge
    def execute_dw_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)
        run_id = node['run_id']

        node_doc = self.dag_service.get_sch_node_details(run_id, node_id)

        json_payload = self.dag_service.get_payload_from_nb_node(node_doc)

        run_doc = self.dag_service.create_run_document(json_payload)

        output = self.dag_service.create_nbrun(run_doc)

        r_id = run_doc['r_id']
        nb_url = run_doc['notebook_file_path']

        name = '{}_{}'.format(run_doc['name'], r_id[:5])
        create_container(nb_url, name, run_id, dag_id)

        ''' Check error status in the '''
        error_status = self.dag_service.get_dag_run_error_status(dag_id)
        if error_status:
            msg = "{} notebook execution failed".format(node_doc['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return False, r_id, msg
        else:
            msg = "{} notebook execution completed.".format(node['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return True, r_id, msg


    def execute_sd_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        run_id = node['run_id']

        node_doc = self.dag_service.get_sch_node_details(run_id, node_id)

        json_payload = self.dag_service.get_payload_from_nb_node(node_doc)

        run_doc = self.dag_service.create_run_document(json_payload)

        output = self.dag_service.create_nbrun(run_doc)

        r_id = run_doc['r_id']
        nb_url = run_doc['notebook_file_path']

        name = '{}_{}'.format(run_doc['name'], r_id[:5])
        create_container(nb_url, name, run_id, dag_id)

        ''' Check error status in the '''
        error_status = self.dag_service.get_dag_run_error_status(dag_id)
        print('Error status : {}'.format(error_status))
        if error_status:
            msg = "{} notebook execution failed".format(node_doc['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return False, r_id, msg
        else:
            msg = "{} notebook execution completed.".format(node['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return True, r_id, msg

    def execute_fe_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        run_id = node['run_id']

        node_doc = self.dag_service.get_sch_node_details(run_id, node_id)

        json_payload = self.dag_service.get_payload_from_nb_node(node_doc)

        run_doc = self.dag_service.create_run_document(json_payload)

        output = self.dag_service.create_nbrun(run_doc)

        r_id = run_doc['r_id']
        nb_url = run_doc['notebook_file_path']
        name = '{}_{}'.format(run_doc['name'], r_id[:5])
        create_container(nb_url, name, run_id, dag_id)
        ''' Check error status in the '''
        error_status = self.dag_service.get_dag_run_error_status(dag_id)
        print('Error status : {}'.format(error_status))
        if error_status:
            msg = "{} notebook execution failed".format(node_doc['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return False, r_id, msg
        else:
            msg = "{} notebook execution completed.".format(node['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return True, r_id, msg


    def execute_mt_node(self, dag_doc, node):
        dag_id, node_id = self.node_ids(node)

        run_id = node['run_id']

        node_doc = self.dag_service.get_sch_node_details(run_id, node_id)

        json_payload = self.dag_service.get_payload_from_nb_node(node_doc)

        run_doc = self.dag_service.create_run_document(json_payload)

        output = self.dag_service.create_nbrun(run_doc)

        r_id = run_doc['r_id']
        nb_url = run_doc['notebook_file_path']
        name = '{}_{}'.format(run_doc['name'],r_id[:5])
        create_container(nb_url, name, run_id, dag_id)

        ''' Check error status in the '''
        error_status = self.dag_service.get_dag_run_error_status(dag_id)
        print('Error status : {}'.format(error_status))
        if error_status:
            msg = "{} notebook execution failed".format(node_doc['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return False, r_id, msg
        else:
            msg = "{} notebook execution completed.".format(node['nb_name'])
            self.dag_service.update_run_log(run_id, msg)
            return True, r_id, msg


    def node_ids(self, node):
        dag_id = node['dag_id']
        node_id = node['node_id']
        return dag_id, node_id


class AIDagRunService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'ai_dag_test'
        self.mongo_client = db_client
        self.dag_doc = None

    def save_test_dag(self, sch_doc):
        res = self.mongo_client.insert_one(self.db_name, "ai_dag_test", sch_doc)
        del sch_doc['_id']
        return to_json(sch_doc)

    def get_scheduled_dag(self, dag_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                            query={'dag_id': dag_id, "is_exec": 1})
        for doc in res:
            del doc['_id']
        return res

    def get_parent_node(self, dag_doc, node_id):
        parents = [node['parents'] for node in dag_doc['nodes'] if node['id'] == node_id]
        return parents[0]

    def get_child_node(self, dag_doc, node_id):
        children = [node['id'] for node in dag_doc['nodes'] if node_id in node['parents']]
        return children

    def get_node_status(self, node_id, run_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                            query={'run_id': run_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['status']

    def get_exec_order(self, dag_doc):
        exec_nodes = [(node['id'], node['order']) for node in dag_doc['nodes'] if node['is_exec'] == 1]
        if len(exec_nodes) > 1:
            node_order = sorted([exec_nodes],key=lambda x: x[1])
        else:
            node_order = [exec_nodes[0][0]]
        return node_order

    def get_node_metadata(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['metadata']

    def update_node_status(self, run_id, node_id, new_status):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name="ai_dag_test",
                                                       query={'run_id': run_id, "node_id": node_id},
                                                       update={'$set': {"status":new_status }})

    def update_node_field(self, run_id, node_id, field_name, field_value):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name="ai_dag_test",
                                                       query={'run_id': run_id, "node_id": node_id},
                                                       update={'$set': {field_name: field_value }})

    def get_node_type(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['type']

    def get_node_details(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res

    def update_child_node_status(self,dag_doc, run_id, node_id, status, check_parent_status):
        child_node_ids = self.get_child_node(dag_doc, node_id)

        for child_node_id in child_node_ids:
            parent_ids = self.get_parent_node(dag_doc, child_node_id)

            for parent_id in parent_ids:
                parent = self.get_sch_node_details(run_id, parent_id)
                if 'C' != parent['status']:
                    return None

            self.update_node_status(run_id, child_node_id, 'S')

    def get_next_scheduled_node(self, run_id):
        node, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                            query={'status': 'S', 'run_id': run_id}, limit=1)
        for doc in node:
            del doc['_id']
        return node[0]

    def exec_node(self, di_dag_id, node_id, node_type):
        pass

    def get_sch_node_details(self, run_id, node_ids):
        node, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_test",
                                             query={'node_id': node_ids,
                                                    'run_id': run_id}, limit=1)
        for doc in node:
            del doc['_id']
        return node[0]

    def get_target_db_connection(self, conn_id):
        return_object, count = self.mongo_client.find(self.db_name,"di_connections", {"conn_id": conn_id})
        for service in return_object:
            del service['_id']
            return_object = service

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

            return pg_db_client

    def get_table_name(self, dataset_id):
        node, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name="di_datasets",
                                             query={'dataset_id': dataset_id},
                                             projection={"table_name": 1})
        if len(node) !=0:
            for doc in node:
                del doc['_id']
            return node[0]['table_name']

    def get_dag_nodes(self, dag_id):
        nodes, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name="ai_dags",
                                                              query={'ai_dag_id': dag_id},
                                                              projection={"nodes": 1})
        if len(nodes) != 0:
            for doc in nodes:
                del doc['_id']
            return nodes[0]['nodes']

    def set_dag_run(self, dag_id):
        dag_run_doc = dict()
        dag_run_doc['dag_id'] = dag_id
        dag_run_doc['start_time'] = datetime.datetime.now()
        dag_run_doc['is_error'] = False
        res = self.mongo_client.insert_one(
                        db_name=self.db_name,
                        collection_name='ai_dag_running',
                        data=dag_run_doc
                        )

    def get_dag_run_error_status(self, dag_id):
        status, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name='ai_dag_running',
                                                               query={'dag_id': dag_id},
                                                               projection={'is_error': 1})
        return status[0]['is_error']

    def unset_dag_run(self, dag_id):
        res = self.mongo_client.find_one_and_delete(
                        db_name=self.db_name,
                        collection_name='ai_dag_running',
                        query={'dag_id': dag_id}
                        )

        result, count = self.mongo_client.find(
            db_name=self.db_name,
            collection_name='ai_dag_running',
            query={'dag_id': dag_id}
        )

    def check_dag_running(self, dag_id):

        result, count = self.mongo_client.find(
                        db_name=self.db_name,
                        collection_name='ai_dag_running',
                        query={'dag_id' : dag_id}
                        )
        return bool(count)

    def get_nb_status(self, nb_id, run_id):
        status, count = self.mongo_client.find_with_projection(db_name=self.db_name, collection_name='nbruns',
                                                               query={'entity_id': nb_id, 'r_id': run_id},
                                                               projection={'status': 1})
        return status

    @staticmethod
    def get_payload_from_nb_node(node_doc):
        inputs = node_doc['metadata']['inputs']
        input_data_marts = [{'key': inp['key'], 'value': inp['value'], 'desc': ''} for inp in inputs
                            if inp['input_type'] == 'data_mart']
        input_cartridges = [{'key': inp['key'], 'value': inp['value'], 'desc': ''} for inp in inputs
                            if inp['input_type'] == 'data_cartridge']
        #input_params = [{inp['key']: inp['value']} for inp in inputs if inp['input_type'] == 'params']

        input_params = {}
        for inp in inputs:
            if inp['input_type'] == 'params':
                input_params.update({inp['key']: inp['value']})

        outputs = node_doc['metadata']['outputs']
        output_data_marts = [{'key': out['key'], 'value': out['value'], 'desc': ''} for out in outputs
                             if out['input_type'] == 'data_mart']
        #output_params = [{out['key']: out['value']} for out in outputs if out['input_type'] == 'params']

        output_params = {}
        for out in outputs:
            if out['input_type'] == 'params':
                output_params.update({out['key']: out['value']})

        output_charts = [{'path': out['value']} for out in outputs
                         if out['input_type'] == 'object' and out['key'] == 'chart']
        output_pickle = [{'path': out['value']} for out in outputs
                         if out['input_type'] == 'object' and out['key'] == 'pickle']

        payload = {"name": node_doc['name'],
                   "version": node_doc['nb_version'],
                   "entity_id": node_doc['nb_id'],
                   "entity_name": node_doc['nb_name'],
                   "input_datamarts": input_data_marts,
                   "input_cartridges": input_cartridges,
                   "output_params": output_params,
                   "output_charts_images": output_charts,
                   "output_pickel_files": output_pickle,
                   "input_params": input_params,
                   "notebook_file_path": node_doc['notebook_file_path'],
                   "output_datamarts": output_data_marts,
                   "error_message": "-",
                   "run_status": "not_started"

                   }
        print(payload)
        return payload

    def get_runs_details(self, dag_id):
        output, count = self.mongo_client.find_latest(self.db_name, "ai_dag_runs", {"dag_id": dag_id},
                                                      projection={'_id': 0},
                                                      limit=100,
                                                      sort_field='start_time')

        results = []
        for doc in output:
            dag_id = doc['dag_id']
            run_id = doc['run_id']

            percent_complete = self.get_dag_completion(dag_id, run_id)
            doc.update({"percent_complete": percent_complete})
            results.append(doc)

        return results

    def get_dag_completion(self, dag_id, run_id):
        completed = self.mongo_client.count_docs(self.db_name, collectionname="ai_dag_test",
                                                 data={
                                                        "dag_id": dag_id,
                                                        "run_id": run_id,
                                                        "status": 'C'
                                                      }
                                                )
        total = self.mongo_client.count_docs(self.db_name, collectionname="ai_dag_test",
                                             data={
                                                     "dag_id": dag_id,
                                                     "run_id": run_id
                                                  }
                                             )
        if total == 0:
            total = 1
        return round(float(completed/total)*100, 2)

    def update_dag_completion(self, dag_id, run_id):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name=COLLECTION,
            query={
                'run_id': run_id
            },
            update={
                    '$set': {
                        'percentage': self.get_dag_completion(dag_id, run_id)
                    }
                }
        )

    def update_run_log(self, run_id, msg):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name=COLLECTION,
            query={
                'run_id': run_id
            },
            update={
                    '$push': {
                        'log': msg
                    }
                }
        )

    def get_duration(self, start_time, end_time, expand=True):
        diff = str_to_datetime(end_time) - str_to_datetime(start_time)
        return self.display_time(diff.seconds, expand)

    @staticmethod
    def display_time(seconds, expand, granularity=2):
        result = []

        for name, count in INTERVAL:
            value = seconds // count
            if value:
                seconds -= value * count
                if value == 1:
                    name = name.rstrip('s')
                name = name[0] if not expand else name
                result.append("{} {}".format(value, name))
        return ', '.join(result[:granularity])



    def create_nbrun(self, run_doc):
        id = self.mongo_client.insert_one(self.db_name, "nbruns", run_doc).inserted_id
        del run_doc['_id']
        return to_json(run_doc)

    def create_run_document(self, data):
        run_doc = {}
        run_doc['r_id'] = str(uuid.uuid4())
        run_doc['name'] = data['name']
        run_doc['entity_id'] = data['entity_id']
        run_doc['notebook_file_path'] = data['notebook_file_path']
        run_doc['version'] = self.get_latest_version(data['entity_id']) + 1
        run_doc['input_params'] = data['input_params'] if 'input_params' in data else {}
        run_doc['input_datamarts'] = data['input_datamarts'] if 'input_datamarts' in data else []
        run_doc['input_cartridges'] = data['input_cartridges'] if 'input_cartridges' in data else []
        run_doc['output_datamarts'] = data['output_datamarts'] if 'output_datamarts' in data else []
        run_doc['output_params'] = {}
        run_doc['output_pickel_files'] = []
        run_doc['output_charts_images'] = []
        run_doc['status'] = 'new'
        run_doc['error_message'] = None
        run_doc['created_at'] = str(datetime.datetime.utcnow())
        run_doc['created_by'] = str('unknown')
        run_doc['updated_at'] = str(datetime.datetime.utcnow())
        run_doc['updated_by'] = str('unknown')
        return run_doc

        # Step 4 updated generated images paths in database

    def get_latest_version(self, entity_id):
        output, count = self.mongo_client.find(self.db_name, "nbruns", {'entity_id': entity_id}, 0, 1, "version",
                                               sort_order=DESCENDING)

        if((output is not None) and (len(output) > 0) and ('r_id' in output[0])):
            return int(output[0]['version'])
        else:
            return 0
