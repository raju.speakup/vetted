from ai_pipeline.utils.mongo_db import db_client
from ai_pipeline.utils.json_utils import to_json, to_list_json
from flask import g
from ai_pipeline.utils.mongo_db import db_client


class AIDagService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.collection_name = 'ai_dags'
        self.mongo_client = db_client
        self.dag_doc = None

    def search_query(self, keyword):
        return {"$or": [{'name': {'$regex': keyword, "$options": 'i'}},
                        {'description': {'$regex': keyword, "$options": 'i'}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and":[{'clientid': client_id}, {'appid': app_id}]}
        return {"$and":[{'clientid': client_id}, {'appid': app_id}, query]}

    def create_dag(self, dag_doc):
        id = self.mongo_client.insert_one(self.db_name, self.collection_name, dag_doc).inserted_id
        del dag_doc['_id']
        return to_json(dag_doc)

    def delete_dag(self, dag_id):
        result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'ai_dag_id': dag_id})
        return to_json(result)

    def bulk_delete_dags(self, ids):
        results =[]
        for id in ids:
            result = self.mongo_client.find_one_and_delete(self.db_name, self.collection_name, {'ai_dag_id': id})
            results.append(result)
        return results

    def list_dag(self, limit=10, offset=0, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.mongo_client.find(self.db_name, self.collection_name, query=query, offset=offset, limit=limit)
        output = []
        for doc in result:
            del doc['_id']
            output.append(doc)

        return to_list_json(output, list_count=count)

    def get_dag(self, dag_id):
        output, count = self.mongo_client.find(self.db_name, self.collection_name, {'ai_dag_id': dag_id})
        for service in output:
            del service['_id']
            output = service

        return to_json(output)

    def update_dag(self, dag_id, new_delta_doc):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name=self.collection_name,

                                                       query={'ai_dag_id': dag_id}, update={'$set': new_delta_doc})
        del output['_id']
        return to_json(output)

    def schedule_dag(self, sch_doc):
        res = self.mongo_client.insert_one(self.db_name, "ai_dag_scheduler", sch_doc)
        del sch_doc['_id']
        return to_json(sch_doc)

    def get_scheduled_dag(self, dag_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                            query={'ai_dag_id': dag_id, "is_exec": 1})
        for doc in res:
            del doc['_id']
        return res

    def get_parent_node(self, dag_doc, node_id):
        print(node_id)
        parents = [node['parents'] for node in dag_doc['nodes'] if node['id'] == node_id]
        print("parents "+str(parents))
        return parents[0]

    def get_child_node(self, dag_doc, node_id):
        children = [node['id'] for node in dag_doc['nodes'] if node_id in node['parents']]
        return children

    def get_node_status(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['status']

    def get_exec_order(self, dag_doc):
        exec_nodes = [(node['id'], node['order']) for node in dag_doc['nodes'] if node['is_exec'] == 1]
        if len(exec_nodes) > 1:
            node_order = sorted([exec_nodes],key=lambda x: x[1])
        else:
            node_order = [exec_nodes[0][0]]
        return node_order

    def get_node_metadata(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['metadata']

    def update_node_status(self, di_dag_id, node_id, new_status):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                                       query={'dag_id': di_dag_id, "node_id": node_id},
                                                       update={'$set': {"status":new_status }})
    def update_node_field(self, di_dag_id, node_id, field_name, field_value):
        output = self.mongo_client.find_one_and_update(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                                       query={'dag_id': di_dag_id, "node_id": node_id},
                                                       update={'$set': {field_name:field_value }})

    def get_node_type(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res[0]['type']

    def get_node_details(self, di_dag_id, node_id):
        res, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                            query={'dag_id': di_dag_id, "node_id": node_id})
        for doc in res:
            del doc['_id']
        return res

    def update_child_node_status(self, dag_id, node_id, status, check_parent_status):
        dag_doc = self.get_dag(dag_id)
        child_node_ids = self.get_child_node(dag_doc['data'], node_id)

        for child_node_id in child_node_ids:
            #print("child "+ child_node_id)

            child = self.get_sch_node_details(child_node_id)
            #print("child_details "+str(child))

            parent_ids = self.get_parent_node(dag_doc['data'], child_node_id)
            #print('parents_ids: ' + str(parent_ids))

            for parent_id in parent_ids:
                parent = self.get_sch_node_details(parent_id)
                #print("parent")
                print(parent)
                if 'C' != parent['status']:
                    return None

            self.update_node_status(dag_id, child_node_id, 'S')

    def get_next_scheduled_node(self):
        node, count = self.mongo_client.find(db_name=self.db_name, collection_name="ai_dag_scheduler",
                                            query={'status': 'S'}, limit=1)
        for doc in node:
            del doc['_id']
        return node[0]

    def exec_node(self, di_dag_id, node_id, node_type):
        pass

    def get_sch_node_details(self, node_ids):
        node, count = self.mongo_client.find(db_name=self.db_name, collection_name="di_dag_scheduler",
                                             query={'node_id': node_ids}, limit=1)
        for doc in node:
            del doc['_id']
        return node[0]

    def get_dag_version(self, dag_id, version):
        if version != 'current_version':
            output, count = self.mongo_client.find_with_projection(self.db_name, 'ai_dag_commits',
                                                                   query={'dag_id': dag_id,
                                                                          'version_number': version},
                                                                   projection={'dag': 1}
                                                                   )

            if not output:
                output, count = self.mongo_client.find(self.db_name, self.collection_name, {'ai_dag_id': dag_id})
                for service in output:
                    del service['_id']
                    output = service
            else:
                for service in output:
                    del service['_id']
                    output = service
                output = output['dag']
        else:
            output, count = self.mongo_client.find(self.db_name, self.collection_name, {'ai_dag_id': dag_id})
            for service in output:
                del service['_id']
                output = service
        return output


dag_service = AIDagService('bat_db')