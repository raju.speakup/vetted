class Const(object):
    MONGO_HOST = "localhost"
    MONGO_PORT = 27017
    API_PORT = 8811
    MONGO_SERVER = 'UBUNTUMONGODEV01:27017'
    MONGO_DB_NAME = 'NoodleApp'
    MONGO_DB_COLLECTION = 'catalog_store'
    MONGO_USER = 'mongo-admin'
    MONGO_PASSWD = 'noodleadmin'

con = Const()