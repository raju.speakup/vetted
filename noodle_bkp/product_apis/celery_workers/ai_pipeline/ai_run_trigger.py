from datetime import datetime
import uuid
from ai_pipeline.services.dag_run import AIDagRunService
from ai_pipeline.services.ai_dag import AIDagService
from ai_pipeline.services.node_status import NodeStatus
from ai_pipeline.services.dag_run import DagRun
from ai_pipeline.configuration.layer_fetch_environ import (
    DB_NAME,
    COLLECTION,
    DATE_FORMAT
)
from ai_pipeline.services.run_status import RunStatus
from ai_pipeline.utils.mongo_db import MongoDBClient


class AIDagRun():

    def __init__(self):
        self.db_name = DB_NAME
        self.service = AIDagRunService(self.db_name)
        self.dag_service = AIDagService(self.db_name)
        self.run_service = DagRun()
        self.mongo = MongoDBClient()

    def get_details(self, dag_id, run_version):
        dag = self.dag_service.get_dag_version(dag_id, run_version)
        run_id = str(uuid.uuid4())
        nodes = dag['nodes']
        responses = []
        for node in nodes:
            node_doc = {}
            node_doc.update({
                "type": node['type'],
                "dag_id": dag_id,
                "run_id": run_id,
                "name": node['name'],
                "parents": node['parents'],
                "node_id": node['id'],
                "status": NodeStatus.New,
                "metadata": node['metadata'],
                "notebook_file_path": node['notebook_file_path'],
                "nb_id": node['nb_id'],
                "nb_version": node['nb_version'],
                "nb_name": node['nb_name'],
            }
            )
            if node['type'] == 'DW':
                node_doc.update({
                    "status": NodeStatus.Schedule
                })

            resp = self.service.save_test_dag(node_doc)
            responses.append(resp)
        return dag, run_id

    def run_dag(self, celery_obj, dag_id, sign, queue, version, schedule, app_id, client_id):
        if self.service.check_dag_running(dag_id):
            print("dag_id : {} is already running".format(dag_id))
        else:
            sig = celery_obj.signature(sign)
            dag, run_id = self.get_details(dag_id, version)
            name = dag.get('name')
            desc = dag.get('description')
            self.service.set_dag_run(dag_id)
            start_time = datetime.now().strftime(DATE_FORMAT)
            msg = "Queuing AI pipeline {}. DAG_ID : {}. RUN_ID : {}. QUEUE : {} ".format(name, dag_id, run_id, queue)
            sig.apply_async(
                args=[run_id, dag_id, dag, start_time, version],
                queue=queue,
                task_id=run_id
            )
            self.mongo.insert_one(
                db_name=self.db_name,
                collection_name=COLLECTION,
                data={
                    'run_id': run_id,
                    'dag_id': dag_id,
                    'meta': {},
                    'percentage': 0,
                    'log': [msg],
                    'name': name,
                    'description': desc,
                    'source': 'scheduler',
                    'schedule': schedule,
                    'start_time': start_time,
                    'end_time': start_time,
                    'version': version,
                    'duration': '0 seconds',
                    'type': 'ai_pipeline',
                    'status': RunStatus().InProgress,
                    'app_id': app_id,
                    'client_id': client_id
                }
            )
