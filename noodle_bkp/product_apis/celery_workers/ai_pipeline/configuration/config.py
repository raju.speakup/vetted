from config_store import config_store_client as conf_client


class Config:
    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='api-ai-pipeline')

    def get_v(self, key):
        return self.cs.get(key)

    def get_env(self):
        return self.cs.get_environment_props()

    def reload(self):
        self.cs.load(ip='*', service_name='api-ai-pipeline')


class CeleryConfig:
    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='celery-worker-config')

    def get_v(self, key):
        return self.cs.get(key)

    def reload(self):
        self.cs.load(ip='*', service_name='celery-worker-config')