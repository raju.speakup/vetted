from datetime import datetime
from ai_pipeline.services.dag_run import AIDagRunService
from ai_pipeline.services.dag_run import DagRun
from ai_pipeline.configuration.layer_fetch_environ import (
    DB_NAME,
    COLLECTION,
    DATE_FORMAT
)
from ai_pipeline.utils.json_utils import to_json
from ai_pipeline.utils.mongo_db import MongoDBClient
from ai_pipeline.services.run_status import RunStatus
from workers.celery_obj_factory import CeleryObjFactory
celery = CeleryObjFactory(type='ai_pipeline').get_object()
service = AIDagRunService(DB_NAME)
mongo = MongoDBClient()
run_doc = dict()

@celery.task
def dag_runner(run_id, dag_id, dag, start_time, version):
    try:
        run_service = DagRun()
        name = dag.get('name')
        msg = "AI pipeline {} received py the worker.".format(name)
        print(msg)
        msg = "Running AI Pipeline : {} ".format(name)
        print(msg)
        service.update_run_log(run_id, msg)
        print("Control flowing to run dag")
        output, r_ids = run_service.run_dag(run_id, dag_id, dag)
        end_time = datetime.now().strftime(DATE_FORMAT)
        if output is True:
            run_service.add_dag_run(dag_id, run_id, start_time, end_time, version, r_ids, status='C')
            run_doc.update(
                {
                    'meta': {'r_ids': r_ids},
                    'status': RunStatus().Success,
                    'end_time': end_time,
                    'duration': service.get_duration(start_time, end_time)

                }
            )
            msg = "AI Pipeline : {} completed successfully".format(name)
            print(to_json({"message": msg}, is_error=False))
            service.update_run_log(run_id, msg)

        else:
            run_service.add_dag_run(dag_id, run_id, start_time, end_time, version, r_ids, status='F')
            run_doc.update(
                {
                    'meta': {'r_ids': r_ids},
                    'status': RunStatus().Failure,
                    'end_time': end_time,
                    'duration': service.get_duration(start_time, end_time)

                }
            )

            print(to_json({"message": output}, is_error=True))
            service.update_run_log(run_id, output)
    except Exception as err:
        end_time = datetime.now().strftime(DATE_FORMAT)
        run_doc.update(
            {
                'status': RunStatus().Failure,
                'end_time': end_time,
                'duration': service.get_duration(start_time, end_time)


            }
        )
        service.update_run_log(run_id, err.args[0])

    mongo.find_one_and_update(
        db_name=DB_NAME,
        collection_name=COLLECTION,
        query={
            'run_id': run_id
        },
        update={
            '$set': run_doc
        }
    )
    service.unset_dag_run(dag_id)
    msg = 'Postprocessing Complete.'
    print(msg)
    service.update_run_log(run_id, msg)


@celery.task
def nb_runner(nb_url, name, r_id):

    print("Running adhoc notebook : {} ".format(nb_url))
    print("Name: {}".format(name))
    run_service = DagRun()
    run_service.run_nb(nb_url, name, r_id)
    print("Completed adhoc run for notebook : {}".format(nb_url))
