#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fetch all confidencial data from config store
    Functionality:
        - If any manupulations needed in environment setting
        this is place to do it

"""
# ------------------------------------------------------------------------------
# IMPORT SECTION
# ------------------------------------------------------------------------------
from ast import literal_eval
from celery_docker.configuration.config import Config, CeleryConfig

# ------------------------------------------------------------------------------
# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES USING CONFIG STORE
# ------------------------------------------------------------------------------

conf = Config()

MONGO_SERVER = conf.get_v('MONGO_SERVER')
MONGO_USER = conf.get_v("MONGO_USER")
MONGO_PASSWD = conf.get_v("MONGO_PASSWD")
DB_URL = conf.get_v("DB_URL")
DB_NAME = conf.get_v("DB_NAME")
NB_RUN_URL = conf.get_v("NB_RUN_URL")



celery_config = CeleryConfig()

BROKER_URL = celery_config.get_v('BROKER_URL')
RESULT_BACKEND = celery_config.get_v('RESULT_BACKEND')
COLLECTION = celery_config.get_v('RUN_STATUS_COLLECTION')
NB_RUN_COLLECTION = celery_config.get_v("NB_RUN_STATUS_COLLECTION")
DATE_FORMAT = celery_config.get_v('DATE_FORMAT')
USERNAME = celery_config.get_v('USERNAME')
PASSWD = celery_config.get_v('PASSWD')
AUTH_URL = celery_config.get_v('AUTH_URL')
CLIENT_ID = celery_config.get_v('CLIENT_ID')
APP_ID = celery_config.get_v('APP_ID')
RETRY = int(celery_config.get_v('RETRY'))
SLEEP = int(celery_config.get_v('SLEEP'))
INTERVAL = literal_eval(celery_config.get_v('INTERVAL'))
