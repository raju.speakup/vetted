from celery_docker.utils.mongo_db import db_client
from celery_docker.configuration.layer_fetch_environ import (
    COLLECTION,
    NB_RUN_COLLECTION
)
class AIRunService:
    def __init__(self, db_name):
        self.db_name = db_name
        self.mongo_client = db_client

    def update_run_log(self, run_id, msg):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name=COLLECTION,
            query={
                'run_id': run_id
            },
            update={
                    '$push': {
                        'log': msg
                    }
                }
        )

    def update_run_dag_error_status(self, dag_id):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name='ai_dag_running',
            query={
                'dag_id': dag_id
            },
            update={
                '$set': {
                    'is_error': True
                }
            }
        )

    def update_nb_run_log(self, r_id, msg):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name=NB_RUN_COLLECTION,
            query={
                'r_id': r_id
            },
            update={
                    '$push': {
                        'log': msg
                    }
                }
        )

    def update_run_nb_error_status(self, r_id, msg):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name=NB_RUN_COLLECTION,
            query={
                'r_id': r_id
            },
            update={
                '$set': {
                    'error_message': msg,
                    'status': 'failed'
                }
            }
        )

    def update_run_nb_success(self, r_id):
        self.mongo_client.find_one_and_update(
            db_name=self.db_name,
            collection_name=NB_RUN_COLLECTION,
            query={
                'r_id': r_id
            },
            update={
                '$set': {
                    'status': 'completed'
                }
            }
        )