import os
import sys
import json
import requests
import nbformat
import nbconvert
from nbconvert.preprocessors import ExecutePreprocessor
from nbconvert.exporters import PythonExporter
from config import Config
import subprocess
sys.path.append('/code')
print(os.getcwd())
from celery_docker.services.run_service import AIRunService
from celery_docker.configuration.layer_fetch_environ import (
    DB_NAME
)
import re

conf = Config()
service = AIRunService(DB_NAME)


def get_notebook(nb_url, r_id):
    print("nb_url ", nb_url)
    service.update_nb_run_log(r_id, 'Getting notebook of NB_URL : {} ;'.format(nb_url))
    try:
        _, nb_name = nb_url.rsplit("/", 1)

        print("nb_name ", nb_name)

        service.update_nb_run_log(r_id, 'nb_name : {} ;'.format(nb_name))
        payload = "username=test&password=test"
        headers = {
            'content-type': "application/x-www-form-urlencoded",
            'authorization': "token " + conf.get_v('jupyterhub.api_token'),
            'cache-control': "no-cache",
            'postman-token': "506704bb-4d60-33d5-9218-ff965c8a6609"
        }
        jupyter_server_name = conf.get_v('jupyterhub.server_name')
        service.update_nb_run_log(r_id, 'jupyter server name : {} ;'.format(jupyter_server_name))
        nb_url = nb_url.replace("http://" + jupyter_server_name + "/user/test/notebooks/",
                                "http://" + jupyter_server_name + "/hub/user/test/api/contents/")

        service.update_nb_run_log(r_id, 'Making GET request with  NB_URL: {} ; payload: {} ; headers: {};'.format(nb_url, payload,
                                                                                                             headers))
        response = requests.request("GET", nb_url, data=payload, headers=headers)
        service.update_nb_run_log(r_id, 'Got the response of NB_URL: {} ; payload: {} ; headers: {}; response: {}; '.format(nb_url, payload, headers, response))

        local_nb_path = "/code/" + nb_name
        print("local_nb_path ", local_nb_path)
        service.update_nb_run_log(r_id, 'Writing the response local_nb_path: {}; '.format(local_nb_path))

        with open(local_nb_path, "w+") as nb:
            nb.write(json.dumps(json.loads(response.text)['content']))
        return local_nb_path
    except Exception as e:
        print(e)
        service.update_nb_run_log(r_id, 'Exception in Getting Notebook; Exception message: {}'.format(e.args[0]))
        service.update_run_nb_error_status(r_id, e)

def convert_notebook(nb_path, r_id):
    print("nb_path ", nb_path)
    service.update_nb_run_log(r_id, 'Converting notebook to python file from the path NBPath: {};'.format(nb_path))
    # from nbconvert.exporters import PythonExporter
    try:
        nb = nbconvert.exporters.export(PythonExporter, nb_path)
        # print(nb)
        # with open(nb_path) as f:
            # nb = nbformat.read(f, as_version=4)
            # ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
            # ep.preprocess(nb, {'metadata': {'path': '/'}})
            # with open('executed_notebook.ipynb', 'wt') as f:
            #     nbformat.write(nb, f)
        # print(enumerate(nb))
        # print(list(enumerate(nb)))
        file_name = '/code/{}.py'.format(os.environ.get('PY_FILENAME'))
        print("filename ", file_name)
        service.update_nb_run_log(r_id, 'writing the converted notebooks into python file fileName: {}; '.format(file_name))
        with open(file_name, 'w') as f:
            for i in nb[0]:
                f.write(i)
    except Exception as e:
        print(e)
        service.update_nb_run_log(r_id, 'Exception in Converting Notebook Exception message: {}'.format(e.args[0]))
        service.update_run_nb_error_status(r_id, str(e))


def run_notebook_without_converting(nb_path, r_id):
    with open(nb_path) as nb_f:
        nb = nbformat.read(nb_f, as_version=4)
        ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
    try:
        out = ep.preprocess(nb, {'metadata': {'path': './'}})
    except Exception as e:
        msg = 'Error executing the notebook "%s".\n\n' % nb_path
        service.update_nb_run_log(r_id, msg)
        service.update_run_nb_error_status(r_id, e)
    finally:
        service.update_nb_run_log(r_id, "Writing the output ")
        with open('executed_notebook.ipynb', 'wt') as f:
            nbformat.write(nb, f)


def run_notebook(r_id):
    print("Run_id : {}".format(r_id))
    service.update_nb_run_log(r_id, 'Running notebook')
    args = [
        'ipython',
        '/code/{}.py'.format(os.environ.get('PY_FILENAME')),
    ]

    try:
        subprocess.check_output(args, stderr=subprocess.PIPE, encoding="UTF-8")
        service.update_run_nb_success(r_id)
        service.update_nb_run_log(r_id, 'Notebook ran Successfully')
    except subprocess.CalledProcessError as e:
        ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
        error = ansi_escape.sub('', e.output)
        service.update_run_log(r_id, 'Exception in running notebook. Exception message: {}'.format(e))
        service.update_run_nb_error_status(r_id, error)

def controller(nb_url, r_id):
    path = get_notebook(nb_url, r_id)
    # path = sys.argv[1]
    print(path)
    print(nb_url)
    convert_notebook(path, r_id)
    # run_notebook_without_converting(path, run_id)
    run_notebook(r_id)


if __name__ == "__main__":
    controller(sys.argv[1], sys.argv[2])