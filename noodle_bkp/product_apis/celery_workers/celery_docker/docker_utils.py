import os
import docker
from celery_docker.services.run_service import AIRunService
from celery_docker.configuration.layer_fetch_environ import (
    DB_NAME
)


client = docker.from_env()
service = AIRunService(DB_NAME)


def create_container(nb_url, py_filename, run_id, dag_id):
        service.update_run_log(run_id, 'Creating docker container. NB_URL : {} ; FILENAME: {}'.format(nb_url, py_filename))
        path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        print("PATH: {}".format(path))
        service.update_run_log(run_id, "PATH: {}".format(path))
        try:
            obj = client.containers.run(
                image='prod-docker.ad.noodle.ai/bat_notebooks:v2',
                name=py_filename,
                command=[
                    'mkdir -p /code/notebooks',

                ],
                environment={
                    'CONF_STORE': os.environ.get('CONF_STORE'),
                    'CONF_ENV': os.environ.get('CONF_ENV'),
                    'NB_URL': nb_url,
                    'PY_FILENAME': py_filename,
                    'DAG_ID': dag_id,
                    'RUN_ID': run_id,
                    'NOTEBOOK_ENGINE_ENV': 'no'
                },
                volumes={
                    path: {
                        'bind': '/code',
                        'mode': 'rw'
                    }
                },
                entrypoint=['/bin/bash', '/code/celery_docker/notebook_deploy.sh'],
                auto_remove=True,
                remove=True,
                detach=True
            )
        except Exception as e:
            print("INSIDE create: {}".format(e))
            service.update_run_log(run_id, e)
        for line in obj.logs(stream=True):
            print(line.strip())


def create_nb_container(nb_url, py_filename, r_id):
    service.update_nb_run_log(r_id, 'Creating docker container. NB_URL : {} ; FILENAME: {}'.format(nb_url, py_filename))
    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    service.update_nb_run_log(r_id, "PATH: {}".format(path))
    try:
        obj = client.containers.run(
            image='prod-docker.ad.noodle.ai/bat_notebooks:v2',
            name=py_filename,
            command=[
                'mkdir -p /code/notebooks',

            ],
            environment={
                'CONF_STORE': os.environ.get('CONF_STORE'),
                'CONF_ENV': os.environ.get('CONF_ENV'),
                'NB_URL': nb_url,
                'PY_FILENAME': py_filename,
                'R_ID': r_id,
                'NOTEBOOK_ENGINE_ENV': 'no'
            },
            volumes={
                path: {
                    'bind': '/code',
                    'mode': 'rw'
                }
            },
            entrypoint=['/bin/bash', '/code/celery_docker/nb_run_deploy.sh'],
            auto_remove=True,
            remove=True,
            detach=True
        )
    except Exception as e:
        print("INSIDE create: {}".format(e))
        service.update_nb_run_log(r_id, e)
        service.update_run_nb_error_status(r_id, e)
    for line in obj.logs(stream=True):
        print(line.strip())
