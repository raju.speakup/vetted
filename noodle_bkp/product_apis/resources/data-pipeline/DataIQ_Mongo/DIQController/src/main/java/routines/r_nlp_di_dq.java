package routines;


public class r_nlp_di_dq {

   
    public static String gen_conn_url(String source,String host,String port,String dbname ) {
        if (source.equals("sqlserver") ) {
            return "jdbc:"+source+"://"+host+":"+port+";databaseName="+dbname; 
        }
        else if(source.equals("oracle:thin:@") ){
        	return "jdbc:"+source+"//"+host+":"+port+"/"+dbname;
        }
        else{
        	return "NA";
        }
        }
}
