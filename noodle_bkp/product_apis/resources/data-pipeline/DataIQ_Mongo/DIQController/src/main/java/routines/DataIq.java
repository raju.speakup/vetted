package routines;
import org.apache.commons.lang3.StringUtils;



public class DataIq {

    
    public static String ReplaceWords(String text,String keys,String values) {
    
    	/*final String[] keys1 = new String[]{"a"};
    	final String[] values1 = new String[]{"b"};
    	*/
    	
    	String[] keys1=keys.split(";");
    	String[] values1=values.split(";");
        
        return StringUtils.replaceEach( text, keys1, values1 );	
        
        
    }
    public static String ReplaceRegex(String text,String keys,String values) {
        
    	/*final String[] keys1 = new String[]{"a"};
    	final String[] values1 = new String[]{"b"};
    	*/
 
    	
        return text.replaceAll(keys, values );	
        
        
    }
    String url = "jdbc:oracle:thin:@myhost:1521:orcl";
    
    public static String gen_conn_url(String source,String host,String port,String dbname, String instance ) {
        if (source.equals("SQLServer") ) {
            return "jdbc:sqlserver://"+host+":"+port+";instanceName="+instance+";databaseName="+dbname; 
        }
        else if(source.equals("oracleServer") && instance.equals("") ){
        	source="oracle:thin:@";
        	return "jdbc:"+source+"//"+host+":"+port+"/"+dbname;
        }
        else if(source.equals("oracleServer") && !instance.equals("") ){
        	source="oracle:thin:@";
        	return "jdbc:"+source+"//"+host+":"+port+":"+instance;
        }
       
        else{
        	return "NA";
        }
        }
}

