#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger

@swagger.model
class CreateSchedule:
    """Create Schedule fields."""

    resource_fields = {
        'dag_id': fields.String,
        'start_date': fields.String,
        'end_date': fields.String,
        'interval': fields.String,
        'type': fields.String,
        'name': fields.String,
        'description': fields.String,
        'version': fields.String,
        'queue': fields.String
    }
    swagger_metadata = {
        "dag_id": {
            "required": True
        },
        "name": {
            "required": True
        },
        "description": {
            "required": True
        },
        "version": {
            "required": True
        },
        "queue": {
            "required": True
        },
        "interval": {
            "enum": ['daily', 'weekly', 'monthly', 'yearly'],
            "required": True
        },
        "type": {
            "enum": ['data_ingestion', 'ai_pipeline'],
            "required": True
        },
        "start_date": {
            "pattern": "\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)",
            "required": True
        },
        "end_date": {
            "pattern": "\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)",
            "required": True
        }
    }


@swagger.model
class UpdateSchedule:
    """Update Schedule fields."""

    resource_fields = {
        'start_date': fields.String,
        'end_date': fields.String,
        'interval': fields.String,
        'type': fields.String,
        'name': fields.String,
        'description': fields.String,
        'version': fields.String,
        'queue': fields.String
    }

    swagger_metadata = {
        "interval": {
            "enum": ['daily', 'weekly', 'monthly', 'yearly']
        },
        "type": {
            "enum": ['data_ingestion', 'ai_pipeline']
        },
        "start_date": {
            "pattern": "\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)"
        },
        "end_date": {
            "pattern": "\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)"
        }
    }

