import json
import kombu
from commons.json_utils import to_json
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR
)
from configuration.layer_fetch_environ import QUEUE_MAPPING


class QueueListService:

    def __init__(self, broker_url):
        self.broker_url = broker_url
        self.conn = kombu.Connection(self.broker_url)

    def get_all_queues(self, type):
        try:
            self.conn.connect()
            client = self.conn.get_manager()
            v_host = self.broker_url.split('/')[-1]
            mapping = json.loads(QUEUE_MAPPING)
            queues = [queue['name'] for queue in client.get_queues(v_host)
                      if mapping[type] in queue['name'] and '@' not in queue['name']]
            return to_json(queues), HTTP_200_OK

        except Exception as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

    def get_all_vhosts(self):
        try:
            self.conn.connect()
            client = self.conn.get_manager()
            hosts = [host['name'] for host in client.get_all_vhosts() if '/' not in host['name']]
            return to_json(hosts), HTTP_200_OK

        except Exception as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

