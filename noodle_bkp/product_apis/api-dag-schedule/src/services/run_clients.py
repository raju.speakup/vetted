import datetime
# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED)
from logger.logger import logger
from configuration.layer_fetch_environ import (
    DATE_FORMAT,
    SCHEDULE_COLLECTION,
    RUN_STATUS_COLLECTION,
    BROKER_URL,
    RESULT_BACKEND,
    SERVER_NAME
)
from flask import g

from bat_celery_sdk.celery_creator import MakeCelery
from configuration.config import RunConfig
from services.run_status import RunStatus


# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError
from celery.task.control import revoke

class RunService:
    """User Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def search_query(self, keyword):
        return {
                "$or":[
                        {'name': {'$regex': keyword, "$options": "-i"}},
                        {'description': {'$regex': keyword, "$options": "-i"}},
                        {'version': {'$regex': keyword, "$options": "-i"}}
                ]
            }

    def find_query(self, client_id, app_id, query, type=None):
        if type is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, {'type': type}, query]}

    def list_all_runs(self, collection_name, type, offset, limit, keyword):
        try:
            query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
            query.update({"type": type})
            return_object = self.connection.list_all(
                collectionname=collection_name,
                data=query,
                skip=offset,
                limit=limit,
                sort=[("start_time", -1)]
            )
            count = self.connection.count_docs(
                collectionname=collection_name,
                data=query
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(return_object, list_count=count), HTTP_200_OK

    def search_run(self, collection_name, type, keyword, offset, limit):
        try:
            query = self.find_query(g.client_id, g.app_id, self.search_query(keyword), type)

            return_object = self.connection.list_all(
                collectionname=collection_name,
                data=query,
                skip=offset,
                limit=limit,
                sort=[("start_time", -1)]
            )
            count = self.connection.count_docs(
                collectionname=collection_name,
                data=query
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(return_object, list_count=count), HTTP_200_OK

    def bulk_delete_runs(self, run_ids):
        try:
            response_dict = dict()
            for run_id in run_ids:
                return_object = self.connection.find_one(
                                        collectionname=RUN_STATUS_COLLECTION,
                                        data={"run_id": run_id}
                                    )
                if not return_object:
                    response_dict[run_id] = "run_id does not exist"
                else:   
                    self.connection.delete_one(
                        collectionname=RUN_STATUS_COLLECTION,
                        data={"run_id": run_id}
                    )
                    response_dict[run_id] = "run_id deleted"

            return to_json(response_dict), HTTP_200_OK
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

