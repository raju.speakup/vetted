class RunStatus():
    InProgress = "P"
    Success = "S"
    Failure = "F"
    Killed = "K"
