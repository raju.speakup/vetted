# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_json, to_list_json
from constants.custom_field_error import (
    HTTP_200_OK,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND,
    HTTP_201_CREATED,
    HTTP_406_NOT_ACCEPTABLE
)
from logger.logger import logger
from configuration.layer_fetch_environ import (
    DATE_FORMAT,
    SCHEDULE_COLLECTION,
    RUN_STATUS_COLLECTION,
    BROKER_URL,
    RESULT_BACKEND
)
import json
import datetime
from bat_celery_sdk.celery_creator import MakeCelery
from configuration.config import RunConfig
from services.run_status import RunStatus
from flask import g


# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError
from celery.task.control import revoke

class ClientService:
    """User Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def search_query(self, keyword):
        return {
                "$or":[
                        {'name': {'$regex': keyword, "$options": "-i"}},
                        {'description': {'$regex': keyword, "$options": "-i"}},
                        {'version': {'$regex': keyword, "$options": "-i"}}
                ]
            }

    def find_query(self, client_id, app_id, query, type=None):
        if type is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, {'type': type}, query]}

    def update_schedule(self, collection_name, schedule_id, data):
        try:
            # name = data.get('name')
            # if name:
            #     print("Name: {}".format(name))
            #     status, resp = self.check_name_exists(collection_name=collection_name, name=name)
            #     print(status, resp)
            #     if status:
            #         print(status)
            #         return resp
            query = self.find_query(g.client_id, g.app_id, {"schedule_id": schedule_id})
            return_object = self.connection.find_one(
                collectionname=collection_name,
                data=query)
            logger.info(data)

            if not return_object:
                return to_json({"message": "schedule_id : {0}  does not exist".format(schedule_id)}, is_error=True), HTTP_404_NOT_FOUND
            else:
                self.connection.update_one(
                    collectionname=collection_name,
                    data=({"schedule_id": schedule_id}, {
                        "$set": data}))
            return to_json(data), HTTP_200_OK

        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

    def delete_schedule(self, schedule_id):
        try:
            return_object = self.connection.find_one(
                collectionname=SCHEDULE_COLLECTION,
                data={"schedule_id": schedule_id})
            logger.info(schedule_id)
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "schedule_id : {0}  does not exist".format(schedule_id)}, is_error=True), \
                   HTTP_404_NOT_FOUND
        else:
            self.connection.delete_one(
                collectionname="dag_schedule",
                data={"schedule_id": schedule_id}
            )

        return to_json({"message": "Dag schedule deleted"}), HTTP_200_OK

    def get_schedule(self, collection_name, schedule_id):
        try:
            query = self.find_query(g.client_id, g.app_id, {"schedule_id": schedule_id})
            return_object = self.connection.find_one(
                collectionname=collection_name,
                data=query)
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return to_json({"message": "schedule_id : {0} does not exist".format(schedule_id)}, is_error=True), HTTP_404_NOT_FOUND
        if return_object:
            return_object.pop("_id")
        return to_json(return_object), HTTP_200_OK

    def check_name_exists(self, collection_name, name):
        try:
            query = self.find_query(g.client_id, g.app_id, {})
            if name in self.connection.find_unique_names(
                    collectionname=collection_name,
                    field='name',
                    query=query
            ):
                return True, (to_json(
                    {
                        "message": "Name '{}' already exists.".format(name)
                    },
                    is_error=True
                ), HTTP_406_NOT_ACCEPTABLE)
            return False, ()
        except PyMongoError as err:
            logger.error(err)
            return True, (to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR)

    def create_schedule(self, collection_name, data):
        try:
            name = data.get('name')
            status, resp = self.check_name_exists(collection_name=collection_name, name=name)
            if status:
                return resp

            self.connection.insert_one(collectionname=collection_name,
                                       data=json.dumps(data))

            return to_json(data), HTTP_201_CREATED
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR


    def list_all_schedules(self, collection_name, type, offset, limit, keyword):
        try:
            query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
            query.update({"type": type})
            return_object = self.connection.list_all(
                collectionname=collection_name,
                data=query,
                skip=offset,
                limit=limit,
                sort=[("start_date", -1)]
            )
            count = self.connection.count_docs(
                collectionname=collection_name,
                data=query
            )
        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(return_object, list_count=count), HTTP_200_OK

    def search_schedule(self, collection_name, type, keyword, offset, limit):
        try:
            query = self.find_query(g.client_id, g.app_id, self.search_query(keyword), type)

            return_object = self.connection.list_all(
                collectionname=collection_name,
                data=query,
                skip=offset,
                limit=limit,
                sort=[("start_date", -1)]
            )
            count = self.connection.count_docs(
                collectionname=collection_name,
                data=query
            )

        except PyMongoError as err:
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        return to_list_json(return_object, list_count=count), HTTP_200_OK

    def bulk_delete_schedule(self, schedule_ids):
        try:
            response_dict = dict()
            for schedule_id in schedule_ids:
                return_object = self.connection.find_one(
                                        collectionname=SCHEDULE_COLLECTION,
                                        data={"schedule_id": schedule_id}
                                    )
                if not return_object:
                    response_dict[schedule_id] = "schedule_id does not exist"
                else:   
                    self.connection.delete_one(
                        collectionname=SCHEDULE_COLLECTION,
                        data={"schedule_id": schedule_id}
                    )
                    response_dict[schedule_id] = "schedule_id deleted"

            return to_json(response_dict), HTTP_200_OK
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

    def kill_dag_run(self, run_id):
        try:
            return_object = self.connection.find_one(
                                collectionname=RUN_STATUS_COLLECTION,
                                data={
                                    '$and': [
                                        {
                                            "run_id": run_id
                                        },
                                        {
                                            "status": RunStatus().InProgress
                                        },
                                    ]

                                }
                            )
        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return to_json({"message": "run_id : {0} is not running or does not exist".format(run_id)}, is_error=True), HTTP_404_NOT_FOUND
        else:
            run_conf = RunConfig(return_object.get('type'))
            celery_class = MakeCelery(
                broker_url=BROKER_URL,
                result_backend=RESULT_BACKEND.format(run_conf.get_val('STATUS_DB')),
                prefetch=run_conf.get_val('PREFETCH'),
                status_table=run_conf.get_val('STATUS_TABLE'),
                imports=run_conf.get_val('IMPORTS')
            )
            celery = celery_class.create_object()
            celery.control.revoke(task_id=run_id, terminate=True, signal='SIGKILL')
            self.connection.update_one(
                collectionname=RUN_STATUS_COLLECTION,
                data=(
                    {
                        "run_id": run_id
                    },
                    {
                        "$set": {
                            'status': RunStatus().Killed,
                            'message': 'DAG run killed',
                            'end_time': datetime.datetime.now().strftime(DATE_FORMAT)

                        }
                    }
                )
            )
        return to_json({"message": "Dag run killed"}), HTTP_200_OK
