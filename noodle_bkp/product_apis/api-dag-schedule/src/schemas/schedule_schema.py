from commons.json_validator import Schema, Prop
# \d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\s+(AM|PM)

def create_schedule():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "description":Prop().string().max(1000).build(),
        "type": Prop().string().max(255).min(1).enum(['data_ingestion', 'ai_pipeline']).build(),
        "interval": Prop().string().max(255).min(1).enum(['daily', 'weekly', 'monthly', 'yearly']).build(),
        "version": Prop().string().max(255).min(1).build(),
        "queue": Prop().string().max(255).min(1).build(),
        "dag_id": Prop().string().max(255).min(0).build(),
        "start_date": Prop().string().format("\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.\d{1,10}").build(),
        "end_date": Prop().string().format("\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.\d{1,10}").build(),
    }
    return Schema().keys(prop).required(["name", "description", "type", "interval", "version",
                                         "queue", "start_date", "end_date", "dag_id"]).build()


def update_schedule():
    prop = {
        "name": Prop().string().max(255).min(1).build(),
        "description":Prop().string().max(1000).build(),
        "type": Prop().string().max(255).min(1).enum(['data_ingestion', 'ai_pipeline']).build(),
        "interval": Prop().string().max(255).min(1).enum(['daily', 'weekly', 'monthly', 'yearly']).build(),
        "version": Prop().string().max(255).min(1).build(),
        "queue": Prop().string().max(255).min(1).build(),
        "dag_id": Prop().string().max(255).min(0).build(),
        "start_date": Prop().string().format("\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.\d{1,10}").build(),
        "end_date": Prop().string().format("\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.\d{1,10}").build(),
    }
    return Schema().keys(prop).required([]).build()


def id_path_schema():
    prop = {
        "schedule_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required(["schedule_id"]).build()


def search_schema():
    prop = {
        "type": Prop().string().max(100).min(1).enum(['data_ingestion', 'ai_pipeline']).build(),
        "limit": Prop().number().max(5).min(1).build(),
        "offset": Prop().number().max(5).min(1).build(),
        "keyword": Prop().string().max(120).min(0).build()
    }

    return Schema().keys(prop).required(["type"]).build()


def list_schema():
    prop = {
        "limit": Prop().number().max(5).min(1).build(),
        "offset": Prop().number().max(5).min(1).build(),
        "type": Prop().string().max(100).min(1).enum(['data_ingestion', 'ai_pipeline']).build(),
    }

    return Schema().keys(prop).required(["type"]).build()


def bulk_delete_schema():
    prop = {
        "schedule_ids": Prop().array().build()
    }

    return Schema().keys(prop).required(["schedule_ids"]).build()


def bulk_delete_run_schema():
    prop = {
        "run_ids": Prop().array().build()
    }

    return Schema().keys(prop).required(["run_ids"]).build()
