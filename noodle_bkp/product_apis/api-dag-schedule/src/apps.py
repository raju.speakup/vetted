#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS
from resources.health import Health
from resources.debug import ConfigStore, CheckDbStatus, ServiceStatus
from resources.scheduleDetails import ScheduleDetails
from resources.scheduleListDelete import ScheduleListDelete
from resources.scheduleBulkDelete import ScheduleBulkDelete
from resources.scheduleListBulk import ScheduleListBulk
from resources.scheduleSearch import ScheduleSearch
from resources.queueDetails import QueueDetails
from resources.dagRunKill import DagRunKill
from resources.runListBulk import RunListBulk
from resources.runBulkDelete import RunBulkDelete
from resources.runSearch import RunSearch

from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG, SECRETE_KEY
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask import request, g


app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="Dag Schedule Management Service",
    resourcePath="/Clients"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
jwt = JWTManager(app)

app.config['PROPAGATE_EXCEPTIONS'] = True

@app.before_request
def validate_auth():
    if (not "/admin/_refresh" == request.path) and (not "/api/spec.html" == request.path):
        check_auth()

@jwt_required
def check_auth():
    print("jwt token validated successfully")
    g.client_id = request.headers.get('clientid')
    g.app_id = request.headers.get('appid')
    print("serving request for client_id {} and app_id {}".format(request.headers.get('clientid'), request.headers.get('appid')))

# ------------------------------------------------------------------------------
# Health services
# ------------------------------------------------------------------------------

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

# http://server/api/v1/debug/config_store
api.add_resource(
    ConfigStore,
    '/api/v1/debug/config_store')

# http://server/api/v1/debug/check_db_status
api.add_resource(
    CheckDbStatus,
    '/api/v1/debug/check_db_status')

# http://server/api/v1/debug/service_status
api.add_resource(
    ServiceStatus,
    '/api/v1/debug/service_status')

# ------------------------------------------------------------------------------
# Client Management services
# ------------------------------------------------------------------------------

# http://server/api/v1/dag/schedule/{dag_id}
api.add_resource(
     ScheduleListDelete, '/api/v1/dag/schedule/<string:schedule_id>')

# http://server/api/v1/dag/schedule
api.add_resource(
     ScheduleDetails, '/api/v1/dag/schedule')

api.add_resource(
     ScheduleBulkDelete, '/api/v1/dag/schedules/delete')

# http://server/api/v1/dag/schedule
api.add_resource(
    QueueDetails, '/api/v1/dag/schedule/queues/<string:type>')

api.add_resource(
    ScheduleListBulk, '/api/v1/dag/schedule/list/<string:type>')

api.add_resource(
    ScheduleSearch, '/api/v1/dag/schedule/search/<string:type>')

api.add_resource(
    DagRunKill, '/api/v1/dag/run/kill/<string:run_id>'
)

api.add_resource(
    RunListBulk, '/api/v1/dag/run/list/<string:type>'
)

api.add_resource(
    RunBulkDelete, '/api/v1/dag/runs/delete'
)

api.add_resource(
    RunSearch, '/api/v1/dag/runs/search/<string:type>'
)



if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8021, debug=True)
