#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fetch all confidencial data from and environment file
    Functionality:
        - If any manupulations needed in environment setting
        this is place to do it

"""
# ------------------------------------------------------------------------------
# IMPORT SECTION
# ------------------------------------------------------------------------------

from configuration.config import Config, CeleryConfig


conf = Config()

SERVER_NAME = conf.get_v('SERVER_NAME')
DB_NAME = conf.get_v("DB_NAME")
USERNAME = conf.get_v("USERNAME")
PASSWORD = conf.get_v("PASSWORD")
PORT = int(conf.get_v("PORT"))
DEFAULT_IP = conf.get_v("DEFAULT_IP")
MODE = conf.get_v("MODE")
DEBUG = True if conf.get_v("MODE") == 'LOCAL' else False
SECRETE_KEY = conf.get_v("SECRETE_KEY")
LDAP_URL = conf.get_v("LDAP_URL")
BROKER_URL = conf.get_v("BROKER_URL")
QUEUE_MAPPING = conf.get_v("QUEUE_MAPPING")
DATE_PATTERN = conf.get_v("DATE_PATTERN")


celery_conf = CeleryConfig()
DATE_FORMAT = celery_conf.get_v("DATE_FORMAT")
SCHEDULE_COLLECTION = celery_conf.get_v("COLLECTION_NAME")
RUN_STATUS_COLLECTION = celery_conf.get_v("RUN_STATUS_COLLECTION")
BROKER_URL = celery_conf.get_v("BROKER_URL")
RESULT_BACKEND = celery_conf.get_v("RESULT_BACKEND")