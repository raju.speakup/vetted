#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
from bson.objectid import ObjectId
from werkzeug import Response
import pymongo


class MongoJsonEncoder(json.JSONEncoder):
    """MongoJsonEncoder."""

    def default(self, obj):
        """
        """
        if isinstance(obj, (datetime.datetime, datetime.date)):
            return obj.isoformat()
        elif isinstance(obj, ObjectId):
            return unicode(obj)
        return json.JSONEncoder.default(self, obj)


def jsonifyCustom(*args, **kwargs):
    """Jsonify with support for MongoDB ObjectId."""
    data = json.loads(*args, **kwargs)
    return Response(
        json.dumps(dict(data), cls=MongoJsonEncoder),
        mimetype='application/json')


def return_result(result=[], result_count=1, error=None, error_code=None):
    """Create a template of json return object.

    Substitute data and error.

    Args:
        result (list): Result of convert_to_json function
        result_count : length of result
        error (str): Error Message if any
        error_code (str): Error Code If any

    Returns:
        json: Description of return value [Single entry]

    """
    return_result = {}
    result = json.loads(result) if type(
        result) == str else result
    return_result.update(
        {
            "error": {
                "code": error_code,
                "message": str(error) if error else error
            },
            "data": json.loads(
                json.dumps(result)) if type(result) != dict else result
        }
    )
    return_result.update({"resultCount": result_count})
    return return_result


def return_result_final(result=[], result_count=1, error=None, error_code=None):
    """Return Result."""
    return_result = {}
    result = json.loads(result) if type(
        result) == str else result
    return_result.update(
        {
            "error": {
                "code": error_code,
                "message": str(error) if error else error
            }
        }
    ) if error else return_result.update(
        {"data": json.loads(json.dumps(result)) if type(
            result) != dict else result})
    if not error and type(result) == list:
        return_result.update({"resultCount": result_count})
    return return_result


def get_url_args(req):
    """Get data from url [Split data in required manner].

    Args:
     req (obj): Pass request object to function and get all split data

    Returns:
     limit: Limit of data result
     offset: Offset of data result
     sort_key: Sorting keys in list [-] indication for DESC
     projection: BASIC or FULL

    """
    try:
        limit = int(req.args.get('limit', 5))
        offset = int(req.args.get('offset', 0))
    except ValueError:
        raise
    try:
        sort_key = str(req.args.get('sort', ''))
    except ValueError:
        raise
    try:
        projection = req.args.get('projection', 'FULL').capitalize()
        if projection != ('Full' or 'Basic'):
            raise ValueError("Projection is wrong")
    except Exception:
        raise
    gen_sort_key = sort_key.split(
        ",") if ',' in sort_key else [sort_key] if sort_key else None
    return limit, offset, gen_sort_key, projection


def get_sorting_list(computed_list=None):
    """
    """
    list_of_tupples = []
    if computed_list:
        for element in computed_list:
            list_of_tupples.append(
                (str(element)[1:] if '-' in element else str(element),
                    pymongo.DESCENDING if '-' in element else pymongo.ASCENDING
                 )
            )
    else:
        list_of_tupples.append(("$natural", pymongo.ASCENDING))
    return list_of_tupples
