#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""python custom decorators."""

from constants.custom_field_error import HTTP_400_BAD_REQUEST, \
    HTTP_403_FORBIDDEN, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_404_NOT_FOUND
from functools import wraps
from jsonschema import validate, ValidationError
from flask import (
    jsonify,
    request,
)
import json
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)
from pymongo.errors import PyMongoError
from commons.mongo_services import NoodleMongoClient


def validate_json(f):
    """Validate format of request as json."""
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except ValueError as err:
            response = jsonify(
                {
                    "error": {
                        "code": "HTTP_400_BAD_REQUEST",
                        "message": err.message
                    }
                })
            response.status_code = HTTP_400_BAD_REQUEST
            return response
        return f(*args, **kw)
    return wrapper


def validate_schema(schema):
    """Validate json with generated schema."""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                validate(request.json, schema)
            except ValidationError as err:
                response = jsonresponse(err)
                return response, HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator


def jsonresponse(err):
    """jsonresponse."""
    response = json.loads(json.dumps(
        {
            "error": {
                "code": "HTTP_400_BAD_REQUEST",
                "message": err.args[0]
            }
        }))
    return response


def validate_app_user():
    """Validate app user for the invoked service. Client Id and User Id needs to be passed to the service"""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_claims = get_jwt_claims()
                user_id = user_claims['user_id']
                client_id = kw['clientid']
                app_id = kw['appid']
                try:
                    connection = NoodleMongoClient()
                    return_object = connection.find_selected_field(
                        collectionname="permissions",
                        data={
                            "user_id": user_id,
                            "clientid": {"$in": [client_id, '*']},
                            "entityId": {"$in": [app_id, '*']},
                            "isActive": True,
                            "entityType": {"$in": ["app", '*']}
                        },
                        projection={"clientid": 1,
                                    "entityId": 1,
                                    "entityType": 1,
                                    "permission": 1}

                    )
                except PyMongoError as err:
                    response = jsonify(
                        {
                            "error": {
                                "code": err.args[0],
                                "message": "Internal Server Error"
                            }
                        })
                    response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
                    return response
                if return_object:
                    if return_object[0]['entityId'] in [app_id, '*']:
                        if request.method != 'GET' and return_object[0]['permission'] == 'read':
                            response = jsonify(
                                {
                                    "error": {
                                        "code": "HTTP_403_FORBIDDEN",
                                        "message": "User does not have required permission for this action"
                                    }
                                })
                            response.status_code = HTTP_403_FORBIDDEN
                            return response
                    else:
                        response = jsonify(
                            {
                                "error": {
                                    "code": "HTTP_403_FORBIDDEN",
                                    "message": "User does not have access to this Application"
                                }
                            })
                        response.status_code = HTTP_403_FORBIDDEN
                        return response

                else:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_403_FORBIDDEN",
                                "message": "User does not have access to this Application"
                            }
                        })
                    response.status_code = HTTP_403_FORBIDDEN
                    return response

            except ValidationError as err:
                response = json.loads(json.dumps(
                    {
                        "error": {
                            "code": "HTTP_400_BAD_REQUEST",
                            "message": err.args[0]
                        }
                    }))
                return response, HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator


def validate_user(roles):
    """Validate user with the required role."""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_claims = get_jwt_claims()
                user_role = user_claims['userRole']
                if user_role not in roles:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_403_FORBIDDEN",
                                "message": "User does not have required role to for this action"
                            }
                        })
                    response.status_code = HTTP_403_FORBIDDEN
                    return response
                if 'clientid' in kw.keys() and user_role == 'manager':
                    user_id = user_claims['user_id']
                    connection = NoodleMongoClient()
                    return_object = connection.find_one(
                        collectionname="users",
                        data={"$and": [
                            {
                                "user_id": user_id
                            },
                            {
                                "clients": {
                                    "$elemMatch": {
                                        "clientid": kw['client_id']
                                    }
                                }
                            }]}
                    )
                    if not return_object:
                        response = jsonify(
                            {
                                "error": {
                                    "code": "HTTP_403_FORBIDDEN",
                                    "message": "Manager does not belong to this client"
                                }
                            })
                        response.status_code = HTTP_403_FORBIDDEN
                        return response

            except ValidationError as err:
                response = json.loads(json.dumps(
                    {
                        "error": {
                            "code": "HTTP_400_BAD_REQUEST",
                            "message": err.args[0]
                        }
                    }))
                return response, HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator


def compare_user_role():
    """compare between logged in and target user role.
       Works only with services which takes user_id as argument"""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_id = kw['user_id']
                connection = NoodleMongoClient()
                return_object = connection.find_one(
                    collectionname="users",
                    data={
                        "user_id": user_id
                    }
                )
            except PyMongoError as err:
                response = jsonify(
                    {
                        "error": {
                            "code": err.args[0],
                            "message": "Internal Server Error"
                        }
                    })
                response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
                return response
            if return_object:
                target_role = return_object['userRole']
                user_claims = get_jwt_claims()
                login_user_role = user_claims['userRole']

                role_hierarchy = {'super-admin': 1,
                                  'admin': 2,
                                  'manager': 3,
                                  'user': 4,
                                  'guest': 5}

                if role_hierarchy[login_user_role] >= role_hierarchy[target_role]:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_403_FORBIDDEN",
                                "message": "User cannot modify another user with same or higher role"
                            }
                        })
                    response.status_code = HTTP_403_FORBIDDEN
                    return response
            else:
                response = jsonify(
                    {
                        "error": {
                            "code": "HTTP_404_NOT_FOUND",
                            "message": "{0} User id is not available".format(user_id)
                        }
                    })
                response.status_code = HTTP_404_NOT_FOUND
                return response

            return f(*args, **kw)
        return wrapper
    return decorator