#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from services.clients import ClientService
from configuration.layer_fetch_environ import (
    SCHEDULE_COLLECTION
)

#------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.schedule_schema import list_schema
#------------------------------------------------------------------------------


class ScheduleListBulk(Resource):
    """Schedule List Delete."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The type to be searched"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            }

        ],
        notes='List all Schedules',
        nickname='List schedules')
    @validate_json
    @validate_schema(list_schema(), is_path=True)
    @jwt_required
    def get(self, type):
        """Get list of all dag schedules"""
        limit = int(request.args.get('limit', 5))
        offset = int(request.args.get('offset', 0))
        keyword = request.args.get('keyword', '')
        output = self.service.list_all_schedules(collection_name=SCHEDULE_COLLECTION,
                                                 type=type,
                                                 offset=offset,
                                                 limit=limit,
                                                 keyword=keyword
                                                 )

        return output

