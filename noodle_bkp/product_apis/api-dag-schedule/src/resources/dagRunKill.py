#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from configuration.layer_fetch_environ import (
    DATE_FORMAT,
    RUN_STATUS_COLLECTION
)
from services.clients import ClientService
from swagger.schedule_swagger import CreateSchedule

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.schedule_schema import create_schedule
#------------------------------------------------------------------------------


class DagRunKill(Resource):
    """Schedule List Delete."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "run_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The run_id to be killed"
            }
        ],
        notes='Kill running DAG',
        nickname='Kill running DAG')
    @jwt_required
    def get(self, run_id):
        """Kill a running DAG"""
        output = self.service.kill_dag_run(run_id)
        return output
