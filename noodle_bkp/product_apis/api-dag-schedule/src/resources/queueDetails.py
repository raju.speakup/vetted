#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from services.queues import QueueListService
from configuration.layer_fetch_environ import (
    BROKER_URL
)

#------------------------------------------------------------------------------


class QueueDetails(Resource):
    """List all queues."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.error = None
        self.error_code = None
        self.service = QueueListService(
            broker_url=BROKER_URL
        )

    @swagger.operation(
        parameters=[
            {
                "name": "type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                       "description": "The type to be searched"
            }
        ],
        notes='List queues',
        nickname='List rabbitmq queues')
    @jwt_required
    def get(self, type):
        """Get list of rabbitmq queues based on type"""
        output = self.service.get_all_queues(type)
        return output
