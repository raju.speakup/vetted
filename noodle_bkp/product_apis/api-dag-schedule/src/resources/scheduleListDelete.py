#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from configuration.layer_fetch_environ import (
    DATE_FORMAT,
    SCHEDULE_COLLECTION
)
from services.clients import ClientService
from swagger.schedule_swagger import UpdateSchedule

#------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.schedule_schema import update_schedule, id_path_schema
#------------------------------------------------------------------------------


class ScheduleListDelete(Resource):
    """Schedule List Delete."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateSchedule.__name__,
                "paramType": "body",
                "required": True,
                "description": "PUT body"
            },
            {
                "name": "schedule_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The dag_id to be updated"
            }
        ],
        notes='Schedule Create',
        nickname='DAG schedule')
    @validate_json
    @validate_schema(update_schedule())
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    def put(self, schedule_id):
        """Edit dag schedule"""
        data = json.loads(request.data.decode('utf-8'))
        user_id = request.headers.get('userid', 'unknown')
        data.update({
            "updated_by": user_id,
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })
        logger.info(data)
        output = self.service.update_schedule(collection_name=SCHEDULE_COLLECTION,
                                              schedule_id=schedule_id,
                                              data=data)

        return output


    @swagger.operation(
        parameters=[
            {
                "name": "schedule_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The dag_id to be fetched"
            }

        ],
        notes='Get dag Schedule ',
        nickname='DAG schedule')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    def get(self, schedule_id):
        """Get dag schedule"""
        output = self.service.get_schedule(collection_name=SCHEDULE_COLLECTION, schedule_id=schedule_id)

        return output


    @swagger.operation(
        parameters=[
            {
                "name": "schedule_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The dag_id to be deleted"
            }

        ],
        notes='Get Schedule',
        nickname='DAG schedule')
    @validate_json
    @validate_schema(id_path_schema(), is_path=True)
    @jwt_required
    def delete(self, schedule_id):
        """Delete dag schedule"""
        output = self.service.delete_schedule(schedule_id)
        return output