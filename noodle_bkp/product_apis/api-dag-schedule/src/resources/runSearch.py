#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from services.run_clients import RunService
from configuration.layer_fetch_environ import (
    RUN_STATUS_COLLECTION
)

#------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.schedule_schema import search_schema
#------------------------------------------------------------------------------


class RunSearch(Resource):
    """Schedule List Delete."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = RunService()

    @swagger.operation(
        parameters=[
            {
                "name": "type",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "The type to be searched"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "The keyword to be searched"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            }

        ],
        notes='List all runs',
        nickname='List runs')
    @validate_json
    @validate_schema(search_schema(), is_path=True)
    @jwt_required
    def get(self, type):
        """Search for a schedule"""
        keyword = request.args.get('keyword', '')
        limit = int(request.args.get('limit', 5))
        offset = int(request.args.get('offset', 0))
        output = self.service.search_run(collection_name=RUN_STATUS_COLLECTION, type=type,
                                              keyword=keyword, offset=offset, limit=limit)

        return output

