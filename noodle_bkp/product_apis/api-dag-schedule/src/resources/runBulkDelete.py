#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from services.run_clients import RunService

#------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.schedule_schema import bulk_delete_run_schema
#------------------------------------------------------------------------------


class RunBulkDelete(Resource):
    """Schedule List Delete."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = RunService()

    @swagger.operation(
        parameters=[
            {
                "name": "run_ids",
                "dataType": 'list',
                "paramType": "body",
                "required": True,
                "description": "The list of run_ids to be deleted"
            }

        ],
        notes='Delete dag runs',
        nickname='Delete DAG runs')
    @validate_json
    @validate_schema(bulk_delete_run_schema())
    @jwt_required
    def delete(self):
        """Bulk delete runs"""
        data = json.loads(request.data.decode('utf-8'))
        ids = data['run_ids']
        output = self.service.bulk_delete_runs(run_ids=ids)
        return output

