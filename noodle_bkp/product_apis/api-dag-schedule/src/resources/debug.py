#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask_restful_swagger import swagger
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from configuration.layer_fetch_environ import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
    PORT,
    DEFAULT_IP,
    DEBUG
)
from logger.logger import logger
from pymongo.errors import PyMongoError
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR)
import json
from bson import json_util
from flask import jsonify
import random


class CheckDbStatus(Resource):
    """Check health of system."""
    @swagger.operation(
        parameters=[],
        notes='Health Get',
        nickname='HealthGet')
    def get(self):
        """Check health of system."""
        result = {}
        result.update({
            'serverName': SERVER_NAME,
            'databaseName': DB_NAME,
            'userName': USERNAME,
            'port': PORT,
            'dafaultIP': DEFAULT_IP
        })

        try:
            mongo_url = "".join(
                ['mongodb://', USERNAME, ':',
                 PASSWORD, '@', SERVER_NAME])
            dummy_url = "".join(
                ['mongodb://', USERNAME, ':',
                 ''.join(random.sample(PASSWORD, len(PASSWORD))), '@', SERVER_NAME])
            if DEBUG:
                mongo_url = None
            connect = MongoClient(
                mongo_url)
            print(connect[DB_NAME].collection_names())
        except PyMongoError as err:
            logger.error(err)
            response = jsonify(
                {
                    "data": {
                        "status": "Not Connected",
                        "MongoUrl": dummy_url
                    }
                })
            response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
            return response

        except ConnectionFailure:
            result.update({
                "databaseUp": "Not able to communicate with MongoDB"})
            logger.error(result)
            return (json.dumps(str(result))), HTTP_500_INTERNAL_SERVER_ERROR
        response = jsonify(
            {
                "data": {
                    "status": 'Connected',
                    "MongoUrl": dummy_url
                }
            })
        response.status_code = HTTP_200_OK
        return response


class ServiceStatus(Resource):
    """Check Service status """
    @swagger.operation(
        parameters=[],
        notes='Service Staus Get',
        nickname='Service Status Get')
    def get(self):
        """Check Service status."""
        result = {}
        result.update({
            'status': 'Connected'
        })
        result = json.dumps(result, default=json_util.default)
        return json.loads(result), HTTP_200_OK


class ConfigStore(Resource):
    """Check health of system."""
    @swagger.operation(
        parameters=[],
        notes='Health Get',
        nickname='HealthGet')
    def get(self):
        """Check health of system."""
        result = {}
        result.update({
            'serverName': SERVER_NAME,
            'databaseName': DB_NAME,
            'userName': USERNAME,
            'port': PORT,
            'dafaultIP': DEFAULT_IP
        })
        result = json.dumps(result, default=json_util.default)
        return json.loads(result), HTTP_200_OK