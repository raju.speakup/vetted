#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

from flask import request, g
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json
from commons.json_validator import validate_schema
from configuration.layer_fetch_environ import (
    DATE_FORMAT,
    SCHEDULE_COLLECTION
)
from services.clients import ClientService
from swagger.schedule_swagger import CreateSchedule

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
from schemas.schedule_schema import create_schedule
#------------------------------------------------------------------------------


class ScheduleDetails(Resource):
    """Schedule List Delete."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = ClientService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateSchedule.__name__,
                "paramType": "body",
                "required": True,
                "description": "POST body"
            }
        ],
        notes='Schedule Create',
        nickname='DAG schedule')
    @validate_json
    @validate_schema(create_schedule())
    @jwt_required
    def post(self):
        """Create a DAG schedule"""
        print (request.data.decode('utf-8'))
        data = json.loads(request.data.decode('utf-8'))
        new_doc = data
        name = data.get('name')
        schedule_id = str(uuid.uuid4())
        user_id = request.headers.get('userid', 'unknown')
        new_doc.update({
            "created_by": user_id,
            "updated_by": user_id,
            "schedule_id": schedule_id,
            "created_at": datetime.datetime.now().strftime(DATE_FORMAT),
            "updated_at": datetime.datetime.now().strftime(DATE_FORMAT)
        })
        if 'appid' in data:
            new_doc['app_id'] = str(data['appid'])
        else:
            new_doc['app_id'] = str(g.app_id)

        if 'clientid' in data:
            new_doc['client_id'] = str(data['clientid'])
        else:
            new_doc['client_id'] = str(g.client_id)

        logger.info(data)
        output = self.service.create_schedule(collection_name=SCHEDULE_COLLECTION,
                                       data=new_doc)
        return output

