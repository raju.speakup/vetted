
#import sys, os
#sys.path.append(os.path.dirname(__file__))

def removeNode():
    # remove id
    print("Remove Node")

def validate_get_url_args(req):
    par_list = ['limit', 'offset', 'sort', 'projection']
    if len(set(req.args.keys()).difference(par_list)):
        return 'ERROR: Unrecognized query parameter'

    if req.args.get("limit", None):
        if type(req.args.get("limit", None)) != str:
            return "ERROR: Bad input for 'limit'"
        try:
            int(req.args.get("limit", None))
        except ValueError:
            return "ERROR: Bad input for 'limit'"

    if req.args.get("offset", None):
        if type(req.args.get("offset", None)) != str:
            return "ERROR: Bad input for 'offset'"
        try:
            int(req.args.get("offset", None))
        except ValueError:
            return "ERROR: Bad input for 'offset'"

    if req.args.get("sort", None):
        if type(req.args.get("sort", None)) != str:
            return "ERROR: Bad input for 'sort'"

    if req.args.get("projection", None):
        if type(req.args.get("projection", None)) != str:
            return "ERROR: Bad input for 'projection'"
        if req.args.get("projection", None) not in ['BASIC', 'FULL']:
            return "ERROR: Bad input for 'projection', supported values: 'BASIC' and 'FULL'"

    return 'OK'

