#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code - Dynamic delete function
    Functionality:
        Generates dynamic delete query.
"""


def dynamic_queris_delete(
        table_name,
        where_query):
    """
    Generate dynamic query for deleting a row entry of table.

    Args:
        table_name (str): Specify table name.
        where_query (str): Specify which record to delete.

    Returns:
        Delete query with table name and where clause
    """

    query = "DELETE FROM " + table_name
    query = query + where_query + ';'
    return query
