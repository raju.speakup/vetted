#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources Dynamic data updates
    Functionality:
        Detect column type and return data in specified datatype
"""


def get_char_set_type(data_type, data):
    """
    Get data-type and data
    Convert datum in equivalent data-type

    Args:
        data_type (str): Specify data-type
        data (str): datum

    Returns:
        bool: Description of return value
    """

    return {
        'varchar': "'{0}'".format(data),

        'bigint': "{0}".format(data),

        'datetime': "'{0}'".format(data)

    }.get(data_type, 'Not Found')
