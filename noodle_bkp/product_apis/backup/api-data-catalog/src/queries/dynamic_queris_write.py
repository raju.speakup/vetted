#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get_column_data: removes any field specified from dict

        table_inseration_query: Create table_inseration_query
            clause with Table Name, Column Names, Data Values and Where Clause.

        correct_data_values: Correct every data values insertion statement
            with bases of column type

        column_list_string: Generate column names as string fro list

        where_query: Create where clause

"""

from .dynamic_data_type import get_char_set_type


def get_column_data(id_field, data_dict={}):
    """
    get_column_data removes any field specified from dict

    Args:
        id_field (str): table name
        data_dict (str): data values

    Returns:
        column_names (list): Column names
        data_values (list): Data values
    """
    column_names = []
    data_values = []

    for index, key in enumerate(data_dict):
        if not key == id_field:
            column_names.append(str(key))
            data_values.append(str(data_dict[key]))
    return column_names, data_values


def table_inseration_query(
        table_name, column_names, data_values, where_clause=[]):
    """
    Create table_inseration_query clause with Table Name,
    Column Names, Data Values and Where Clause.

    Args:
        table_name (str): table name
        column_names (str): Column names
        data_values (str): Data values
        where_clause (list): Where clause in list

    Returns:
        query (str): Part query as insertion clause.
    """
    column_names = ",".join(column_names)

    query = "INSERT INTO " + table_name
    if column_names:
        query = query + " ( " + column_names + " ) " + \
            "VALUES" + " (" + data_values + " ) "

    query = query + where_clause if len(where_clause) > 0 else query + " ;"

    return query


def correct_data_values(data_values=[], column_names=[], column_type={}):
    """
    Correct every data values insertion statement with bases of column type.

    Args:
        data_values (list): data values
        column_names (list): column names
        column_type (dict): column type

    Returns:
        data_values_return (str): Part query as insertion clause.
    """
    data_values_return = ""

    for index, element in enumerate(column_names):
        data_values_return = data_values_return + get_char_set_type(
            column_type.get(element), data_values[index])
        data_values_return = data_values_return + \
            ',' if not index == len(column_names) - 1 else data_values_return

    return data_values_return


def column_list_string(column_names=[]):
    """
    Generate column names as string fro list

    Args:
        column_names (list): column names

    Returns:
        column_names (str): column names separated by ,(comma)
    """
    column_names = ','.join(column_names)
    return column_names


def where_query(fields=[]):
    """
    Create where clause

    Args:
        fields (list): list of fields which consist 3 parts

    Returns:
        where_query (str): string containing where clause.
    """
    if not fields:
        return ""

    where_query = " WHERE "
    for index, field in enumerate(fields):
        where_query = where_query + \
            field[0] + " " + field[1] + " " + field[2]

        if not index == len(fields) - 1:
            where_query = where_query + " and "
    return where_query
