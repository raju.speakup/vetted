#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        dynamic_queris_read: Generate dynamic query for
                            reading data from table.

        get_column_names: Get column names as string.

        get_sorting_order: Get sorting order of every field specified.

        get_sorting_dict: Create a dict of filed provided in URL

        between_query: Create between clause with field list

        where_query: Create where clause with field list
"""


def dynamic_queris_read(
        table_name, between_query, where_query,
        column_names=[], sorting={}):
    """
    Generate dynamic query for reading data from table.

    Args:
        table_name (str): Specify table name.
        between_query (str): Specify between query.
        where_query (str): Specify where query.
        column_names (list): Column names in list.
        sorting (dict): Specify sorting fields.

    Returns:
        query (str): Select query.
    """
    query = "SELECT " + \
        get_column_names(column_names) + \
        " FROM " + \
        table_name

    query = query + between_query if between_query else query

    query = query + (where_query.replace("WHERE", "")
                     if between_query else where_query)
    query = query + get_sorting_order(sorting) + \
        ';' if sorting else query + ';'

    return query


def get_column_names(column_names=[]):
    """
    Get column names as string.

    Args:
        column_names (str): provide column names list.

    Returns:
        columns (str): column names as string separated by ,
    """
    columns = ""
    columns = ','.join(
        map(str, column_names)) if column_names else "*"

    return columns


def get_sorting_order(sorting={}):
    """
    Get sorting order of every field specified.

    Args:
        sorting (dict): Specify sorting fields in dict.

    Reference query:
        SELECT * FROM Customers
        ORDER BY Country ASC, CustomerName DESC;

    Returns:
        Part query of order sql
    """
    sort_query = "ORDER BY"
    for index, key in enumerate(sorting):

        sort_query = ' ' + \
            sort_query + \
            ' ' + key + ' ' + \
            sorting[key]

        if not index == len(sorting) - 1:
            sort_query = sort_query + ','

    return sort_query


def get_sorting_dict(fields=[]):
    """
    Create a dict of filed provided in URL

    Args:
        fields (list): List of fields

    Operation:
        Detect every entry in list - If any entry has negative (-) sign
        convert field as DESC else ASC

    Returns:
        sorting_dict (dict): Key value pair of field and it's sort order
    """
    sorting_dict = {}

    for field in fields:
        if field:
            sort_type = 'DESC' if '-' in field else 'ASC'

            remove_symbol = field.replace(
                "-", "") if '-' in field else field

            sorting_dict.update({remove_symbol: sort_type})
    return sorting_dict


def between_query(fields=[]):
    """
    Create between clause with field list

    Args:
        fields (str): list of fields

    Operation:
        Each field is contains 3 subpart
            a) Column name
            b) Operation
            c) Value

    Returns:
        between_query (str): Part query as between clause.
    """
    between_query = " WHERE "
    for index, field in enumerate(fields):
        between_query = between_query + \
            field[0] + " between " + field[1] + " and " + field[2]

        if not index == len(fields) - 1:
            between_query = between_query + " and "

    return between_query


def where_query(fields=[]):
    """
    Create where clause with field list

    Args:
        fields (str): list of fields

    Operation:
        Each field is contains 3 subpart
            a) Column name
            b) Operation
            c) Value

    Returns:
        where_query (str): Part query as between clause.
    """
    if not fields:
        return ""

    where_query = " WHERE "
    for index, field in enumerate(fields):
        where_query = where_query + \
            field[0] + " " + field[1] + " " + field[2]

        if not index == len(fields) - 1:
            where_query = where_query + " and "
    return where_query
