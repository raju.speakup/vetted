schema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Data catalog",
    "description": "A Noodle App component",
    "type": "object",
    "properties": {
        "datasets" : {
            "description": "Datasets for the data store item",
            "type": "array",
            "items" : {
                "type" : "object",
                "properties" : {
                    "name" : {"description": "Name of the dataset",
                              "type": "string", "minLength" : 1},
                    "description" : {"description": "Description of the dataset",
                                     "type": "string"},
                    # "status" : {"description": "Description of the dataset",
                    #             "type": "string",
                    #             "enum": ["CATALOGED", "INGESTED", "PROCESSING", "CONSUMPTION_READY"]},
                    # "createdBy" : {"description": "User who creates the dataset",
                    #                "type": "string"},
                    "dbSourceTableName" : {"description": "Table name from the source dataset table",
                                           "type": "string"},
                    "dbTargetTableName" : {"description": "Table name for the target dataset table",
                                           "type": "string"},
                    # "createdOn" : {"description": "Date of the dataset creation",
                    #                "type": "string"},
                    # "modifiedBy" : {"description": "User who updates the dataset",
                    #                 "type": "string"},
                    # "modifiedOn" : {"description": "Date when dataset was updated",
                    #                 "type": "string"},
                    # "estimatedSizeMB" : {"description": "Dataset size in MBs",
                    #                      "type": "number"},
                    # "estimatedRecord" : {"description": "Dataset record count in number of rows",
                    #                      "type": "number"},
                    # "labels" : {"description": "Dataset record count in number of rows",
                    #             "type": "array",
                    #             "items": {"type": "string"}},
                    "schemaJSON" : {"description": "Dataset schema",
                                    "type": "object"},
                    # "schemaRecordJSON" : {"description": "Dataset records in JSON output format",
                    #                       "type": "object"},
                },
                "required" : ["name", "dbSourceTableName", "dbTargetTableName"]
            }
        },
    },

}