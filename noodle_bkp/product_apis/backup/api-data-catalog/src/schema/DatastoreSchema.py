schema_datastore_create = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Create a new Data Store",
    "description": "Creata a new Data Store",
    "type": "object",
    "properties": {
        "clientId": {"type": "string"},
        "appIds": {
            "description": "Application ID to link to the data store item",
            "type": "array",
            "items": {
                "type": "string"}
        },
        "datastoreName": {"type": "string"},
        "datastoreDescription": {"type": "string"},
        "datastoreCreatedBy": {"type": "string"},
        "datastoreType": {
            "description": "Classification of data store",
            "type": "string",
            "enum": ["Public", "Internal", "External", "Client"]
        },
        "ingestionType": {
            "type": "string",
            "enum": ["FTP", "SQLDatabase"]
        },
        "ingestionDetails": {
            "type": "object",
            "properties": {
                # if the source is of type database
                "dbSourceHost": {"type": "string"},
                "dbSourceName": {"type": "string"},
                "dbSourceUsername": {"type": "string"},
                "dbSourcePassword": {"type": "string"},
                "dbSourcePort": {"type": "number"},
                "dbSourceSchemaName": {"type": "string"},
                "dbSourceType": {
                    "type": "string",
                    "enum": ["SQLServer", "oracleServer", "mySQLServer"]},
                # if the source of type FTP
                "ftpSourceHost": {"type": "string"},
                "ftpSourceUsername": {"type": "string"},
                "ftpSourcePassword": {"type": "string"},
                "ftpSourcePort": {"type": "number"},
                "ftpSourceDirectory": {"type": "string"},
                "ftpSourceFileName": {"type": "string"},
                "ftpSourceFileDelimiter": {"type": "string"},
                "ftpSourceFileExtension": {"type": "string"},
                "ftpSourceFileHeader": {"type": "number"},
                "ftpSourceFileTextQualifier": {"type": "string"},
            }
        },
        "targetDetails": {
            "description": "Data store target details",
            "type": "object",
            "properties": {
                "dbTargetHost": {
                    "description": "Db target host name", "type": "string"},
                "dbTargetName": {"type": "string"},
                "dbTargetUsername": {"type": "string"},
                "dbTargetPassword": {"type": "string"},
                "dbTargetPort": {"type": "number"},
                "dbTargetSchemaName": {"type": "string"}

            }
        },
    },

    "required": ["datastoreName", "datastoreDescription", "datastoreCreatedBy", "datastoreUpdatedBy", "datastoreType",
                 "ingestionType", "ingestionDetails", "targetDetails"],

    "dependencies": {
        "ingestionDetails": ["ingestionType"]
    }
}

schema_datastore_get = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Create a new Data Store",
    "description": "Creata a new Data Store",
    "type": "object",
    "properties": {
        "clientId": {"type": "string"},
        "appIds": {
            "description": "Application ID to link to the data store item",
            "type": "array",
            "items": {
                "type": "string"}
        },
        "datastoreName": {"type": "string"},
        "datastoreDescription": {"type": "string"},
        "datastoreCreatedBy": {"type": "string"},
        "datastoreType": {
            "description": "Classification of data store",
            "type": "string",
            "enum": ["Public", "Internal", "External", "Client"]
        },
        "ingestionType": {
            "type": "string",
            "enum": ["FTP", "SQLDatabase"]
        },
        "ingestionDetails": {
            "type": "object",
            "properties": {
                # if the source is of type database
                "dbSourceHost": {"type": "string"},
                "dbSourceName": {"type": "string"},
                "dbSourceUsername": {"type": "string"},
                "dbSourcePassword": {"type": "string"},
                "dbSourcePort": {"type": "number"},
                "dbSourceSchemaName": {"type": "string"},
                "dbSourceType": {
                    "type": "string",
                    "enum": ["SQLServer", "oracleServer", "mySQLServer"]},
                # if the source of type FTP
                "ftpSourceHost": {"type": "string"},
                "ftpSourceUsername": {"type": "string"},
                "ftpSourcePassword": {"type": "string"},
                "ftpSourcePort": {"type": "number"},
                "ftpSourceDirectory": {"type": "string"},
                "ftpSourceFileName": {"type": "string"},
                "ftpSourceFileDelimiter": {"type": "string"},
                "ftpSourceFileExtension": {"type": "string"},
                "ftpSourceFileHeader": {"type": "number"},
                "ftpSourceFileTextQualifier": {"type": "string"},
            }
        },
        "targetDetails": {
            "description": "Data store target details",
            "type": "object",
            "properties": {
                "dbTargetHost": {
                    "description": "Db target host name", "type": "string"},
                "dbTargetName": {"type": "string"},
                "dbTargetUsername": {"type": "string"},
                "dbTargetPassword": {"type": "string"},
                "dbTargetPort": {"type": "number"},
                "dbTargetSchemaName": {"type": "string"}

            }
        },
    },

    "required": ["datastoreName", "datastoreDescription", "datastoreCreatedBy", "datastoreUpdatedBy", "datastoreType",
                 "ingestionType", "ingestionDetails", "targetDetails", "datastoreId", "datastoreCreatedAt",
                 "datastoreUpdatedAt"],

    "dependencies": {
        "ingestionDetails": ["ingestionType"]
    }
}

schema_datastore_update = {

    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Update a Datastore",
    "description": "Creata a new Data Store",
    "type": "object",
    "properties": {
        "clientId": {"type": "string"},
        "appIds": {
            "description": "Application ID to link to the data store item",
            "type": "array",
            "items": {
                "type": "string"}
        },
        "datastoreName": {"type": "string"},
        "datastoreDescription": {"type": "string"},
        "datastoreUpdatedBy": {"type": "string"},
        "ingestionDetails": {
            "type": "object",
            "properties": {
                # if the source is of type database
                "dbSourceHost": {"type": "string"},
                "dbSourceName": {"type": "string"},
                "dbSourceUsername": {"type": "string"},
                "dbSourcePassword": {"type": "string"},
                "dbSourcePort": {"type": "number"},
                "dbSourceSchemaName": {"type": "string"},
                "dbSourceType": {
                    "type": "string",
                    "enum": ["SQLServer", "oracleServer", "mySQLServer"]},
            }
        },
        "targetDetails": {
            "description": "Data store target details",
            "type": "object",
            "properties": {
                "dbTargetHost": {
                    "description": "Db target host name", "type": "string"},
                "dbTargetName": {"type": "string"},
                "dbTargetUsername": {"type": "string"},
                "dbTargetPassword": {"type": "string"},
                "dbTargetPort": {"type": "number"},
                "dbTargetSchemaName": {"type": "string"}

            }
        },
    },
}
