schema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Create  Data Store",
    "description": "creating data store",
    "type": "object",
    "properties": {
        "name": {
            "description": "Name of the data store item",
            "type": "string",
            "minLength": 3
        },
        "clientId": {
            "description": "Engagement ID to link to the data store item",
            "type": "string",
        },
        "appIds": {
            "description": "Application ID to link to the data store item",
            "type": "array",
            "items": {
                "type": "string"}
        },
        "description": {
            "description": "Description of the data store item",
            "type": "string"
        },
        "dataStoreId": {
            "description": "data store ID",
            "type": "string"
        },
        "ingestionType": {
            "description": "Ingestion type of the data store item",
            "type": "string",
            "enum": ["FTP", "SQLDatabase"]
        },
        "ingestionDetails": {
            "description": "Data store ingestion details from source, "
                           "choose the keys based on ingestion type",
            "type": "object",
            "properties": {
                # if the source is of type database
                "dbSourceHost": {"type": "string"},
                "dbSourceName": {"type": "string"},
                "dbSourceUsername": {"type": "string"},
                "dbSourcePassword": {"type": "string"},
                "dbSourcePort": {"type": "number"},
                "dbSourceSchemaName": {"type": "string"},
                "dbSourceType": {
                    "type": "string",
                    "enum": ["SQLServer", "oracleServer", "mySQLServer"]},
                # if the source of type FTP
                "ftpSourceHost": {"type": "string"},
                "ftpSourceUsername": {"type": "string"},
                "ftpSourcePassword": {"type": "string"},
                "ftpSourcePort": {"type": "number"},
                "ftpSourceDirectory": {"type": "string"},
                "ftpSourceFileName": {"type": "string"},
                "ftpSourceFileDelimiter": {"type": "string"},
                "ftpSourceFileExtension": {"type": "string"},
                "ftpSourceFileHeader": {"type": "number"},
                "ftpSourceFileTextQualifier": {"type": "string"},

            }
        },
        "targetDetails": {
            "description": "Data store target details",
            "type": "object",
            "properties": {
                "dbTargetHost": {
                    "description": "Db target host name", "type": "string"},
                "dbTargetName": {"type": "string"},
                "dbTargetUsername": {"type": "string"},
                "dbTargetPassword": {"type": "string"},
                "dbTargetPort": {"type": "number"},
                "dbTargetSchemaName": {"type": "string"}

            }
        },
        "datasetCount": {
            "description": "Number of datasets for the given datastore item",
            "type": "number",
            "minimum": 0
        },
        # "licensingType": {
        #     "description": "Number of datasets for the given datastore item",
        #     "type": "string",
        # },
        "dataStoreType": {
            "description": "Classification of data store",
            "type": "string",
            "enum": ["Public", "Internal", "External", "Client"]
        },
        # "isPaid": {
        #     "description": "Data store purchasing and availability information",
        #     "type": "boolean",
        # },
        # "owner": {
        #     "description": "Owner of the data store item",
        #     "type": "string",
        # },
        # "websiteURL": {
        #     "description": "Web source of the data store item",
        #     "type": "string",
        #     # pattern matching to be done in the REST handler using urlparse
        # },
        "datasets": {
            "description": "Datasets for the data store item",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {"description": "Name of the dataset",
                             "type": "string", "minLength": 3},
                    "description": {
                        "description": "Description of the dataset",
                        "type": "string"},
                    "datasetId": {"description": "Description of the dataset",
                                  "type": "string"
                                  },
                    "status": {"description": "Description of the dataset",
                               "type": "string",
                               "enum": [
                                   "CATALOGED", "INGESTED",
                                   "PROCESSING", "CONSUMPTION_READY"]},
                    "createdBy": {"description": "User who creates the dataset",
                                  "type": "string"},
                    "dbSourceTableName": {
                        "description": "Table name from the source dataset table",
                        "type": "string"},
                    "dbTargetTableName": {
                        "description": "Table name for the target dataset table",
                        "type": "string"},
                    "createdOn": {
                        "description": "Date of the dataset creation",
                        "type": "string"},
                    "modifiedBy": {
                        "description": "User who updates the dataset",
                        "type": "string"},
                    "modifiedOn": {
                        "description": "Date when dataset was updated",
                        "type": "string"},
                    "estimatedSizeMB": {
                        "description": "Dataset size in MBs",
                        "type": "number"},
                    "estimatedRecord": {
                        "description": "Dataset record count in number of rows",
                        "type": "number"},
                    "labels": {
                        "description": "Dataset record count in number of rows",
                        "type": "array",
                        "items": {"type": "string"}},
                    "schemaJSON": {"description": "Dataset schema",
                                   "type": "object"},
                    "schemaRecordJSON": {
                        "description": "Dataset records in JSON output format",
                        "type": "object"},
                },
                "required": ["name", "dbSourceTableName", "dbTargetTableName"]
            }
        },
    },

    "required": ["name", "ingestionType", "targetDetails", "clientId", "appIds"],

    "dependencies": {
        "ingestionDetails": ["ingestionType"]
    }
}
