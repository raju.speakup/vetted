schema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Data catalog",
    "description": "A Noodle App component",
    "type": "object",
    "properties": {
        "labels" : {"description": "Dataset record count in number of rows",
                    "type": "array",
                    "items": {"type": "string"}},
    },
    "required" : ["labels"]
}