schema_dataset_create = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Data set",
    "description": "A Noodle App component",
    "type": "object",
    "properties": {
        "datasets": {
            "description": "Datasets for the data store item",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "datasetName": {"type": "string"},
                    "datasetDescription": {"type": "string"},
                    "datasetCreatedBy": {"type": "string"},
                    "dbSourceTableName": {"description": "Table name from the source dataset table",
                                          "type": "string"},
                    "dbTargetTableName": {"description": "Table name for the target dataset table",
                                          "type": "string"},
                },
                "required": ["datasetName", "datasetDescription", "datasetCreatedBy", "dbSourceTableName",
                             "dbTargetTableName"]
            }
        },
    },
}

schema_dataset_get_no_table_schema = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Data set no table schema.",
    "description": "A Noodle App component",
    "type": "object",
    "properties": {
        "datasets": {
            "description": "Datasets for the data store item",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "datasetName": {"type": "string"},
                    "datasetNescription": {"type": "string"},
                    "datasetId ": {"type": "string"},
                    "datasetCreatedBy": {"type": "string"},
                    "datasetCreatedAt": {"type": "string"},
                    "datasetUpdatedBy": {"type": "string"},
                    "datasetUpdatedAt": {"type": "string"},
                    "dbSourceTableName": {"description": "Table name from the source dataset table",
                                          "type": "string"},
                    "dbTargetTableName": {"description": "Table name for the target dataset table",
                                          "type": "string"},
                },
                "required": ["datasetName", "datasetDescription", "datasetCreatedBy", "datasetCreatedAt",
                             "datasetUpdatedBy", "datasetUpdatedAt", "dbSourceTableName", "dbTargetTableName"]
            }
        },
    },
}

schema_dataset_get_with_table_schema = {}
