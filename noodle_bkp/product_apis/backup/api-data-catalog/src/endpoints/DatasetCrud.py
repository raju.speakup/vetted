#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    return_result_list,
    get_url_args,
    validate_get_url_args,
    return_result,
    json_to_dict,
    validateJsonWithSchema,
    getCurrenDateTimeUtc,
    getCurrentDateTimeIst,
    getErrorResponse,
    get_valid_api_response
)
from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)
#from schema.DatastoreSchema import schema_datastore_create, schema_datastore_get
#from swaggerdoc.catalog_store_swagger import DatasetsList
from swaggerdoc.Dataset import UpdateDataset
from schema.DatasetSchema import schema_dataset_create, schema_dataset_get_no_table_schema, schema_dataset_get_with_table_schema

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from flask import request
import uuid
from logger.logger import log
from http import HTTPStatus

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"


class DatasetCrud(Resource):

    def __init__(self):
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        log.debug("__init__ : " + connection_string)
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "description": "Max limit until which the datasets are required",
                "dataType": 'int',
                "paramType": "query",
                "default": 10},
            {
                "name": "offset",
                "description": "Offset from which the datasets are required",
                "dataType": 'int',
                "paramType": "query"},
            {
                "name": "projection",
                "description": "Projection types namely: BASIC/FULL",
                "dataType": 'string',
                "paramType": "query",
                "default": "FULL"},
            {
                "name": "sort",
                "description": "Sort key",
                "dataType": 'string',
                "paramType": "query"
            },
        ],
        notes='Get a dataset')

    def get(self, client_id, app_id, datastore_id, dataset_id):
        """
        Get a dataset for datastore
        """
        log.info("");log.info("Start of GET : Get a Dataset");
        log.debug("Received inputs client_id :{}, app_id :{}, datastore_id :{}, dataset_id :{}".format(client_id, app_id, datastore_id, dataset_id))
        limit, offset, sort_key, projection = get_url_args(req=request)

        # execute get query
        try:
            if projection == PROJECTION_FULL:
                query_output = self.store.find_one(
                    {"clientId": client_id, "datastoreId": datastore_id},
                    {"datasets": {"$elemMatch": {"datasetId": dataset_id}}, '_id': False}
                )
            else:
                query_output = self.store.find_one(
                    {"clientId": client_id, "datastoreId": datastore_id},
                    {'_id': 0, 'datastoreName': 1, "datastoreDescription": 1,
                     'datastoreCreatedBy': 1, 'datastoreCreatedAt': 1, 'datastoreId': 1
                     }
                )
            log.debug("query_output")
            log.debug(query_output)
            if query_output is None:
                log.error("Fail : Fetching a Dataset using query : find_one - params client_id, datastore_id, dataset_id ")
                return getErrorResponse("UNABLE_TO_GET_DATASET",
                                        "Getting a Dataset failed for dataset id : " + dataset_id)
            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_READ_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : obtained the json from mongo collection")


        response_json = get_valid_api_response(query_output)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK

    # def post(self, client_id, app_id, datastore_id):
    #     pass

    @swagger.operation(
        parameters=[
        ],
        notes='Delete a datastore ')

    def delete(self, client_id, app_id, datastore_id, dataset_id):
        log.info("Start of Api - Delete a dataset")
        log.info("Received inputs client_id :{}, app_id :{}, datastore_id: {}, dataset_id: {}".format(client_id, app_id, datastore_id, dataset_id))

        # Confirm if the dataset exists  and then delete
        try:
            log.debug("Checking if dataset exists")
            find_query_output = self.store.find_one(
                    {"clientId": client_id, "datastoreId": datastore_id},
                    {"datasets": {"$elemMatch": {"datasetId": dataset_id}}, '_id': False}
            )
            if find_query_output is None:
                log.debug("Find a Dataset using query : find_one - params clientId, datastore_id, dataset_id. Output : " + str(
                    find_query_output))
                self.connector.close()
                err_msg = "Fail : Unable to find the dataset with datasetId : " + dataset_id
                log.error(err_msg)
                return getErrorResponse("DATASET_NOT_FOUND", err_msg)
            else:
                log.debug("Found the Dataset")

            # Delete the datastore
            log.info("Trying to delete the dataset")
            query_output = self.store.update(
                #{"clientId": client_id, "appIds": app_id, "datastoreId": datastore_id, "datasetId": dataset_id}
                {},
                {"$pull": {
                    client_id : { datastore_id : { "datasets": {
                        "$elemMatch":{"datasetId": dataset_id}
                    }
                }
                }}},
            )
            log.debug(query_output.acknowledged)
            log.debug(query_output.deleted_count)
            log.debug(query_output.raw_result)
            log.debug(type(query_output.deleted_count))
            if (query_output.deleted_count != 1):
                log.error(
                    "Fail : Deleting a Dataset using query : delete_one - params client_id, app_id, datastore_id, dataset_id : ")
                log.error("delete_result.raw_result" + str(query_output.raw_result))
                return getErrorResponse("FAIL_DELETE_A_DATASET", "Deleting a Dataset failed")
            else:
                log.info("Successfully deleted the dataset with id : " + dataset_id)

        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR

        response_json = get_valid_api_response("Successfully deleted dataset with id : " + dataset_id)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK

    @swagger.operation(
        parameters=[
            { "name": "client_id",      "dataType": "string",   "paramType": "path",    "required": True,},
            { "name": "app_id",         "dataType": "string",   "paramType": "path",    "required": True,},
            { "name": "datastore_id",   "dataType": "string",   "paramType": "path",    "required": True,},
            { "name": "dataset_id",     "dataType": "string",   "paramType": "path",    "required": True,},
            { "name": "parameters",
              "dataType": UpdateDataset.__name__,
              "paramType": "body",
              "required": True,
              "description": "Input schema design"
            },
        ],
        notes='Update the data-set into catalog store item')

    #Todo : add updatedBy in request and updatedBy, updatedAt in response
    def put(self, client_id, app_id, datastore_id, dataset_id):
        """
        Update an existing Dataset of a Datastore
        """
        payload_str = request.data.decode('utf-8')
        log.info("");log.info("Start of API : Update a Dataset");
        log.debug("Received inputs client_id :{}, app_id :{}, datastore_id :{}, dataset_id :{}".format(client_id, app_id, datastore_id, dataset_id))
        log.debug("Received the following payload \n" + payload_str);

        # check if request payload json format is valid
        log.debug("check if request payload json format is valid")
        payload_dict = json_to_dict(payload_str)
        if 'error' in payload_dict:
            return payload_dict, HTTPStatus.BAD_REQUEST

        # Update Json Schema
        result = []
        data = payload_dict

        updated_keydict = {}
        query_id = "datasets.$."
        for key, value in data.items():
            new_key = "".join([query_id, key])
            updated_keydict.setdefault(new_key, value)

        try:
            query_output_updated = self.store.update_one(
                {"clientId": client_id, "datastoreId": datastore_id, "datasets": {"$elemMatch": {"datasetId": dataset_id}}},
                {"$set": updated_keydict}
            )
            log.debug("query_output_updated")
            log.debug(query_output_updated.raw_result)
            log.debug(query_output_updated.acknowledged)
            log.debug(query_output_updated.matched_count)
            log.debug(query_output_updated.modified_count)
            self.connector.close()
        except PyMongoError as err:
            msg = str(err)

        log.debug("result")
        log.debug(result)

        if query_output_updated.matched_count != 1:
            log.error(
                "Fail : Updating a Dataset")
            log.error("query_output_updated" + str(query_output_updated.raw_result))
            return getErrorResponse("FAIL_DELETE_A_DATASTORE", "Updating a datastore failed")
        else:
            log.info("Successfully updated dataset with id : " + dataset_id)

        try:
            query_output = self.store.find_one(
                {"clientId": client_id, "datastoreId": datastore_id},
                {"datasets": {"$elemMatch": {"datasetId": dataset_id}}, '_id': False}
            )
            log.debug("query_output")
            log.debug(query_output)
            if query_output is None:
                log.error("Fail : Fetching a Dataset using query : find_one - params client_id, datastore_id, dataset_id ")
                return getErrorResponse("UNABLE_TO_GET_DATASET",
                                        "Getting a Dataset failed for dataset id : " + dataset_id)
            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_READ_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : obtained the json from mongo collection")

        response_json = get_valid_api_response(query_output)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK