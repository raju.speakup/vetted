#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    return_result_list,
    get_url_args,
    validate_get_url_args,
    return_result,
    json_to_dict,
    validateJsonWithSchema,
    getCurrenDateTimeUtc,
    getCurrentDateTimeIst,
    getErrorResponse,
    get_valid_api_response
)
from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)
from schema.DatastoreSchema import schema_datastore_create, schema_datastore_get
from swaggerdoc.Datastore import CreateDataStoreItem
# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from flask import request
import uuid
from logger.logger import log
from http import HTTPStatus

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"


class Datastore(Resource):

    def __init__(self):
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        log.debug("__init__ : " + connection_string)
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateDataStoreItem.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input schema design"
            },
        ],
        notes='Create a DataStore')
    # TODO : Wrong implementation. The datastore does not belong to a app. IT belongs to 10 apps.
    def post(self, client_id, app_id):
        """
        :param client_id:   an uui4 value
        :param app_id:      an uui4 value
        :return:    json representation of the created datastore
        """
        payload_str = request.data.decode('utf-8')
        log.info("Start of POST : Creating Datastore");
        log.debug("Received inputs client_id :{}, app_id :{}".format(client_id, app_id))
        log.debug("Received the following payload \n" + payload_str);

        # check if request payload json format is valid
        log.debug("check if request payload json format is valid")
        payload_dict = json_to_dict(payload_str)
        if 'error' in payload_dict:
            return payload_dict, HTTPStatus.BAD_REQUEST

        # check validation of request payload json with schema
        log.debug("check validation of request payload json with schema");
        log.debug("schema_datastore_create");
        log.debug(schema_datastore_create)
        schema_validation_result = validateJsonWithSchema(payload_dict, schema_datastore_create,
                                                          "schema_datastore_create")
        if 'error' in schema_validation_result:
            return schema_validation_result, HTTPStatus.BAD_REQUEST

        persistance_data = {}  # The data that needs to persist in the mongo db.
        persistance_data.update(payload_dict);

        # Adding missing nodes
        persistance_data['datastoreId'] = str(uuid.uuid4())
        persistance_data['datastoreCreatedAt'] = getCurrenDateTimeUtc()
        persistance_data['datastoreUpdatedAt'] = getCurrentDateTimeIst()
        persistance_data['clientId'] = client_id
        # list_of_app_ids = []
        # list_of_app_ids.append(app_id)
        # persistance_data['appIds'] = list_of_app_ids
        log.debug("Data to persist into DB : ");
        log.debug(persistance_data)

        # check validation of request payload json with schema
        log.debug("check validation of persitance data with schema")
        schema_validation_result = {}
        schema_validation_result = validateJsonWithSchema(persistance_data, schema_datastore_get,
                                                          "schema_datastore_get")
        if 'error' in schema_validation_result:
            return schema_validation_result

        # Persist data into mongo DB
        log.info("Trying to persist document to mongo db")
        try:
            query_output = self.store.insert_one(persistance_data)
            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_PERSISTANCE_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : Saving the json to mongo collection")

        # Retrieve saved data to return as response
        log.info("Retrieve saved data to return as response")
        try:
            query_output = self.store.find({"clientId": client_id, "datastoreId": persistance_data['datastoreId']})
            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_READ_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : Get the json from mongo collection")
        #Todo: _id needs to removed using query
        log.info("Cleanup _id and construct the api response.")
        result = []
        try:
            for item in query_output:
                if item["_id"]:
                    item.pop("_id", None)
                result.append(item)
        except PyMongoError as err:
            msg = str(err)

        response_json = get_valid_api_response(result)
        log.debug("Response : ");
        log.debug(response_json)
        return response_json, HTTPStatus.CREATED

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "description": "Client Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
            },
            {
                "name": "app_id",
                "description": "App Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
            },
            {
                "name": "limit",
                "description": "Max limit until which the datasets are required",
                "dataType": 'int',
                "paramType": "query"},
            {
                "name": "offset",
                "description": "Offset from which the datasets are required",
                "dataType": 'int',
                "paramType": "query"},
            {
                "name": "projection",
                "description": "Projection types namely: BASIC/FULL",
                "dataType": 'string',
                "paramType": "query"},
            {
                "name": "sort",
                "description": "Sort key",
                "dataType": 'string',
                "paramType": "query"
            },
        ],
        notes='List the data-store items in offset to limit range from Database')
    # Todo : when we pass incorrect client id like 1000 the query still succeeds
    def get(self, client_id, app_id):
        """
        :param client_id:   an uui4 value
        :param app_id:      an uui4 value
        :return:    json representation of all the created datastores for the client.
        """
        log.info("Start of Api - Get all Datastores")
        log.debug("Received inputs client_id :{}, app_id :{}".format(client_id, app_id))
        limit, offset, sort_key, projection = get_url_args(req=request)

        # execute get query
        try:
            if projection == PROJECTION_FULL:
                query_output = self.store.find(
                    {"clientId": client_id, "appIds": app_id},
                    {'_id': 0}
                )
            else:
                query_output = self.store.find(
                    {"clientId": client_id, "appIds": app_id},
                    {'_id': 0, 'datastoreName': 1, "datastoreDescription": 1,
                     'datastoreCreatedBy': 1, 'datastoreCreatedAt': 1, 'datastoreId': 1
                     }
                )
            if query_output is None:
                log.error("Fail : Fetching a Datastore using query : find - params client_id, app_id : ")
                return getErrorResponse("UNABLE_TO_GET_ALL_DATASTORES",
                                        "Getting all datastore failed for client with id : " + client_id)
            self.connector.close()

            log.info("Cleanup _id and construct the api response.")
            result = []
            try:
                for item in query_output:
                    result.append(item)
            except PyMongoError as err:
                msg = str(err)

        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_READ_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : obtained the json from mongo collection")

        response_json = get_valid_api_response(result)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "description": "Client Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
            },
            {
                "name": "app_id",
                "description": "App Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
            }
        ],
        notes='delete all datastores for a client')
    # Todo : when we pass incorrect client id like 1000 the query still succeeds
    def delete(self, client_id, app_id):
        """
        :param client_id:   an uui4 value
        :param app_id:      an uui4 value
        :return: result as success / failure json
        """
        log.info("Start of Api - Delete all datastores")
        log.debug("Received inputs client_id :{}, app_id :{}".format(client_id, app_id))

        # Confirm if atleast 1 datastore exists  and then delete all
        try:
            log.debug("Checking if atleast 1 datastore exists")
            num_of_datastores = self.store.find(
                {"clientId": client_id},
                {'_id': False}
            ).count(with_limit_and_skip=True)

            if num_of_datastores == 0:
                log.debug(
                    "Find A Datastore using query : find_one - params clientId .count(with_limit_and_skip=True). Output : " + str(
                        num_of_datastores))
                self.connector.close()
                err_msg = "Fail : Unable to find even a single datastore for the client_id : " + client_id
                log.error(err_msg)
                return getErrorResponse("DATASTORES_NOT_FOUND", err_msg)
            else:
                log.debug("Number of datastores : " + str(num_of_datastores))

            # Delete the datastore
            log.info("Trying to delete the datastore")
            delete_result = self.store.delete_many(
                {"clientId": client_id, "appIds": app_id}
            );
            log.debug(delete_result.acknowledged)
            log.debug(delete_result.deleted_count)
            log.debug(delete_result.raw_result)
            log.debug(type(delete_result.deleted_count))
            if (delete_result.deleted_count == 0):
                log.error("Fail : Deleting all Datastore using query : delete_one - params client_id : ")
                log.error("delete_result.raw_result" + str(delete_result.raw_result))
                return getErrorResponse("FAIL_DELETE_A_DATASTORE", "Deleting a Datastore failed")
            else:
                log.info("Successfully deleted the datastore for client_id : " + client_id)

        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR

        response_json = get_valid_api_response(
            "Successfully deleted " + str(num_of_datastores) + " datastores for client_id: " + client_id)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK
