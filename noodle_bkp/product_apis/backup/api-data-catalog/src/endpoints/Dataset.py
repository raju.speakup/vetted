#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    return_result_list,
    get_url_args,
    validate_get_url_args,
    return_result,
    json_to_dict,
    validateJsonWithSchema,
    getCurrenDateTimeUtc,
    getCurrentDateTimeIst,
    getErrorResponse,
    get_valid_api_response
)
from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)
#from schema.DatastoreSchema import schema_datastore_create, schema_datastore_get
#from swaggerdoc.catalog_store_swagger import DatasetsList
from swaggerdoc.Dataset import CreateDataset
from schema.DatasetSchema import schema_dataset_create, schema_dataset_get_no_table_schema, schema_dataset_get_with_table_schema

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from flask import request
import uuid
from logger.logger import log
from http import HTTPStatus

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"


class Dataset(Resource):

    def __init__(self):
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        log.debug("__init__ : " + connection_string)
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-store ID"
            },
            {
                "name": "parameters",
                "dataType": CreateDataset.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input schema design"
            },
        ],
        notes='Create a new Dataset in a Datastore')
    def post(self, client_id, app_id, datastore_id):
        """
        Create a new Dataset in a Datastore
        """
        payload_str = request.data.decode('utf-8')
        log.info("");log.info("Start of API : Create a Dataset");
        log.debug("Received inputs client_id :{}, app_id :{}, datastore_id :{}".format(client_id, app_id, datastore_id))
        log.debug("Received the following payload \n" + payload_str);

        # check if request payload json format is valid
        log.debug("check if request payload json format is valid")
        payload_dict = json_to_dict(payload_str)
        if 'error' in payload_dict:
            return payload_dict, HTTPStatus.BAD_REQUEST

        # check validation of request payload json with schema
        log.debug("check if payload json is matching with schema named : schema_dataset_create");
        schema_validation_result = validateJsonWithSchema(payload_dict, schema_dataset_create,
                                                          "schema_dataset_create")
        if 'error' in schema_validation_result:
            return schema_validation_result, HTTPStatus.BAD_REQUEST

        # Create the json which needs to be persisted in mongodb
        persistance_data = {}  # The data that needs to persist in the mongo db.
        for index, item in enumerate(payload_dict['datasets']):
            persistance_data.update(item)

        # Adding missing nodes
        persistance_data['datasetId'] = str(uuid.uuid4())
        persistance_data['datasetCreatedAt'] = getCurrenDateTimeUtc()
        persistance_data['datasetUpdatedBy'] = persistance_data['datasetCreatedBy']
        persistance_data['datasetUpdatedAt'] = getCurrentDateTimeIst()
        log.debug(persistance_data);log.debug("persistance_data")

        # check validation of request payload json with schema
        log.debug("check if persistance json is matching with schema named : schema_dataset_get_no_table_schema");
        schema_validation_result = {}
        schema_validation_result = validateJsonWithSchema(persistance_data, schema_dataset_get_no_table_schema,
                                                          "schema_dataset_get_no_table_schema")
        if 'error' in schema_validation_result:
            return schema_validation_result

        # Persist data into mongo DB
        log.info("Trying to persist document to mongo db")
        try:
            query_output = self.store.update_one(
                {"clientId": client_id, "appIds": app_id, "datastoreId": datastore_id},
                {"$addToSet": {"datasets": persistance_data}})
            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_PERSISTANCE_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Successsfully saved the json collection")

        response_json = get_valid_api_response(persistance_data)
        log.debug("Response : ");
        log.debug(response_json)
        return response_json, HTTPStatus.CREATED

    @swagger.operation(
        parameters=[
            {"name": "client_id",    "description": "Client Id",                                        "dataType": 'string', "paramType": "path","required": True,},
            {"name": "app_id",       "description": "App Id",                                           "dataType": 'string', "paramType": "path","required": True,},
            {"name": "datastore_id", "description": "Datastore Id",                                     "dataType": 'string', "paramType": "path","required": True,},
            {"name": "limit",        "description": "Max limit until which the datasets are required",  "dataType": 'int', "paramType": "query"},
            {"name": "offset",       "description": "Offset from which the datasets are required",      "dataType": 'int', "paramType": "query"},
            {"name": "projection",   "description": "Projection types namely: BASIC/FULL",              "dataType": 'string',"paramType": "query"},
            {"name": "sort",         "description": "Sort key",                                         "dataType": 'string',"paramType": "query"},
        ],
        notes='List the data-store items in offset to limit range from Database')

    def get(self, client_id, app_id, datastore_id):
        """
        Get all datasets for a datastore

        """
        log.info("Start of Api - Get all Datasets for a Datastore")
        log.debug("Received inputs client_id :{}, app_id :{}, datastore_id :{}".format(client_id, app_id, datastore_id))
        limit, offset, sort_key, projection = get_url_args(req=request)

        # execute get query
        try:
            if projection == PROJECTION_FULL:
                query_output = self.store.find(
                    {"clientId": client_id, "appIds": app_id, "datastoreId": datastore_id},
                    {'_id': 0,"datasets": []}
                )
            else:
                #Todo : fix to filter only certain fields of the response not all.
                query_output = self.store.find(
                    {"clientId": client_id, "appIds": app_id, "datastoreId": datastore_id},
                    {'_id': 0, "datasets": []}
                    #{'_id': 0, 'datasetname': 1, "datasetdescription": 1,'datasetCreatedBy': 1, 'datasetCreatedAt': 1, 'datasetId': 1 }
                )
            if query_output is None:
                log.error("Fail : Fetching all Datasets using query : find - params client_id, app_id, datastore_id")
                return getErrorResponse("UNABLE_TO_GET_ALL_DATASETS",
                                        "Getting all datasets failed for client with datastore_id : " + datastore_id)
            self.connector.close()

            result = []
            try:
                for item in query_output:
                    for key in item:
                        result = item[key];
            except PyMongoError as err:
                msg = str(err)
            num_of_datasets = len(result)
            log.debug("len")
            log.debug(type(num_of_datasets))
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_READ_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : obtained the json from mongo collection")

        api_response = get_valid_api_response(result)
        api_response['total_count'] = num_of_datasets
        return api_response, HTTPStatus.OK

    def delete(self, client_id, app_id, datastore_id):
        pass