#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    get_url_args,
    getErrorResponse,
    get_valid_api_response,
    json_to_dict,
    validateJsonWithSchema,
    getCurrentDateTimeIst,
    dict_to_json
)
from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)
from constants.catalog_keys import *
from logger.logger import log
# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from http import HTTPStatus
from schema.DatastoreSchema import schema_datastore_update
from swaggerdoc.Datastore import UpdateDataStoreItem

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"


class DatastoreCrud(Resource):

    def __init__(self):
        """Init."""
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "description": "Max limit until which the datasets are required",
                "dataType": 'int',
                "paramType": "query",
                "default": 10},
            {
                "name": "offset",
                "description": "Offset from which the datasets are required",
                "dataType": 'int',
                "paramType": "query"},
            {
                "name": "projection",
                "description": "Projection types namely: BASIC/FULL",
                "dataType": 'string',
                "paramType": "query",
                "default": "FULL"},
            {
                "name": "sort",
                "description": "Sort key",
                "dataType": 'string',
                "paramType": "query"
            },
        ],
        notes='Get a datastore')
    def get(self, client_id, app_id, datastore_id):
        log.info("Start of Api DataStoreGetAll - Get")
        log.debug("Received inputs client_id :{}, app_id :{}, datastore_id: {}".format(client_id, app_id, datastore_id))
        limit, offset, sort_key, projection = get_url_args(req=request)

        # execute get query
        query = '{"clientId": client_id, "datastoreId": datastore_id},{\'_id\': False}'
        log.debug("Executing query : find_one : " + query)
        try:
            if projection == PROJECTION_FULL:
                query_output = self.store.find_one(
                    {"clientId": client_id, "datastoreId": datastore_id},
                    {'_id': False}
                )
            else:
                query_output = self.store.find_one(
                    {"clientId": client_id, "datastoreId": datastore_id},
                    {'_id': 0, 'datastoreName': 1, "datastoreDescription": 1,
                     'datastoreCreatedBy': 1, 'datastoreCreatedAt': 1, 'datastoreId': 1
                     }
                )
            log.debug("query_output")
            log.debug(query_output)
            if query_output is None:
                log.error("Fail : Fetching a Datastore using query : find_one - params client_id,datastore_id : ")
                return getErrorResponse("UNABLE_TO_GET_DATASTORE",
                                        "Getting a Datastore failed for datastore id : " + datastore_id)

            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_READ_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : obtained the json from mongo collection")

        response_json = get_valid_api_response(query_output)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateDataStoreItem.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input schema design"
            },
        ],
        notes='Update DataStore details')
    # TODO : Implement put
    def put(self, client_id, app_id, datastore_id):
        """
        Datastore - Update a datastore
        """
        log.info("Start of Api - Update a Datastore")
        log.info("Received inputs client_id :{}, app_id :{}, datastore_id: {}".format(client_id, app_id, datastore_id))
        payload_str = request.data.decode('utf-8')
        log.info("Received the following payload \n" + payload_str);

        # check if request payload json format is valid
        log.debug("check if request payload json format is valid")
        payload_dict = json_to_dict(payload_str)
        if 'error' in payload_dict:
            return payload_dict, HTTPStatus.BAD_REQUEST

        # check validation of request payload json with schema
        log.debug("check validation of request payload json with schema");
        schema_validation_result = validateJsonWithSchema(payload_dict, schema_datastore_update,
                                                          "schema_datastore_update")
        if 'error' in schema_validation_result:
            return schema_validation_result, HTTPStatus.BAD_REQUEST

        payload_dict['datastoreUpdatedAt'] = getCurrentDateTimeIst()
        # log.debug("payload_dict")
        # log.debug(payload_dict)
        # payload_str = dict_to_json(payload_dict)
        # Persist data into mongo DB
        log.info("Trying to update document to mongo db")
        try:
            query_output = self.store.update_one(
                {"clientId" : client_id, "datastoreId": datastore_id},
                {"$set": payload_dict}
            )
            log.debug(query_output.raw_result)
            log.debug(query_output.nModified)
            # log.debug(query_output.deleted_count)

            self.connector.close()
        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_PERSISTANCE_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR
        else:
            log.info("Success : Saving the json to mongo collection")

    @swagger.operation(
        parameters=[
        ],
        notes='Delete a datastore ')
    def delete(self, client_id, app_id, datastore_id):
        log.info("Start of Api - Delete a datastore")
        log.info("Received inputs client_id :{}, app_id :{}, datastore_id: {}".format(client_id, app_id, datastore_id))

        # Confirm if the datastore exists  and then delete
        try:
            log.debug("Checking if datastore exists")
            find_query_output = self.store.find_one(
                {"clientId": client_id, "datastoreId": datastore_id},
                {'_id': False}
            )

            if find_query_output is None:
                log.debug("Find A Datastore using query : find_one - params clientId,datastore_id. Output : " + str(
                    find_query_output))
                self.connector.close()
                err_msg = "Fail : Unable to find the datastore with datastoreId : " + datastore_id
                log.error(err_msg)
                return getErrorResponse("DATASTORE_NOT_FOUND", err_msg)
            else:
                log.debug("Found the Datastore")

            # Delete the datastore
            log.info("Trying to delete the datastore")
            query_output = self.store.delete_one(
                {"clientId": client_id, "appIds": app_id, "datastoreId": datastore_id}
            );
            log.debug(query_output.acknowledged)
            log.debug(query_output.deleted_count)
            log.debug(query_output.raw_result)
            log.debug(type(query_output.deleted_count))
            if (query_output.deleted_count != 1):
                log.error(
                    "Fail : Deleting a Datastore using query : delete_one - params client_id,app_id. datastore_id : ")
                log.error("delete_result.raw_result" + str(query_output.raw_result))
                return getErrorResponse("FAIL_DELETE_A_DATASTORE", "Deleting a Datastore failed")
            else:
                log.info("Successfully deleted the datastore with id : " + datastore_id)

        except PyMongoError as err:
            error_msg = str(err)
            error_response = getErrorResponse("MONGO_DB_ERROR", error_msg)
            return error_response, HTTPStatus.INTERNAL_SERVER_ERROR

        response_json = get_valid_api_response("Successfully deleted datastore with id : " + datastore_id)
        log.debug("Response : ")
        log.debug(response_json)
        return response_json, HTTPStatus.OK
