from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class DatasetLabels:
    resource_fields = {
        "labels": fields.List,
    }


@swagger.model
class TargetDetails:
    resource_fields = {
        "dbTargetHost": fields.String,
        "dbTargetPort": fields.Integer,
        "dbTargetSchemaName": fields.String,
        "dbTargetName": fields.String,
        "dbTargetUsername": fields.String,
        "dbTargetPassword": fields.String,
    }


@swagger.model
class IngestionDetails:
    resource_fields = {  # if the source is of type database
        "dbSourceType": fields.String,
        "dbSourceHost": fields.String,
        "dbSourcePort": fields.Integer,
        "dbSourceSchemaName": fields.String,
        "dbSourceName": fields.String,
        "dbSourceUsername": fields.String,
        "dbSourcePassword": fields.String,
        # # if the source of type FTP
        # "ftpSourceHost": fields.String,
        # "ftpSourceUsername": fields.String,
        # "ftpSourcePassword": fields.String,
        # "ftpSourcePort": fields.Integer,
        # "ftpSourceDirectory": fields.String,
        # "ftpSourceFileName": fields.String,
        # "ftpSourceFileDelimiter": fields.String,
        # "ftpSourceFileExtension": fields.String,
        # "ftpSourceFileHeader": fields.Integer,
        # "ftpSourceFileTextQualifier": fields.String,
    }

    swagger_metadata = {
        "dbSourceType": {
            "enum": ["SQLServer"]
        }
    }


@swagger.model
class Datasets:
    resource_fields = {
        "datasetId": fields.String,
        "name": fields.String,
        "description": fields.String,
        "status": fields.String,
        "dbSourceTableName": fields.String,
        "dbTargetTableName": fields.String,
        "schemaJSON": fields.Nested({}),
    }

    swagger_metadata = {
        "status": {
            "enum": ['CATALOG', 'INGESTED', 'PROCESSING', 'CONSUMPTION_READY']
        }
    }


@swagger.model
@swagger.nested(
    datasets=Datasets.__name__
)
class DatasetsList:
    resource_fields = {
        'datasets': fields.List(fields.Nested(Datasets.resource_fields))
    }


@swagger.model
@swagger.nested(
    ingestionDetails=IngestionDetails.__name__,
    targetDetails=TargetDetails.__name__,
    datasets=Datasets.__name__
)
class CreateDataStoreItem:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        "clientId": fields.String,
        "appIds": fields.String,
        'ingestionType': fields.String,
        'ingestionDetails': fields.Nested(IngestionDetails.resource_fields),
        'targetDetails': fields.Nested(TargetDetails.resource_fields),
        'datasets': fields.List(fields.Nested(Datasets.resource_fields)),
    }


@swagger.model
@swagger.nested(
    targetDetails=TargetDetails.__name__,
    ingestionDetails=IngestionDetails.__name__
)
class UpdateDataStoreItem:
    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        "clientId": fields.String,
        "appIds": fields.String,
        'dataStoreType': fields.String,
        "dataStoreId": fields.String,
        'ingestionType': fields.String,
        'ingestionDetails': fields.Nested(IngestionDetails.resource_fields),
        'targetDetails': fields.Nested(TargetDetails.resource_fields),
    }
