from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class Datasets:
    resource_fields = {
        "datasetName": fields.String,
        "datasetDescription": fields.String,
        "datasetCreatedBy": fields.String,
        "dbSourceTableName": fields.String,
        "dbTargetTableName": fields.String,
        "schemaJSON": fields.Nested({}),
    }


@swagger.model
@swagger.nested(
    datasets=Datasets.__name__
)
class CreateDataset:
    resource_fields = {
        'datasets': fields.List(fields.Nested(Datasets.resource_fields))
    }


@swagger.model
class UpdateDataset:
    resource_fields = {"datasetName": fields.String,
                       "datasetDescription": fields.String,
                       "dbSourceTableName": fields.String,
                       "dbTargetTableName": fields.String,
                       "schemaJSON": fields.Nested({}),
                       }
