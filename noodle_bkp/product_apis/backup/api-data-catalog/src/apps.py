from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger

from endpoints.Datastore import Datastore
from endpoints.DatastoreCrud import DatastoreCrud
from endpoints.Dataset import Dataset
from endpoints.DatasetCrud import DatasetCrud

import logging

app = Flask(__name__)
logging.getLogger('werkzeug').disabled = True   # comments these to get the Flask console logs
app.logger.disabled = True                      # comments these to get the Flask console logs

api = swagger.docs(
    Api(app), apiVersion='0.1',
    basePath="http://localhost:5000/api/v0/",
    description="docs for data catalog api")

#Create a datastore,    method - Post   Done
#Get All DataStores,    method - Get    Done
#Delete All Datastores, method - Delete Done
api.add_resource(Datastore, '/clients/<string:client_id>/apps/<string:app_id>/datastores')

#Get a datastore,   method - Get    Done
#Update a datastore,method - Put    TBD
#Delete a datastore,method - Delete Done
api.add_resource(DatastoreCrud, '/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>')

#Create a dataset,      method - Post   Done
#Get All datasets,      method - Get    Done
#Delete All datasets    method - Delete
api.add_resource(Dataset, '/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>/datasets')

# Get a dataset     method - Get    Done
# Update a dataset, method - Put    Done
# Delete a dataset, method - Delete TBD
api.add_resource(DatasetCrud, '/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>/datasets/<string:dataset_id>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8804, debug=True)

# api.add_resource(
#     CatalogStoreDatasetsLabels,
#     '/datastores/<string:datastore_id>/datasets/<string:dataset_id>/labels')
#
# api.add_resource(
#     CatalogStoreDatasetsDeleteLabels,
#     '/datastores/<string:datastore_id>/datasets/<string:dataset_id>/labels/<string:label_name>')

# api.add_resource(
#     CatalogStoreGet,
#     '/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>')

# api.add_resource(
#     CatalogStoreGet,
#     '/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>/datasets')

# api.add_resource(
#     CatalogStoreGet,
#     '/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>/datasets/<string:dataset_id>')

# api.add_resource(
#     DataCatalog,
#     '/catalog/')
#
# api.add_resource(
#     DatasetTables,
#     '/catalog/dataset_schema/<string:dataset_id>')

# api.add_resource(DataStoreCreate, '/datastores/clients/<string:client_id>/datastores')
# api.add_resource(DataStoreGetAll, '/datastores/clients/<string:client_id>/datastores/all')
# api.add_resource(CatalogStoreViewByDatasets,'/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>/datasets/')
# api.add_resource(CatalogStoreViewByDatasetsId,'/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>/datasets/<string:dataset_id>')
# api.add_resource(CatalogStoreDatasetStats, '/datastores/<string:datastore_id>/datasets/<string:dataset_id>/stats/')
# api.add_resource(DataStoreGetOne, '/datastores/clients/<string:client_id>/datastores/<string:datastore_id>')
# api.add_resource(CatalogStoreViewById, '/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores/<string:datastore_id>')
# api.add_resource(CatalogStoreGet, '/datastores/clients/<string:client_id>/apps/<string:app_id>/datastores')

# from endpoints.DataStoreCrud import DataStoreCreate, DataStoreGetAll
# from resources.catalog_store import CatalogStore, CatalogStoreGet
# from resources.data_catalog import DataCatalog
# from resources.dataset_tables import DatasetTables
# from resources.catalog_store_viewby_id import CatalogStoreViewById
# from resources.catalog_store_viewby_datasets import CatalogStoreViewByDatasets
# from resources.catalog_store_viewby_datasets_id import CatalogStoreViewByDatasetsId
# from resources.catalog_store_datasets_labels import CatalogStoreDatasetsLabels
# from resources.catalog_store_datasets_delete_labels import CatalogStoreDatasetsDeleteLabels
# from resources.catalog_store_dataset_stats import CatalogStoreDatasetStats
