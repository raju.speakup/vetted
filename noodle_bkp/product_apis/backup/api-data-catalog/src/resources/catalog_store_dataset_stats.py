#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    validate_get_url_args,
    return_result,
    exclude_column_names,
    decode_and_validate)

#from services.db_services import databaseConnect
#
# from config.config import (
#     SERVER_NAME,
#     DB_NAME,
#     USERNAME,
#     PASSWORD,
# )

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.catalog_store import schema

from swaggerdoc.catalog_store_swagger import CreateDataStoreItem

from constants.catalog_keys import *

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from bson.objectid import ObjectId
from flask import request
#import pandas as pd
#import pymssql
import json

class CatalogStoreDatasetStats(Resource):
    """Class for data catalog entries.
    """

    def __init__(self):
        connection_string = "".join(['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {"name": "datastore_id", "description": "Datastore Id which has the table name",
             "dataType": 'string', "paramType": "path"},
            {"name": "dataset_id", "description": "Dataset name on which stats to be run",
             "dataType": 'string', "paramType": "path"},
        ],
        notes='List the data-store items in offset to limit range from Database')
    def get(self, datastore_id, dataset_id):
        """get method to retrieve stats on dataset for a given data-store.

        :arg: None

        :return: result data containing list of data-store items.
        Appropriate errors based on requests and data availability.
        """

        result = []
        table_name = ""
        db_name = ""
        user = ""
        pwd = ""
        dbo = ""
        server = ""

        query_output = self.store.find_one({"_id": ObjectId(datastore_id)})
        self.connector.close()

        try:
            for key, item in query_output.items():
                if key == DATASTORE_TARGET_DETAILS:
                    if "dbTargetHost" in item:
                        server = item['dbTargetHost']
                    if "dbTargetName" in item:
                        db_name = item['dbTargetName']
                    if "dbTargetUsername" in item:
                        user = item['dbTargetUsername']
                    if "dbTargetPassword" in item:
                        pwd = item['dbTargetPassword']
                    if "dbTargetSchemaName" in item:
                        dbo = item['dbTargetSchemaName']
                    else:
                        print("Invalid keys found in Target table details")
                elif key == DATASTORE_DATASETS:
                    for id, val in enumerate(item):
                        print("Getting datasets", val['id'])
                        if val['id'] == dataset_id:
                            table_name = val['dbTargetTableName']

            if table_name:
                try:
                    mssqlConnector = pymssql.connect(server,user,pwd,db_name)

                    table = ''.join(["[", db_name, "].", "[", dbo, "].", "[", table_name, "]"])
                    query = ''.join(["SELECT TOP 1000 * FROM ", table, ";"])
                    df = pd.read_sql(query, mssqlConnector)
                    stats_result = df.describe(include='all').to_json()
                    try:
                        stats_result = json.loads(stats_result)
                    except ValueError as err:
                        msg = "ERROR: Cannot parse the stats output"
                        return return_result(
                            result=result, error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR

                    for key, value in stats_result.items():
                        temp = dict()
                        temp.setdefault("columnName", key)
                        temp.setdefault("stats", value)
                        result.append(temp)

                except pymssql.Error as err:
                    msg = str(err)
                    return return_result(
                        result=result, error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR
            else:
                msg = "ERROR: Table name not found in the dataset item list"
                return return_result(
                    result=result, error=msg, error_code=None), HTTP_400_BAD_REQUEST
        except PyMongoError as err:
            msg = str(err)
            return return_result(
                result=result, error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=result, error=None, error_code=None), HTTP_200_OK