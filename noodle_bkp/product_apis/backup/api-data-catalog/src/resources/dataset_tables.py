#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
# from services.db_services import databaseConnect
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names)

# from config.config import (
#     SERVER_NAME,
#     DB_NAME,
#     USERNAME,
#     PASSWORD,
# )

from constants.constants import (
    TBL_DATA_CATALOG,
    TBL_DATA_CATALOG_JSON,
    TBL_DATA_CATALOG_EXTENDED,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

DATASET_COLUMNS = ["DatasetId", "Name", "SchemaJSON"]
# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json


class DatasetTables(Resource):
    """Class for data catalog entries.
    """

    def __init__(self):
        self.connector = databaseConnect(server_name=SERVER_NAME,
                                         database_name=DB_NAME,
                                         username=USERNAME,
                                         password=PASSWORD)

    @swagger.operation(
        parameters=[
            {"name": "dataset_id", "description": "Dataset Id for which the schema is required",
             "dataType": 'int', "paramType": "path"}
        ],
        notes='get dataset schema details by id')
    def get(self, dataset_id):
        """get method to retrieve data sets from catalog entries.

        :arg: None

        :return:
        """

        result = []

        catalog_id = "".join([TBL_DATA_CATALOG, ".", DATASET_COLUMNS[0]])
        json_id = "".join([TBL_DATA_CATALOG_JSON, ".", DATASET_COLUMNS[0]])

        query = " ".join(["SELECT", catalog_id, ",", DATASET_COLUMNS[1], ",",
                          "JSON_QUERY(", DATASET_COLUMNS[2], ",", "'$')",
                          "FROM",TBL_DATA_CATALOG, "INNER JOIN", TBL_DATA_CATALOG_JSON,
                          "ON",catalog_id,"=", json_id, "WHERE",catalog_id, "=", dataset_id])
        db_result = self.connector.execute(query)

        print(db_result)

        if db_result:
            result = convert_to_json(
                rows=db_result[0],
                column_names=DATASET_COLUMNS
            )
        return return_result_list(
            result=result, error=None, error_code=None)


