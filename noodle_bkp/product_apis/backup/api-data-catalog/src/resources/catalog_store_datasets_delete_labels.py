#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        delete: delete the label for a given datasets [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names,
    decode_and_validate)

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.catalog_store_datasets_labels import schema

from swaggerdoc.catalog_store_swagger import DatasetLabels

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from bson.objectid import ObjectId


class CatalogStoreDatasetsDeleteLabels(Resource):
    """Class to delete the dataset labels.
    """

    def __init__(self):
        connection_string = "".join(['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {"name": "datastore_id", "description": "Datastore Id for which the label needs to be deleted",
             "dataType": 'string', "paramType": "path"},
            {"name": "dataset_id", "description": "Dataset Id for which the label needs to be deleted",
             "dataType": 'string', "paramType": "path"},
            {"name": "label_name", "description": "Label name which needs to be deleted",
             "dataType": 'string', "paramType": "path"},
        ],
        notes='delete data-set labels by id')
    def delete(self, datastore_id, dataset_id, label_name):
        """delete method to delete data sets labels from catalog entries.

        :arg: None

        :return: Send back the updated list of labels.
        """

        result = []

        try:
            query_output = self.store.find_one({"_id": ObjectId(datastore_id)},
                                               {"datasets": {"$elemMatch": {"id": dataset_id}}})
            for key, item in query_output.items():
                if key == 'datasets':
                    for idx, val in enumerate(item):
                        original_list = val['labels']
            if len(original_list):
                updated_labels = [item for item in original_list if item != label_name]
            else:
                msg = "ERROR: cannot delete from empty labels list on the dataset"
                return return_result(result=result, error=msg, error_code=None), HTTP_400_BAD_REQUEST

            query_id = "".join(['datasets.$.', 'labels'])

            self.store.update_one({"_id": ObjectId(datastore_id),
                                   "datasets": {"$elemMatch": {"id": dataset_id}}},
                                  {"$set": {query_id: updated_labels}})
            self.connector.close()

            query_output = self.store.find_one({"_id": ObjectId(datastore_id)},
                                               {"datasets": {"$elemMatch": {"id": dataset_id}}})
            for key, item in query_output.items():
                if key == 'datasets':
                    for idx, val in enumerate(item):
                        result.append(val['labels'])
        except PyMongoError as err:
            msg = str(err)
            return return_result(
                result=result, error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=result, error=None, error_code=None), HTTP_200_OK