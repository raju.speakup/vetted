#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    return_result_list,
    get_url_args,
    validate_get_url_args,
    return_result,
    decode_and_validate)

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.DataStoreCrud import schema_datastore_create

from swaggerdoc.catalog_store_swagger import CreateDataStoreItem

from constants.catalog_keys import *

# from services.db_services import databaseConnect as Dbconnector

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from flask import request
import uuid
import copy
from logger.logger import log
import logging
import os
import sys
print("CatalogStore sys.path")
print(sys.path)
#from ApiUtils import *
#removeNode()

#
# log = logging.getLogger(__name__)
# sys.path.append('D:\repo\mvp\product_apis\\api-common-lib')
# log.info("sys.path : ")
# log.info(sys.path)


# main_path = os.path.dirname(sys.modules['__main__'].__file__)
# log.info("main_path : ")
# log.info(main_path)
#
# project_root = os.path.dirname(os.path.dirname(__file__))
# output_path = os.path.join(project_root, 'api-common-lib')
# log.info("project_root : " + project_root)
# log.info("output_path  : " + output_path)
log.info("sys.path : ")
log.info(sys.path)
# from ApiUtils import *
# removeNode()

default_dataStore = {
    CLIENT_ID: '',
    APP_IDS: '',
    DATASTORE_NAME: '',
    DATASTORE_DESCRIPTION: '',
    DATASTORE_TYPE: '',
    DATASTORE_CREATED_BY : '',
    DATASTORE_CREATED_AT : '',
    DATASTORE_UPDATED_BY : '',
    DATASTORE_UPDATED_AT : '',
    DATASTORE_INGESTION_TYPE: '',
    DATASTORE_INGESTION_DETAILS: {},
    DATASTORE_TARGET_DETAILS: {},
}

default_dataset = {
    DATASET_NAME: '',
    DATASET_DESCRIPTION: '',
    DATASET_STATUS: '',
    DATASET_DB_SOURCE_TABLE: '',
    DATASET_DB_TARGET_TABLE: '',
    DATASET_SCHEMA_JSON: {},
}

class CatalogStore(Resource):
    """Class for data catalog entries.
    """

    def __init__(self):
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateDataStoreItem.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input schema design"
            },
        ],
        notes='Add new item into catalog store with datasets')

    def post(self):
        msg = "Created data store successfully"
        data, code = decode_and_validate(request, schema_datastore_create)

        if code == HTTP_200_OK:
            filled_data = dict(default_dataStore)
            datastore_id = str(uuid.uuid4())
            data.update({'dataStoreId': datastore_id})
            print("filled_data before ");print(filled_data)
            filled_data.update(data);print("filled_data after ")
            print(filled_data)
            data_list = []
            if 'datasets' in data:
                for index, item in enumerate(filled_data['datasets']):
                    temp = dict(default_dataset)
                    temp.update(item)
                    dset_id = str(uuid.uuid4())
                    temp.setdefault("dataSetid", dset_id)
                    data_list.append(temp)
                filled_data['datasets'] = copy.deepcopy(data_list)

            try:
                query_output = self.store.insert_one(filled_data)
                self.connector.close()
            except PyMongoError as err:
                msg = str(err)
                return return_result(
                    result=data,
                    error=msg,
                    error_code=None
                ), HTTP_500_INTERNAL_SERVER_ERROR

            if query_output.acknowledged:
                query_output = self.store.find_one()
                filled_data["_id"] = filled_data.setdefault(
                    "id", str(filled_data["_id"]))
                filled_data.pop("_id", None)
        else:
            return data, code

        return return_result(
            result=filled_data,
            error=msg,
            error_code=None
        ), HTTP_200_OK


class CatalogStoreGet(Resource):
    """Class for data catalog entries."""

    def __init__(self):
        """Init."""
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "description": "Max limit until which the datasets are required",
                "dataType": 'int',
                "paramType": "query"},
            {
                "name": "offset",
                "description": "Offset from which the datasets are required",
                "dataType": 'int',
                "paramType": "query"},
            {
                "name": "projection",
                "description": "Projection types namely: BASIC/FULL",
                "dataType": 'string',
                "paramType": "query"},
            {
                "name": "sort",
                "description": "Sort key",
                "dataType": 'string',
                "paramType": "query"
            },
            {
                "name": "client_id",
                "description": "Client Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
            },
            {
                "name": "app_id",
                "description": "App Id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
            },
        ],
        notes='List the data-store items in offset to limit range from Database')

    def get(self, client_id, app_id):
        log.info("Start of Api Get Datastores")
        result = []
        out = validate_get_url_args(req=request)
        if out != 'OK':
            print("NOT Ok")
            return return_result(
                result=[],
                error=out,
                error_code=None
            ), HTTP_400_BAD_REQUEST

        lim, offset, sort_key, projection = get_url_args(req=request)

        if projection == PROJECTION_BASIC:
            log.info("API_DATASTORE_GET IN PROJECTION_BASIC")
            log.info("API_DATASTORE_GET Client_id : " + client_id)
            log.info("API_DATASTORE_GET Apps_id : " + app_id)
            query_output = self.store.find({"clientId": client_id},{"name": "", "description": "", "dataStoreId": ""}).skip(int(offset)).limit(int(lim))
            log.info(query_output)
        elif projection == PROJECTION_FULL:
            log.info("IN PROJECTION_FULL")
            query_output = self.store.find({"clientId": client_id}).skip(int(offset)).limit(int(lim))
        self.connector.close()

        try:
            for item in query_output:
                log.info("API_DATASTORE_GET : item in query_output")
                log.info('item');log.info(item)
                log.info("result before:"); log.info(result)
                if item["_id"]:
                    item.pop("_id", None)
                result.append(item)
                log.info("result after:");log.info(result)
        except PyMongoError as err:
            msg = str(err)
            return return_result(
                result=[],
                error=msg,
                error_code=None
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result_list(
            result=result,
            error=None,
            error_code=None
        ), HTTP_200_OK
