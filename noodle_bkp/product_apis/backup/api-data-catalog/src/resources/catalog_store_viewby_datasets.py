#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets for a given datastore item[multiple entry]
        post: create a list of dataset(s) for a given datastore item [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    return_result_list,
    get_url_args,
    validate_get_url_args,
    return_result,
    decode_and_validate)

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.catalog_store_viewby_datasets import schema

from swaggerdoc.catalog_store_swagger import DatasetsList

from constants.catalog_keys import *
#from services.db_services import databaseConnect as Dbconnector

CATALOG_BASIC = ['id', 'name', 'description',
                 'status', 'createdOn', 'modifiedOn']

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"
# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from bson.objectid import ObjectId
from itertools import islice
from flask import request
import uuid
#import pymssql
from resources.catalog_store_viewby_id import CatalogStoreViewById

default_dataset = {
    DATASET_NAME: '',
    DATASET_DESCRIPTION: '',
    DATASET_STATUS: '',
    DATASET_DB_SOURCE_TABLE: '',
    DATASET_DB_TARGET_TABLE: '',
    DATASET_SCHEMA_JSON: {},
}


class CatalogStoreViewByDatasets(Resource):
    """Class for data catalog entries by datastore id view.
    """

    def __init__(self):
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-store ID"
            },
            {
                "name": "parameters",
                "dataType": DatasetsList.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input schema design"
            },
        ],
        notes='Add new data-set into catalog store item')
    def post(self, client_id, app_id, datastore_id):
        """post method to add dataset(s) into the catalog store.

        :arg:
            None

        :return:
            json: Returns json object of data-set created else empty error object

        """
        print("IN CatalogStoreViewByDatasets post")
        print("request")
        print(request)
        print("schema")
        print(schema)
        data, code = decode_and_validate(request, schema)
        print("data")
        print(data)
        if code == HTTP_200_OK:
            try:
                if 'datasets' in data:
                    for index, item in enumerate(data['datasets']):
                        filled_data = dict(default_dataset)
                        filled_data.update(item)
                        dset_id = str(uuid.uuid1().hex)
                        filled_data.setdefault("datasetId", dset_id)
                        print("filled_data")
                        print(filled_data)
                        self.store.update_one(
                            {"dataStoreId": datastore_id,
                             "clientId": client_id,
                             "appIds": app_id},
                            {"$addToSet": {"datasets": filled_data}})
                else:
                    msg = "Expecting list with 'datasets' key"
                    return return_result(
                        result=data,
                        error=msg,
                        error_code=None
                    ), HTTP_400_BAD_REQUEST

                #query_output = self.store.find_one({"_id": ObjectId(datastore_id)}, {"datasets": []})
                #query_output = self.store.find_one({"_id": datastore_id}, {"datasets": []})
                query_output = self.store.find({"clientId": client_id})
                query_output = self.store.find({"clientId": client_id,"appIds": app_id, "dataStoreId": datastore_id})
                print("query_output datasets")
                print(query_output)
                for item in query_output:
                    print("item in query_output")
                    print(item)
                    if item.get('datasets', None):
                        print("in datasets")
                        data=item.get('datasets')
                # for key, item in query_output.items():
                #     if key == 'datasets':
                #         self.item = item
                #         data = self.item
            except PyMongoError as err:
                msg = str(err)
                return return_result(
                    result=data,
                    error=msg,
                    error_code=None
                ), HTTP_500_INTERNAL_SERVER_ERROR
            self.connector.close()
        else:
            return data, code

        return return_result(
            result=data, error=None, error_code=None), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-store ID"
            },
            {
                "name": "limit",
                "description": "Max limit until which the datasets are required",
                "dataType": 'int',
                "paramType": "query"
            },
            {
                "name": "offset",
                "description": "Offset from which the datasets are required",
                "dataType": 'int',
                "paramType": "query"
            },
            {
                "name": "projection",
                "description": "Projection types namely: BASIC/FULL",
                "dataType": 'string',
                "paramType": "query"
            },
            {
                "name": "sort",
                "description": "Sort key",
                "dataType": 'string',
                "paramType": "query"
            }
        ],
        notes='List the data-sets in offset to limit range from Database')
    def get(self, client_id, app_id, datastore_id):
        """get method to retrieve dataset list for a catalog store item.

        :arg: None

        :return:
        """
        result = []
        out = validate_get_url_args(req=request)
        get_target = CatalogStoreViewById()
        target_data = get_target.get(client_id, app_id, datastore_id)
        target_details = target_data.get('data', None)
        if out != 'OK':
            return return_result(
                result=[], error=out, error_code=None), HTTP_400_BAD_REQUEST
        limit, offset, sort_key, projection = get_url_args(req=request)

        query_output = self.store.find(
            {"dataStoreId": datastore_id,
             "clientId": client_id,
             "appIds": app_id}, {"datasets": []})
        self.connector.close()

        try:
            for key, value in query_output[0].items():
                if key == 'datasets':
                    for index, val in islice(enumerate(value), int(offset), int(limit)):
                        matadata = {
                            "totalRows": "",
                            "totalData": "",
                            "totalColumns": "",
                        }
                        if target_details:
                            try:
                                connector_sql = Dbconnector(
                                    server_name=target_details[
                                        'targetDetails']['dbTargetHost'],
                                    database_name=target_details[
                                        'targetDetails']['dbTargetName'],
                                    username=target_details[
                                        'targetDetails']['dbTargetUsername'],
                                    password=target_details[
                                        'targetDetails']['dbTargetPassword']
                                )
                                query = "exec sp_spaceused N'{0}'".format(
                                    val['dbTargetTableName'])
                                db_result = connector_sql.execute(query)
                                matadata.update(
                                    {
                                        "totalRows": int(db_result[0][0][1]),
                                        "totalData": float(
                                            "{0:.2f}".format(
                                                float(
                                                    db_result[0][0][2].split(
                                                        " "
                                                    )[0]
                                                ) / 1024
                                            )
                                        )
                                    })

                                query = "SELECT count(COLUMN_NAME) FROM " +\
                                    "INFORMATION_SCHEMA.Columns where " +\
                                    "TABLE_NAME = '{0}'".format(
                                        val['dbTargetTableName'])
                                db_result = connector_sql.execute(query)
                                matadata.update(
                                    {"totalColumns": db_result[0][0][0]})
                            except pymssql.Error as err:
                                print(err)
                        temp = {}
                        for my_key in CATALOG_BASIC:
                            if my_key in val:
                                temp.setdefault(my_key, val[my_key])
                        temp.update({"tableMetaData": matadata})
                        result.append(temp)
        except PyMongoError as err:
            msg = str(err)
            return return_result(
                result=result,
                error=msg,
                error_code=None
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result_list(
            result=result, error=None, error_code=None), HTTP_200_OK
