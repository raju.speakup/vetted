#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        post: add labels to a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names,
    decode_and_validate)

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.catalog_store_datasets_labels import schema

from swaggerdoc.catalog_store_swagger import DatasetLabels

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from bson.objectid import ObjectId
from flask import request
from jsonschema import validate, ValidationError
import json


class CatalogStoreDatasetsLabels(Resource):
    """Class to add labels to datasets.
    """

    def __init__(self):
        connection_string = "".join(['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {"name": "datastore_id", "description": "Datastore Id for which the label needs to be added",
             "dataType": 'string', "paramType": "path"},
            {"name": "dataset_id", "description": "Dataset Id for which the label needs to be added",
             "dataType": 'string', "paramType": "path"},
            {
                "name": "parameters",
                "dataType": DatasetLabels.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input labels as list of strings"
            },
        ],
        notes='Add new data-set labels by id')
    def post(self, datastore_id, dataset_id):
        """post method to add label to a dataset.

        :arg:
            None

        :return:
            json: Returns list of labels that exists for dataset.

        """

        data, code = decode_and_validate(request, schema)

        if code == HTTP_200_OK:
            query_id = "".join(['datasets.$.', 'labels'])

            try:
                query_output = self.store.update_one({"_id": ObjectId(datastore_id),
                                                      "datasets": {"$elemMatch": {"id": dataset_id}}},
                                                     {"$addToSet": {query_id: {"$each": data['labels']}}})

                if query_output.acknowledged:
                    query_output = self.store.find_one({"_id": ObjectId(datastore_id)},
                                                       {"datasets": {"$elemMatch": {"id": dataset_id}}})

                for key, item in query_output.items():
                    if key == 'datasets':
                        for idx, val in enumerate(item):
                            data = val['labels']
            except PyMongoError as err:
                msg = str(err)
                return return_result(
                    result=data, error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR
            self.connector.close()
        else:
            return data, code

        return return_result(
            result=data, error=None, error_code=None), HTTP_200_OK