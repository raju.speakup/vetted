#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names,
    decode_and_validate)

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.catalog_store_viewby_id import schema

from swaggerdoc.catalog_store_swagger import UpdateDataStoreItem

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from bson.objectid import ObjectId
from flask import request


class CatalogStoreViewById(Resource):
    """Class for data catalog entries.
    """

    def __init__(self):
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    # @swagger.operation(
    #     parameters=[
    #         {
    #             "name": "datastore_id",
    #             "dataType": "string",
    #             "paramType": "path",
    #             "required": True,
    #             "description": "Data-store ID"
    #         },
    #         {
    #             "name": "parameters",
    #             "dataType": UpdateDataStoreItem.__name__,
    #             "paramType": "body",
    #             "required": True,
    #             "description": "Input schema design"
    #         },
    #     ],
    #     notes='Update the data-store details by id')
    # def put(self, datastore_id):
    #     """put method to update datastore item details into the catalog store.

    #     :arg:
    #         None

    #     :return:
    #         json: Returns json object of dataset created else empty error object

    #     """

    #     data, code = decode_and_validate(request, schema)

    #     if code == HTTP_200_OK:
    #         try:
    #             if 'datasets' not in data:
    #                 self.store.update_one({"_id": ObjectId(datastore_id)},
    #                                       {"$set": data})
    #             else:
    #                 print(
    #                     "Wrong input, datasets modification not allowed on this route")
    #         except PyMongoError as err:
    #             msg = str(err)
    #             return return_result(
    #                 result=[], error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR
    #         self.connector.close()
    #     else:
    #         return data, code

    #     return return_result(result=data, error=None, error_code=None)

    @swagger.operation(
        parameters=[
            {"name": "datastore_id", "description": "Dataset Id for which the schema is required",
             "dataType": 'int', "paramType": "path"}
        ],
        notes='Query the data store details by id')
    def get(self, client_id, app_id, datastore_id):
        """get method to retrieve data store information by Id.

        :arg: None

        :return:
        """

        result = dict()

        query_output = self.store.find_one(
            {"clientId": client_id,
             "appIds": app_id,
             "dataStoreId": datastore_id}
        )
        self.connector.close()

        try:
            for key, item in query_output.items():
                if key == 'datasets':
                    continue
                elif key == '_id':
                    key = "id"
                    item = str(item)
                result.setdefault(key, item)
        except PyMongoError as err:
            msg = str(err)
            return return_result(
                result=result, error=msg, error_code=None), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=result, error=None, error_code=None)
