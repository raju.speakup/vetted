#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the dataset details given the datastore id and the dataset id[multiple entry]
        put: update the dataset details given the datastore id and the dataset id[single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from bson.objectid import ObjectId
from flask import request

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    validate_get_url_args,
    return_result,
    decode_and_validate)

from config.config import (
    MONGO_SERVER,
    MONGO_DB_NAME,
    MONGO_DB_COLLECTION,
    MONGO_USER,
    MONGO_PASSWD)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from schema.catalog_store_viewby_datasets_id import schema

from swaggerdoc.catalog_store_swagger import Datasets

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"


class CatalogStoreViewByDatasetsId(Resource):
    """Class for data catalog entries by datastore and dataset id view."""

    def __init__(self):
        """__init__ method."""
        connection_string = "".join(
            ['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        self.connector = MongoClient(
            connection_string, serverSelectionTimeoutMS=3000)
        self.coll = self.connector[MONGO_DB_NAME]
        self.store = self.coll[MONGO_DB_COLLECTION]

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-store ID"
            },
            {
                "name": "dataset_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-set ID"
            },
            {
                "name": "parameters",
                "dataType": Datasets.__name__,
                "paramType": "body",
                "required": True,
                "description": "Input schema design"
            },
        ],
        notes='Update the data-set into catalog store item')
    def put(self, client_id, app_id, datastore_id, dataset_id):
        """Put method to update dataset details into the database.

        :arg:
            None

        :return:
            json: Returns json object of data-set updated else empty
            error object

        """
        result = []
        data, code = decode_and_validate(request, schema)

        if code == HTTP_200_OK:
            updated_keydict = {}
            query_id = "datasets.$."
            for key, value in data.items():
                new_key = "".join([query_id, key])
                updated_keydict.setdefault(new_key, value)

            try:
                self.store.update_one(
                    {"_id": ObjectId(datastore_id), "datasets": {
                        "$elemMatch": {"id": dataset_id}}},
                    {"$set": updated_keydict})

                query_output = self.store.find_one(
                    {
                        "dataStoreId": datastore_id,
                        "clientId": client_id,
                        "appIds": app_id,
                        "datasets": {
                            "$elemMatch": {
                                "id": dataset_id
                            }
                        }
                    }
                )
                for key, item in query_output.items():
                    if key == 'datasets':
                        for entry in item:
                            if entry['id'] == dataset_id:
                                result.append(entry)

                self.connector.close()
            except PyMongoError as err:
                msg = str(err)
                return return_result(
                    result=data,
                    error=msg,
                    error_code=None), HTTP_500_INTERNAL_SERVER_ERROR
        else:
            return data, code

        return return_result(
            result=result, error=None, error_code=None), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-store ID"
            },
            {
                "name": "dataset_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Data-set ID"
            },
        ],
        notes='Query the data-set from catalog store item')
    def get(self, client_id, app_id, datastore_id, dataset_id):
        """Get method to retrieve data sets from catalog entries.

        :arg: None

        :return:
        """
        print("in get catalog_store_viewby_datasets_id")
        result = dict()
        out = validate_get_url_args(req=request)
        if out != 'OK':
            return return_result(
                result=[], error=out, error_code=None), HTTP_400_BAD_REQUEST

        try:
            query_output = self.store.find(
                {
                    "dataStoreId": datastore_id,
                    "clientId": client_id,
                    "appIds": app_id,
                    "datasets": {
                        "$elemMatch": {
                            "datasetId": dataset_id
                        }
                    }
                })
            self.connector.close()
            print("query_output")
            print(query_output)

            for item in query_output:
                print("item in query_output")
                print(item)
                if item.get('datasets', None):
                    print("in datasets")
                    result1 = item.get('datasets')
                    result = item.get('datasets')[0]
                    print("result1")
                    print(result1)
                    print("result")
                    print(result)
            # for key, item in query_output.items():
            #     if key == 'datasets':
            #         result = item
        except PyMongoError as err:
            msg = str(err)
            return return_result(
                result=result,
                error=msg,
                error_code=None), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=result,
            error=None,
            error_code=None), HTTP_200_OK
