#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data Catalog API
    REST endpoints:
        get: get the list of datasets [multiple entry]
        post: create a dataset [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
#from services.db_services import databaseConnect
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names)
#
# from config.config import (
#     SERVER_NAME,
#     DB_NAME,
#     USERNAME,
#     PASSWORD,
# )

from constants.constants import (
    TBL_DATA_CATALOG,
    TBL_DATA_CATALOG_JSON,
    TBL_DATA_CATALOG_EXTENDED,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

CATALOG_ELEMENTS = ['Name', 'Description', 'Type', 'Status', 'IsPaid', 'LicensingType',
                    'WebsiteURL', 'Owner', 'ModifiedOn', 'ModifiedBy', 'CreatedOn',
                    'CreatedBy', 'EstimatedRecord', 'EstimatedSizeMB']
CATALOG_BASIC = ['Name', 'Description', 'Type', 'Status', 'IsPaid', 'LicensingType', 'Owner']

DATASET_COL = "DatasetId"

CATALOG_JSON = ['SchemaJSON', 'SampleRecordJSON']
CATALOG_EXTENDED = [DATASET_COL, '[Key]', '[Value]']
DATASET_TAG = "tags"

PROJECTION_BASIC = "BASIC"
PROJECTION_FULL = "FULL"

INGESTION = "Ingestion_details"

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
from operator import itemgetter
import json


class DataCatalog(Resource):
    """Class for data catalog entries.
    """

    def __init__(self):
        self.connector = databaseConnect(server_name=SERVER_NAME,
                                         database_name=DB_NAME,
                                         username=USERNAME,
                                         password=PASSWORD)

    @swagger.operation(
        parameters=[

            {"name": "Name", "description": "Name of the dataset to be added",
             "dataType": "string", "paramType": "string"},
            {"name": "Description", "description": "Short description of the data source",
             "dataType": "string", "paramType": "string"},
            {"name": "Type", "description": "Type of the data source namely: internal/external/public",
             "dataType": "string", "paramType": "string"},
            {"name": "Status", "description": "Status of the dataset namely: cataloged/ingested/ready_to_use",
             "dataType": "string", "paramType": "string"},
            {"name": "isPaid", "description": "Classify the data to free or paid",
             "dataType": "boolean", "paramType": "boolean"},
            {"name": "LicensingType", "description": "Licensing information",
             "dataType": "string", "paramType": "string"},
            {"name": "WebsiteURL", "description": "URL of the data source",
             "dataType": "string", "paramType": "string"},
            {"name": "Owner", "description": "Owner of the dataset, i.e., Noodle_Admin",
             "dataType": "string", "paramType": "string"},
            {"name": "ModifiedOn", "description": "Date on which the dataset was last updated",
             "dataType": "string", "paramType": "string"},
            {"name": "Modifiedby", "description": "User who updated the dataset, i.e., Noodle_Admin",
             "dataType": "string", "paramType": "string"},
            {"name": "CreatedOn", "description": "Date on which the dataset was created",
             "dataType": "string", "paramType": "string"},
            {"name": "CreatedBy", "description": "User who created the dataset, i.e., Noodle_Admin",
             "dataType": "string", "paramType": "string"},
            {"name": "EstimatedRecord", "description": "Number of records of the dataset",
             "dataType": "int", "paramType": "int"},
            {"name": "EstimatedSizeMB", "description": "Size of the dataset in MBs",
             "dataType": "int", "paramType": "int"},
            {"name": "ingestion_details", "description": "Ingestion details required for data fetch",
             "dataType": "dict", "paramType": "json"},
            {"name": "tags", "description": "List of Tags created on or assigned to dataset",
             "dataType": "list", "paramType": "list"},
            {"name": "SchemaJSON", "description": "Schema of the actual data table for the dataset",
             "dataType": "dict", "paramType": "json"},
            {"name": "SampleRecordJSON", "description": "Sample of the actual data table for the dataset",
             "dataType": "dict", "paramType": "json"},
        ],
        notes='Add new dataset into catalog')
    def post(self):
        """post method to add data set into the catalog table.

        :arg:
            None

        :return:
            json: Returns json object of dataset created else empty error object

        """

        data = json.loads(request.data.decode('utf-8'))

        catalog_data = ""

        catalog_columns = ""

        catalog_json_columns = DATASET_COL

        ingestion_columns = ", ".join(CATALOG_EXTENDED)
        ingestion_data = ""

        for request_key, request_data in data.items():
            if request_key in CATALOG_ELEMENTS:
                if catalog_columns:
                    catalog_columns = ", ".join([catalog_columns, request_key])
                else:
                    catalog_columns = request_key
                if catalog_data:
                    catalog_data = "".join([catalog_data, ", " "'", str(request_data), "'"])
                else:
                    catalog_data = "".join(["'", str(request_data), "'"])

        query = "".join([" INSERT INTO ", TBL_DATA_CATALOG, " ( ", catalog_columns, " ) ",
                         "OUTPUT INSERTED.", DATASET_COL,
                         " VALUES", " ( ", catalog_data, " ) ", " ;"])

        db_result = self.connector.execute(query)

        if len(db_result) > 0:
            try:
                dataset_id = str(db_result[0][0][0])
            except IndexError:
                print("Dataset ID cannot be retrieved from catalog table")
                # TODO DELETE the catalog entry and return failure or exit as is.
            catalog_json_data = dataset_id

            for request_key, request_data in data.items():

                if request_key in CATALOG_JSON:
                    catalog_json_columns = ", ".join([catalog_json_columns, request_key])
                    catalog_json_data = "".join([catalog_json_data, ", ", "'", json.dumps(request_data), "'"])

                elif request_key == INGESTION:
                    for ingest_key, ingest_value in request_data.items():
                        if ingest_value is not None:
                            if type(ingest_value) != int:
                                ingest_value = "".join(["'", str(ingest_value), "'"])
                            prepared_data = "".join([" ( ", dataset_id, ", ", "'", ingest_key, "'", ", ",
                                                     ingest_value, " ) ", ])
                            if ingestion_data:
                                ingestion_data = ", ".join([ingestion_data, prepared_data])
                            else:
                                ingestion_data = prepared_data

                elif request_key == DATASET_TAG:
                    for item in request_data:
                        prepared_data = "".join([" ( ", dataset_id, ", ", "'", item, "'", ", ",
                                                 "'", str(True), "'", " ) ", ])
                        if ingestion_data:
                            ingestion_data = ", ".join([ingestion_data, prepared_data])
                        else:
                            ingestion_data = prepared_data
                elif request_key not in CATALOG_ELEMENTS:
                    print("Wrong input, please check the catalog POST entry")

            # TODO handle transaction
            if catalog_json_data:
                query = "".join([" INSERT INTO ", TBL_DATA_CATALOG_JSON, " ( ", catalog_json_columns, " ) ",
                                 "VALUES", " ( ", catalog_json_data, " ) ", " ;"])
                self.connector.execute_no_return(query)

            if ingestion_data:
                query = "".join([" INSERT INTO ", TBL_DATA_CATALOG_EXTENDED, " ( ", ingestion_columns, " ) ",
                                 "VALUES ", ingestion_data, " ;"])
            self.connector.execute_no_return(query)

            # return the Dataset Id
            data.setdefault(DATASET_COL, dataset_id)

        return return_result(
            result=data, error=None, error_code=None)

    @swagger.operation(
        parameters=[
            {"name": "limit", "description": "Max limit until which the datasets are required",
             "dataType": 'int', "paramType": "query"},
            {"name": "offset", "description": "Offset from which the datasets are required",
             "dataType": 'int', "paramType": "query"},
            {"name": "projection", "description": "Projection types namely: BASIC/FULL",
             "dataType": 'string', "paramType": "query"},
            {"name": "sort", "description": "Sort key",
             "dataType": 'string', "paramType": "query"}],
        notes='List the data-sets in offset to limit range from Database')
    def get(self):
        """get method to retrieve data sets from catalog entries.

        :arg: None

        :return:
        """

        result = []

        limit, offset, sort_key, projection = get_url_args(req=request)

        max_entry = int(offset) + int(limit)

        if projection == PROJECTION_BASIC:
            print("projection identified")
            catalog_columns = ", ".join(CATALOG_BASIC)
            query =  "".join(["SELECT ", DATASET_COL, " ,", catalog_columns, " FROM ",
                              "( SELECT ROW_NUMBER() OVER (ORDER BY ", TBL_DATA_CATALOG, ".", DATASET_COL,
                              " ASC) AS rownumber, ", TBL_DATA_CATALOG, ".", DATASET_COL, " ,", catalog_columns,
                              " FROM ", TBL_DATA_CATALOG, ") AS tab WHERE rownumber",
                              " BETWEEN ", str(offset), " AND ", str(max_entry) ])
            print(query)

            db_result = self.connector.execute(query)
            print( db_result)

            catalog_columns = [DATASET_COL] + CATALOG_BASIC
            if db_result:
                result = convert_to_json(
                    rows=db_result[0],
                    column_names=catalog_columns
                )

        elif projection == PROJECTION_FULL:
            catalog_elements = ", ".join(CATALOG_ELEMENTS)

            schema_json = " ".join(["JSON_QUERY(", CATALOG_JSON[0], ", '$') AS ", CATALOG_JSON[0] ])
            records_json = " ".join(["JSON_QUERY(", CATALOG_JSON[1], ", '$') AS ", CATALOG_JSON[1] ])

            catalog_columns = ", ".join([catalog_elements, schema_json, records_json])

            query = "".join(["SELECT ", DATASET_COL, " ,", catalog_elements, " ,", CATALOG_JSON[0],
                             ", ",CATALOG_JSON[1]," FROM ",
                              "( SELECT ROW_NUMBER() OVER (ORDER BY ", TBL_DATA_CATALOG, ".", DATASET_COL,
                              " ASC) AS rownumber, ", TBL_DATA_CATALOG, ".", DATASET_COL, " ,", catalog_columns,
                              " FROM ", TBL_DATA_CATALOG, " INNER JOIN ", TBL_DATA_CATALOG_JSON, " ON ",
                             TBL_DATA_CATALOG, ".", DATASET_COL, " = ", TBL_DATA_CATALOG_JSON, ".", DATASET_COL,
                             ") AS tab WHERE rownumber",
                             " BETWEEN ", str(offset), " AND ", str(max_entry)])
            db_result_1 = self.connector.execute(query)

            column_names = [DATASET_COL] + CATALOG_ELEMENTS + CATALOG_JSON
            if db_result_1:
                result = convert_to_json(
                    rows=db_result_1[0],
                    column_names=column_names
                )

                extended_columns = ", ".join(CATALOG_EXTENDED)
                query = "".join(["SELECT ", extended_columns, " FROM ", TBL_DATA_CATALOG_EXTENDED,
                                 " WHERE ", TBL_DATA_CATALOG_EXTENDED, ".", DATASET_COL, " BETWEEN ",
                                 str(offset), " AND ", str(max_entry)])
                db_result_2 = self.connector.execute(query)

                for item in result:
                    if db_result_2[0]:
                        for dataset_id, key, value in db_result_2[0]:
                            if dataset_id == item[DATASET_COL]:
                                if key.startswith("tag_"):
                                    item.setdefault(DATASET_TAG, [])
                                    item[DATASET_TAG].append(key)
                                else:
                                    item.setdefault(INGESTION, {})
                                    item[INGESTION].setdefault(key, value)
                    else:
                        item.setdefault(INGESTION, {})
                        item.setdefault(DATASET_TAG, [])

        for item in sort_key:
            if item in CATALOG_ELEMENTS:
                sorted_result = sorted(result, key=itemgetter(*sort_key))
            else:
                sorted_result = result

        return return_result_list(
            result=sorted_result, error=None, error_code=None)