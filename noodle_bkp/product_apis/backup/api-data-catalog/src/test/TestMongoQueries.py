import unittest
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from logger.logger import *

class TestMongoQueries(unittest.TestCase):

    def test_sample(self):
        MONGO_SERVER='localhost:27017'
        MONGO_DB_NAME='NoodleApp'
        MONGO_DB_COLLECTION='catalog_store'
        MONGO_USER = 'admin'
        MONGO_PASSWD ='admin'
        lim = '100'
        offset = '0'

        connection_string = "".join(['mongodb://', MONGO_USER, ':', MONGO_PASSWD, '@', MONGO_SERVER])
        print("connection_string : " + connection_string )
        connector = MongoClient(connection_string, serverSelectionTimeoutMS=3000)
        coll = connector[MONGO_DB_NAME]
        store = coll[MONGO_DB_COLLECTION]

        client_id = 'dffab3837546442c89d082686a1ffcz1'
        app_id = 'dffab3837546442c89d082686a1ffcz2'
        #query_output = store.find({"clientId": client_id}, {"name": "", "description": ""}).skip(int(offset)).limit(int(lim))
        query_output = store.find({"clientId": client_id, "appIds" : app_id})
        #query_output = store.find({"clientId": client_id}, {"name": "", "description": ""})
        #query_output = store.find({"clientId": client_id}, {"name": "", "description": ""}).skip(int(offset)).limit(int(lim))
        #query_output = store.find({"clientId": client_id,"appIds": app_id}, {"name": "", "description": ""}).skip(int(offset)).limit(int(lim))
        #query_output = store.find({"clientId": client_id}).skip(int(offset)).limit(int(lim))
        #query_output = store.find_one({"clientId": 'dffab3837546442c89d082686a1ffca0',"appIds": 'dffab3837546442c89d082686a1ffcda1', "dataStoreId": '5ae4be7afeed4403b8454b20'})
        #query_output = store.find_one({"clientId": 'dffab3837546442c89d082686a1ffca0', "appIds": 'dffab3837546442c89d082686a1ffcda1',"dataStoreId": '48c01ce24b9f11e887c7d481'})
        #query_output = store.find_one({"_id": '48c01ce24b9f11e887c7d481'}, {"datasets": []})
        # query_output = store.find(
        #     {
        #         "clientId": 'dffab3837546442c89d082686a1ffca0',
        #         "appIds": 'dffab3837546442c89d082686a1ffcda1',
        #         "dataStoreId": '48c01ce24b9f11e887c7d481',
        #         "datasets": {
        #             "$elemMatch": {
        #                 "datasetId": '6a27898a4bc411e8b202d481d7e3df06'
        #             }
        #         }
        #     })
        #
        result = []
        print("TYPE QUERY OUTPUT")
        print(type(query_output))
        for item in query_output:
            print("item in query_output")
            print(item)
            result.append(item)
        print("Final output")
        print(result)
        # for item in query_output:
        #     print("item in query_output")
        #     print(item)
        #     if item.get('datasets', None):
        #         print("in datasets")
        #         result = item.get('datasets')[0]
        #         print("result")
        #         print(result)
        print("Testing A")
        log.info("LOGGER INFO MESSAGE")

if __name__ == '__main__':
    unittest.main()