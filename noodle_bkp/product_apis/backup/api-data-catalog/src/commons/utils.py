import json
from jsonschema import validate, ValidationError, SchemaError
from logger.logger import log
import pprint
import datetime

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

from http import HTTPStatus


def convert_to_json(rows, column_names):
    """convert_to_json.

    Converts all the rows in json using column names.
    """
    result = []
    for i in rows:
        result.append(dict(zip(column_names, i)))
    json_string = json.dumps(result, sort_keys=True, indent=4)
    return json.loads(json_string)


def return_result(result=[], error=None, error_code=None):
    return_result = {}

    return_result.update(
        {
            "data": result,
            "error": {
                "code": error_code,
                "message": error
            }
        }
    )

    return_result = json.dumps(return_result)
    return json.loads(return_result)


def return_result_list(result=[], error=None, error_code=None):
    return_result_list = {}

    return_result_list.update(
        {
            "data": result,
            "total_count": len(result),
            "error": {
                "code": error_code,
                "message": error
            }
        }
    )

    return_result_list = json.dumps(return_result_list)
    return json.loads(return_result_list)


def return_result_delete(result={}, error=None, error_code=None):
    return_result_list = {}

    return_result_list.update(
        {
            "data": result,
            "records_effected": 1,
            "error": {
                "code": error_code,
                "message": error
            }
        }
    )

    return_result_list = json.dumps(return_result_list)
    return json.loads(return_result_list)


def get_url_args(req):
    """
    """
    limit = req.args.get("limit", 10)
    offset = req.args.get("offset", 0)
    sort_key = req.args.get("sort", "id")
    projection = req.args.get("projection", "BASIC")

    sort_key = sort_key.split(",") if ',' in sort_key else [sort_key]
    return limit, offset, sort_key, projection


def validate_get_url_args(req):
    par_list = ['limit', 'offset', 'sort', 'projection']
    if len(set(req.args.keys()).difference(par_list)):
        return 'ERROR: Unrecognized query parameter'

    if req.args.get("limit", None):
        if type(req.args.get("limit", None)) != str:
            return "ERROR: Bad input for 'limit'"
        try:
            int(req.args.get("limit", None))
        except ValueError:
            return "ERROR: Bad input for 'limit'"

    if req.args.get("offset", None):
        if type(req.args.get("offset", None)) != str:
            return "ERROR: Bad input for 'offset'"
        try:
            int(req.args.get("offset", None))
        except ValueError:
            return "ERROR: Bad input for 'offset'"

    if req.args.get("sort", None):
        if type(req.args.get("sort", None)) != str:
            return "ERROR: Bad input for 'sort'"

    if req.args.get("projection", None):
        if type(req.args.get("projection", None)) != str:
            return "ERROR: Bad input for 'projection'"
        if req.args.get("projection", None) not in ['BASIC', 'FULL']:
            return "ERROR: Bad input for 'projection', supported values: 'BASIC' and 'FULL'"

    return 'OK'


def exclude_column_names(column_names=[], exclude_list=[]):
    for col in exclude_list:
        if col:
            column_names.remove(col)
    return column_names


def exclude_entry_dict(passing_dict={}, exclude_list=[]):
    for col in exclude_list:
        if col:
            del passing_dict[col]
    return passing_dict


def decode_and_validate(request, schema={}):
    try:
        data = json.loads(request.data.decode('utf-8'))
    except ValueError as error:
        msg = error.args[0]
        return return_result(
            result=[], error=msg, error_code=None), HTTP_400_BAD_REQUEST

    try:
        validate(data, schema)
    except ValidationError as err:
        msg = err.message
        reason = [item for item in str(err).split('\n') if item.startswith('On instance')]
        if len(reason):
            msg = " ".join([msg, reason[0]])
        return return_result(
            result=[], error=msg, error_code=None), HTTP_400_BAD_REQUEST
    return data, HTTP_200_OK


def getErrorResponse(error_code, error_msg):
    error = {}
    error.update(
        {
            "error": {
                "error_code": error_code,
                "error_message": error_msg
            }
        }
    )
    # TODO : Catch Exceptions for json.dumps
    error_response_json_str = json.dumps(error)
    # TODO : Catch Exceptions for json.loads
    return json.loads(error_response_json_str)


def json_to_dict(json_str):
    try:
        json_dict = json.loads(json_str)
    except ValueError as error:
        error_response = getErrorResponse("INVALID_JSON", error.args[0])
        log.error("Unable to load the json using json.loads. Following is the error response")
        log.error(error_response)
        log.error("Following is the json \n." + json_str)
        return error_response
    else:
        log.debug("The json is in valid format")
        return json_dict


def validateJsonWithSchema(payload, json_schema, json_schema_name):
    try:
        validate(payload, json_schema)
    except ValidationError as err:
        error_msg = err.message
        reason = [item for item in str(err).split('\n') if item.startswith('On instance')]
        if len(reason):
            error_msg = " ".join([error_msg, reason[0]])
        error_response = getErrorResponse("JSON_FAILS_SCHEMA_VALIDATION", error_msg)
        log.error("for schema named " + json_schema_name + " validation failed for the json Payload")
        log.error(error_response)
        return error_response
    result_json = json.dumps({"info": "JSON is valid for the SCHEMA " + json_schema_name})
    return json.loads(result_json)


def getCurrenDateTimeUtc():
    currentDateTime = str(datetime.datetime.utcnow())
    return currentDateTime


def getCurrentDateTimeIst():
    currentDateTime = str(datetime.datetime.now())
    return currentDateTime


def dict_to_json(json_obj):
    json_string = json.dumps(json_obj, sort_keys=True, indent=4)
    return json.loads(json_string)


def get_valid_api_response(response):
    return_result = {}
    return_result.update(
        {
            "data": response,
        }
    )
    return_result = json.dumps(return_result)
    return json.loads(return_result)


def return_result(result=[], error=None, error_code=None):
    return_result = {}

    return_result.update(
        {
            "data": result,
            "error": {
                "code": error_code,
                "message": error
            }
        }
    )
    return_result = json.dumps(return_result)
    return json.loads(return_result)
