# README #

* This project runs Django REST framework backend and React.js UI.

### What is this repository for? ###

* The Noodle Application is part of MVP development and version 1.0 implements the "data catalog" section.
* Confluence: https://noodleai.atlassian.net/wiki/spaces/SE/pages/36044818/Data+Catalog+product+requirements

### How do I get set up? ###

** Backend environment setup**

This section covers the environment setup needed for development.

The backend project runs on python3 (3.5 onwards) and following setup starts 

from installing python3 setup to setting up virtual environment. 

You may ignore this section, if completed.

***Python3, pip3 and virtualenv installation***

* Mac users

    `brew install python3`

    `brew install pip3`
    
    `pip3 install virtualenv`
    
    `virtualenv path-to-env/env_name`
    
    `source path-to-env/bin/activate`

* Windows Users

    *Download and install the .exe from official Python Website*
    
    *The installation consists of pip3 for python3.5 onwards*

* Linux Users (debian)

    `sudo apt-get update`

    `sudo apt-get install python3.6`

    `sudo apt-get -y install python3-pip`
    
    `virtualenv path-to-env/env_name`
    
    `source path-to-env/bin/activate`


**TODO**
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests: Python unit test framework
* Code review : SE team and DE team for the database aspects
* Other guidelines : tbd

### Who do I talk to? ###

* SE team is the primary contact