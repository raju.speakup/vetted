#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS
import json

from functools import wraps
from constants.custom_field_error import HTTP_400_BAD_REQUEST,HTTP_200_OK,HTTP_401_UNAUTHORIZED
from jsonschema import validate, ValidationError
from flask import (
    jsonify,
    request,
)

from resources.health import Health

from resources.clientdetailsid import Client
from resources.clientdetailslist import ClientList

# from resources.userdetailsid import User
# from resources.userdetailslist import UserList

#from resources.userroleid import Role
#from resources.userrolelist import RoleList

from resources.clientassociationid import ClientAssociation
from resources.clientassociationlist import ClientAssociationList

from resources.clientuserslist import ClientUsersList
from resources.clientuserid import ClientUser

from resources.clientappslist import ClientAppsList
from resources.clientappsid import ClientApp

from resources.appdetailsid import App
from resources.appdetailslist import AppsList

from resources.appusersid import AppUser
from resources.appuserslist import AppUsersList

from resources.userroleid import UserRole
from resources.userroleslist import UserRolesList

from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG

from resources.userid import (
    UserLogin,
    CreateUser,
    DeleteUser,
    SecretKey,
    Refresh)

from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

from commons.mongo_services import NoodleMongoClient
from configuration.layer_fetch_environ import SECRETE_KEY

app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="MVP Authorization Service",
    resourcePath="/Auth"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)
jwt._set_error_handler_callbacks(app)

blacklist = set()


class Decorator:
    """Logout."""

    @jwt_required
    def validate_user(roles):
        """Validate json with generated schema."""

        def decorator(f):
            @wraps(f)
            def wrapper(*args, **kw):
                try:
                    user_claims = get_jwt_claims()
                    user_role = user_claims['userRole']
                    user_permission = user_claims['permission']
                    if user_role in roles:
                        if not request.method != 'GET' and user_permission in ['read', 'read/write']:
                            return jsonify({"msg": "User does not have required permission"}), HTTP_401_UNAUTHORIZED
                        else:
                            return jsonify({'msg': 'user is validated'}), HTTP_200_OK
                    else:
                        return jsonify({"msg": "User does not have required roles"}), HTTP_401_UNAUTHORIZED

                except ValidationError as err:
                    response = jsonresponse(err)
                    return response, HTTP_400_BAD_REQUEST
                return f(*args, **kw)

            return wrapper

        return decorator

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """check_if_token_in_blacklist."""
    jti = decrypted_token['jti']
    return jti in blacklist


@jwt.user_claims_loader
def add_claims_to_access_token(user_id):
    """add_claims_to_access_token.

    Connect to mongo authorization service
    and get permission information of user
    """
    connection = NoodleMongoClient()
    return_object = connection.find_one(
        collectionname="appUsers",
        data={

            "users": {
                "$elemMatch": {
                    "userId": user_id,
                    "isActive": True
                }
            }
        })
    if return_object:

        return {'userId': user_id, 'userRole': return_object['users'][0]['userRole'],
                'permission': return_object['users'][0]['permission']}


class LogOutAccess(Resource):
    """Logout."""

    @jwt_required
    def delete(self):
        """Delete token."""
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return jsonify({"msg": "Successfully Removed Access Token"})


class LogOutRefresh(Resource):
    """Logout."""

    @jwt_refresh_token_required
    def delete(self):
        """delete."""
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return jsonify({"msg": "Successfully Removed Refresh Token"})


# ------------------------------------------------------------------------------
# Health services
# ------------------------------------------------------------------------------

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

# ------------------------------------------------------------------------------
# Authentication services
# ------------------------------------------------------------------------------

# http://server/api/v1/login
api.add_resource(UserLogin, '/api/v1/login')

# http://server/api/v1/refresh
api.add_resource(Refresh, '/api/v1/refresh')

# http://server/api/v1/logoutaccess
api.add_resource(LogOutAccess, '/api/v1/logoutaccess')

# http://server/api/v1/logoutrefresh
api.add_resource(LogOutRefresh, '/api/v1/logoutrefresh')

# http://server/api/v1/createuser
api.add_resource(CreateUser, '/api/v1/createuser')

# http://server/api/v1/deleteuser/user/{user_id}
api.add_resource(DeleteUser, '/api/v1/deleteuser/user/<string:user_id>')

# http://server/api/v1/secretekey
api.add_resource(SecretKey, '/api/v1/secretekey')


# ------------------------------------------------------------------------------
# Authorization services
# ------------------------------------------------------------------------------

# # http://server/api/v1/clients
api.add_resource(
    ClientList,
    '/api/v1/clients')

# http://server/api/v1/clients/clients/{client_id}
api.add_resource(
    Client,
    '/api/v1/clients/clients/<string:client_id>')


# http://server/api/v1/metadata/association
api.add_resource(
    ClientAssociationList,
    '/api/v1/metadata/association/clients')

# http://server/api/v1/metadata/association/{client_id}
api.add_resource(
    ClientAssociation,
    '/api/v1/metadata/association/<string:client_id>')


# http://server/api/v1/{client_id}/users
api.add_resource(
    ClientUsersList,
    '/api/v1/client/<string:client_id>/users')

# http://server/api/v1/client/{client_id}
api.add_resource(
    ClientUser,
    '/api/v1/client/<string:client_id>/<string:user_id>')

# http://server/api/v1/{client_id}/users
api.add_resource(
    ClientAppsList,
    '/api/v1/client/<string:client_id>/apps')

# http://server/api/v1/client/{client_id}/app/{app_id}
api.add_resource(
    ClientApp,
    '/api/v1/client/<string:client_id>/app/<string:app_id>')

# # # http://server/api/v1/users
# # api.add_resource(
# #     UserList,
# #     '/api/v1/users')

# # # http://server/api/v1/clients/users/{user_id}
# # api.add_resource(
# #     User,
# #     '/api/v1/clients/users/<string:user_id>')

#  http://server/api/v1/clients/{client_id}/apps/{app_id}/users/{user_id}
api.add_resource(
    AppUser, '/api/v1/clients/<string:client_id>/apps/<string:app_id>/users/<string:user_id>')

#  http://server/api/v1/clients/{client_id}/apps/{app_id}/users
api.add_resource(
    AppUsersList, '/api/v1/clients/<string:client_id>/apps/<string:app_id>/users')

#  http://server/api/v1/user/{user_id}/roles/{role_id}
#api.add_resource(
#    UserRole,
#     '/api/v1/user/<string:user_id>/roles/<string:role_id>')

#  http://server/api/v1/user/{user_id}/roles
#api.add_resource(
#    UserRolesList,
#     '/api/v1/user/<string:user_id>/roles')

# # http://server/api/v1/clients/{client_id}/users/{user_id}/engagements/
# api.add_resource(
#     EngagementList,
#     '/api/v1/clients/<string:client_id>/users/<string:user_id>/engagements')

# http://server/api/v1/metadata/app/{app_id}
api.add_resource(
     App, '/api/v1/metadata/app/<string:app_id>')

# http://server/api/v1/metadata/apps
api.add_resource(
     AppsList, '/api/v1/metadata/apps')


if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8016, debug=DEBUG)
