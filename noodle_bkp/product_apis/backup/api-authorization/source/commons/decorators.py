#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""python custom decorators."""

from constants.custom_field_error import HTTP_400_BAD_REQUEST,HTTP_200_OK,\
    HTTP_401_UNAUTHORIZED, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_404_NOT_FOUND
from functools import wraps
from jsonschema import validate, ValidationError
from flask import (
    jsonify,
    request,
)
import json
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)
from pymongo.errors import PyMongoError
from commons.util import return_result_final
from commons.mongo_services import NoodleMongoClient


def validate_json(f):
    """Validate format of request as json."""
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except ValueError as err:
            response = jsonify(
                {
                    "error": {
                        "code": "HTTP_400_BAD_REQUEST",
                        "message": err.message
                    }
                })
            response.status_code = HTTP_400_BAD_REQUEST
            return response
        return f(*args, **kw)
    return wrapper


def validate_schema(schema):
    """Validate json with generated schema."""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                validate(request.json, schema)
            except ValidationError as err:
                response = jsonresponse(err)
                return response, HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator


def jsonresponse(err):
    """jsonresponse."""
    response = json.loads(json.dumps(
        {
            "error": {
                "code": "HTTP_400_BAD_REQUEST",
                "message": err.args[0]
            }
        }))
    return response


def validate_user(roles):
    """Validate json with generated schema."""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_claims = get_jwt_claims()
                user_role = user_claims['userRole']
                user_permission = user_claims['permission']
                if user_role in roles:
                    if request.method != 'GET' and user_permission == 'read':
                        response = jsonify(
                            {
                                "error": {
                                    "code": "HTTP_401_UNAUTHORIZED",
                                    "message": "User does not have required permission for this action"
                                }
                            })
                        response.status_code = HTTP_401_UNAUTHORIZED
                        return response

                else:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_401_UNAUTHORIZED",
                                "message": "User does not have required role to for this action"
                            }
                        })
                    response.status_code = HTTP_401_UNAUTHORIZED
                    return response

            except ValidationError as err:
                response = jsonresponse(err)
                return response, HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator


def compare_user_role():
    """comapare between logged in and tagret user role"""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_id = kw['user_id']
                connection = NoodleMongoClient()
                return_object = connection.find_one(
                    collectionname="user",
                    data={
                        "userId": user_id,
                        "isActive": True
                    }
                )
            except PyMongoError as err:
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR), HTTP_500_INTERNAL_SERVER_ERROR
            if return_object:
                target_role = return_object['userRole']
                user_claims = get_jwt_claims()
                login_user_role = user_claims['userRole']

                role_hierarchy = {'super-admin': 1,
                                  'admin': 2,
                                  'manager': 3,
                                  'user': 4,
                                  'guest': 5}

                if role_hierarchy[login_user_role] >= role_hierarchy[target_role]:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_401_UNAUTHORIZED",
                                "message": "User does not have required role to for this shit"
                            }
                        })
                    response.status_code = HTTP_401_UNAUTHORIZED
                    return response
            else:
                return return_result_final(
                    [],
                    "{0} User id is not available".format(user_id),
                    "USER_NOT_FOUND"
                ), HTTP_404_NOT_FOUND

            return f(*args, **kw)
        return wrapper
    return decorator