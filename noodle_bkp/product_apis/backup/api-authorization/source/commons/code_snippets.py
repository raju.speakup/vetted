#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.util import return_result, get_url_args, get_sorting_list
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_406_NOT_ACCEPTABLE)
from pymongo.errors import PyMongoError
import sys
import os


def get_list(self, collectionname, querydata, request):
    """Get list data."""
    sys.path.insert(0, os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))))
    from logger.logger import logger
    data = list()
    try:
        limit, offset, gen_sort_key, projection = get_url_args(request)
    except ValueError as err:
        logger.info(err)
        return return_result(
            data,
            "Wrong Query Parameters",
            "HTTP_406_NOT_ACCEPTABLE"), HTTP_406_NOT_ACCEPTABLE
    sort_dict = get_sorting_list(gen_sort_key)
    try:
        return_object = self.connection.find_all(
            collectionname=collectionname,
            data=querydata,
            skip=offset,
            limit=limit,
            sortdata=sort_dict
        )
        logger.info(querydata)
    except PyMongoError as err:
        logger.info(err)
        return return_result(
            data,
            err.args[0],
            HTTP_500_INTERNAL_SERVER_ERROR), HTTP_500_INTERNAL_SERVER_ERROR
    if return_object:
        for entry in return_object:
            if entry:
                entry.pop("_id")
                data.append(entry)

    return return_result(
        data,
        self.error,
        self.error_code), HTTP_200_OK
