#!/usr/bin/env python
import ldap


def authenticate(address, username, password):
    """Authenticate."""
    conn = ldap.initialize('ldap://' + address)
    conn.protocol_version = 3
    conn.set_option(ldap.OPT_REFERRALS, 0)
    try:
        conn.simple_bind_s(username, password)
    except ldap.INVALID_CREDENTIALS:
        return False, "Invalid credentials"
    except ldap.SERVER_DOWN:
        return False, "Server down"
    except ldap.LDAPError as e:
        if type(e.message) == dict and 'desc' in e.message:
            return "Other LDAP error: " + e.message['desc']
        else:
            return False, "Other LDAP error: " + e
    finally:
        conn.unbind_s()

    return True, "Succesfully authenticated"
