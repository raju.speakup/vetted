#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Engagement API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------

from swagger.appuser_swagger import CreateAppUser, UpdateAppUser


class AppUser(Resource):
    """Engagement App User"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='App User Get',
        nickname='App User Get')
    def get(self, client_id, app_id, user_id):
        """ App User Get."""
        data = dict()
        try:
            return_object = self.connection.find_one_complex(
                collectionname="appUsers",
                data=[
                    {
                        "clientId": client_id,
                        "appId": app_id
                    },
                    {
                        "users": {
                            "$elemMatch": {
                                "userId": user_id,
                                "isActive": True
                            }
                        }
                    }]
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if return_object:
            return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='App User Delete',
        nickname='App User Delete')
    def delete(self, client_id, app_id, user_id):
        """App User Delete """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="appUsers",
                data={
                    "clientId": client_id,
                    "appId": app_id,
                    "users": {
                        "$elemMatch": {
                            "userId": user_id
                        }
                    }
                })
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "{0} user id do not exists for this app".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="appUsers",
                data=({
                    "clientId": client_id,
                    "appId": app_id,
                    "users": {
                        "$elemMatch": {
                            "userId": user_id
                        }
                    }
                }, {
                    "$set": {"users.$.isActive": False}
                }))
            logger.info(data)
        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateAppUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }

        ],
        notes='App User Update',
        nickname='App User Update')
    @validate_json
    #@validate_schema(schema)
    def put(self, client_id, app_id, user_id):
        """App User Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="appUsers",
                data={
                    "clientId": client_id,
                    "appId": app_id,
                    "users": {
                        "$elemMatch": {
                            "userId": user_id
                        }
                    }
                })
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} User for the App do not exists".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="appUsers",
                data=({
                    "clientId": client_id,
                    "appId": app_id,
                    "users": {
                        "$elemMatch": {
                            "userId": user_id,
                        }
                    }
                }, {
                    "$set": {"users.$.permission": data['permission'],"users.$.userRole": data['role']}
                }))
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            },
            {
                "name": "parameters",
                "dataType": CreateAppUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='App User Create',
        nickname='App User Create')
    @validate_json
    #@validate_schema(schema)
    def post(self, client_id, app_id, user_id):
        """App User Create"""
        data = {}
        try:
            self.connection.update_one(
                collectionname="appUsers",
                data=(
                    {
                        "clientId": client_id,
                        "appId": app_id
                    },
                    {
                        "$push": {
                            "users": {
                                "userId": user_id,
                                "permission": request.json.get('permission'),
                                "userRole": request.json.get('role'),
                                "isActive": True
                            }
                        }
                    }
                )
            )
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            {"message": "User Assigned"},
            self.error,
            self.error_code
        ), HTTP_201_CREATED
