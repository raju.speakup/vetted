#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema, validate_user
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_201_CREATED,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, )
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientdetails import schema
from swagger.clientuser_swagger import UpdateClientUser

class ClientUser(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },


        ],
        notes='Client User Get',
        nickname='Client User Get')
    @jwt_required
    @validate_user(roles=['manager', 'admin','super-admin'])
    def get(self, client_id,user_id):
        """Client User Get."""
        data = dict()
        try:
            return_object = self.connection.find_one_complex(
                collectionname="clientusers",
                data=[
                    {
                        "clientId": client_id
                    },
                    {
                        "clientusers":
                            {
                                "$elemMatch":
                                {
                                    "userId": user_id,
                                    "isActive": True
                                }
                            }
                    }
                ]
            )
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if return_object:
            return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='Client user delete',
        nickname='ClientUser delete')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    def delete(self, client_id, user_id):
        """Client User Delete."""
        data = {"client_id":client_id}
        try:
            return_object = self.connection.find_one(
                collectionname="clientusers",
                data={
                    "clientId": client_id,
                    "clientusers": {
                        "$elemMatch": {
                            "userId": user_id
                        }
                    }
                })
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} User id do not belong to the client".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clientusers",
                data=({
                    "clientId": client_id,
                    "clientusers": {
                        "$elemMatch": {
                            "userId": user_id
                        }
                    }
                }, {
                    "$set": {"clientusers.$.isActive": False}
                }))
        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            },
            {
                "name": "parameters",
                "dataType": UpdateClientUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
        ],
        notes='Client User Update',
        nickname='Client User Update')
    @validate_json
    @validate_schema(schema)
    @jwt_required
    @validate_user(roles=['admin','super-admin'])
    def put(self, client_id, user_id):
        """Client User Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="clientusers",
                data={
                    "clientId": client_id,
                    "clientusers": {
                        "$elemMatch": {
                            "userId": user_id
                        }
                    }
                })
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} User id do not belong to the client".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clientusers",
                data=({
                          "clientId": client_id,
                          "clientusers": {
                              "$elemMatch": {
                                  "userId": user_id
                              }
                          }
                      }, {
                          "$set": {"clientusers.$.userRole": data['userRole'],"clientusers.$.isActive": bool(data['isActive'])}
                      }))
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK