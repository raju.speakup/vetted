#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_201_CREATED,
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, )

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientdetails import schema
from swagger.clientassociation_swagger import (
    CreateClientAssociation, UpdateClientAssociation)


class ClientAssociation(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client  Association Get',
        nickname='Client Association Get')
    def get(self, client_id):
        """Client Association Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="clientassociation",
                data={"clientId": client_id, "isActive": True})
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if return_object:
            return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client Association Delete',
        nickname='Client Association Delete')
    def delete(self, client_id):
        """Client Association Delete.

        This is soft delete for client - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="clientassociation",
                data={"clientId": client_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "{0} Client Association do not exist -".format(client_id) +
                " No client for given domain in email address",
                "CLIENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clientassociation",
                data=({"clientId": client_id}, {
                    "$set": {"isActive": False}}))

        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateClientAssociation.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }

        ],
        notes='Client Association Update',
        nickname='Client Association Update')
    @validate_json
    @validate_schema(schema)
    def put(self, client_id):
        """Client Association Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="clientassociation",
                data={"clientId": client_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} client do not exists".format(client_id),
                "CLIENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clientassociation",
                data=({"clientId": client_id}, {
                    "$set": data}))
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateClientAssociation.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client Association Create',
        nickname='Client Association Create')
    @validate_json
    @validate_schema(schema)
    def post(self, client_id):
        """Client  Association Create."""
        data = json.loads(request.data)
        data.update(
            {"clientId": client_id,
             "isActive": True
             }
        )
        return_obejct = self.connection.find_one(
            collectionname="clientdetails",
            data={"clientId": client_id, "isActive": True})
        logger.info(data)
        data.update(
            {"isActive": True})
        logger.info(data)
        if return_obejct:
            try:
                self.connection.insert_one(collectionname="clientassociation",
                                           data=json.dumps(data))
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            data,
            self.error,
            self.error_code
        ), HTTP_201_CREATED
