#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource, reqparse
from flask import request, jsonify
from flask_restful_swagger import swagger
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json, validate_schema, validate_user, compare_user_role
from commons.util import return_result_final
from commons.code_snippets import get_list
from configuration.layer_fetch_environ import (
    TOKEN_EXPIRE_TIME,
    LDAP_URL,
    REFRESH_EXPIRE_TIME)
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED,
    HTTP_203_NON_AUTHORITATIVE_INFORMATION,
    HTTP_403_FORBIDDEN)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.login_swagger import Login, Registration
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    jwt_refresh_token_required)
from commons.ldap import authenticate

# ------------------------------------------------------------------------------
# PYTHON FUNCTIONS
# ------------------------------------------------------------------------------
import uuid
import json
import datetime
import bcrypt


class UserLogin(Resource):
    """UserLogin.

    Login mechanism.
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": Login.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='User Login',
        nickname='User Login')
    def post(self):
        """ User Login"""
        parser = reqparse.RequestParser()
        parser.add_argument(
            'email', help='This field cannot be blank', required=True)
        parser.add_argument(
            'password', help='This field cannot be blank', required=True)
        data = parser.parse_args()

        try:
            return_object = self.connection.find_one(
                collectionname="user",
                data={"email": data.get('email'), "isActive": True})
            logger.info(return_object)

        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                "Sorry! Something went wrong with Authorization service",
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if return_object:
            if return_object['type'] == "AD":
                result, message = authenticate(
                    LDAP_URL, data.get('email'), data.get('password'))
                if not result:
                    return return_result_final(
                        data,
                        message,
                        HTTP_403_FORBIDDEN
                    ), HTTP_403_FORBIDDEN
            else:
                if not bcrypt.checkpw(
                    data.get('password').encode('utf-8'),
                    return_object['password'].encode('utf-8')
                ):
                    return return_result_final(
                        data,
                        "Email and password combination is wrong",
                        HTTP_403_FORBIDDEN
                    ), HTTP_403_FORBIDDEN

            return_object.pop('_id')
            return_object.pop('password')
            data.update(return_object)

            expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
            referesh_expires = datetime.timedelta(minutes=REFRESH_EXPIRE_TIME)
            accesstoken = create_access_token(
                identity=data['userId'], expires_delta=expires)
            refreshtoken = create_refresh_token(
                identity=data['userId'], expires_delta=referesh_expires)

            from apps import add_claims_to_access_token
            add_claims_to_access_token(return_object)
            data.update({'accessToken': accesstoken,
                         'accesstokenExpTime': TOKEN_EXPIRE_TIME,
                         'refreshToken': refreshtoken,
                         'refreshTokenExpTime': REFRESH_EXPIRE_TIME,
                         })
            data.pop('password')
        else:
            try:
                check_domain = self.connection.find_one(
                    collectionname="clientassociation",
                    data={"clientDomain": data['email'].split('@')[1]}
                )
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR

            if not check_domain:
                return return_result_final(
                    [],
                    "Domain Provided in email is not registered with us",
                    HTTP_203_NON_AUTHORITATIVE_INFORMATION
                ), HTTP_203_NON_AUTHORITATIVE_INFORMATION

            client_id = check_domain.get('clientId')
            password_hash = bcrypt.hashpw(
                data.get(
                    'password').encode('utf-8'),
                bcrypt.gensalt())
            userid = uuid.uuid4().hex
            data.update(
                {
                    'userId': userid,
                    'isActive': True,
                    'type': "AD",
                    "userRole": "guest",
                    'password': password_hash.decode('utf-8')
                })
            try:
                self.connection.insert_one(
                    collectionname="user",
                    data=json.dumps(data))
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR

            try:
                self.connection.update_one(
                    collectionname="clientusers",
                    data=(
                        {
                            "clientId": client_id
                        },
                        {
                            "$push": {
                                "clientusers": {
                                    "userId": userid,
                                    "userRole": "guest",
                                    "isActive": True
                                }
                            }
                        }
                    )
                )
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
            data.pop('password')
            return data, HTTP_201_CREATED

        return data, HTTP_200_OK


class CreateUser(Resource):
    """CreateUser.
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": Registration.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes=' User Create',
        nickname='User Create')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    def post(self):
        """Create User."""
        data = request.json
        try:
            return_object = self.connection.find_one(
                collectionname="user",
                data={"email": data['email']}
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                [],
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            try:
                check_domain = self.connection.find_one(
                    collectionname="clientassociation",
                    data={"clientDomain": data['email'].split('@')[1]}
                )
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
            if not check_domain:
                return return_result_final(
                    [],
                    "Domain Provided in email is not registered with us",
                    HTTP_203_NON_AUTHORITATIVE_INFORMATION
                ), HTTP_203_NON_AUTHORITATIVE_INFORMATION

            if data['type'] == "AD":
                result, message = authenticate(
                    LDAP_URL, data.get('email'), data.get('password'))
                if not result:
                    return return_result_final(
                        data,
                        message,
                        HTTP_403_FORBIDDEN
                    ), HTTP_403_FORBIDDEN

            client_id = check_domain.get('clientId')
            password_hash = bcrypt.hashpw(
                data.get(
                    'password').encode('utf-8'),
                bcrypt.gensalt())
            userid = uuid.uuid4().hex
            data.update(
                {
                    'userId': userid,
                    'isActive': True,
                    'password': password_hash.decode('utf-8')
                })
            try:
                self.connection.insert_one(
                    collectionname="user",
                    data=json.dumps(data))
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR

            try:
                self.connection.update_one(
                    collectionname="clientusers",
                    data=(
                        {
                            "clientId": client_id
                        },
                        {
                            "$push": {
                                "clientusers": {
                                    "userId": userid,
                                    "userRole": data['userRole'],
                                    "isActive": True
                                }
                            }
                        }
                    )
                )
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR

            return data, HTTP_201_CREATED
        else:
            return return_result_final(
                [],
                "User Exist",
                HTTP_203_NON_AUTHORITATIVE_INFORMATION
            ), HTTP_203_NON_AUTHORITATIVE_INFORMATION


class DeleteUser(Resource):
    """DeleteUser.
        """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='User delete',
        nickname='User delete')
    @jwt_required
    @validate_user(roles=['super-admin', 'admin'])
    @compare_user_role()
    def delete(self, user_id):
        """User Delete."""
        data = {"user_id": user_id}
        try:
            return_object = self.connection.find_one(
                collectionname="user",
                data={
                    "userId": user_id
                })
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} User id is not available".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:

            self.connection.update_one(
                collectionname="user",
                data=({
                          "userId": user_id,
                      }, {
                          "$set": {"isActive": False}
                      }))
            return return_result_final(
                json.dumps([{"message": "Soft Delete Applied"}]),
                self.error,
                self.error_code), HTTP_200_OK


class SecretKey(Resource):
    """SecretKey."""

    @swagger.operation(
        parameters=[],
        notes='Secrete Get',
        nickname='Secrete')
    def get(self):
        """Get."""
        import os
        import binascii
        key = binascii.hexlify(os.urandom(24))
        return {
            'secreteKey': str(key)
        }


class Refresh(Resource):
    """Refresh Token."""

    @jwt_refresh_token_required
    def post(self):
        """Post."""
        data = {}
        # from apps import check_if_token_in_blacklist
        # if check_if_token_in_blacklist(get_raw_jwt()):
        #     data.update({'message': 'Token BlackListed'})
        #     return jsonify(data)
        current_user = get_jwt_identity()
        expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
        data.update({
            'accessToken': str(create_access_token(
                identity=current_user, expires_delta=expires))
        })
        return jsonify(data)
