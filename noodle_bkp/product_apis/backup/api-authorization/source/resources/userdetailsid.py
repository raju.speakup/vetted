#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.userdetails import schema
from swagger.user_swagger import UpdateUser


class User(Resource):
    """User."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='User Get',
        nickname='User')
    def get(self, user_id):
        """User Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="userdetails",
                data={"userId": user_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='User Delete',
        nickname='UserDelete')
    def delete(self, user_id):
        """User Delete.

        This is soft delete for client - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="userdetails",
                data={"userId": user_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "{0} User do not exists".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="userdetails",
                data=({"userId": user_id}, {
                    "$set": {"isActive": False}}))

        self.connection.update_many(
            collectionname="engagementCollection",
            data=({"userId": user_id}, {
                "$set": {"isActive": False}}))

        self.connection.update_many(
            collectionname="engagementAppCollection",
            data=({"userId": user_id}, {
                "$set": {"isActive": False}}))

        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateUser.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "user_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }

        ],
        notes='Client Create',
        nickname='Client Create')
    @validate_json
    @validate_schema(schema)
    def put(self, user_id):
        """Client Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="userdetails",
                data={"userId": user_id, "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} User do not exists".format(user_id),
                "USER_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="userdetails",
                data=({"userId": user_id}, {
                    "$set": data}))
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK
