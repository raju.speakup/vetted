#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config

from configuration.layer_fetch_environ import LOG_FILE_PATH

dict_config = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "json": {
            "format": "\
             %(asctime)s \
             %(created)f \
             %(name)s \
             %(thread)d \
             %(threadName)s\
             %(message)s \
             %(lineno)d \
             %(pathname)s \
             %(levelname)s \
             %(process)d \
             %(processName)s \
             %(thread)d\
             %(filename)s\
             %(funcName)s",
            "class": "pythonjsonlogger.jsonlogger.JsonFormatter"
        }

    },
    "handlers": {
        "file_handler": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "formatter": "json",
            "filename": LOG_FILE_PATH,
            "mode": "a",
            "encoding": "utf-8"
        },
        "stdout":
            {
            "class": "logging.StreamHandler",
            "formatter": "json",
            "stream": "ext://sys.stdout"
        }
    },
    "loggers": {
        "application": {
            "handlers": ["file_handler", "stdout"],
            "level": "DEBUG",
            "propagate": True
        }
    },
    "root": {
        "level": "DEBUG",
        "handlers": ["file_handler", "stdout"]
    }
}

logging.config.dictConfig(dict_config)
logger = logging.getLogger(__name__)
