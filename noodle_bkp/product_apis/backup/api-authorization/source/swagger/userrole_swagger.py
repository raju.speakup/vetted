#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateUserRole:
    """Create User Role fields."""

    resource_fields = {
        'user_id': 'fields.String',
        'role_id': 'fields.String'
    }


@swagger.model
class UpdateUserRole:
    """Update User Role fields."""

    resource_fields = {
        'isActive': 'fields.boolean'
    }