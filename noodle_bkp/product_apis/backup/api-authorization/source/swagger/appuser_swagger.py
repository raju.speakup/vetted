#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateAppUser:
    """CreateApp fields."""

    resource_fields = {
        'permission': 'fields.String',
        'role': 'fields.String'
    }


@swagger.model
class UpdateAppUser:
    """UpdateApp fields."""

    resource_fields = {
        'permission': 'fields.String',
        'role': 'fields.String'

    }