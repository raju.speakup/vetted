from flask_restful_swagger import swagger

@swagger.model
class UpdateClientApp:
    """Update Client App fields."""

    resource_fields = {
        'appVersion': 'fields.String',
        'isEnabled': 'fields.String',
    }

@swagger.model
class CreateClientApp:
    """Create Client App fields."""

    resource_fields = {
        'appVersion': 'fields.String',
    }