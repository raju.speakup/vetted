#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementApp:
    """CreateApp fields."""

    resource_fields = {
        'appName': 'fields.String',
    }


@swagger.model
class UpdateEngagementApp:
    """UpdateApp fields."""

    resource_fields = {
        'appName': 'fields.String',

    }