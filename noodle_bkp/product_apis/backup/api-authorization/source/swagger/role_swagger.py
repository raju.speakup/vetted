#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateRole:
    """CreateUser fields."""

    resource_fields = {
        'id': 'fields.String',
        'role': 'fields.String'
    }


@swagger.model
class UpdateRole:
    """UpdateUser fields."""

    resource_fields = {
        'id': 'fields.String',
        'role': 'fields.String'
    }
