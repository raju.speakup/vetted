#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateApp:
    """Create App fields."""

    resource_fields = {
        'appName': 'fields.String',
        'description': 'fields.String',
        'currentAppVersion': 'fields.String',
    }


@swagger.model
class UpdateApp:
    """Update App fields."""

    resource_fields = {
        'appName': 'fields.String',
        'description': 'fields.String',
        'currentAppVersion': 'fields.String',
    }
