#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateClientAssociation:
    """CreateClient fields."""

    resource_fields = {
        'clientDomain': 'fields.String',
    }


@swagger.model
class UpdateClientAssociation:
    """UpdateClient fields."""

    resource_fields = {
        'clientDomain': 'fields.String',

    }
