#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateClientEngagement:
    """Create Engagement fields."""

    resource_fields = {
        'engagementName': 'fields.String',
    }


@swagger.model
class UpdateClientEngagement:
    """Update Engagement fields."""

    resource_fields = {
        'engagementName': 'fields.String',

    }