#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger
from flask_restful import fields


@swagger.model
class Engagements:
    """Engagement array."""

    resource_fields = {
        "permission": "fields.String",
        "level": "fields.String"
    }


@swagger.model
@swagger.nested(
    engagemenrs=Engagements.__name__)
class CreateEngagement:
    """CreateEngagement fields."""

    resource_fields = {
        'permission': 'fields.String',
        # 'engagements': fields.List(fields.Nested(Engagements.resource_fields))
    }


@swagger.model
@swagger.nested(
    engagements=Engagements.__name__)
class UpdateEngagement:
    """UpdateEngagement fields."""

    resource_fields = {
        'permission': 'fields.String',
        # 'engagements': fields.List(fields.Nested(Engagements.resource_fields))
    }
