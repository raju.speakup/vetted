#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateUser:
    """CreateUser fields."""

    resource_fields = {
        'userName': 'fields.String',
        'firstName': 'fields.String',
        'lastName': 'fields.String',
        'contactEmail': 'fields.String',
        'userAddress': 'fields.String',
        'address2': 'fields.String',
        'createdOn': 'fields.String',
        'modifiedOn': 'fields.String'
    }


@swagger.model
class UpdateUser:
    """UpdateUser fields."""

    resource_fields = {
        'userName': 'fields.String',
        'firstName': 'fields.String',
        'lastName': 'fields.String',
        'contactEmail': 'fields.String',
        'userAddress': 'fields.String',
        'address2': 'fields.String',
        'createdOn': 'fields.String',
        'modifiedOn': 'fields.String'
    }
