#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger

@swagger.model
class UpdateClientUser:
    """UpdateClientUser fields."""

    resource_fields = {
        'userRole': 'fields.String',
        'isActive': 'fields.String',

    }