# Noodle MVP APIs

### Tech

Noodle MVP API uses a number of open source projects to work properly:

* [Python Flask](http://flask.pocoo.org/) - Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions.
* [Python FLASK RESTful](https://flask-restful.readthedocs.io/en/latest/) - Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs.
* [Sublime Text Editor](https://www.sublimetext.com/) - Sublime Text is a sophisticated text editor for code, markup and prose
* [Pymssql](http://www.pymssql.org/en/stable/) - Python 3 friendly; Works on most popular operating systems; Written in Cython for performance; Includes a supported and documented low-level module ( _mssql ) that you can use instead of the DB-API

Stack will be updated as and when new libraries are added to project.

### Installation | Ubuntu 

Update and upgrade operating system.

```

sudo apt-get update && sudo apt-get -y upgrade

```


It is recommended by the Flask Software Foundation to use Python 3, so once everything is updated, we can install Python 3 by using the following command:

```

sudo apt-get install python3

```


Now that we have Python 3 installed, we will also need pip in order to install packages from PyPi, PythonÃ¢â‚¬â„¢s package repository.

```

sudo apt-get install -y python3-pip

```


virtualenv is a virtual environment where you can install software and Python packages in a contained development space, which isolates the installed software and packages from the rest of your machineÃ¢â‚¬â„¢s global environment. This convenient isolation prevents conflicting packages or software from interacting with each other.

To install virtualenv, we will use the pip3 command, as shown below:

```

pip3 install virtualenv

```


Now, create a virtual environment within the project directory by typing:

```

virtualenv newenv

```


To install packages into the isolated environment, you must activate it by typing:

```

source newenv/bin/activate

```


All the depedencies for backend is stored in requirements.txt file.
Go to codebase and find requirement.txt file
Install all dependencies from requirements file to virtualenv.

```

pip3 install -r requirements.txt

```

Following shoud be statuses and stages in database - Provided by Kumar <kumar.srivastava@noodle.ai>

Status
````
Not started
In progress
Blocked
Completed
````

Stages
```
Discussion
data ingest
Eda
Modeling
Deployment
Production
```

Following packages are used in developing stack

```

# FLASK RELATED PACKAGES
Flask==0.12.2
Flask-RESTful==0.3.6

# DOCUMENTATION RELATED PACKAGE
Sphinx==1.6.5
sphinx-rtd-theme==0.2.4
flask-restful-swagger==0.19

# DATABASE RELATED PACKAGES
pymssql==2.1.3

# LOCAL DATABASE RELATED PACKAGES
--extra-index-url http://nexus.ad.noodle.ai:8081/repository/python-group/simple
dbconnector==1.0.0

# PYTHON RELATED PACKAGES
python-json-logger==0.1.8
python-dotenv==0.7.1

```
Repository Maintainer 
---------------------

* Gururaj Jeerge <gururaj.jeerge@noodle.ai>
