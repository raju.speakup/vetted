#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code project starter
    Functionality:
        Create flask application
        Create routes for resources views
        Define swagger
"""

from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger
from resource.client_resource_id import Client
from resource.client_resource_list import Clients
from resource.user_resources_id import User
from resource.user_resource_list import Users, UsersPost
from resource.client_engagement_id import Engagement
from resource.client_engagement_list import Engagements, EngagementsPost
from resource.create_default_apps import DefaultApps
from resource.client_engagement_status_list import EngagementsStatuses
from resource.client_engagement_status_id import EngagementsStatus
from resource.client_engagement_stage_list import EngagementsStages
from resource.client_engagement_stage_id import EngagementsStage
from resource.client_engagement_dataset_id import EngagementsDataset
from resource.client_engagement_dataset_list import EngagementsDatasets
from resource.client_engagement_preference_list import EngagementsPreferences
from resource.client_engagement_preference_id import EngagementsPreference
from resource.client_engagement_noodleapp_id import EngagementsNoodleapp
from resource.client_engagement_noodleapp_list import (
    EngagementsNoodleapps,
    EngagementsNoodleappsPost,
    EngagementsNoodleappsSingle
)

from resource.noodle_engagement_doc_list import (
    EngagementsDocs,
    EngagementsDocsPost
)
from resource.noodle_engagement_doc_id import EngagementDoc
from resource.health import Health
from config.config import DEFAULT_IP

app = Flask(__name__)


api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="MVP Client API Documentation",
    resourcePath="/Client"
)

# http://server/api/v1/client/client/{client_id}
api.add_resource(
    Client,
    '/api/v1/clients/clients/<int:client_id>')

# http://server/api/v1/client
api.add_resource(
    Clients,
    '/api/v1/clients')

# http://server/api/v1/client/{client_id}/users/{user_id}
api.add_resource(
    User,
    '/api/v1/clients/clients/<int:client_id>/users/<int:user_id>')

# http://server/api/v1/client/{client_id}/users
api.add_resource(
    Users,
    '/api/v1/clients/<int:client_id>/users')

# http://server/api/v1/client/users
api.add_resource(
    UsersPost,
    '/api/v1/clients/users')

# http://server/api/v1/client/{client_id}/engagements
api.add_resource(
    Engagements,
    '/api/v1/clients/<int:client_id>/engagements')

# http://server/api/v1/client/{client_id}/engagement/{engagement_id}
api.add_resource(
    Engagement,
    '/api/v1/clients/<int:client_id>/engagements/<int:engagement_id>')

# http://server/api/v1/client/engagements
api.add_resource(
    EngagementsPost,
    '/api/v1/clients/engagements')

# http://server/api/v1/client/engagement/status
api.add_resource(
    EngagementsStatuses,
    '/api/v1/clients/metadata/statuses')

# http://server/api/v1/client/engagement/status/{status_id}
api.add_resource(
    EngagementsStatus,
    '/api/v1/clients/metadata/statuses/<string:status_id>')

# http://server/api/v1/client/engagement/stages
api.add_resource(
    EngagementsStages,
    '/api/v1/clients/metadata/stages')

# http://server/api/v1/client/engagement/stage/stage_id
api.add_resource(
    EngagementsStage,
    '/api/v1/clients/metadata/stages/<string:stage_id>')

# http://server/api/v1/client/engagement/datasets
api.add_resource(
    EngagementsDatasets,
    '/api/v1/clients/engagements/datasets')

# http://server/api/v1/client/engagement/datasets/{dataset_id}
api.add_resource(
    EngagementsDataset,
    '/api/v1/clients/engagements/<int:engagement_id>/' +
    'datasets/<int:dataset_id>')

# http://server/api/v1/client/engagement/preferences
api.add_resource(
    EngagementsPreferences,
    '/api/v1/clients/<int:client_id>/engagements/preferences')

# http://server/api/v1/client/engagement/{engagement_id}
# /preference/{pref_id}
api.add_resource(
    EngagementsPreference,
    '/api/v1/clients/<int:client_id>/' +
    'engagements/<int:engagement_id>/preferences/<int:pref_id>')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/noodleapp/{app_id}
api.add_resource(
    EngagementsNoodleapp,
    '/api/v1/clients/<int:client_id>/engagements/' +
    '<int:engagement_id>/noodleapps/<int:app_id>')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/noodleapps
api.add_resource(
    EngagementsNoodleapps,
    '/api/v1/clients/<int:client_id>/engagements/' +
    '<int:engagement_id>/noodleapps')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/noodleapps/{app_id}
api.add_resource(
    EngagementsNoodleappsSingle,
    '/api/v1/clients/<int:client_id>/engagements/' +
    '<int:engagement_id>/noodleapp/<int:app_id>')


# http://server/api/v1/client/engagement/{engagement_id}/
# noodleapps
api.add_resource(
    EngagementsNoodleappsPost,
    '/api/v1/clients/engagements/noodleapps')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/docs
api.add_resource(
    EngagementsDocs,
    '/api/v1/clients/<int:client_id>/engagements/' +
    '<int:engagement_id>/docs')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/docs/{doc_id}
api.add_resource(
    EngagementDoc,
    '/api/v1/clients/<int:client_id>/engagements/' +
    '<int:engagement_id>/docs/<string:document_name>')

# http://server/api/v1/client/engagement/docs
api.add_resource(
    EngagementsDocsPost,
    '/api/v1/clients/engagement/docs')

# http://server/api/v1/client/engagement/create_default
api.add_resource(
    DefaultApps,
    '/api/v1/clients/engagement/create_default')

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8000, debug=False)
