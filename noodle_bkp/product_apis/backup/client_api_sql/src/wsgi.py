#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code connect with wsgi
    Functionality:
        Call flask starter source code and run as application
"""

from apps import app

if __name__ == "__main__":
    app.run()
