src\.resource package
=====================

Submodules
----------

src\.resource\.client\_engagement\_dataset\_id module
-----------------------------------------------------

.. automodule:: src.resource.client_engagement_dataset_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_dataset\_list module
-------------------------------------------------------

.. automodule:: src.resource.client_engagement_dataset_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_id module
--------------------------------------------

.. automodule:: src.resource.client_engagement_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_list module
----------------------------------------------

.. automodule:: src.resource.client_engagement_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_noodleapp\_id module
-------------------------------------------------------

.. automodule:: src.resource.client_engagement_noodleapp_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_noodleapp\_list module
---------------------------------------------------------

.. automodule:: src.resource.client_engagement_noodleapp_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_preference\_id module
--------------------------------------------------------

.. automodule:: src.resource.client_engagement_preference_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_preference\_list module
----------------------------------------------------------

.. automodule:: src.resource.client_engagement_preference_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_stage\_id module
---------------------------------------------------

.. automodule:: src.resource.client_engagement_stage_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_stage\_list module
-----------------------------------------------------

.. automodule:: src.resource.client_engagement_stage_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_status\_id module
----------------------------------------------------

.. automodule:: src.resource.client_engagement_status_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_engagement\_status\_list module
------------------------------------------------------

.. automodule:: src.resource.client_engagement_status_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_noodleapp\_id module
-------------------------------------------

.. automodule:: src.resource.client_noodleapp_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_noodleapp\_list module
---------------------------------------------

.. automodule:: src.resource.client_noodleapp_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_resource\_id module
------------------------------------------

.. automodule:: src.resource.client_resource_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_resource\_list module
--------------------------------------------

.. automodule:: src.resource.client_resource_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.health module
----------------------------

.. automodule:: src.resource.health
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.noodle\_engagement\_doc\_id module
-------------------------------------------------

.. automodule:: src.resource.noodle_engagement_doc_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.noodle\_engagement\_doc\_list module
---------------------------------------------------

.. automodule:: src.resource.noodle_engagement_doc_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.user\_resource\_list module
------------------------------------------

.. automodule:: src.resource.user_resource_list
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.user\_resources\_id module
-----------------------------------------

.. automodule:: src.resource.user_resources_id
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.resource
    :members:
    :undoc-members:
    :show-inheritance:
