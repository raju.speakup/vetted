from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreatePreference:
    """CreatePreference fields.

    Defining post json fields
    """

    resource_fields = {
        'engagementid': fields.String,
        'preferencevalue': fields.String
    }


@swagger.model
class UpdatePreference:
    """CreatePreference fields.

    Defining post json fields
    """

    resource_fields = {
        'preferencevalue': fields.String
    }
