from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateClient:
    """CreateClient fields.

    Defining post json fields
    """

    resource_fields = {
        'contactname': fields.String,
        'addressline1': fields.String,
        'addressline2': fields.String,
        'clientname': fields.String,
        'zip': fields.String,
        'contactemail': fields.String,
        'statecode': fields.String,
        'contactphone': fields.String,
        'city': fields.String,
        'clientshortname': fields.String,
        'countrycode': fields.String,
        'onboardedby': fields.String,
        'preferreddateformat': fields.String,
        'timezone': fields.String,
    }


@swagger.model
class UpdateClient:
    """UpdateClient fields.

    Defining post json fields
    """

    resource_fields = {
        'contactname': fields.String,
        'addressline1': fields.String,
        'addressline2': fields.String,
        'clientname': fields.String,
        'zip': fields.String,
        'contactemail': fields.String,
        'statecode': fields.String,
        'contactphone': fields.String,
        'city': fields.String,
        'clientshortname': fields.String,
        'countrycode': fields.String,
        'onboardedby': fields.String,
        'preferreddateformat': fields.String,
        'timezone': fields.String,
    }
