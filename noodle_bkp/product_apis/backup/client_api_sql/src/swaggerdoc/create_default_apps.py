from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateDefaultApps:
    """CreateClient fields.

    Defining post json fields
    """

    resource_fields = {
        'engagementid': fields.String,
    }
