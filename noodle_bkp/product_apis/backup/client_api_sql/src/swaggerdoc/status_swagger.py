from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateStatus:
    """CreateStatus fields.

    Defining post json fields
    """

    resource_fields = {
        'statusname': fields.String
    }


@swagger.model
class UpdateStatus:
    """UpdateStatus fields.

    Defining post json fields
    """

    resource_fields = {
        'statusname': fields.String
    }
