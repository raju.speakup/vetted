from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateStages:
    """CreateStatus fields.

    Defining post json fields
    """

    resource_fields = {
        'stagename': fields.String
    }


@swagger.model
class UpdateStages:
    """UpdateStatus fields.

    Defining post json fields
    """

    resource_fields = {
        'stagename': fields.String
    }
