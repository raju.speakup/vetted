from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateApp:
    """CreateApp fields.

    Defining post json fields
    """

    resource_fields = {
        'appversion': fields.String,
        'isenabled': fields.String,
        'engagementid': fields.String,
        'description': fields.String,
        'validfromdate': fields.String,
        'validtodate':fields.String,
        'validtodate': fields.String,
        'appname': fields.String
    }

@swagger.model
class UpdateApp:
    """UpdateApp fields.

    Defining post json fields
    """

    resource_fields = {
        'appversion': fields.String,
        'isenabled': fields.String,
        'engagementid': fields.String,
        'description': fields.String,
        'validfromdate': fields.String,
        'validtodate':fields.String,
        'validtodate': fields.String,
        'appname': fields.String
    }
