from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateUser:
    """CreateUser fields.

    Defining post json fields
    """

    resource_fields = {
        'contactname': fields.String,
        'contactemail': fields.String,
        'createdby': fields.String,
        'contactphone': fields.String,
        'username': fields.String,
        'useraddress': fields.String,
        'clientid': fields.String
    }


@swagger.model
class UpdateUser:
    """UpdateUser fields.

    Defining post json fields
    """

    resource_fields = {
        'contactname': fields.String,
        'contactemail': fields.String,
        'createdby': fields.String,
        'contactphone': fields.String,
        'username': fields.String,
        'useraddress': fields.String,
    }
