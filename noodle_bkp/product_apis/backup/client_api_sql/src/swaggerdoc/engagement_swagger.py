from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateEngagement:
    """CreateEngagement fields.

    Defining post json fields
    """

    resource_fields = {
        'clientid': fields.String,
        'statusid': fields.String,
        'stageid': fields.String,
        'startdate': fields.String,
        'enddate': fields.String,
        'engagementname': fields.String,
        'description': fields.String
    }


@swagger.model
class UpdateEngagement:
    """UpdateEngagement fields.

    Defining post json fields
    """

    resource_fields = {
        'statusid': fields.String,
        'stageid': fields.String,
        'startdate': fields.String,
        'enddate': fields.String,
        'engagementname': fields.String,
        'description': fields.String
    }
