from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateDataset:
    """CreateDataset fields.

    Defining post json fields
    """

    resource_fields = {
        'engagementid': fields.String,
        'appid': fields.String,
        'datasetid': fields.String,
        'isenabled': fields.String
    }


@swagger.model
class UpdateDataset:
    """UpdateDataset fields.

    Defining post json fields
    """

    resource_fields = {
        'appid': fields.String,
        'isenabled': fields.String
    }
