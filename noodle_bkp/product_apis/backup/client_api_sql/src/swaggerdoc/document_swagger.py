from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateDocument:
    """CreateDocument fields.

    Defining post json fields
    """

    resource_fields = {
        'documenname': fields.String,
        'modifiedon': fields.String,
        'createdon': fields.String,
        'owner': fields.String,
        'engagementid': fields.String,
        'documentpath': fields.String
    }


@swagger.model
class UpdateDocument:
    """UpdateDocument fields.

    Defining post json fields
    """

    resource_fields = {
        'documenname': fields.String,
        'modifiedon': fields.String,
        'createdon': fields.String,
        'owner': fields.String,
        'documentpath': fields.String
    }
