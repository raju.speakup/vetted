#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources configuration file
    Functionality:
        Define all confidential data

"""
import os
from dotenv import load_dotenv


APP_ROOT = os.path.join(os.path.dirname(__file__), '..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)

SERVER_NAME = os.getenv(os.getenv("MODE") + '_' + "SERVER_NAME")
DB_NAME = os.getenv(os.getenv("MODE") + '_' + "DB_NAME")
USERNAME = os.getenv(os.getenv("MODE") + '_' + "USERNAME")
PASSWORD = os.getenv(os.getenv("MODE") + '_' + "PASSWORD")
DATABASE_SCHEMA = os.getenv(os.getenv("MODE") + '_' + "DATABASE_SCHEMA")
DATABASE_NAME = os.getenv(os.getenv("MODE") + '_' + "DATABASE_NAME")
DEFAULT_IP = os.getenv("DEFAULT_IP")
LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")
