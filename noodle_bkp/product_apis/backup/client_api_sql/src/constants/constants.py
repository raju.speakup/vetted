#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources Constant declaration
    Functionality:
        Declare project variable to access table names in code base

"""
from config.config import DATABASE_SCHEMA
CLIENT_PROFILE = DATABASE_SCHEMA + '.[client]'
CLIENT_PROFILE_ABSTRACT = 'client'

USER_PROFILE = DATABASE_SCHEMA + '.[ClientUser]'
USER_PROFILE_ABSTRACT = 'ClientUser'

CLIENT_ENGAGEMENT = DATABASE_SCHEMA + '.[Engagement]'
CLIENT_ENGAGEMENT_ABSTRACT = 'Engagement'

CLIENT_ENGAGEMENT_APP = DATABASE_SCHEMA + '.[EngagementApp]'
CLIENT_ENGAGEMENT_APP_ABSTRACT = 'EngagementApp'

CLIENT_ENGAGEMENT_DATASET = DATABASE_SCHEMA + '.[EngagementAppDataset]'
CLIENT_ENGAGEMENT_DATASET_ABSTRACT = 'EngagementAppDataset'

CLIENT_ENGAGEMENT_PREFERENCE = DATABASE_SCHEMA + '.[EngagementPreference]'
CLIENT_ENGAGEMENT_PREFERENCE_ABSTRACT = 'EngagementPreference'

CLIENT_ENGAGEMENT_STAGE_LOOKUP = DATABASE_SCHEMA + '.[EngagementStageLookup]'
CLIENT_ENGAGEMENT_STAGE_LOOKUP_ABSTRACT = 'EngagementStageLookup'

CLIENT_ENGAGEMENT_STATUS_LOOKUP = DATABASE_SCHEMA + '.[EngagementStatusLookup]'
CLIENT_ENGAGEMENT_STATUS_LOOKUP_ABSTRACT = 'EngagementStatusLookup'

CLIENT_ENGAGEMENT_NOODLE_APP = DATABASE_SCHEMA + '.[NoodleApp]'
CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT = 'NoodleApp'


CLIENT_ENGAGEMENT_DOC = DATABASE_SCHEMA + '.[EngagementDocument]'
CLIENT_ENGAGEMENT_DOC_ABSTRACT = 'EngagementDocument'
CLIENT_ENGAGEMENT_DOC_V = DATABASE_SCHEMA + '.[SearchEngagementDocument]'
CLIENT_ENGAGEMENT_DOC_V_ABSTRACT = 'SearchEngagementDocument'