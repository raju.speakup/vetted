#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client Engagement Datasets API code
    Functionality:
        get: get dataset details by ids [single entry]
        delete: delete dataset by id [single entry]
        put: update dataset details by id [single entry]

    .. module:: client_users
    :platform: OS X
    :synopsis: Creating get (single dataset of client)
    and post (single entry) method.

    .. script:: Gururaj Jeerge <gururaj.jeerge@noodle.ai>
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete
)
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)
from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_delete import (
    dynamic_queris_delete
)

from constants.constants import (
    CLIENT_ENGAGEMENT_DATASET,
    CLIENT_ENGAGEMENT_DATASET_ABSTRACT

)

from queries.dynamic_queris_update import (
    dynamic_queris_update
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    JSON_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.dataset_swagger import UpdateDataset


class EngagementsDataset(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            }
        ],
        notes='get dataset details with engagement id and dataset id',
        nickname='Engagement Dataset')
    def get(self, engagement_id, dataset_id):
        """Get Dataset.

        Args:
            clientid (int): Client id is unique key associated
                            client in database
            dataset_id (int): Dataset Id

        Operations:
            1) Get Column names of table from Dbconnector.columnnames()
            2) Write conditional clauses
            3) Generate Query
            4) Execute query through Dbconnector.execute()
            5) Convert Result in Json
            6) Return Json and status code

        Returns:
            Returns json object of Dataset else empty data object

        """
        result = []

        # Operation 1
        try:
            self.column_names = [
                x.lower() for x in self.connector.columnnames(
                    CLIENT_ENGAGEMENT_DATASET_ABSTRACT)]
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 2
        where_filds = [['engagementid', '=', engagement_id],
                       ["datasetid", "=", dataset_id]]
        between_fields = []
        sorting_filed = {}

        # Operation 3
        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_DATASET,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=self.column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        # Operation 4
        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 5
        result = convert_to_json(
            rows=db_result[0],
            column_names=self.column_names
        )

        # Operation 6
        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            }
        ],
        notes='delete dataset associated to engagement',
        nickname='Engagement Dataset')
    def delete(self, engagement_id, dataset_id):
        """Delete Dataset.

        Args:
            engagement_id (int): engagement_id is unique key associated
                            particular engagement
            dataset_id (int): dataset set id

        Operations:
            1) Write conditional clauses
            2) Generate Query
            3) Execute query through Dbconnector.execute()
            4) Return Json and status code

        Returns:
            Returns json object of dataset deleted else error object

        """
        result = {}

        # Operation 1
        where_filds = [['engagementid', '=', engagement_id],
                       ["datasetid", "=", dataset_id]]

        # Operation 2
        query = dynamic_queris_delete(
            table_name=CLIENT_ENGAGEMENT_DATASET,
            where_query=where_query(fields=where_filds),
        )
        logger.info(query)

        # Operation 3
        try:
            self.connector.delete(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 4
        return return_result_delete(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "dataset_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            },
            {
                "name": "isenabled",
                "dataType": UpdateDataset.__name__,
                "paramType": "body",
                "required": True,
                "description": "Post parameters"
            }
        ],
        notes='Update dataset details with engagement id and dataset id')
    def put(self, engagement_id, dataset_id):
        """Update Dataset.

        Args:
            engagement_id (int): engagement_id is unique key associated
                            particular engagement
            dataset_id (int): dataset set id

        Operations:
            1) Get Data in Json format from request.data and convert it to
            data dictionary
            2) Get Column names of table from Dbconnector.columnnames()
            3) Generate Correct Data
            4) Generate Conditional operators
            5) Generate Query
            6) Execute query through Dbconnector.execute_no_return()
            7) Convert Result in Json
            8) Return Json and status code

        Returns:
            Returns json object of dataset updated else error object

        """
        # Operation 1
        try:
            data = json.loads(request.data.decode('utf-8'))
            self.column_names, data_values = get_column_data(
                "",
                data
            )
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            data = {}
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        # Operation 2
        try:
            self.column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_DATASET_ABSTRACT)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 3
        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in self.column_names],
                self.column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        # Operation 4
        where_filds = [['engagementid', '=', engagement_id],
                       ["datasetid", "=", dataset_id]]

        # Operation 5
        query = dynamic_queris_update(
            CLIENT_ENGAGEMENT_DATASET,
            self.column_names,
            corrected_data, where_query(where_filds))
        logger.info(query)

        # Operation 6
        try:
            self.connector.update(query)
        except ValueError as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 7
        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
