#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get Stages details [multiple entry]
        post: create Stages [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result_list,
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT_STAGE_LOOKUP,
    CLIENT_ENGAGEMENT_STAGE_LOOKUP_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.stage_swagger import CreateStages


class EngagementsStages(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        Constructor function to create instance of class.
        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)

        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[],
        notes='List all Stages')
    def get(self):
        """Get Stages List.

        Args:
            No Arguments - bulk result

        Returns:
            json: Returns json object of stage else empty data object

        """
        result = []

        try:
            column_names = [x.lower() for x in self.connector.columnnames(
                CLIENT_ENGAGEMENT_STAGE_LOOKUP_ABSTRACT)]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        between_fields = []
        where_filds = []
        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_STAGE_LOOKUP,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateStages.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[StageId] [int] IDENTITY(1,1) NOT NULL," +
                "[StageName] [varchar](512) NOT NULL,"
            }
        ],
        notes='Create Stages')
    def post(self):
        """Create Stage.

        Args:
            No Arguments

        Returns:
        json: Returns json object of stage created else empty error object

        """
        try:
            data = json.loads(request.data.decode('utf-8'))
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        column_names, data_values = get_column_data(
            "",
            data)

        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_STAGE_LOOKUP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args)
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values, [x.lower() for x in column_names], column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result_list(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            CLIENT_ENGAGEMENT_STAGE_LOOKUP,
            [x.lower() for x in column_names],
            corrected_data,
            [])
        logger.info(query)

        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
