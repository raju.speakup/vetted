#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get Status details by id [single entry]
        delete: delete Status by id [single entry]
        put: update Status details by id [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete
)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_delete import (
    dynamic_queris_delete
)

from constants.constants import (
    CLIENT_ENGAGEMENT_STATUS_LOOKUP,
    CLIENT_ENGAGEMENT_STATUS_LOOKUP_ABSTRACT

)

from queries.dynamic_queris_update import (
    dynamic_queris_update
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    JSON_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING

)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.status_swagger import UpdateStatus


class EngagementsStatus(Resource):
    """EngagementsStatus functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "status_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Status ID"
            }
        ],
        notes='Get status details with status id')
    def get(self, status_id):
        """Get Status.

        Args:
            status_id (int): Status id is unique id associated to status

        Returns:
            json: Returns json object of client else empty data object

        """
        result = []
        try:
            column_names = self.connector.columnnames(
                CLIENT_ENGAGEMENT_STATUS_LOOKUP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        column_names = [x.lower() for x in column_names]

        where_field = [['statusid', '=', status_id]]

        between_fields = []
        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_STATUS_LOOKUP,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_field),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "status_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Status ID"
            }
        ],
        notes='Delete status with status id')
    def delete(self, status_id):
        """Delete Status.

        Args:
            status_id (int): StatusId

        Returns:
            json: Returns json object of client deleted else error object

        """
        result = {}
        where_field = [['statusid', '=', status_id]]

        query = dynamic_queris_delete(
            table_name=CLIENT_ENGAGEMENT_STATUS_LOOKUP,
            where_query=where_query(fields=where_field),
        )
        logger.info(query)

        try:
            self.connector.delete(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result_delete(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "status_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Status ID"
            },
            {
                "name": "parameters",
                "dataType": UpdateStatus.__name__,
                "paramType": "body",
                "required": True,
                "description": "Post parameters"
            }
        ],
        notes='get dataset details by id')
    def put(self, status_id):
        """Update Status.

        Args:
            status_id (int): Status_id

        Returns:
            json: Returns json object of status updated else error object

        """
        data = []
        try:
            data = json.loads(request.data.decode('utf-8'))
            column_names, data_values = get_column_data("", data)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_STATUS_LOOKUP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        where_field = [["statusid", "=", status_id]]
        query = dynamic_queris_update(
            CLIENT_ENGAGEMENT_STATUS_LOOKUP,
            column_names,
            corrected_data, where_query(where_field))
        try:
            self.connector.update(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        )
