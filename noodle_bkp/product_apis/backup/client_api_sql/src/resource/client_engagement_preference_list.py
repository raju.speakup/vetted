#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get preference details [multiple entry]
        post: create preference [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from constants.constants import (
    CLIENT_ENGAGEMENT_PREFERENCE,
    CLIENT_ENGAGEMENT_PREFERENCE_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_DETAIL_OBJECT,
    INVALID_QUERY)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
# from flask import request
# import json
# from collections import OrderedDict
from logger.logger import logger
import pymssql
from config.config import DATABASE_NAME

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
# from swaggerdoc.preference_swagger import CreatePreference


class EngagementsPreferences(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []

    @swagger.operation(
        parameters=[],
        notes='List all preferences')
    def get(self, client_id):
        """Get Preference List.

        Args:
            No Arguments - bulk result

        Returns:
            json: Returns json object of Preference else empty data object

        """
        result = []
        try:
            column_names = [x.lower() for x in self.connector.columnnames(
                CLIENT_ENGAGEMENT_PREFERENCE_ABSTRACT)]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        between_fields = []
        inter_mediate_query = 'select eng.EngagementId from ' + \
            DATABASE_NAME + '..Engagement  eng join ' +\
            DATABASE_NAME + '..client cli on ' +\
            'cli.clientid = eng.clientid where cli.ClientId = {0}'.format(
                client_id)

        try:
            get_engagements = self.connector.execute(inter_mediate_query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        where_filds = []
        if get_engagements[0]:
            for entry in get_engagements[0][0]:
                where_filds.append(['engagementid', '=', entry])
        else:
            return return_result(
                result=result,
                error='Client do not have any Engagement',
                error_code=NO_DETAIL_OBJECT
            ), HTTP_500_INTERNAL_SERVER_ERROR

        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_PREFERENCE,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    # @swagger.operation(
    #     parameters=[
    #         {
    #             "name": "parameters",
    #             "dataType": CreatePreference.__name__,
    #             "paramType": "body",
    #             "required": True,
    #             "description": "Table Design" +
    #             "[EngagementId] [bigint] NOT NULL, " +
    #             "[PreferenceKey] [varchar](64) NOT NULL," +
    #             "[PreferenceValue] [varchar](256) NULL,"
    #         }
    #     ],
    #     notes='Create preferences for engagement')
    # def post(self):
    #     """Create Preference.

    #     Args:
    #         No Arguments

    #     Returns:
    # json: Returns json object of Preference created else empty error object

    #     """
    #     try:
    #         data = json.loads(request.data.decode('utf-8'),
    #                           object_pairs_hook=OrderedDict)

    #         # Adding random preference key to table - No auto generation in db
    #         # We need to modify this section once we implement uuid
    #         from random import randint
    #         preferencekey = randint(0, 100000)
    #         data.update({'preferencekey': preferencekey})
    #     except ValueError as err:
    #         self.error = err.args[0]
    #         self.error_code = NO_VALID_JSON
    #         data = {}
    #         logger.error(err.args[0])
    #         return return_result(
    #             result=data,
    #             error=self.error,
    #             error_code=self.error_code
    #         ), HTTP_400_BAD_REQUEST

    #     column_names, data_values = get_column_data(
    #         "",
    #         data)

    #     try:
    #         column_type = self.connector.column_names_datatype(
    #             CLIENT_ENGAGEMENT_PREFERENCE_ABSTRACT)
    #     except pymssql.Error as err:
    #         self.error = err.args[0]
    #         self.error_code = DATABASE_CONNECTION_ISSUE
    #         logger.error(err.args[0])
    #         return return_result(
    #             result=data,
    #             error=self.error,
    #             error_code=self.error_code
    #         ), HTTP_500_INTERNAL_SERVER_ERROR

    #     corrected_data = correct_data_values(
    #         data_values, [x.lower() for x in column_names], column_type)

    #     query = table_inseration_query(
    #         CLIENT_ENGAGEMENT_PREFERENCE,
    #         [x.lower() for x in column_names],
    #         corrected_data,
    #         [])
    #     logger.info(query)

    #     try:
    #         self.connector.execute_no_return(query)
    #     except pymssql.Error as err:
    #         self.error = err.args[0]
    #         self.error_code = DATABASE_CONNECTION_ISSUE
    #         logger.error(err.args[0])
    #         return return_result(
    #             result=data,
    #             error=self.error,
    #             error_code=self.error_code
    #         ), HTTP_500_INTERNAL_SERVER_ERROR

    #     return return_result(
    #         result=data,
    #         error=self.error,
    #         error_code=self.error_code
    #     ), HTTP_200_OK
