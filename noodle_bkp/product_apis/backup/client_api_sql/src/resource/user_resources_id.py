#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get User details by id [single entry]
        delete: delete User by id [single entry]
        put: update User details by id [single entry]

    .. module:: client_users
    :platform: OS X
    :synopsis: Creating get (multiple users of client)
    and post (single entry) method.

    .. script:: Gururaj Jeerge <gururaj.jeerge@noodle.ai>
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete,
    exclude_column_names,
    exclude_entry_dict
)
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)
from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_delete import (
    dynamic_queris_delete
)

from constants.constants import (
    USER_PROFILE,
    USER_PROFILE_ABSTRACT

)

from queries.dynamic_queris_update import (
    dynamic_queris_update
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    JSON_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.user_swagger import UpdateUser


class User(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        Constructor function to create instance of class.
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "user_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='get user details by id')
    def get(self, client_id, user_id):
        """Get User.

        Function Args:
            client_id (int): Client id is unique key associated
                            client in database
            user_id (int): User id

        Returns:
            Returns json object of User else empty data object

        """
        result = []

        try:
            column_names = exclude_column_names(
                [x.lower()
                 for x in self.connector.columnnames(USER_PROFILE_ABSTRACT)],
                [])
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        where_filds = [['clientid', '=', client_id], ['userid', '=', user_id]]

        between_fields = []
        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=USER_PROFILE,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "user_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "User ID"
            }
        ],
        notes='Delete user details by id')
    def delete(self, client_id, user_id):
        """Delete User.

        Args:
            clientid (int): Client id is unique key associated
                            client in database
            user_id (int): User id

        Returns:
            Returns json object of User deleted else empty error object

        """
        result = {}
        where_filds = [['clientid', '=', client_id],
                       ['userid', '=', str(user_id)]]

        query = dynamic_queris_delete(
            table_name=USER_PROFILE,
            where_query=where_query(fields=where_filds),
        )
        logger.info(query)

        try:
            self.connector.delete(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result_delete(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "user_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "User ID"
            },
            {
                "name": "parameters",
                "dataType": UpdateUser.__name__,
                "paramType": "body",
                "description": "Post Parameters",
                "required": True
            }
        ],
        notes='get dataset details by id')
    def put(self, client_id, user_id):
        """Update User.

        Args:
            clientid (int): Client id is unique key associated
                            client in database
            user_id (int): User id

        Returns:
            Returns json object of User updated else empty error object

        """
        try:
            data = json.loads(request.data.decode('utf-8'))
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            data = {}
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        column_names, data_values = get_column_data(
            "",
            exclude_entry_dict(
                data,
                []
            )
        )

        try:
            column_type = self.connector.column_names_datatype(
                USER_PROFILE_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        where_filds = [["clientid", "=", client_id], ["userid", "=", user_id]]
        query = dynamic_queris_update(
            USER_PROFILE,
            column_names,
            corrected_data, where_query(where_filds))
        logger.info(query)

        try:
            self.connector.update(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
