#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get Engagement details by id [single entry]
        delete: delete Engagement by id [single entry]
        put: update Engagement details by id [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete,
    exclude_entry_dict
)
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)
from queries.dynamic_queris_read import (
    where_query
)

from constants.constants import (
    CLIENT_ENGAGEMENT,
    CLIENT_ENGAGEMENT_ABSTRACT)

from queries.dynamic_queris_update import (
    dynamic_queris_update
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    JSON_ERROR,
    PROGRAMMING_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_409_CONFLICT)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql
from config.config import DATABASE_NAME

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.engagement_swagger import UpdateEngagement


class Engagement(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            }
        ],
        notes='get client details by id')
    def get(self, client_id, engagement_id):
        """Get Engagement.

        Function Args:
            client_id (int): Client id
            engagement_id (int): Engagement id

        Operations:
            1) Define Column names
            2) Hard code query
            3) Execute query through Dbconnector.execute()
            4) Extract required data from result and connect to other API
            5) Update data Json
            6) Convert Result in Json
            7) Return Json and status code

        Returns:
            Json containing all clients engagements else empty Json

        """
        result = []

        column_names = [
            'engagementid',
            'engagementname',
            'description',
            'startdate',
            'enddate',
            'clientname',
            'stagename',
            'statusname'
        ]

        # Multiple table join query is hard coded - Need to make this
        # auto generated
        # Need a generic function to make it auto generate
        # TODO: Sprint 2 will cover this generic function
        query = 'select \
                eng.EngagementId, \
                eng.EngagementName, \
                eng.Description,\
                eng.StartDate, \
                eng.EndDate, \
                cli.ClientName, \
                cli.ClientId, \
                stg.StageName, \
                stat.StatusName from ' +\
                DATABASE_NAME + '..Engagement eng join ' +\
                DATABASE_NAME + '..client cli on ' +\
                'cli.clientid = eng.clientid join ' +\
                DATABASE_NAME + '..EngagementStageLookup stg on ' +\
                'stg.StageId = eng.StageId join ' +\
                DATABASE_NAME + '..EngagementStatusLookup stat on ' +\
                'stat.StatusId = eng.StatusId where ' +\
                'cli.ClientId = {0} and eng.EngagementId={1}'.format(
                    client_id, engagement_id)
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        from .client_engagement_noodleapp_id import EngagementsNoodleapp
        from .client_noodleapp_id import Noodleapp
        engagement_join = EngagementsNoodleapp()
        noodle_app = Noodleapp()

        try:
            for count, entry in enumerate(result):
                engagement_jd = engagement_join.get(
                    str(entry.get('engagementid'))).get('data')
                for inner_count, inner_entry in enumerate(engagement_jd):
                    noodle_app_desc = noodle_app.get(
                        str(inner_entry.get('appid')))
                    inner_entry.update(
                        {'appdetails': noodle_app_desc.get('data')})
                result[count].update(
                    {'engagementjoint': engagement_jd})
        except Exception as e:
            self.error = e.args[0]
            self.error_code = PROGRAMMING_ERROR
            logger.error(e.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_409_CONFLICT

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            }
        ],
        notes='get client details by id')
    def delete(self, client_id, engagement_id):
        """Delete Engagement.

        Args:
            client_id (int): Client id is unique key associated
                            client in database
            engagement_id (int): engagement id

        Operations:
            1) Write conditional clauses
            2) Generate Query
            3) Execute query through Dbconnector.execute()
            4) Return Json and status code

        Returns:
            Returns json object of engagement deleted else error object

        """
        result = {}

        # Operation 2
        query1 = "delete from  " +\
            DATABASE_NAME + "..EngagementPreference " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId ={0} and EngagementId={1})".format(
                client_id, engagement_id)

        query2 = "delete from " +\
            DATABASE_NAME + "..EngagementDocument " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId={0} and EngagementId={1})".format(
                client_id, engagement_id)

        query3 = "delete from " +\
            DATABASE_NAME + "..EngagementAppDataset " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId={0} and EngagementId={1})".format(
                client_id, engagement_id)

        query4 = "delete from " +\
            DATABASE_NAME + "..EngagementApp " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId={0} and EngagementId={1})".format(
                client_id, engagement_id)

        query5 = "delete from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId = {0} and EngagementId = {1}".format(
                client_id, engagement_id)

        logger.info(query1)
        logger.info(query2)
        logger.info(query3)
        logger.info(query4)
        logger.info(query5)

        # Operation 3
        try:
            self.connector.delete(query1)
            self.connector.delete(query2)
            self.connector.delete(query3)
            self.connector.delete(query4)
            self.connector.delete(query5)

        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result_delete(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "parameters",
                "dataType": UpdateEngagement.__name__,
                "paramType": "body",
                "required": True,
                "description": "Post parameters"
            }
        ],
        notes='get dataset details by id')
    def put(self, client_id, engagement_id):
        """Update Engagement.

        Args:
            client_id (int): Client id is unique key associated
                            client in database
            engagement_id: Engagement id

        Operations:
            1) Get Data in Json format from request.data and convert it to
            data dictionary
            2) Get Column names of table from Dbconnector.columnnames()
            3) Generate Correct Data
            4) Generate Conditional operators
            5) Generate Query
            6) Execute query through Dbconnector.execute_no_return()
            7) Convert Result in Json
            8) Return Json and status code

        Returns:
            Returns json object of engagement updated else error object

        """
        # Operation 1
        try:
            data = json.loads(request.data.decode('utf-8'))
            data = exclude_entry_dict(data, [])
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            data = {}
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        self.column_names, data_values = get_column_data("", data)
        self.column_names = [x.lower() for x in self.column_names]

        # Operation 2
        try:
            self.column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 3
        try:
            corrected_data = correct_data_values(
                data_values,
                self.column_names,
                self.column_type
            )
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        # Operation 4
        where_filds = [["engagementid", "=", engagement_id],
                       ['clientid', '=', client_id]]

        # Operation 5
        query = dynamic_queris_update(
            CLIENT_ENGAGEMENT,
            self.column_names,
            corrected_data, where_query(where_filds))
        logger.info(query)

        # Operation 6
        try:
            self.connector.update(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 7 & 8
        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
