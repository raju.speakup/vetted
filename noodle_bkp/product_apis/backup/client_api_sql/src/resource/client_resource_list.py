#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get client details [multiple entry]
        post: create client [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_PROFILE,
    CLIENT_PROFILE_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.client_swagger import CreateClient

# ------------------------------------------------------------------------------
# EXTERNAL APIs
# ------------------------------------------------------------------------------
from .user_resource_list import Users
from .client_engagement_list import Engagements


class Clients(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "full OR basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Select Options - " +
                "clientid, clientname," +
                "addressline1, addressline2, city, statecode," +
                "zip, contactname, contactemail, contactphone," +
                "onboardedby, onboardedon, countrycode," +
                "clientshortname, preferreddateformat, timezone"
            }
        ],
        notes='List all Clients of MVP',
        nickname='Client')
    def get(self):
        """List Clients.

        Program Args:
            Function do not accept any argument.

        URLParameters:
            limit
            offset
            projection
            sort

        Operations:
            1) Get all URL parameters from get_url_args() - Pass request object
            2) Get Column names of table from Dbconnector.columnnames()
            3) Write conditional clauses
            4) Generate Query
            5) Execute query through Dbconnector.execute()
            6) Extract required data from result and connect to other API
            7) Update data Json
            8) Convert Result in Json
            9) Return Json and status code

        Returns:
            Json containing all clients of MVP else empty Json

        """
        result = []

        # Operation 1
        limit, offset, sort_key, projection = get_url_args(req=request)
        sort_key = ['clientid'] if not sort_key else sort_key

        # Operation 2
        try:
            self.column_names = exclude_column_names(
                self.connector.columnnames(CLIENT_PROFILE_ABSTRACT),
                [])
            self.column_names = [x.lower() for x in self.column_names]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 3
        between_fields = []
        where_filds = []

        # Operation 4
        query = dynamic_queris_read(
            table_name=CLIENT_PROFILE,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=self.column_names,
            sorting=get_sorting_dict(fields=sort_key),
            limit=limit,
            offset=offset
        )
        logger.info(query)

        # Operation 5
        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 6
        user_details = Users()
        engagement_details = Engagements()
        result = convert_to_json(
            rows=db_result[0],
            column_names=self.column_names
        ) if db_result else []

        # Operation 7
        for count, entry in enumerate(result):
            user_data = user_details.get(str(entry.get('clientid')))[0]
            result[count].update({'users': user_data.get('data')})

        for count, entry in enumerate(result):
            engagement_data = engagement_details.get(
                str(entry.get('clientid')))[0]
            result[count].update({'engagement': engagement_data.get('data')})

        # Operation 8 & 9
        return return_result_list(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateClient.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[ClientId] [bigint] IDENTITY(1,1) NOT NULL," +
                "[ClientName] [varchar](256) NOT NULL," +
                "[Addressline1] [varchar](1024) NULL," +
                "[Addressline2] [varchar](1024) NULL," +
                "[City] [varchar](256) NULL," +
                "[StateCode] [varchar](8) NULL," +
                "[Zip] [varchar](8) NULL," +
                "[ContactName] [varchar](512) NULL," +
                "[ContactEmail] [varchar](512) NULL," +
                "[ContactPhone] [varchar](16) NULL," +
                "[OnboardedBy] [varchar](32) NULL," +
                "[OnboardedOn] [datetime] NULL," +
                "[CountryCode] [varchar](3) NULL," +
                "[ClientShortName] [varchar](8) NULL," +
                "[PreferredDateFormat] [varchar](16) NULL," +
                "[TimeZone] [varchar](32) NULL"
            },
        ],
        notes='Post Method to Create Client',
        nickname='Client')
    def post(self):
        """Create Client.

        Acceptable Structure:
            {
              "addressline1": "",
              "city": "",
              "zip": "",
              "clientshortname": "",
              "timezone": "",
              "statecode": "",
              "onboardedby": "",
              "contactemail": "",
              "contactphone": "",
              "addressline2": "",
              "preferreddateformat": "",
              "clientname": "",
              "contactname": "",
              "countrycode": ""
            }

        Program Args:
            No Arguments accepted in function

        Operations:
            1) Get Data in Json format from request.data and convert it to
            data dictionary
            2) Get Column names of table from Dbconnector.columnnames()
            3) Generate Query
            4) Execute query through Dbconnector.execute_no_return()
            5) Convert Result in Json
            6) Return Json and status code

        Returns:
            Returns json object of client created else error object

        """
        try:
            data = json.loads(request.data.decode('utf-8'))
            column_names, data_values = get_column_data(
                "clientid",
                data)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result_list(
                result=data,
                error=self.error,
                error_code=self.error_code), HTTP_400_BAD_REQUEST

        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_PROFILE_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR
        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            CLIENT_PROFILE,
            [x.lower() for x in column_names],
            corrected_data,
            [])
        logger.info(query)

        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
