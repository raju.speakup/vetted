#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask_restful_swagger import swagger
from services.db_service import Dbconnector
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
    DATABASE_SCHEMA,
    DEFAULT_IP,
    LOG_FILE_PATH
)
import pymssql
from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK)


class Health(Resource):

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[],
        notes='Get health of system',
        nickname='Health')
    def get(self):
        """Check health of system."""
        result = {}
        result.update({
            'serverName': SERVER_NAME,
            'databaseName': DB_NAME,
            'userName': USERNAME,
            'databaseSchema': DATABASE_SCHEMA,
            'dafaultIP': DEFAULT_IP,
            'logStorePath': LOG_FILE_PATH
        })

        try:
            self.connector.connect()
            result.update({
                'databaseUp': 'okay'})
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = DATABASE_CONNECTION_ISSUE
            result.update({
                'databaseUp': 'Not Okay',
                'error': err.args})
            return (result), HTTP_500_INTERNAL_SERVER_ERROR
        return (result), HTTP_200_OK
