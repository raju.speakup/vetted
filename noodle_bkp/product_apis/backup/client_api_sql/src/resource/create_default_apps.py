#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        post: create default app [single entry]

    .. module:: client_users
        :platform: OS X
        :synopsis: Creating get (multiple users of client)
        and post (single entry) method.

    .. script:: Gururaj Jeerge <gururaj.jeerge@noodle.ai>
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    return_result_list,
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT_APP,
    CLIENT_ENGAGEMENT_APP_ABSTRACT

)

from constants.error_constants import (
    JSON_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.create_apps import create_apps

from constants.return_error_constants import (
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.create_default_apps import CreateDefaultApps


class DefaultApps(Resource):
    """UsersPost class.

    Args:
        None

    .. note:: UsersPost will contain only post method with No arguments

    Returns:
        Function specific results
    """

    def __init__(self):
        """Init.

        Constructor function to create instance of class.
        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateDefaultApps.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Create a user for a client',
        nickname='Create User')
    def post(self):
        """Create User.

        Acceptable Structure:
            {
              "engagementid": ""
            }

        Program Args:
            No Arguments accepted in function

        Operations:
            1) Get Data in Json format from request.data and convert it to
            data dictionary
            2) Get Column names of table from Dbconnector.columnnames()
            3) Generate Query
            4) Execute query through Dbconnector.execute_no_return()
            5) Convert Result in Json
            6) Return Json and status code

        Returns:
            Returns json object of user created else error object

        """
        # Operation 1, 2
        data_master = []
        try:
            data = json.loads(request.data.decode('utf-8'))
            self.column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_APP_ABSTRACT)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            logger.error(err.args[0])
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code), HTTP_400_BAD_REQUEST

        # Operation 3
        for item in create_apps:
            data.update(item)
            data_master.append(data)
            self.column_names, data_values = get_column_data("", data)
            print(data_values)
            try:
                corrected_data = correct_data_values(
                    data_values,
                    [x.lower() for x in self.column_names],
                    self.column_type)
            except ValueError as err:
                self.error = err
                self.error_code = INVALID_OR_FIELD_MISSING
                data = {}
                logger.error(err.args)
                return return_result(
                    result=data,
                    error=self.error,
                    error_code=self.error_code
                ), HTTP_400_BAD_REQUEST

            query = table_inseration_query(
                CLIENT_ENGAGEMENT_APP,
                self.column_names,
                corrected_data,
                [])
            logger.info(query)

            # Operation 4
            try:
                self.connector.execute_no_return(query)
            except pymssql.Error as err:
                self.error = err.args[0]
                self.error_code = INVALID_QUERY
                logger.error(err.args[0])
                return return_result_list(
                    result={},
                    error=self.error,
                    error_code=self.error_code)

        return return_result(
            result=data_master,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
