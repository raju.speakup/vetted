#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get client details by id [single entry]
        delete: delete client by id [single entry]
        put: update client details by id [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete,
    exclude_column_names,
    exclude_entry_dict
)
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)
from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from constants.constants import (
    CLIENT_PROFILE,
    CLIENT_PROFILE_ABSTRACT)

from queries.dynamic_queris_update import (
    dynamic_queris_update)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    JSON_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql
from config.config import DATABASE_NAME

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.client_swagger import UpdateClient


class Client(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True
            }
        ],
        notes='get client details by id',
        nickname='Client')
    def get(self, client_id):
        """Get Client.

        Program Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
            Returns json object of client else empty data object

        """
        result = []

        try:
            column_names = exclude_column_names(
                self.connector.columnnames(CLIENT_PROFILE_ABSTRACT),
                [])
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        column_names = [x.lower() for x in column_names]

        between_fields = []
        where_filds = [['clientid', '=', client_id]]
        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=CLIENT_PROFILE,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
            result = convert_to_json(
                rows=db_result[0],
                column_names=column_names
            )
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        from .user_resource_list import Users
        from .client_engagement_list import Engagements
        user_details = Users()
        engagement_details = Engagements()

        for count, entry in enumerate(result):
            user_data = user_details.get(str(entry.get('clientid')))[0]
            result[count].update({'users': user_data.get('data')})

        for count, entry in enumerate(result):
            engagement_data = engagement_details.get(
                str(entry.get('clientid')))[0]
            result[count].update({'engagement': engagement_data.get('data')})

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True
            }
        ],
        notes='Delete Client',
        nickname='Client')
    def delete(self, client_id):
        """Delete Client.

        Program Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
           Returns json object of client deleted else error

        """
        result = {}

        query1 = "delete from " +\
            DATABASE_NAME + "..EngagementPreference" +\
            " where EngagementId in (select EngagementId" +\
            " from " +\
            DATABASE_NAME + "..Engagement" + \
            " where ClientId={0})".format(client_id)

        query2 = "delete from " +\
            DATABASE_NAME + "..EngagementDocument " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId={0})".format(client_id)

        query3 = "delete from " +\
            DATABASE_NAME + "..EngagementAppDataset " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId={0})".format(client_id)

        query4 = "delete from " +\
            DATABASE_NAME + "..EngagementApp " +\
            "where EngagementId in " +\
            "(select EngagementId from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId={0})".format(client_id)

        query5 = "delete from " +\
            DATABASE_NAME + "..Engagement " +\
            "where ClientId = {0}".format(
                client_id)
        query6 = "delete from " +\
            DATABASE_NAME + "..ClientUser " +\
            "where ClientId = {0}".format(
                client_id)
        query7 = "delete from " +\
            DATABASE_NAME + "..ClientUser " +\
            "where ClientId = {0}".format(
                client_id)
        query8 = "delete from " +\
            DATABASE_NAME + "..Client " +\
            "where ClientId = {0}".format(
                client_id)

        logger.info(query1)
        logger.info(query2)
        logger.info(query3)
        logger.info(query4)
        logger.info(query5)
        logger.info(query6)
        logger.info(query7)
        logger.info(query8)

        try:
            self.connector.delete(query1)
            self.connector.delete(query2)
            self.connector.delete(query3)
            self.connector.delete(query4)
            self.connector.delete(query5)
            self.connector.delete(query6)
            self.connector.delete(query7)
            self.connector.delete(query8)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_204_NO_CONTENT

        return return_result_delete(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True
            },
            {
                "name": "parameters",
                "dataType": UpdateClient.__name__,
                "paramType": "body",
                "required": True
            }
        ],
        notes='Update client information with client id',
        nickname='Client')
    def put(self, client_id):
        """Update Client.

        Acceptable Structure:
            {
              "name": "",
              "contactname": "",
              "addressline1": "",
              "addressline2": "",
              "clientname": "",
              "zip": "",
              "contactemail": "",
              "statecode": "",
              "contactphone": "",
              "city": ""
            }

        Program Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
            json: Returns json object of client updated else error object

        """
        data = []
        try:
            data = json.loads(request.data.decode('utf-8'))
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST
        data = exclude_entry_dict(data, [])

        column_names, data_values = get_column_data("", data)
        column_names = [x.lower() for x in column_names]
        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_PROFILE_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values, column_names, column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        where_filds = [["clientid", "=", client_id]]
        query = dynamic_queris_update(
            CLIENT_PROFILE,
            column_names,
            corrected_data, where_query(where_filds))
        logger.info(query)

        try:
            self.connector.update(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
