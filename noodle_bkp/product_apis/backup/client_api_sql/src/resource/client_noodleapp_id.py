#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get Noodleapp details by id [single entry]
        delete: delete Noodleapp by id [single entry]
        put: update Noodleapp details by id [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete
)
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)
from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_delete import (
    dynamic_queris_delete
)

from constants.constants import (
    CLIENT_ENGAGEMENT_NOODLE_APP,
    CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT

)

from queries.dynamic_queris_update import (
    dynamic_queris_update
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json


class Noodleapp(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)

    @swagger.operation(
        parameters=[
            {
                "name": "app_id",
                "dataType": 'int',
                "paramType": "path"
            }
        ],
        notes='Get app details with app id')
    def get(self, app_id):
        """Get method to get client information from id.

        Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
            json: Returns json object of client else empty data object

        """
        result = []
        column_names = [x.lower() for x in self.connector.columnnames(
            CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT)]

        where_filds = [["appid", "=", str(app_id)]]
        between_fields = []
        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_NOODLE_APP,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )

        db_result = self.connector.execute(query)

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result, error=None, error_code=None)

    @swagger.operation(
        parameters=[
            {
                "name": "app_id",
                "dataType": 'int',
                "paramType": "path"
            }
        ],
        notes='Delete app entry with app id')
    def delete(self, app_id):
        """Delete method to delete client.

        Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
            json: Returns json object of client deleted else empty error object

        """
        result = {}
        where_filds = [["appid", "=", str(app_id)]]
        query = dynamic_queris_delete(
            table_name=CLIENT_ENGAGEMENT_NOODLE_APP,
            where_query=where_query(fields=where_filds),
        )

        self.connector.delete(query)

        return return_result_delete(
            result=result, error=None, error_code=None)

    @swagger.operation(
        parameters=[
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path"
            },
            {
                "name": "parameters",
                "dataType": "string",
                "paramType": "body"
            }
        ],
        notes='Update app details')
    def put(self, app_id):
        """Put method to update client.

        Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
            json: Returns json object of client updated else empty error object

        """
        column_names, data_values = get_column_data(
            "",
            json.loads(request.data.decode('utf-8'))
        )

        column_type = self.connector.column_names_datatype(
            CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT)
        
        corrected_data = correct_data_values(
            data_values,
            [x.lower() for x in column_names],
            column_type)

        where_filds = [["appid", "=", str(app_id)]]
        query = dynamic_queris_update(
            CLIENT_ENGAGEMENT_NOODLE_APP,
            column_names,
            corrected_data, where_query(where_filds))

        self.connector.update(query)

        return return_result(
            result=json.loads(request.data.decode('utf-8')),
            error=None,
            error_code=None
        )
