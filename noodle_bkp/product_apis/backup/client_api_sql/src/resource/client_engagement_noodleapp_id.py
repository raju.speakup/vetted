#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get NoodleApp details by id [single entry]
        delete: delete NoodleApp by id [single entry]
        put: update NoodleApp details by id [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    return_result_delete
)
from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)
from queries.dynamic_queris_read import (
    where_query,
    dynamic_queris_read,
    get_sorting_dict,
    between_query
)

from queries.dynamic_queris_delete import (
    dynamic_queris_delete
)

from constants.constants import (
    CLIENT_ENGAGEMENT_APP,
    CLIENT_ENGAGEMENT_APP_ABSTRACT,
    CLIENT_ENGAGEMENT_NOODLE_APP,
    CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT,
    CLIENT_ENGAGEMENT_DATASET

)

from queries.dynamic_queris_update import (
    dynamic_queris_update
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)


# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.noodleapp_swagger import UpdateApp


class EngagementsNoodleapp(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        Constructor function to create instance of class.
        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    def get(self, engagement_id):
        """Get method to get client information from id.

        Args:
            clientid (int): Client id is unique key associated
                            client in database

        Returns:
            json: Returns json object of client else empty data object

        """
        result = []
        try:
            column_names = [x.lower() for x in self.connector.columnnames(
                CLIENT_ENGAGEMENT_APP_ABSTRACT)]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        where_filds = [["engagementid", "=", engagement_id]]
        between_fields = []
        sorting_filed = {}

        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_APP,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        )

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "app_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            }
        ],
        notes='Delete app entry for any engagement')
    def delete(self, client_id, engagement_id, app_id):
        """Delete Engagement App.

        Args:
            client_id (int): client_id
            engagement_id (int): engagement_id
            app_id (int): app_id

        Returns:
            json: Returns json object of Noodle App deleted
            else empty error object

        """
        result = {}
        where_filds = [["appid", "=", app_id],
                       ["engagementid", "=", engagement_id]]

        query = dynamic_queris_delete(
            table_name=CLIENT_ENGAGEMENT_DATASET,
            where_query=where_query(fields=where_filds),
        )
        logger.info(query)
        try:
            self.connector.delete(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        where_filds = [["engagementid", "=", engagement_id],
                       ["appid", "=", app_id]]

        query = dynamic_queris_delete(
            table_name=CLIENT_ENGAGEMENT_APP,
            where_query=where_query(fields=where_filds),
        )
        logger.info(query)
        try:
            self.connector.delete(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result_delete(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "app_id",
                "dataType": "int",
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "parameters",
                "dataType": UpdateApp.__name__,
                "paramType": "body",
                "required": True,
                "description": "Post parameters"
            }
        ],
        notes='Update details of noodle app with engagement id and app id')
    def put(self, client_id, engagement_id, app_id):
        """Update Engagement App.

        Args:
            client_id (int): client_id
            engagement_id (int): engagement_id
            app_id (int): app_id
        Returns:
            json: Returns json object of Noodle APP updated else empty
            error object

        """
        try:
            data = json.loads(request.data.decode('utf-8'))
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        column_names, data_values = get_column_data(
            "",
            data
        )
        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_APP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        where_filds = [["engagementid", "=", engagement_id],
                       ["appid", "=", app_id]]

        query = dynamic_queris_update(
            CLIENT_ENGAGEMENT_APP,
            column_names,
            corrected_data,
            where_query(where_filds)
        )
        logger.info(query)

        try:
            self.connector.update(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Second table
        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        corrected_data = correct_data_values(
            data_values,
            [x.lower() for x in column_names],
            column_type)

        where_filds = [["appid", "=", app_id]]

        query = dynamic_queris_update(
            CLIENT_ENGAGEMENT_NOODLE_APP,
            column_names,
            corrected_data,
            where_query(where_filds)
        )
        logger.info(query)

        try:
            self.connector.update(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
