#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get Noodleapp details [multiple entry]
        post: create Noodleapp [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT_NOODLE_APP,
    CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql


class Noodleapps(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self, app_id):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()
        self.app_id = app_id

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "required": True
            }
        ],
        notes='List all Noodle Apps')
    def post(self):
        """Create NoodleApp.

        Args:
            No Arguments

        Returns:
        json: Returns json object of client created else empty error object

        """
        try:
            data = json.loads(request.data.decode('utf-8'))
            data.update({'appid': self.app_id})
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_NOODLE_APP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        temp_column_names = [x[0] for x in column_type]
        data = {k: v for k, v in data.items() if k in temp_column_names}

        column_names, data_values = get_column_data("", data)

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type
            )
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            CLIENT_ENGAGEMENT_NOODLE_APP,
            [x.lower() for x in column_names],
            corrected_data,
            []
        )
        logger.info(query)

        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
