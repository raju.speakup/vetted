#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get User details [multiple entry]
        post: create User [single entry]

    .. module:: client_users
        :platform: OS X
        :synopsis: Creating get (multiple users of client)
        and post (single entry) method.

    .. script:: Gururaj Jeerge <gururaj.jeerge@noodle.ai>
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result,
    exclude_column_names)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    USER_PROFILE,
    USER_PROFILE_ABSTRACT

)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    JSON_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.user_swagger import CreateUser


class Users(Resource):
    """Users class.

    Args:
        None

    .. note:: Users will contain only get method with No arguments

    Returns:
        Function specific results
    """

    def __init__(self):
        """Init.

        Constructor function to create instance of class.
        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },

            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "full OR basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Select Options - " +
                "clientid,userid,username," +
                "contactname,contactemail,contactphone," +
                "useraddress,createdon,createdby"
            }
        ],
        notes='List all Users associated to client',
        nickname='List User')
    def get(self, client_id):
        """List Users.

        Program Args:
            client_id (int): Client_id to get users associated to client.

        URLParameters:
            limit
            offset
            projection
            sort

        Operations:
            1) Get all URL parameters from get_url_args() - Pass request object
            2) Get Column names of table from Dbconnector.columnnames()
            3) Write conditional clauses
            4) Generate Query
            5) Execute query through Dbconnector.execute()
            6) Convert Result in Json
            7) Return Json and status code

        Returns:
            Json containing all users associated to
            client of MVP else empty Json

        """
        result = []

        # Operation 1
        limit, offset, sort_key, projection = get_url_args(req=request)
        sort_key = ['clientid'] if not sort_key else sort_key

        # Operation 2
        try:
            self.column_names = exclude_column_names(
                [x.lower() for x in self.connector.columnnames(
                    USER_PROFILE_ABSTRACT)],
                [])
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 3
        between_fields = []
        where_filds = [["clientid", "=", client_id]]

        # Operation 4
        query = dynamic_queris_read(
            table_name=USER_PROFILE,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=self.column_names,
            sorting=get_sorting_dict(fields=sort_key),
            limit=limit,
            offset=offset
        )
        logger.info(query)

        # Operation 5
        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 6
        result = convert_to_json(
            rows=db_result[0],
            column_names=self.column_names
        ) if db_result else result

        # Operation 7
        return return_result_list(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK


class UsersPost(Resource):
    """UsersPost class.

    Args:
        None

    .. note:: UsersPost will contain only post method with No arguments

    Returns:
        Function specific results
    """

    def __init__(self):
        """Init.

        Constructor function to create instance of class.
        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateUser.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[ClientId] [bigint] NOT NULL,"
                "[UserId] [varchar](256) NOT NULL," +
                "[UserName] [varchar](512) NULL," +
                "[ContactName] [varchar](512) NULL," +
                "[ContactEmail] [varchar](512) NULL," +
                "[ContactPhone] [varchar](16) NULL," +
                "[UserAddress] [varchar](1024) NULL," +
                "[CreatedOn] [datetime] NULL," +
                "[CreatedBy] [varchar](256) NULL,"
            }
        ],
        notes='Create a user for a client',
        nickname='Create User')
    def post(self):
        """Create User.

        Acceptable Structure:
            {
              "contactname": "",
              "contactemail": "",
              "createdby": "",
              "contactphone": "",
              "username": "",
              "useraddress": "",
              "clientid": ""
            }

        Program Args:
            No Arguments accepted in function

        Operations:
            1) Get Data in Json format from request.data and convert it to
            data dictionary
            2) Get Column names of table from Dbconnector.columnnames()
            3) Generate Query
            4) Execute query through Dbconnector.execute_no_return()
            5) Convert Result in Json
            6) Return Json and status code

        Returns:
            Returns json object of user created else error object

        """
        # Operation 1, 2
        data = []
        try:
            data = json.loads(request.data.decode('utf-8'))
            # Adding random preference key to table - No auto generation in db
            # We need to modify this section once we implement uuid
            from random import randint
            userid = randint(0, 100000)
            data.update({'userid': userid})
            self.column_names, data_values = get_column_data("", data)
            self.column_type = self.connector.column_names_datatype(
                USER_PROFILE_ABSTRACT)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = JSON_ERROR
            logger.error(err.args[0])
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code), HTTP_400_BAD_REQUEST

        # Operation 3
        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in self.column_names],
                self.column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            USER_PROFILE,
            self.column_names,
            corrected_data,
            [])
        logger.info(query)

        # Operation 4
        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = INVALID_QUERY
            logger.error(err.args[0])
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code)

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
