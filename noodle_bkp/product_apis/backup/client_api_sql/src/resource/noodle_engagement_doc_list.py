#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get client details [multiple entry]
        post: create client [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT_DOC,
    CLIENT_ENGAGEMENT_DOC_ABSTRACT,
    CLIENT_ENGAGEMENT_DOC_V,
    CLIENT_ENGAGEMENT_DOC_V_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
from collections import OrderedDict
import pymssql

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.document_swagger import CreateDocument


class EngagementsDocs(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": 'path',
                "description": "Client ID",
                "required": True
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": 'path',
                "description": "Engagement ID",
                "required": True
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "full OR basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "required": True,
                "description": "Sorting Parameters" +
                "engagementid, documenname, owner"
            }
        ],
        notes='List all docs')
    def get(self, client_id, engagement_id):
        """List Docs.

        Args:
            client_id (int): client_id
            engagement_id (int): engagement_id

        Returns:
            json: Returns json object of docs else empty data object

        """
        result = []

        try:
            column_names = [x.lower() for x in self.connector.columnnames(
                CLIENT_ENGAGEMENT_DOC_V_ABSTRACT)]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        between_fields = []
        where_fields = [["engagementid", "=", engagement_id],
                        ['clientid', '=', client_id]]
        sorting_fields = {}

        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_DOC_V,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_fields),
            column_names=column_names,
            sorting=get_sorting_dict(fields=sorting_fields)
        )
        logger.info(query)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = INVALID_QUERY
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK


class EngagementsDocsPost(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateDocument.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[EngagementId] [bigint] NOT NULL," +
                "[DocumenName] [varchar](256) NOT NULL," +
                "[DocumentPath] [varchar](512) NOT NULL," +
                "[Owner] [varchar](256) NULL," +
                "[CreatedOn] [datetime] NULL," +
                "[ModifiedOn] [datetime] NULL,"

            }
        ],
        notes='Create doc entry')
    def post(self):
        """Create Document.

        Function Args:
            No Arguments

        Acceptable Structure:
            {
              "owner": "",
              "documenname": "",
              "modifiedon": "",
              "documentpath": "",
              "engagementid": "",
              "createdon": ""
            }

        Returns:
            json object of created doc else error.

        """
        try:
            data = json.loads(request.data.decode('utf-8'),
                              object_pairs_hook=OrderedDict)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        column_names, data_values = get_column_data(
            "",
            data)
        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_DOC_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type
            )
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            CLIENT_ENGAGEMENT_DOC,
            [x.lower() for x in column_names],
            corrected_data,
            [])
        logger.info(query)

        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
