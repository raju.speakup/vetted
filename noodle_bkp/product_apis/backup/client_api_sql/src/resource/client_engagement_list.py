#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get Engagement details [multiple entry]
        post: create Engagement [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args,
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT,
    CLIENT_ENGAGEMENT_ABSTRACT
)

from queries.dynamic_queris_joint_read import (
    create_featured_filters
)

from queries.sorting_mapper import (
    engagement_mapper
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    PROGRAMMING_ERROR,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_409_CONFLICT)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
import pymssql
from config.config import DATABASE_NAME

# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.engagement_swagger import CreateEngagement


class Engagements(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "full OR basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Select Options - " +
                "clientid, engagementid, engagementname," +
                "startdate, enddate, clientname, clientid, " +
                "stagename, statusname",
            }
        ],
        notes='List Engagments of client')
    def get(self, client_id):
        """List Engagements.

        Function Args:
            client_id (int): Client id

        URLParameters:
            limit
            offset
            projection
            sort

        Operations:
            1) Define Column names
            2) Hard code query
            3) Execute query through Dbconnector.execute()
            4) Extract required data from result and connect to other API
            5) Update data Json
            6) Convert Result in Json
            7) Return Json and status code

        Returns:
            Json containing all clients engagements else empty Json
        """
        result = []

        limit, offset, sort_key, projection = get_url_args(req=request)

        column_names = [
            'engagementid',
            'engagementname',
            'description',
            'startdate',
            'enddate',
            'clientname',
            'clientid'
            'stagename',
            'statusname'
        ]

        # Multiple table join query is hard coded - Need to make this
        # auto generated
        # Need a generic function to make it auto generate
        # TODO: Sprint 2 will cover this generic function
        query = 'select \
                eng.EngagementId, \
                eng.EngagementName, \
                eng.Description, \
                eng.StartDate, \
                eng.EndDate, \
                cli.ClientName, \
                cli.ClientId,  \
                stg.StageName, \
                stat.StatusName from ' +\
                DATABASE_NAME + '..Engagement eng join ' + \
                DATABASE_NAME + '..client ' + \
                'cli on cli.clientid = eng.clientid join ' + \
                DATABASE_NAME + '..EngagementStageLookup stg on ' +\
                'stg.StageId = eng.StageId join ' +\
                DATABASE_NAME + '..EngagementStatusLookup stat on ' + \
                'stat.StatusId = eng.StatusId where cli.ClientId = {0}'.format(
                    client_id)

        logger.info(query)

        query = create_featured_filters(
            query, engagement_mapper, sort_key, limit, offset)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = INVALID_QUERY
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if db_result:
            result = convert_to_json(
                rows=db_result[0],
                column_names=column_names
            )
        from .client_engagement_noodleapp_id import EngagementsNoodleapp
        from .client_noodleapp_id import Noodleapp
        engagement_join = EngagementsNoodleapp()
        noodle_app = Noodleapp()
        try:
            for count, entry in enumerate(result):
                engagement_jd = engagement_join.get(
                    str(entry.get('engagementid'))).get('data')
                for inner_count, inner_entry in enumerate(engagement_jd):
                    noodle_app_desc = noodle_app.get(
                        str(inner_entry.get('appid')))
                    inner_entry.update(
                        {'appdetails': noodle_app_desc.get('data')})
                result[count].update(
                    {'engagementjoint': engagement_jd})
        except Exception as e:
            self.error = e.args[0]
            self.error_code = PROGRAMMING_ERROR
            logger.error(e.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_409_CONFLICT

        return return_result_list(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK


class EngagementsPost(Resource):
    """Post Engagement.

    Separate class for post - URL do not accept any parameters
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateEngagement.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[EngagementName] [varchar](256) NOT NULL," +
                "[ClientId][bigint] NOT NULL," +
                "[StageId][int] NOT NULL," +
                "[StatusId][int] NOT NULL," +
                "[StartDate][datetime] NOT NULL," +
                "[EndDate][datetime] NULL"
            },
        ],
        notes='get dataset details by id')
    def post(self):
        """Create Engagement.

        Function Args:
            No arguments

        Acceptable Structure:
            {
              "clientid": "",
              "statusid": "",
              "stageid": "",
              "startdate": "",
              "enddate": "",
              "engagementname": ""
            }

        Operations:
            1) Get Data in Json format from request.data and convert it to
            data dictionary
            2) Get Column names of table from Dbconnector.columnnames()
            3) Generate Correct Data
            4) Generate Query
            5) Execute query through Dbconnector.execute_no_return()
            6) Return Json and status code

        Returns:
            Json containing all created engagements else + error Json

        """
        # Operation 1
        try:
            data = json.loads(request.data.decode('utf-8'))
            self.column_names, data_values = get_column_data(
                "", data)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result_list(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        self.column_names = [x.lower() for x in self.column_names]

        # Operation 2
        try:
            self.column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args)
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 3
        try:
            corrected_data = correct_data_values(
                data_values,
                self.column_names,
                self.column_type
            )
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result_list(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        # Operation 4
        query = table_inseration_query(
            CLIENT_ENGAGEMENT,
            self.column_names,
            corrected_data,
            [])
        logger.info(query)

        # Operation 5
        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result_list(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 6
        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
