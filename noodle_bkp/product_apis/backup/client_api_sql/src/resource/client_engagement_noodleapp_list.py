#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get NoodleApp details [multiple entry]
        post: create NoodleApp [single entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT_APP,
    CLIENT_ENGAGEMENT_APP_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from collections import OrderedDict
from logger.logger import logger
import pymssql
from .client_noodleapp_list import Noodleapps
from config.config import DATABASE_SCHEMA
# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.noodleapp_swagger import CreateApp


class EngagementsNoodleapps(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": 'path',
                "description": "Client ID",
                "required": True
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": 'path',
                "description": "Engagement ID",
                "required": True
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "full OR basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
            }
        ],
        notes='List all Noodle Apps associated to engagement')
    def get(self, client_id, engagement_id):
        """List Engagement App.

        Function Args:
            No Arguments - bulk result

        Returns:
            json: Returns json object of apps else empty data object

        """
        result = []

        column_names = [
            'engagementid',
            'engagementname',
            'clientid',
            'appid',
            'appname',
            'currentappversion',
            'description',
            'validfromdate',
            'validtodate']

        query = "SELECT eid.EngagementId, eid.EngagementName, eid.Clientid," +\
            "eapp.AppId, napp.AppName, napp.CurrentAppVersion, " +\
            "napp.Description, napp.ValidFromDate, napp.ValidToDate FROM " +\
            DATABASE_SCHEMA + ".Engagement eid JOIN " +\
            DATABASE_SCHEMA + ".EngagementApp " +\
            "eapp ON eid.EngagementId = eapp.EngagementId JOIN " +\
            DATABASE_SCHEMA + ".NoodleApp napp ON eapp.AppId = napp.AppId " +\
            "WHERE eid.EngagementId = {0} and eid.Clientid = {1}".format(
                engagement_id, client_id)

        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result, error=None, error_code=None)


class EngagementsNoodleappsPost(Resource):
    """Create Engagement App.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "Parameters",
                "dataType": CreateApp.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[EngagementId] [bigint] NOT NULL," +
                "[AppId] [varchar](64) NOT NULL," +
                "[AppVersion] [numeric](22, 3) NULL," +
                "[IsEnabled] [bit] NULL,"
            }
        ],
        notes='Create Noodle App')
    def post(self):
        """Create App.

        Function Args:
            No Arguments

        Acceptable Json:
            {
              "appversion": "",
              "appid": "",
              "isenabled": "",
              "engagementid": ""
            }

        Returns:
        json: Returns json object of app created else empty error object

        """
        try:
            data = json.loads(request.data.decode('utf-8'))
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        from random import randint
        app_id = randint(0, 100000)
        data.update({'appid': app_id})
        try:
            column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_APP_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        temp_column_names = [x[0] for x in column_type]
        data = {k: v for k, v in data.items() if k in temp_column_names}

        column_names, data_values = get_column_data(
            "",
            json.loads(
                json.dumps(data),
                object_pairs_hook=OrderedDict))

        external_data = Noodleapps(app_id)
        return_data = external_data.post()
        span_data = {}
        if return_data[1] == 200:
            span_data = return_data[0].get('data')
            data.update(span_data)

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in column_names],
                column_type
            )
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            CLIENT_ENGAGEMENT_APP,
            [x.lower() for x in column_names],
            corrected_data,
            []
        )
        logger.info(query)
        try:
            self.connector.execute_no_return(query)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = INVALID_QUERY
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        )


class EngagementsNoodleappsSingle(Resource):
    """Get App.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        App json else errors

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Client ID",
            },
            {
                "name": "engagement_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID",
            },
            {
                "name": "app_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "App ID",
            },
        ],
        notes='List all Noodle Apps associated to engagement')
    def get(self, client_id, engagement_id, app_id):
        """Get Engagement App.

        Function Args:
            No Arguments - bulk result

        Returns:
            json: Returns json object of app else empty data object

        """
        result = []

        column_names = [
            'engagementid',
            'engagementname',
            'clientid',
            'appid',
            'appname',
            'currentappversion',
            'description',
            'validfromdate',
            'validtodate']

        query = "SELECT eid.EngagementId, eid.EngagementName, eid.Clientid," +\
            "eapp.AppId, napp.AppName, napp.CurrentAppVersion, " +\
            "napp.Description, napp.ValidFromDate, napp.ValidToDate FROM " +\
            DATABASE_SCHEMA + ".Engagement eid JOIN " +\
            DATABASE_SCHEMA + ".EngagementApp " +\
            "eapp ON eid.EngagementId = eapp.EngagementId JOIN " +\
            DATABASE_SCHEMA + ".NoodleApp napp ON " +\
            "eapp.AppId = napp.AppId WHERE eid.EngagementId " +\
            "= {0} and eid.Clientid = {1} and eapp.AppId = '{2}'".format(
                engagement_id, client_id, app_id)

        logger.info(query)
        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=column_names
        )

        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
