#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get dataset details [multiple entry]
        post: create dataset [single entry]

    .. module:: client_users
    :platform: OS X
    :synopsis: Creating get (Multiple dataset of client)
    and post (single entry) method.

    .. script:: Gururaj Jeerge <gururaj.jeerge@noodle.ai>
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from services.db_service import Dbconnector
from commons.utils import (
    convert_to_json,
    return_result,
    get_url_args)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from queries.dynamic_queris_write import (
    get_column_data,
    correct_data_values,
    table_inseration_query,
)

from constants.constants import (
    CLIENT_ENGAGEMENT_DATASET,
    CLIENT_ENGAGEMENT_DATASET_ABSTRACT
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    INVALID_QUERY,
    INVALID_OR_FIELD_MISSING)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import json
from logger.logger import logger
from collections import OrderedDict
import pymssql
# ------------------------------------------------------------------------------
# DOCUMENTATION TOOL IMPORTS
# ------------------------------------------------------------------------------
from swaggerdoc.dataset_swagger import CreateDataset


class EngagementsDatasets(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = Dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "full OR basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Select Options - " +
                "clientid, engagementid, engagementname," +
                "startdate, enddate, clientname, clientid, " +
                "stagename, statusname",
            }
        ],
        notes='List all Datasets available',
        nickname='Datasets')
    def get(self):
        """List Datasets.

        Program Args:
            Function do not accept any argument.

        URLParameters:
            limit
            offset
            projection
            sort

        Operations:
            1) Get all URL parameters from get_url_args() - Pass request object
            2) Get Column names of table from Dbconnector.columnnames()
            3) Write conditional clauses
            4) Generate Query
            5) Execute query through Dbconnector.execute()
            6) Return Json and status code

        Returns:
            Json containing all datasets else empty Json

        """
        result = []

        # Operation 1
        limit, offset, sort_key, projection = get_url_args(req=request)
        sort_key = ['clientid'] if not sort_key else sort_key

        # Operation 2
        try:
            self.column_names = [
                x.lower() for x in self.connector.columnnames(
                    CLIENT_ENGAGEMENT_DATASET_ABSTRACT)]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # Operation 3
        between_fields = []
        where_filds = []
        sorting_filed = {}

        # Operation 4
        query = dynamic_queris_read(
            table_name=CLIENT_ENGAGEMENT_DATASET,
            between_query=between_query(fields=between_fields),
            where_query=where_query(fields=where_filds),
            column_names=self.column_names,
            sorting=get_sorting_dict(fields=sorting_filed)
        )
        logger.info(query)

        # Operation 5
        try:
            db_result = self.connector.execute(query)
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = INVALID_QUERY
            logger.error(err.args[0])
            return return_result(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        result = convert_to_json(
            rows=db_result[0],
            column_names=self.column_names
        )

        # Operation 6
        return return_result(
            result=result,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateDataset.__name__,
                "paramType": "body",
                "required": True,
                "description": "Table Design" +
                "[EngagementId] [bigint] NOT NULL, " +
                "[AppId] [varchar](64) NOT NULL," +
                "[DatasetId] [bigint] NOT NULL," +
                "[IsEnabled] [bit] NULL,"
            }
        ],
        notes='Assign dataset to engagement')
    def post(self):
        """Create Dataset Entry.

        Args:
            No Arguments

        Returns:
        json: Returns json object of client created else empty error object

        """
        try:
            data = json.loads(
                request.data.decode('utf-8'),
                object_pairs_hook=OrderedDict)

            self.column_names, data_values = get_column_data(
                "", data)
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            data = {}
            logger.error(err.args[0])
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        try:
            self.column_type = self.connector.column_names_datatype(
                CLIENT_ENGAGEMENT_DATASET_ABSTRACT)
        except pymssql.Error as err:
            self.error = err.args
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        try:
            corrected_data = correct_data_values(
                data_values,
                [x.lower() for x in self.column_names],
                self.column_type)
        except ValueError as err:
            self.error = err
            self.error_code = INVALID_OR_FIELD_MISSING
            data = {}
            logger.error(err.args)
            return return_result(
                result=data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        query = table_inseration_query(
            CLIENT_ENGAGEMENT_DATASET,
            [x.lower() for x in self.column_names],
            corrected_data,
            [])
        logger.info(query)

        try:
            self.connector.execute_no_return(query)
        except Exception as e:
            self.error = e.args
            self.error_code = INVALID_QUERY
            logger.error(e.args)
            return return_result(
                result={},
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        return return_result(
            result=data,
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
