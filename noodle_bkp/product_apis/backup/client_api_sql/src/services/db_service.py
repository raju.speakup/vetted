#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get: get client details by id [single entry]
        delete: delete client by id [single entry]
        put: update client details by id [single entry]
"""


import pymssql


class Dbconnector(object):
    """
    Class which create database connection.

    Features:
        Creates connection.
        Close Connection.
        Executes statements.
        Delete rows from database.
        Updates rows from database.
        Execute queries and returns nothing
            [For queries which do not need return values].
        Returns column names of table name.
        Returns column names with datatype associated to columns.

    Args:

    Returns:
        creates instant of class.
    """

    def __init__(self, server_name='',
                 username='', password='', database_name=''):
        """
        Init function will work like constructor to this class.

        First thing to execute in this class - Let's take advantage of this.

        Args:
            server_name (str): Specify server name - Host is idel
            username (str): Username
            password (str): Password
            database_name (str): Database name

        Returns:
            bool: Description of return value
        """
        self.server = server_name
        self.database = database_name
        self.username = username
        self.password = password

    def connect(self):
        """Connect to mssql database.

        Get cursor and set connection flag to True

        Args:
            No Arguments - Default self is contains credentials to connect

        Returns:
            bool: Description of return value
        """
        self.connection = pymssql.connect(
            self.server,
            self.username,
            self.password,
            self.database
        )
        self.cursor = self.connection.cursor()

    def close(self):
        """Close connection from mssql.

        Commit changes before closing connection and
        set connection flag to false

        Args:
            No Arguments

        Returns:
            bool: Description of return value
        """
        self.connection.commit()
        self.connection.close()

    def execute(self, statement):
        """Execute queries from connection cursor.

        Args:
            statement (str): statement to update
            arg2 (str): Description of arg2

        Returns:
            list: row data in list.
        """
        queries = []

        self.connect()
        self.cursor.execute(statement)
        data = self.cursor.fetchall()
        queries.append(data)
        self.close()
        return queries

    def delete(self, statement):
        """Execute queries from connection cursor.

        Args:
            statement (str): statement to delete

        Returns:
            str: task status
        """
        self.connect()
        self.cursor.execute(statement)
        self.close()

    def update(self, statement):
        """Execute queries from connection cursor.

        Args:
            statement (str): statement to update

        Returns:
            str: task status
        """
        self.connect()
        self.cursor.execute(statement)
        self.close()

    def execute_no_return(self, statement):
        """Execute queries from connection cursor.

        Args:
            statement (str): statement to update

        Returns:
            str: task status
        """
        self.connect()
        self.cursor.execute(statement)
        self.close()

    def columnnames(self, tablename):
        """Get column names of table specified.

        This will help in creating json key names easily.

        Args:
            tablename (str): specify only table name

        Returns:
            list: list of column names
        """
        columns = list()

        self.connect()
        base_query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS \
        WHERE TABLE_NAME = '{0}'\
        ORDER BY ORDINAL_POSITION" .format(tablename)

        self.cursor.execute(base_query)
        columns_row = self.cursor.fetchall()

        for i in columns_row:
            if i:
                columns.append(i[0])

        self.close()
        return columns

    def column_names_datatype(self, tablename):
        """Get coumn names of table specified.

        This will help in creating json key names easily.

        Args:
            tablename (str): specify only table name

        Returns:
            dict: returns dict with key value pair of
                  column names and data type of corresponding
        """
        result = list()

        self.connect()
        base_query = "SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS \
        WHERE TABLE_NAME = '{0}' \
        ORDER BY ORDINAL_POSITION" .format(tablename)

        self.cursor.execute(base_query)
        columns_row = self.cursor.fetchall()

        for entry in columns_row:
            if entry:
                result.append([entry[0].lower(), entry[1], entry[2]])

        self.close()
        return result
