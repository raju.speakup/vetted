#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources Dynamic data updates
    Functionality:
        Detect column type and return data in specified datatype
"""

import datetime


def get_char_set_type(data_type, data):
    """
    Get data-type and data
    Convert datum in equivalent data-type

    Args:
        data_type (str): Specify data-type
        data (str): datum

    Returns:
        bool: Description of return value
    """

    return {
        'varchar': "'{0}'".format(data),

        'bigint': "{0}".format(data),

        'datetime': datevalidation(data_type, data),

        'bit': '{0}'.format(data),

        'numeric': '{0}'.format(data),

        'int': '{0}'.format(data)

    }.get(data_type, 'Not Found')


def datevalidation(data_type, date_data):
    """
    """
    if data_type == 'datetime' and date_data:
        if datetime.datetime.strptime(date_data, '%m/%d/%Y'):
            return "'{0}'".format(date_data)
        else:
            raise ValueError("{0} invalid date format".format(date_data))
    elif data_type == 'datetime' and date_data == '':
        return "'{0}'".format(date_data)
