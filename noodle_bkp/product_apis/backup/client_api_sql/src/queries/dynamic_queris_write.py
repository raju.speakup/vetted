#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code
    Functionality:
        get_column_data: removes any field specified from dict

        table_inseration_query: Create table_inseration_query
            clause with Table Name, Column Names, Data Values and Where Clause.

        correct_data_values: Correct every data values insertion statement
            with bases of column type

        column_list_string: Generate column names as string fro list

        where_query: Create where clause

"""

from .dynamic_data_type import get_char_set_type


def get_column_data(id_field, data_dict={}):
    """
    get_column_data removes any field specified from dict

    Args:
        id_field (str): table name
        data_dict (str): data values

    Returns:
        column_names (list): Column names
        data_values (list): Data values
    """
    column_names = []
    data_values = []

    for index, key in enumerate(data_dict):
        if not key == id_field:
            column_names.append(str(key))
            data_values.append(str(data_dict[key]))
    return column_names, data_values


def table_inseration_query(
        table_name, column_names, data_values, where_clause=[]):
    """
    Create table_inseration_query clause with Table Name,
    Column Names, Data Values and Where Clause.

    Args:
        table_name (str): table name
        column_names (str): Column names
        data_values (str): Data values
        where_clause (list): Where clause in list

    Returns:
        query (str): Part query as insertion clause.
    """
    column_names = ",".join(column_names)

    query = "INSERT INTO " + table_name
    if column_names:
        query = query + " ( " + column_names + " ) " + \
            "VALUES" + " (" + data_values + " ) "

    query = query + where_clause if len(where_clause) > 0 else query + " ;"

    return query


def correct_data_values(data_values=[], column_names=[], column_type=[]):
    """
    Correct every data values insertion statement with bases of column type.

    Args:
        data_values (list): data values
        column_names (list): column names
        column_type (list): column type

    Returns:
        data_values_return (str): Part query as insertion clause.
    """
    # Create a empty string to append all correction of values and keep it as
    # global to function - More ever every corrected value will be appended.
    data_values_return = ""

    # We need to loop all column_names and get count of column names along with
    # element of columns - to get this lets use enumerate default function
    for index, element in enumerate(column_names):

        # Now we have column_names list element and index count of element
        # We need to loop column_type in which we can find type of element
        # We can even find is element is NULL constraint allowed and size
        # of element
        for x in range(len(column_type)):

            # Lets find element in given column_type with index of base loop
            # and index of column_type to secondary index of 0
            # Example to column_type -
            # [column_name, column_data_type, is_required]
            if column_type[x][0] == element:
                # If column_type is required and data is not served in json
                # Lets raise a ValueError Exception
                # This can be catch in calling function
                if column_type[x][2] == 'NO' and data_values[index] == '':
                    raise ValueError(
                        "{0} is required filed".format(column_type[x][0]))

                # Lets find datatype appending from get_char_set_type switch
                # and append result to data_values_return string
                try:
                    data_values_return = data_values_return + \
                        get_char_set_type(
                            column_type[x][1], data_values[index])
                except ValueError:
                    err = "{0} is Invalid - Expected format mm/dd/yyyy".format(
                        column_names[index])
                    raise ValueError(err)

                # We need to append comma (,) to every individual entry
                # appended to data_values_return string but we should not
                # append comma (,) at last
                data_values_return = data_values_return + \
                    ',' if not index == len(column_names) - \
                    1 else data_values_return

    return data_values_return


def column_list_string(column_names=[]):
    """
    Generate column names as string fro list

    Args:
        column_names (list): column names

    Returns:
        column_names (str): column names separated by ,(comma)
    """
    column_names = ','.join(column_names)
    return column_names


def where_query(fields=[]):
    """
    Create where clause

    Args:
        fields (list): list of fields which consist 3 parts

    Returns:
        where_query (str): string containing where clause.
    """
    if not fields:
        return ""

    where_query = " WHERE "
    for index, field in enumerate(fields):
        second_string = "'" + field[2] + \
            "'" if type(field[2]) == str else field[2]
        where_query = where_query + \
            field[0] + " " + field[1] + " " + second_string

        if not index == len(fields) - 1:
            where_query = where_query + " and "
    return where_query
