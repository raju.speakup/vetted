#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code | Update query
    Functionality:
        dynamic_queris_update: create update query [single entry]
        correct_updating_value: update query assignment values [single entry]
"""


def dynamic_queris_update(
        table_name,
        column_names,
        data_values,
        where_query):
    """
    Create update clause with field list

    Args:
        table_name (str): table name
        data_values (str): data values
        where_query (str): where clause

    Returns:
        query (str): Part query as update clause.
    """
    query = " UPDATE " + table_name + " SET " + \
        correct_string(column_names, data_values)
    query = query + where_query + ';'
    return query


def correct_string(column_names, data_values):
    """correct_string."""
    data_values = data_values.split(',')
    final_string = ""
    for index in range(len(data_values)):
        final_string = final_string + \
            column_names[index] + "=" + data_values[index] + ","
    return final_string[:-1]
