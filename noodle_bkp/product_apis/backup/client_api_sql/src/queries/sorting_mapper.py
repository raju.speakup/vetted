def engagement_mapper(data_type):
    """
    Get data-type and data
    Convert datum in equivalent data-type

    Args:
        data_type (str): Specify data-type
        data (str): datum

    Returns:
        bool: Description of return value
    """

    return {
        'varchar': "{0}",
        'clientid': "cli.ClientId",
        'engagementid': "eng.EngagementId",
        'engagementname': "eng.EngagementName",
        'startdate': "eng.StartDate",
        'enddate': "eng.EndDate",
        'clientname': "cli.ClientName",
        'clientid': "cli.ClientId",
        'stagename': "stg.StageName",
        'statusname': "stat.StatusName"
    }.get(data_type, 'Not Found')
