#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .sorting_mapper import *


def create_simple_join(table_name, join_fields=[], joins=[], where_query=[]):
    """
    """
    query = "select" + ",".join(join_fields) + "from" + table_name
    joint = ""

    if not joins:
        joint = ""

    for index, field in enumerate(joins):
        joint = joint + 'join' + \
            field[0] + " " + field[1] + " " + field[2]

    return query + joint


def create_featured_filters(
        query, sort_mapping, sorting=[], limit=None, offset=None):
    """
    """
    if sorting:
        query = query + \
            final_sort_query(get_sorting(sort_mapping, sorting)
                             ) if len(sorting) > 0 else query

        query = query + " OFFSET " + offset + \
            " ROWS "if limit and len(sorting) > 0 else query + ';'
        query = query + " FETCH NEXT " + limit + \
            " ROWS ONLY" + ';' if limit and len(sorting) > 0 else query + ';'

    return query


def get_sorting(sort_mapping, fields=[]):
    """
    Create a dict of filed provided in URL

    Args:
        fields (list): List of fields

    Operation:
        Detect every entry in list - If any entry has negative (-) sign
        convert field as DESC else ASC

    Returns:
        sorting_dict (dict): Key value pair of field and it's sort order
    """
    sorting_dict = {}
    for field in fields:
        if field:
            sort_type = 'DESC' if '-' in field else 'ASC'

            remove_symbol = field.replace(
                "-", "") if '-' in field else field

            sorting_dict.update({sort_mapping(remove_symbol): sort_type})
    return sorting_dict


def final_sort_query(sorting={}):
    """
    Get sorting order of every field specified.

    Args:
        sorting (dict): Specify sorting fields in dict.

    Reference query:
        SELECT * FROM Customers
        ORDER BY Country ASC, CustomerName DESC;

    Returns:
        Part query of order sql
    """
    sort_query = " ORDER BY "
    for index, key in enumerate(sorting):

        sort_query = ' ' + \
            sort_query + \
            ' ' + key + ' ' + \
            sorting[key]

        if not index == len(sorting) - 1:
            sort_query = sort_query + ','

    return sort_query
