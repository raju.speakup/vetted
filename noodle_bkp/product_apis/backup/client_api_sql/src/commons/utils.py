#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources utility function
    Functionality:
        convert_to_json: Convert given rows and column_names to dict.

        return_result: Create a template of json return object
                        and substitute data and error

        return_result_list: Create a template of json return object
                        and substitute data and error

        return_result_delete: Create a template of json return object
                        and substitute data and error

        get_url_args: Get data from url [Split data in required manner]

        exclude_column_names: Exclude some columns from fetched columns from DB

        exclude_entry_dict: Exclude some dict entry
"""

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
import json


def convert_to_json(rows, column_names):
    """Convert given rows and column_names to dict.

    Args:
        rows (obj): This is database result returning object.
        column_names (list): List of all column_names present in table

    Returns:
        json: key pair value of column name and associated values

    """
    result = []
    for i in rows:
        result.append(dict(zip(column_names, i)))

    json_string = json.dumps(
        result,
        sort_keys=True,
        indent=4,
        default=str
    )

    return json.loads(json_string)


def return_result(result=[], error=None, error_code=None):
    """Create a template of json return object
        and substitute data and error

    Args:
        result (list): Result of convert_to_json function
        error (str): Error Message if any
        error_code (str): Error Code If any

    Returns:
        json: Description of return value [Single entry]

    """
    result = [] if error else result
    return_result = {}

    return_result.update(
        {
            "data": result,
            "error": {
                "code": error_code,
                "message": str(error)
            }
        }
    )

    return_result = json.dumps(return_result)
    return json.loads(return_result)


def return_result_list(result=[], error=None, error_code=None):
    """Create a template of json return object
        and substitute data and error

    Args:
        result (list): Result of convert_to_json function
        error (str): Error Message if any
        error_code (str): Error Code If any

    Returns:
        json: Description of return value [Multiple entry]

    """
    return_result_list = {}

    return_result_list.update(
        {
            "data": result,
            "total_count": len(result),
            "error": {
                "code": error_code,
                "message": str(error)
            }
        }
    )

    return_result_list = json.dumps(return_result_list)
    return json.loads(return_result_list)


def return_result_delete(result={}, error=None, error_code=None):
    """Create a template of json return object
        and substitute data and error

    Args:
        result (dict): Input json
        error (str): Error Message if any
        error_code (str): Error Code If any

    Returns:
        json: Description of return value

    """
    return_result_list = {}

    return_result_list.update(
        {
            "data": result,
            "records_effected": 1,
            "error": {
                "code": error_code,
                "message": str(error)
            }
        }
    )

    return_result_list = json.dumps(return_result_list)
    return json.loads(return_result_list)


def get_url_args(req):
    """Get data from url [Split data in required manner]

    Args:
        req (obj): Pass request object to function and get all split data

    Returns:
        limit: Limit of data result
        offset: Offset of data result
        sort_key: Sorting keys in list [-] indication for DESC
        projection: BASIC or FULL

    """
    limit = req.args.get("limit", "10")
    offset = req.args.get("offset", "0")
    sort_key = req.args.get("sort", "")
    projection = req.args.get("projection", "FULL")
    if sort_key:
        gen_sort_key = sort_key.split(",") if ',' in sort_key else [sort_key]
    else:
        gen_sort_key = None
    return limit, offset, gen_sort_key, projection


def exclude_column_names(column_names=[], exclude_list=[]):
    """Exclude some columns from fetched columns from DB

    Usage:
        Projection can be executed [BASIC|FULL]

    Args:
        column_names (list): List of columns from database.
        exclude_list (list): List of columns to exclude from fetched columns.
    Returns:
        column_names: List of column names

    """
    for col in exclude_list:
        if col:
            column_names.remove(col)
    return column_names


def exclude_entry_dict(passing_dict={}, exclude_list=[]):
    """Exclude some dict entry

    Args:
        passing_dict (dict): Dict of key value pair
        exclude_list (dict): Exclude dict
    Returns:
        passing_dict: resulted dict

    """
    for col in exclude_list:
        if col:
            del passing_dict[col]
    return passing_dict
