
from pymongo import MongoClient
import json
import sys

from configuration.layer_fetch_environ import (
    DB_NAME, PORT, SERVER_NAME, PASSWORD, USERNAME, SESSION_TIMEOUT)

from logger.logger import logger


class NoodleMongoClient:
    """
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        mongo_url = "".join(
            ['mongodb://', USERNAME, ':',
             PASSWORD, '@', SERVER_NAME, '/', DB_NAME])
        self.client = MongoClient(
            mongo_url)
        self.database = self.client[DB_NAME]

    def insert_one(self, collectionname, data):
        """Insert One."""
        try:
            data = json.loads(data)
        except ValueError as err:
            message = err.args[0] + ' - ERROR IN - ' + collectionname
            logger.error(message)
            sys.exit(message)
        return self.database[collectionname].insert_one(
            data)

    def insert_many(self, collectionname, data):
        """InsertMany."""
        try:
            data = json.loads(data)
        except ValueError as err:
            message = err.args[0] + ' - ERROR IN - ' + collectionname
            logger.error(message)
            sys.exit(message)
        return self.database[collectionname].insert_many(
            data)

    def find_one(self, collectionname, data):
        """Find One."""
        return self.database[collectionname].find_one(data)

    def find_all(self, collectionname, data,
                 skip=None, limit=None, sortdata=list()):
        """Find One."""
        skip = 0 if skip is None or not type(skip) is int else skip
        limit = 5 if limit is None or not type(limit) is int else limit
        return self.database[collectionname].find(
            data).skip(skip).limit(limit).sort(sortdata)

    def update_one(self, collectionname, data):
        """Find One."""
        return self.database[collectionname].update_one(data[0], data[1])

    def update_many(self, collectionname, data):
        """Find Many."""
        return self.database[collectionname].update_many(data[0], data[1])

    def delete_many(self, collectionname, data):
        """Delete Many."""
        return self.database[collectionname].delete_many(data)
