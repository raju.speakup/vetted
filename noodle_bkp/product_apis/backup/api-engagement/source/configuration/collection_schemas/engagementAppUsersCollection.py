schema = {
    "type": "object",
    "properties": {
        "userId": {
            "type": "string"
        }

    },
    "required": ["userId"],
    "additionalProperties": False
}
