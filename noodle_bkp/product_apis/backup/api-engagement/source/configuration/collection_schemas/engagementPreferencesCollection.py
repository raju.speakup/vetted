schema = {
    "type": "object",
    "properties": {
        "preferenceKey": {
            "type": "string"
        },
        "preferenceValue": {
            "type": "string",
        }

    },
    "required": ["preferenceKey", "preferenceValue"],
    "additionalProperties": False
}
