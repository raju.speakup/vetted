schema = {
    "type": "object",
    "properties": {
        "clientName": {
            "type": "string",
            "minLength": 1
        },
        "clientShortName": {
            "type": "string",
            "minLength": 1
        },
        "contactName": {
            "type": "string"
        },
        "contactEmail": {
            "type": "string",
            "minLength": 1,
            "format": "email"
        },
        "contactAddress1": {
            "type": "string"
        },
        "contactAddress2": {
            "type": "string"
        },
        "city": {
            "type": "string"
        },
        "stateCode": {
            "type": "string"
        },
        "preferredDateFormat": {
            "type": "string"
        },
        "timeZone": {
            "type": "string"
        },
        "onBoardedBy": {
            "type": "string"
        },

    },
    "required": ["clientName", "contactEmail"],
    "additionalProperties": False
}
