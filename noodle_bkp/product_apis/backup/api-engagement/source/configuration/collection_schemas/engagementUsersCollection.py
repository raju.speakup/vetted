schema = {
    "type": "object",
    "properties": {
        "engagementId": {
            "statusId": "string"
        },
        "userId": {
            "statusName": "bool",
        }

    },
    "required": ["engagementId", "userId"],
    "additionalProperties": False
}
