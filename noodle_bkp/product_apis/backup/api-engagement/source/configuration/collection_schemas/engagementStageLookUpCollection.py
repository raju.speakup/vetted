schema = {
    "type": "object",
    "properties": {
        "stageId": {
            "type": "string"
        },
        "stageName": {
            "type": "string",
        }

    },
    "required": ["stageId", "stageName"],
    "additionalProperties": False
}
