schema = {
    "type": "object",
    "properties": {
        "appId": {
            "type": "string"
        },
        "datasetId": {
            "type": "bool",
        }

    },
    "required": ["appId", "datasetId"],
    "additionalProperties": False
}
