schema = {
    "type": "object",
    "properties": {
        "appName": {
            "type": "string"
        },
        "currentAppVersion": {
            "type": "string",
        },
        "description": {
            "type": "string",
        },
        "version": {
            "type": "object",
            "properties": {
                "version": {
                    "type": "string",
                },
                "validFromDate": {
                    "type": "string",
                },
                "validToDate": {
                    "type": "string",
                }
            },
            "required": ["version", "validFromDate"],
            "additionalProperties": False
        }

    },
    "required": ["appName", "currentAppVersion"],
    "additionalProperties": False
}
