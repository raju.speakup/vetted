schema = {
    "type": "object",
    "properties": {
        "appId": {
            "statusId": "string"
        },
        "datasetId": {
            "statusName": "bool",
        }

    },
    "required": ["statusId", "statusName"],
    "additionalProperties": False
}
