schema = {
    "type": "object",
    "properties": {
        "documentName": {
            "type": "string"
        },
        "documentPath": {
            "type": "bool",
        },
        "owner": {
            "type": "string"
        },
        "createdOn": {
            "type": "bool",
        },
        "modifiedOn": {
            "type": "string"
        }

    },
    "required": ["documentName", "documentPath", "owner"],
    "additionalProperties": False
}
