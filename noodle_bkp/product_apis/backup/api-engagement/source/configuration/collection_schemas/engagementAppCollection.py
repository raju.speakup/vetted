schema = {
    "type": "object",
    "properties": {
        "appVersion": {
            "type": "string"
        },
        "isEnabled": {
            "type": "bool",
        }

    },
    "required": ["appVersion", "isEnabled"],
    "additionalProperties": False
}
