schema = {
    "type": "object",
    "properties": {
        "engagementName": {
            "type": "string"
        },
        "description": {
            "type": "string",
        },
        "clientId": {
            "type": "string",
        },
        "stageId": {
            "type": "integer",
        },
        "statusId": {
            "type": "integer",
        },
        "startDate": {
            "type": "string",
        },
        "endDate": {
            "type": "string",
        }

    },
    "required": ["engagementName", "description",
                 "stageId", "statusId", "startDate", "endDate"],
    "additionalProperties": False
}
