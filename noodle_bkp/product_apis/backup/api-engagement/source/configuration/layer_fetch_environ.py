#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fetch all confidencial data from and environment file
    Functionality:
        - If any manupulations needed in environment setting
        this is place to do it

"""
# ------------------------------------------------------------------------------
# IMPORT SECTION
# ------------------------------------------------------------------------------

import os

from dotenv import load_dotenv


# ------------------------------------------------------------------------------
# CONFIGURING ENVIRONMENT FILE AND FETCHING DATA
# ------------------------------------------------------------------------------

APP_ROOT = os.path.join(os.path.dirname(__file__), '../..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)


# ------------------------------------------------------------------------------
# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES
# ------------------------------------------------------------------------------

SERVER_NAME = os.getenv(os.getenv("MODE") + '_' + "SERVER_NAME")
DB_NAME = os.getenv(os.getenv("MODE") + '_' + "DB_NAME")
USERNAME = os.getenv(os.getenv("MODE") + '_' + "USERNAME")
PASSWORD = os.getenv(os.getenv("MODE") + '_' + "PASSWORD")
PORT = int(os.getenv(os.getenv("MODE") + '_' + "PORT"))
SESSION_TIMEOUT = int(os.getenv(os.getenv("MODE") + '_' + "SESSION_TIMEOUT"))
DEFAULT_IP = os.getenv("DEFAULT_IP")
LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")
DEBUG = True if os.getenv("MODE") == 'DEV' else False
