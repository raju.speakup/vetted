#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger

# from resource.home import Home
from resources.health import Health
from resources.clientid import Client
from resources.clientlist import ClientList
from resources.engagementid import Engagement
from resources.engagementlist import EngagementList
from resources.engagementappid import EngagementApp
from resources.engagementapplist import EngagementAppList

from resources.engagementstatuslookupid import Status
from resources.engagementstatuslookuplist import StatusList

from resources.engagementstagelookupid import Stage
from resources.engagementstagelookuplist import StageList

from resources.engagementdocsid import EngagementDoc
from resources.engagementdocslist import EngagementDocList

from resources.engagementdatasetid import EngagementAppDataset
from resources.engagementdatasetlist import EngagementAppDatasetList

from resources.noodleappid import NoodleApp
from resources.noodleapplist import NoodleAppList

from resources.engagementpreferenceid import EngagementPreference
from resources.engagementpreferencelist import EngagementPreferenceList

from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG

app = Flask(__name__)

api = swagger.docs(
    Api(app),
    apiVersion='2.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="MVP Engagement API Documentation | Mongo DB",
    resourcePath="/Client"
)

# api.add_resource(
#     Home,
#     '/')

api.add_resource(
    Health,
    '/api/v1/health')

# http://server/api/v1/clients/clients/{client_id}
api.add_resource(
    Client,
    '/api/v1/clients/clients/<string:client_id>')

# http://server/api/v1/clients
api.add_resource(
    ClientList,
    '/api/v1/clients')

# http://server/api/v1/clients/{client_id}/engagements/{engagement_id}
api.add_resource(
    Engagement,
    '/api/v1/clients/<string:client_id>/engagements/<string:engagement_id>')

# http://server/api/v1/client/{client_id}/engagements
api.add_resource(
    EngagementList,
    '/api/v1/clients/<string:client_id>/engagements')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/engagementapps/{app_id}
api.add_resource(
    EngagementApp,
    '/api/v1/clients/<string:client_id>/engagements/' +
    '<string:engagement_id>/engagementapps/<string:app_id>')

# http://server/api/v1/client/{client_id}/engagement/
# {engagement_id}/engagementapps/{app_id}
api.add_resource(
    EngagementAppList,
    '/api/v1/clients/<string:client_id>/engagements/' +
    '<string:engagement_id>/engagementapps')

# http://server/api/v1/client/metadata/status
api.add_resource(
    Status,
    '/api/v1/clients/metadata/statuses/<int:status_id>')

# http://server/api/v1/client/metadata/status
api.add_resource(
    StatusList,
    '/api/v1/clients/metadata/statuses')

# http://server/api/v1/client/metadata/stages
api.add_resource(
    Stage,
    '/api/v1/clients/metadata/stages/<int:stage_id>')

# http://server/api/v1/client/metadata/stages
api.add_resource(
    StageList,
    '/api/v1/clients/metadata/stages')

# http://server/api/v1/clients/{client_id}/engagements/
# {engagement_id}/docs/{doc_id}
api.add_resource(
    EngagementDoc,
    '/api/v1/clients/<string:client_id>/engagements/' +
    '<string:engagement_id>/docs/<string:doc_id>')

# http://server/api/v1/client/engagement/docs
api.add_resource(
    EngagementDocList,
    '/api/v1/clients/<string:client_id>/engagements/' +
    '<string:engagement_id>/docs')

# http://server/api/v1/clients/engagements/datasets/{dataset_id}
api.add_resource(
    EngagementAppDataset,
    '/api/v1/clients/engagements/<string:engagement_id>/' +
    'datasets/<string:dataset_id>')

# http://server/api/v1/clients/engagements/datasets
api.add_resource(
    EngagementAppDatasetList,
    '/api/v1/clients/engagements/<string:engagement_id>/' +
    'datasets')

# http://server/api/v1/noodleapp/<string:app_id>
api.add_resource(
    NoodleApp,
    '/api/v1/noodleapp/<string:app_id>')

# http://server/api/v1/noodleapp
api.add_resource(
    NoodleAppList,
    '/api/v1/noodleapp')

# http://server/api/v1/clients/engagements/datasets
api.add_resource(
    EngagementPreference,
    '/api/v1/clients/engagements/<string:engagement_id>/' +
    'preference/<string:preferencekey>')

# http://server/api/v1/clients/engagements/datasets
api.add_resource(
    EngagementPreferenceList,
    '/api/v1/clients/engagements/<string:engagement_id>/' +
    'preference')

if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8000, debug=DEBUG)
