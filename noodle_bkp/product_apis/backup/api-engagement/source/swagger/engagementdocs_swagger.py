#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementDoc:
    """CreateEngagementDataset fields."""

    resource_fields = {
        'documentId': 'fields.String',
        'documentName': 'fields.String',
        'documentPath': 'fields.String',
        'owner': 'fields.String',
        'createdOn': 'fields.String',
        'modifiedOn': 'fields.String'
    }


@swagger.model
class UpdateEngagementDoc:
    """UpdateEngagementDataset fields."""

    resource_fields = {
        'documentId': 'fields.String',
        'documentName': 'fields.String',
        'documentPath': 'fields.String',
        'owner': 'fields.String',
        'createdOn': 'fields.String',
        'modifiedOn': 'fields.String'
    }
