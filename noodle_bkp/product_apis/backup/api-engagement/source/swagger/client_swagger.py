#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateClient:
    """CreateClient fields."""

    resource_fields = {
        'clientName': 'fields.String',
        'clientShortName': 'fields.String',
        'contactName': 'fields.String',
        'contactEmail': 'fields.String',
        'contactAddress1': 'fields.String',
        'contactAddress2': 'fields.String',
        'city': 'fields.String',
        'stateCode': 'fields.String',
        'preferredDateFormat': 'fields.String',
        'timeZone': 'fields.String',
        'onBoardedBy': 'fields.String',
    }


@swagger.model
class UpdateClient:
    """UpdateClient fields."""

    resource_fields = {
        'clientName': 'fields.String',
        'clientShortName': 'fields.String',
        'contactName': 'fields.String',
        'contactEmail': 'fields.String',
        'contactAddress1': 'fields.String',
        'contactAddress2': 'fields.String',
        'city': 'fields.String',
        'stateCode': 'fields.String',
        'preferredDateFormat': 'fields.String',
        'timeZone': 'fields.String',
        'onBoardedBy': 'fields.String'
    }
