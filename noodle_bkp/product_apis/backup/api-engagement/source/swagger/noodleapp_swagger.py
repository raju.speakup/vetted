#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger
from flask_restful import fields


@swagger.model
class Version:
    """Version."""

    resource_fields = {
        'version': 'field.String',
        'validFromDate': 'field.String',
        'validToDate': 'field.String'

    }


@swagger.model
@swagger.nested(
    version=Version.__name__)
class CreateNoodleApp:
    """CreateNoodleApp fields."""

    resource_fields = {
        'appName': 'field.String',
        'currentAppVersion': 'field.String',
        'description': 'field.String',
        'version': fields.List(fields.Nested(Version.resource_fields)),
    }


@swagger.model
@swagger.nested(
    version=Version.__name__)
class UpdateNoodleApp:
    """UpdateNoodleApp fields."""

    resource_fields = {
        'appName': 'field.String',
        'currentAppVersion': 'field.String',
        'description': 'field.String',
        'version': fields.List(fields.Nested(Version.resource_fields)),
    }
