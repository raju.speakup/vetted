#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementStatus:
    """CreateEngagementStatus fields."""

    resource_fields = {
        'statusId': 'fields.String',
        'statusName': 'fields.String'
    }


@swagger.model
class UpdateEngagementStatus:
    """UpdateEngagementStatus fields."""

    resource_fields = {
        'statusName': 'fields.String'
    }
