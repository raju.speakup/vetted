#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger
from flask_restful import fields


@swagger.model
class CreateEngagement:
    """CreateEngagement fields."""

    resource_fields = {
        'engagementName': 'fields.String',
        'description': 'fields.String',
        'stageId': fields.Integer,
        'statusId': fields.Integer,
        'startDate': 'fields.String',
        'endDate': 'fields.String'
    }


@swagger.model
class UpdateEngagement:
    """UpdateEngagement fields."""

    resource_fields = {
        'engagementName': 'fields.String',
        'description': 'fields.String',
        'stageId': 'fields.String',
        'statusId': 'fields.String',
        'startDate': 'fields.String',
        'endDate': 'fields.String'
    }
