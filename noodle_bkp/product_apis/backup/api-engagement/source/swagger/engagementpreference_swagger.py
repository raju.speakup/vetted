#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementPreference:
    """CreateEngagementPreference fields."""

    resource_fields = {
        'preferenceKey': 'fields.String',
        'preferenceValue': 'fields.String'
    }


@swagger.model
class UpdateEngagementPreference:
    """UpdateEngagementPreference fields."""

    resource_fields = {
        'preferenceKey': 'fields.String',
        'preferenceValue': 'fields.String'
    }
