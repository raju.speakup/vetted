#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementApp:
    """CreateEngagementApp fields."""

    resource_fields = {
        'appVersion': 'fields.String',
        'isEnabled': 'fields.String',
    }


@swagger.model
class UpdateEngagementApp:
    """UpdateEngagementApp fields."""

    resource_fields = {
        'appVersion': 'fields.String',
        'isEnabled': 'fields.String',
    }
