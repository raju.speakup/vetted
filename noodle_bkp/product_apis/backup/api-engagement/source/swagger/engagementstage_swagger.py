#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementStage:
    """CreateEngagementStage fields."""

    resource_fields = {
        'stageId': 'fields.String',
        'stageName': 'fields.String'
    }


@swagger.model
class UpdateEngagementStage:
    """UpdateEngagementStage fields."""

    resource_fields = {
        'stageName': 'fields.String'
    }
