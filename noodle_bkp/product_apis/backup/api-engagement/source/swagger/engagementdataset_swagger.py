#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class CreateEngagementDataset:
    """CreateEngagementDataset fields."""

    resource_fields = {
        'appId': 'fields.String',
        'datasetId': 'fields.String',
        'isEnabled': 'fields.String',
    }


@swagger.model
class UpdateEngagementDataset:
    """UpdateEngagementDataset fields."""

    resource_fields = {
        'appId': 'fields.String',
        'datasetId': 'fields.String',
        'isEnabled': 'fields.String',
    }
