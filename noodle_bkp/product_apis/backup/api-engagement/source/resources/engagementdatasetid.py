#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.engagementAppDatasetCollection import (
    schema)
from swagger.engagementdataset_swagger import UpdateEngagementDataset


class EngagementAppDataset(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            }
        ],
        notes='Engagement App Dataset Get',
        nickname='EngagementAppDataset')
    def get(self, engagement_id, dataset_id):
        """Engagement App Dataset Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementAppDatasetCollection",
                data={
                    "engagementId": engagement_id,
                    "datasetId": dataset_id,
                    "isActive": True
                }
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if return_object:
            return_object.pop("_id")
        else:
            return return_result_final(
                data,
                "No Document present for given input combination \
                engagement_id={0}, datasetId={1} ".format(
                    engagement_id,
                    dataset_id),
                "DOCUMENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            }
        ],
        notes='Engagement App Dataset Delete',
        nickname='EngagementAppDataset')
    def delete(self, engagement_id, dataset_id):
        """Engagement App Dataset Delete.

        This is soft delete for Engagement Dataset - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementAppDatasetCollection",
                data={
                    "engagementId": engagement_id,
                    "datasetId": dataset_id,
                    "isActive": True
                }
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "No Document present for given input combination \
                clientId={0}, engagement_id={1}, datasetId={2} ".format(

                    engagement_id,
                    dataset_id),
                "DOCUMENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementAppDatasetCollection",
                data=(
                    {
                        "engagementId": engagement_id,
                        "datasetId": dataset_id
                    }, {
                        "$set": {"isActive": False}}))
            logger.info(data)
        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateEngagementDataset.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            }
        ],
        notes='Engagement App Dataset Update',
        nickname='EngagementAppDatasetUpdate')
    @validate_json
    @validate_schema(schema)
    def put(self, engagement_id, dataset_id):
        """Engagement App Dataset Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="engagementAppDatasetCollection",
                data={
                    "engagementId": engagement_id,
                    "datasetId": dataset_id,
                    "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "No Document present for given input combination \
                engagement_id={0}, datasetId={1} ".format(
                    engagement_id,
                    dataset_id),
                "DOCUMENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementAppDatasetCollection",
                data=(
                    {
                        "engagementId": engagement_id,
                        "datasetId": dataset_id
                    }, {
                        "$set": data}))
            logger.info(data)
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK
