#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientCollection import schema
from swagger.client_swagger import UpdateClient


class Client(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client Get',
        nickname='Client')
    def get(self, client_id):
        """Client Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="clientCollection",
                data={"clientId": client_id, "isActive": True})
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }
        ],
        notes='Client Delete',
        nickname='ClientDelete')
    def delete(self, client_id):
        """Client Delete.

        This is soft delete for client - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="clientCollection",
                data={"clientId": client_id, "isActive": True})
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "{0} client do not exists".format(client_id),
                "CLIENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clientCollection",
                data=({"clientId": client_id}, {
                    "$set": {"isActive": False}}))

        self.connection.update_many(
            collectionname="engagementCollection",
            data=({"clientId": client_id}, {
                "$set": {"isActive": False}}))

        self.connection.update_many(
            collectionname="engagementAppCollection",
            data=({"clientId": client_id}, {
                "$set": {"isActive": False}}))

        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateClient.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            }

        ],
        notes='Client Create',
        nickname='Client Create')
    @validate_json
    @validate_schema(schema)
    def put(self, client_id):
        """Client Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="clientCollection",
                data={"clientId": client_id, "isActive": True})
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} client do not exists".format(client_id),
                "CLIENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="clientCollection",
                data=({"clientId": client_id}, {
                    "$set": data}))
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK
