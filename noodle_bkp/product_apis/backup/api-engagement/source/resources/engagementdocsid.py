#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.engagementDocumentCollection import (
    schema)
from swagger.engagementdocs_swagger import UpdateEngagementDoc


class EngagementDoc(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "doc_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Document ID"
            }
        ],
        notes='Engagement Doc Get',
        nickname='EngagementDoc')
    def get(self, client_id, engagement_id, doc_id):
        """Engagement Doc Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementDocumentCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "documentId": doc_id,
                    "isActive": True
                }
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if return_object:
            return_object.pop("_id")
        else:
            return return_result_final(
                data,
                "No Document present for given input combination \
                clientId={0}, engagement_id={1}, documentId={2} ".format(
                    client_id,
                    engagement_id,
                    doc_id),
                "DOCUMENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "doc_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            }
        ],
        notes='Engagement Doc Delete',
        nickname='EngagementDoc')
    def delete(self, client_id, engagement_id, doc_id):
        """Engagement Doc Delete.

        This is soft delete for client - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementDocumentCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "documentId": doc_id,
                    "isActive": True
                }
            )
            logger.info(data)
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "No Document present for given input combination \
                clientId={0}, engagement_id={1}, documentId={2} ".format(
                    client_id,
                    engagement_id,
                    doc_id),
                "DOCUMENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementDocumentCollection",
                data=(
                    {
                        "clientId": client_id,
                        "engagementId": engagement_id,
                        "documentId": doc_id
                    }, {
                        "$set": {"isActive": False}}))
            logger.info(data)
        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateEngagementDoc.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "doc_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Doc ID"
            }
        ],
        notes='Engagement Doc Update',
        nickname='EngagementDocUpdate')
    @validate_json
    @validate_schema(schema)
    def put(self, client_id, engagement_id, doc_id):
        """Engagement Doc Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="engagementDocumentCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "documentId": doc_id,
                    "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "No Document present for given input combination \
                clientId={0}, engagement_id={1}, documentId={2} ".format(
                    client_id,
                    engagement_id,
                    doc_id),
                "DOCUMENT_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementDocumentCollection",
                data=(
                    {"clientId": client_id,
                     "engagementId": engagement_id,
                     "documentId": doc_id
                     }, {
                        "$set": data}))
            logger.info(data)
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK
