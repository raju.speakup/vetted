#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.clientCollection import schema
from swagger.client_swagger import CreateClient


class ClientList(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Client List',
        nickname='Clients')
    def get(self):
        """Client List."""
        result = get_list(self, "clientCollection", {
                          "isActive": True}, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateClient.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Client Create',
        nickname='Client Create')
    @validate_json
    @validate_schema(schema)
    def post(self):
        """Client Create."""
        data = json.loads(request.data)
        data.update(
            {"clientId": uuid.uuid4().hex,
             "isActive": True})
        try:
            self.connection.insert_one(collectionname="clientCollection",
                                       data=json.dumps(data))
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            data,
            self.error,
            self.error_code
        ), HTTP_201_CREATED
