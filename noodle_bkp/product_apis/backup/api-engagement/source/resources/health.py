#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask_restful_swagger import swagger
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from configuration.layer_fetch_environ import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
    PORT,
    DEFAULT_IP,
    LOG_FILE_PATH,
    SESSION_TIMEOUT
)

from constants.custom_field_error import (
    HTTP_200_OK)


class Health(Resource):
    """Check health of system."""

    @swagger.operation(
        parameters=[],
        notes='Health Get',
        nickname='HealthGet')
    def get(self):
        """Check health of system."""
        result = {}
        result.update({
            'serverName': SERVER_NAME,
            'databaseName': DB_NAME,
            'userName': USERNAME,
            'port': PORT,
            'dafaultIP': DEFAULT_IP,
            'logStorePath': LOG_FILE_PATH
        })

        try:
            mongo_url = "".join(
                ['mongodb://', USERNAME, ':',
                 PASSWORD, '@', SERVER_NAME, '/', DB_NAME])
            connect = MongoClient(
                mongo_url)
            result.update(
                {"dataBaseIsMaster": connect.admin.command('ismaster')})
            from bson import json_util
            import json
            result = json.dumps(result, default=json_util.default)
        except ConnectionFailure:
            result.update({
                'databaseUp': 'Not able to communicate with MongoDB'})
            return (result), HTTP_200_OK
        return (json.loads(result)), HTTP_200_OK
