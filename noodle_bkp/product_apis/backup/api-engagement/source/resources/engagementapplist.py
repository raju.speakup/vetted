#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.engagementCollection import schema
from swagger.engagementapp_swagger import CreateEngagementApp


class EngagementAppList(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            }
        ],
        notes='Engagement App List',
        nickname='EngagementApp')
    def get(self, client_id, engagement_id):
        """Engagement App List."""
        result = get_list(self, "engagementAppCollection", {
            "clientId": client_id,
            "engagementId": engagement_id,
            "isActive": True
        }, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateEngagementApp.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            }
        ],
        notes='Engagement App Create',
        nickname='EngagementAppCreate')
    @validate_json
    @validate_schema(schema)
    def post(self, client_id, engagement_id):
        """Engagement App Create."""
        try:
            data = {}
            return_object = self.connection.find_all(
                collectionname="engagementCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "isActive": True
                }
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                [],
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if return_object.count() > 0:
            data = json.loads(request.data)
            data.update(
                {
                    "appId": uuid.uuid4().hex,
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "isActive": True
                })
            logger.info(data)
            try:
                self.connection.insert_one(
                    collectionname="engagementAppCollection",
                    data=json.dumps(data))
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
            return return_result_final(
                data,
                self.error,
                self.error_code
            ), HTTP_201_CREATED
        else:
            return return_result_final(
                [],
                "clientid {0}, engagement {1} wrong combination".format(
                    client_id, engagement_id),
                HTTP_404_NOT_FOUND
            ), HTTP_404_NOT_FOUND
