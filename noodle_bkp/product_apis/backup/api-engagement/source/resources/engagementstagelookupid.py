#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.engagementStageLookUpCollection import (
    schema)
from swagger.engagementstage_swagger import UpdateEngagementStage


class Stage(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "stage_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Stage ID"
            }
        ],
        notes='Stage Get',
        nickname='Stage')
    def get(self, stage_id):
        """Stage Get."""
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementStageLookUpCollection",
                data={"stageId": stage_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if return_object:
            return_object.pop("_id")
        else:
            return return_result_final(
                data,
                "stage id {0} do not contain any data".format(stage_id),
                "STAGE_NOT_FOUND"), HTTP_404_NOT_FOUND

        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "stage_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Stage ID"
            }
        ],
        notes='Stage Delete',
        nickname='Stage')
    def delete(self, stage_id):
        """Stage Delete.

        This is Hard delete for Stage"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementStageLookUpCollection",
                data={"stageId": stage_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "{0} stage is not available in backend".format(stage_id),
                "stage_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.delete_many(
                collectionname="engagementStageLookUpCollection",
                data=({"stageId": stage_id}))

        return return_result_final(
            json.dumps([{"message": "Deleted"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateEngagementStage.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "stage_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Stage ID"
            }
        ],
        notes='Engagement Stage Update',
        nickname='EngagementStageUpdate')
    @validate_json
    @validate_schema(schema)
    def put(self, stage_id):
        """Engagement Stage Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="engagementStageLookUpCollection",
                data={"stageId": stage_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} stage is not available in backend".format(stage_id),
                "stage_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementStageLookUpCollection",
                data=({"stageId": stage_id}, {
                    "$set": data}))
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK
