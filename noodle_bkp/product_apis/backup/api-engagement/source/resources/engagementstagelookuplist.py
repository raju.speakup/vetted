#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from commons.code_snippets import get_list
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_201_CREATED)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.engagementStageLookUpCollection import (
    schema)
from swagger.engagementstage_swagger import CreateEngagementStage


class StageList(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Stage List',
        nickname='Stage')
    def get(self):
        """Stage List."""
        result = get_list(self, "engagementStageLookUpCollection",
                          {}, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateEngagementStage.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Engagement Stage Create',
        nickname='EngagementStageCreate')
    @validate_json
    @validate_schema(schema)
    def post(self):
        """Engagement Stage Create."""
        data = json.loads(request.data)
        try:
            self.connection.insert_one(
                collectionname="engagementStageLookUpCollection",
                data=json.dumps(data))
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            data,
            self.error,
            self.error_code
        ), HTTP_201_CREATED
