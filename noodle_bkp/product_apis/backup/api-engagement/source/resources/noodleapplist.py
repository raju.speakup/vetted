#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_201_CREATED)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.noodleAppCollection import schema
from swagger.noodleapp_swagger import CreateNoodleApp


class NoodleAppList(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Noodle Apps List',
        nickname='NoodleApps')
    def get(self):
        """Noodle Apps List."""
        result = get_list(self, "noodleAppCollection", {
                          "isActive": True}, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": CreateNoodleApp.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Noodle App Create',
        nickname='NoodleAppCreate')
    @validate_json
    @validate_schema(schema)
    def post(self):
        """Noodle Apps Create."""
        data = json.loads(request.data)
        data.update(
            {
                "appId": uuid.uuid4().hex,
                "isActive": True
            })
        try:
            self.connection.insert_one(
                collectionname="noodleAppCollection",
                data=json.dumps(data))
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            data,
            self.error,
            self.error_code
        ), HTTP_201_CREATED
