#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
import json
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.engagementAppCollection import schema
from swagger.engagementapp_swagger import UpdateEngagementApp


class EngagementApp(Resource):
    """Client."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            }
        ],
        notes='Engagement App Get',
        nickname='EngagementApp')
    def get(self, client_id, engagement_id, app_id):
        """Engagement App Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementAppCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "appId": app_id,
                    "isActive": True
                }
            )
            logger.info(data)
            if return_object:
                return_object.pop("_id")
        except PyMongoError as err:
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            json.loads(jsonify(return_object).data),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            }
        ],
        notes='Engagement App Delete',
        nickname='EngagementApp')
    def delete(self, client_id, engagement_id, app_id):
        """Engagement App Delete.

        This is soft delete for Engagement App - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="engagementAppCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "appId": app_id,
                    "isActive": True
                }
            )
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                "{0} Engagement Application do not exists".format(app_id),
                "ENGAGEMENT_APP_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementAppCollection",
                data=(
                    {
                        "clientId": client_id,
                        "engagementId": engagement_id,
                        "appId": app_id
                    }, {
                        "$set": {"isActive": False}}))
            logger.info(data)
        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateEngagementApp.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "engagement_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Engagement ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            }
        ],
        notes='Engagement App Create',
        nickname='EngagementAppCreate')
    @validate_json
    @validate_schema(schema)
    def put(self, client_id, engagement_id, app_id):
        """Update Client information."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="engagementAppCollection",
                data={
                    "clientId": client_id,
                    "engagementId": engagement_id,
                    "appId": app_id,
                    "isActive": True})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                "{0} Engagement Application do not exists".format(app_id),
                "ENGAGEMENT_APP_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="engagementAppCollection",
                data=(
                    {"clientId": client_id,
                     "engagementId": engagement_id,
                     "appId": app_id
                     }, {
                        "$set": data}))
            logger.info(data)
        return return_result_final(
            data,
            self.error,
            self.error_code), HTTP_200_OK
