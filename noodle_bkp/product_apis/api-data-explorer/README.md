# Noodle MVP APIs

### Tech

Noodle MVP API uses a number of open source projects to work properly:

* [Python Flask](http://flask.pocoo.org/) - Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions.
* [Python FLASK RESTful](https://flask-restful.readthedocs.io/en/latest/) - Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs.
* [Sublime Text Editor](https://www.sublimetext.com/) - Sublime Text is a sophisticated text editor for code, markup and prose

Stack will be updated as and when new libraries are added to project.

### Installation | Ubuntu 

Update and upgrade operating system.

```

sudo apt-get update && sudo apt-get -y upgrade

```


It is recommended by the Flask Software Foundation to use Python 3, so once everything is updated, we can install Python 3 by using the following command:

```

sudo apt-get install python3

```


Now that we have Python 3 installed, we will also need pip in order to install packages from PyPi, PythonÃ¢â‚¬â„¢s package repository.

```

sudo apt-get install -y python3-pip

```


virtualenv is a virtual environment where you can install software and Python packages in a contained development space, which isolates the installed software and packages from the rest of your machineÃ¢â‚¬â„¢s global environment. This convenient isolation prevents conflicting packages or software from interacting with each other.

To install virtualenv, we will use the pip3 command, as shown below:

```

pip3 install virtualenv

```


Now, create a virtual environment within the project directory by typing:

```

virtualenv newenv

```


To install packages into the isolated environment, you must activate it by typing:

```

source newenv/bin/activate

```


All the depedencies for backend is stored in requirements.txt file.
Go to codebase and find requirement.txt file
Install all dependencies from requirements file to virtualenv.

```

pip3 install -r requirements.txt

```

Repository Maintainer 
---------------------

* Gururaj Jeerge 
