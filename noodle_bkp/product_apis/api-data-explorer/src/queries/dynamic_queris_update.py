#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code | Update query
    Functionality:
        dynamic_queris_update: create update query [single entry]
        correct_updating_value: update query assignment values [single entry]
"""

from .dynamic_data_type import get_char_set_type


def dynamic_queris_update(
        table_name,
        data_values,
        where_query):
    """
    Create update clause with field list

    Args:
        table_name (str): table name
        data_values (str): data values
        where_query (str): where clause

    Returns:
        query (str): Part query as update clause.
    """
    query = " UPDATE " + table_name + " SET " + data_values
    query = query + where_query + ';'
    return query


def correct_updating_value(data_values=[], column_names=[], column_type={}):
    """
    correcting all update parameters with data types association

    Args:
        data_values (list): list of data values
        column_names (list): list of column names
        column_type (dict): key value pair of column names and data types

    Returns:
        data_values_return (str): Corrected data values.
    """
    data_values_return = ""

    for index, element in enumerate(column_names):
        data_values_return = data_values_return + \
            element + "=" + get_char_set_type(
                column_type.get(element), data_values[index])
        data_values_return = data_values_return + \
            ',' if not index == len(column_names) - 1 else data_values_return

    return data_values_return
