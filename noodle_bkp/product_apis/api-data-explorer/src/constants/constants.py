#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources Constant declaration
    Functionality:
        Declare project variable to access table names in code base

"""

EXPLORERSTATE = '[NOODLEAPP].[dbo].[ExplorerStat]'
EXPLORERSTATE_TABLE = 'ExplorerStat'

# ------------------------------------------------------------------------------
# DATA PIPELINE CONSTANTS
# ------------------------------------------------------------------------------

PIPELINE_DATASET_ID = 'DatasetId'
PIPELINE_TARGET_TABLE_NAME = 'DbTargetTableName'
PIPELINE_PIPELINE_ID = 'PipelineId'

PIPELINE_HOST = 'DbTargetHost'
PIPELINE_DATABASE = 'DbTargetName'
PIPELINE_USERNAME = 'DbTargetUsername'
PIPELINE_PASSWORD = 'DbTargetPassword'
