#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client resources API code project starter
    Functionality:
        Create flask application
        Create routes for resources views
        Define swagger
"""

from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger
from resource.data_explorer_list import DataExplorer
from resource.data_explorer_list_stat import DataExplorerStats
from resource.health import Health

from config.config import DEFAULT_IP

app = Flask(__name__)

api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="MVP DataExplorer API Documentation",
    resourcePath="/data-explorer"
)

# http://server/api/v1/data-explorer/data/ +\
# datastore/<string:datastore_id>/dataset/ +\
# <string:dataset_id>/records
api.add_resource(
    DataExplorer,
    '/api/v1/data-explorer/data/datastore/' +
    '<string:datastore_id>/dataset/' +
    '<string:dataset_id>/records')

# http://server/api/v1/data-explorer/statdata/datastore/ +\
# <string:datastore_id>/dataset/<string:dataset_id>/stats
api.add_resource(
    DataExplorerStats,
    '/api/v1/data-explorer/statdata/datastore/' +
    '<string:datastore_id>/dataset/' +
    '<string:dataset_id>/stats')

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8000, debug=True)
