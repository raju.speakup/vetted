import os
from dotenv import load_dotenv


APP_ROOT = os.path.join(os.path.dirname(__file__), '..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)

DOMAIN = os.getenv("CATLOG_DOMAIN")
PIPELINE_ABSTRACT = DOMAIN + os.getenv("PIPELINE_ABSTRACT")
PIPELINE_STATS = DOMAIN + os.getenv("PIPELINE_STATS")
