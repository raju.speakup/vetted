#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data-Explorer resources API code
    Functionality:
        get: get ExplorerStat details [multiple entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    return_result_list)

from config.config import (
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,
)

from constants.return_error_constants import (
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_409_CONFLICT)

from constants.error_constants import (
    NO_VALID_JSON,
    NO_PIPELINE_AVALABLE
)

from externalentity.url_declration import (
    PIPELINE_STATS
)
# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
import requests
from logger.logger import logger
# ------------------------------------------------------------------------------
# NOODLE CUSTOME PACKAGE IMPORTS
# ------------------------------------------------------------------------------
from dbconnector import dbconnector


class DataExplorerStats(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connector = dbconnector(server_name=SERVER_NAME,
                                     database_name=DB_NAME,
                                     username=USERNAME,
                                     password=PASSWORD)
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True},
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True}],
        notes='Table Stats')
    def get(self, datastore_id, dataset_id):
        """Get Stats.

        Args:
            datastore_id - datastore id
            dataset_id - dataset id

        Returns:
            json: Returns json object of records in
            ExplorerStat table else empty data object

        """
        data_pipe_data = []
        url_generation = PIPELINE_STATS + \
            str(datastore_id) + '/' + 'datasets' + '/' + \
            str(dataset_id) + '/' + 'stats' + '/'
        logger.info(url_generation)
        try:
            data_pipe = requests.get(url_generation)
        except requests.exceptions.ConnectionError as err:
            self.error = err.args[0]
            self.error_code = NO_PIPELINE_AVALABLE
            logger.error(err.args[0])
            return return_result_list(
                result=data_pipe_data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_409_CONFLICT

        try:
            data_pipe_data = data_pipe.json() if data_pipe.ok else []
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            logger.error(err.args[0])
            return return_result_list(
                result=data_pipe_data,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        return return_result_list(
            result=data_pipe_data.get('data'),
            error=self.error,
            error_code=self.error_code
        ), HTTP_200_OK
