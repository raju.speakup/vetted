#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Data-Explorer resources API code
    Functionality:
        get: get ExplorerStat details [multiple entry]
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMMPORTS
# ------------------------------------------------------------------------------
from commons.utils import (
    convert_to_json,
    return_result_list,
    get_url_args)

from queries.dynamic_queris_read import (
    dynamic_queris_read,
    get_sorting_dict,
    between_query,
    where_query
)

from constants.error_constants import (
    DATABASE_CONNECTION_ISSUE,
    NO_VALID_JSON,
    DATA_NOT_FOUND_IN_TABLE,
    NO_PIPELINE_AVALABLE
)

from constants.return_error_constants import (
    HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_409_CONFLICT)

# ------------------------------------------------------------------------------
# PYTHON FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from flask import request
import requests

# ------------------------------------------------------------------------------
# NOODLE CUSTOME PACKAGE IMPORTS
# ------------------------------------------------------------------------------
from dbconnector import dbconnector
import pymssql
from logger.logger import logger

from externalentity.url_declration import (
    PIPELINE_ABSTRACT
)


class DataExplorer(Resource):
    """Client associated functional class.

    Args:
        No arguments - Create a instance of class and use functions

    Returns:
        Function specific results

    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.error = None
        self.error_code = None
        self.column_names = []
        self.sort_key = list()

    @swagger.operation(
        parameters=[
            {
                "name": "datastore_id",
                "dataType": 'int',
                "paramType": "path",
                "required": True,
                "description": "DataStore Id"
            },
            {
                "name": "dataset_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Dataset ID"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
            }
        ],
        notes='List all data in ExplorerStat table',
        nickname='List Records')
    def get(self, datastore_id, dataset_id):
        """Get Records.

        Args:
            dataset_id - bulk result
            table_name -  table name

        Returns:
            json: Returns json object of records in
            ExplorerStat table else empty data object

        """
        result = []
        try:
            data_pipe = requests.get(PIPELINE_ABSTRACT)
        except requests.exceptions.ConnectionError as err:
            self.error = err.args[0]
            self.error_code = NO_PIPELINE_AVALABLE
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_409_CONFLICT

        try:
            data_pipe_data = data_pipe.json() if data_pipe.ok else []
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        table_name = ''
        database = ''
        password = ''
        server = ''
        username = ''

        try:
            for entry in data_pipe_data.get('data'):
                did = entry.get('id')
                if did == datastore_id:
                    credentials = entry.get('targetDetails')
                    database = credentials.get('dbTargetName')
                    password = credentials.get('dbTargetPassword')
                    server = credentials.get('dbTargetHost')
                    username = credentials.get('dbTargetUsername')
                    for subentry in entry.get('datasets'):
                        dtt = subentry.get('id')
                        if dtt == dataset_id:
                            table_name = subentry.get('dbTargetTableName')
        except ValueError as err:
            self.error = err.args[0]
            self.error_code = NO_VALID_JSON
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_400_BAD_REQUEST

        self.connector = dbconnector(
            server_name=server,
            database_name=database,
            username=username,
            password=password)

        result = []
        limit, offset, sort_key, projection = get_url_args(req=request)

        # # Get Column names of table
        try:
            column_names = [x.lower()
                            for x in self.connector.columnnames(
                table_name)]
        except pymssql.Error as err:
            self.error = err.args[0]
            self.error_code = DATABASE_CONNECTION_ISSUE
            logger.error(err.args[0])
            return return_result_list(
                result=result,
                error=self.error,
                error_code=self.error_code
            ), HTTP_500_INTERNAL_SERVER_ERROR

        # # Generate conditional queries
        between_fields = []
        where_filds = []
        sort_key = list()

        if column_names:
            sort_key.append(column_names[0])

            # # Generate query
            query = dynamic_queris_read(
                table_name=table_name,
                between_query=between_query(fields=between_fields),
                where_query=where_query(fields=where_filds),
                column_names=column_names,
                sorting=get_sorting_dict(fields=sort_key),
                limit=limit,
                offset=offset
            )
            logger.info(query)

            # # Get result from db
            try:
                db_result = self.connector.execute(query)
            except pymssql.Error as err:
                self.error = err.args[0]
                self.error_code = DATABASE_CONNECTION_ISSUE
                logger.error(err.args[0])
                return return_result_list(
                    result=result,
                    error=self.error,
                    error_code=self.error_code
                ), HTTP_500_INTERNAL_SERVER_ERROR

            # # Convert result to json
            if db_result:
                result = convert_to_json(
                    rows=db_result[0],
                    column_names=column_names
                )
        else:
            self.error = 'No Data in Table'
            self.error_code = DATA_NOT_FOUND_IN_TABLE

        return return_result_list(
            result=result,
            error=self.error,
            error_code=self.error_code), HTTP_200_OK
