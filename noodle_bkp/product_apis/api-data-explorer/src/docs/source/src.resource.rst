src\.resource package
=====================

Submodules
----------

src\.resource\.client\_resource\_id module
------------------------------------------

.. automodule:: src.resource.client_resource_id
    :members:
    :undoc-members:
    :show-inheritance:

src\.resource\.client\_resource\_list module
--------------------------------------------

.. automodule:: src.resource.client_resource_list
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.resource
    :members:
    :undoc-members:
    :show-inheritance:
