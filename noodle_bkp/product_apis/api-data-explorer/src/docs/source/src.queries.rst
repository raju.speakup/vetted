src\.queries package
====================

Submodules
----------

src\.queries\.dynamic\_data\_type module
----------------------------------------

.. automodule:: src.queries.dynamic_data_type
    :members:
    :undoc-members:
    :show-inheritance:

src\.queries\.dynamic\_queris\_delete module
--------------------------------------------

.. automodule:: src.queries.dynamic_queris_delete
    :members:
    :undoc-members:
    :show-inheritance:

src\.queries\.dynamic\_queris\_read module
------------------------------------------

.. automodule:: src.queries.dynamic_queris_read
    :members:
    :undoc-members:
    :show-inheritance:

src\.queries\.dynamic\_queris\_update module
--------------------------------------------

.. automodule:: src.queries.dynamic_queris_update
    :members:
    :undoc-members:
    :show-inheritance:

src\.queries\.dynamic\_queris\_write module
-------------------------------------------

.. automodule:: src.queries.dynamic_queris_write
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.queries
    :members:
    :undoc-members:
    :show-inheritance:
