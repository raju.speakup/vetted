#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource, reqparse
from flask import request, jsonify
from flask_restful_swagger import swagger
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.decorators import validate_json, validate_schema, validate_user, compare_user_role
from commons.util import return_result_final
from commons.code_snippets import get_list, add_audit_log
from configuration.layer_fetch_environ import (
    TOKEN_EXPIRE_TIME,
    LDAP_URL,
    REFRESH_EXPIRE_TIME)
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED,
    HTTP_203_NON_AUTHORITATIVE_INFORMATION,
    HTTP_403_FORBIDDEN)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.login_swagger import ChangePassword
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    jwt_refresh_token_required
)
from commons.ldap import authenticate

# ------------------------------------------------------------------------------
# PYTHON FUNCTIONS
# ------------------------------------------------------------------------------
import uuid
import json
import datetime
import bcrypt


class ChangePassword(Resource):
    """
    Super Admin password change
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": ChangePassword.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Change Password',
        nickname='Change Password')
    def post(self):
        """ Change Password"""
        parser = reqparse.RequestParser()
        parser.add_argument(
            'email', help='This field cannot be blank', required=True)
        parser.add_argument(
            'oldPassword', help='This field cannot be blank', required=True)
        parser.add_argument(
            'newPassword', help='This field cannot be blank', required=True)
        data = parser.parse_args()

        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"email": data.get('email'), "isActive": True})
            logger.info(return_object)

        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                "Sorry! Something went wrong with Authentication service",
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if return_object:
            if return_object['type'] == "AD":
                #dn = str.replace(data.get('email'), '@noodle.ai', '@ad.noodle.ai')
                result, message, username = authenticate(
                    LDAP_URL, data.get('email'), data.get('oldPassword'))
                if not result:
                    return return_result_final(
                        data,
                        message,
                        HTTP_403_FORBIDDEN
                    ), HTTP_403_FORBIDDEN
            else:
                if not bcrypt.checkpw(
                    data.get('oldPassword').encode('utf-8'),
                    return_object['password'].encode('utf-8')
                ):
                    return return_result_final(
                        data,
                        "Email and password combination is wrong",
                        HTTP_403_FORBIDDEN
                    ), HTTP_403_FORBIDDEN

            return_object.pop('_id')
            if 'password' in return_object.keys():
                return_object.pop('password')
            data.update(return_object)
            #set_access_cookies(data, accesstoken)
            password_hash = bcrypt.hashpw(
                data.get(
                    'newPassword').encode('utf-8'),
                bcrypt.gensalt())
            try:
                self.connection.update_one(
                    collectionname="users",
                    data=({"email": data.get('email')}, {
                        "$set": {"password": password_hash.decode('utf-8')}}))
                logger.info(data)
                audit_data = {
                              "action": "password changed",
                              "time": str(datetime.datetime.now())}
                add_audit_log(self, 'auditLogs', audit_data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data.get('email'),
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
            return return_result_final(
                json.dumps([{"message": "Password is changed"}]),
                self.error,
                self.error_code), HTTP_200_OK

        else:
            return return_result_final(
                data,
                "{0} client do not exists".format(data.get('email')),
                "EMAIL_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        return data, HTTP_200_OK
