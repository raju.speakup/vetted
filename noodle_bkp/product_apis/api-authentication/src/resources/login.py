#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""User API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource, reqparse
from flask import request, jsonify
from flask_restful_swagger import swagger

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from services.login import LoginService
from configuration.layer_fetch_environ import (
    TOKEN_EXPIRE_TIME,
    REFRESH_EXPIRE_TIME)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from swagger.login_swagger import Login, Registration
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_refresh_token_required
)

# ------------------------------------------------------------------------------
# PYTHON FUNCTIONS
# ------------------------------------------------------------------------------
import datetime


class UserLogin(Resource):
    """UserLogin.
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = LoginService()

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": Login.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='User Login',
        nickname='User Login')
    def post(self):
        """ User Login"""
        parser = reqparse.RequestParser()
        parser.add_argument(
            'email', help='This field cannot be blank', required=True)
        parser.add_argument(
            'password', help='This field cannot be blank', required=True)
        data = parser.parse_args()
        output = self.service.user_login(data)
        return output


class SecretKey(Resource):
    """SecretKey."""

    @swagger.operation(
        parameters=[],
        notes='Secrete Get',
        nickname='Secrete')
    def get(self):
        """Get."""
        import os
        import binascii
        key = binascii.hexlify(os.urandom(24))
        return {
            'secreteKey': str(key)
        }


class Refresh(Resource):
    """Refresh Token."""

    @jwt_refresh_token_required
    def post(self):
        """Post."""
        data = {}
        current_user = get_jwt_identity()
        expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
        refresh_expires = datetime.timedelta(minutes=REFRESH_EXPIRE_TIME)

        data.update({
            'accessToken': str(create_access_token(identity=current_user, expires_delta=expires)),
            'refreshToken': str(create_refresh_token(identity=current_user, expires_delta=refresh_expires))
        })
        return jsonify(data)

