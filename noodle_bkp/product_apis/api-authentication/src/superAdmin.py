#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Super Admin Creation API
    REST EndPoints:
    post
"""
from logger.logger import logger

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND, HTTP_201_CREATED,
    HTTP_203_NON_AUTHORITATIVE_INFORMATION,
    HTTP_403_FORBIDDEN)

# ------------------------------------------------------------------------------
# PYTHON FUNCTIONS
# ------------------------------------------------------------------------------
import uuid
import json
import bcrypt
import datetime


class SuperAdmin:
    """SuperAdmin.
    """

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def create_super_admin(self):
        """ Create Super Admin """

        data ={
                'email': 'superadmin@noodle.ai',
                'isActive': True,
                'password': 'Noodleadmin1+',
                'type': "DB",
                "userRole": "super-admin",
                "isEnabled": True,
                "firstName": "super",
                "lastName": "admin"

            }
        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"email": data['email'], "isActive": True})
            logger.info(return_object)

        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                "Sorry! Something went wrong with Mongo service",
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            password_hash = bcrypt.hashpw(
                data.get(
                    'password').encode('utf-8'),
                bcrypt.gensalt())
            userid = str(uuid.uuid4())
            data.update(
                {
                    'user_id': userid,
                    'password': password_hash.decode('utf-8'),
                    'created_at': str(datetime.datetime.now())
                })
            try:
                self.connection.insert_one(
                    collectionname="users",
                    data=json.dumps(data))
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
            if 'password' in data.keys():
                data.pop('password')
            try:
                self.connection.insert_one(
                    collectionname="permissions",
                    data=json.dumps({
                        "clientid": "*",
                        "user_id": userid,
                        "entityId": "*",
                        "entityType": "*",
                        "permission": "*",
                        "isActive": True
                    }))
                logger.info(data)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    data,
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
            return data, HTTP_201_CREATED
        return data, HTTP_200_OK


sa = SuperAdmin()

sa.create_super_admin()


