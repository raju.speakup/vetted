#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS
from functools import wraps
from constants.custom_field_error import HTTP_400_BAD_REQUEST,HTTP_200_OK,HTTP_401_UNAUTHORIZED
from jsonschema import validate, ValidationError
from flask import (
    jsonify,
    request,
)
from resources.health import Health
from resources.debug import ConfigStore, CheckDbStatus, ServiceStatus
from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG
from resources.login import (
    UserLogin,
    SecretKey,
    Refresh)
from resources.changePassword import ChangePassword
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

from commons.mongo_services import NoodleMongoClient
from configuration.layer_fetch_environ import SECRETE_KEY

app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="MVP Authentication Service",
    resourcePath="/Auth"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)
jwt._set_error_handler_callbacks(app)
app.config['PROPAGATE_EXCEPTIONS'] = True
blacklist = set()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """check_if_token_in_blacklist."""
    jti = decrypted_token['jti']
    return jti in blacklist


@jwt.expired_token_loader
def my_expired_token_callback():
    return jsonify({
        'status': 403,
        'msg': 'The refresh token has expired'
    }), 403


@jwt.user_claims_loader
def add_claims_to_access_token(user_id):
    """add_claims_to_access_token.

    Connect to mongo authorization service
    and get permission information of user
    """
    connection = NoodleMongoClient()
    return_object = connection.find_selected_field(
        collectionname="users",
        data={
            "user_id": user_id
        },
        projection={"userRole": 1}
           )
    if return_object:
        return {'user_id': user_id,
                'userRole': return_object[0]['userRole']
               }


class LogOutAccess(Resource):
    """Logout."""

    #@jwt_required
    def delete(self):
        """Delete token."""
        #jti = get_raw_jwt()['jti']
        #blacklist.add(jti)
        return jsonify({"msg": "Successfully Removed Access Token"})


class LogOutRefresh(Resource):
    """Logout."""

    @jwt_refresh_token_required
    def delete(self):
        """delete."""
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return jsonify({"msg": "Successfully Removed Refresh Token"})


# ------------------------------------------------------------------------------
# Health services
# ------------------------------------------------------------------------------

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

# http://server/api/v1/debug/config_store
api.add_resource(
    ConfigStore,
    '/api/v1/debug/config_store')

# http://server/api/v1/debug/check_db_status
api.add_resource(
    CheckDbStatus,
    '/api/v1/debug/check_db_status')

# http://server/api/v1/debug/service_status
api.add_resource(
    ServiceStatus,
    '/api/v1/debug/service_status')


# ------------------------------------------------------------------------------
# Authentication services
# ------------------------------------------------------------------------------

# http://server/api/v1/login
api.add_resource(UserLogin, '/api/v1/login')

# http://server/api/v1/refresh
api.add_resource(Refresh, '/api/v1/refresh_access_token')

# http://server/api/v1/logoutaccess
api.add_resource(LogOutAccess, '/api/v1/logoutaccess')

# http://server/api/v1/logoutrefresh
api.add_resource(LogOutRefresh, '/api/v1/logoutrefresh')

# http://server/api/v1/secretekey
api.add_resource(SecretKey, '/api/v1/secretekey')

# ------------------------------------------------------------------------------
# Super Admin services
# ------------------------------------------------------------------------------

# http://server/api/v1/change_password
api.add_resource(ChangePassword, '/api/v1/change_password')

if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8016, debug=True)
