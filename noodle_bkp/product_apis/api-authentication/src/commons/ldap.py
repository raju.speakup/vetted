#!/usr/bin/env python
import ldap
from constants.custom_field_error import HTTP_404_NOT_FOUND
from commons.util import return_result_final


def authenticate(address, email, password):
    """Authenticate."""
    conn = ldap.initialize('ldap://' + address)
    conn.protocol_version = 3
    conn.set_option(ldap.OPT_REFERRALS, 0)

    base = "dc=prod,dc=ad,dc=noodle,dc=ai"
    criteria = "(&(objectClass=user))"
    attributes = ['CN']
    cn = email.split("@")[0]
    try:
        conn.simple_bind_s(email, password)

    except ldap.INVALID_CREDENTIALS:
        return False, "Invalid credentials", cn
    except ldap.SERVER_DOWN:
        return False, "Server down", cn
    except ldap.LDAPError as e:
        if type(e.message) == dict and 'desc' in e.message:
            return "Other LDAP error: " + e.message['desc'], cn
        else:
            return False, "Other LDAP error: " + e, cn
    try:
        result = conn.search_s(base, ldap.SCOPE_SUBTREE, criteria, attributes)
        result.pop()
        username = [entry['cn'][0].decode("utf-8") for dn, entry in result if entry['cn'][0].decode("utf-8") == cn][0]
    except:
        username = cn
    finally:
        conn.unbind_s()

    return True, "Successfully authenticated", username


def get_user_name(address, email):
    """get user first name and last name"""

    domain = email.split("@")[1]
    if domain == 'ad.noodle.ai':
        base = "dc=ad,dc=noodle,dc=ai"
        address = 'ad.noodle.ai'
    else:
        base = "dc=prod,dc=ad,dc=noodle,dc=ai"
        address = 'prod.ad.noodle.ai'

    conn = ldap.initialize('ldap://' + address)
    conn.protocol_version = 3
    conn.set_option(ldap.OPT_REFERRALS, 0)

    attributes = ['*']
    cn = email.split("@")[0]

    criteria = "(&(objectClass=user)(sAMAccountName=" + cn + "))"

    conn.simple_bind_s('beastadmin@prod.ad.noodle.ai', 'Noodleadmin1+')
    result = conn.search(base, ldap.SCOPE_SUBTREE, criteria, attributes)

    conn_type, user = conn.result(result, 60)
    name, attrs = user[0]

    if 'sn' in attrs.keys():
        last_name = attrs['sn'][0].decode("utf-8")
    else:
        last_name = ''
    if 'givenName' in attrs.keys():
        first_name = attrs['givenName'][0].decode("utf-8")
    else:
        first_name = ''

    return first_name, last_name