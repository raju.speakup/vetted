#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_swagger import swagger


@swagger.model
class Login:
    """Login fields."""

    resource_fields = {
        'email': 'fields.String',
        'password': 'fields.String'
    }


@swagger.model
class Registration:
    """Registration Fields."""
    resource_fields = {
        'username': 'fields.String',
        'password': 'fields.String',
        'email': 'fields.String',
        'firstName': 'fields.String',
        'lastName': 'fields.String',
        'type': 'fields.String',
        'userRole': 'fields.String',
        'clientid': 'fields.String'
    }

@swagger.model
class ChangePassword:
    """Change password Fields."""
    resource_fields = {
        'email': 'fields.String',
        'oldPassword': 'fields.String',
        'newPassword': 'fields.String',
    }