from config_store import config_store_client as conf_client
from logger.logger import logger


class Config:
    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='api-authentication')
        logger.info("load config completed")

    def get_v(self, key):
        return self.cs.get(key)

    def get_env(self):
        return self.cs.get_environment_props()

    def reload(self):
        self.cs.load(ip='*', service_name='api-authentication')
