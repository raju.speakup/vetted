#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Fetch all confidencial data from and environment file
    Functionality:
        - If any manupulations needed in environment setting
        this is place to do it

"""
# ------------------------------------------------------------------------------
# IMPORT SECTION
# ------------------------------------------------------------------------------

#import os

#from dotenv import load_dotenv
from configuration.config import Config

# ------------------------------------------------------------------------------
# CONFIGURING ENVIRONMENT FILE AND FETCHING DATA
# ------------------------------------------------------------------------------

#APP_ROOT = os.path.join(os.path.dirname(__file__), '../..')
#dotenv_path = os.path.join(APP_ROOT, '.env')
#load_dotenv(dotenv_path)


# ------------------------------------------------------------------------------
# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES
# ------------------------------------------------------------------------------

#SERVER_NAME = os.getenv(os.getenv("MODE") + '_' + "SERVER_NAME")
#DB_NAME = os.getenv(os.getenv("MODE") + '_' + "DB_NAME")
#USERNAME = os.getenv(os.getenv("MODE") + '_' + "USERNAME")
#PASSWORD = os.getenv(os.getenv("MODE") + '_' + "PASSWORD")
#PORT = int(os.getenv(os.getenv("MODE") + '_' + "PORT"))
#SESSION_TIMEOUT = int(os.getenv(os.getenv("MODE") + '_' + "SESSION_TIMEOUT"))
#DEFAULT_IP = os.getenv("DEFAULT_IP")
#LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")
#MODE = os.getenv("MODE")
#DEBUG = True if os.getenv("MODE") == 'LOCAL' else False
#SECRETE_KEY = os.getenv("SECRET_KEY", default="some-difficult-value")
#TOKEN_EXPIRE_TIME = int(os.getenv("TOKEN_EXPIRE_TIME", default=15))
#REFRESH_EXPIRE_TIME = int(os.getenv("REFRESH_EXPIRE_TIME", default=120))
#LDAP_URL = os.getenv("LDAP_URL", default="localhost")


# ASSIGNMENT SECTION FOR PROJECT GROBAL VARIABLES USING CONFIG STORE
# ------------------------------------------------------------------------------

conf = Config()

SERVER_NAME = conf.get_v('SERVER_NAME')
DB_NAME = conf.get_v("DB_NAME")
USERNAME = conf.get_v("USERNAME")
PASSWORD = conf.get_v("PASSWORD")
PORT = int(conf.get_v("PORT"))
DEFAULT_IP = conf.get_v("DEFAULT_IP")
MODE = conf.get_v("MODE")
DEBUG = True if conf.get_v("MODE") == 'LOCAL' else False
SECRETE_KEY = conf.get_v("SECRETE_KEY")
TOKEN_EXPIRE_TIME = int(conf.get_v("TOKEN_EXPIRE_TIME"))
REFRESH_EXPIRE_TIME = int(conf.get_v("REFRESH_EXPIRE_TIME"))
SESSION_TIMEOUT = int(conf.get_v("SESSION_TIMEOUT"))
LDAP_URL = conf.get_v("LDAP_URL")
CONF_STORE_URL, CONF_ENV = conf.get_env()
