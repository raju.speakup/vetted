# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.json_utils import to_json
from commons.code_snippets import add_audit_log
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_403_FORBIDDEN)
from logger.logger import logger
from flask_jwt_extended import create_refresh_token, create_access_token
from configuration.layer_fetch_environ import LDAP_URL, TOKEN_EXPIRE_TIME, REFRESH_EXPIRE_TIME
from commons.ldap import authenticate, get_user_name
# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError
# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import bcrypt
import datetime


class LoginService:
    """Login Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def user_login(self, data):
        if len(data.get('password')) == 0:
            return to_json({"message": "Email and password combination is wrong"}, is_error=True), HTTP_403_FORBIDDEN

        try:
            return_object = self.connection.find_one(
                collectionname="users",
                data={"email": data.get('email'), "isActive": True})
            logger.info(return_object)

        except PyMongoError as err:
            logger.error(err)
            return to_json({"message": err.args[0]}, is_error=True), HTTP_500_INTERNAL_SERVER_ERROR

        if return_object:
            if return_object['isEnabled'] is False:
                return to_json({"message": "User has been disabled"}, is_error=True), HTTP_403_FORBIDDEN

            if return_object['type'] == "AD":
                email = str.replace(data.get('email'), '@noodle.ai', '@ad.noodle.ai')
                result, message, username = authenticate(
                    LDAP_URL, email, data.get('password'))
                if not result:
                    return to_json({"message": message}, is_error=True), HTTP_403_FORBIDDEN
            else:
                if not bcrypt.checkpw(
                    data.get('password').encode('utf-8'),
                    return_object['password'].encode('utf-8')
                ):
                    return to_json({"message": "Email and password combination is wrong"}, is_error=True),\
                           HTTP_403_FORBIDDEN

            return_object.pop('_id')
            if 'password' in return_object.keys():
                return_object.pop('password')
            data.update(return_object)

            expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
            refresh_expires = datetime.timedelta(minutes=REFRESH_EXPIRE_TIME)
            access_token = create_access_token(
                identity=data['user_id'], expires_delta=expires)
            refresh_token = create_refresh_token(
                identity=data['user_id'], expires_delta=refresh_expires)

            from apps import add_claims_to_access_token
            add_claims_to_access_token(return_object)
            data.update({'accessToken': access_token,
                         'accesstokenExpTime': TOKEN_EXPIRE_TIME,
                         'refreshToken': refresh_token,
                         'refreshTokenExpTime': REFRESH_EXPIRE_TIME,
                         })
            if 'password' in data.keys():
                data.pop('password')
            #set_access_cookies(data, accesstoken)
            audit_data = {"user_id": data['user_id'],
                          "action": "login",
                          "time": str(datetime.datetime.now())}
            add_audit_log(self, 'auditLogs', audit_data)

        else:
            output, status = self.create_guest_user(data)
            if not status:
                return to_json({"message": output}, is_error=True), HTTP_403_FORBIDDEN
            else:
                userid = output

            expires = datetime.timedelta(minutes=TOKEN_EXPIRE_TIME)
            refresh_expires = datetime.timedelta(minutes=REFRESH_EXPIRE_TIME)
            access_token = create_access_token(
                identity=userid, expires_delta=expires)
            refresh_token = create_refresh_token(
                identity=userid, expires_delta=refresh_expires)

            from apps import add_claims_to_access_token
            add_claims_to_access_token(return_object)
            data.update({'accessToken': access_token,
                         'accesstokenExpTime': TOKEN_EXPIRE_TIME,
                         'refreshToken': refresh_token,
                         'refreshTokenExpTime': REFRESH_EXPIRE_TIME,
                         })
            if 'password' in data.keys():
                data.pop('password')
        return data, HTTP_200_OK

    def create_guest_user(self, data):
        email = str.replace(data.get('email'), '@noodle.ai', '@ad.noodle.ai')
        result, message, username = authenticate(
            LDAP_URL, email, data.get('password'))
        if not result:
            return message, False
        first_name, last_name = get_user_name(LDAP_URL, email)
        userid = str(uuid.uuid4())
        data.pop('password')
        data.update(
            {
                'user_id': userid,
                'isActive': True,
                'type': "AD",
                "userRole": "guest",
                'userName': username,
                'firstName': first_name,
                'lastName': last_name,
                'isEnabled': True

            })
        try:
            self.connection.insert_one(
                collectionname="users",
                data=json.dumps(data))
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return err.args[0], False
        if 'password' in data.keys():
            data.pop('password')
        try:
            self.connection.insert_one(
                collectionname="permissions",
                data=json.dumps({
                    "clientid": "*",
                    "user_id": userid,
                    "entityId": "*",
                    "entityType": "*",
                    "permission": "read",
                    "isActive": True
                }))
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return err.args[0], False
        audit_data = {"user_id": userid,
                      "action": "guest user created",
                      "time": str(datetime.datetime.now())}
        add_audit_log(self, 'auditLogs', audit_data)

        return userid, True
