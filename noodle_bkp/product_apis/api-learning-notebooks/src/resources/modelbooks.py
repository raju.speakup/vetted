# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.modelbooks import ModelService
from src.utils.json_validator import validate_schema
from src.schemas.modelbook_schema import id_path_schema
from flask import request
import json
import datetime
from src.utils.config import conf
from flask_restful_swagger import swagger
from src.swagger.learning_notebook_swagger import NoteBook

class ModelBooksResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = ModelService(db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "mb_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "ModelBook ID"
            }
        ],
        nickname="Get ModelBook ",
        notes="Get ModelBook by id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, mb_id):
        client_id = request.args.get("client_id", None)
        app_id = request.args.get("app_id", None)
        modelbook = self.service.get_modelbook(mb_id)
        return modelbook

    @swagger.operation(
        parameters=[
            {
                "name": "mb_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "ModelBook ID"
            }
        ],
        nickname="Delete ModelBook",
        notes="Delete ModelBook by id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, mb_id):
        modelbook = self.service.delete_modelbook(mb_id)
        return modelbook

    @swagger.operation(
        nickname="Update ModelBook",
        notes="Update ModelBook by ID",
        parameters=[
            {
                "name": "mb_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "DataWrangling-book ID"
            },
            {
                "name": "parameter",
                "dataType": NoteBook.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, mb_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_modelbook(data, mb_id)
        return update_response
