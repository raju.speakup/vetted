# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request
import datetime

from src.services.modelbooks import ModelService
from src.services.nb_service import NBService
from src.utils.json_validator import validate_schema
from src.schemas.modelbook_schema import modelbook_schema, list_arg_schema
from src.utils.constants import Const
from src.services.S3Service import S3Service
from src.utils.config import conf

class ModelBookBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = ModelService(db_name)

    def put(self):
        data = json.loads(request.data.decode('utf-8'))
        action = request.args.get("action", 'get')

        if action == 'delete':
            ids = data['ids']
            delete_response = self.service.delete_modelbooks(ids)
            return delete_response

        return []