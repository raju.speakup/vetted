# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.commit_service import CommitService
from flask import request
import json
import datetime
from src.utils.config import conf
from src.services.nb_service import NBService


class NBCommitResource(Resource):

    def __init__(self):
        self.git_base_path = conf.get_v('git_base_dir')
        db_name = conf.get_v('db.name')
        self.commit_service = CommitService(db_name)
        self.nb_service = NBService()

    @staticmethod
    def change_version(self, url, new_version):
        tokens = str(url).split('_')
        tokens[-1] = str(new_version) + ".ipynb"
        return "_".join(tokens)

    # create new version from the current contents of notebook
    def post(self, entity_id):
        data = json.loads(request.data.decode('utf-8'))

        nb_url = data['notebook_file_path']

        new_nb_url = nb_url
        tokens = str(nb_url).split('_')
        tokens[-1]

        nb_api_url = nb_url.replace("test/notebooks", "test/api/contents")
        notebook_content = self.nb_service.get_content_from_notebook(nb_api_url)

        message = request.args.get("message", "")
        version_number = request.args.get("version_number", "v0")
        author = request.args.get("user_id", "unknown")

        item = {}
        item['entity_id'] = entity_id
        item['version_number'] = version_number
        item['author'] = author
        item['committed_datetime'] = str(datetime.datetime.utcnow())
        item['size'] = len(notebook_content)
        item['message'] = message
        item['notebook_content'] = notebook_content
        item['notebook_url'] = nb_url

        self.commit_service.create_commit(item)
        return item

    # override current notebook contents against revert_to_version_number or select notebook contect of select_version
    def put(self, entity_id):
        revert_to_version_number = request.args.get('revert_to_version_number', None)
        select_version = request.args.get('select_version', None)

        if revert_to_version_number is not None and select_version is not None:
            return {'error': 'cant select both revert_to_version_number and select_version'}

        if revert_to_version_number is not None:
            commit = self.commit_service.get_commit(entity_id, revert_to_version_number)
            notebook_api_url = commit['data']['notebook_url'].replace("test/notebooks", "test/api/contents")
            notebook_content = self.nb_service.get_content_from_notebook(notebook_api_url)

            author = request.args.get("user_id", "unknown")

            item = {}
            item['entity_id'] = entity_id
            item['version_number'] = revert_to_version_number
            item['author'] = author
            item['committed_datetime'] = str(datetime.datetime.utcnow())
            item['size'] = len(notebook_content)
            item['notebook_content'] = notebook_content
            item['notebook_url'] = commit['data']['notebook_url']

            self.commit_service.update_commit(entity_id, revert_to_version_number, item)

            return {'status': "success", "notebook_url": commit['data']['notebook_url']}

        if select_version is not None:
            commit = self.commit_service.get_commit(entity_id, select_version)
            notebook_api_url = commit['data']['notebook_url'].replace("test/notebooks", "test/api/contents")
            status = self.nb_service.override_notebook(notebook_api_url, commit['data']['notebook_content'])

            return {'status': status, "notebook_url": commit['data']['notebook_url']}

        return {"status": "failed"}

    # list all the versions of a notebook along with its content so that UI can diff
    def get(self, entity_id):
        limit = request.args.get("limit", "10")
        offset = request.args.get("offset", "0")
        version_number = request.args.get("version_number", None)
        history = self.commit_service.list_commit(entity_id, int(limit), int(offset), version_number)
        return history