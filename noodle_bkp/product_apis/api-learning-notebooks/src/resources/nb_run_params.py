# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.nb_runs import NBRunService
from src.schemas.nbrun_schema import id_path_schema
from flask import request
import json
import datetime
import logging
from src.utils.config import conf

class NBRunParamsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = NBRunService(db_name)


    def get(self, entity_id):
        record = {}
        try:
            record = self.service.get_params(entity_id)
        except Exception as e:
            record["error"] = str(e)

        return record

    def put(self, entity_id):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        }

        if 'app_id' in data:
            data_ext['app_id'] = str(data['app_id'])

        if 'eng_id' in data:
            data_ext['eng_id'] = str(data['eng_id'])

        if 'input_params' in data:
            data_ext['input_params'] = data['input_params']

        if 'output_params' in data:
            data_ext['output_params'] = data['output_params']

        if 'output_chars_path' in data:
            data_ext['output_chars_path'] = data['output_chars_path']

        if 'run_status' in data:
            data_ext['run_status'] = data['run_status']

        output = self.service.update_params(data_ext, entity_id)
        return output
