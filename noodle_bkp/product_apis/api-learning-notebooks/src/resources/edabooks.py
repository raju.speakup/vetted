# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.eda_books import EDABooksService
from src.utils.json_validator import validate_schema
from src.schemas.edabook_schema import id_path_schema
from src.utils.config import conf
from flask import request
import json
import datetime


class EDABooksResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = EDABooksService(db_name)

    @validate_schema(id_path_schema(), is_path=True)
    def get(self, eda_id):
        edabook = self.service.get_edabook(eda_id)
        return edabook

    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, eda_id):
        edabooks = self.service.delete_edabook(eda_id)
        return edabooks

    @validate_schema(id_path_schema(), is_path=True)
    def put(self, eda_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_edabook(data, eda_id)
        return update_response
