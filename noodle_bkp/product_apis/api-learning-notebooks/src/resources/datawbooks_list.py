# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
import uuid
import json
from flask import request
import datetime
from flask import g

from src.services.datawbooks import DataWBooksService
from src.services.nb_service import NBService
from src.utils.json_validator import validate_schema
from src.schemas.datawbook_schema import datawbook_schema, list_arg_schema
from src.services.S3Service import S3Service
from src.utils.config import conf
from src.services.nb_runs import NBRunService
from src.services.commit_service import CommitService
from flask_restful_swagger import  swagger
from swagger.learning_notebook_swagger import NoteBook

class DataWBookListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataWBooksService(db_name)
        self.nb_service = NBService()
        self.s3service = S3Service()
        self.nb_run_service = NBRunService(db_name)
        self.nb_commit_service = CommitService(db_name)

    @swagger.operation(
        nickname="Get List of DataWrangling Books",
        notes="Get List of DataWrangling Books",
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "keyword",
                "dataType": 'string',
                "paramType": "query",
                "description": "Takes a keyword to search"
            }
        ]
    )
    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        datawbook = self.service.list_datawbooks(limit, offset, keyword)
        return datawbook

    @swagger.operation(
        nickname="Create DataWrangling Book",
        notes="Create DataWrangling Book",
        parameters=[
            {
                "name": "parameter",
                "dataType": NoteBook.__name__,
                "paramType": "body",
                "description": ""
            }
        ]
    )
    @validate_schema(datawbook_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))
        jupyterhub_nb_folder = conf.get_v('jupyterhub.nb_folder')

        user_name = request.headers.get('userName')


        data_ext = {
            'dw_id': str(uuid.uuid4()),
            'name': str(data['name']),
            'description': str(data['description']),
            'sample_notebook_path': None,
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': user_name,
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': user_name
        }

        if 'notebook_file_path' not in data or len(str(data['notebook_file_path'])) < 10:
            notebook_name = data_ext['name'].replace(' ', '_')
            data_ext['notebook_file_path'] = jupyterhub_nb_folder + notebook_name + ".ipynb"
            self.nb_service.create(data_ext['dw_id'], notebook_name, data_ext['sample_notebook_path'])
        else:
            data_ext['notebook_file_path'] = str(data['notebook_file_path'])

        if 'sample_notebook_path' in data:
            data_ext['sample_notebook_path'] = str(data['sample_notebook_path'])

        if 'app_id' in data:
            data_ext['app_id'] = str(data['app_id'])
        else:
            data_ext['app_id'] = str(g.app_id)

        if 'client_id' in data:
            data_ext['client_id'] = str(data['client_id'])
        else:
            data_ext['client_id'] = str(g.client_id)

        if 'snapshot_url' in data:
            data_ext['snapshot_url'] = str(data['snapshot_url'])

        datawbook = self.service.create_datawbook(data_ext)

        self.create_and_save_run(data_ext)
        self.create_and_save_commit(data_ext)

        return datawbook, 201

    def create_and_save_run(self, data_ext):
        run_doc = {
            'r_id': str(uuid.uuid4()),
            'version': 0,
            'entity_id': str(data_ext['dw_id']),
            'entity_name': str('dw'),
            'status': str('new'),
            'error_message': None,
            'input_params': {},
            'input_datamarts': [],
            'input_cartridges': [],
            'output_datamarts': [],
            'output_params': {},
            'output_chars_path': [],
            'notebook_file_path': str(data_ext['notebook_file_path']),
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str(data_ext['created_by']),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str(data_ext['created_by'])
        }

        if 'app_id' in data_ext:
            run_doc['app_id'] = str(data_ext['app_id'])
        else:
            run_doc['app_id'] = str(g.app_id)

        if 'client_id' in data_ext:
            run_doc['client_id'] = str(data_ext['client_id'])
        else:
            run_doc['client_id'] = str(g.client_id)
        run_out = self.nb_run_service.create_nbrun(run_doc)

    def create_and_save_commit(self, data):
        user_name = conf.get_v('jupyterhub.user')
        nb_url = data['notebook_file_path']

        nb_api_url = nb_url.replace(user_name+"/notebooks", user_name+"/api/contents")
        notebook_content = self.nb_service.get_content_from_notebook(nb_api_url)

        item = {}
        item['entity_id'] = str(data['dw_id'])
        item['version_number'] = "0"
        item['author'] = data['created_by']
        item['committed_datetime'] = str(datetime.datetime.utcnow())
        item['size'] = len(notebook_content)
        item['message'] = "first commit"
        item['notebook_content'] = notebook_content
        item['notebook_url'] = nb_url

        run_out = self.nb_commit_service.create_commit(item)