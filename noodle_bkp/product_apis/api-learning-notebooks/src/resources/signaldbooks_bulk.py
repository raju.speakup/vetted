# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
import json
from flask import request

from src.services.signal_detection_books import SignalDBooksService
from src.utils.config import conf


class SignalDBookBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = SignalDBooksService(db_name)

    def put(self):
        data = json.loads(request.data.decode('utf-8'))
        action = request.args.get("action", 'get')

        if action == 'delete':
            ids = data['ids']
            delete_response = self.service.delete_signaldbooks(ids)
            return delete_response

        return []