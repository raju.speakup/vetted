# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.utils.json_utils import to_json
from src.services.S3Service import S3Service
from src.utils.constants import Const
from flask import request
import json


class PickleSourceResource(Resource):

    def __init__(self):
        self.s3service = S3Service()

    def get(self):
        results = self.s3service.list_bucket_objects(bucketName=Const.S3_BUCKET_NAME)
        return to_json(results)

    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        results = {}
        try:
            if 'bucket_name' in data and 'filter' in data:
                results = self.s3service.list_bucket_objects(bucketName=data['bucket_name'], filter=data['filter'])
                return to_json(results)
        except Exception as e:
            return to_json(str(e), True)
