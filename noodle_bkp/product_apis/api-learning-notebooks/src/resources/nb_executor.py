# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.nb_runs import NBRunService
from src.utils.json_validator import validate_schema
from src.schemas.nbrun_schema import id_path_schema
from flask import request
import json
import datetime
from src.utils.config import conf
from flask import g

class NBExecutorResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = NBRunService(db_name)

    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
            'entity_id': str(data['entity_id']),
            'notebook_file_path': str(data['notebook_file_path']),
            'input_params': data['input_params'],
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str('unknown'),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        }

        if 'app_id' in data:
            data_ext['app_id'] = str(data['app_id'])
        else:
            data_ext['app_id'] = str(g.app_id)

        if 'client_id' in data:
            data_ext['client_id'] = str(data['client_id'])
        else:
            data_ext['client_id'] = str(g.client_id)

        data_ext['input_datamarts'] = data['input_datamarts'] if 'input_datamarts' in data else []
        data_ext['input_cartridges'] = data['input_cartridges'] if 'input_cartridges' in data else []
        data_ext['output_datamarts'] = data['output_datamarts'] if 'output_datamarts' in data else []

        output = self.service.nb_executor_async_task(data_ext)
        return output

    def put(self):
        jupyterhub_nb_folder = conf.get_v('jupyterhub.nb_folder')
        data = json.loads(request.data.decode('utf-8'))
        notebook_url = str(data['notebook_file_path'])
        self.service.save_as_readonly(notebook_url, jupyterhub_nb_folder)
        return {'status': 'success'}