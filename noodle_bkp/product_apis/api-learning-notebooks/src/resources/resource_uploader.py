from flask_restful import Resource
import json
import requests
from flask import request
from src.utils.config import conf

ALLOWED_EXTENSIONS = set(['txt', 'ipynb', 'py'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def identical_extension(file1, file2):
    return file1.rsplit('.', 1)[1].lower() == file2.rsplit('.', 1)[1].lower()


class UploadResource(Resource):
    def __init__(self):
        # self.token = "c73c5793484d0bee618e6b82adc439883a7652403e4d2731"
        # self.host = "localhost"
        # self.port = "8888"
        # self.username = ""
        # self.userlabel = ""
        self.token = conf.get_v('jupyterhub.api_token')  # from config store
        self.server_name = conf.get_v('jupyterhub.server_name')
        self.username = "test"
        self.userlabel = "user"

    def post(self, filename):
        if 'file' not in request.files:
            return {"status": 'No file part'}, 400
        file = request.files['file']
        if file.filename == '':
            return {"status": 'No selected file'}, 400
        if not (allowed_file(file.filename) and allowed_file(filename) and identical_extension(file.filename,filename)):
            return {"status": "Invalid file extension"}, 400

        if not ('filetype' in request.form and 'path' in request.form):
            return {"status": "Invalid parameters"}, 400
        if file:
            # target_filename = file.filename
            target_filename = filename
            url_sub = "notebooks"
            file_type = request.form['filetype']
            path = request.form['path']
            if len(path) > 0 and path[len(path)-1] != '/':
                path = path + '/'
            try:
                if file_type == "file":
                    content = file.read().decode('utf8')
                    file_format = "text"
                    url_sub = "edit"
                elif file_type == "notebook":
                    content = json.load(file)
                    file_format = "json"
                else:
                    return {"status": "Invalid parameters"}, 400

                url = "http://" + self.server_name + "/" + self.userlabel + "/" + self.username + "/api/contents/" + path + target_filename
                file_path = "http://" + self.server_name + "/" + self.userlabel + "/" + self.username + "/" + url_sub + "/" + path + target_filename
                # file_path = "http://" + self.host + ":" + self.port + "/" + url_sub + "/" + path + target_filename
                # url = "http://" + self.host + ":" + self.port +  "/api/contents/" + path + target_filename
                jup_payload = {
                    "name": str(target_filename),
                    "path": str(path) + str(target_filename),
                    "type": str(file_type),
                    "format": str(file_format),
                    "content": content
                }
                jup_payload_str = json.dumps(jup_payload)
                headers = {
                    'content-type': "application/json",
                    'authorization': str(self.token),
                    'cache-control': "no-cache"
                }
                querystring = {"token": self.token}

                response = requests.request("PUT", url, data=jup_payload_str, headers=headers, params=querystring)
                if response.ok:
                    return {"status": "file created/updated successfully", "path": file_path}, 200
                else:
                    return {"status": "couldn't upload into jupyter"}, 403

            except Exception as e:
                return {"status": "exception raised in server", "exception" : str(e)}, 400

        return {"status": "failed to upload file into jupyter"}, 400
