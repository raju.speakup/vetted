# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
import uuid
import json
from flask import request
import logging
import time
from src.services.nb_runs import NBRunService
from src.services.nb_service import NBService
from src.utils.constants import Const
from src.services.S3Service import S3Service
from src.utils.config import conf
from src.utils.mongo_db import db_client
from src.utils.nlogger import logger
from bat_auth_sdk import role_validator
from flask_jwt_extended import jwt_required


class AdminResource(Resource):

    def __init__(self):
        self.service = NBRunService("test_database")
        self.nb_service = NBService()
        self.s3service = S3Service()
        role_validator.init(conf.get_v("db.url"), conf.get_v("db.name"))

#    @role_validator.validate_app_user()
    def get(self):
        logger.log().info("refresh completed at " + time.asctime())
        conf.reload()
        db_client.reconnect()
        response = {'status': 'success',
                    'db_name': db_client.url,
                    'is_db_connected': db_client.is_connected(),
                    'log_name': logger.log().getLogger().name,
                    'log_level': logger.log().getLogger().getEffectiveLevel()}
        return response
