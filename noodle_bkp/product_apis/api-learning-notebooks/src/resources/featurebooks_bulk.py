# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.featurebooks import FeatureBooksService
from src.utils.json_validator import validate_schema
from src.schemas.featurebook_schema import id_path_schema
from flask import request
import json
import datetime
from src.utils.config import conf


class FeatureBooksBulkResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = FeatureBooksService(db_name)

    def put(self):
        data = json.loads(request.data.decode('utf-8'))
        action = request.args.get("action", 'get')

        if action == 'delete':
            ids = data['ids']
            delete_response = self.service.delete_featurebooks(ids)
            return delete_response

        return []