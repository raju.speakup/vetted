# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import request
import datetime
from flask import g

from src.services.eda_books import EDABooksService
from src.utils.json_validator import validate_schema
from src.schemas.edabook_schema import list_arg_schema
from src.utils.config import conf


class EDABookListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = EDABooksService(db_name)

    @validate_schema(list_arg_schema(), is_arg=True)
    def get(self, explorer_id, dataset_id):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        keyword = request.args.get("keyword", "")
        edabook = self.service.list_edabooks(explorer_id, dataset_id, limit, offset, keyword)
        return edabook
