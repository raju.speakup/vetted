# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.signal_detection_books import SignalDBooksService
from src.utils.json_validator import validate_schema
from src.schemas.signaldbook_schema import id_path_schema
from flask import request
import json
import datetime
from src.utils.config import conf
from flask_restful_swagger import swagger
from src.swagger.learning_notebook_swagger import NoteBook

class SignalDBooksResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = SignalDBooksService(db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "sd_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "SignalDBooks ID"
            }
        ],
        nickname="Get SignalDBooks ",
        notes="Get SignalDBooks by id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, sd_id):
        signaldbook = self.service.get_signaldbook(sd_id)
        return signaldbook

    @swagger.operation(
        parameters=[
            {
                "name": "sd_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "SignalDBooks ID"
            }
        ],
        nickname="Delete SignalDBooks ",
        notes="Delete SignalDBooks by id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, sd_id):
        signaldbook = self.service.delete_signaldbook(sd_id)
        return signaldbook

    @swagger.operation(
        nickname="Update SignalDBooks",
        notes="Update SignalDBooks by ID",
        parameters=[
            {
                "name": "sd_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "SignalDBooks ID"
            },
            {
                "name": "parameter",
                "dataType": NoteBook.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, sd_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at']= str(datetime.datetime.utcnow())
        update_response = self.service.update_signaldbook(data, sd_id)
        return update_response
