# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.nb_runs import NBRunService
from src.utils.json_validator import validate_schema
from src.schemas.nbrun_schema import id_path_schema
from flask import request
import json
import datetime
from src.utils.config import conf


class NBRunsResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = NBRunService(db_name)

    @validate_schema(id_path_schema(), is_path=True)
    def get(self, r_id):
        nbrun = self.service.get_nbrun(r_id)
        return nbrun

    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, r_id):
        nbrun = self.service.delete_nbrun(r_id)
        return nbrun

    @validate_schema(id_path_schema(), is_path=True)
    def put(self, r_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_nbrun(data, r_id)
        return update_response
