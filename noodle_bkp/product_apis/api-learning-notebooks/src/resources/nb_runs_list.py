# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
import uuid
import json
from flask import request
import datetime

from src.services.nb_runs import NBRunService
from src.services.nb_service import NBService
from src.utils.json_validator import validate_schema
from src.schemas.nbrun_schema import nbrun_schema, list_arg_schema
from src.utils.constants import Const
from src.services.S3Service import S3Service
from src.utils.config import conf
from flask import g


class NBRunsListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = NBRunService(db_name)
        self.nb_service = NBService()
        self.s3service = S3Service()

    def get(self):
        limit = request.args.get("limit", 10)
        offset = request.args.get("offset", 0)
        entity_id = request.args.get("entity_id", None)
        nb_run = self.service.list_nbrun(limit, offset, entity_id)
        return nb_run

    @validate_schema(nbrun_schema())
    def post(self):
        data = json.loads(request.data.decode('utf-8'))

        data_ext = {
            'r_id': str(uuid.uuid4()),
            'version': str(data['version']),
            'entity_id': str(data['entity_id']),
            'entity_name': str(data['entity_name']),
            'run_status': str(data['run_status']),
            'error_message': str(data['error_message']),
            'input_params': data['input_params'],
            'output_params': data['output_params'],
            'output_chars_path': data['output_chars_path'],
            'notebook_file_path': str(data['notebook_file_path']),
            'created_at': str(datetime.datetime.utcnow()),
            'created_by': str('unknown'),
            'updated_at': str(datetime.datetime.utcnow()),
            'updated_by': str('unknown')
        }

        if 'app_id' in data:
            data_ext['app_id'] = str(data['app_id'])
        else:
            data_ext['app_id'] = str(g.app_id)

        if 'client_id' in data:
            data_ext['client_id'] = str(data['client_id'])
        else:
            data_ext['client_id'] = str(g.client_id)

        nb_run = self.service.create_nbrun(data_ext)
        return nb_run
