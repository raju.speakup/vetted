# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.featurebooks import FeatureBooksService
from src.utils.json_validator import validate_schema
from src.schemas.featurebook_schema import id_path_schema
from flask import request
import json
import datetime
from src.utils.config import conf
from flask_restful_swagger import swagger
from src.swagger.learning_notebook_swagger import NoteBook

class FeatureBooksResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = FeatureBooksService(db_name)

    @swagger.operation(
        nickname="Get Feature Books",
        notes="Get Feature Books by ID",
        parameters=[
            {
                "name": "fb_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Feature-book ID"

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, fb_id):
        featurebook = self.service.get_featurebook(fb_id)
        return featurebook

    @swagger.operation(
        nickname="Delete Feature-Book",
        notes="Delete Feature-Book by ID",
        parameters=[
            {
                "name": "fb_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Feature-book ID"

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, fb_id):
        featurebooks = self.service.delete_featurebook(fb_id)
        return featurebooks

    @swagger.operation(
        nickname="Update Feature-book",
        notes="Update Feature-book by ID",
        parameters=[
            {
                "name": "fb_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "Feature-book ID"
            },
            {
                "name": "parameter",
                "dataType": NoteBook.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, fb_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_featurebook(data, fb_id)
        return update_response
