# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.utils.json_utils import to_json
from src.services.nb_service import NBService
from src.services.featurebooks import FeatureBooksService
from src.services.S3Service import S3Service
from src.utils.config import conf
from  flask_restful_swagger import swagger

class NotebookResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.nbservice = NBService()
        self.fbservice = FeatureBooksService(db_name)
        self.s3service = S3Service()

    @swagger.operation(
        nickname="Note Book",
        notes="Note Book",
        parameters=[
            {
                "name": "fb_id",
                "dataType": "string",
                "paramType": "body",
                "required": True,
                "description": "Feature-book ID"

            }
        ]
    )
    def get(self, fb_id):
        fb = self.fbservice.get_featurebook(fb_id)
        output_features = self.s3service.list_features("feature-engineering-out/"+fb['data']['destination_file_path'])
        result = self.fbservice.update_featurebook({'output_features': output_features}, fb_id)
        return to_json(result)

    def post(self, fb_id):
        results = self.nbservice.create(None, "fe56848", "in", "out")
        return to_json(results)
