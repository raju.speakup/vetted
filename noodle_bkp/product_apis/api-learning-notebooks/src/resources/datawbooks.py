# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.services.datawbooks import DataWBooksService
from src.utils.json_validator import validate_schema
from src.schemas.datawbook_schema import id_path_schema
from src.utils.config import conf
from flask import request
import json
import datetime
from flask_restful_swagger import swagger
from src.swagger.learning_notebook_swagger import NoteBook

class DataWBooksResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.service = DataWBooksService(db_name)

    @swagger.operation(
        parameters=[
            {
                "name": "dw_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "DataWrangling book ID"
            }
        ],
        nickname="Get Data Wrangling Books",
        notes="Get Data Wrangling Books by id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def get(self, dw_id):
        datawbook = self.service.get_datawbook(dw_id)
        return datawbook

    @swagger.operation(
        parameters=[
            {
                "name": "dw_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "DataWrangling book ID"
            }
        ],
        nickname="Delete Data Wrangling Books",
        notes="Delete Data Wrangling Book by id"
    )
    @validate_schema(id_path_schema(), is_path=True)
    def delete(self, dw_id):
        datawbooks = self.service.delete_datawbook(dw_id)
        return datawbooks

    @swagger.operation(
        nickname="Update DataWrangling-book",
        notes="Update DataWrangling-book by ID",
        parameters=[
            {
                "name": "dw_id",
                "dataType": "string",
                "paramType": "path",
                "required": True,
                "description": "DataWrangling-book ID"
            },
            {
                "name": "parameter",
                "dataType": NoteBook.__name__,
                "paramType": "body",
                "required": True,
                "description": ""

            }
        ]
    )
    @validate_schema(id_path_schema(), is_path=True)
    def put(self, dw_id):
        data = json.loads(request.data.decode('utf-8'))

        data['updated_at'] = str(datetime.datetime.utcnow())
        update_response = self.service.update_datawbook(data, dw_id)
        return update_response
