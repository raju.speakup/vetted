# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from src.utils.json_utils import to_json
from src.services.nb_packages import NBPackagesService
from src.utils.config import conf
from  flask_restful_swagger import swagger
from flask import request
import json


class NBPackagesListResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.package_service = NBPackagesService(db_name)

    @swagger.operation(
        nickname="List Notebook Packages",
        notes="List Notebook Packages",
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            }
        ]
    )
    def get(self):
        limit = request.args.get("limit", 30)
        offset = request.args.get("offset", 0)
        result = self.package_service.get_nb_packages(limit, offset)
        return result

    @swagger.operation(
        nickname="Add Notebook Package",
        notes="Add Notebook Package"
    )
    def post(self):
        package = json.loads(request.data.decode('utf-8'))
        result = self.package_service.add_nb_packages(package)
        return result


class NBPackagesResource(Resource):

    def __init__(self):
        db_name = conf.get_v('db.name')
        self.package_service = NBPackagesService(db_name)

    @swagger.operation(
        nickname="Update Notebook Packages",
        notes="Update Notebook Packages",
        parameters=[
            {
                "name": "pkg_id",
                "dataType": 'string',
                "paramType": "path",
                "description": "Package ID"
            }
        ]
    )
    def put(self, pkg_id):
        package = json.loads(request.data.decode('utf-8'))
        result = self.package_service.update_nb_package(pkg_id, package)
        return result

    @swagger.operation(
        nickname="Delete Notebook Packages",
        notes="Delete Notebook Packages",
        parameters=[
            {
                "name": "pkg_id",
                "dataType": 'string',
                "paramType": "path",
                "description": "Package ID"
            }
        ]
    )
    def delete(self, pkg_id):
        result = self.package_service.delete_nb_package(pkg_id)
        return result
