import logging


class NLogger():

    def __init__(self):
        logging.basicConfig(filename='/tmp/api-learning-notebooks.log', level=logging.DEBUG)

    def log(self):
        return logging

logger = NLogger()