#!/bin/sh

if [ -f "$1" ]
then
	notebook_url=$1
	notebook_name=$(basename $notebook_url)
	python_ext=".py"
	echo "notebook execution started"
	echo "step 1: reading parameters"
	echo "notebook file:"$notebook_url"<=="
	echo "notebook name:"$notebook_name"<=="
else
	echo "notebook path does not exist"
	exit 1
fi

echo "step 2: converting the notebook to python file"
jupyter nbconvert --to python $notebook_url  

echo "step 3: calling the web socket for executing the code"
python_file=${notebook_url/.ipynb/$python_ext}

cd /var/www/html/api-learning-notebooks/src/scripts/kg-websocket

echo "local file location to run notebook is "$python_file
docker-compose run -d -v /var/log:/var/ client_v1 --times=1 --code="`cat "$python_file"`"

echo "notebook ["$notebook_name"] execution completed"