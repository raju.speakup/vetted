#!/bin/sh

if [ -f "$1" ]
then
	notebook_url=$1
	notebook_name=$(basename $notebook_url)
	python_ext=".html"
	echo "notebook execution started"
	echo "step 1: reading parameters"
	echo "notebook file:"$notebook_url"<=="
	echo "notebook name:"$notebook_name"<=="

	echo "step 2: converting the notebook to html file"
    jupyter nbconvert $notebook_url

    echo "notebook conversion completed"
else
	echo "notebook path does not exist"
	exit 1
fi