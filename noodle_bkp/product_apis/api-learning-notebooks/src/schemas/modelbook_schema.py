from src.utils.json_validator import Schema, Prop


def modelbook_schema():
    prop = {
        "name": Prop().string().max(255).min(3).build(),
        "description": Prop().string().max(1000).build(),
        "source_file_path": Prop().string().max(1000).min(0).build(),
        "destination_file_path": Prop().string().max(1000).min(0).build(), #TODO: make it optionl
        "notebook_file_path": Prop().string().max(1000).min(0).build(),
        "sample_notebook_path": Prop().string().max(1000).min(0).build(),
        "status": Prop().string().max(255).min(0).build(),
        "app_id": Prop().string().max(255).min(0).build(),
        "eng_id": Prop().string().max(255).min(0).build(),
        "snapshot_url": Prop().string().max(255).min(0).build()
    }

    return Schema().keys(prop).required(["name"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build(),
        "keyword": Prop().string().max(125).min(0).build()
    }

    return Schema().keys(prop).required([]).build()

def id_path_schema():
    prop = {
        "mb_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required([]).build()