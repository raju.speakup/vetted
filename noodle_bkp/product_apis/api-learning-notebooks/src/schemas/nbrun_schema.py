from src.utils.json_validator import Schema, Prop


def nbrun_schema():
    prop = {
        "version": Prop().string().max(255).min(0).build(),
        "input_params": Prop().object().build(),
        "output_params": Prop().object().build(),
        "output_chars_path": Prop().object().build(),
        "notebook_file_path": Prop().string().max(1000).min(0).build(),
        "run_status": Prop().string().max(255).min(0).build(),
        "entity_id": Prop().string().max(255).min(0).build(),
        "entity_name": Prop().string().max(255).min(0).build(),
        "app_id": Prop().string().max(255).min(0).build(),
        "eng_id": Prop().string().max(255).min(0).build(),
        "error_message": Prop().string().max(2000).min(0).build()
    }

    return Schema().keys(prop).required(["entity_id"]).build()


def list_arg_schema():
    prop = {
        "limit": Prop().string().max(5).min(1).build(),
        "offset": Prop().string().max(5).min(1).build()
    }

    return Schema().keys(prop).required([]).build()

def id_path_schema():
    prop = {
        "r_id": Prop().string().max(100).min(1).build()
    }

    return Schema().keys(prop).required([]).build()