from flask import Flask, g
from flask import request
from flask_restful import Api
from flask_restful_swagger import swagger
from src.resources.notebook import NotebookResource
from src.resources.picklesource import PickleSourceResource

from flask_jwt_extended import JWTManager

from src.resources.featurebooks import FeatureBooksResource
from src.resources.featurebooks_list import FeatureBookListResource
from src.resources.featurebooks_bulk import FeatureBooksBulkResource

from src.resources.datawbooks import DataWBooksResource
from src.resources.datawbooks_list import DataWBookListResource
from src.resources.datawbooks_bulk import DataWBookBulkResource


from src.resources.modelbooks import ModelBooksResource
from src.resources.modelbooks_list import ModelBookListResource
from src.resources.modelbooks_bulk import ModelBookBulkResource

from src.resources.signaldbooks import SignalDBooksResource
from src.resources.signaldbooks_list import SignalDBookListResource
from src.resources.signaldbooks_bulk import SignalDBookBulkResource

from src.resources.edabooks import EDABooksResource
from src.resources.edabooks_list import EDABookListResource
from src.resources.edabooks_bulk import EDABookBulkResource
from src.resources.edabooks_create import EDABookCreateResource

from src.resources.nb_executor import  NBExecutorResource
from src.resources.nb_commit import NBCommitResource

from src.resources.nb_runs import NBRunsResource
from src.resources.nb_runs_list import NBRunsListResource
from src.resources.nb_run_params import NBRunParamsResource

from src.resources.resource_uploader import UploadResource
from src.resources.nb_packages import NBPackagesListResource, NBPackagesResource

from flask_cors import CORS
from src.utils.constants import con
from src.resources.admin import AdminResource
from src.utils.nlogger import logger
from flask_jwt_extended import jwt_required
import re

SECRETE_KEY = 'some-difficult-value'

app = Flask(__name__)
CORS(app)


@app.before_first_request
def global_init():
    logger.log().info("started initialising")

    logger.log().info("completed initialising")


@app.before_request
def validate_auth():
    if (not "/admin/_refresh" == request.path) and (not re.compile('/api/spec*').match(request.path)):
        check_auth()


@jwt_required
def check_auth():
    print("jwt token validated successfully")
    g.client_id = request.headers.get('clientid')
    g.app_id = request.headers.get('appid')
    print("serving request for client_id {} and app_id {}".format(request.headers.get('clientid'), request.headers.get('appid')))

api = swagger.docs(
    Api(app), apiVersion='0.1',
    basePath="http://localhost:5000/api/v0/",
    api_spec_url='/api/spec',
    description="docs for Learning Layer API's",
    resourcePath="/learnings"
)

api.add_resource(NotebookResource, '/feature-books/<string:fb_id>/_publish')
api.add_resource(PickleSourceResource, '/feature-books/_sources')
api.add_resource(FeatureBooksResource, '/feature-books/<string:fb_id>')
api.add_resource(FeatureBookListResource, '/feature-books')
api.add_resource(FeatureBooksBulkResource, '/feature-books/_bulk')

api.add_resource(DataWBookBulkResource, '/dataw-books/_bulk')
api.add_resource(DataWBooksResource, '/dataw-books/<string:dw_id>')
api.add_resource(DataWBookListResource, '/dataw-books')

api.add_resource(ModelBookBulkResource, '/model-books/_bulk')
api.add_resource(ModelBooksResource, '/model-books/<string:mb_id>')
api.add_resource(ModelBookListResource, '/model-books')

api.add_resource(SignalDBookBulkResource, '/signald-books/_bulk')
api.add_resource(SignalDBooksResource, '/signald-books/<string:sd_id>')
api.add_resource(SignalDBookListResource, '/signald-books')

api.add_resource(EDABookBulkResource, '/eda-books/_bulk')
api.add_resource(EDABooksResource, '/eda-books/<string:eda_id>')
api.add_resource(EDABookListResource, '/eda-books/explorer-id/<string:explorer_id>/dataset-id/<string:dataset_id>')
api.add_resource(EDABookCreateResource, '/eda-books')

api.add_resource(NBExecutorResource, '/nb-runs/_run')
api.add_resource(NBRunParamsResource, '/nb-runs/params/<string:entity_id>')
api.add_resource(NBRunsResource, '/nb-runs/<string:r_id>')
api.add_resource(NBRunsListResource, '/nb-runs')

api.add_resource(UploadResource, '/upload_resource/<string:filename>')
api.add_resource(NBPackagesListResource, '/nb_packages')
api.add_resource(NBPackagesResource, '/nb_packages/<string:pkg_id>')

api.add_resource(NBCommitResource, '/nb-commits/<string:entity_id>')

api.add_resource(AdminResource, '/admin/_refresh')

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
app.config['PROPAGATE_EXCEPTIONS'] = True

jwt = JWTManager(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=con.API_PORT, debug=True)