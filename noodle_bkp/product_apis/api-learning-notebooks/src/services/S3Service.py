from os import getenv
import boto3
import pandas as pd
import _pickle as cPickle


class S3Service:

    def __init__(self):
        self.s3 = boto3.resource('s3')

    def list_bucket_objects(self, bucketName, filter="data-wrangling-out"):
        results = []
        bucket_contents = self.s3.Bucket(bucketName)
        try:
            for object in bucket_contents.objects.filter(Prefix=filter):
                results.append(object.key)
        except Exception as e:
            print(e)
            return e

        return results

    def list_features(self, s3FileName):
        s3 = boto3.client('s3')
        resource = boto3.resource('s3')
        bucket = resource.Bucket('noodleai-postman-collections')

        obj = s3.get_object(Bucket='noodleai-postman-collections', Key=s3FileName)
        string_pkl = obj['Body'].read()
        df = cPickle.loads(string_pkl)
        return list(df)