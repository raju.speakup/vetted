from git import Repo, Git
from src.utils.config import conf
from src.utils.json_utils import to_json, to_list_json
from src.utils.mongo_db import db_client
import os
import requests
from src.utils.nlogger import logger


class CommitService:

    def __init__(self, db_name):
        self.base_dir = conf.get_v('git_base_dir')
        self.db_name = db_name
        self.db_client = db_client

    def create_commit(self, commit):
        id = self.db_client.insert_one(self.db_name, "nb_commits", commit).inserted_id
        del commit['_id']
        return to_json(commit)

    def update_commit(self, entity_id, version_number, commit):
        output = self.db_client.find_one_and_update(self.db_name, "nb_commits", {'entity_id': entity_id, 'version_number': version_number}, {'$set':commit})
        del output['_id']
        return to_json(output)

    def list_commit(self, entity_id, limit, offset, version_number = None):
        query1 = {'entity_id': entity_id}
        if version_number is not None:
            query1 = {'entity_id': entity_id, 'version_number': version_number}

        result, count = self.db_client.find(self.db_name, "nb_commits", query=query1, offset=offset, limit=limit)
        output = []
        for commit in result:
            del commit['_id']
            output.append(commit)

        return to_list_json(output, list_count=count)

    def get_commit(self, entity_id, version_number):
        result, count = self.db_client.find(self.db_name, "nb_commits", query={'entity_id': entity_id, 'version_number': version_number})
        output = {}
        for commit in result:
            del commit['_id']
            output = commit

        return to_json(output)