import http.client
from src.utils.constants import Const
from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
import uuid


class NBPackagesService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.collection_name = 'nb_packages'

    def get_nb_packages(self,limit, offset):
        result, count = self.client.find(self.db_name, self.collection_name,
                                         {}, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def add_nb_packages(self, package):
        package.update({"pkg_id": str(uuid.uuid4())})
        id = self.client.insert_one(self.db_name, self.collection_name, package).inserted_id
        del package['_id']
        return to_json(package)

    def update_nb_package(self, pkg_id, package):
        output = self.client.find_one_and_update(self.db_name, self.collection_name,
                                                 {'pkg_id': pkg_id}, {'$set': package})
        del output['_id']
        return to_json(output)

    def delete_nb_package(self, pkg_id):
        output = self.client.find_one_and_delete(self.db_name, self.collection_name,
                                                 {'pkg_id': pkg_id})
        return to_json(output)

