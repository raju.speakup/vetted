from src.utils.mongo_db import db_client
import json
from bson import json_util
from pymongo.errors import PyMongoError
from src.utils.json_utils import to_json, to_list_json
from flask import g

class FeatureBooksService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.collection_name = "featurebooks"

    def search_query(self, keyword):
        return {"$or":[{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query]}

    def list_featurebooks(self, limit, offset, keyword=""):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.client.find(self.db_name, self.collection_name, query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_featurebook(self, id):
        query = self.find_query(g.client_id, g.app_id, {'fb_id': id})
        output, count =self.client.find(self.db_name, self.collection_name, query)
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_featurebook(self, newObj, id):
        output = self.client.find_one_and_update(self.db_name, self.collection_name, {'fb_id': id}, {'$set':newObj})
        del output['_id']
        return to_json(output)

    def delete_featurebook(self, id):
        result = self.client.find_one_and_delete(self.db_name, self.collection_name, {'fb_id': id})
        return to_json(result)

    def delete_featurebooks(self, ids):
        results = []
        for id in ids:
            result = self.client.find_one_and_delete(self.db_name, self.collection_name, {'fb_id': id})
            results.append(result)
        return results

    def create_featurebook(self, app):
        id = self.client.insert_one(self.db_name, self.collection_name, app).inserted_id
        del app['_id']
        return to_json(app)