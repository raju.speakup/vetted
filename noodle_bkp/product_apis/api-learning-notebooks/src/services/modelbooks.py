from src.utils.mongo_db import db_client
import json
from bson import json_util
from pymongo.errors import PyMongoError
from src.utils.json_utils import to_json, to_list_json
from flask import g

class ModelService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.collection_name = "modelbooks"

    def search_query(self, keyword):
        return {"$or":[{'name': {'$regex': keyword}}, {'description': {'$regex': keyword}}]}

    def find_query(self, client_id, app_id, query):
        if query is None:
            return {"$and":[{'client_id': client_id}, {'app_id': app_id}]}
        return {"$and":[{'client_id': client_id}, {'app_id': app_id}, query]}

    def list_modelbooks(self, limit, offset, keyword):
        query = self.find_query(g.client_id, g.app_id, self.search_query(keyword))
        result, count = self.client.find(self.db_name, self.collection_name, query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_modelbook(self, id):
        query = self.find_query(g.client_id, g.app_id, {'mb_id': id})
        output, count =self.client.find(self.db_name, self.collection_name, query)
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_modelbook(self, newObj, id):
        output = self.client.find_one_and_update(self.db_name, self.collection_name, {'mb_id': id}, {'$set':newObj})
        del output['_id']
        return to_json(output)

    def delete_modelbook(self, id):
        result = self.client.find_one_and_delete(self.db_name, self.collection_name, {'mb_id': id})
        return to_json(result)

    def delete_modelbooks(self, ids):
        results = []
        for id in ids:
            result = self.client.find_one_and_delete(self.db_name, self.collection_name, {'mb_id': id})
            results.append(result)
        return results

    def create_modelbook(self, app):
        id = self.client.insert_one(self.db_name, self.collection_name, app).inserted_id
        del  app['_id']
        return to_json(app)