from src.utils.mongo_db import db_client
from src.utils.json_utils import to_json, to_list_json
import uuid
import datetime
import os
import requests
import json
from src.utils.config import conf
from pymongo import DESCENDING
from src.utils.config import conf
from celery_tasks import celery

class NBRunService:

    def __init__(self, db_name):
        self.db_name = db_name
        self.client = db_client
        self.sign = conf.get_v('NB_CELERY_SIGN')

    def list_nbrun(self, limit, offset, entity_id=None):

        query = {}
        if entity_id is not None:
            query = {'entity_id': entity_id}

        result, count = self.client.find(self.db_name, "nbruns", query, offset=offset, limit=limit)
        output = []
        for service in result:
            del service['_id']
            output.append(service)

        return to_list_json(output, list_count=count)

    def get_nbrun(self, id):
        output, count =self.client.find(self.db_name, "nbruns", {'r_id': id})
        result = {}
        for service in output:
            result = service
            del result['_id']
        return to_json(result)

    def update_nbrun(self, newObj, id):
        if 'input_params' in newObj:
            output, count = self.client.find(self.db_name, "nbruns", {'r_id': id})
            output[0]['input_params'].update(newObj['input_params'])
            newObj['input_params'] = output[0]['input_params']

        if 'output_params' in newObj:
            output, count = self.client.find(self.db_name, "nbruns", {'r_id': id})
            output[0]['output_params'].update(newObj['output_params'])
            newObj['output_params'] = output[0]['output_params']

        output = self.client.find_one_and_update(self.db_name, "nbruns", {'r_id': id}, {'$set':newObj})
        #del output['_id']
        return to_json(output)

    def delete_nbrun(self, id):
        result = self.client.find_one_and_delete(self.db_name, "nbruns", {'r_id': id})
        return to_json(result)

    def create_nbrun(self, run_doc):
        id = self.client.insert_one(self.db_name, "nbruns", run_doc).inserted_id
        del run_doc['_id']
        return to_json(run_doc)

    def get_params(self, entity_id):
        try:
            record, count = self.client.find(self.db_name, "nbruns", {'entity_id': entity_id}, 0, 1, "version", sort_order=DESCENDING)
            id = record[0]['r_id']

            output, count =self.client.find(self.db_name, "nbruns", {'r_id': id})
            result = {}
            for service in output:
                result = service
                del result['_id']
            return to_json(result)
        except Exception as e:
            return to_json({"error": e})

    def get_latest_version(self, entity_id):
        output, count = self.client.find(self.db_name, "nbruns", {'entity_id': entity_id}, 0, 1, "version", sort_order=DESCENDING)

        if((output is not None) and (len(output) > 0) and ('r_id' in output[0])):
            return int(output[0]['version'])
        else:
            return 0

    def update_params(self, newObj, entity_id):
        output, count = self.client.find(self.db_name, "nbruns", {'entity_id': entity_id}, 0, 1, "version", sort_order=DESCENDING)
        id = output[0]['r_id']

        if 'input_params' in newObj:
            output, count = self.client.find(self.db_name, "nbruns", {'r_id': id})
            output[0]['input_params'].update(newObj['input_params'])
            newObj['input_params'] = output[0]['input_params']

        if 'output_params' in newObj:
            output, count = self.client.find(self.db_name, "nbruns", {'r_id': id})
            output[0]['output_params'].update(newObj['output_params'])
            newObj['output_params'] = output[0]['output_params']

        output = self.client.find_one_and_update(self.db_name, "nbruns", {'r_id': id}, {'$set':newObj})
        #del output['_id']
        return to_json(output)

    def create_from_exc(self, data):

        record = {}
        record['r_id'] = str(uuid.uuid4())
        record['version'] = data['version'] + 1
        record['entity_id'] = data['entity_id']
        record['notebook_file_path'] = data['notebook_file_path']
        record['input_params'] = {}
        record['output_params'] = {}
        record['output_chars_path'] = {}
        record['status'] = 'new'
        record['error_message'] = '-'
        record['app_id'] = data['app_id']
        record['eng_id'] = data['eng_id']
        record['created_at'] = str(datetime.datetime.utcnow())
        record['created_by'] = str('unknown')
        record['updated_at'] = str(datetime.datetime.utcnow())
        record['updated_by'] = str('unknown')

        self.create_nbrun(record)

    def nb_executor_async2(self, data):
        # Step 1: insert new run in database
        run_doc = self.create_run_document(data)
        self.create_nbrun(run_doc)

        # Step 2: set run status as started
        self.update_params({'status': 'started'}, data['entity_id'])

        # Step 3: run notebook async
        nb_path = data["notebook_file_path"]
        local_nb_path = self.get_notebook(nb_path)

        os.system("bash /var/www/html/api-learning-notebooks/src/scripts/execute-notebook.sh "+local_nb_path)

        # Step 4: set run status as in-progress
        self.update_params({'status': 'in-progress'}, data['entity_id'])

        return self.get_params(data['entity_id'])

    def save_as_readonly(self, notebook_url):
        local_nb_path = self.get_notebook(notebook_url)

        os.system("bash /var/www/html/api-learning-notebooks/src/scripts/generate-readonly-notebook.sh "+local_nb_path)

    def create_run_document(self, data):
        run_doc = {}
        run_doc['r_id'] = str(uuid.uuid4())
        run_doc['entity_id'] = data['entity_id']
        run_doc['notebook_file_path'] = data['notebook_file_path']
        run_doc['version'] = self.get_latest_version(data['entity_id']) + 1
        run_doc['input_params'] = data['input_params'] if 'input_params' in data else {}
        run_doc['input_datamarts'] = data['input_datamarts'] if 'input_datamarts' in data else []
        run_doc['input_cartridges'] = data['input_cartridges'] if 'input_cartridges' in data else []
        run_doc['output_datamarts'] = data['output_datamarts'] if 'output_datamarts' in data else []
        run_doc['output_params'] = {}
        run_doc['output_pickel_files'] = []
        run_doc['output_charts_images'] = []
        run_doc['status'] = 'new'
        run_doc['error_message'] = None
        run_doc['created_at'] = str(datetime.datetime.utcnow())
        run_doc['created_by'] = str('unknown')
        run_doc['updated_at'] = str(datetime.datetime.utcnow())
        run_doc['updated_by'] = str('unknown')
        run_doc['log'] = []
        return run_doc

        # Step 4 updated generated images paths in database
        paths = []
        for path in os.listdir(output_image_path):
            paths.append(path)
        
        # Step 5 updated run metadata

        self.update_params({'status': ('success' if status == 0 else 'failed')}, data['entity_id'])
        self.update_params({'output_chars_path': {'paths':paths}}, data['entity_id'])
        self.update_params({'updated_at': str(datetime.datetime.utcnow())}, data['entity_id'])

        # Step 6 create new run in advance so that notebook can update input params
        self.create_from_exc(data)

        return data

    def get_notebook(self, nb_url):
        _, nb_name = nb_url.rsplit("/", 1)
        payload = "username=test&password=test"
        headers = {
            'content-type': "application/x-www-form-urlencoded",
            'authorization': "token "+conf.get_v('jupyterhub.api_token'),
            'cache-control': "no-cache",
            'postman-token': "506704bb-4d60-33d5-9218-ff965c8a6609"
        }
        jupyter_server_name = conf.get_v('jupyterhub.server_name')
        nb_url = nb_url.replace("http://"+jupyter_server_name+"/user/test/notebooks/",
                                "http://"+jupyter_server_name+"/hub/user/test/api/contents/")
        response = requests.request("GET", nb_url, data=payload, headers=headers)

        local_nb_path = "/var/tmp/notebooks/"+nb_name
        with open(local_nb_path, "w+") as nb:
            nb.write(json.dumps(json.loads(response.text)['content']))
        return local_nb_path

    def nb_executor_async_task(self, data):
        # Step 1: insert new run in database
        run_doc = self.create_run_document(data)
        self.create_nbrun(run_doc)

        # Step 2: set run status as started
        self.update_params({'status': 'started'}, data['entity_id'])

        # Step 3: run notebook async
        nb_path = data["notebook_file_path"]

        r_id = run_doc['r_id']
        name = 'nb_{}'.format(r_id[:5])
        sig = celery.signature(self.sign)
        sig.apply_async(
            args=[nb_path, name, r_id],
            task_id=r_id,
            queue='ai_queue_1'
        )

        sch_details = {"run_id": r_id}
        return to_json(sch_details)


