from src.services.exp_tracker import ExpTracker

tracker = ExpTracker()

# set input
a = tracker.set_input_param('a', 10)
b = tracker.set_input_param('b', 20)
c = tracker.set_input_param('c', 30)
d = tracker.set_input_param('d', 40)

#run some code
count = a
for i in range(10):
    print(i)
    count = count + i

# set output
result = tracker.set_output_param('result', count)

print("value of a is {}".format(a))
print("value of b is {}".format(b))
print("value of c is {}".format(c))
print("value of d is {}".format(d))
print("value of result is {}".format(result))