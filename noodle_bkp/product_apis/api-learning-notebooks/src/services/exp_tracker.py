import pandas

class ExpTracker:

    def __init__(self, path = "/tmp/client_1/run_id_1/"):
        self.dir_path = path

    def set_input_param(self, name, value, name_space=globals()):
        name_space[name] = value
        return name_space[name]

    def set_output_param(self, name, value, name_space=globals()):
        name_space[name] = value
        return name_space[name]

    def set_output_frame(self, name, dataframe):
        pandas.to_pickle(dataframe, self.dir_path + name)

    def set_output_charts(self, name, image):
        pass