import http.client
from src.utils.constants import Const
import json
import requests
from src.utils.config import conf


class NBService:

    def get_content_from_notebook(self, notebook_path):
        jupyterhub_api_token = conf.get_v('jupyterhub.api_token')
        try:
            payload = "username=test&password=test"
            headers = {
                'content-type': "application/x-www-form-urlencoded",
                'authorization': "token " + jupyterhub_api_token,
                'cache-control': "no-cache"
            }

            response = requests.request("GET", notebook_path, data=payload, headers=headers)

            return response.text
        except Exception as e:
            print(e)
            return e

    def get_content(self, entity_id, mongo_db_url, db_name, config_url, config_env, sample_notebook_content=None):
        initial_json_str = """{
            "format": "json",
            "content": {
                 "cells": [
                   {
                       "cell_type": "code",
                       "execution_count": 0,
                       "metadata": {},
                       "outputs": [],
                       "source": [
                            "from bat_expr_sdk.expr_tracker import ExprTracker\\n",
                            "\\n",
                            "\\n",
                            "tracker = ExprTracker('"""+entity_id+"""', env='"""+config_env+"""')\\n",
                            "\\n",
                            "learning_rate = tracker.set_input_param('learning_rate', 0.01)\\n",
                            "epoch = tracker.set_input_param('epoch', 50)\\n",
                            "batch_size = tracker.set_input_param('batch_size', 100)\\n",
                            "\\n",
                            "iterations = int(epoch) * int(batch_size)\\n",
                            "\\n",
                            "tracker.set_output_param('iterations', iterations)\\n",
                            "\\n",
                            "tracker.close()\\n",
                            "\\n",
                            "print('end of notebook')"
                           ]
                    }
                 ],
                 "metadata": {
                  "kernelspec": {
                   "display_name": "Python 3",
                   "language": "python",
                   "name": "python3"
                  },
                  "language_info": {
                   "codemirror_mode": {
                    "name": "ipython",
                    "version": 3
                   },
                   "file_extension": ".py",
                   "mimetype": "text/x-python",
                   "name": "python",
                   "nbconvert_exporter": "python",
                   "pygments_lexer": "ipython3",
                   "version": "3.6.3"
                  }
                 },
                 "nbformat": 4,
                 "nbformat_minor": 1
            },
            "writable": true,
            "type": "notebook"
        }"""

        initial_json = json.loads(initial_json_str)

        if sample_notebook_content is not None:
            sample_cells = json.loads(sample_notebook_content)["content"]["cells"]
            for cell in sample_cells:
                initial_json["content"]["cells"].insert(-1, cell)

        return json.dumps(initial_json)

    def override_notebook(self, notebook_url, notebook_content):
        print("overriding notebook with below details")
        print("url {}".format(notebook_url))
        print("content {}".format(notebook_content))

        try:
            jupyterhub_server_name = conf.get_v('jupyterhub.server_name')
            jupyterhub_api_token = conf.get_v('jupyterhub.api_token')

            headers = { 'content-type': 'application/json',
                        'authorization': 'token ' + jupyterhub_api_token,
                        'cache-control': 'no-cache'
            }
            print("server {}".format(jupyterhub_server_name))
            print("headers {}".format(headers))
            # conn = http.client.HTTPConnection(jupyterhub_server_name)
            # conn.request("PUT", notebook_url, notebook_content, headers)
            # res = conn.getresponse()
            # data = res.read()
            # print(data.decode("utf-8"))
            requests.request("PUT", notebook_url, data=notebook_content, headers=headers)
        except Exception as e:
            print(e)
            return e
        return {'status':'success'}

    def create(self, entity_id, notebookName, sample_notebook=None):
        jupyterhub_server_name = conf.get_v('jupyterhub.server_name')
        jupyterhub_api_token = conf.get_v('jupyterhub.api_token')
        jupyterhub_nb_api_folder = conf.get_v('jupyterhub.nb_api_folder')
        user_name = conf.get_v('jupyterhub.user')
        headers = {
                        'content-type': "application/json",
                        'authorization': "token " + jupyterhub_api_token,
                        'cache-control': "no-cache"
        }

        try:
            if sample_notebook is not None:
                sample_notebook = sample_notebook.replace(user_name+"/notebooks", user_name+"/api/contents")
                print(sample_notebook)
                sample_notebook_content = self.get_content_from_notebook(sample_notebook)
            else:
                sample_notebook_content = None

            mongo_db_url = conf.get_v('db.url')
            db_name = conf.get_v('db.name')
            config_url = conf.get_v('config.url')
            config_env = conf.get_v('config.env')
            nb_content = self.get_content(entity_id, mongo_db_url, db_name, config_url, config_env, sample_notebook_content)

            # conn = http.client.HTTPConnection(jupyterhub_server_name)
            # conn.request("PUT", jupyterhub_nb_api_folder+notebookName+".ipynb", nb_content, headers)
            # res = conn.getresponse()
            # data = res.read()
            # print(data.decode("utf-8"))
            requests.request("PUT", jupyterhub_nb_api_folder+notebookName+".ipynb", data=nb_content, headers=headers)
        except Exception as e:
            print(e)
            return e
        return {"created": "success"}