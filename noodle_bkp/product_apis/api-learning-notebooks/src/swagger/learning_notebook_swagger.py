from flask_restful_swagger import swagger
from flask_restful import fields

@swagger.model
class NoteBook:
    resource_fields= {
        "name": fields.String,
        "description": fields.String,
        "notebook_file_path": fields.String
    }