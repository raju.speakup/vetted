from src.utils.config import conf

CELERY_BROKER_URL = conf.get_v('CELERY_BROKER_URL')