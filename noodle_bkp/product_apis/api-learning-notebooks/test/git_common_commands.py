from git import Repo, RemoteProgress

# rorepo is a Repo instance pointing to the git-python repository.
# For all you know, the first argument to Repo is a path to the repository
# you want to work with
repo = Repo("/opt/dev/code/test_git_python")

def print_repository(repo):
    print('Repo description: {}'.format(repo.description))
    print('Repo active branch is {}'.format(repo.active_branch))
    for remote in repo.remotes:
        print('Remote named "{}" with URL "{}"'.format(remote, remote.url))
    print('Last commit for repo is {}.'.format(str(repo.head.commit.hexsha)))

def print_commit(commit):
    print('----')
    print(str(commit.hexsha))
    print("\"{}\" by {} ({})".format(commit.summary,
                                     commit.author.name,
                                     commit.author.email))
    print(str(commit.authored_datetime))
    print(str("count: {} and size: {}".format(commit.count(),
                                              commit.size)))
print("===============")
print("listing all the commits:")
commits = list(repo.iter_commits('master'))[:5]
for commit in commits:
    print_commit(commit)

print("===============")
print("listing all the untracked files:")
for untrack_file in repo.untracked_files:
    print(untrack_file)

print("===============")
print("listing all the tags:")
for tag in repo.tags:
    print(tag)


print("===============")
print("listing all the branches:")
for branch in repo.branches:
    print(branch)
print("active branch is:"+repo.active_branch.name)
#past = repo.create_tag('test_tag_3', ref=commit, message="This is a tag created using python api")

print("===============")
print("listing all the fetch details:")
class MyProgressPrinter(RemoteProgress):

    def update(self, op_code, cur_count, max_count=None, message=''):
        print(op_code, cur_count, max_count, cur_count / (max_count or 100.0), message or "NO MESSAGE")

origin = repo.remotes[0]
#for fetch_info in origin.fetch(progress=MyProgressPrinter()):
#            print("Updated %s to %s" % (fetch_info.ref, fetch_info.commit))


print("===============")
print("listing all the pull:")
#for fetch_info in origin.pull(progress=MyProgressPrinter()):
#            print("Updated %s to %s" % (fetch_info.ref, fetch_info.commit))
print("===============")
print("create commit")
#repo.index.add(['file4.py'])
#commit = repo.index.commit("adding file3 to the repo")
#origin.push()


print("===============")
print("create tag")
#repo.branches['master'].checkout()
#new_tag = repo.create_tag("tag_till_file4")
#origin.push(new_tag)

print("===============")
print("create branch")
#repo.git.checkout('HEAD', b="branch5")
#repo.git.push("tag_till_file4", "branch5")

print("===============")
print("checkout tag")
from git import Git
g = Git("/opt/dev/code/test_git_python")
#g.checkout('tag_till_file4')

#g.checkout('tag101')

print("===============")
print("list history of a file")
path = 'file1.py'

revlist = (
    (commit, (commit.tree / path).data_stream.read())
    for commit in repo.iter_commits(paths=path)
)

count = 0
commit_old = None
for commit, filecontents in revlist:
    if count == 2:
        commit_old = commit
    print(commit)
    print(filecontents)
    count = count+1

print("reverting to " + commit_old.hexsha)
repo.git.revert(commit_old.hexsha, no_edit = True)