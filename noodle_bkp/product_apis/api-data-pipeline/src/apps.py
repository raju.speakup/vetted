#from flask import Flask
from flask import Flask
from flask import request
from flask_restful import Api
from flask_restful_swagger import swagger
from resource.debug import Debug
from resource.fetch_context_variables import ContextVariables
from resource.fetch_all_pipelines import AllPipelines
from resource.fetch_pipeline_details import PipelineDetails
from resource.fetch_all_quality_rules import QualityRules
from resource.fetch_dbClass import DbClass
from resource.validate_connections_resource import ConnectionValidation
from resource.validate_schema import SchemaValidation
from resource.create_pipeline import CreatePipeline
from resource.update_pipeline import UpdatePipeline
from resource.update_quality_rule import UpdateQualityRule
from resource.update_cleansing_rule import UpdateCleansingRule
from resource.update_pipeline_info import UpdatePipelineInfo
from resource.update_dataset import UpdateDataset
from resource.create_cleansing_rule import CreateCleansingRule
from resource.create_quality_rule import CreateQualityRule
from resource.execution_status import Status
from resource.save_and_schedule import SaveAndSchedule
from resource.save_and_trigger import SaveAndTrigger
from resource.preview_source_data import PreviewSourceData
from resource.update_schedule import UpdateSchedule
from resource.preview_target_data import PreviewTargetData
from encoder import MyJSONEncoder
from resource.delete_cleansing_rule import DeleteCleansingRule
from resource.delete_quality_rule import DeleteQualityRule
from resource.delete_pipeline import DeletePipeline
from resource.config_store_info import Config
from flask_jwt_extended import JWTManager
#from src.utils.nlogger import logger
#from flask_jwt_extended import jwt_required


app = Flask(__name__)
app.json_encoder = MyJSONEncoder

# @app.before_first_request
# def global_init():
#     logger.log().info("started initialising")
#
#     logger.log().info("completed initialising")


# @app.before_request
# def validate_auth():
#     if (not "/admin/_refresh" == request.path) and (not "/api/spec.html" == request.path):
#          check_auth()
#
#
# @jwt_required
# def check_auth():
#     print("jwt token validated successfully")
#     g.clientid = request.headers.get('clientid')
#     g.appid = request.headers.get('appid')



api = swagger.docs(
    Api(app),
    apiVersion='0.1',
    basePath="http://localhost:5000/api/v0/",
    resourcePath='/',
    description="docs for Data Pipeline api",
    api_spec_url='/api/spec',
    produces=["application/json", "text/html"]

)

# JWT settings
app.config['JWT_SECRET_KEY'] = 'some-difficult-value'
jwt = JWTManager(app)
app.config['PROPAGATE_EXCEPTIONS'] = True

api.add_resource(Debug, '/data-pipeline/v0/debug/service_status')
api.add_resource(ContextVariables,'/data-pipeline/v0/context_variables')
api.add_resource(AllPipelines,'/data-pipeline/v0/client/<string:clientid>/apps/<string:appid>/pipelines')
api.add_resource(PipelineDetails,'/data-pipeline/v0/pipelines/<string:pipelineId>/details')
api.add_resource(QualityRules,'/data-pipeline/v0/quality_rules/_names')
api.add_resource(DbClass,'/data-pipeline/v0/<string:dbType>/db_class')
api.add_resource(ConnectionValidation,'/data-pipeline/v0/connection_validation')
api.add_resource(SchemaValidation,'/data-pipeline/v0/schema_validation')
api.add_resource(SaveAndTrigger,'/data-pipeline/v0/pipelines/triggerRun')
# api.add_resource(SaveAndSchedule,'/data-pipeline/v0/client/apps/pipelines/scheduleRun')
api.add_resource(SaveAndSchedule,'/data-pipeline/v0/pipelines/scheduleRun')
#api.add_resource(CreatePipeline, '/data-pipeline/v0/new_pipeline')
api.add_resource(UpdateQualityRule,'/data-pipeline//<string:pipelineId>/<string:qualityRuleId>/update_quality_rule')
api.add_resource(UpdateCleansingRule,'/data-pipeline/v0/<string:pipelineId>/<string:cleansingRuleId>/update_cleansing_rule')
api.add_resource(UpdatePipelineInfo,'/data-pipeline/v0/<string:pipelineId>/update_pipeline_info')
api.add_resource(UpdateDataset,'/data-pipeline/v0/<string:pipelineId>/update_dataset')
api.add_resource(UpdatePipeline,'/data-pipeline/v0/update_pipeline')
api.add_resource(UpdateSchedule,'/data-pipeline/v0/pipelines/updateSchedule')
api.add_resource(CreateCleansingRule,'/data-pipeline/v0/pipelines/<string:id>/cleansing_rules')
api.add_resource(CreateQualityRule,'/data-pipeline/v0/pipelines/<string:id>/quality_rules')
api.add_resource(Status,'/data-pipeline/v0/<string:datastoreId>/<string:datasetId>/execution_status')
api.add_resource(PreviewSourceData,'/data-pipeline/v0/pipelines/<string:datastoreId>/<string:datasetId>/previewSourceData')
api.add_resource(PreviewTargetData,'/data-pipeline/v0/pipelines/<string:datasetId>/previewTargetData')
api.add_resource(DeleteCleansingRule,'/data-pipeline/v0/pipelines/<string:pipelineId>/<string:cleansingRuleId>/deleteCleansingRule')
api.add_resource(DeleteQualityRule,'/data-pipeline/v0/pipelines/<string:pipelineId>/<string:qualityRuleId>/deleteQualityRule')
api.add_resource(DeletePipeline,'/data-pipeline/v0/pipelines/deletePipelines')
api.add_resource(Config,'/data-pipeline/v0/debug/config_store')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5010, debug=True,threaded=True)



