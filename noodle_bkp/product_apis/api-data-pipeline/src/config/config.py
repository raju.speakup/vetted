import os
from dotenv import load_dotenv

APP_ROOT = os.path.join(os.path.dirname(__file__), '..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)

SERVER_NAME = os.getenv("SERVER_NAME")
DB_NAME = os.getenv("DB_NAME")
USERNAME = os.getenv("USERNAME")
PASSWORD = os.getenv("PASSWORD")
DATABASE_SCHEMA = os.getenv("DATABASE_SCHEMA")
DATABASE_NAME = os.getenv("DATABASE_NAME")
DEFAULT_IP = os.getenv("DEFAULT_IP")
CONTEXT = os.getenv("CONTEXT")
LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")

MONGO_SERVER = os.getenv("MONGO_SERVER")
MONGO_DB = os.getenv("MONGO_DB")
MONGO_AUTHDB = os.getenv("MONGO_AUTHDB")
MONGO_USER = os.getenv("MONGO_USER")
MONGO_PASSWORD = os.getenv("MONGO_PASSWORD")
MONGO_COLLECTION = os.getenv("MONGO_COLLECTION")
AUTH_MECHANISM = os.getenv("AUTH_MECHANISM")

# modified to pass the metadata to other file


class ConfigParam:
    def passParam(self):
        SERVER_NAME = os.getenv("SERVER_NAME")
        DB_NAME = os.getenv("DB_NAME")
        USERNAME = os.getenv("USERNAME")
        return(SERVER_NAME, DB_NAME, USERNAME)
