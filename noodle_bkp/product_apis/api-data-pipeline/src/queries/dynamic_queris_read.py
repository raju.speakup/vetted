

def dynamic_queris_read(
        table_name, between_query,
        where_query, column_names=[], sorting={}):
    """
    """

    query = "SELECT " + \
        get_column_names(column_names) + \
        " FROM " + \
        table_name

    query = query + between_query if between_query else query

    query = query + (where_query.replace("WHERE", "")
                     if between_query else where_query)
    query = query + get_sorting_order(sorting) + \
        ';' if sorting else query + ';'

    return query


def get_column_names(column_names=[]):
    """
    """
    columns = ""
    columns = ','.join(
        map(str, column_names)) if column_names else "*"

    return columns


def get_sorting_order(sorting={}):
    """
    Reference query:
        SELECT * FROM Customers
        ORDER BY Country ASC, CustomerName DESC;

    """
    sort_query = "ORDER BY"
    for index, key in enumerate(sorting):

        sort_query = ' ' + \
            sort_query + \
            ' ' + key + ' ' + \
            sorting[key]

        if not index == len(sorting) - 1:
            sort_query = sort_query + ','

    return sort_query


def get_sorting_dict(fields=[]):
    """
    """
    sorting_dict = {}

    for field in fields:
        if field:
            sort_type = 'DESC' if '-' in field else 'ASC'

            remove_symbol = field.replace(
                "-", "") if '-' in field else field

            sorting_dict.update({remove_symbol: sort_type})
    return sorting_dict


def between_query(fields=[]):
    """

    """
    between_query = " WHERE "
    for index, field in enumerate(fields):
        between_query = between_query + \
            field[0] + " between " + field[1] + " and " + field[2]

        if not index == len(fields) - 1:
            between_query = between_query + " and "

    return between_query


def where_query(fields=[]):
    """
    """
    if not fields:
        return ""

    where_query = " WHERE "
    for index, field in enumerate(fields):
        where_query = where_query + \
            field[0] + " " + field[1] + " " + field[2]

        if not index == len(fields) - 1:
            where_query = where_query + " and "
    return where_query
