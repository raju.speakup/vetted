from .dynamic_data_type import get_char_set_type


def dynamic_queris_update(
        table_name,
        data_values,
        where_query):
    """
    """
    query = " UPDATE " + table_name + " SET " + data_values
    query = query + where_query + ';'
    return query


def correct_updating_value(data_values=[], column_names=[], column_type={}):
    """
    """
    data_values_return = ""

    for index, element in enumerate(column_names):
        data_values_return = data_values_return + \
            element + "=" + get_char_set_type(
                column_type.get(element), data_values[index])
        data_values_return = data_values_return + \
            ',' if not index == len(column_names) - 1 else data_values_return

    return data_values_return
