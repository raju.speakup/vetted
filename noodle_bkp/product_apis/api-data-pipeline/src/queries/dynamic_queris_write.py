from .dynamic_data_type import get_char_set_type


def get_column_data(id_field, data_dict={}):
    """
    """
    column_names = []
    data_values = []

    for index, key in enumerate(data_dict):
        if not key == id_field:
            column_names.append(str(key))
            data_values.append(str(data_dict[key]))
    return column_names, data_values


def table_inseration_query(
        table_name, column_names, data_values, where_clause=[]):
    """
    """
    column_names = ",".join(column_names)

    query = "INSERT INTO " + table_name
    if column_names:
        query = query + " ( " + column_names + " ) " + \
            "VALUES" + " (" + data_values + " ) "

    query = query + where_clause if len(where_clause) > 0 else query + " ;"

    return query


def correct_data_values(data_values=[], column_names=[], column_type={}):
    """
    """
    import ipdb; ipdb.set_trace()
    data_values_return = ""

    for index, element in enumerate(column_names):
        data_values_return = data_values_return + get_char_set_type(
            column_type.get(element), data_values[index])
        data_values_return = data_values_return + \
            ',' if not index == len(column_names) - 1 else data_values_return

    return data_values_return


def column_list_string(column_names=[]):
    """
    """
    column_names = ','.join(column_names)
    return column_names


def where_query(fields=[]):
    """
    """
    if not fields:
        return ""

    where_query = " WHERE "
    for index, field in enumerate(fields):
        where_query = where_query + \
            field[0] + " " + field[1] + " " + field[2]

        if not index == len(fields) - 1:
            where_query = where_query + " and "
    return where_query
