def get_char_set_type(data_type, data):
    """
    """
    return {
        'varchar': "'{0}'".format(data),

        'bigint': "{0}".format(data),

        'datetime': "'{0}'".format(data)

    }.get(data_type, 'Not Found')
