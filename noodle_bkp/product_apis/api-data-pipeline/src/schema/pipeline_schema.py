schema={
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title":"Pipeline",
  "type": "object",
  "properties": {
        "actionOnData": {
          "type": "string"
        },
        "Cleansing_Rules": {
          "type": "array"
        },
        "datasetId": {
          "type": "string"
        },
        "datastoreId": {
          "type": "string"
        },
        "isActive": {
          "type": "number"
        },
        "pipelineName": {
          "type": "string"
        },
        "Quality_Rules": {
          "type": "array"
        },
        "scheduleIsActive": {
          "type": "number"
        },
        "scheduleInterval": {
          "type": "string"
        },
        "scheduleType": {
          "type": "string"
        },
        "startDateTime": {
          "type": "string",
          "format": "date"
        }
      }
  }



