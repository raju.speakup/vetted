schema={
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Cleansing",
  "type": "object",
  "properties": {
    "IsRegex": {
      "type": "string",
      "enum":["Yes","No"]
    },
    "ColName": {
      "type": "string"
    },
    "ReplacePhrase": {
      "type": "string"
    },
    "ReplaceWithPhrase":{
      "type":"string"
    }
  }
}