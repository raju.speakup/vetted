from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors


class ContextVariables(Resource):
    @swagger.operation(
        notes='This method gets the context variables '
        'to be used by DataIQ Talend package and the API'             
        ' of all quality check rules',
        nickname='GET'
    )
    def get(self):
        """
               Fetches context variables
               :return: Success or failure message based on data fetched
               """
        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            context_var = queries.contextVariables()
            return jsonify({'data': context_var})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
