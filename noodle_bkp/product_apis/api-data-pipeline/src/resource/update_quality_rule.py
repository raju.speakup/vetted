from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB



class UpdateQualityRule(Resource):
    """
    This class calls methods to update a pipeline
    with necessary details
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to update Cleansing Rules',
        nickname='POST',
        parameters =[
        {
            "name": "body",
            "description": "Attributes to update a Cleansing rule",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
        ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def post(self,pipelineId,qualityRuleId):
        """
        Updates a pipeline with specified details
        :return: Success or failure message based on data inserted
        """

        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            data = queries.updateQualityRule(pipelineId,qualityRuleId)
            logger.info(data)
            return (data)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': str(e)}})



