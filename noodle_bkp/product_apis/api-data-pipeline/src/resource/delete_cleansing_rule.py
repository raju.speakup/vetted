from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB


class DeleteCleansingRule(Resource):
   """
   This class calls methods that delete a
   cleansing rule for a pipeline
   """

   def __init__(self):
       role_validator.init(DB_URL, MONGO_DB)
   @swagger.operation(
       notes='This method is used to delete a '
             'cleansing rule for a pipeline',
       nickname='DELETE'
   )
   @jwt_required
   @role_validator.validate_app_user()
   def delete(self, pipelineId,cleansingRuleId):
       """
       Deletes a cleansing rule for a pipeline
       :return: Success or failure message based on data deleted
       """
       try:
           queries = Queries()
           logger.info(queries)

       except pymongo.errors.PyMongoError as e:
           logger.info(e)
           return jsonify({'Error': {
               'Message': [e.args[0]]}})
       try:
           data = queries.deleteCleansingRules(pipelineId,cleansingRuleId)
           return jsonify({'data': data})

       except pymongo.errors.PyMongoError as e:
           return jsonify({'Error': {
               'Message': [e.args[0]]}})