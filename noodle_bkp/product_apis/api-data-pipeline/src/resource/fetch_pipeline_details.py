from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from commons.queries import DB_URL,MONGO_DB
from bat_auth_sdk import role_validator



class PipelineDetails(Resource):
    """
    This class calls methods to get details for a specific pipeline
    including the cleansing and qiality check rules applied on it
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)

    @swagger.operation(
        notes='This method is used to provide details of each entity '
              'including descriptions of rules applied on it',
        nickname='GET',
        parameters =[
        {
            "name": "pipelineId",
            "description": "The ID is a unique identifier of each entity",
            "required": True,
            "allowMultiple": False,
            "dataType": "String",
            "paramType": "path"
        }]
    )





    @jwt_required
    @role_validator.validate_app_user()
    def get(self, pipelineId,clientid, appid):
        """
        Gets pipeline details,rules applied, schedule
        and schema details
        :param id:  Unique identifier of a pipeline
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)
        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            var_pipeline_details = queries.pipeline_details(pipelineId)
            return jsonify ({'data':var_pipeline_details})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})










