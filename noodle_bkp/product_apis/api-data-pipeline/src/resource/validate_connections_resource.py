from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from pymongo.errors import PyMongoError
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema


class ConnectionValidation(Resource):
    """
    This class calls methods to check if
    source and target connections are valid
    and if the user has correct authorization
    """
    @swagger.operation(
        notes='This method is used to check the connectivity '
              'of source and destination database',
        nickname='PUT',
        #
        parameters =[
           {
            "name": "body",
            "description": "Attributes to insert record",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
        ]
    )
    def put(self):
        """
        Validates source and target connections
        :return: Success or failure message based on data inserted
        """

        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            data = queries.connectionValidation(0)
            return jsonify({'data': data})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': str(e)}})




