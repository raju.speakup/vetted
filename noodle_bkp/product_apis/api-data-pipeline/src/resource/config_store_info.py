from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger

import pymongo.errors

class Config(Resource):
    @swagger.operation(
        notes='This method is used to fetch the config store parameters',
        nickname='GET'
    )
    def get(self):
        try:
            queries = Queries()
            logger.info(queries)
        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify(
                {'Error': {
                    'Message': e
                }})
        try:
            Data = queries.configStoreInfo()
            return jsonify({'data': Data})
        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e]}})
