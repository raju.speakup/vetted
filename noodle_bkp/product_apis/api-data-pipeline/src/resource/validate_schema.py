from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema


class SchemaValidation(Resource):
    """
    This class calls methods to validate the schema
    to ensure source and target have the correct
    column names and datatypes
    """
    @swagger.operation(
        notes='This method is used to match the schema of target table with schema given in catalog',
        nickname='PUT',
        parameters=[
            {
                "name": "body",
                "description": "Attributes to validate the schema",
                "required": True,
                "type": "application/json",
                "paramType": "body"
            }
        ]
    )
    def put(self):
        """
        Validates schema of source and target
        :return: Success or failure message based on data inserted
        """

        try:
            queries = Queries()

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            data = queries.check_schema()
            if data is None:
                raise Exception("Incorrect schema")
            else:
                return jsonify({'data': data})


        except pymongo.errors.PyMongoError as e:

            return jsonify({'Error': {
                'Message': str(e)}})



