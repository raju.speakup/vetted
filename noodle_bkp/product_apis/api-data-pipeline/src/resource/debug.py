from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
# from config.config import (
#     SERVER_NAME,
#     DB_NAME,
#     USERNAME,
#     DATABASE_SCHEMA,
#     DEFAULT_IP
# )


class Debug(Resource):
    """
    This class calls method to check status of db connection
    and to check on the Airflow configuration parameters and to fetch the ipv address
    and account name used to run API
    """
    @swagger.operation(
        notes='This method is used to check if db connection'
              'is successful',
        nickname='GET'
    )

    def get(self):
        """
        Fetches details on db connection and configuration parameters for API and Airflow
        :return: Success or failure message based on data fetched and
        the configuration parameters for Airflow and API
        """
        try:
            queries = Queries()
            logger.info(queries)
        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify(
                {'Error': {
                    'Message': e.args[0],
                    # 'Server name': SERVER_NAME,
                    # 'Db name': DB_NAME,
                    # 'Username': USERNAME,
                    # 'Schema': DATABASE_SCHEMA,
                    # 'Default IP': DEFAULT_IP,
                    # 'IsConnectionSuccessful': 'No'
                }})
        # try:
        #     # server, db, user, collection = queries.debug()
        #     msq = queries.debug()
        #     # s, a = queries.APIConfig()
        #     # data = queries.airflowConfig()
        #     return jsonify(#{'DB':
        #                         # {'Server name': server,
        #                         #      'Db name': db,
        #                         #      'Username': user,
        #                         #      'Collection': collection,
        #                              # 'IsConnectionSuccessful': 'Yes'
        #                      {'data':msq})
        #                    # ,{'API':{
        #                    #           'server_ip': s,
        #                    #           'account_name': a}},
        #                    #
        #                    # {'AIRFLOW':{
        #                    #           'Context':data[0]['Context'],
        #                    #           'Host': data[0]['Host'],
        #                    #           'Passwords': data[0]['Passwords'],
        #                    #           'Username': data[0]['Username']
        #                    #           }})


        try:
            message = queries.debug()
            # s, a = queries.APIConfig()
            # data = queries.airflowConfig()
            return jsonify({'status':message})
            # return jsonify({'DB': {'Server name': server,
            #                          'Db name': db,
            #                          'Username': user,
            #                          'Collection': collection,
            #                          'IsConnectionSuccessful': 'Yes'}}
            #                ,{'API':{
            #                          'server_ip': s,
            #                          'account_name': a}},
            #
            #                {'AIRFLOW':{
            #                          'Context':data[0]['Context'],
            #                          'Host': data[0]['Host'],
            #                          'Passwords': data[0]['Passwords'],
            #                          'Username': data[0]['Username']
            #                          }})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})


