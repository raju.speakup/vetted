from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema


class PreviewSourceData(Resource):
    """
    This class calls the method to preview
    data in the source
    """
    @swagger.operation(
        notes='This method is used to preview data from the source',
        nickname='GET',
        parameters =[
            {
                "name": "datasetId",
                "description": "The datasetId is a unique identifier of each entity",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "path"
            },
            {
                "name": "datastoreId",
                "description": "The datastoreId is a unique identifier",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "path"
            }
        ]
    )
    def get(self, datastoreId, datasetId):
        """
        Fetches data from source
        :return: Success or failure message based on data retrieved
        """

        try:
                queries = Queries()

        except pymongo.errors.PyMongoError as e:
                return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
                data = queries.previewSourceData(datastoreId, datasetId)
                return jsonify({'data': data})

        except pymongo.errors.PyMongoError as e:
                return jsonify({'Error': {
                    'Message': str(e)}})

