from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB

class Status(Resource):
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to check the status of execution',
        nickname='GET',
        parameters=[
            {
                "name": "datastoreId",
                "description": "The ID is a unique identifier of a datastore",
                "required": True,
                "allowMultiple": False,
                "dataType": "int",
                "paramType": "path"
            },
            {
                "name": "datasetId",
                "description": "The ID is a unique identifier of a dataset",
                "required": True,
                "allowMultiple": False,
                "dataType": "int",
                "paramType": "path"
            }
        ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def get(self, datastoreId, datasetId):
        """
        This method is used to retrieve the execution
        status of the pipeline
        :param datastoreId:  Unique identifier of a datastore
        :param datasetId:  Unique identifier of a dataset
        :return: Execution status, dataset id
        """
        try:
            queries = Queries()

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            data = queries.execution_status(datastoreId, datasetId)
            return jsonify({'Data':{'DatastoreId': datastoreId,
                                    'DatasetId': datasetId,
                                    'Execution status' : data}})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

