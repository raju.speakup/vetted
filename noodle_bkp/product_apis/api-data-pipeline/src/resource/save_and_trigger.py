from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from commons.queries import DB_URL,MONGO_DB
from bat_auth_sdk import role_validator


class SaveAndTrigger(Resource):
    """
    This class calls the method to create a pipeline
    with all associated cleansing rules, quality
    check rules, schedule and schema information
    and trigger the pipeline run
    """

    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to create a pipeline '
              'and trigger the run',
        nickname='PUT',
        parameters =[
        {
            "name": "body",
            "description": "Attributes to create a pipeline, "
                           "schedule execution, insert schema details"
                           " and optionally insert cleansing "
                           "and/or quality rules",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
        ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def put(self):
        """
        Creates a new pipeline with specified details
        and triggers a test run
        :return: Success or failure message based on data inserted
        """

        try:
                queries = Queries()

        except pymongo.errors.PyMongoError as e:
                return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
                data = queries.saveAndTrigger()
                return jsonify({'data': data})

        except pymongo.errors.PyMongoError as e:
                return jsonify({'Error': {
                    'Message': str(e)}})
