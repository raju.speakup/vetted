from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema





class UpdatePipeline(Resource):
    """
    This class calls methods to update a pipeline
    with necessary details
    """
    @swagger.operation(
        notes='This method is used to update pipeline details',
        nickname='POST',
        parameters =[
        {
            "name": "body",
            "description": "Attributes to update a pipeline",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
        ]
    )


    def post(self):
        """
        Updates a pipeline with specified details
        :return: Success or failure message based on data inserted
        """

        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            data = queries.updatePipeline()
            logger.info(data)
            return (data)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': str(e)}})



