from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors


class DbClass(Resource):
    """
    This class calls methods that fetch basic pipeline
    information to display to the user
    """
    @swagger.operation(
        notes='This method is used to retrieve details '
        'about the DB class for a specific type of database',
        nickname='GET'
    )
    def get(self, dbType):
        """
        Fetches db class of source DB
        :param sourceType: The type of db (Oracle, SQLServer etc.)
        :return: Success or failure message based on response
        """
        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            dbClass = queries.dbClass(dbType)
            logger.info(dbClass)
            return jsonify({'data': dbClass})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

