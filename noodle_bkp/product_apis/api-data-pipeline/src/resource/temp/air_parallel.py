from airflow.models import DAG
from airflow.operators import PythonOperator
from datetime import datetime, timedelta
import os

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.strptime("$start_date",'%Y-%m-%d %H:%M:%S.%f'),
    'max_active_runs': 1,
    'catchup':False
    #    'email_on_failure': True,
    #   'email_on_retry': False,
    #    'max_active_run':1
    # 'retries': 1,
    # 'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}


def pipe_run():
    #    for i in pipes:
    os.system(
        'bash /home/airflow_service/airflow/scripts/DataIQ_Mongo/DIQController/DIQController_run.sh '
        '--context_param pipelineid=$pipeid '
        '--context_param datastoreid=$datastoreid '
        '--context_param datasetid=$datasetid '
        '--context_param pipelineurl=$pipelineurl')


dag = DAG('DIQ_'+'$pipeid', default_args=default_args,
          schedule_interval=timedelta(days=$days,hours=$hours,weeks=$weeks))

# t1, t2 and t3 are examples of tasks created by instantiating operators
t1 = PythonOperator(
    task_id='DataIQ',
    python_callable=pipe_run,
    dag=dag)
