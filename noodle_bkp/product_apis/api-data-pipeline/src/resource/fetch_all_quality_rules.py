from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB

class QualityRules(Resource):
    """
    This class calls methods to fetch names and descriptions
    of quality check rules that can be applied on the pipeline
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to retrieve names and descriptions'
              ' of all quality check rules',
        nickname='GET'
    )
    @jwt_required
    @role_validator.validate_app_user()
    def get(self):
        """
        Fetches quality rule names and descriptions
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            quality_rule_name = queries.quality_rule_names()
            return jsonify({'data' : quality_rule_name})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
