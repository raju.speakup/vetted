from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema


class PreviewTargetData(Resource):
    """
    This class calls the method to preview
    data in the source
    """
    @swagger.operation(
        notes='This method is used to preview data from the target',
        nickname='GET',
        parameters =[
            {
                "name": "datasetId",
                "description": "The datasetId is a unique identifier of each entity",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "path"
            }

        ]
    )
    def get(self, datasetId):
        """
        Fetches data from target
        :return: Success or failure message based on data retrieved
        """

        try:
                queries = Queries()

        except pymongo.errors.PyMongoError as e:
                return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
                data = queries.previewTargetData(datasetId)
                return jsonify({data})
        # except pymssql.ProgrammingError  as e:
        #         logger.info(e)
        #         return jsonify({'Error': {
        #             'Message': str(e)}})
        # except pymssql.OperationalError  as e:
        #         return jsonify({'Error': {
        #             'Message': str(e)}})
        # except pymssql.DataError as e:
        #         return jsonify({'Error': {
        #             'Message': str(e)}})
        except pymongo.errors.PyMongoError as e:
                return jsonify({'Error': {
                    'Message': str(e)}})

