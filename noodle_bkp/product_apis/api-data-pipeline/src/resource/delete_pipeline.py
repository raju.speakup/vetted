from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB



class DeletePipeline(Resource):
    """
    This class calls methods that delete a
    pipeline
    """

    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to update Pipeline Info',
        nickname='POST',
        parameters =[
        {
            "name": "body",
            "description": "Attributes to update Pipeline name and description",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
        ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def delete(self):
        """
        Deletes a pipeline
        :return: Success or failure message based on data deleted
        """
        try:
            queries = Queries()
            logger.info(queries)


        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})
        try:
            data = queries.deletePipeline()
            return jsonify({'data': data})
        except pymongo.errors.PyMongoError as e:
            return jsonify({'Error': {
                'Message': [e.args[0]]}})