from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from logger.logger import logger
import pymssql
import pymongo.errors
from flask import request
from schema.cleansing_schema import schema
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB





class CreateCleansingRule(Resource):
    """
    This class calls methods to create (insert) a cleansing rule
    for a specific pipeline with input from the user
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to create a cleansing rule for a '
              'particular entity',
        nickname='PUT',
        parameters =[
    {
        "name": "id",
        "description": "The ID is a unique identifier of each entity",
        "required": True,
        "allowMultiple": False,
        "dataType": "int",
        "paramType": "path"
    },

        {
            "name": "body",
            "description": "Attributes to insert record",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
    ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def put(self, id):
        """
        Creates a cleansing rule for a pipeline
        :param id: Unique identifier of a pipeline
        :return: Success or failure message based on data inserted
        """
        q = Queries()
        data, code = q.validate(request, schema)
        if code == "HTTP_200_OK":
            try:
                queries = Queries()
                logger.info(queries)
            except pymongo.errors.PyMongoError as e:
                logger.info(e)
                return jsonify({'Error': {
                        'Message': [e.args[0]]}})
            try:
                data = queries.create_cleansing_rule(id)
                return (data)
            except pymongo.errors.PyMongoError as e:
                logger.info(e)
                return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        else:
            return jsonify({'Error': {'Message': 'Schema invalid. Please re-enter the correct data'}})


