from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from schema.pipeline_schema import schema
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from bat_auth_sdk import role_validator
from commons.queries import DB_URL,MONGO_DB


class CreatePipeline(Resource):
    """
    This class calls methods to create a pipeline
    with all associated cleansing rules, quality
    check rules, schedule and schema information
    """
    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to create a pipeline with all '
              'source/target details',
        nickname='PUT',
        parameters =[
        {
            "name": "body",
            "description": "Attributes to create a pipeline, "
                           "schedule execution, insert schema details"
                           " and optionally insert cleansing "
                           "and/or quality rules",
            "required": True,
            "type": "application/json",
            "paramType": "body"
        }
        ]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def put(self):
        """
        Creates a new pipeline with specified details
        :return: Success or failure message based on data inserted
        """
        q = Queries()

        data, code = q.validate(request, schema)
        if code == 'HTTP_200_OK':
            try:
                queries = Queries()
            except pymongo.errors.PyMongoError as e:
                logger.info(e)
                return jsonify({'Error': {
                    'Message': [e.args[0]]}})
            try:
                context_data = queries.contextVariables()
                pipelineURL = (context_data[0]['pipelineurl'])
                pipeline_id, connectionError = queries.create_pipeline()
                if(pipeline_id is None):
                    raise Exception(connectionError)
                else:
                    data = queries.create_dag(pipeline_id, pipelineURL)
                    return jsonify({'data': data})
            except pymongo.errors.PyMongoError as e:
                    logger.info(e)
                    return jsonify({'Error': {
                        'Message': [e.args[0]]}})

        else:
            return jsonify({'Error': {'Message': 'Schema invalid. Please re-enter the correct data'}})


