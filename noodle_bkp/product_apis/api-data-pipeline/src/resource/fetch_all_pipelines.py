from flask_restful import Resource
from commons.queries import Queries
from flask_restful_swagger import swagger
from flask import jsonify
from flask import request
from logger.logger import logger
import pymssql
import pymongo.errors
from commons.decorators import validate_app_user
from flask_jwt_extended import jwt_required
from commons.queries import DB_URL, MONGO_DB
from bat_auth_sdk import role_validator


class AllPipelines(Resource):
    """
    This class calls methods that fetch basic pipeline
    information to display to the user
    """

    def __init__(self):
        role_validator.init(DB_URL, MONGO_DB)
    @swagger.operation(
        notes='This method is used to retrieve basic entity details '
        'and the number of cleansing and quality of rules applied on '
        'it as well as the execution status of the latest run',
        nickname='GET',
        parameters=[
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
            },{
                "name": "searchElement",
                "dataType": 'varchar',
                "paramType": "query",
            }]
    )
    @jwt_required
    @role_validator.validate_app_user()
    def get(self,clientid, appid):
        """
        Fetches details on pipelines
        :return: Success or failure message based on data fetched
        """
        try:
            queries = Queries()
            logger.info(queries)

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                    'Message': [e.args[0]]}})
        try:
            limit = request.args.get("limit", 10)
            offset = request.args.get("offset", 0)
            searchElement = request.args.get("searchElement","null")
            pipeline_details,pipeline_count = queries.pipeline(clientid,appid,limit,offset,searchElement)
            logger.info(pipeline_details)
            return jsonify({'data': pipeline_details,"PipelinesCount":pipeline_count})

        except pymongo.errors.PyMongoError as e:
            logger.info(e)
            return jsonify({'Error': {
                'Message': [e.args[0]]}})

