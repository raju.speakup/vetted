import json
def return_result_final(result=[], error=None, error_code=None):
    """Return Result."""
    return_result = {}
    result = json.loads(result) if type(
        result) == str else result
    return_result.update(
        {
            "error": {
                "code": error_code,
                "message": str(error) if error else error
            }
        }
    ) if error else return_result.update(
        {"data": json.loads(json.dumps(result)) if type(
            result) != dict else result})
    if not error and type(result) == list:
        return_result.update({"resultCount": len(result)})
    return return_result



def return_result(result=[], error=None, error_code=None):
    return_result = {}

    return_result.update(
        {
            "data": result,
            "error": {
                "code": error_code,
                "message": error
            }
        }
    )

    return_result = json.dumps(return_result)
    return json.loads(return_result)

