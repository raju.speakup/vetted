from flask import request
import pymssql
import requests
from commons.utils import return_result
import datetime
import io,os
import paramiko
import json
from pymongo import MongoClient
import uuid
from ftplib import FTP
from io import BytesIO
import subprocess
from flask import jsonify
import re
from bat_auth_sdk import role_validator



import jsonschema
from jsonschema import validate
from config.config import (
    CONTEXT,
    SERVER_NAME,
    DB_NAME,
    USERNAME,
    PASSWORD,

    # MONGO_SERVER,
    # MONGO_DB,
    # MONGO_AUTHDB,
    # MONGO_USER,
    # MONGO_PASSWORD,
    # MONGO_COLLECTION,
    # AUTH_MECHANISM
)
from config_store import config_store_client as csc
cs = csc.ConfigStore()
cs.load(ip='*', service_name='api-data-pipeline')
MONGO_SERVER=cs.get("MONGO_SERVER")
MONGO_DB=cs.get("MONGO_DB")
MONGO_AUTHDB=cs.get("MONGO_AUTHDB")
MONGO_USER=cs.get("MONGO_USER")
MONGO_PASSWORD=cs.get("MONGO_PASSWORD")
AUTH_MECHANISM=cs.get("AUTH_MECHANISM")
AWS=cs.get("AWS")
DATASTORE_URL=cs.get("DATASTORE_URL")
DB_URL =   cs.get("DB_URL")



class Queries:
    """
    Init constructor to instantiate the class
    """

    def __init__(self):
        # connection_string="".join\
        #     (['mongodb://', MONGO_USER, ':',
        #       MONGO_PASSWORD, '@', MONGO_SERVER])

        self.client = MongoClient(MONGO_SERVER,
                             username=MONGO_USER,
                             password=MONGO_PASSWORD,
                             authSource=MONGO_AUTHDB,
                             authMechanism=AUTH_MECHANISM)
        self.database = self.client[MONGO_DB]
        self.currentDate=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]



    def find_selected_field(self, collectionname, data, projection):
        """Find Selected."""
        docs = []
        for doc in self.database[collectionname].find(data, projection):
            doc.pop('_id')
            docs.append(doc)
        return docs

    def debug(self):
        """
        To check if the metadata db exists and is valid
        :return: Metadata db credentials
        """
        if(self.client):
            return "connected"
        else:
            return "Not connected"
            # return (MONGO_SERVER,MONGO_DB, MONGO_USER,
            #         MONGO_COLLECTION )


    def contextVariables(self):
        """
        Fetches the context variables to run in a
        specific environment
        :return: Context variables, if present
        """
        db = self.client[MONGO_DB]
        data = []
        cur = db.diq_context.find({},
            {'_id': 0, 'lastUpdatedBy': 0, 'lastUpdatedTime': 0})
        for doc in cur:
            data.append(doc)
        return data


    def pipeline(self,clientId,appId,limit,offset,searchElement):
        """
        Gets pipeline details from the metadata table including
        the number of cleansing rules and quality check rules
        applied on each pipeline
        :return:
        Returns the data in a json format
        """
        db = self.client[MONGO_DB]
        result=[]
        if(searchElement=="callinternally" or searchElement=="null"):
            cur = db.diq_pipeline_master.find({"clientId": clientId, "appId": appId},
                                              {"_id": 0, "datasetId": 1, "datastoreId": 1, "clientId": 1, "appId": 1,
                                               "dbTargetTableName": 1,
                                               "isActive": 1, "pipelineId": 1,
                                               "pipelineName": 1, "description": 1, "ingestionType": 1,
                                               "replaceRegex": 1, "replaceWords": 1,
                                               "entityRule": 1, "lastUpdated": 1, "lastUpdatedBy": 1}).skip(
                int(offset)).limit(int(limit))

            pipelineCount = db.diq_pipeline_master.find({"clientId": clientId, "appId": appId}, {"_id": 0, "datasetId": 1}).count()
        else:
            cur = db.diq_pipeline_master.find({"clientId":clientId,"appId":appId,"pipelineName":
                    {"$regex" :searchElement,"$options":"i"}},
                {"_id":0,"datasetId":1,"datastoreId":1,"clientId":1,"appId":1,
                 "dbTargetTableName":1,
                 "isActive":1,"pipelineId":1,
                 "pipelineName":1,"description":1,"ingestionType":1,
                 "replaceRegex":1,"replaceWords":1,
                 "entityRule":1,"lastUpdated":1,"lastUpdatedBy":1}).skip(int(offset)).limit(int(limit))

            pipelineCount=db.diq_pipeline_master.find({"clientId":clientId,"appId":appId,"pipelineName":
                    {"$regex" :searchElement,"$options":"i"}},{"_id":0,"datasetId":1}).count()

        if (cur.count() == 0):
            return (result, pipelineCount)
        else:

            for docum in cur:
                rereg = 0
                rewords = 0
                qcount=0
                if 'replaceRegex' in list(docum.keys()):
                    rereg = len(docum['replaceRegex'])
                    del docum['replaceRegex']
                if 'replaceWords' in list(docum.keys()):
                    rewords = len(docum['replaceWords'])
                    del docum['replaceWords']
                if 'entityRule' in list(docum.keys()):
                    qcount=len(docum['entityRule'])
                    del docum['entityRule']
                docum['CleansingRulesCount']=rereg+rewords
                docum['QualityRulesCount'] = qcount
                result.append(docum)

        self.client.close()
        return(result,pipelineCount)


    def pipeline_details(self, id):
        """
        Gets source and target details of each pipeline
        :param id:
        Parameter to identify which pipeline to fetch details for
        :return:
        Returns pipeline details in json format
        """
        db = self.client[MONGO_DB]
        cur = db.diq_pipeline_master.find({"pipelineId":str(id)},
                {"_id":0,"lastUpdated":0,"lastUpdatedTime":0,
                "entityRule.lastUpdatedTime":0,
                 "entityRule.lastUpdated":0})

        item = ""
        if(cur.count() == 0):
            return("PipelineId does not exist")
        else:
            for item in cur:
             for key in item.keys():
                 if (key=="entityRule"):
                     item["Quality_Rules"]=item["entityRule"]
                     del item["entityRule"]
                 if (key=="replaceWords"):
                     item["Cleansing_Rules"]=item["replaceWords"]
                     del item["replaceWords"]
                 if (key=="replaceRegex"):
                    item["Cleansing_Regex_Rules"]=item["replaceRegex"]
                    del item["replaceRegex"]
        self.client.close()
        return(item)


    def quality_rule_names(self):
        """
        Fetches the names and descriptions of quality check rules
        :return:
        Returns name and description in json format
        """
        db = self.client[MONGO_DB]
        result=[]
        cur = db.diq_rule_master.find({},{"_id": 0,"ruleName": 1,
            "ruleDescription": 1,"ruleNo":1,"ruleValidationQuery":1,
            "ruleValidationMessage":1})

        if (cur.count() == 0):
            return ("No quality rules present")
        else:
            for item in cur:
                result.append(item)

        self.client.close()
        return(result)


    def create_cleansing_rule(self, id):
        """
        Accepts json with various cleansing rules and columns
        on which they must be applied and populates the
        SQL tables with the data. Also performs a check
        to see if the cleansing rule is a regular expression
        and loads data into a different table if it is
        :param id:
        Unique identifier of the pipeline on which the cleansing
        rules must be applied
        :return:
        Returns data that will be inserted
        """
        db = self.client[MONGO_DB]
        data = request.json
        keys = []
        values = []
        for key, value in data.items():
            if (key == 'isRegex'): continue
            else:
                keys.append(key)
                values.append(value)
        if (data.get('isRegex') == 'Yes' ):
                cleansingRuleId = uuid.uuid4().hex
                db.diq_pipeline_master.update(
                    {'pipelineId': id},
                        {
                        "$push": {
                        "replaceRegex": {
                        "$each": [{ "cleansingRuleId":cleansingRuleId,
                                    keys[0]: values[0],
                                    keys[1]: values[1],
                                    keys[2]: values[2]}]}

                        }
                    }
                )
                db.diq_pipeline_master.update_one({'pipelineId': id},{'$set':{'lastUpdated':self.currentDate,'lastUpdatedBy':MONGO_USER}})
                cursor = db.diq_pipeline_master.find({'pipelineId':id},{'_id':0})
                for d in cursor:
                    resultset = d
                self.client.close()
                return resultset
        else:
            cleansingRuleId = uuid.uuid4().hex
            db.diq_pipeline_master.update(
                {'pipelineId': id},
                {
                    "$push": {
                        "replaceWords": {
                            "$each": [{"cleansingRuleId":cleansingRuleId,
                                       keys[0]: values[0],
                                       keys[1]: values[1],
                                       keys[2]: values[2]}]}
                    }
                }
            )
            db.diq_pipeline_master.update_one({'pipelineId': id},
                                              {'$set': {'lastUpdated': self.currentDate, 'lastUpdatedBy': MONGO_USER}})
            cursor = db.diq_pipeline_master.find({'pipelineId': id}, {'_id': 0})
            for d in cursor:
                resultset = d
            self.client.close()
            return resultset


    def find_quality_rule(self):
        """
        Fetches the rule number of the rule selected by the user
        :return:
        Rule number
        """
        db = self.client[MONGO_DB]

        data = request.json
        ruleName= data.get("ruleName")
        rule=str(ruleName)
        result1=db.diq_rule_master.find({"ruleName":rule},
                                      {"_id":0,"ruleNo":1})
        self.client.close()
        return result1


    def create_quality_rule(self,id, ruleNo):
        """
        Creates a quality rule for a particular pipeline
        :param id:
        Unque identifier of the pipeline for which the rule
        is to be created/applied
        :param ruleNo:
        Rule number of the quality check rule to be applied
        :return:
        Returns data that will be inserted
        """
        db = self.client[MONGO_DB]
        now = datetime.datetime.now()
        currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        data = request.json
        er_ref_col = data.get('referenceColumns')
        qualityRuleId = uuid.uuid4().hex
        ruleName = data["ruleName"]
        db.diq_pipeline_master.update(
            {'pipelineId': id},
            {
                "$push": {
                    "entityRule": {
                        "$each": [{"qualityRuleId":qualityRuleId,
                                   "ruleNo": ruleNo,
                                   "ruleName":ruleName,
                                   "referenceColumns": er_ref_col,
                                   "isActive": 1,
                                   "lastUpdatedBy":MONGO_USER,
                                   "lastUpdated":currentDate}]}
                }
            }
        )
        db.diq_pipeline_master.update_one({'pipelineId': id},
                                          {'$set': {'lastUpdated': self.currentDate, 'lastUpdatedBy': MONGO_USER}})
        cursor = db.diq_pipeline_master.find({'pipelineId':id},{'_id':0})
        for d in cursor:
            resultset = d
        self.client.close()
        return resultset


    def connectionValidation(self, DCData):
        """
        This method is used to validate the existence of
        source and target connections as well as to
        check if the user has the appropriate
        authorization
        :param DCData: Source and target data passed
        from Data Catalog
        :return: Success or failure message based on
        connections and authorization
        """
        if(DCData) == 0:
            data = request.json
        else:
            data = DCData
        if(data["data"]["ingestion_type"]=="SQLDatabase"):
            server_source = data["data"]["ingestion_details"]['db_source_host']
            user_source = data["data"]["ingestion_details"]['db_source_username']
            password_source = data["data"]["ingestion_details"]['db_source_password']
            db_source = data["data"]["ingestion_details"]['db_source_name']
            sourceConection = pymssql.connect(server_source, user_source,
                            password_source, db_source)
            if (sourceConection):
                server_target = data["data"]["target_details"]["db_target_host"]
                user_target = data["data"]["target_details"]["db_target_username"]
                password_target = data["data"]["target_details"]["db_target_password"]
                db_target = data["data"]["target_details"]["db_target_name"]
                targetConnection = pymssql.connect(server_target,
                                                   user_target,
                                                   password_target,
                                                   db_target)
                if (targetConnection):
                    cursor = targetConnection.cursor()
                    cursor.execute("""select name, 
                    role from (select  p.[name], r.[name] as role
                    from sys.database_role_members m
                    join
                    sys.database_principals r
                    on m.role_principal_id = r.principal_id
                    join
                    sys.database_principals p
                    on m.member_principal_id = p.principal_id
                    where
                    r.[name] = 'db_datareader' 
                    and r.[name] = 'db_datawriter'
                    and r.[name]= 'db_ddladmin' 
                    union 
                    select  p.[name], r.[name] as role
                    from sys.database_role_members m
                    join
                    sys.database_principals r
                    on m.role_principal_id = r.principal_id
                    join
                    sys.database_principals p
                    on m.member_principal_id = p.principal_id
                    where r.[name] = 'db_owner') 
                    as t1 where name= '"""+user_target+"'")
                    final_data = []
                    result = cursor.fetchall()
                    for data in result:
                        final_data.append(data[1])
                    if(final_data != ""):
                        return ("Connections validated")

        elif (data["data"]["ingestion_type"]=="FTP"):
            server_ftp = data["data"]["ingestion_details"]['ftp_source_host']
            port_ftp = data["data"]["ingestion_details"]['ftp_source_port']
            user_ftp = data["data"]["ingestion_details"]['ftp_source_username']
            password_ftp = data["data"]["ingestion_details"]['ftp_source_password']
            transport = paramiko.Transport((server_ftp, port_ftp))
            transport.connect(username=user_ftp, password=password_ftp)
            sftpConnection = paramiko.SFTPClient.from_transport\
                (transport)

            if(sftpConnection):
                server_target = data["data"]["target_details"]["db_target_host"]
                user_target = data["data"]["target_details"]["db_target_username"]
                password_target = data["data"]["target_details"]["db_target_password"]
                db_target = data["data"]["target_details"]["db_target_name"]
                targetConnection = pymssql.connect(server_target,
                                                   user_target,
                                                   password_target,
                                                   db_target)
                if (targetConnection):
                    cursor = targetConnection.cursor()
                    cursor.execute("""select name, role 
                    from (select  p.[name], r.[name] as role
                    from sys.database_role_members m
                    join
                    sys.database_principals r
                    on m.role_principal_id = r.principal_id
                    join
                    sys.database_principals p
                    on m.member_principal_id = p.principal_id
                    where
                    r.[name] = 'db_datareader' 
                    and r.[name] = 'db_datawriter'
                    and r.[name]= 'db_ddladmin' 
                    union 
                    select  p.[name], r.[name] as role
                    from
                    sys.database_role_members m
                    join
                    sys.database_principals r
                    on m.role_principal_id = r.principal_id
                    join
                    sys.database_principals p
                    on m.member_principal_id = p.principal_id
                    where
                    r.[name] = 'db_owner') as t1 
                    where name= '""" + user_target + "'")
                    final_data = []
                    result = cursor.fetchall()
                    for data in result:
                        final_data.append(data[1])
                        if(final_data != ""):
                            return ("Connections validated")
        else:
            return("Source type not recognized")


    def create_pipeline(self, data):
            """
            Creates a pipeline with all source
            and destination details
            :return:
            Returns data that will be inserted
            """
            now = datetime.datetime.now()
            db = self.client[MONGO_DB]
            currentDate = now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            #data = request.json
            url = "http://192.168.2.142:8022/datastores/"\
                  +data["datastoreId"]+"/"
            # response = requests.get(url)
            # catalogData = response.json()
            # connectionCheck = self.connectionValidation(catalogData)
            storeid = data["datastoreId"]
            setid = data["datasetId"]

        # if (connectionCheck)=="Connections validated":
            cur = db.diq_pipeline_master.find({"datasetId": {"$in": [setid]}})
            #cur=db.diq_pipeline_master.find({"datastoreId":storeid,"datasetId":setid})
            #if(cur.count()==0):
            pipeline_id = uuid.uuid4().hex
            cursor = db.diq_pipeline_master.insert({"pipelineId":pipeline_id,
                                    "pipelineName":data["pipelineName"],
                                    "description":data["description"],
                                    "actionOnData":data["actionOnData"],
                                    "datasetId":data["datasetId"],
                                    "datastoreId":data["datastoreId"],
                                    "clientId":data["clientId"],
                                    "appId":data["appId"],
                                    "isActive":data["isActive"],
                                    "createdAt":currentDate,
                                    "lastUpdated":currentDate,
                                    "lastUpdatedBy":MONGO_USER,
                                    "localDirectory":"/tmp/",
                                    "targetExists":0,
                                    "scheduleType":data["scheduleType"],
                                    "scheduleIsActive": data["scheduleIsActive"],
                                    "scheduleInterval": data["scheduleInterval"],
                                    "startDateTime": data["startDateTime"]})
            if(data["Cleansing_Rules"]):
                    for cleans_rule in data["Cleansing_Rules"]:
                        keys = []
                        values = []
                        for key, value in cleans_rule.items():
                            if (key == 'isRegex'):
                                continue
                            else:
                                keys.append(key)
                                values.append(value)
                        if (cleans_rule.get('isRegex') == 'Yes'):
                            cleansingRuleId = uuid.uuid4().hex
                            db.diq_pipeline_master.update(
                                {'pipelineId': pipeline_id},
                                {
                                    "$push": {
                                        "replaceRegex": {
                                            "$each": [{"cleansingRuleId": cleansingRuleId,
                                                       keys[0]: values[0],
                                                       keys[1]: values[1],
                                                       keys[2]: values[2]}]}
                                    }
                                }
                            )


                        else:
                            cleansingRuleId = uuid.uuid4().hex
                            db.diq_pipeline_master.update(
                                {'pipelineId': pipeline_id},
                                {
                                    "$push": {
                                        "replaceWords": {
                                            "$each": [{"cleansingRuleId": cleansingRuleId,
                                                       keys[0]: values[0],
                                                       keys[1]: values[1],
                                                       keys[2]: values[2]}]}
                                    }
                                }
                            )

            if(data["Quality_Rules"]):
                    for item in data["Quality_Rules"]:
                        result1 = db.diq_rule_master.find(
                {"ruleName": item["ruleName"]},
                {"_id": 0, "ruleNo": 1})
                        for item1 in result1:
                            rule_no=item1["ruleNo"]
                            qualityRuleId = uuid.uuid4().hex
                            db.diq_pipeline_master.update(
                    {'pipelineId': pipeline_id},
                    {
                        "$push": {
                            "entityRule": {
                                "$each": [{
                                    "qualityRuleId": qualityRuleId,
                                    "ruleNo": rule_no,
                                    "ruleName" : item["ruleName"],
                                    "referenceColumns":
                                    data["Quality_Rules"][0]
                                    ["referenceColumns"],
                                    "isActive": 1,
                                    "lastUpdatedBy": MONGO_USER,
                                    "lastUpdated": currentDate}]}
                        }
                    }
                )
            # else:
            #
            #     res = db.diq_pipeline_master.find({"datastoreId":storeid,"datasetId":setid},{"_id":0,"pipelineId":1})
            #
            #     for item in res:
            #
            #         pipeline_id = item["pipelineId"]
            #
            #     db.diq_pipeline_master.update({"datastoreId":storeid,"datasetId":setid},{
            #         "$set":{
            #                                  "pipelineName": data["pipelineName"],
            #                                  "description": data["description"],
            #                                  "actionOnData": data["actionOnData"],
            #                                  "datasetId": data["datasetId"],
            #                                  "datastoreId": data["datastoreId"],
            #                                  "clientId": data["clientId"],
            #                                  "appId": data["appId"],
            #                                  "isActive": data["isActive"],
            #                                  "lastUpdated": currentDate,
            #                                  "lastUpdatedBy": MONGO_USER,
            #                                  "localDirectory": "/tmp/",
            #                                  "targetExists": 0,
            #                                  "scheduleType": data["scheduleType"],
            #                                  "scheduleIsActive": data["scheduleIsActive"],
            #                                  "scheduleInterval": data["scheduleInterval"],
            #                                  "startDateTime": data["startDateTime"]}})
            #
            cursor = db.diq_pipeline_master.find({"pipelineId":pipeline_id},{"_id":0})
            for d in cursor:
                resultset = d

            self.client.close()
            return pipeline_id, "No error",resultset
        # else:
        #     return None, connectionCheck


    def updatePipeline(self):
        """
        Updates a pipeline with source and target details
        fetched from Catalog
        :return: Returns data that is updated

        """
        db = self.client[MONGO_DB]
        data = request.json
        if (data["data"]["ingestionType"] == "FTP"):
            cursor = db.diq_pipeline_master.update(
                {'pipelineId': data["data"]["pipelineId"]},
                {
                    "$set": {
                        "dbTargetTableName": data["data"]["targetDetails"]["dbTargetTableName"],
                        "dbTargetHost": data["data"]["targetDetails"]["dbTargetHost"],
                        "dbTargetName": data["data"]["targetDetails"]["dbTargetName"],
                        "dbTargetPassword": data["data"]["targetDetails"]["dbTargetPassword"],
                        "dbTargetPort": data["data"]["targetDetails"]["dbTargetPort"],
                        "dbTargetQueryTotal":
                            data["data"]["targetDetails"]["dbTargetQueryTotal"],
                        "dbTargetUsername": data["data"]["targetDetails"]["dbTargetUsername"],
                        "targetExists":
                            data["data"]["targetDetails"]["targetExists"],
                        "ftpSourceHost": data["data"]["ingestionDetails"]["ftpSourceHost"],
                        "ftpSourceUsername":
                            data["data"]["ingestionDetails"]["ftpSourceUsername"],
                        "ftpSourcePassword": data["data"]["ingestionDetails"]["ftpSourcePassword"],
                        "ftpSourcePort": data["data"]["ingestionDetails"]["ftpSourcePort"],
                        "ftpSourceDirectory":
                            data["data"]["ingestionDetails"]["ftpSourceDirectory"],
                        "ftpSourceFileName": data["data"]["ingestionDetails"]["ftpSourceFileName"],
                        "ftpSourceFileDelimiter":
                            data["data"]["ingestionDetails"]["ftpSourceFileDelimiter"],
                        "ftpSourceFileExtension":
                            data["data"]["ingestionDetails"]["ftpSourceFileExtension"],
                        "ftpSourceFileHeader":
                            data["data"]["ingestionDetails"]["ftpSourceFileHeader"],
                        "ftpSourceFileTextQualifier":
                            data["data"]["ingestionDetails"]["ftpSourceFileTextQualifier"]
                    }

                }

            )
        elif (data["data"]["ingestionType"] == "SQLDatabase"):
            cursor = db.diq_pipeline_master.update(
                {'pipelineId': data["data"]["pipelineId"]},
                {
                    "$set": {
                        "targetExists": data["data"]["targetDetails"]["targetExists"],
                        "dbTargetTableName": data["data"]["targetDetails"]["dbTargetTableName"],
                        "dbTargetHost": data["data"]["targetDetails"]["dbTargetHost"],
                        "dbTargetName": data["data"]["targetDetails"]["dbTargetName"],
                        "dbTargetPassword": data["data"]["targetDetails"]["dbTargetPassword"],
                        "dbTargetPort": data["data"]["targetDetails"]["dbTargetPort"],
                        "dbTargetUsername": data["data"]["targetDetails"]["dbTargetUsername"],
                        "dbSourceHost": data["data"]["ingestionDetails"]["dbSourceHost"],
                        "dbSourceName": data["data"]["ingestionDetails"]["dbSourceName"],
                        "dbSourcePassword": data["data"]["ingestionDetails"]["dbSourcePassword"],
                        "dbSourcePort": data["data"]["ingestionDetails"]["dbSourcePort"],
                        "dbSourceType": data["data"]["ingestionDetails"]["dbSourceType"],
                        "dbSourceUsername": data["data"]["ingestionDetails"]["dbSourceUsername"],

                    }

                }

            )
        for d in cursor:
            resultset = d
        self.client.close()
        return (resultset)


    def create_dag(self,pipeline_id, pipeline_url):
        """
          This method creates an airflow DAG for the
          created pipeline using schedule information
          entered
        :param pipeline_id: Unique identifier of the pipeline
        :return: Returns data if dag is successfully created
        """
        data = request.json
        db = self.client[MONGO_DB]
        # engage_name=data['pipelineName']
        start_date=data['startDateTime']
        datasetid=data['datasetId']
        datastoreid=data['datastoreId']

        if data['scheduleType']=='day':
            day=data['scheduleInterval']
            hour=0
            week=0
        elif data['scheduleType']=='hour':
            day=0
            hour=data['scheduleInterval']
            week=0
        elif data['scheduleType']=='week':
            day=0
            hour=0
            week=data['scheduleInterval']
        if data['scheduleType']=='day' or data['scheduleType']=='hour' or data['scheduleType']=='week':
            os.system('mkdir /tmp/'+str(pipeline_id))
            airf = io.open('/tmp/'+str(pipeline_id)+'/air_'+str(pipeline_id) + '.py', 'w')
            for line in io.open('resource/temp/air_parallel.py', 'r'):
                line = line.replace('$start_date', str(start_date))
                line = line.replace('$pipeid', str(pipeline_id))
                line = line.replace('$days', str(day))
                line = line.replace('$hours',str(hour))
                line = line.replace('$weeks',str(week))
                # line = line.replace('$engage_name', str(engage_name))
                line = line.replace('$datasetid', str(datasetid))
                line = line.replace('$datastoreid', str(datastoreid))
                line = line.replace('$pipelineurl', str(pipeline_url))
                airf.write(line)

            airf.close()
            context=CONTEXT
            cur = db.diq_context.find({"context":context},
                                     {"_id": 0, "airflowhost": 1, "airflowuser": 1,
                                      "airflowpassword": 1})
            host=""
            username=""
            for item in cur:
                host=item["airflowhost"]
                username=item["airflowuser"]
                password=item["airflowpassword"]
            if(AWS=='Yes'):
                os.system('scp -i /var/www/html/api-data-pipeline/src/resource/keys/pod-vm-setup-01.pem -o StrictHostKeyChecking=no -r /tmp/'+str(pipeline_id)+' '+username+'@'+host+':/home/airflow_service/airflow/dags/')
            else:
                os.system(' scp -r /tmp/'+str(pipeline_id)+' '+username+'@'+host+':airflow/dags/')
           # os.system('rm -rf /tmp/' + str(engage_name) + '_'+str(pipeline_id))
        self.client.close()
        return data


    def check_schema(self):
        """
        This method checks if the schema of
        source and target match
        :return: Success or failure message
        based on validation of table structure
        """

        data = request.json
        datastoreId = data["data"]["datastore_id"]
        dsURL = "http://10.0.1.95:8022/datastores/" + datastoreId
        response = requests.get(dsURL)
        dsData = response.json()
        targetHost = dsData["data"]["target_details"]["db_target_host"]
        targetUsername = dsData["data"]["target_details"]["db_target_username"]
        targetPassword = dsData["data"]["target_details"]["db_target_password"]
        targetDB = dsData["data"]["target_details"]["db_target_name"]

        self.conn = pymssql.connect(targetHost, targetUsername, targetPassword, targetDB)

        resulted_json_field = []
        resulted_json_type = []
        resulted_json_primary = []

        data_field = data["data"]["schema"]
        for i in data_field:
            i["is_primary_key"] = str(i["is_primary_key"])
        for i in data_field:
            resulted_json_field.append(i["name"])
            resulted_json_type.append(i["type"])

        resulted_json_field = sorted(resulted_json_field)
        self.cursor1 = self.conn.cursor()
        self.cursor2 = self.conn.cursor()
        self.cursor3 = self.conn.cursor()
        self.cursor1.execute("""SELECT
                     c.name 'Column Name'
                      FROM sys.columns c
                      INNER JOIN
                      sys.types t
                      ON c.user_type_id = t.user_type_id
                      LEFT OUTER JOIN
                      sys.index_columns ic
                      ON ic.object_id = c.object_id
                      AND ic.column_id = c.column_id
                      LEFT OUTER JOIN
                      sys.indexes i
                      ON ic.object_id = i.object_id
                      AND ic.index_id = i.index_id
                      WHERE
                      c.object_id = OBJECT_ID('""" + data["data"]
        ["db_target_table_name"] + """')""")
        results = self.cursor1.fetchall()
        result = sorted([i for (i,) in results])
        flag1 = 0
        for i in range(len(resulted_json_field)):
            if (result[i] != resulted_json_field[i]):
                flag1 = 1
        if (flag1 != 1):
            self.cursor2.execute("""SELECT
                                 t.Name 'Data type'
                                  FROM sys.columns c
                                  INNER JOIN
                                  sys.types t
                                  ON c.user_type_id = t.user_type_id
                                  LEFT OUTER JOIN
                                  sys.index_columns ic
                                  ON ic.object_id = c.object_id
                                  AND ic.column_id = c.column_id
                                  LEFT OUTER JOIN
                                  sys.indexes i
                                  ON ic.object_id = i.object_id
                                  AND ic.index_id = i.index_id
                                  WHERE
                                  c.object_id = OBJECT_ID('""" + data["data"]
            ["db_target_table_name"] + """')""")
            results = self.cursor2.fetchall()
            result1 = sorted([i for (i,) in results])

            flag2 = 0
            for i in range(len(resulted_json_type)):
                findString = '('
                if '(' in resulted_json_type[i]:
                    slicedString = resulted_json_type[i].lower()[:resulted_json_type[i].lower().find(findString)]
                    if (result1[i] != slicedString.lower()):
                        flag2 = 1
                else:
                    if (result1[i] != resulted_json_type[i].lower()):
                        flag2=1
            if (flag2 == 0):
                self.cursor3.execute("""SELECT  c.name 'Column Name'
                                   FROM sys.columns c
                                   INNER JOIN
                                   sys.types t
                                   ON c.user_type_id = t.user_type_id
                                  LEFT OUTER JOIN
                                  sys.index_columns ic
                                  ON ic.object_id = c.object_id
                                  AND ic.column_id = c.column_id
                                  LEFT OUTER JOIN
                                  sys.indexes i
                                  ON ic.object_id = i.object_id
                                  AND ic.index_id = i.index_id
                                  WHERE
                                  c.object_id = OBJECT_ID
                                  ('""" + data["data"]
                            ["db_target_table_name"] + """')
                                  and is_primary_key=1""")
                results = self.cursor3.fetchall()
                result2 = sorted([i for (i,) in results])
                for i in data_field:
                    if (i["is_primary_key"] == 'True'):
                        resulted_json_primary.append(i["name"])
                resulted_json_primary = sorted(resulted_json_primary)
                flag3 = 0
                for i in range(len(resulted_json_primary)):
                    if (result2[i] != resulted_json_primary[i]):
                        flag3 = 1

                if (flag3 == 0):
                    return ("Schema Validated")
                else:
                    return None
        self.conn.close()


    def dbClass(self, dbType):
        """
        This method accepts sourceType of DB (Oracle,
        SQLServer etc.) and returns the DB class
        specific to that database
        :param sourceType: Type of db
        :return: Success or failure message based on response
        """
        db = self.client[MONGO_DB]
        result = []
        cur = db.diq_dbclass.find({"dbType": dbType},
            {"_id": 0, "dbClass": 1})
        for docum in cur:
            result.append(docum)
        self.client.close()
        return (result)

    # def APIConfig(self):
    #     """
    #     This method is used to retrieve the ipv4 address and
    #     account name being used to run the API
    #     :return:
    #     Returns the ipv4 address and account name
    #     """
    #     ipv4 = os.popen('ip addr show eth0').read().split\
    #     ("inet ")[1].split("/")[0]
    #     result=getpass.getuser()
    #     result=result[9:]
    #     return (ipv4,result)

    # def airflowConfig(self):
    #     """
    #     This method is used to fetch the Airflow configuration
    #     parameters from the SQL table
    #     :return:
    #     Returns the data (Airflow config parameters) in a json format
    #     """
    #     self.cursor=self.conn.cursor()
    #     self.cursor.execute("select * "
    #                         "from DIQEnvVariables "
    #                         "where Context='%s'" %CONTEXT)
    #     data=self.cursor.fetchall()
    #     self.conn.close()
    #     return (data)


    def execution_status(self, datastoreId, datasetId):
        """
        This method fetches the execution status
        of a pipeline
        :param datastoreId: Unique identifier for a datastore
        :param datasetId: Unique identifier for a dataset
        :return: The execution status
        """
        db = self.client[MONGO_DB]
        cur = db.diq_pipeline_master.find({'datastoreId':datastoreId,
                                         'datasetId':datasetId},
                                 {'_id': 0, 'pipelineId':1,
                                  'dbTargetHost': 1,
                                  'dbTargetName': 1,
                                  'dbTargetUsername':1,
                                  'dbTargetPassword':1})
        TARGET_DB_NAME = ""
        TARGET_SERVER = ""
        TARGET_USERNAME = ""
        TARGET_PASSWORD = ""
        pipelineId = ""

        if (cur.count() == 0):
            return ("Pipeline not created")
        else:
            for doc in cur:
                for key in doc.keys():
                    if (key == "dbTargetName"):
                        TARGET_DB_NAME = (doc["dbTargetName"])
                    if (key == "dbTargetUsername"):
                        TARGET_USERNAME = (doc["dbTargetUsername"])
                    if (key == "dbTargetHost"):
                        TARGET_SERVER = (doc["dbTargetHost"])
                    if (key == "dbTargetPassword"):
                        TARGET_PASSWORD = (doc["dbTargetPassword"])
                    if(key == "pipelineId"):
                        pipelineId = (doc["pipelineId"])

            self.conn = pymssql.connect(TARGET_SERVER, TARGET_USERNAME,
                                        TARGET_PASSWORD, TARGET_DB_NAME)
            self.cursor = self.conn.cursor()
            self.cursor.execute('''select t2.[start] as StartDate, t1.[end] as EndDate, t1.duration as duration, t1.RunId as RunId,t1.[message] as [message]
                    from 
                    (select max(moment) as [end], max(duration/1000) as duration, RunId, [message]
                    from DIQLogs
                    where PipelineId='%s' 
                    and job='DIQController'
                    and [message] in 
                    (
                    select [message]
                    from DIQLogs 
                    where [message]! = 'NA'
                    group by RunId, [message]
                    )
                    group by [message],RunId) t1 join
                    
                    (select min(moment) as [start], RunId
                    from DIQLogs
                    where PipelineId='%s' 
                    and job='DIQController'
                    group by RunId) t2 on t1.RunId = t2.RunId'''%(pipelineId,pipelineId))
            data = self.cursor.fetchall()
        return (data)


    def validate(self,request,schema={}):
        """
        This method validates the input JSON's to
        check if they are of the correct datatypes
        :param request: Parameter to get JSON entered
        :param schema: Parameter that validates
        correct schema
        :return: Success or failure message based on output
        """
        try:
            data = json.loads(request.data.decode('utf - 8'))
        except ValueError as error:
            msg = error.args[0]
            return return_result(
                result=[], error=msg, error_code=None), \
                   'HTTP_400_BAD_REQUEST'

        try:
            validate(data, schema)
        except jsonschema.exceptions.ValidationError as err:
            msg = err.message
            reason = [item for item in str(err).split('\n')
                      if item.startswith('On instance')]
            if len(reason):
                msg = " ".join([msg, reason[0]])
            return return_result(
                result=[], error=msg, error_code=None), \
                   'HTTP_400_BAD_REQUEST'
        return data, 'HTTP_200_OK'

    def saveAndTrigger(self):
        """
        This method is used to create a pipeline
        with entered details and trigger a test
        run of the pipeline
        :return: Data entered
        """
        contextData = self.contextVariables()
        pipelineURL = (contextData[0]['pipelineurl'])
        data = request.json
        db = self.client[MONGO_DB]
        pipelineId, connectionError,resultset = self.create_pipeline(data)

        host =contextData[0]['airflowhost']
        username = contextData[0]['airflowuser']
        password = contextData[0]['airflowpassword']
        datastoreid=data['datastoreId']
        datasetid=data['datasetId']
        if(AWS=='Yes'):
            command='ssh -i /var/www/html/api-data-pipeline/src/resource/keys/pod-vm-setup-01.pem -o StrictHostKeyChecking=no '+username+'@'+host+' \'bash /home/airflow_service/airflow/scripts/DataIQ_Mongo/DIQController/DIQController_run.sh --context_param pipelineid='+pipelineId+' --context_param datastoreid='+datastoreid+' --context_param datasetid='+datasetid+' --context_param pipelineurl='+pipelineURL+'\''
        else:
            command = 'ssh ' + username + '@' + host + ' \'bash /home/service/airflow/scripts/DataIQ_Mongo/DIQController/DIQController_run.sh --context_param pipelineid=' + pipelineId + ' --context_param datastoreid=' + datastoreid + ' --context_param datasetid=' + datasetid + ' --context_param pipelineurl=' + pipelineURL + '\''
        # print(command)
        # p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
        # p=subprocess.check_output([command],shell=True)
        p = subprocess.run([command], shell=True,stdout=subprocess.PIPE)
        return p.stdout.decode("utf-8")

    def saveAndSchedule(self):
        """
         This method is used to create a pipeline
         with entered details and trigger a test
         run of the pipeline
        :param datasetId: Unique identifier of a
        dataset that the pipeline is created for
        :return: Data entered
        """
        data = request.json
        contextData = self.contextVariables()
        pipelineURL = (contextData[0]['pipelineurl'])
        # pipelineId = ""
        # db = self.client[MONGO_DB]
        # cur = db.diq_pipeline_master.find({"datasetId": datasetId},
        #                                 {"_id": 0, "pipelineId": 1})
        # if (cur.count() != 0):
        pipelineId, connectionError, resultset = self.create_pipeline(data)
        self.create_dag(pipelineId, pipelineURL)

        return resultset

    def previewSourceData(self, datastoreId, datasetId):
        """
        This method displays sample data from the source
        :param datastoreId: Unique udentifier of datastore
        :param datasetId: Unique udentifier of dataset
        :return: Data
        """
        queryEntered = request.json
        db = self.client[MONGO_DB]
        cur = db.diq_pipeline_master.find({"datasetId": datasetId},
                                        {"_id": 0})
        catalogData = 0
        if(cur.count() == 0):
            return("Pipeline does not exist")
        elif (cur.count() != 0):
            dbSource = db.diq_pipeline_master.find({"dbSourceHost": {"$exists": "true"}})
            for item in dbSource:
                if (item['dbSourceHost']):
                    url = "http://192.168.2.142:8022/datastores/" \
                          + datastoreId + "/"
                    response = requests.get(url)
                    catalogData = response.json()
        data = catalogData
        if (data["data"]["ingestion_type"] == "SQLDatabase"):
            server_source = data["data"]["ingestion_details"]['db_source_host']
            user_source = data["data"]["ingestion_details"]['db_source_username']
            password_source = data["data"]["ingestion_details"]['db_source_password']
            db_source = data["data"]["ingestion_details"]['db_source_name']
            sourceConnection = pymssql.connect(server_source, user_source,
                                               password_source, db_source)
            if (sourceConnection):
                cursor = sourceConnection.cursor()
                cursor.execute('' + queryEntered + '')
                sourceData = cursor.fetchall()
                return sourceData

        elif (data["data"]["ingestionType"] == "FTP"):
            server_ftp = data["data"]["ingestion_details"]['ftpSourceHost']
            port_ftp = data["data"]["ingestion_details"]['ftpSourcePort']
            user_ftp = data["data"]["ingestion_details"]['ftpSourceUsername']
            password_ftp = data["data"]["ingestion_details"]['ftpSourcePassword']
            file_directory_ftp = data["data"]["ingestion_details"]['ftpSourceDirectory']
            filename_ftp = data["data"]["ingestion_details"]['ftpSourceFileName']
            chars = ['*', '?']
            flag = 0
            for i in chars:
                if i in filename_ftp:
                    flag = 1
            if (flag == 0):
                ftp = FTP(''+server_ftp+'')
                ftp.login(user_ftp, password_ftp)
                r = BytesIO()
                ftp.retrbinary('RETR '+file_directory_ftp+' '+filename_ftp+'',
                           r.write)
                return (r.getvalue())
            else:
                transport = paramiko.Transport((server_ftp, port_ftp))
                transport.connect(username=user_ftp, password=password_ftp)
                sftpConnection = paramiko.SFTPClient.from_transport \
                (transport)
                if (sftpConnection):
                    files = sftpConnection.dir()
                    fileList = (files)
                    return fileList

    def previewTargetData(self, datasetId):
        """
        This method displays data from the target table
        :param datasetId:  unique identifier of a dataset
        :return: Subset of target data
        """
        db = self.client[MONGO_DB]
        cur = db.diq_pipeline_master.find({"datasetId": datasetId},
                                        {"_id": 0})
        if (cur.count() != 0):
            for item in cur:
                server_target = item['dbTargetHost']
                user_target = item['dbTargetUsername']
                password_target = item['dbTargetPassword']
                db_target = item['dbTargetName']
                targetConnection = pymssql.connect(server_target, user_target,
                                                   password_target, db_target)
                targetTableName = item["dbTargetTableName"]
                if(targetConnection):
                    cursor = targetConnection.cursor()
                    cursor.execute("select top(1000) * from %s"%targetTableName)
                    targetData = cursor.fetchall()

                    return targetData
                else:
                    return("Connection failed")




    def updateSchedule(self):
        """
        This method is used to update the schedule
        to run the pipeline
        :return: Updated schedule information
        """
        db = self.client[MONGO_DB]
        data=request.json
        db.diq_pipeline_master.update({"datasetId":data["datasetId"]},
                                    {"$set":
                                        {"scheduleInterval":
                                        data["scheduleInterval"],
                                        "scheduleType":
                                        data["scheduleType"],
                                        "scheduleIsActive":
                                        data["scheduleIsActive"],
                                        "startDateTime":
                                        data["startDateTime"],
                                         "actionOnData":
                                        data["actionOnData"]
                                         }
                                     })

        # pipelineName=data['pipelineName']
        res = db.diq_pipeline_master.find({"datasetId":
                                    data["datasetId"]},
                            {"_id":0,"pipelineId":1})
        pipelineId = ""
        for item in res:
            pipelineId = item['pipelineId']
        contextData = self.contextVariables()
        pipelineURL=(contextData[0]['pipelineurl'])
        host = (contextData[0]['airflowhost'])
        user = (contextData[0]['airflowuser'])
        db.diq_pipeline_master.update_one({'pipelineId': pipelineId},
                                          {'$set': {'lastUpdated': self.currentDate, 'lastUpdatedBy': MONGO_USER}})
        if(AWS=='Yes'):
            os.system('ssh  -i /var/www/html/api-data-pipeline/src/resource/keys/pod-vm-setup-01.pem -o StrictHostKeyChecking=no '+user+'@'+host+' rm -rf /home/airflow_service/airflow/dags/'+str(pipelineId))
        else:
            os.system('ssh '+user+'@'+host+' rm -rf airflow/dags/'+str(pipelineId))
        self.create_dag(pipelineId, pipelineURL)
        self.client.close()
        result = db.diq_pipeline_master.find({"datasetId":data["datasetId"]})
        cursor = db.diq_pipeline_master.find({'pipelineId':pipelineId},{'_id':0})
        for d in cursor:
            resultset = d
        return(resultset)

    def deleteCleansingRules(self, pipelineId,cleansingRuleId):
        """
        This method deletes a cleansing rule for the pipeline
        :param pipelineId: Unique identifier of a pipeline
        :return: Rule deleted
        """

        db = self.client[MONGO_DB]
        db.diq_pipeline_master.update_one(
            {"pipelineId": pipelineId},
            {"$pull": {'replaceRegex': {"cleansingRuleId": cleansingRuleId}}}
        )
        db.diq_pipeline_master.update(
            {"pipelineId": pipelineId},
            {"$pull": {'replaceWords': {"cleansingRuleId": cleansingRuleId
                            }}})
        self.client.close()
        return ("Cleansing rule deleted")

    def deleteQualityRules(self, pipelineId, qualityRuleId):
        """
        This method deletes a quality rule for the pipeline
        :param pipelineId: Unique identifier of a pipeline
        :return: Rule number of rule deleted
        """
        db = self.client[MONGO_DB]
        db.diq_pipeline_master.update_one(
            {"pipelineId": pipelineId},
            {"$pull": {'entityRule': {"qualityRuleId": qualityRuleId}}}
        )
        self.client.close()
        return ("Quality rule deleted")

    def deletePipeline(self):
        """
        This method deletes a pipeline
        :param pipelineId: Unique identifier of a pipeline
        :return: PIpelineId of pipeline deleted
        """
        db = self.client[MONGO_DB]
        data=request.json
        result=[]
        for pipelineId in data['ids']:
            checkPipelineId = db.diq_pipeline_master.find_one(
                {
                    "pipelineId": pipelineId
                }
            )
            result.append(checkPipelineId)
            db.diq_pipeline_master.remove(
                {'pipelineId': pipelineId})

        self.client.close()
        return result

    def updateCleansingRule(self,pipelineId,cleansingRuleId):
        """
        This method updates a cleaning rule for a pipeline
        :param pipelineId:
        :return:
        """
        db = self.client[MONGO_DB]
        data = request.json
        db.diq_pipeline_master.update({'$and': [{'pipelineId': pipelineId},
                                              {'replaceWords.cleansingRuleId': cleansingRuleId}]},
                                    {'$set': {'replaceWords.$.cleansingRuleId': cleansingRuleId,
                                              'replaceWords.$.colName': data['colName'],
                                              'replaceWords.$.replacePhrase': data['replacePhrase'],
                                              'replaceWords.$.replaceWithPhrase': data['replaceWithPhrase']}}
                                    )
        db.diq_pipeline_master.update({'$and': [{'pipelineId': pipelineId},
                                              {'replaceRegex.cleansingRuleId': cleansingRuleId}]},
                                    {'$set': {'replaceRegex.$.cleansingRuleId': cleansingRuleId,
                                              'replaceRegex.$.colName': data['colName'],
                                              'replaceRegex.$.replacePhrase': data['replacePhrase'],
                                              'replaceRegex.$.replaceWithPhrase': data['replaceWithPhrase']}}
                                    )
        db.diq_pipeline_master.update_one({'pipelineId':pipelineId },
                                          {'$set': {'lastUpdated': self.currentDate, 'lastUpdatedBy': MONGO_USER}})
        self.client.close()

        cursor = db.diq_pipeline_master.find({'pipelineId':pipelineId},{'_id':0})
        for d in cursor:
            resultset = d
        return resultset

    def updateQualityRule(self, pipelineId, qualityRuleId):
        """
        This method updates a cleaning rule for a pipeline
        :param pipelineId:
        :return:
        """
        db = self.client[MONGO_DB]
        data = request.json

        result1 = db.diq_rule_master.find({"ruleName": data["ruleName"]},{"_id": 0, "ruleNo": 1})
        for item1 in result1:
            rule_no = item1["ruleNo"]
            db.diq_pipeline_master.update({'$and': [{'pipelineId': pipelineId},
                                              {'entityRule.qualityRuleId': qualityRuleId}]},
                                    {'$set': {'entityRule.$.qualityRuleId': qualityRuleId,
                                              'entityRule.$.ruleNo': rule_no,
                                              'entityRule.$.ruleName': data["ruleName"],
                                              'entityRule.$.referenceColumns': data['referenceColumns']}})

        db.diq_pipeline_master.update_one({'pipelineId': pipelineId},
                                          {'$set': {'lastUpdated': self.currentDate, 'lastUpdatedBy': MONGO_USER}})

        cursor = db.diq_pipeline_master.find({'pipelineId':pipelineId},{'_id':0})
        for d in cursor:
            resultset = d
        self.client.close()
        return resultset

    def updatePipelineInfo(self,pipelineId):
        """
        This method updates pipeline name and description
        :param pipelineId:
        :return:
        """
        resultset = ""
        db=self.client[MONGO_DB]
        data=request.json

        db.diq_pipeline_master.update_one({'pipelineId':pipelineId},
                        {'$set':{'pipelineName':data['pipelineName'],
                        'description':data['description']}})
        clientId = data["clientId"]
        appId = data["appId"]
        pipelinesJSON, pipelineCount = self.pipeline(str(clientId), str(appId), 1000, 0,"callinternally")

        for obj in pipelinesJSON:
            if (obj["pipelineId"] == pipelineId):
                data = obj
        cursor = db.diq_pipeline_master.find({'pipelineId': pipelineId},{'_id': 0,'pipelineName':1,
                                                                          'description':1})
        for d in cursor:
            resultset = d
            print(d)
        self.client.close()
        return resultset

    def updateDataset(self, pipelineId):
        """
        :param datastoreId:  Unique id of a datastore
        :param datasetId: Unique id of a dataset
        :return: Updated values
        """

        db = self.client[MONGO_DB]
        cur = db.diq_pipeline_master.find({'pipelineId':pipelineId},
                                          {'_id': 0,'datastoreId': 1,
                                           'datasetId': 1,
                                           'dbTargetName': 1,
                                           'dbTargetTableName': 1,
                                           'dbTargetHost':1,
                                           'dbTargetUsername':1,
                                           'dbTargetPassword':1})

        hostName= ""
        username= ""
        password= ""
        dbName= ""
        targetTable=""

        for item in cur:

            if 'dbTargetName' in list(item.keys()):
                dbName = item['dbTargetName']
            if 'dbTargetTableName' in list(item.keys()):
                targetTable = item['dbTargetTableName']
            if 'dbTargetHost' in list(item.keys()):
                hostName = item['dbTargetHost']
            if 'dbTargetUsername' in list(item.keys()):
                username = item['dbTargetUsername']
            if 'dbTargetPassword' in list(item.keys()):
                password = item['dbTargetPassword']
            datastoreId=item['datastoreId']
            datasetId=item['datasetId']



        targetConnection = pymssql.connect(hostName, username,
                                          password, dbName)
        cursor = targetConnection.cursor()
        spQueryString = "use "+dbName +" exec sp_spaceused "+targetTable
        cursor.execute(spQueryString)
        jsondata = cursor.fetchall()

        totalRowNum = int(re.sub('\s +', '', jsondata[0][1]))
        diskSpaceOccupied = int(re.sub('[^0-9]', '', jsondata[0][3])) / 1024
        colCountQuery = "use "+dbName +" SELECT COUNT(*) AS numberOfColumns " \
                "FROM "+dbName +".INFORMATION_SCHEMA.COLUMNS " \
                "WHERE TABLE_NAME = N'% s'" % targetTable

        cursor.execute(colCountQuery)

        columnCount = cursor.fetchone()[0]


        datasetURL = "http://"+DATASTORE_URL+"/datastores/"+datastoreId+"/datasets"+datasetId+""

        payload = "{\n\t\"metadata\": {'total_columns': "+str(columnCount) + "," \
                  "'total_rows': "+str(totalRowNum) +"',total_size': "+str(diskSpaceOccupied)+"}\n}"

        headers = {'content-type': "application/json"}

        response = requests.request("PUT", datasetURL, data=payload, headers=headers)
        return (response.text)


    def configStoreInfo(self):
        dict1={
            'MONGO_AUTHDB':MONGO_AUTHDB,
            'MONGO_DB':MONGO_DB,
            'MONGO_PASSWORD':MONGO_PASSWORD,
            'MONGO_SERVER':MONGO_SERVER,
            'MONGO_USER':MONGO_USER,
            'DATASTORE_URL':DATASTORE_URL,
            'AWS':AWS,
            'AUTH_MECHANISM':AUTH_MECHANISM
        }

        return dict1




