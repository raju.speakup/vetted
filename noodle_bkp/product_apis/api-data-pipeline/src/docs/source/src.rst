src package
===========

Subpackages
-----------

.. toctree::

    src.commons
    src.config
    src.constants
    src.logger
    src.queries
    src.resource
    src.services

Submodules
----------

src\.apps module
----------------

.. automodule:: src.apps
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src
    :members:
    :undoc-members:
    :show-inheritance:
