#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Flask base application declaration and URL configuration."""

from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_restful_swagger import swagger
from flask_cors import CORS
from resources.health import Health
from resources.commDetailsId import Communication
from resources.commDetailsList import CommList
from resources.commsBulk import CommunicationBulk
from configuration.layer_fetch_environ import DEFAULT_IP, DEBUG, SECRETE_KEY
from flask_jwt_extended import JWTManager

from resources.commSearch import CommSearch

app = Flask(__name__)
CORS(app)
api = swagger.docs(
    Api(app),
    apiVersion='1.0.0',
    basePath="http://localhost:8000/api/v1/",
    produces=["application/json"],
    api_spec_url='/api/spec',
    description="Communication Service",
    resourcePath="/Comms"
)

# JWT settings
app.config['JWT_SECRET_KEY'] = SECRETE_KEY
jwt = JWTManager(app)

app.config['PROPAGATE_EXCEPTIONS'] = True
# ------------------------------------------------------------------------------
# Health services
# ------------------------------------------------------------------------------

# http://server/api/v1/health
api.add_resource(
    Health,
    '/api/v1/health')

# ------------------------------------------------------------------------------
# Comm Management services
# ------------------------------------------------------------------------------

# # http://server/api/v1/comms
api.add_resource(
    CommList,
    '/api/v1/client/<string:client_id>/app_id/<string:app_id>/comms')

# http://server/api/v1/comms/{comm_id}
api.add_resource(
    Communication,
    '/api/v1/client/<string:client_id>/app_id/<string:app_id>/comm/<string:comm_id>')

# http://server/api/v1/comms/_bulk}
api.add_resource(
    CommunicationBulk,
    '/api/v1/client/<string:client_id>/app_id/<string:app_id>/comms/_bulk')

api.add_resource(
    CommSearch,
    '/api/v1/client/<string:client_id>/app_id/<string:app_id>/comms/<string:searchString>')



if __name__ == '__main__':
    app.run(host=DEFAULT_IP, port=8020, debug=True)
