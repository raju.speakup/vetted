#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restful import fields
from flask_restful_swagger import swagger


@swagger.model
class CreateComm:
    """CreateClient fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String
    }


@swagger.model
class UpdateComm:
    """UpdateComm fields."""

    resource_fields = {
        'name': fields.String,
        'description': fields.String,
        'isActive': fields.Boolean
    }


@swagger.model
class BulkComm:
    """BulkComm fields."""

    resource_fields = {
        'ids': fields.List
    }
@swagger.model
class SearchComm:
    """BulkComm fields."""

    resource_fields = {
    
    }