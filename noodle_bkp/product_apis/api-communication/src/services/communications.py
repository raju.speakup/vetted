# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema, validate_user, compare_user_role
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)
from logger.logger import logger
import datetime
# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError


class CommService:
    """Comm Service"""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    def bulk_delete_comm(self, client_id, app_id, comm_ids):
        results = []
        for comm_id in comm_ids:
            try:
                result = self.connection.update_one(
                    collectionname="communications",
                    data=({
                              "comm_id": comm_id,
                              "clientid": client_id,
                              "appid": app_id


                          }, {
                              "$set": {"isActive": False}
                          }))
                results.append(comm_id)
                logger.debug(comm_id)
            except PyMongoError as err:
                logger.error(err)
                return return_result_final(
                    [],
                    err.args[0],
                    HTTP_500_INTERNAL_SERVER_ERROR
                ), HTTP_500_INTERNAL_SERVER_ERROR
        return results
