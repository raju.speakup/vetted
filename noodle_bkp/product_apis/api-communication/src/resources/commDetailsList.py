#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request, jsonify
from logger.logger import logger
from flask_jwt_extended import jwt_required

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list
from bat_auth_sdk import role_validator
from configuration.layer_fetch_environ import DB_URL, DB_NAME
from commons.decorators import validate_json, validate_schema, validate_app_user
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED, HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.communications import schema
from swagger.comm_swagger import CreateComm


class CommList(Resource):
    """Communications."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        role_validator.init(DB_URL, DB_NAME)

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "limit",
                "dataType": 'int',
                "paramType": "query",
                "description": "Total Number of Entries"
            },
            {
                "name": "offset",
                "dataType": 'int',
                "paramType": "query",
                "description": "Start index of records"
            },
            {
                "name": "projection",
                "dataType": 'string',
                "paramType": "query",
                "description": "Full OR Basic",
            },
            {
                "name": "sort",
                "dataType": 'string',
                "paramType": "query",
                "description": "Take any valid key and paste' +\
                'here [-] represents sort descending"
            }
        ],
        notes='Comm List',
        nickname='Communications')
    @jwt_required
    @role_validator.validate_app_user()
    def get(self, client_id, app_id):
        """Comm List."""
        result = get_list(self, "communications",
                          {
                              "clientid": client_id,
                              "appid": app_id,
                              "isActive": True
                          }, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('resultCount'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "parameters",
                "dataType": CreateComm.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }
        ],
        notes='Comm Create',
        nickname='Comm Create')
    @validate_json
    @validate_schema(schema)
    @jwt_required
    @role_validator.validate_app_user()
    def post(self, client_id, app_id):
        """Comm Create."""
        data = json.loads(request.data)
        try:
            data.update(
                {"comm_id": str(uuid.uuid4()),
                 "isActive": True,
                 "clientid": client_id,
                 "appid": app_id,
                 "created_at": str(datetime.datetime.now())
                 })
            self.connection.insert_one(collectionname="communications",
                                       data=json.dumps(data))
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        return return_result_final(
            data,
            1,
            self.error,
            self.error_code
        ), HTTP_201_CREATED
        
