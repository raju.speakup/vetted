#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get_list:
        post:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request, jsonify
from logger.logger import logger
from flask_jwt_extended import jwt_required

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import uuid
import datetime

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError
from bson.json_util import dumps,loads
# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.code_snippets import get_list, get_list_comm
from commons.decorators import validate_json, validate_schema, validate_app_user
from constants.custom_field_error import (
    HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED, HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.communications import schema
from swagger.comm_swagger import SearchComm

class CommSearch(Resource):

    def __init__(self):
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "searchString",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Communication List"
            }
        ],
        notes='Communication Search',
        nickname='Communication Search')
    @jwt_required
    def get(self, client_id, app_id, searchString):
        result = get_list_comm(self, "communications", {
            "name": {'$regex': searchString, "$options":'i'},
            "isActive": True}, request)
        return return_result_final(
            result[0].get('data'),
            result[0].get('resultCount'),
            result[0].get('error').get('message'),
            result[0].get('error').get('code')), result[1]

    