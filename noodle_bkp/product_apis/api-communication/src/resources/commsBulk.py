#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Comm Bulk API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
from logger.logger import logger

from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema, validate_app_user
from bat_auth_sdk import role_validator
from configuration.layer_fetch_environ import DB_URL, DB_NAME
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)
from services.communications import CommService

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.communications import schema
from swagger.comm_swagger import BulkComm


class CommunicationBulk(Resource):
    """Communication."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        self.service = CommService()
        role_validator.init(DB_URL, DB_NAME)

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "parameters",
                "dataType": BulkComm.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            }

        ],
        notes='Comm Bulk Delete',
        nickname='Comm Bulk Delete')
    @jwt_required
    @role_validator.validate_app_user()
    def delete(self, client_id, app_id):
        """Comm bulk delete."""

        data = json.loads(request.data)
        comm_ids = data['ids']
        delete_response = self.service.bulk_delete_comm(client_id, app_id, comm_ids)
        return return_result_final(
            json.dumps([{"deleted comms": delete_response}]),
            1,
            self.error,
            self.error_code), HTTP_200_OK