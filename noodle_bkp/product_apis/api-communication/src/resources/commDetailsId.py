#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Client API
    REST EndPoints:
        get:
        put:
        delete:
"""

# ------------------------------------------------------------------------------
# FLASK FEATURES IMPORT
# ------------------------------------------------------------------------------
from flask_restful import Resource
from flask import jsonify, request
from flask_restful_swagger import swagger
from logger.logger import logger

from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)

# ------------------------------------------------------------------------------
# PYMONGO FEATURES IMPORT
# ------------------------------------------------------------------------------
from pymongo.errors import PyMongoError

# ------------------------------------------------------------------------------
# PYTHON FEATURES IMPORT
# ------------------------------------------------------------------------------
import json
import datetime

# ------------------------------------------------------------------------------
# CUSTOM FUNCTION IMPORTS
# ------------------------------------------------------------------------------
from commons.mongo_services import NoodleMongoClient
from commons.util import return_result_final
from commons.decorators import validate_json, validate_schema
from bat_auth_sdk import role_validator
from configuration.layer_fetch_environ import DB_URL,DB_NAME
from constants.custom_field_error import (
    HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_404_NOT_FOUND)

# ------------------------------------------------------------------------------
# VALIDATION SCHEMA AND SWAGGER EXTERNAL IMPORT
# ------------------------------------------------------------------------------
from configuration.collection_schemas.communications import schema
from swagger.comm_swagger import UpdateComm


class Communication(Resource):
    """Communication."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        self.connection = NoodleMongoClient()
        self.error = None
        self.error_code = None
        role_validator.init(DB_URL, DB_NAME)

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "comm_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Comm ID"
            }
        ],
        notes='Comm Get',
        nickname='Communication')
    @jwt_required
    @role_validator.validate_app_user()
    def get(self, client_id, app_id, comm_id):
        """Comm Get."""
        data = dict()
        try:
            return_object = self.connection.find_one(
                collectionname="communications",
                data={"comm_id": comm_id,
                      "appid": app_id,
                      "clientid": client_id})
        except PyMongoError as err:
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                0,
                "Communication {0} is not found".format(comm_id),
                "COMM_NOT_FOUND"), HTTP_404_NOT_FOUND
        if return_object:
            return_object.pop("_id")
        return return_result_final(
            json.loads(jsonify(return_object).data.decode('utf-8')),
            1,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            },
            {
                "name": "comm_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Comm ID"
            }
        ],
        notes='Comm Delete',
        nickname='Comm Delete')
    @jwt_required
    @role_validator.validate_app_user()
    def delete(self, client_id, app_id, comm_id):
        """Comm Delete.

        This is soft delete for client - Adding is active = False"
        """
        data = list()
        try:
            return_object = self.connection.find_one(
                collectionname="communications",
                data={"comm_id": comm_id,
                      "appid": app_id,
                      "clientid": client_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR
        if not return_object:
            return return_result_final(
                data,
                0,
                "{0} Communication not found".format(comm_id),
                "COMM_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            self.connection.update_one(
                collectionname="communications",
                data=({
                          "appid": app_id,
                          "clientid": client_id,
                          "comm_id": comm_id
                      }, {
                    "$set": {"isActive": False}}))

        return return_result_final(
            json.dumps([{"message": "Soft Delete Applied"}]),
            1,
            self.error,
            self.error_code), HTTP_200_OK

    @swagger.operation(
        parameters=[
            {
                "name": "parameters",
                "dataType": UpdateComm.__name__,
                "paramType": "body",
                "required": True,
                "description": ""
            },
            {
                "name": "comm_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Comm ID"
            },
            {
                "name": "client_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "Client ID"
            },
            {
                "name": "app_id",
                "dataType": 'string',
                "paramType": "path",
                "required": True,
                "description": "App ID"
            }

        ],
        notes='Comm Update',
        nickname='Comm Update')
    @validate_json
    @validate_schema(schema)
    @jwt_required
    @role_validator.validate_app_user()
    def put(self, client_id, app_id, comm_id):
        """Comm Update."""
        data = json.loads(request.data)
        try:
            return_object = self.connection.find_one(
                collectionname="communications",
                data={"comm_id": comm_id,
                      "appid": app_id,
                      "clientid": client_id})
            logger.info(data)
        except PyMongoError as err:
            logger.error(err)
            return return_result_final(
                data,
                0,
                err.args[0],
                HTTP_500_INTERNAL_SERVER_ERROR
            ), HTTP_500_INTERNAL_SERVER_ERROR

        if not return_object:
            return return_result_final(
                data,
                0,
                "Communication {0} not found".format(comm_id),
                "COMM_NOT_FOUND"
            ), HTTP_404_NOT_FOUND
        else:
            if 'isActive' in data.keys():
                data.update({"isActive": bool(data['isActive'])})
            data.update({"updated_at": str(datetime.datetime.now()),
                         "clientid": client_id,
                         "appid": app_id})
            self.connection.update_one(
                collectionname="communications",
                data=({"comm_id": comm_id}, {
                    "$set": data}))
        return return_result_final(
            data,
            1,
            self.error,
            self.error_code), HTTP_200_OK
