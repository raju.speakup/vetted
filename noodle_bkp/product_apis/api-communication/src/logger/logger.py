#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging.config
from os import path

log_file_path = path.join(path.dirname(path.abspath(__file__)), 'logger.conf')
logging.config.fileConfig(log_file_path, disable_existing_loggers=True)
logger = logging.getLogger(__name__)