#!/usr/bin/env python
# coding: utf-8

# ## Anomaly Detection - Train
# 
# Anomaly detection (also outlier detection) is the identification of items, events or observations which do not conform to an expected pattern or other items in a dataset. In this notebook, anomaly detection is done on timeseries data, by converting timeseries into a smaller latent vector and finding reconstruction loss between the reconstructed output and the input. The higher the reconstruction loss, the more chances of it being an anomaly.
# 
# **Methodology followed:**
# * Use Variational Recurrent AutoEncoder (VRAE) for dimensionality reduction of the timeseries
# * Reconstruct output for test_dataset and compute reconstruction loss
# * Higher the score, higher the chances of it being an anomaly
# * This is a classical example of Unsupervised Learning
# 
# 
# **In this notebook:**
# * Train the VRAE model and dump it
# 
# **Paper:**
# 
# * [Variational Inference for On-line Anomaly Detection in High-Dimensional Time Series](https://arxiv.org/pdf/1412.6581.pdf)
# * [Variational Recurrent Auto-encoders](https://arxiv.org/pdf/1602.07109.pdf)
# 
# **Author: Tejas Lodaya <br>
# Version: v1 <br>**

# #### Contents
# 
# 0. [Load data and preprocess](#Load-data-and-preprocess)
# 1. [Initialize VRAE object](#Initialize-VRAE-object)
# 2. [Fit the model onto dataset](#Fit-the-model-onto-dataset)
# 3. [Save the model to be fetched later](#Save-the-model-to-be-fetched-later)

# In[1]:


from IPython.display import HTML
HTML('''<script>
code_show=true; 
function code_toggle() {
if (code_show){
$('div.input').hide();
} else {
$('div.input').show();
}
code_show = !code_show
} 
$( document ).ready(code_toggle);
</script>
<form action="javascript:code_toggle()"><input type="submit" value="Click here to toggle on/off the raw code."></form>''')


# #### Add python path

# In[18]:


from bat_expr_sdk.expr_tracker import ExprTracker
tracker = ExprTracker('6443c53d-7fac-4a40-b734-58336faca030', env='qa', debug=False, url='192.168.10.180:8811')


# In[19]:


import sys
import os
sys.path.append('../../')


# In[20]:


os.environ['CONF_STORE']= '192.168.10.180:8811'
os.environ['CONF_ENV']= 'qa'
from bat_data_sdk.data_client import DataClient
dc = DataClient()
df = dc.get_datamart('pam_train')
print(df.head())


# ### Import required modules

# In[21]:


from bat_nb_sdk.signal_extraction.clustering.vrae import VRAE
from bat_nb_sdk.signal_extraction.clustering.utils import *
import numpy as np
from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
from random import randint
import pandas as pd
import torch

from plotly.graph_objs import *
import plotly
import matplotlib
from numpy import genfromtxt
from torch.utils.data import DataLoader, TensorDataset
#plotly.offline.init_notebook_mode()


# ### Input parameters

# In[22]:


dload = './model_dir'
#train_data_file = '../data/fake_sensor_data_30000.csv'
#train_data_file = '../../data/chfdb_chf13_45590_TRAIN.pkl'


# ### Hyper parameters

# In[23]:


hidden_size = 90
number_of_features = 1
hidden_layer_depth = 1
latent_length = 20
batch_size = 8
learning_rate = 0.0005
n_epochs = 10
dropout_rate = 0.0
optimizer = 'Adam' # options: ADAM, SGD
cuda = False
print_every = 1
clip = True
max_grad_norm = 5
loss = 'MSELoss' # options: SmoothL1Loss, MSELoss, ReconLoss
block = 'LSTM' # options: LSTM, GRU


# ### Load data and preprocess

# In[24]:


#df = pd.DataFrame(pd.read_pickle(train_data_file))
X_train = df.values[:, 0]
X_train = np.expand_dims(X_train, axis = 1)


# In[25]:


#plt.plot(X_train)
#plt.show()


# **Fetch `sequence_length` from dataset**

# In[26]:


sequence_length = 100


# ### Initialize VRAE object
# 
# VRAE inherits from `sklearn.base.BaseEstimator` and overrides `fit`, `transform` and `fit_transform` functions, similar to sklearn modules

# In[27]:


vrae = VRAE(
            sequence_length=sequence_length,
            number_of_features = number_of_features,
            hidden_size = hidden_size, 
            hidden_layer_depth = hidden_layer_depth,
            latent_length = latent_length,
            batch_size = batch_size,
            learning_rate = learning_rate,
            n_epochs = n_epochs,
            dropout_rate = dropout_rate,
            optimizer = optimizer, 
            cuda = cuda,
            print_every=print_every, 
            clip=clip, 
            max_grad_norm=max_grad_norm,
            loss = loss,
            block = block,
            dload = dload)


# ### Create train dataset

# In[28]:


#train_dataset = create_train_dataset(X_train, vrae, batch_given = True)

# If batch is not given, pass `batch_given` as false. Also pass the shift_size (gap between each cut)
train_dataset = create_train_dataset(X_train, vrae, batch_given = False, shift_size = sequence_length, sequence_length=sequence_length)


# ### Fit the model onto dataset

# In[29]:


#vrae.fit(train_dataset)

#If the model has to be saved, with the learnt parameters use:
# vrae.fit(dataset, save = True)


# ### Save the model to be fetched later

# In[30]:


tracker.set_output_pkls(key='model_file',value='chfdb_chf13_45590.pth')
#vrae.save('chfdb_chf13_45590.pth')

# To load a presaved model, execute:
# vrae.load('vrae.pth')


# In[31]:


tracker.close()


# In[ ]:




