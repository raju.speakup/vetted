import os
import sys
import json
import requests
# import nbformat
import nbconvert
# from nbconvert.preprocessors import ExecutePreprocessor
from nbconvert.exporters import PythonExporter
from config import Config


conf = Config()


def get_notebook(nb_url):

    print(nb_url)
    _, nb_name = nb_url.rsplit("/", 1)
    payload = "username=test&password=test"
    headers = {
        'content-type': "application/x-www-form-urlencoded",
        'authorization': "token " + conf.get_v('jupyterhub.api_token'),
        'cache-control': "no-cache",
        'postman-token': "506704bb-4d60-33d5-9218-ff965c8a6609"
    }
    jupyter_server_name = conf.get_v('jupyterhub.server_name')
    nb_url = nb_url.replace("http://" + jupyter_server_name + "/user/test/notebooks/",
                            "http://" + jupyter_server_name + "/hub/user/test/api/contents/")
    response = requests.request("GET", nb_url, data=payload, headers=headers)

    local_nb_path = "/code/notebooks/" + nb_name
    with open(local_nb_path, "w+") as nb:
        nb.write(json.dumps(json.loads(response.text)['content']))
    return local_nb_path


def convert_notebook(nb_path):
    print("Converting notebook...")
    # from nbconvert.exporters import PythonExporter
    nb = nbconvert.exporters.export(PythonExporter, nb_path)
    # print(nb)
    # with open(nb_path) as f:
        # nb = nbformat.read(f, as_version=4)
        # ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
        # ep.preprocess(nb, {'metadata': {'path': '/'}})
        # with open('executed_notebook.ipynb', 'wt') as f:
        #     nbformat.write(nb, f)
    # print(enumerate(nb))
    # print(list(enumerate(nb)))
    file_name = '/code/{}.py'.format(os.environ.get('PY_FILENAME'))
    with open(file_name, 'w') as f:
        for i in nb[0]:
            print(i)
            f.write(i)


def run_notebook():
    print("Running notebook...")
    # os.system('')


def controller(nb_url):
    path = get_notebook(nb_url)
    # path = sys.argv[1]
    print(path)
    convert_notebook(path)
    run_notebook()


if __name__ == "__main__":
    print(sys.argv[1])
    controller(sys.argv[1])