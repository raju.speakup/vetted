#!bin/bash
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
pip3 install -r /code/requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
python /code/preprocessor.py $NB_URL
python /code/$PY_FILENAME.py > /code/$PY_FILENAME.txt

