import docker
from celery import Celery

app = Celery('anamoly', broker="amqp://admin:pass@192.168.10.185:5672/pam", backend="amqp://admin:pass@192.168.10.185:5672/pam")

client = docker.from_env()


def create_image(path):
    img_obj = client.images.build(path=path)


@app.task
def create_container(nb_url, py_filename):
        obj = client.containers.run(
            image='python:3.6-jessie',
            name=py_filename,
            command=[
                'apt-get -qq -yy update',
                'apt-get -qq -yy install git freetds-dev  freetds-common freetds-bin libxml-xpath-perl curl vim',
                'apt-get -qq -yy install apache2 apache2-dev python-dev sshpass',
                'apt-get -qq -yy install libldap2-dev libsasl2-dev',
                'mkdir /code/',
                'mkdir /code/notebooks/'
                'chmod -R 777 /code/notebooks/'
                'cp notebook_deploy.sh /',
                'chmod +x /notebook_deploy.sh'

            ],
            environment={
                'CONF_STORE': '192.168.10.180:8811',
                'CONF_ENV': 'qa',
                'NB_URL': nb_url,
                'PY_FILENAME': py_filename
            },
            volumes={
                '/home/bat-user/code/notebook_celery': {
                    'bind': '/code',
                    'mode': 'rw'
                }
            },
            entrypoint=['/bin/bash', '/code/celery_docker/notebook_deploy.sh'],
            auto_remove=True,
            remove=True,
            detach=True
        )
        for line in obj.logs(stream=True):
            print(line.strip())

# create_image('/home/bat-user/code/notebook_celery/celery_docker')
# create_container('http://192.168.2.142:8006/user/test/notebooks/PAM%20Data/anomaly_detection_train.ipynb')