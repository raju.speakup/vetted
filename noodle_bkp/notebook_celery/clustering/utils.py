from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
import numpy as np
from random import randint
import os
from torch.utils.data import DataLoader, TensorDataset
import math
import torch
import pandas as pd
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from plotly.graph_objs import *
import plotly

from sklearn.metrics import mean_squared_error


def plot_clustering(z_run, labels, engine='plotly', download=False, folder_name='clustering'):
    """
    Given latent variables for all timeseries, and output of k-means, run PCA and tSNE on latent vectors and color the points using cluster_labels.
    :param z_run: Latent vectors for all input tensors
    :param labels: Cluster labels for all input tensors
    :param engine: plotly/matplotlib
    :param download: If true, it will download plots in `folder_name`
    :param folder_name: Download folder to dump plots
    :return:
    """

    def plot_clustering_plotly(z_run, labels):

        labels = labels[:z_run.shape[0]]  # because of weird batch_size

        hex_colors = []
        for _ in np.unique(labels):
            hex_colors.append('#%06X' % randint(0, 0xFFFFFF))

        colors = [hex_colors[int(i)] for i in labels]

        z_run_pca = TruncatedSVD(n_components=3).fit_transform(z_run)
        z_run_tsne = TSNE(perplexity=80, min_grad_norm=1E-12, n_iter=3000).fit_transform(z_run)

        trace = Scatter(
            x=z_run_pca[:, 0],
            y=z_run_pca[:, 1],
            mode='markers',
            marker=dict(color=colors)
        )
        data = Data([trace])
        layout = Layout(
            title='PCA on z_run',
            showlegend=False
        )
        fig = Figure(data=data, layout=layout)
        plotly.offline.iplot(fig)

        trace = Scatter(
            x=z_run_tsne[:, 0],
            y=z_run_tsne[:, 1],
            mode='markers',
            marker=dict(color=colors)
        )
        data = Data([trace])
        layout = Layout(
            title='tSNE on z_run',
            showlegend=False
        )
        fig = Figure(data=data, layout=layout)
        plotly.offline.iplot(fig)

    def plot_clustering_matplotlib(z_run, labels, download, folder_name):

        labels = labels[:z_run.shape[0]]  # because of weird batch_size

        hex_colors = []
        for _ in np.unique(labels):
            hex_colors.append('#%06X' % randint(0, 0xFFFFFF))

        colors = [hex_colors[int(i)] for i in labels]

        z_run_pca = TruncatedSVD(n_components=3).fit_transform(z_run)
        z_run_tsne = TSNE(perplexity=80, min_grad_norm=1E-12, n_iter=3000).fit_transform(z_run)

        plt.scatter(z_run_pca[:, 0], z_run_pca[:, 1], c=colors, marker='*', linewidths=0)
        if download:
            if os.path.exists(folder_name):
                pass
            else:
                os.mkdir(folder_name)
            plt.savefig(folder_name + "/pca.png")
        else:
            plt.show()

        plt.scatter(z_run_tsne[:, 0], z_run_tsne[:, 1], c=colors, marker='*', linewidths=0)
        if download:
            if os.path.exists(folder_name):
                pass
            else:
                os.mkdir(folder_name)
            plt.savefig(folder_name + "/tsne.png")
        else:
            plt.show()

    if (download == False) & (engine == 'plotly'):
        plot_clustering_plotly(z_run, labels)
    if (download) & (engine == 'plotly'):
        print("Can't download plotly plots")
    if engine == 'matplotlib':
        plot_clustering_matplotlib(z_run, labels, download, folder_name)


def open_data(direc, ratio_train=0.8, dataset="ECG5000"):
    """Input:
    direc: location of the UCR archive
    ratio_train: ratio to split training and testset
    dataset: name of the dataset in the UCR archive"""
    datadir = direc + '/' + dataset + '/' + dataset
    data_train = np.loadtxt(datadir + '_TRAIN', delimiter=',')
    data_test_val = np.loadtxt(datadir + '_TEST', delimiter=',')[:-1]
    data = np.concatenate((data_train, data_test_val), axis=0)

    N, D = data.shape

    ind_cut = int(ratio_train * N)
    ind = np.random.permutation(N)
    return data[ind[:ind_cut], 1:], data[ind[ind_cut:], 1:], data[ind[:ind_cut], 0], data[ind[ind_cut:], 0]


def anomaly_score(x, x_decoded):
    x = x[:len(x_decoded)]
    loss = np.array([mean_squared_error(i, j) for i, j in zip(x, x_decoded)])
    return loss


def cut_and_score(X_test, vrae, batch_given=False, shift_size=None, sequence_length=None):
    if not batch_given:
        if not sequence_length:  # if sequence_length is not given, fetch it from the model.
            sequence_length = vrae.sequence_length
        num_of_sequences = math.floor((X_test.shape[0] - sequence_length) / shift_size) + 1
        start_indices = [shift_size * each_sequence for each_sequence in range(num_of_sequences)]
        end_indices = [sequence_length + (shift_size * each_sequence) for each_sequence in range(num_of_sequences)]

        complete_batches = math.floor(num_of_sequences / vrae.batch_size)
        X_test_mod = np.zeros(((complete_batches + 1) * vrae.batch_size, sequence_length, X_test.shape[1]))

        for k, (i, j) in enumerate(zip(start_indices, end_indices)):
            X_test_mod[k, :, :] = X_test[i:j, :]

        test_dataset = TensorDataset(torch.from_numpy(X_test_mod))
        x_decoded = vrae.reconstruct(test_dataset).swapaxes(0, 1)
        x_decoded = x_decoded[:num_of_sequences, :, :]
        X_test_mod = X_test_mod[:num_of_sequences, :, :]

        score_df = pd.DataFrame({'start': start_indices,
                                 'end': end_indices,
                                 'anomaly_score': anomaly_score(x_decoded, X_test_mod)})

        return score_df
    else:
        test_dataset = TensorDataset(torch.from_numpy(X_test))
        x_decoded = vrae.reconstruct(test_dataset)

        score_df = pd.DataFrame({'index': range(x_decoded.shape[0]),
                                 'anomaly_score': anomaly_score(x_decoded, X_test)})

        return score_df


def create_train_dataset(X_train, vrae, batch_given=False, shift_size=None, sequence_length=None):
    if not batch_given:
        if not sequence_length:
            sequence_length = vrae.sequence_length
        num_of_sequences = math.floor((X_train.shape[0] - sequence_length) / shift_size) + 1
        start_indices = [shift_size * each_sequence for each_sequence in range(num_of_sequences)]
        end_indices = [sequence_length + (shift_size * each_sequence) for each_sequence in range(num_of_sequences)]

        complete_batches = math.floor(num_of_sequences / vrae.batch_size)
        X_train_mod = np.zeros(((complete_batches + 1) * vrae.batch_size, sequence_length, X_train.shape[1]))

        for i in start_indices[((2 * len(start_indices)) - ((complete_batches + 1) * vrae.batch_size)):]:
            start_indices.append(i)
        for i in end_indices[((2 * len(end_indices)) - ((complete_batches + 1) * vrae.batch_size)):]:
            end_indices.append(i)

        for k, (i, j) in enumerate(zip(start_indices, end_indices)):
            X_train_mod[k, :, :] = X_train[i:j, :]

        train_dataset = TensorDataset(torch.from_numpy(X_train_mod))
        return train_dataset
    else:
        train_dataset = TensorDataset(torch.from_numpy(X_train))
        return train_dataset

def plot_anomaly(X_test, score_df):
    score_array = np.zeros_like(X_test)
    for _, i in score_df.iterrows():
        start = int(i['start'])
        end = int(i['end'])
        score = i['anomaly_score']
        score_array[start:end, :] =  score
    
    number_of_features = X_test.shape[1]
    for i in range(number_of_features):        
        fig, ax1 = plt.subplots()
        ax1.plot(X_test[:, i])
        ax1.set_xlabel('Index')
        ax1.set_ylabel('Value')
        ax2 = ax1.twinx()

        ax2.set_ylabel('score', color='r')
        ax2.plot(score_array, color = 'r')
        fig.tight_layout()
        plt.show()
        plt.close()