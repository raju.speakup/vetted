from celery import group
from celery_docker.docker_utils import create_container


job_list = list()
for i in range(0, 2):
    job_list.append(
        create_container.s(
            nb_url='http://192.168.2.142:8006/user/test/notebooks/PAM%20Data/anomaly_detection_train.ipynb',
            py_filename='anomaly_detection_train_{}'.format(str(i))

        )
    )
jobs = group(job_list)
j_no = str(len(job_list))
print("No of tasks : {} ".format(j_no))
result = jobs.apply_async()
