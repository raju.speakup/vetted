#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""python custom decorators."""

from bat_auth_sdk.constants import HTTP_400_BAD_REQUEST,HTTP_200_OK,\
    HTTP_401_UNAUTHORIZED, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN
from bat_auth_sdk.mongo_client import NoodleMongoClient

from functools import wraps
from jsonschema import validate, ValidationError

from flask import (
    jsonify,
    request,
)
import json
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    get_raw_jwt,
    get_jwt_claims,
    jwt_refresh_token_required)
from pymongo.errors import PyMongoError

db_url = None
db_name = None
connection = None


def init(url, database_name):
    global db_url, db_name, connection
    db_url = url
    db_name = database_name
    connection = NoodleMongoClient(db_url, db_name)


def validate_app_user():
    """Validate app user for the invoked service. Client Id and User Id needs to be passed to the service"""
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                user_claims = get_jwt_claims()
                user_id = user_claims['user_id']
                if 'clientid' in kw.keys():
                    client_id = kw['clientid']
                if 'appid' in kw.keys():
                    app_id = kw['appid']
                if 'clientid' not in kw.keys():
                    client_id = request.headers.get('clientid')
                if 'appid' not in kw.keys():
                    app_id = request.headers.get('appid')
                if not client_id or not app_id:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_404_NOT_FOUND",
                                "message": "client_id or app_id is not passed to the service"
                            }
                        })
                    response.status_code = HTTP_404_NOT_FOUND
                    return response
                try:
                    return_object = connection.find_selected_field(
                        collectionname="permissions",
                        data={
                            "user_id": user_id,
                            "clientid": {"$in": [client_id, '*']},
                            "entityId": {"$in": [app_id, '*']},
                            "isActive": True,
                            "entityType": {"$in": ["app", '*']}
                        },
                        projection={"clientid": 1,
                                    "entityId": 1,
                                    "entityType": 1,
                                    "permission": 1}

                    )
                except PyMongoError as err:
                    response = jsonify(
                        {
                            "error": {
                                "code": err.args[0],
                                "message": "Internal Server Error"
                            }
                        })
                    response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
                    return response
                if return_object:
                    if return_object[0]['entityId'] in [app_id, '*']:
                        if request.method != 'GET' and return_object[0]['permission'] == 'read':
                            response = jsonify(
                                {
                                    "error": {
                                        "code": "HTTP_403_FORBIDDEN",
                                        "message": "User does not have required permission for this action"
                                    }
                                })
                            response.status_code = HTTP_403_FORBIDDEN
                            return response
                    else:
                        response = jsonify(
                            {
                                "error": {
                                    "code": "HTTP_403_FORBIDDEN",
                                    "message": "User does not have access to this Application"
                                }
                            })
                        response.status_code = HTTP_403_FORBIDDEN
                        return response

                else:
                    response = jsonify(
                        {
                            "error": {
                                "code": "HTTP_403_FORBIDDEN",
                                "message": "User does not have access to this Application"
                            }
                        })
                    response.status_code = HTTP_403_FORBIDDEN
                    return response

            except ValidationError as err:
                response = json.loads(json.dumps(
                    {
                        "error": {
                            "code": "HTTP_400_BAD_REQUEST",
                            "message": err.args[0]
                        }
                    }))
                return response, HTTP_400_BAD_REQUEST
            return f(*args, **kw)
        return wrapper
    return decorator