from os import getenv
import boto3
import pandas as pd
import _pickle as cPickle


def version():
    return "1.0.0"


def save(fileName, dataframe):
    dataframe.to_pickle(fileName)

    s3 = boto3.client('s3')
    resource = boto3.resource('s3')
    bucket = resource.Bucket('noodleai-postman-collections')

    return bucket.upload_file(fileName, Key=fileName)


def load(fileName):
    s3 = boto3.client('s3')
    resource = boto3.resource('s3')
    bucket = resource.Bucket('noodleai-postman-collections')

    obj = s3.get_object(Bucket='noodleai-postman-collections', Key=fileName)
    string_pkl = obj['Body'].read()
    df = cPickle.loads(string_pkl)
    return df