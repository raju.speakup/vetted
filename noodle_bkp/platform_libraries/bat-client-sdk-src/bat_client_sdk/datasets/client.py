from os import getenv
import pymssql
import pandas as pd


def version():
    return "1.0.0"


def load1(tableName, top=100):
    server = "PreProd"
    user = "noodle_product_dev"
    password = "NoodleProdDev1403#"

    conn = pymssql.connect(server, user, password, "NoodleApp")
    stmt = "SELECT TOP " + str(top) + " * FROM " + tableName

    return pd.read_sql(stmt, conn)


def load(tableName, server="PreProd", user="noodle_product_dev", password="NoodleProdDev1403#", db_name="NoodleApp",
         top=100):
    conn = pymssql.connect(server, user, password, db_name)
    stmt = "SELECT TOP " + str(top) + " * FROM " + tableName

    return pd.read_sql(stmt, conn)