from distutils.core import setup
from setuptools import setup, find_packages

setup(
    # Application name:
    name="bat_client_sdk",

    # Version number (initial):
    version="1.0.3",

    # Application author details:
    author="nimesh agarwal",
    author_email="nimesh.agarwal@noodle.ai",

    # Packages
    packages=find_packages(),

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://192.168.2.129:8081/repository/python-local/",

    #
    # license="LICENSE.txt",
    description="Library to interact with BAT platform",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "requests",
        "pandas",
        "boto3",
        "pymssql",
        "jsonschema",
        "pymongo"
    ],
)
