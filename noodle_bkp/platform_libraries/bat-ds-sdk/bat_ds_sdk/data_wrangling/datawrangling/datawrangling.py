import pandas as pd
import warnings
import collections

warnings.filterwarnings('ignore')


class DataWrangling(object):
    """ The main DataWrangling object

    The DataWrangling is a python class which provides methods
    for data wrangling.
    Data wrangling is the process of transforming and mapping data from one raw data form into
    another format with the intent of making it more appropriate and valuable for a variety of downstream purposes such as analytics.
    It is formed from the pandas dataframe.

    :param data: pandas dataFrame
    :return: :class:`DataWrangling` object

    Usage::
            >>> data_file = "datafile.csv"
            >>> df = pd.read_csv(data_file)
            >>> data_wrangling = DataWrangling(df)

    """

    def __init__(self, data):
        """  Initializing the class variables.

        :param data: pandas dataFrame
        """
        self.data = data

    def append(self, col1, col2):
        """ Append one column to another

        :param col1: str,
                     name of the column to which to be appended
        :param col2: str,
                     name of the column which is appended
        :return: pandas dataFrame
        """
        dataframe = self.data[col1].append(self.data[col2], ignore_index=True)
        return dataframe.to_frame()

    def stack(self):
        """ Stack the dataframe.

        Stacking a DataFrame means moving (also rotating or pivoting) the
        innermost column index to become the innermost row index.

        :return: pandas dataFrame
        """
        dataframe = self.data.stack()
        return dataframe.to_frame()

    def unstack(self):
        """ Unstack the dataframe

        :return: pandas dataFrame
        """

        dataframe = self.data.unstack()
        return dataframe.to_frame()

    def filter_items(self, column, name):
        """ This function is used to filter out columns.

        :param column: list of column names.
        :param   name: str,
                       action performed.

        """
        self.data[[s + "_" + name for s in column]] = self.data.filter(items=column)

    def filter_regex(self, regex, axis):
        """ Filter data using regex.

        :param regex: str,s
                      the regular expression to be matched.
        :param  axis: int,
                      the axis to filter on.

        :return: pandas dataFrame
        """
        return self.data.filter(regex=regex, axis=axis)

    def merge(self, dataframe1, dataframe2, type):
        """ Merge/Join two dataframes.

        :param dataframe1: pandas dataFrame
        :param dataframe2: pandas dataFrame
        :param       type: str,
                           type of join.

        :return: pandas dataFrame
        """
        return dataframe1.merge(dataframe2, how=type)

    def union(self, column):
        """ Union of the data

        :param column: list of column names.

        :return: pandas dataFrame
        """
        a = []
        for i in column:
            a.append(self.data[i])
        return pd.concat(a, axis=1, join='outer')

    def intersection(self, column):
        """ Intersection the data

        :param column: list of column names.

        :return: pandas dataFrame
        """
        a = []
        for i in column:
            a.append(self.data[i])
        return pd.concat(a, axis=1, join='inner')

    def subset(self, column, cond, value):
        """ Subset from a particular column.

        :param column: str,
                       column name.
        :param   cond: str,
                       on what condition should be satisfied(==/</<=/>/>=/!=).
        :param  value: int,

        :return: pandas DataFrame
        """
        val = pd.DataFrame()
        if cond == "==":
            self.data = self.data[self.data[column] == value]
        elif cond == "<":
            self.data = self.data[self.data[column] < value]
        elif cond == "<=":
            self.data = self.data[self.data[column] <= value]
        elif cond == ">":
            self.data = self.data[self.data[column] > value]
        elif cond == ">=":
            self.data = self.data[self.data[column] >= value]
        elif cond == "!=":
            self.data = self.data[self.data[column] != value]
        return val

    def concat(self, col1, col2, name):
        """ Concatenates two columns.

        :param col1: str,
                     name of column 1.
        :param col2: str,
                     name of column 2.
        :param name: str,
                     name of the new column.
        :return: pandas dataFrame
        """
        return pd.DataFrame(data=(pd.concat([self.data[col1], self.data[col2]])), columns=[name])

    def select_run(self, dict):
        """ This is an utility function to invoke multiple data wrangling methods.

        :param dict: dict,
                     it specifies a dictionary where method names are keys and the corresponding parameter list are its  values.
        """
        dict = collections.OrderedDict(dict)
        for i in dict.keys():
            method = getattr(self, i)
            if type(dict[i]) is tuple:
                a = method(*dict[i])
            else:
                a = method(dict[i])
