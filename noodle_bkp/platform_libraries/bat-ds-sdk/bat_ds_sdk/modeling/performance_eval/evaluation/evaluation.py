import pandas as pd
import numpy as np
from sklearn import metrics
import matplotlib.pyplot as plt
import plotly.graph_objs as go
from plotly.graph_objs import *
import os
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from tabulate import tabulate

plt.rcParams["figure.figsize"] = (20, 14)
init_notebook_mode(connected=True)

class PerformanceEvaluation(object):
    """ PerformanceEvaluation class is used for the evaluation of predictions for different models.

    :return: :class:`PerformanceEvaluation` object

    Usage::
            >>> eval_performance = PerformanceEvaluation(actuals = actuals, predictions = predictions,id_cols = id_cols, target_id_cols = target_id_cols,  dep_col = dep_col, pred_col = pred_col, date_col = date_col, topK = topK, dload = dload,metric = metric,threshold=threshold)

    """
    def __init__(self, actuals, predictions, id_cols, target_id_cols, dep_col, pred_col, date_col, test_end_dates = None,topK = 5, dload = '.',metric = 'mape', threshold = 10,filter_with_threshold = False,include_nan = True ):
        """
        :param actuals : pandas `DataFrame`,
                         Actual data.
        :param predictions : pandas `DataFrame`,
                             predicted data.
        :param id_cols : list of str,
                         id columns to identify each time series uniquely.
        :param target_id_cols : list of str,
                                column names based on which to group the time series.
        :param dep_col : str,
                         dependent column name.
        :param pred_col : str,
                          predicted column name.
        :param date_col : str,
                          date column.
        :param topK : int,
                      top K value.
        :param dload : str,
                       download path.
        :param metric : str or list of str,
                        metric that has to computed.
        :param threshold : int,
                           aggregate sum of a group should satisfy the threshold.
        :param filter_with_threshold: bool,
                                      True if only time series who aggregate sum meets the threshold has to be considered.
        :param include_nan: bool,
                            include nan for metric values.
        """
        self.actual = actuals
        self.prediction = predictions
        self.id_cols = id_cols
        self.target_id_cols = target_id_cols
        self.dep_col = dep_col
        self.pred_col = pred_col
        self.date_col = date_col
        self.test_end_dates = test_end_dates
        self.topK = topK
        self.result = None
        self.__preprocess()
        self.__merge_actual_pred()
        self.metric_values = None
        self._groups = None
        self.dload = dload
        self.metric = metric
        self.threshold = threshold
        self.filter_with_threshold = filter_with_threshold
        self.include_nan = include_nan
        self.summary = None
        self.describe()
        if not isinstance(self.metric,list):
            self.metric = [self.metric]
        if self.test_end_dates is None :
            self.test_end_dates = [str(self.prediction[self.date_col].max())]
        self.test_end_dates = pd.to_datetime(self.test_end_dates,format = '%d/%m/%Y')
        
    def __preprocess(self):
        """  Utility function to preprocess the data.

        """
        self.actual = self.actual.loc[:, self.id_cols + [self.dep_col, self.date_col]]
#         self.actual.loc[self.actual[self.dep_col] <=0, self.dep_col] = 0
#         self.prediction.loc[self.prediction[self.pred_col] <= 0, self.pred_col] = 0
        self.prediction[self.date_col] = pd.to_datetime(self.prediction[self.date_col])
        self.actual[self.date_col] = pd.to_datetime(self.actual[self.date_col])

    def __merge_actual_pred(self):
        """ Utility function of merge the actual and predicted dataframe.
         
        """
        self.result = self.actual.merge(self.prediction, how='left')
        self.result[self.date_col] = pd.to_datetime(self.result[self.date_col])
        self.result.sort_values(self.id_cols + [self.date_col], inplace=True)
        self.result = self.result[ self.result[self.date_col] <= self.prediction[self.date_col].max()]
        
    def umape(self,actual,pred):
        """ Computes Unit Mape.
        
        :param actual: list of float,
                       actual values.
        :param pred: list of float,
                     predicted values.
        :return: float
        """
        if actual.sum() == 0:
            return np.inf
        return float(format((100*  (abs(actual - pred)).sum()/actual.sum() ),'.3f'))
    
    def mape(self,actual,pred):
        """ Computes Mape.
        
        :param actual: list of float,
                       actual values.
        :param pred: list of float,
                     predicted values.
        :return: float
        """
        if actual.sum() == 0:
            return np.inf
        return  float(
                        format((100*  ((abs((actual - pred)/actual )).mean())),'.3f'))
       

    def rmse(self,actual,pred):
        """ Computes RMSE.
        
        :param actual: list of float,
                       actual values.
        :param pred: list of float,
                     predicted values.
        :return: float
        """
        return float(format((np.sqrt(metrics.mean_squared_error(actual,pred))),'.3f'))

    def mean_abs(self,actual,pred):
        """ Computes Mean absolute error.
        
        :param actual: list of float,
                       actual values.
        :param pred: list of float,
                     predicted values.
        :return: float
        """
        return float(format((metrics.mean_absolute_error(actual,pred)),'.3f'))

    def calculate_metrics(self):
        """ To calculate the metrics.

        """
        self.metric_values = pd.DataFrame(columns = self.id_cols + ['test_end_date'] + self.metric )
        self._groups = self.result.groupby(self.target_id_cols)
        for i in self._groups.indices:
            for k in self.test_end_dates:
                _ = pd.DataFrame(self._groups.get_group(i)).dropna()
                _ = _[_[self.date_col] <= k]
                if self.filter_with_threshold and _[self.dep_col].sum() < self.threshold:
                    continue
                list_temp = []
                for j in self.metric:
                    method = getattr(self, j)
                    list_temp.append( method(_[self.dep_col], _[self.pred_col]) )
                if len(self.target_id_cols) > 1 :
                    cols_names = list(i)
                else :
                    cols_names = [i]
                self.metric_values.loc[self.metric_values.shape[0]] = cols_names + [k] + list_temp
        self.metric_values.sort_values(self.target_id_cols + ['test_end_date'],inplace = True)
        
    def plot_topK_good_fit(self, engine = 'matplotlib', download = 'False' , folder_name = 'ts_topK'):
        """ Plot Top K good fits.
        
        :param engine: str,
                       'plotly' or 'matplotlib
        :param download: bool,
                         True if the plot has to be downloaded else False
        :param folder_name: str,
                            name of the folder where plot has to be saved.
                            
        """
        for date in self.test_end_dates:
            df = self.metric_values[self.metric_values['test_end_date'] == date]
            for i in self.metric:
                sort_df = df.sort_values(i)[:self.topK]
                for j in sort_df[self.target_id_cols].values:
                    if len(j)>1:
                        _ = list(j)
                    else:
                        _ = j
                    self.plot_specfic_ts( group = _ , engine = engine , download = download , folder_name = folder_name,metric = i,end_date = date)

    def plot_topK_bad_fit(self, engine = 'matplotlib', download = 'False' , folder_name = 'ts_topK'):
        """ Plot Top K bad fits.
        
        :param engine: str,
                       'plotly' or 'matplotlib
        :param download: bool,
                         True if the plot has to be downloaded else False
        :param folder_name: str,
                            name of the folder where plot has to be saved.
        """
        for date in self.test_end_dates:
            df = self.metric_values[self.metric_values['test_end_date'] == date]
            for i in self.metric:
                if self.include_nan:
                    sort_df = df.sort_values(i,ascending = False)[:self.topK]
                else :
                    df = df[np.isfinite(df[i])]
                    sort_df = df.sort_values(i,ascending = False)[:self.topK]
                for j in sort_df[self.target_id_cols].values:
                    if len(j)>1:
                        _ = list(j)
                    else:
                        _ = j
                    self.plot_specfic_ts( group = _ , engine = engine , download = download , folder_name = folder_name,metric = i,end_date = date)
                
                
    def plot_specfic_ts(self, group = None, engine = 'matplotlib', download = 'False' , folder_name = 'ts_specific', metric = None,end_date = None):
        """ Plot a specific time series.
        
        :param group: list or tuple,
                      unique Id of the time series.
        :param engine: str,
                       'plotly' or 'matplotlib
        :param download: bool,
                         True if the plot has to be downloaded else False
        :param folder_name: str,
                            name of the folder where plot has to be saved.              
        :param metric: str,
                       metric name.
        :param end_date: list of timestamp,
                         test end dates.
        """
        
        if len(group)==1:
            group = str(group[0])
        if self.__subset_data(self.metric_values,self.target_id_cols,group).empty:
            raise ValueError("Incorrect group ID or aggregate sum less than the threshold")
        if isinstance(group,list):
            g = tuple(group)
        else:
            g = group
        df = pd.DataFrame(self._groups.get_group(g))
        if not isinstance(group,list):
            group = [group]
            
        def __plot_specfic_ts_plotly( df,metric,end_date):
            """  Plots using plotly.
            """
            
            str_temp = str(dict(zip(self.target_id_cols,group))).replace('{','').replace('}','')
            if metric is not None :
                x = self.__subset_data(self.metric_values,self.target_id_cols,group)
                x = str(x[x['test_end_date'] == end_date].reset_index(drop = True)[metric][0])
                title= str_temp + '<br>' + metric + ' : ' + x + '<br>' + 'Test end date : ' + str(end_date)
            else :
                title = str_temp
            shapes = list()
            for i in self.test_end_dates:
                shapes.append({'type': 'line',
                               'x0': i,
                               'y0': 0,
                               'x1': i,
                               'y1': max(df[self.dep_col]),
                               'line': {
                                        'color': 'rgb(128, 0, 128)',
                                        'width': 1,
                                        'dash': 'dot',
                                        }
                              })
            layout = go.Layout(
                title= title,
                xaxis=dict(title=self.date_col),
                yaxis=dict(title=self.dep_col),
                shapes = shapes
                )

            all_traces = []

            if not df[self.dep_col] is None:
                all_traces.append(
                    go.Scatter(
                        x=df[self.date_col],
                        y=df[self.dep_col],
                        name='Actual'
                    )
                )

            all_traces.append(
                go.Scatter(
                    x = df[self.date_col],
                    y = df[self.pred_col],
                    name = 'Predicted'))

            fig = go.Figure(data=all_traces, layout=layout)
            return fig

        def __plot_specfic_ts_matplotlib( df, download, folder_name,metric,end_date):
            """ Plots using matplotlib.
            """
            #self.dload = os.path.expanduser(directory)
            save_dir = self.dload +  "/" + folder_name
            str_temp = str(dict(zip(self.target_id_cols,group))).replace('{','').replace('}','')
            if metric is not None :
                x = self.__subset_data(self.metric_values,self.target_id_cols,group)
                x = str(x[x['test_end_date'] == end_date].reset_index(drop = True)[metric][0])
                title= str_temp + '\n'+ metric + ' : ' + x + '\n' + 'Test end date : ' + str(end_date)
            else :
                title = str_temp
            plt.figure(figsize=(20, 12))
            if not df[self.dep_col] is None:
                plt.plot(df[self.date_col], df[self.dep_col], label = 'Actual')
            plt.plot(df[self.date_col], df[self.pred_col], label = 'Predicted')
            _ = [plt.axvline(i,linewidth=1, color='g',linestyle = 'dashdot') for i in self.test_end_dates]
            plt.suptitle(title)
            plt.xlabel(self.date_col)
            plt.ylabel(self.dep_col)
            if download:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" + str(self.target_id_cols) + '.png')
                plt.close()
            else:
                plt.show()

        if (download == False) & (engine == 'plotly'):
            fig = __plot_specfic_ts_plotly(df,metric,end_date)
            iplot(fig)
        if (download) & (engine == 'plotly'):
            print("Can't download plotly plots")
        if engine == 'matplotlib':
            __plot_specfic_ts_matplotlib(df, download, folder_name,metric,end_date)
            
            
    def describe(self):
        """ Summarizes the dataset.

        """
        self.summary = pd.DataFrame( columns = ['key' , 'value' ]) 
        
        self.summary.loc[self.summary.shape[0]] = ['Actual data start date' , str(self.result[self.date_col].min()) ]
        self.summary.loc[self.summary.shape[0]] = ['Actual data end date', str(self.result[self.date_col].max())]
        self.summary.loc[self.summary.shape[0]] = ['Periodicity', self.result.loc[1, self.date_col] - self.result.loc[0, self.date_col]]
        self.summary.loc[self.summary.shape[0]] = ['Train start date', str(self.actual[self.date_col].min())]
        self.summary.loc[self.summary.shape[0]] = ['Train end date', str((self.prediction[self.date_col].min() - (self.result[self.date_col][1] - self.result[self.date_col][0])))]
        self.summary.loc[self.summary.shape[0]] = ['Test start date',str(self.prediction[self.date_col].min())]
        self.summary.loc[self.summary.shape[0]] = ['Test end date', str(self.prediction[self.date_col].max())]       
        self.summary.loc[self.summary.shape[0]] = ['No. of Time Series', self.result[self.target_id_cols].drop_duplicates().shape[0]]
        self.summary.loc[self.summary.shape[0]] = ['No. of train data',self.result[ self.result[self.date_col] < self.prediction[self.date_col].min()].shape[0]]
        self.summary.loc[self.summary.shape[0]] = ['No. of test data', self.prediction.shape[0]]
    
    def __subset_data(self,df,column,val):
        """ Subset data based on one or more column values.

        :param df: dataframe,
                   dataset.
        :param column: list of str,
                       list of column names.
        :param val: list,
                    values corresponding to each column.
        :return:
        """
        if not isinstance(val,list):
            val = [val]
        for k,v in dict(zip(column,val)).items():
             df = df[df[k]==v]
        return df
    
    def print_summary(self):
        """ print summary.
        """
        print(tabulate(self.summary, tablefmt='fancy_grid',showindex="never"))
    
    def plot_hist_error(self,engine = 'matplotlib', download = 'False' , folder_name = 'error_plot', metric = None , test_end_dates = None):
        """ Plot histogram of error values.

        :param engine: str,
                       'plotly' or 'matplotlib
        :param download: bool,
                         True if the plot has to be downloaded else False
        :param folder_name: str,
                            name of the folder where plot has to be saved.
        :param metric: str,
                       metric name.
        :param test_end_dates: list of timestamp,
                         test end dates.
        """
        if not isinstance(metric,list):
            metric  = [metric]
        if metric is None :
            metric = self.metric
        if test_end_dates is None : 
            test_end_dates = self.test_end_dates
        def __plot_hist_matplotlib(download , folder_name,metric,end_date):
            """ Plots using matplotlib.
            """
            save_dir = self.dload +  "/" + folder_name
            for i in metric :
                plt.figure()
                binwidth = 1
                df = self.metric_values[self.metric_values['test_end_date'] <= end_date][i].dropna()
                df = df.replace([np.inf, -np.inf], np.nan).dropna()
            
                plt.hist(df, bins=10, rwidth=0.5)
                plt.ylabel('Frequency')
                plt.suptitle(i + '\n' + 'Test end date : ' + str(end_date))
                plt.legend()
                if download :
                    if os.path.exists(save_dir):
                        pass
                    else:
                        os.mkdir(save_dir)
                    plt.savefig(save_dir + "/" + i +'-'+ str(end_date) + '.png')
                    plt.close()
                else :
                    plt.show()

        def __plot_hist_plotly(metric,end_date):
            """ Plots using plotly.
            """
            for i in metric:
                series = self.metric_values[self.metric_values['test_end_date'] <= end_date][i]
                trace = Histogram(
                    x=series.values,
                    marker=dict(color='#fd95b8'),
                    opacity=0.75)

                data = Data([trace])

                layout = Layout(
                    title=series.name + '<br>' + 'Test end date : ' +  str(end_date),
                    titlefont=dict(size=20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=series.name 
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title='Frequency',
                    ),
                    bargap=0.15
                )
                fig = Figure(data=data, layout=layout)
                iplot(fig)
        for end_date in test_end_dates:    
            if (download == False) & (engine == 'plotly'):
                __plot_hist_plotly(metric,end_date)
            if (download) & (engine == 'plotly'):
                print("Can't download plotly plots")
            if engine == 'matplotlib':
                __plot_hist_matplotlib(download, folder_name,metric,end_date)
    
    def plot_cumulative_error(self,engine = 'matplotlib', download = 'False' , folder_name = 'error_plot', metric = None , test_end_dates = None):
        """ Plot cumulative frequency plot of error values.

        :param engine: str,
                       'plotly' or 'matplotlib
        :param download: bool,
                         True if the plot has to be downloaded else False
        :param folder_name: str,
                            name of the folder where plot has to be saved.
        :param metric: str,
                       metric name.
        :param test_end_dates: list of timestamp,
                         test end dates.
        """
        if metric is None :
            metric = self.metric
        if test_end_dates is None : 
            test_end_dates = self.test_end_dates
        if not isinstance(metric,list):
            metric  = [metric]
        def __plot_hist_matplotlib(download , folder_name,metric,end_date):
            """ Plots using matplotlib.
            """
            save_dir = self.dload +"/" + folder_name
            for i in metric :
                plt.figure()
                binwidth = 1
                df = self.metric_values[self.metric_values['test_end_date'] <= end_date][i].dropna()
                df = df.replace([np.inf, -np.inf], np.nan).dropna()
            
                plt.hist(df,bins=10, rwidth=0.5,cumulative=True,weights = np.ones_like(df.index) / len(df.index) )
                plt.ylabel('Frequency')
                plt.suptitle(i + '\n' + 'Test end date : ' + str(end_date))
                plt.legend()
                if download :
                    if os.path.exists(save_dir):
                        pass
                    else:
                        os.mkdir(save_dir)
                    plt.savefig(save_dir + "/" + i +'-'+ str(end_date) + '.png')
                    plt.close()
                else :
                    plt.show()

        def __plot_hist_plotly(metric,end_date):
            """ Plots using plotly.
            """
            for i in metric:
                series = self.metric_values[self.metric_values['test_end_date'] <= end_date][i]
                trace = Histogram(
                    x = series.values,
                    histnorm='probability',
                    cumulative=dict(enabled=True),
                    marker=dict(color='#fd95b8'),
                    opacity=0.75)

                data = Data([trace])

                layout = Layout(
                    title=series.name + '<br>' + 'Test end date : ' + str(end_date),
                    titlefont=dict(size=20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=series.name 
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title='Frequency',
                    ),
                    bargap=0.15
                )
                fig = Figure(data=data, layout=layout)
                iplot(fig,filename = 'line-mode')
        for end_date in pd.to_datetime(test_end_dates):  
            if (download == False) & (engine == 'plotly'):
                __plot_hist_plotly(metric,end_date)
            if (download) & (engine == 'plotly'):
                print("Can't download plotly plots")
            if engine == 'matplotlib':
                __plot_hist_matplotlib(download, folder_name,metric,end_date)
