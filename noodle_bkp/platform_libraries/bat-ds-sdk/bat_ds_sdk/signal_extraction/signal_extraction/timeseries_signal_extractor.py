from Pynotebooks.core.time_series.time_series import ts
import fitter as fit_dist
import matplotlib.pyplot as plt
import scipy.stats as stats
import plotly
import plotly.graph_objs as go
import os
plotly.offline.init_notebook_mode()
from sklearn.cluster import KMeans
from fbprophet import Prophet
import pandas as pd
import math
import seaborn as sns

class timeseries_signal():
    '''
    Generates signals for a bunch of time series.
    '''

    def __init__(self, data, time_column, value_column, id_field):
        '''
        Initialize class
        :param data: (DataFrame) pandas DataFrame containing time series
        :param time_column: (str) column containing dates
        :param value_column: (str) column containing target value
        :param id_field: (list) columns over which we aggregate to get different time series
        '''

        self.id_field = id_field
        self.value_column = value_column
        self.time_column = time_column

        #Filter columns on which we perform analysis
        for col in id_field:
            data[col] = data[col].astype(object)

        self.data = data.set_index(id_field)
        df = data[[time_column, value_column] + id_field]

        #Generate time series object (noodle core)
        self.ts_data = ts(df, time_column, value_column)

        #Details of distribution fits (Fitter library object for each time series)
        self.dist = pd.DataFrame(columns=self.ts_data.data.columns)

        #Best fit model
        self.best_fit_dist = pd.DataFrame(columns=self.ts_data.data.columns)

        #Outlier details for each time series
        self.outlier = pd.DataFrame(columns=self.ts_data.data.columns)

        #Cluster of combination (id_fields) based on median and standard deviation
        self.cluster_data = None

        #Correlation between variables and target for each time series
        self.corr_var = None

        #Correlation between time series
        self.ts_corr = None

        #Category analysed
        self.category = None

        #Trend of selected category
        self.trend = None

        #Yearly seasonality of selected category
        self.yearly = None


    def __fit_distribution(self, observed_data, distributions=['norm', 'gennorm']):
        '''
        Fits distribution on each time series using Fitter class
        :param observed_data: (array of int/float) time series data to fit
        :param distributions: (list of string) list of scipy distribution names
        :return: (fitter object)
        '''
        f = fit_dist.Fitter(observed_data, verbose=False, distributions=distributions)
        f.fit()
        return f

    def fit_dist(self):
        '''
        Fit distribution and store result
        :return:
        '''
        for col in self.ts_data.data.columns:
            self.dist[col] = [self.__fit_distribution(self.ts_data.data[col].values)]
        self.set_best_fit_dist()

    def set_best_fit_dist(self):
        '''
        Store best fit result
        :return:
        '''
        for col in self.best_fit_dist.columns:
            pdf = self.dist[col][0]
            self.best_fit_dist[col] = [pdf.df_errors.iloc[0].name, pdf.fitted_param[pdf.df_errors.iloc[0].name],
                                       pdf.df_errors.iloc[0].sumsquare_error]

    def plot_good_fit(self, top=9, download=False, plot_filename='good_fit_plot.png'):
        '''
        Plot top good fits. Plots are saved under Anamoly detection folder.
        :param top: (int) number of distribution to display
        :param download: (bool) save plot as png
        :param plot_filename:
        :return:
        '''
        comb = self.best_fit_dist.loc[2].astype(float).nsmallest(top).index.labels
        levels = self.best_fit_dist.loc[2].astype(float).nsmallest(top).index.levels
        index_names = self.best_fit_dist.loc[2].astype(float).nsmallest(top).index.names
        self.__plot_signal(comb, levels, index_names, type='hist', n_figs=top, download=download, plot_filename=plot_filename)

    def plot_bad_fit(self, top=9, download=False, plot_filename='bad_fit_plot.png'):
        '''
        Plot top bad fits. Plots are saved under Anamoly detection folder.
        :param top: (int) number of distribution to display
        :param download: (bool) save plot as png
        :param plot_filename:
        :return:
        '''

        comb = self.best_fit_dist.loc[2].astype(float).nlargest(top).index.labels
        levels = self.best_fit_dist.loc[2].astype(float).nlargest(top).index.levels
        index_names = self.best_fit_dist.loc[2].astype(float).nlargest(top).index.names
        self.__plot_signal(comb, levels, index_names, type='hist', n_figs=top, download=download, plot_filename=plot_filename)

    def __plot_signal(self, comb, levels, index_names, n_figs, type='hist', download=False, plot_filename='plot.png'):
        ht = 3*math.ceil(n_figs/3)
        wd = 15
        final_comb = list()
        for i in range(len(comb[0])):
            new_comb = tuple()
            itr_comb = tuple()
            for j in range(len(comb)):
                new_comb += (comb[j][i],)

            for j in range(len(new_comb)):
                itr_comb += (levels[j][new_comb[j]],)

            final_comb.append(itr_comb)

        titles = []
        for itr_comb in final_comb:
            title = ''
            for itr_names in range(len(index_names)):
                title += index_names[itr_names] + ':' + str(itr_comb[itr_names]) + ' '

            titles.append(title)

        f, a = plt.subplots(math.ceil(n_figs / 3), 3)
        f.set_figheight(ht)
        f.set_figwidth(wd)
        a = a.ravel()
        count = 0
        for idx, ax in enumerate(a):
            dist_name = self.best_fit_dist[final_comb[count]][0]
            if type == 'hist':
                ax.hist(self.dist[final_comb[count]][0]._data, bins=self.dist[final_comb[count]][0].bins, normed=True)
                ax.plot(self.dist[final_comb[count]][0].x, self.dist[final_comb[count]][0].fitted_pdf[dist_name],
                        lw=2, label=dist_name)
            if type == 'box':
                ax.boxplot(self.ts_data.data[final_comb[count]])
            ax.set_title(titles[idx])
            ax.set_xlabel(self.ts_data.value_col_name)
            count += 1
            if count>=n_figs:
                break

        plt.tight_layout()
        if download:
            if os.path.exists("Anamoly detection"):
                pass
            else:
                os.mkdir("Anamoly detection")
            plt.savefig('Anamoly detection/' + plot_filename + '.png')

    def __get_comb_conf(self, comb, score=.9):
        val_range = ()
        if self.best_fit_dist[comb][0] == 'gennorm':
            dist = stats.gennorm
            val_range = dist.interval(score, *self.best_fit_dist[comb][1])
        return val_range

    def __count_outliers(self, comb, range_val):
        return (self.ts_data.data[(self.ts_data.data[comb] < range_val[0]) |
                                  (self.ts_data.data[comb] > range_val[1])].shape[0])

    def run_outlier_count(self, score=.9):
        '''
        Calculate outliers for each times series. Score is the confidence interval outside which outliers lie.
        :param score: (float) confidence interval (0-1)
        :return:
        '''
        for col in self.outlier.columns:
            self.outlier[col] = [score, self.__count_outliers(col, self.__get_comb_conf(col, score=score))]

    def plot_least_outlier(self, top=9, download=False, plot_filename='less_outlier_plot.png'):
        '''
        Plot boxplot of combination which has low outlier. PLot is saved under Anamly detection folder
        :param top: (int) number of distribution to display
        :param download: (bool) save plot as png
        :param plot_filename: name of file
        :return:
        '''

        comb = self.outlier.loc[1].astype(float).nsmallest(top).index.labels
        levels = self.outlier.loc[1].astype(float).nsmallest(top).index.levels
        index_names = self.outlier.loc[1].astype(float).nsmallest(top).index.names
        self.__plot_signal(comb, levels, index_names, type='box', n_figs=top, download=download, plot_filename=plot_filename)

    def plot_max_outlier(self, top=9, download=False, plot_filename='max_outlier_plot.png'):
        '''
        Plot boxplot of combination which has high outlier. PLot is saved under Anamly detection folder
        :param top: (int) number of distribution to display
        :param download: (bool) save plot as png
        :param plot_filename: name of file
        :return:
        '''
        comb = self.outlier.loc[1].astype(float).nlargest(top).index.labels
        levels = self.outlier.loc[1].astype(float).nlargest(top).index.levels
        index_names = self.outlier.loc[1].astype(float).nlargest(top).index.names
        self.__plot_signal(comb, levels, index_names, type='box', n_figs=top, download=download, plot_filename=plot_filename)

    def plot_correlation(self, download_plot=False):
        '''
        Correlation between time series. Helps understand relation between different time series
        :return:
        '''
        self.ts_corr = self.ts_data.data.corr()
        folder_name = "demand_risk_cluster"
        x = [''.join(str(item)) for item in self.ts_data.data.columns.values]
        y = [''.join(str(item)) for item in self.ts_data.data.columns.values]
        z = self.ts_corr.values
        if download_plot:
            if not os.path.exists(folder_name):
                os.mkdir(folder_name)
            fig, ax = plt.subplots(figsize=(len(x)/4, len(y) / 4))
            sns.heatmap(z, xticklabels=x, yticklabels=y, ax=ax)
            plt.savefig(folder_name + '/plot_correlation.png')
        else:
            trace = go.Heatmap(z=z,x=x,y=y)
            layout = go.Layout(title=str('-'.join(self.ts_data.data.columns.names) + ' correlation'))
            fig = go.Figure(data=[trace], layout=layout)
            plotly.offline.iplot(fig, filename='basic-heatmap')

    def create_cluster(self, n_cluster=3, *args):
        '''
        Create cluster of time series based on median and standard deviation of distribution they were fit to.
        :param n_cluster: (int) number of cluster to fit
        :param args: args for kmeans clustering sklearn
        :return:
        '''
        self.n_cluster = n_cluster
        self.cluster_data = self.best_fit_dist.transpose()[1].apply(pd.Series)[[1, 2]]
        self.cluster_data.columns = ['median', 'std']
        kmeans = KMeans(n_clusters=self.n_cluster)
        self.cluster_data['cluster_id'] = kmeans.fit_predict(self.cluster_data, *args)

    def plot_cluster(self, folder_name = 'Cluster_plots' ,file_name = 'plot' ,download_plot=True):
        '''
        Plot cluster once clusters are created (create_cluster func)
        :return:
        '''
        if download_plot:
            if not os.path.exists(folder_name):
                os.mkdir(folder_name)
            for cluster_id in range(self.n_cluster):
                x = self.cluster_data[self.cluster_data['cluster_id'] == cluster_id]['median']
                y = self.cluster_data[self.cluster_data['cluster_id'] == cluster_id]['std']
                # print(x)
                plt.scatter(x, y)
            plt.xlabel('Median - ' + str(self.value_column))
            plt.ylabel('Standard Deviation - ' + str(self.value_column))
            plt.title("Plot Clusters")
            plt.savefig(folder_name + '/'+file_name+'.png')
           
        else:
            plot_data = []
            for cluster_id in range(self.n_cluster):
                trace = go.Scatter(
                    x=self.cluster_data[self.cluster_data['cluster_id'] == cluster_id]['median'],
                    y=self.cluster_data[self.cluster_data['cluster_id'] == cluster_id]['std'],
                    text=[''.join(str(item)) for item in self.cluster_data.index.values],
                    name=str(cluster_id),
                    mode='markers'
                )
                plot_data.append(trace)
            layout = go.Layout(title=str('-'.join(self.cluster_data.index.names) + ' cluster'),
                               xaxis=dict(title=str('Median - ' + str(self.value_column))),
                               yaxis=dict(title=str('Standard Deviation - ' + str(self.value_column)))
                               )

            fig = go.Figure(data=plot_data, layout=layout)
            plotly.offline.iplot(fig, filename='scatter')

    def ts_components(self, comb, download_plot=True):
        '''
        Time series components for a given time series. Compnents are generated using Prophet
        :param comb: (tuple) time series index
        :return:
        '''
        folder_name = 'ts_component'
        df_ts = pd.DataFrame({'ds': self.ts_data.data.index, 'y': self.ts_data.data[comb]}).reset_index()
        m = Prophet()
        m.fit(df_ts)
        future = m.make_future_dataframe(periods=0)
        future.tail()
        forecast = m.predict(future)
        self.category = comb
        self.trend = forecast['trend']
        self.seasonality = forecast['yearly']
        if download_plot:
            if not os.path.exists(folder_name):
                os.mkdir(folder_name)
            plot_fig = m.plot(forecast)
            #plot_fig.suptitle(str(comb))
            #plot_fig.tight_layout()
            plot_fig.savefig(folder_name + '/forecast.png')
            m.plot_components(forecast).savefig(folder_name + '/components.png')


    def calc_var_corr(self):
        '''
        Correlation calculation between features and respective time series
        :return:
        '''
        corr_var = []
        index_val = []
        for idx, sub_df in self.data.groupby(self.id_field):
            index_val.append(idx)
            curr_corr = []
            for col in sub_df.columns:
                if (col not in [self.time_column, self.value_column]) and (
                    sub_df[col].dtype == 'float' or sub_df[col].dtype == 'int'):
                    curr_corr.append(sub_df[self.value_column].corr(sub_df[col]))
            corr_var.append(curr_corr)

        var = []
        for col in sub_df.columns:
            if (col not in [self.time_column, self.value_column] and 
               (sub_df[col].dtype == 'float' or sub_df[col].dtype == 'int')):
                var.append(col)
        self.corr_var = pd.DataFrame(data=corr_var, index=self.ts_data.data.columns, columns=var)

    def plot_var_correlation(self, download_plot=False, plot_folder='causal_signal', *args):
        '''
        Plot correlation between features and time series
        :param download_plot: bool
        :param plot_folder: str folder path to download to
        :param args:
        :return:
        '''
        folder_name = plot_folder
        if download_plot:
            x = [''.join(str(item)) for item in self.corr_var.columns.values]
            y = [''.join(str(item)) for item in self.corr_var.index.values]
            z = self.corr_var.values

            fig, ax = plt.subplots(figsize=(max(10,len(x)), len(y)/4))

            sns.heatmap(z, xticklabels=x, yticklabels=y, ax=ax, *args)
            plt.title("Correlation between features and time series")
            if not os.path.exists(folder_name):
                os.mkdir(folder_name)
            plt.savefig(folder_name + '/plot_var_correlation.png')
        else:
            trace = go.Heatmap(z=self.corr_var.values,
                               x=[''.join(str(item)) for item in self.corr_var.columns.values],
                               y=[''.join(str(item)) for item in self.corr_var.index.values])
            layout = go.Layout(title=str('-'.join(self.id_field) + ' correlation with features'))
            fig = go.Figure(data=[trace], layout=layout)
            plotly.offline.iplot(fig, filename='basic-heatmap')


