import pandas as pd
import numpy as np
import os
from Pynotebooks.data_analysis.dataquality.dataquality import dqa
import plotly
import plotly.figure_factory as FF
from plotly.offline import init_notebook_mode,iplot
from plotly.graph_objs import *
import matplotlib.pyplot as plt
import matplotlib
init_notebook_mode(connected=True)
from scipy.stats import skew,kurtosis
import warnings
warnings.filterwarnings('ignore')

class SummaryStats(object):
    """ Summary statistics of the time series data
     parameters:
     ----------
     data:pandas data frame

     Methods:
     ---------
     plot_ts: plots the histogram plot to know the distribution of target column
     summary_stats: lists various summary stats of overall data along with the distribution plot of specified time horizon
     ts_summary_stats: summary stats for individual time series along with the scatter plot for the summary stats specified
     plot_ts_distribution: distribution plot with respective to time series set

     Returns:
     --------
     class:`SummaryStats` object
    """
    
    def __init__(self,data,id_field,date_column,target_column,dload,folder_name):
        """ 
          :param data: pandas 'Dataframe'
          :param id_field:list of str,
                        id column which identifies the time series uniquely
          :param date_column:str,
                             datetime column
          :param target_column:str,
                             target column
          :param dload : str,
                       download path
        """
        self.data=data 
        self.id_col=id_field
        self._groups = None
        self.target_column=target_column
        self.dload=dload
        self.folder=folder_name
        self.t_period=dqa(self.data)
        
        
               
              
    @staticmethod    
    def bytesize(num, suffix='B'):
        for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
            if abs(num) < 1024.0:
                return "%3.1f %s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f %s%s" % (num, 'Y', suffix)
    
    def plot_ts(self,df,engine,download,title,file_name):
        """
        Plot time series
        :param df: pandas 'Dataframe'
        :param engine: str,
                       'plotly' or 'matplotlib
        :param download:bool,
                         True if the plot has to be downloaded else False
        :param folder_name: str,
                             name of the folder where plot has to be saved
        :param title: str,
                         the title for the plot
        :param file_name: str,
                             file name for for downloading the file
        """
                
        def __plot_specfic_ts_plotly(df,download):
            """
            Plots using plotly
            :param df: pandas 'Dataframe'
            :param download: bool,
                         True if the plot has to be downloaded else False
            """
        
            
            layout = Layout(
                title= title,
                xaxis=dict(title=self.target_column),
                yaxis=dict(title="Frequency")
                )

            all_traces = []

            all_traces.append(
                Histogram(
                   x=df[self.target_column],
                   marker=dict(color='#fd95b8'),
                   opacity=0.75
                   ))

            fig =Figure(data=all_traces, layout=layout)
            
            if download==True:
                print("can't download plotly")
            elif download==False:
                iplot(fig)
                
            
        def __plot_specfic_ts_matplotlib(df,download,file_name):
            """
            Plots using matplotlib
            :param df: pandas 'Dataframe'
            :param download:bool,
                         True if the plot has to be downloaded else False
            :param folder_name: str,
                             name of the folder where plot has to be saved
            :param file_name: str,
                             file name for for downloading the file
            """
           
            save_dir = self.dload+"/" +self.folder
            
            fig=plt.figure(figsize=(20, 12))
            fig.suptitle(title,fontsize=18)
            binwidth = 1
            plt.hist(df[self.target_column].dropna(), bins=20,color = "skyblue", ec="skyblue")
            plt.xlabel(self.target_column)
            plt.ylabel('Frequency')
            
            if download==True:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" +file_name+ '.png')
            elif download==False:
                plt.show() 
            
                  
        if engine == 'matplotlib':
            __plot_specfic_ts_matplotlib(df,download,file_name)
        elif engine=='plotly':
            __plot_specfic_ts_plotly(df,download)
            
     
    def summary_stats(self,date_column,id_field,target_column,engine,download=False,time_horizon=None):
        """ summary statistics for time series data
        :param date_column:str,
                            datetime column
        :param id_field:list of str,
                        id column which identifies the time series uniquely
        
        :param target_column:list of str,
                            target column
        :param engine:str,
                       'plotly' or 'matplotlib
        :param download:bool,
                         True if the plot has to be downloaded else False
        :param time_horizon:str,
                               time period for which the distribution plot will be plotted 
        
        """
        self.data[date_column]=pd.to_datetime(self.data[date_column])
        self.data[date_column]=self.data[date_column].sort_values()
        if time_horizon==None:
            title='Distribution plot for'+' '+str(self.target_column)
            file_name='Distribution plot for'+' '+str(self.target_column)
            self.plot_ts(self.data,engine, download,title,file_name)
        else:
            temp_data =self.data[(self.data[date_column] >=time_horizon['start_date']) & (self.data[date_column] <=time_horizon['end_date'])]
            title='Distribution plot for'+' '+str(self.target_column)+'\n '+'from:'+str(time_horizon['start_date'])+'  '+'to:'+str(time_horizon['end_date'])
            file_name='Distribution plot for'+' '+str(self.target_column)+' '+'of time horizon :'+str(time_horizon['start_date'])+'  '+'-'+str(time_horizon['end_date'])
            self.plot_ts(temp_data,engine, download,title,file_name)
        
        
        self.data[date_column]=pd.to_datetime(self.data[date_column])
        n_columns=len(self.data.columns)
        col_names=[i[0:] for i in self.data]
        n_rows=len(self.data)
        memsize=self.bytesize(self.data.memory_usage(index=True).sum())
        na=np.round(self.data.isnull().values.sum()*100/len(self.data),decimals=2)
        nan=np.round(np.isnan(self.data[target_column]).sum()*100/len(self.data),decimals=3)
        ts_array,period=self.t_period.check_periodicity(date_column,False)
        ts=len(self.data[id_field].drop_duplicates())
        Skew=np.round(skew(a=self.data[target_column],nan_policy='omit'),decimals=5)
        Kurtosis=np.round(kurtosis(a=self.data[target_column],nan_policy='omit'),decimals=5)
        
        self.result=pd.DataFrame({'stats':['number_of_columns','name_of_columns','number_of_rows','skewness','kutosis',
                                           'periodicticity','number_of_time_series_sets','missing_values_in_percentage',
                                           'percentage of nan','memory_size'],
                                  'details':[n_columns,col_names,n_rows,Skew,Kurtosis,period,ts,na,nan,memsize]})  
        
        self.result=self.result[['stats','details']]
        return self.result 
  
    def ts_summary_stats(self,id_field,target_column,date_column,stats_column_1,stats_column_2,download):
        """Summary Stats of the respective time series set and
              Scatter plot for the summary stats for time series set provided in column_1 and column_2
          :param id_field:str,
                        id column
          :param target_column:str,
                            target column
          :param date_column:str,
                            datetime column
          :param stats_column_1:str,
                                 summary statistics
          :param stats_column_2:str,
                                 summary statistics
          :param download:True if the plot has to be downloaded else False
        """
        self.data=self.data.sort_values(id_field)
        data_groups =self.data.groupby(id_field)
        ts_stats_output=pd.DataFrame()
        
        for i,group in data_groups:
            temp_data=group
            temp_data['mean']=temp_data[target_column].mean()
            temp_data['standard_deviation']=temp_data[target_column].std()
            temp_data['min']=temp_data[target_column].min()
            temp_data['max']=temp_data[target_column].max()
            temp_data['25%']=temp_data[target_column].quantile(0.25)
            temp_data['50%']=temp_data[target_column].quantile(0.50)
            temp_data['75%']=temp_data[target_column].quantile(0.75)
            temp_data['missing_Values']=np.round((temp_data[target_column].isnull().sum().sum())*100/len(self.data[target_column]),decimals=2)
            temp_data['skewness']=np.round(skew(a=temp_data[target_column],nan_policy='omit'),decimals=5)
            temp_data['kurtosis']=np.round(kurtosis(a=temp_data[target_column],nan_policy='omit'),decimals=5)
            ts_stats_output=ts_stats_output.append(temp_data)
            
        ts_stats_output=ts_stats_output.groupby(id_field).first()
        ts_stats_output=ts_stats_output[['mean','standard_deviation','skewness',
                                                   'kurtosis','missing_Values','min','max','25%','50%','75%']]
        #Scatter plot for summary stats

        save_dir = self.dload+"/" +self.folder
        file_name='Summary stats plot for'+' '+str(stats_column_1)+' '+'vs'+' '+str(stats_column_2)
        title='Summary stats plot for'+'  '+str(stats_column_1)+'  '+'vs'+'  '+str(stats_column_2)

        fig=plt.figure(figsize=(12,8))
        fig.suptitle(title,fontsize=18)
        plt.scatter(x=ts_stats_output[stats_column_1],y=ts_stats_output[stats_column_2])
        plt.xlabel(str(stats_column_1))
        plt.ylabel(str(stats_column_2))
        

        if download==True:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" +file_name+ '.png')
        elif download==False:
                plt.show()
            
        return ts_stats_output
     
    def plot_ts_distribution(self,ts_group,engine,download):
        """Distribution plot with respective to time series set
        :param ts_group: list,
                             unique id of time series
                       :ts_group:empty list,
                                           plots with respective to all time series set
                       :ts_group:list of unique ids,
                                                   will plot for all those unique id
                       :ts_group:None,
                                     will not plot                             
        :param engine:str,
                         'plotly' or 'matplotlib
        :param download:True if the plot has to be downloaded else False
        """
        
        self.groups = self.data.groupby(self.id_col)
        if ts_group==[]:
            ts_group = tuple(eval(str(ts_group)))
            for i,group in self.groups:
                df=group                
                ts_title='Distribution plot for'+' '+str(i)
                ts_id=list(zip(self.id_col,i))
                ts_file_name="Distribution plot for id_field"+' '+str(ts_id)
                self.plot_ts(df,engine,download,ts_title,ts_file_name)
        elif ts_group:
            for i in ts_group:
                ts_id=list(zip(self.id_col,i))
                if len(str(i[0]))>1:                 
                    i=tuple(eval(str(i)))
                else:
                    i=i[0]
                df=pd.DataFrame(self.groups.get_group(i))
                ts_title='Distribution plot for'+' '+str(i)
                ts_file_name="Distribution plot for id_field"+' '+str(ts_id)
                self.plot_ts(df,engine, download,ts_title,ts_file_name)                     
        else:
            print("can't plot")
            
       


            
        
        
        
            
        
            
        
        
        
        
        
    
        
        
            
        
        

        
