import pandas as pd
from plotly.graph_objs import *
import os
import plotly
import time
from shutil import move
import seaborn as sns
sns.set(color_codes=True)
from .plotly import Plotly_library
from .matplotlib import EDA_matplotlib
import collections

class DataAnalysis(object):
    """The main data analysis object

    The DataAnalysed object is formed from the pandas data frame
    and we perform various EDA by invoking methods. The results
    are stored in object variables after a method finishes.
    The architecture is similar to sklearn.

    :param data: The pandas data frame
    :return: :class:`DataAnalysis` object

    Usage::

        >>> data_file = "edw_cdr.csv"
        >>> data = pd.read_csv(data_file)
        >>> data_analysed = DataAnalysis(data)

    """

    def __init__(self, data):
        self.data = data
        self.numerical_columns = [col for col, s in data.iteritems() if self.get_vartype(s) == 'NUM']
        self.categorical_columns = [col for col, s in data.iteritems() if self.get_vartype(s) == 'CAT']
        self.plot_class = None
        self.dload = None
        self.plotly_class = Plotly_library(data)
        self.matplotlib_class = EDA_matplotlib(data)

    @staticmethod
    def _fmt_percent(v):
        return "{:2.1f}%".format(v * 100)

    @staticmethod
    def _fmt_float(v):
        return str(float('{:.5g}'.format(v))).rstrip('0').rstrip('.')

    def get_vartype(self, data):
        """Returns the variable type for the given data series

        :param data: pandas series
        :return: returns the variable type
        :rtype: basestring
        """
        distinct_count = data.nunique(dropna=False)
        leng = len(data)
        if distinct_count <= 1:
            return 'CONST'
        elif pd.api.types.is_numeric_dtype(data):
            return 'NUM'
        elif pd.api.types.is_datetime64_dtype(data):
            return 'DATE'
        elif distinct_count == leng:
            return 'UNIQUE'
        else:
            return 'CAT'
    
    def plotly_download(self,fig,folder_name):
        """
        Function to download the plotly plots. Downloads to a user specified folder in downloads folder

        :param fig:
        :param folder_name:
        :return:
        """
        save_dir = self.dload+"/"+folder_name
        print(save_dir)
        if os.path.exists(save_dir):
            pass
        else:
            os.mkdir(self.dload+'/'+folder_name)
        for j in fig:
            title = j['layout']['title']
            plotly.offline.iplot(j,image = "png",filename=title)
            time.sleep(2)
            print(move('{}/{}.png'.format(str(os.path.expanduser('~/Downloads')), title),'{}/{}.png'.format(save_dir,title)))

    def plot_histogram(self, folder_name, column_name, engine = 'plotly', download = True):
        """
        Plots and downloads histograms based on user defined column names, folder name and choice of engine

        :param folder_name:
        :param column_name:
        :param engine:
        :param download:
        :return:
        """
        if download & (engine == 'plotly'):
            fig = self.plotly_class.plot_histogram_plotly(column_name)

            self.plotly_download(fig,folder_name)
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_histogram_plotly(column_name)
            for j in fig:
                plotly.offline.iplot(j) 
        elif engine == 'matplotlib':
            self.matplotlib_class.plot_histogram_matplotlib(column_name)            
            
    def plot_frequency(self, folder_name, column_name, engine = 'plotly', download = True):
        """
        Plots and downloads frequency plots based on user defined column names, folder name and choice of engine

        :param folder_name:
        :param column_name:
        :param engine:
        :param download:
        :return:
        """
        if download & (engine == 'plotly'):
            fig = self.plotly_class.plot_frequency_plotly(column_name)
            self.plotly_download(fig,folder_name)
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_frequency_plotly(column_name)
            for j in fig:
                plotly.offline.iplot(j) 
        elif engine == 'matplotlib':
            self.matplotlib_class.matplotlib_plot_frequency(column_name)            
                    
    def plot_distribution(self, folder_name, column_name, engine = 'plotly', download = True):
        """
        Plots and downloads distribution plots based on user defined column names, folder name and choice of engine

        :param folder_name:
        :param column_name:
        :param engine:
        :param download:
        :return:
        """
        if download & (engine == 'plotly'):
            fig = self.plotly_class.plot_distribution_plotly(column_name)
            plotly.offline.iplot(fig)
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_distribution_plotly(column_name)
            plotly.offline.iplot(fig) 
        elif engine == 'matplotlib':
            self.matplotlib_class.matplotlib_plot_distribution(column_name) 
        
               
    def plot_qq(self, folder_name, column_name, engine = 'plotly',download = True):
        """
        Plots and downloads QQ plots based on user defined column names, folder name and choice of engine

        :param folder_name:
        :param column_name:
        :param engine:
        :param download:
        :return:

        ..note:: Plotly and matplotlib generates the same set of plots
        """
        self.matplotlib_class.matplotlib_plot_qq(column_name)
    
   
    def plot_heatmap(self, folder_name, cat_col1,cat_col2, engine = 'plotly', download = True):
        """
        Plots and downloads heatmap plots based on user defined column names, folder name and choice of engine

        :param folder_name:
        :param cat_col1:
        :param cat_col2:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_heatmap(cat_col1,cat_col2)
            self.plotly_download(fig,folder_name)
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_heatmap(cat_col1,cat_col2)
            for j in fig:
                plotly.offline.iplot(j) 
        elif engine == 'matplotlib':
            plt = self.matplotlib_class.matplotlib_heatmap(cat_col1,cat_col2)
   
    def plot_correlation(self, folder_name, engine = 'plotly', download = True):
        """
        Plots and downloads correlation plots by choice of engine for all columns in given folder name

        :param folder_name:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plot_correlation()
            save_dir = self.dload+"/"+folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(self.dload+'/'+folder_name)
            plotly.offline.iplot(fig,image = "png",filename="Correlation")
            time.sleep(1)
            print(move('{}/{}.png'.format(str(os.path.expanduser('~/Downloads')), "Correlation"),'{}/{}.png'.format(save_dir,"Correlation")))
    
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_correlation()
            plotly.offline.iplot(fig) 
        elif engine == 'matplotlib':
            self.matplotlib_class.matplotlib_correlation()
 
            
    def plot_box(self, folder_name, engine = 'plotly', download = True):
        """
        Plots and downloads box plots by choice of engine for all columns in given folder name

        :param folder_name:
        :param engine:
        :param download:
        :return:

        ..note:: Plotly box plot cannot be downloaded
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plot_box()
            plotly.offline.iplot(fig) 
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_box()
            plotly.offline.iplot(fig) 
        elif engine == 'matplotlib':
            self.matplotlib_class.matplotlib_box_plot()
 
    def plot_all_histogram(self, folder_name,engine = 'plotly',download  = True):
        """
        Plots and downloads all histograms for all the columns based on choice of engine in the user defined folder

        :param folder_name:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_all_histogram_plot()
            plotly.offline.iplot(fig,image = "png",filename="All_histogram")
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_all_histogram_plot()
            plotly.offline.iplot(fig) 
        elif engine == 'matplotlib':
            self.matplotlib_class.plot_all_histogram_matplotlib()
            
    def plot_all_frequency(self, folder_name, engine = 'plotly',download  = True):
        """
        Plots and downloads all frequency plots for all the columns based on choice of engine in the user defined folder

        :param folder_name:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_all_frequency_plot()
            plotly.offline.iplot(fig,image = "png",filename="all_frequency")
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_all_frequency_plot()
            plotly.offline.iplot(fig) 
        elif engine == 'matplotlib':
            self.matplotlib_class.plot_all_frequency_matplotlib()
            
            
    def plot_scatterplot(self, folder_name, num_col1, num_col2, engine = 'plotly', download = True):
        """
        Plots and downloads scatter plots based on choice of engine in the user defined folder from 2 lists of numerical columns

        :param folder_name:
        :param num_col1:
        :param num_col2:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_scatter(num_col1,num_col2)
            self.plotly_download(fig,folder_name)
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_scatter(num_col1,num_col2)
            for j in fig:
                plotly.offline.iplot(j) 
        elif engine == 'matplotlib':
            self.matplotlib_class.scatter_plot_matplotlib(num_col1,num_col2)
   
    def plot_stacked_histogram(self, folder_name, num_col, cat_col, engine = 'plotly', download = True):
        """
        Plots and downloads stacked histogram plots based on choice of engine in the user defined folder from a list of numerical and another list of categorical columns

        :param folder_name:
        :param num_col:
        :param cat_col:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plot_stacked_histogram(num_col,cat_col)
            self.plotly_download(fig,folder_name)
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_stacked_histogram(num_col,cat_col)
            for j in fig:
                plotly.offline.iplot(j) 
        elif engine == 'matplotlib':
            self.matplotlib_class.stacked_histogram(num_col,cat_col)
   
    def plot_ts(self, folder_name, time_column, target_column, engine = 'plotly', download = True):
        """
        Plots and downloads the time series plots based on a target and time column.. The function also provides a choice of engine

        :param folder_name:
        :param time_column:
        :param target_column:
        :param engine:
        :param download:
        :return:
        """
        self.data = self.data.sort_values(time_column).reset_index(drop=True)
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_ts(time_column,target_column)
            save_dir = self.dload+"/"+folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(self.dload+'/'+folder_name)
            title ='TimeSeries'
            plotly.offline.iplot(fig,image = "png",filename=title)
            time.sleep(2)
            print(move('{}/{}.png'.format(str(os.path.expanduser('~/Downloads')), title),'{}/{}.png'.format(save_dir,title)))
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plotly_ts(time_column,target_column)
            plotly.offline.iplot(fig)
        elif engine == 'matplotlib':
            plt = self.matplotlib_class.matplotlib_ts(time_column,target_column)
            
            
    def plot_autocorrelation(self, folder_name, num_col,engine = 'plotly', download = True):
        """
        Plots and downloads autocorrelation plots based on a numerical column

        :param folder_name:
        :param num_col:
        :param engine:
        :param download:
        :return:
        """
        self.matplotlib_class.matplotlib_autocorrelation(num_col)
            
    def plot_lag(self, folder_name, num_col, engine = 'plotly', download = True):
        """
        Plots and downloads lag plots based on a numerical column

        :param folder_name:
        :param num_col:
        :param engine:
        :param download:
        :return:
        """
        
        self.matplotlib_class.matplotlib_lag(num_col)
                    
    def plot_pcaf(self, folder_name, num_col,lags = 50 ,engine = 'plotly', download = True):
        """
        Plots and downloads partial autocorrelation plots based on a numerical column and lags

        :param folder_name:
        :param num_col:
        :param lags:
        :param engine:
        :param download:
        :return:
        """
        self.matplotlib_class.matplotlib_pacf(num_col,lags = lags )
    
    def stl_decomposition(self,folder_name,time_column, target_column, freq = 7,engine = 'plotly',download = True):
        """
        Plots and downloads stl decomposition plots based on a time and target column and frequency

        :param folder_name:
        :param time_column:
        :param target_column:
        :param freq:
        :param engine:
        :param download:
        :return:
        """
        if (download) & (engine == 'plotly'):
            fig = self.plotly_class.plot_decomposition(time_column, target_column, freq = freq)
            save_dir = self.dload+"/"+folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(self.dload+'/'+folder_name)
            title ='STLDecomposition'
            plotly.offline.iplot(fig,image = "png",filename=title)
            time.sleep(2)
            print(move('{}/{}.png'.format(str(os.path.expanduser('~/Downloads')), title),'{}/{}.png'.format(save_dir,title)))
        elif (download == False) & (engine == 'plotly'):
            fig = self.plotly_class.plot_decomposition(time_column, target_column, freq = freq)
            plotly.offline.iplot(fig) 
        elif engine == 'matplotlib':
            self.matplotlib_class.stl_matplotlib_decomposition(time_column,target_column)
                
    def select_run(self, input_dict, base_download_folder ,folder_name, engine, download):
        self.dload = base_download_folder
        self.matplotlib_class.set_base_details(base_download_folder,folder_name,download)
        input_dict = collections.OrderedDict(input_dict)
        for i in input_dict.keys():
            method = getattr(self, i)
            method(folder_name, **input_dict[i], download=download, engine=engine)
            
            
    

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
