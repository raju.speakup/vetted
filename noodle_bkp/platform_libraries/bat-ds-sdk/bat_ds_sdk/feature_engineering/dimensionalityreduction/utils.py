from torch.utils.data import DataLoader, TensorDataset
import math
import torch
import numpy as np

def create_train_dataset(X_train, vrae, batch_given=False, shift_size=None, sequence_length=None):
    if not batch_given:
        if not sequence_length:
            sequence_length = vrae.sequence_length
        num_of_sequences = math.floor((X_train.shape[0] - sequence_length) / shift_size) + 1
        start_indices = [shift_size * each_sequence for each_sequence in range(num_of_sequences)]
        end_indices = [sequence_length + (shift_size * each_sequence) for each_sequence in range(num_of_sequences)]

        complete_batches = math.floor(num_of_sequences / vrae.batch_size)
        X_train_mod = np.zeros(((complete_batches + 1) * vrae.batch_size, sequence_length, X_train.shape[1]))

        for i in start_indices[((2 * len(start_indices)) - ((complete_batches + 1) * vrae.batch_size)):]:
            start_indices.append(i)
        for i in end_indices[((2 * len(end_indices)) - ((complete_batches + 1) * vrae.batch_size)):]:
            end_indices.append(i)

        for k, (i, j) in enumerate(zip(start_indices, end_indices)):
            X_train_mod[k, :, :] = X_train[i:j, :]

        train_dataset = TensorDataset(torch.from_numpy(X_train_mod))
        return train_dataset
    else:
        train_dataset = TensorDataset(torch.from_numpy(X_train))
        return train_dataset
    
def reduce(X_test, ae, batch_given=False, shift_size=None, sequence_length=None):
    if not batch_given:
        if not sequence_length:  # if sequence_length is not given, fetch it from the model.
            sequence_length = ae.sequence_length
        num_of_sequences = math.floor((X_test.shape[0] - sequence_length) / shift_size) + 1
        start_indices = [shift_size * each_sequence for each_sequence in range(num_of_sequences)]
        end_indices = [sequence_length + (shift_size * each_sequence) for each_sequence in range(num_of_sequences)]

        complete_batches = math.floor(num_of_sequences / ae.batch_size)
        X_test_mod = np.zeros(((complete_batches + 1) * ae.batch_size, sequence_length, X_test.shape[1]))

        for k, (i, j) in enumerate(zip(start_indices, end_indices)):
            X_test_mod[k, :, :] = X_test[i:j, :]
        
        test_dataset = TensorDataset(torch.from_numpy(X_test_mod))
        x_reduced = ae.transform(test_dataset).swapaxes(0, 1)
        x_reduced = x_reduced[:num_of_sequences, :, :] 
        X_test_mod = X_test_mod[:num_of_sequences, :, :]
        x_reduced = x_reduced.reshape(-1, x_reduced.shape[-1])

        return x_reduced
    else:
        test_dataset = TensorDataset(torch.from_numpy(X_test))
        x_reduced = ae.transform(test_dataset).swapaxes(0, 1)

        return x_reduced    