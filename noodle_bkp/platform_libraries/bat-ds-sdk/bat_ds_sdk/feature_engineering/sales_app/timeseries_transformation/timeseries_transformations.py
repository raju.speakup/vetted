import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from sklearn import preprocessing
from statsmodels.tsa.stattools import adfuller
import os

class timeseries_transformations(object):

        def __init__(self, data, id_cols, time_col, dfolder, transformation_function):
            self.data = data
            self.id_cols = id_cols
            self.time_col = time_col
            self.standscaler = None
            self.dfolder = dfolder
            self.transform = transformation_function
            self.out_df = pd.DataFrame()

        def log_transform(self, input_col, output_col):
            """
            This function transforms the input numeric column to log values
            :param input_col: input column name
            :param output_col: output column name

            """
            self.data[output_col] = self.data[input_col].apply(np.log)
            return (self.data)

        def exp_transform(self, input_col, output_col):
            """
            This function calculate exponential of input log values
            :param input_col: input column name
            :param output_col: output column name

            """
            self.data[output_col] = self.data[input_col].apply(np.exp)
            return (self.data)

        def standardscalar_transf(self, input_col, output_col, pickle_filename, usepickle):
            """
            Standardizes multiple groups of data
            :param input_col: input column name
            :param output_col: output column name
            :param pickle_filename: stores scaler objects
            :param usepickle: True/False; whether to save scaler objects in pickle file
            """
            result = pd.DataFrame()
            self.standscaler = pd.DataFrame(columns=(self.id_cols)+['Scaler_object'])
            for id_cols, group in self.data.groupby(self.id_cols):
                id_series = pd.Series(id_cols)
                values = group[input_col].astype(float)
                # Standard scaling
                values = values.reshape((len(values), 1))
                scaler = preprocessing.StandardScaler()
                # appends idcols + scaler object
                self.standscaler.loc[self.standscaler.shape[0]] = list(id_series)+[scaler]
                normalized = scaler.fit_transform(values)
                group[output_col] = normalized
                result = result.append(group)
            # if usepickle is true, save the scaler object dataframe in pickle file
            if(usepickle == True):                
                if os.path.exists(self.dfolder +  "/"):
                    pass
                else:
                    os.mkdir(self.dfolder +  "/")
                self.standscaler.to_pickle(self.dfolder +  "/" + pickle_filename)
            return result.reset_index(drop=True)

        def inveresescalar_tranf(self,input_col, output_col, pickle_filename, usepickle):
            """
            Rescale multiple groups of data
            :param input_col: input column name
            :param output_col: output column name
            :param pickle_filename: stores scaler objects
            :param usepickle: True/False; whether to save scaler objects in pickle file or not
            """
            inverseresult = pd.DataFrame()
            if usepickle == True:
                self.standscaler = pd.read_pickle(self.dfolder +  "/" + pickle_filename)
            for id_cols, group in self.data.groupby(self.id_cols):
                invvalues = group[input_col]
                invvalues = invvalues.reshape(len(invvalues), 1)
                # for each group idcols, need to pick corresponding scaler object.
                scaler = self.standscaler.loc[[set(pd.Series(id_cols)).issubset(x) for x in list(self.standscaler.values)].index(True), 'Scaler_object']
                denormalized = scaler.inverse_transform(invvalues)
                group[output_col] = denormalized
                inverseresult = inverseresult.append(group)
            return inverseresult.reset_index(drop=True)

        def minmaxscalar_transf(self,input_col, output_col, pickle_filename, usepickle):
            """
            Standardizes multiple groups of data
            :param input_col: input column name
            :param output_col: output column name
            :param pickle_filename: stores scaler objects
            :param usepickle: True/False; whether to save scaler objects in pickle file
            """
            result = pd.DataFrame()
            self.standscaler = pd.DataFrame(columns=(self.id_cols)+['Scaler_object'])
            for id_cols, group in self.data.groupby(self.id_cols):
                id_series = pd.Series(id_cols)
                values = group[input_col].astype(float)
                # Standard scaling
                values = values.reshape((len(values), 1))
                scaler = preprocessing.MinMaxScaler()
                # appends idcols + scaler object
                self.standscaler.loc[self.standscaler.shape[0]] = list(id_series)+[scaler]
                normalized = scaler.fit_transform(values)
                group[output_col] = normalized
                result = result.append(group)
            # if usepickle is true, save the scaler object dataframe in pickle file
            if usepickle == True:
                if os.path.exists(self.dfolder +  "/"):
                    pass
                else:
                    os.mkdir(self.dfolder +  "/")
                self.standscaler.to_pickle(self.dfolder +  "/" + pickle_filename)
            return result.reset_index(drop=True)

        def difference_tranf(self, input_col, output_col):
            """
            First difference of non-stationary time series, adfuller test is done to find non-stationary time series
            :param input_col: input column name
            :param output_col: output column name
            """
            result = pd.DataFrame()
            interval = 1
            self.standscaler = pd.DataFrame(columns= (self.id_cols) + [input_col]) #, output_col

            for id_cols, group in self.data.groupby(self.id_cols):
                group = group.sort_values([self.time_col])
                values = group[input_col]
                adfullerresult = adfuller(values)
                # checking p-value
                if adfullerresult[1] > 0.05:
                    group[output_col] = (values - values.shift(interval))
                else:
                    group[output_col] = values
                result = result.append(group)
            if os.path.exists(self.dfolder +  "/"):
                pass
            else:
                os.mkdir(self.dfolder +  "/")
            return result.reset_index(drop=True)

        def inverse_difference(self, input_col, differencing_file, diffinput_column, diffoutput_column ):
            """
            inverse difference of time series
            :param input_col: input column name
            :param output_col: output column name
            :param differencing_file: reads this file to rescale differenced time series
            :param diffinput_column: input column name of helper file
            :param diffoutput_column: outputcolumn name of helper file
            """
            result = pd.DataFrame()
            self.diffscaler = pd.read_csv(self.dfolder +  "/" + differencing_file, na_values='NaN', parse_dates = [self.time_col], dtype={i: object for i in self.id_cols})
            data_group = self.data.groupby(self.id_cols)
            for id_cols, group in self.diffscaler.groupby(self.id_cols):
                group = group.sort_values([self.time_col])
                temp_df = data_group.get_group(id_cols)
                if((group[diffinput_column] - group[diffoutput_column]).sum() != 0):
                    diff = group.iloc[-1][diffinput_column]
                    re_diff = []
                    for x in temp_df[input_col]:
                        re_diff.append(diff+x)
                        diff = x+diff
                    temp_df[input_col] = re_diff
                result = result.append(temp_df)
            return result.reset_index(drop=True)
            
        def run_transform(self, args):
            """
            Utility function to run the Transformation.
            :return:
            """
            method = getattr(self, self.transform)
            self.out_df = method(**args)


