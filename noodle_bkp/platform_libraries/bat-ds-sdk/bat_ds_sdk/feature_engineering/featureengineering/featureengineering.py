import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from numpy import array
from numpy import abs, sum, linspace
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_selection import VarianceThreshold
from statsmodels.tsa.tsatools import lagmat, lagmat2ds
from scipy import stats
import matplotlib.pyplot as plt
import pywt
import scipy
from sklearn.cluster import KMeans
from statsmodels.tsa.seasonal import seasonal_decompose
from numpy.fft import rfft
import librosa
from scipy.fftpack import dct
from sklearn.preprocessing import OneHotEncoder
from sklearn.ensemble import RandomForestClassifier
import operator


class FeatureEngineering(object):
    """ The main FeatureEngineering object

    The FeatureEngineering is an python Class which provides methods
    for feature engineering .
    Feature engineering is the process of using domain knowledge of the data to
    create features that make machine learning algorithms work.
    It is formed from the pandas dataframe.

    :param data: pandas dataFrame
    :return: :class:`FeatureEngineering` object

    Usage::
            >>> data_file = 'datafile.csv'
            >>> df = pd.read_csv(data_file)
            >>> feature_engineering = FeatureEngineering(df)


    """
    def __init__(self, data):
        self.data = data

    def log_transform(self,column,name):
        """ This function transforms the input numeric column to log values

        :param column: list of column name(s) to be transformed.
        :param   name: str,
                       action performed, e.g. 'log' .

        """
        self.data[[s + "_" + name for s in column]]=self.data[column].apply(np.log)


    def square_transform(self,column,name):
        """ This function transforms the input numeric column to square values

        :param column: list of column name(s) to be transformed.
        :param   name: str,
                       action performed, e.g. 'sq' .

        """
        self.data[[s + "_" + name for s in column]]=self.data[column].apply(lambda x: x**2)

    def sqroot_transform(self,column,name):
        """ This function transforms the input numeric column to square root values

        :param column: list of column name(s) to be transformed.
        :param   name: str,
                       action performed, e.g. 'sqroot' .

        """

        self.data[[s + "_" + name for s in column]]=self.data[column].apply(np.sqrt)

    def sigmoid_transform(self,column,name):
        """  This function does sigmoid transform the input numeric column

        :param column: list of column name(s) to be transformed.
        :param   name: str,
                       action performed, e.g. 'sigmoid' .

        """
        self.data[[s + "_" + name for s in column]]=1 / (1 + np.exp(-self.data[column]))

    def tanh_transform(self,column,name):
        """   This function does tanh transform the input numeric column

        :param column: list of column name(s) to be transformed.
        :param   name: str,
                       action performed, e.g. 'tanh' .

        """

        self.data[[s + "_" + name for s in column]]=self.data[column].apply(np.tanh)


    def mean_normalization(self,column,name):
        """  Normalization of data about the mean
        ynorm=(y-mean(y))/(y_max-y_min)

        :param column: list of column name(s) to be normalized.
        :param   name: str,
                       action performed, e.g. 'mean_norm' .

        """
        self.data[[s + "_" + name for s in column]] = self.data[column].apply(lambda x: (x - x.mean()) / (max(x) - min(x)))


    def minmax_normalization(self, column,name):
        """    Normalization of data about the min max
        ynorm=(y-min(y))/(y_max-y_min)

        :param column: list of column name(s) to be transformed.
        :param   name: str,
                       action performed, e.g. 'min_max_norm' .

        """
        self.data[[s + "_"+ name for s in column]] = self.data[column].apply(lambda x: (x - min(x)) / (max(x) - min(x)))


    def one_hot_encoding(self,column):
        """ Converts categorical columns to equivalent one hot encoding

        One hot encoding is a process by which categorical variables are converted into
        a form that could be provided to ML algorithms to do a better job in prediction.

        :param column: str,
                       column name.

        """
        values = array(self.data[column])
        label_encoder = LabelEncoder()
        integer_encoded = label_encoder.fit_transform(values)
        onehot_encoder = OneHotEncoder(sparse=False)
        integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
        onehot_encoded = onehot_encoder.fit_transform(integer_encoded)
        data = pd.DataFrame(data=onehot_encoded[0:, 0:])
        self.data = pd.concat([self.data, data], axis=1)


    def concat(self,col1,col2,name):
        """ Concatenates two columns.

        :param col1: str,
                     name of column 1.
        :param col2: str,
                     name of column 2.
        :param name: str,
                     name of the new column.
        :return: pandas dataFrame
        """
        return pd.DataFrame(data=(pd.concat([self.data[col1], self.data[col2]])), columns=[name])

    def apply(self,column, function,name):
        """ Apply any function on columns

        :param   column: list of column name(s).
        :param function: function,
                         Function to apply to each column.
        :param     name: str,
                       action performed, e.g. 'function' .

        """
        self.data[[s + "_" + name for s in column]] = self.data[column].apply(function)


    def fourier_transform(self,column, dim , type):
        """
        Perform fourier transformation on column
        Fourier analysis is fundamentally a method for expressing a function as a sum of periodic components, and for recovering the function from those components. When both the function and its Fourier transform are replaced with discretized counterparts, it is called the discrete Fourier transform (DFT).

        :param column: list of column names to be transformed.
        :param    dim: int,
                       one or two or n dimension
        :param   type: str,
                       discrete or inverse

        """
        if type == "discrete":
                if dim == 1:
                    value = np.fft.fft(self.data[column])
                elif dim == 2:
                    value = np.fft.fft2(self.data[column])
                else:
                    value = np.fft.fftn(self.data[column])
        else:
                if dim == 1:
                    value = np.fft.ifft(self.data[column])
                elif dim == 2:
                    value = np.fft.ifft2(self.data[column])
                else:
                    value = np.fft.ifftn(np.fft.fftn(self.data[column]))
        self.data = pd.concat([self.data,pd.DataFrame(data = value)], axis=1)


    def shift(self,column,shift,trim,original):
        """  Shifting of data on a column

        :param   column: list of column names.
        :param    shift: int,
                         all lags from zero to maxlag are included
        :param     trim: str,
                         'forward' : trim invalid observations in front
                         'backward' : trim invalid initial observations
                         'both' : trim invalid observations on both sides
                         'none' , None : no trimming of observations
        :param original: str,
                         'ex' : drops the original array returning only the lagged values.
                         'in' : returns the original array and the lagged values as a single array.
                         'sep' : returns a tuple (original array, lagged values). The original
                         array is truncated to have the same number of rows as the returned lagmat.

        """
        y_lagged, y = lagmat(self.data[column], maxlag=shift, trim=trim, original=original)
        self.data = pd.concat([self.data, pd.DataFrame(data = y_lagged)], axis=1)


    def rolling_features(self,column,window):
        """

        :param column: list of column names.
        :param window: int,
                       Size of the moving window. This is the number of observations used for calculating the statistic.

        """
        self.data[[s + "_roll_mean" for s in column]]=pd.rolling_mean(self.data[column], window)
        self.data[[s + "_roll_sum" for s in column]]=pd.rolling_sum(self.data[column], window)
        self.data[[s + "_roll_median" for s in column]]=pd.rolling_median(self.data[column], window)
        self.data[[s + "_roll_var" for s in column]]=pd.rolling_var(self.data[column], window)


    def remove_features_low_variance(self,columns,threshold):
        """
        Remove features with low variance

        :param   columns: list of column names.
        :param threshold: float,
                          Features with a variance lower than this threshold will be removed.

        :return: pandas dataframe.
        """
        sel = VarianceThreshold(threshold=threshold)
        value = sel.fit_transform(self.data[columns])
        return pd.DataFrame(data=value)

    def groupby(self,columns):
        """  Groupby columns and return groupby object

        :param columns: list of column names.

        :return: GroupBy object
        """
        return self.data.groupby(columns)


    def boxcox(self, column):
        """ Box Cox transformation

        :param column: str,
                       column name.


        """
        transform = np.asarray(self.data[[column]].values)
        dft = stats.boxcox(transform)[0]
        self.data = pd.concat([self.data, pd.DataFrame(data=dft)], axis=1)



    def plot_boxcox(self, column, bins):
        """ Plotting Box Cox transformation

        :param column: str,
                       column name.
        :param   bins: int,
                       bins + 1 bin edges are calculated and returned.

        """
        print("Original histogram")
        plt.hist(self.data[column], bins=bins)
        plt.show()
        transform = np.asarray(self.data[[column]].values)
        dft = stats.boxcox(transform)[0]
        print("\nHistogram after box-cox transformation")
        plt.hist(dft, bins=bins)
        plt.show()


    def binning(self,column,bins,name):
        """ Binning of data

        :param column: str,
                       column name.
        :param   bins: int,
                       it defines the number of equal-width bins in the range of column.
        :param   name: str,
                       action performed.

        """

        self.data[name+'_'+column] = pd.cut(self.data[column], bins=bins)

    def kmeans(self,columns,clusters):
        """ K means clustering

        :param  columns: list of column names.
        :param clusters: int,
                         number of clusters.

        """

        km = KMeans(n_clusters=clusters, random_state=1)
        new = self.data[columns]._get_numeric_data().dropna(axis=1)
        km.fit(new)
        predict=km.predict(new)
        self.data = pd.concat([self.data, pd.DataFrame(data= (pd.Series(predict, index=self.data.index)))], axis=1)

    def calendar_var(self,column):
        """ Generate Calender variables

        :param column: str,
                       column name.

        """

        yr=pd.DatetimeIndex(self.data[column]).year
        woy=pd.DatetimeIndex(self.data[column]).week
        mt=pd.DatetimeIndex(self.data[column]).month
        qtr=pd.DatetimeIndex(self.data[column]).quarter
        dow=pd.DatetimeIndex(self.data[column]).dayofweek
        doy=pd.DatetimeIndex(self.data[column]).dayofyear
        wkdy=pd.DatetimeIndex(self.data[column]).weekday
        wk_name=pd.DatetimeIndex(self.data[column]).weekday_name
        self.data['year']=yr
        self.data['week']=woy
        self.data['month']=mt
        self.data['quarter']=qtr
        self.data['dayofweek']=dow
        self.data['dayofyear']=doy
        self.data['weeekday']=wkdy
        self.data['weekdayname']=wk_name


    def standardization(self,column,name):
        """ Standardize the input numeric column

        :param column: list of numeric column names.
        :param   name: str,
                       action performed.

        """

        self.data[[s + "_" + name for s in column]] = self.data[column].apply(lambda x:(x-x.mean())/x.std())


    def aggregate(self,column_gp,column_sum):
        """ Performs aggregation

        :param  column_gp: str,
                           Used to determine the groups for the groupby.
        :param column_sum: str,
                           column name.

        """

        agg=self.data.groupby(column_gp)[column_sum].sum()
        #self.data = pd.concat([self.data, pd.DataFrame(data = agg)], axis=1)
        return agg


    def csum(self,column,name):
        """ Performs cumulative sum.

        :param column: list of column names.
        :param   name: str,
                       action performed.

        """
        self.data[[s + "_" + name for s in column]] = np.cumsum(self.data[column],axis=0)


    def stats(self,column):
        """ Stats for numeric data

        :param column: list of column names.

        """
        self.data['gmean']=scipy.stats.gmean(self.data[column], axis=0, dtype=None)
        self.data['skewness']=scipy.stats.skew(self.data[column], axis=0, bias=True)
        self.data['kurtosis']=scipy.stats.kurtosis(self.data[column], axis=0, bias=True)
        self.data['mode']=scipy.stats.mode(self.data[column], axis=0)
        self.data['mean']=np.mean(self.data[column], axis=None)
        self.data['median']=np.median(self.data[column], axis=None)

    def ts_classify(self,columnx,columny):
        """ Time series classification based on adi,cv^2 values

        :param columnx: str,
                        column name.
        :param columny: str,
                        column name.

        """
        new_df=self.data.loc[(self.data[columny] !=0)]
        adi=np.diff(new_df[columnx]).sum()/(new_df[columnx].count()-1)
        CV2=(np.std(new_df[columny])/np.mean(new_df[columny]))**2
        ts = "none"
        if adi <1.32 and CV2<0.49:
            ts ='smooth'
        elif adi >=1.32 and CV2<0.49:
            ts ='intermittent'
        elif adi <1.32 and CV2>=0.49:
            ts ='erratic'
        elif adi >=1.32 and CV2>=0.49:
            ts ='lumpy'
        self.data['adi'] = adi
        self.data['CV2'] = CV2
        self.data['ts_class'] = ts


# stl decomposition

    def ts_stl(self,column_date,columny):
        """ STL decomposition of time series data

        :param column_date: str,
                            column name of type time.
        :param     columny: str,
                            column name that is dependent on column_date.

        """
        ds=self.data.loc[:,[column_date,columny]]
        ds[column_date] = pd.to_datetime(ds[column_date], errors='coerce')
        ds =ds.set_index(column_date).dropna() #set date as col index
        result = seasonal_decompose(ds,freq=1)
        self.data['trend']=np.asarray(result.trend)
        self.data['seasonal']=np.asarray(result.seasonal)
        self.data['residual']=np.asarray(result.resid)



    def ewma(self,columny,alph):
        """ Exponentially weighted moving average

        :param columny: str,
                        column name.
        :param    alph: float,
                        specifies smoothing factor.

        """

        self.data['EWMA'] = pd.ewma(self.data[columny],alpha=alph)


    def dwt(self,column):
        """  discrete wavelet transform.

        Wavelets are mathematical basis functions that are localized in both time and frequency.
        Wavelet transforms are time-frequency transforms employing wavelets.
        They are similar to Fourier transforms, the difference being that
        Fourier transforms are localized only in frequency instead of in
        time and frequency.

        :param column: str,
                       column name.

        :return: pandas dataframe

        """
        (cA, cD) = pywt.dwt(self.data[column], 'db1')
        new_df = pd.DataFrame({'cA':cA,'cD':cD}, columns = ['cA','cD'])
        return new_df

    def dct_out(self,columny,type):
        """  Generate discrete cosine transform

        A discrete cosine transform (DCT) expresses a finite sequence of data
        points in terms of a sum of cosine functions oscillating at different
        frequencies. DCTs are important to numerous applications in science
        and engineering, from lossy compression of audio (e.g. MP3) and
        images (e.g. JPEG)
        (where small high-frequency components can be discarded), to
        spectral methods for the numerical solution of partial differential
        equations

        :param columny: str,
                        column name.
        :param    type: int,
                        type of DCT (1/2/3).

        """
        df_out = self.data
        output = dct(np.array(df_out[columny]), type)
        self.data['DCT'] = output


    def spectral_features(self, column_date, columny):
        """ Find Spectral features

        :param column_date: str,
                            column name of type time.
        :param     columny: str,
                            column name that is dependent on column_date.

        :return: list of arrays
        """
        ds = self.data.loc[:, [column_date, columny]]
        ds = ds.set_index(column_date).dropna()  # set date as col index
        spectrum = abs(rfft(ds))
        # normalized spectrum
        normalized_spectrum = spectrum / sum(spectrum)  # like a probability mass function
        # normlized fre
        normalized_frequencies = linspace(0, 1, len(spectrum))
        # spectral centroid
        spectral_centroid = sum(normalized_frequencies * normalized_spectrum)
        # Band width
        bw = librosa.feature.spectral_bandwidth(y=np.array(ds[columny]))
        # rolloff
        rolloff = librosa.feature.spectral_rolloff(y=np.array(ds[columny]))
        # flatness
        flatness = librosa.feature.spectral_flatness(y=np.array(ds[columny]))
        # zero crossing rate
        zc = librosa.feature.zero_crossing_rate(np.array(ds[columny]))
        # stft
        stft = librosa.core.stft(np.array(ds[columny]))
        return stft, spectrum, normalized_spectrum, normalized_frequencies, spectral_centroid, bw, rolloff, flatness, zc

    def select_run(self, dict):
        """ This is an utility function to invoke multiple feature engineering methods.

        :param dict: dict,
                     it specifies a dictionary where method names are keys and the corresponding parameter list are its  values.
        """
        for i in dict.keys():
            method = getattr(self, i)
            if type(dict[i]) is tuple:
                method(*dict[i])
            else:
                method(dict[i])
        return None

    def feature_importance_values(self,features,label):
        """ This function uses RandomForestClassifier to perform feature importance.

        It returns a dictionary of the features and their value of importance along with a horizontal bar plot

        :param features: str,
                         column name.
        :param    label: str,
                         column name.

        :return: list
        """
        x = self.data[features]
        y = self.data[label]
        rnd_clf = RandomForestClassifier(n_estimators=500, n_jobs=-1, random_state=42)
        rnd_clf.fit(x,y)
        d = {}
        for name, importance in zip(x, rnd_clf.feature_importances_):
            d[name] = importance
        lists = sorted(d.items(), key=operator.itemgetter(1))
        x, y = zip(*lists)
        plt.figure(figsize=(7, 7))
        plt.barh(x, y)
        plt.show()
        return lists

    def new_columns(self,dict):
        """ This is an utility function gives the new columns.

        :param dict: it specifies a dictionary where list of (column name,transform function) are its  values.

        """
        for i in dict.values():
            new_cols = [a+"_"+ i[1] for a in i[0]]
            for new_col in new_cols:
                print(new_col)



