from abc import ABC, abstractclassmethod
import uuid
import os
from datetime import datetime
import pandas as pd
import pickle
#import git

class ExpTracker(ABC):
    """ An abstract class to implement experiment tracker .

    Experiment tracker is used to store all the experiment runs so that these runs
    can be analysed later.

    Values such as input parameters , plots , output metrics gets stored in the specified folder.

    """
    exp_id = None
    labels = {}
    input_params = {}
    output_metric = {}
    start_time = None
    end_time = None
    runtime = None
    def __init__(self):
        self.start_time = datetime.now()
    '''
    def get_git_commit_id(self):
        """ To access the current git hash.

        :return: str,
                 git commit id.
        """

        repo = git.Repo(search_parent_directories=True)
        return repo.head.object.hexsha
    '''
    @abstractclassmethod
    def close(self):
        pass

    @abstractclassmethod
    def init(self, exp_id=None):
        pass

    def get_experiment_id(self):
        """ To get the experiment id.

        :return: str,
                 exp id.
        """
        return self.exp_id

    def get_start_time(self):
        """ To get the start time of the experiment.

        :return: :class:`datetime` object
                 start time .
        """
        return self.start_time

    def get_label(self, key):
        """ To get the value of the corresponding key.

        :param   key: str,
                      label key.

        :return: str, None
                 label value if key exist else None.
        """
        if key in self.labels:
            return self.labels[key]
        else:
            None

    def get_all_labels(self):
        """ To get all labels.

        :return: dict,
                 pair of key , val of all labels.
        """
        return self.labels

    @abstractclassmethod
    def set_label(self, key, val):
        pass

    @abstractclassmethod
    def get_input_param(self, key, val=None):
        pass

    @abstractclassmethod
    def set_output(self, key, val):
        pass


from pathlib import Path
import json

class local_tracker(ExpTracker):
    """ The main Experiment tracker object .

    The local_tracker is an python Class which provides methods
    for experiment tracking .
    Experiment tracking helps to compare and analyze various output of an experiment for
    the given different input parameters.
    This class is derived from :class:`ExpTracker`

    :return: :class:`local_tracker` object

    Usage::
             Track experiments
             >>>from Pynotebooks.core.experiment_tracker.abstract_tracker import local_tracker
             >>>tracker = local_tracker(base_folder='/tmp/')

             Inside parameter cell
             >>>id_field = tracker.get_input_param('id_field', ['Product','DC'])
             >>>time_column = tracker.get_input_param('time_column', 'Date')

    """

    base_folder = None
    plot_folder = None
    exp_folder = None
    data_folder = None
    exp_file = 'experiments.csv'
    output_data = []
    flag = 0

    def __init__(self, base_folder='./', exp_file='exp_result.csv', plot_folder='plot/', data_folder='data/', exp_id=None):
        super().__init__()
        self.base_folder = base_folder
        """
        str,
        base folder name. 
        """
        self.plot_folder = plot_folder
        """
        str,
        folder name where the plot have to stored.
        """
        self.data_folder = data_folder
        """
        str,
        folder name where the plot have to stored.
        """
        self.init(exp_id=exp_id)
        self.exp_file = exp_file
        """
        str,
        file name where the runs get stored.
        """
        

    def init(self, exp_id=None):
        """ Initializing the experiment.

        :param exp_id: str,
                       experiment Id.
        """
        if exp_id is None:
            self.exp_id = str(uuid.uuid4())[0:6]
        else:
            self.exp_id = exp_id

        self.exp_folder = self.base_folder + self.exp_id + '/'
        if not os.path.exists(self.exp_folder):
            self.__create_folders()

    def __create_folders(self):
        """ A helper function to create folders if they don't exist.

        """
        os.makedirs(self.exp_folder)
        self.plot_folder = self.exp_folder + self.plot_folder
        os.makedirs(self.plot_folder)
        self.data_folder = self.exp_folder + self.data_folder
        os.makedirs(self.data_folder)

    def set_label(self, key='test', val='test'):
        """ Add labels for the experiment

        :param key: str,
                    label key.
        :param val: str,
                    label value.

        """
        self.labels[key] = val

    def get_label(self, key):
        """ To get the value of the corresponding key.

        :param   key: str,
                      label key.
        :return: str, None
                 label value if key exist else None.
        """
        if key in self.labels:
            return self.labels[key]
        else:
            return None

    def get_input_param(self, key, val):
        """ Set input params for tracking.

        :param key: str,
                    variable to store value against
        :param val: str or int or float or bool,
                    value of the variable

        :return: str or int or float or bool,
                 val is returned as well

        Usage::
                >>>variable_name = tracker.get_input_param('variable_name',value)
        """
        if self.flag != 1:
            if val is not None:
                self.input_params[key] = str(val)
                return val
            else:
                #TODO: Input will fetched from BAT UI
                return None
            
        else:
            raise Exception('Tracker is not closed, Please close the tracker using tracker.close() and restart')

    def set_output(self, key, val):
        """ Set output

        :param key: str,
                    name of the key or filename
        :param val: str,int,float or DataFrame
                    output value.

        Usage::
                >>>tracker.set_output('variable_name',weights)

        """
        if isinstance(val, pd.DataFrame):
            file_object = open(self.data_folder + key+".pkl",'wb')
            pickle.dump(val, file_object)
            file_object.close()
            self.output_data.append(self.data_folder + key + ".pkl")
        else:
            self.output_metric[key] = val

    def get_image_folder(self):
        """ Fetch folder to write images

        :return: str,
                 Folder path.
        """
        return self.plot_folder

    def close(self):
        """ Close tracking and write results to csv

        :return: dict,
                 experiment details are returned.
        """
        self.flag = 1
        self.end_time = datetime.now()
        self.runtime = self.end_time - self.start_time
        data = { 'exp_id':self.exp_id,
                 'start_time': self.start_time.strftime("%d/%m/%Y %H:%M:%S"),
                 'end_time': self.end_time.strftime("%d/%m/%Y %H:%M:%S"),
                 'runtime': str(self.runtime),
                 'plots': self.get_image_folder(),
                 'labels': json.dumps(self.labels),
                 'input_params': json.dumps(self.input_params),
                 'output_metric': json.dumps(self.output_metric),
                 #'git_commit': self.get_git_commit_id(),
                 'output_data': json.dumps(self.output_data)
                 }
        exp_file_path = self.base_folder + self.exp_file
        my_file = Path(exp_file_path)
        if my_file.exists():
            exp_result = pd.read_csv(exp_file_path)
            exp_result = pd.concat([exp_result, pd.DataFrame.from_dict([data])])
            exp_result.to_csv(exp_file_path, index=False)
        else:
            pd.DataFrame.from_dict([data]).to_csv(exp_file_path, index=False)

        return data
