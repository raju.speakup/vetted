import sys
import pandas as pd
from bat_data_sdk.postgres_db import PostgresDBClient
from sqlalchemy import create_engine


def get_target_db_engine(self, source_id, source_type):
    engine = None
    try:
        if source_type == 'data_store':
            return_object = self.mongo_client.find("di_datastores",
                                                          {"datastore_id": source_id})
            if return_object:
                conn_id = return_object['conn_id']

        elif source_type == 'data_cartridge':
            return_object = self.mongo_client.find("di_data_cartridges",
                                                          {"cartridge_id": source_id})
            if return_object:
                conn_id = return_object['conn_id']
        else:
            return_object = self.mongo_client.find("di_datamarts",
                                                          {"datamart_id": source_id})
            if return_object:
                conn_id = return_object['conn_id']

        return_object = self.mongo_client.find("di_connections", {"conn_id": conn_id})

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']
            schema = return_object['schema_name']

            engine =create_engine("postgresql+psycopg2://{}:{}@{}/{}".format(user,password,host,db_name))

        return engine
    except Exception as e:
        message = 'Error : {} .Unable to get engine for' \
                  ' source_id:{} and source_type:{} '.format(e.args[0], source_id, source_type)
        return message


def dataframe_from_table(query, conn):
    """
    Takes the query and the database connections and converts
    the query to a pandas dataframe
    """
    try:
        return pd.read_sql(query, conn)
    except Exception as e:
        message = 'Error : {}. Unable to get dataframe for query :{}'.format(e.args[0], query)
        return message


def save_df_to_mart(df, table_name, conn, mode):
    """
    Takes the query and the database connections and converts
    the query to a pandas dataframe
    """
    try:
        df.to_sql(table_name, con=conn, if_exists=mode, index=False)
        return "success"
    except Exception as e:
        message = 'Error : {}. Unable to save dataframe to table :{}'.format(e.args[0], table_name)
        return message


def get_target_db_connection(self, source_id, source_type):
    pg_db_client = None
    try:
        if source_type == 'data_store':
            return_object = self.mongo_client.find("di_datastores",
                                                          {"datastore_id": source_id})
            if return_object:
                conn_id = return_object['conn_id']

        elif source_type == 'data_cartridge':
            return_object = self.mongo_client.find("di_data_cartridges",
                                                          {"cartridge_id": source_id})
            if return_object:
                conn_id = return_object['conn_id']
        else:
            return_object = self.mongo_client.find("di_datamarts",
                                                          {"datamart_id": source_id})
            if return_object:
                conn_id = return_object['conn_id']

        return_object = self.mongo_client.find("di_connections", {"conn_id": conn_id})

        if return_object:
            db_name = return_object['db_name']
            host = return_object['host']
            port = return_object['port']
            user = return_object['user_name']
            password = return_object['password']
            schema = return_object['schema_name']

            pg_db_client = PostgresDBClient(db_name, user, password, host, port)

        return pg_db_client
    except Exception as e:
        message = 'Error : {} .Unable to get connection for' \
                  ' source_id:{} and source_type:{} '.format(e.args[0], source_id, source_type)
        sys.exit(message)