from config_store import config_store_client as conf_client

mapping = {
    'data_cartridge': 'di_data_cartridges',
    'data_mart': 'di_datamarts',
    'data_store': 'di_datastores'
}

query = 'select * from {} LIMIT {}'

dataset_collection_name = 'di_datasets'


class Config():

    def __init__(self):
        self.cs = conf_client.ConfigStore()
        self.cs.load(ip='*', service_name='api-datastore')

    def get_v(self, key):
        return self.cs.get(key)

    def reload(self):
        self.cs.load(ip='*', service_name='api-datastore')


conf = Config()