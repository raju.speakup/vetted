import sys
import json


from bat_data_sdk.config import (
    mapping,
    dataset_collection_name,
    query
    )
from bat_data_sdk.mongo_client import NoodleMongoClient
from bat_data_sdk.utils import (
    get_target_db_connection,
    dataframe_from_table, save_df_to_mart, get_target_db_engine)
from bat_data_sdk.config import conf


class DataClient:
    """SourceDataClient."""

    def __init__(self):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """
        db_name = conf.get_v('db.name')
        db_url = conf.get_v('db.url')
        self.url = db_url
        self.db_name = db_name
        self.mongo_client = NoodleMongoClient(self.url, self.db_name)

    def get_datamart(self, dataset_name, top=1000, filter_query=None):
        output = self.get_dataframe(dataset_name, top, filter_query)
        return output

    def get_data_cartridge(self, dataset_name, top=1000, filter_query=None):
        output = self.get_dataframe(dataset_name, top, filter_query)
        return output

    def get_datastore(self, dataset_name, top=1000, filter_query=None):
        output = self.get_dataframe(dataset_name, top, filter_query)
        return output

    def save_to_datamart(self, data_frame, target_mart_name, mode='replace'):
        output = self.save_dataframe(data_frame, target_mart_name, mode)
        return output

    def save_to_cartridge(self, data_frame, target_mart_name, mode='replace'):
        output = self.save_dataframe(data_frame, target_mart_name, mode)
        return output

    def save_to_datastore(self, data_frame, target_mart_name, mode='replace'):
        output = self.save_dataframe(data_frame, target_mart_name, mode)
        return output

    def get_dataframe(self, dataset_name, top, filter_query):

        dataset_doc = self.mongo_client.find_one_selected_field(
            collectionname=dataset_collection_name,
            data={
                'name': {
                    '$regex': '^{}$'.format(dataset_name.lower()),
                    '$options': 'i'
                }
            },
            projection={
                'source_id': 1,
                'source_type': 1,
                'table_name': 1,
                '_id': 0

            }
        )
        try:
            source_id = dataset_doc['source_id']
            source_type = dataset_doc['source_type']
            table_name = dataset_doc['table_name']

        except KeyError as err:
            message = err.args[0] + \
                      ' - ERROR - No data for source_name : ' + \
                      dataset_name
            return message

        except TypeError as err:
            message = err.args[0] + \
                      ' - ERROR - Name "'+dataset_name + '" not found in ' \
                      'db_name : ' + self.db_name
            return message

        cursor = get_target_db_connection(self, source_id, source_type)

        if filter_query:
            df = dataframe_from_table(filter_query, cursor.connection)
        else:
            df = dataframe_from_table(query.format(table_name, top), cursor.connection)
        return df

    def save_dataframe(self, data_frame, target_mart_name, mode):
        dataset_doc = self.mongo_client.find_one_selected_field(
            collectionname=dataset_collection_name,
            data={
                'name': {
                    '$regex': '^{}$'.format(target_mart_name.lower()),
                    '$options': 'i'
                }
            },
            projection={
                'source_id': 1,
                'source_type': 1,
                'table_name': 1,
                '_id': 0

            }
        )
        try:
            source_id = dataset_doc['source_id']
            source_type = dataset_doc['source_type']
            table_name = dataset_doc['table_name']

        except KeyError as err:
            message = err.args[0] + \
                      ' - ERROR - No data for source_name : ' + \
                      target_mart_name
            return message

        except TypeError as err:
            message = err.args[0] + \
                      ' - ERROR - Name "' + target_mart_name + '" not found in ' \
                      'db_name : ' + self.db_name
            return message

        engine = get_target_db_engine(self, source_id, source_type)
        
        status = save_df_to_mart(data_frame, table_name, engine, mode=mode)
        
        if status == 'success':
            return "dataframe is saved to target data mart"
        else:
            return status

        



