from pymongo import MongoClient
import json
import sys


class NoodleMongoClient:
    """NoodleMongoClient."""

    def __init__(self, db_url, db_name):
        """Init.

        method is run as soon as an object of a class is instantiated.
        The method is useful to do any initialization you want to do
        with your object
        """

        self.url = db_url
        self.db_name = db_name
        self.client = MongoClient(self.url)
        self.database = self.client[self.db_name]

    def find(self, collectionname, data):
        """Find One."""
        return self.database[collectionname].find_one(data)

    def find_one_selected_field(self, collectionname, data, projection):
        return self.database[collectionname].find_one(data, projection)