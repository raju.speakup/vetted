from celery import Celery
from bat_celery_sdk import celery_config

class MakeCelery():

    def __init__(self,broker_url,
                 result_backend, status_table, imports, prefetch=0):
        self.broker_url = broker_url
        self.result_backend = result_backend
        self.prefetch = prefetch
        self.status_table = status_table
        self.imports = imports
        self.app = Celery()

    def create_object(self):

        self.app.config_from_object(celery_config)
        self.app.conf.update(
            broker_url=self.broker_url,
            result_backend=self.result_backend,
            imports=self.imports,
            worker_prefetch_multiplier=self.prefetch,
                database_table_names={
                    'task': '{}_task'.format(self.status_table),
                    'group': '{}_group'.format(self.status_table),
            }

        )

        return self.app

