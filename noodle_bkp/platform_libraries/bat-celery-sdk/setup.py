from distutils.core import setup
from setuptools import setup, find_packages

setup(
    # Application name:
    name="bat_celery_sdk",

    # Version number (initial):
    version="1.0.0",

    # Application author details:
    author="rajkumar",
    author_email="c_rajkumar.t@noodle.ai",

    # Packages
    packages=find_packages(),

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://192.168.2.129:8081/repository/python-local/",

    #
    # license="LICENSE.txt",
    description="Library to create celery workers",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "flask",
        "celery",
        "config-store"
    ],
)
