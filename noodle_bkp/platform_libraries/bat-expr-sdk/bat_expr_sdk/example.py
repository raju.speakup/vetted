import os
print(os.getenv('DB_NAME', None))
os.environ['DB_URL'] = 'mongodb://mongo-admin:noodleadmin@192.168.2.170:27017'
os.environ['DB_NAME'] = 'bat_db'
os.environ['CONF_ENV'] = 'dev'
os.environ['CONF_STORE'] = '10.0.1.95:8811'
os.environ['NOTEBOOK_ENGINE_ENV'] = 'yes'

from bat_expr_sdk.expr_tracker import ExprTracker


tracker = ExprTracker('af816078-a163-427a-9d8c-cce94e8b750c')

a = tracker.set_input_param('a', 10)

a = a + 10

tracker.set_output_param('a_new', a)

tracker.close()

folder = tracker.get_charts_folder()

print(folder)