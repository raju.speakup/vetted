from bat_expr_sdk.mongo_db import db_client
import os

class TrackingDBUtils:

    def __init__(self):
        self.db_client = db_client

    def get_input_params(self, entity_id):
        record, count = self.db_client.find(os.getenv('DB_NAME', None), "nbruns", {'entity_id': entity_id}, 0, 1, "version")
        return record[0]['input_params']

    def get_input(self, entity_id, input_type):
        record, count = self.db_client.find(os.getenv('DB_NAME', None), "nbruns", {'entity_id': entity_id}, 0, 1, "version")
        return record[0][input_type]

    def get_run_id(self, entity_id):
        record, count = self.db_client.find(os.getenv('DB_NAME', None), "nbruns", {'entity_id': entity_id}, 0, 1, "version")
        return record[0]['r_id']

    def save_output_params(self, run_id, output_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'output_params': output_params}})
        return output

    def save_input_params(self, run_id, input_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'input_params': input_params}})
        return output

    def save_input_pkls(self, run_id, input_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'input_pkls': input_params}})
        return output

    def save_input_csvs(self, run_id, input_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'input_csvs': input_params}})
        return output

    def save_input_datamarts(self, run_id, input_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'input_datamarts': input_params}})
        return output

    def save_input_cartridges(self, run_id, input_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'input_cartridges': input_params}})
        return output

    def save_output_pkls(self, run_id, output_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'output_pkls': output_params}})
        return output

    def save_output_csvs(self, run_id, output_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'output_csvs': output_params}})
        return output

    def save_output_datamarts(self, run_id, output_params):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'output_datamarts': output_params}})
        return output

    def save_status(self, run_id, status):
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'status': status}})
        return output

    def save_charts(self, run_id, folder, files_list):
        images = []
        base_url = os.getenv('BAT_JUPYTER_URL', None)
        for x in files_list:
            folder = folder.replace('/home/jovyan/', base_url + '/user/test/files/')
            images.append(folder + x)
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'output_charts_images': images}})
        return output

    def save_files(self, run_id, folder, files_list):
        images = []
        base_url = os.getenv('BAT_JUPYTER_URL', None)
        for x in files_list:
            folder = folder.replace('/home/jovyan/', base_url + '/user/test/files/')
            images.append(folder + x)
        output = self.db_client.find_one_and_update(os.getenv('DB_NAME', None), 'nbruns', {'r_id': run_id}, {'$set':{'output_files_url': images}})
        return output

    def get_output_params(self, entity_id):
        record, count = self.db_client.find(os.getenv('DB_NAME', None), "nbruns", {'entity_id': entity_id,
                                                                                   "status" : "completed"}, 0, 1,
                                            "version")
        if record:
            return record[0]['output_params']
        else:
            return 'no params found'

    def get_output_datamarts(self, entity_id):
        record, count = self.db_client.find(os.getenv('DB_NAME', None), "nbruns", {'entity_id': entity_id,
                                                                                   "status" : "completed"}, 0, 1,
                                            "version")
        if record:
            return record[0]['output_datamarts']
        else:
            return 'no datamarts found'

    def get_output_pickles(self, entity_id):
        record, count = self.db_client.find(os.getenv('DB_NAME', None), "nbruns", {'entity_id': entity_id,
                                                                                   "status" : "completed"}, 0, 1,
                                            "version")
        if record:
            return record[0]['output_pkls']
        else:
            return 'no pickles found'

    def get_parent_output_params(self, dag_id, entity_id):
        result = []
        output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                      query={'ai_dag_id': dag_id,'nodes.nb_id': entity_id},
                                                      projection={'nodes.$.parents':1})
        parents = output[0]['nodes'][0]['parents']

        for parent in parents:
            output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                                query={'nodes.id': parent, 'ai_dag_id': dag_id},
                                                                projection={'nodes.$.nb_id':1})
            parent_entity = output[0]['nodes'][0]['nb_id']
            params = self.get_output_params(parent_entity)
            result.append(params)
        return result

    def get_parent_input_params(self, dag_id, entity_id):
        result = []
        output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                      query={'ai_dag_id': dag_id,'nodes.nb_id': entity_id},
                                                      projection={'nodes.$.parents': 1})
        parents = output[0]['nodes'][0]['parents']

        for parent in parents:
            output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                                query={'nodes.id': parent, 'ai_dag_id': dag_id},
                                                                projection={'nodes.$.nb_id': 1})
            parent_entity = output[0]['nodes'][0]['nb_id']
            params = self.get_input_params(parent_entity)
            result.append(params)
        return result

    def get_parent_output_datamarts(self, dag_id, entity_id):
        result = []
        output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                      query={'ai_dag_id': dag_id,'nodes.nb_id': entity_id},
                                                      projection={'nodes.$.parents': 1})
        parents = output[0]['nodes'][0]['parents']

        for parent in parents:
            output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                                query={'nodes.id': parent, 'ai_dag_id': dag_id},
                                                                projection={'nodes.$.nb_id': 1})
            parent_entity = output[0]['nodes'][0]['nb_id']
            params = self.get_output_datamarts(parent_entity)
            result.append(params)
        return result

    def get_parent_output_pickles(self, dag_id, entity_id):
        result = []
        output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                      query={'ai_dag_id': dag_id,'nodes.nb_id': entity_id},
                                                      projection={'nodes.$.parents': 1})
        parents = output[0]['nodes'][0]['parents']

        for parent in parents:
            output, count = self.db_client.find_with_projection(os.getenv('DB_NAME', None), 'ai_dags',
                                                                query={'nodes.id': parent, 'ai_dag_id': dag_id},
                                                                projection={'nodes.$.nb_id': 1})
            parent_entity = output[0]['nodes'][0]['nb_id']
            params = self.get_output_pickles(parent_entity)
            result.append(params)
        return result


tracker_utils = TrackingDBUtils()