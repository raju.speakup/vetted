from pymongo import MongoClient, ReturnDocument, ASCENDING, DESCENDING
import os

class MongoDBClient:
    def __init__(self):
        self.url = os.getenv('DB_URL', None)
        self.client = MongoClient(self.url, maxPoolSize=10, minPoolSize=0,
                                  serverSelectionTimeoutMS=30000, connectTimeoutMS=20000, maxIdleTimeMS=3000,
                                  appname="expr_tracker")

    def find(self, db_name, collection_name, query={}, offset=0, limit=1, sort_field = "_id"):
        db = self.client[db_name]

        if db is None:
            raise ConnectionError(self.error)

        results = []
        db_results = db[collection_name].find(query).sort(sort_field, DESCENDING).skip(int(offset)).limit(int(limit))
        results_count = db_results.count()
        for result in db_results:
            results.append(result)
        self.client.close()
        return results, results_count

    def find_with_projection(self, db_name, collection_name, query={}, projection={}, offset=0, limit=1, sort_field = "_id"):
        db = self.client[db_name]

        if db is None:
            raise ConnectionError(self.error)

        results = []
        db_results = db[collection_name].find(query, projection).sort(sort_field, DESCENDING).skip(int(offset)).limit(int(limit))
        results_count = db_results.count()
        for result in db_results:
            results.append(result)
        self.client.close()
        return results, results_count

    def find_with_cursor(self, db_name, collection_name, query={}):
        db = self.client[db_name]

        if db is None:
            raise ConnectionError(self.error)

        return db[collection_name].find(query)

    def insert_one(self, db_name, collection_name, data):
        db = self.client[db_name]
        if db is None:
            raise ConnectionError(self.error)
        return db[collection_name].insert_one(data)

    def find_one_and_update(self, db_name, collection_name, query, update):
        db = self.client[db_name]
        if db is None:
            raise ConnectionError(self.error)
        return db[collection_name].find_one_and_update(query, update, return_document=ReturnDocument.AFTER)

    def find_one_and_delete(self, db_name, collection_name, query):
        db = self.client[db_name]
        if db is None:
            raise ConnectionError(self.error)
        return db[collection_name].find_one_and_delete(query, projection={'_id': False})

    def close(self):
        return self.client.close()

db_client = MongoDBClient()
