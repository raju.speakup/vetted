import pandas as pd
import pickle
import os


class ExprTracker:

    def __init__(self, entity_id, client='global', env="prod", url='192.168.10.180:8811', debug=False):
        os.environ['CONF_ENV'] = env
        os.environ['CONF_STORE'] = url
        from bat_expr_sdk.config import conf
        conf.reload()
        os.environ['DB_URL'] = conf.get_v('db.url')
        os.environ['DB_NAME'] = conf.get_v('db.name')
        os.environ['BAT_JUPYTER_URL'] = conf.get_v('jupyter.url')
        if os.getenv('NOTEBOOK_ENGINE_ENV', None) is None:
            os.environ['NOTEBOOK_ENGINE_ENV'] = conf.get_v('nb.engine')

        if debug:
            print("{} -> {}".format('DB_URL', os.getenv('DB_URL', None)))
            print("{} -> {}".format('DB_NAME', os.getenv('DB_NAME', None)))
            print("{} -> {}".format('NOTEBOOK_ENGINE_ENV', os.getenv('NOTEBOOK_ENGINE_ENV', None)))
            print("{} -> {}".format('BAT_JUPYTER_URL', os.getenv('BAT_JUPYTER_URL', None)))

        from bat_expr_sdk.tracking_utils import tracker_utils

        self._entity_id = entity_id
        self.run_id = tracker_utils.get_run_id(entity_id)
        self._override_input = (os.getenv('NOTEBOOK_ENGINE_ENV', None) == 'yes')
        self._charts_folder = '~/exprs/' + client + '/' + self._entity_id + '/' + self.run_id + '/charts/'
        self._data_folder = '~/exprs/' + client + '/' + self._entity_id + '/' + self.run_id + '/data/'
        self._store = {'input_params': tracker_utils.get_input_params(self._entity_id),
                       'output_params': {},
                       'input_pkls': [],
                       'input_csvs': [],
                       'input_datamarts': tracker_utils.get_input(self._entity_id, 'input_datamarts'),
                       'input_cartridges': tracker_utils.get_input(self._entity_id, 'input_cartridges'),
                       'output_pkls': [],
                       'output_csvs': [],
                       'output_datamarts': tracker_utils.get_input(self._entity_id, 'output_datamarts')
                       }

    def set_input_param(self, key, value):
        if self._override_input:
            self._store['input_params'][key] = value
            return value
        else:
            return self._store['input_params'][key]

    def set_input_pkls(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['input_pkls'].append(rec)
            return value
        else:
            return self._store['input_pkls'][key]

    def set_input_csvs(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['input_csvs'].append(rec)
            return value
        else:
            return self._store['input_csvs'][key]

    def find_in_array(self, docs, key):
        for x in docs:
            if x['key'] == key:
                return x['value']
        return None

    def set_input_datamarts(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['input_datamarts'].append(rec)
            return value
        else:
            return self.find_in_array(self._store['input_datamarts'], key)

    def set_input_cartridges(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['input_cartridges'].append(rec)
            return value
        else:
            return self.find_in_array(self._store['input_cartridges'], key)

    def set_output_pkls(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['output_pkls'].append(rec)
            return value
        else:
            return self._store['output_pkls'][key]

    def set_output_csvs(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['output_csvs'].append(rec)
            return value
        else:
            return self._store['output_csvs'][key]

    def set_output_datamarts(self, key, value, desc=''):
        if self._override_input:
            rec = {'key': key, 'value': value, 'desc': desc}
            self._store['output_datamarts'].append(rec)
            return value
        else:
            return self.find_in_array(self._store['output_datamarts'], key)

    def set_output_param(self, key, value):
        if isinstance(value, pd.DataFrame):
            file_path = self._data_folder + key + ".pkl"
            file_object = open(file_path, 'wb')
            self._store['output_params'][key] = file_path
            pickle.dump(value, file_object)
            file_object.close()
            return True
        else:
            self._store['output_params'][key] = value
            return value

    def get_charts_folder(self):
        os.system('mkdir -p ' + self._charts_folder)
        return os.path.expanduser(self._charts_folder)

    def get_data_folder(self):
        return os.path.expanduser(self._data_folder)

    def close(self):
        from bat_expr_sdk.tracking_utils import tracker_utils

        tracker_utils.save_output_params(self.run_id, self._store['output_params'])
        tracker_utils.save_input_params(self.run_id, self._store['input_params'])
        tracker_utils.save_input_pkls(self.run_id, self._store['input_pkls'])
        tracker_utils.save_input_csvs(self.run_id, self._store['input_csvs'])
        tracker_utils.save_input_datamarts(self.run_id, self._store['input_datamarts'])
        tracker_utils.save_input_cartridges(self.run_id, self._store['input_cartridges'])

        tracker_utils.save_output_pkls(self.run_id, self._store['output_pkls'])
        tracker_utils.save_output_csvs(self.run_id, self._store['output_csvs'])
        tracker_utils.save_output_datamarts(self.run_id, self._store['output_datamarts'])

        dir_path = self.get_charts_folder()
        if os.path.isdir(dir_path) and os.path.exists(dir_path):
              tracker_utils.save_charts(self.run_id, self.get_charts_folder(), os.listdir(dir_path))

        if not self._override_input:
            tracker_utils.save_status(self.run_id, 'completed')

        return True

    def get_parent_input_params(self):
        dag_id = os.environ['DAG_ID']
        from bat_expr_sdk.tracking_utils import tracker_utils
        output = tracker_utils.get_parent_input_params(dag_id, self._entity_id)
        return output

    def get_parent_output_params(self):
        dag_id = os.environ['DAG_ID']
        from bat_expr_sdk.tracking_utils import tracker_utils
        output = tracker_utils.get_parent_output_params(dag_id, self._entity_id)
        return output

    def get_parent_output_datamarts(self):
        dag_id = os.environ['DAG_ID']
        from bat_expr_sdk.tracking_utils import tracker_utils
        output = tracker_utils.get_parent_output_datamarts(dag_id, self._entity_id)
        return output

    def get_parent_output_pickles(self):
        dag_id = os.environ['DAG_ID']
        from bat_expr_sdk.tracking_utils import tracker_utils
        output = tracker_utils.get_parent_output_pickles(dag_id, self._entity_id)
        return output