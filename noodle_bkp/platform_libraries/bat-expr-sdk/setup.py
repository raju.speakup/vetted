from distutils.core import setup
from setuptools import setup, find_packages

setup(
    # Application name:
    name="bat_expr_sdk",

    # Version number (initial):
    version="1.0.23",

    # Application author details:
    author="Nimesh Agarwal",
    author_email="nimesh.agarwal@noodle.ai",

    # Packages
    packages=find_packages(),

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://192.168.2.129:8081/repository/python-local/",

    #
    # license="LICENSE.txt",
    description="Library to track experiments on BAT platform",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "requests",
        "Flask",
        "pandas",
        "pymongo",
        "config-store"
    ],
)
