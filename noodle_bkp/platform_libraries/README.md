 
### Setup:
 
You can install package using below PIP command:
 
pip install bat_client_sdk==1.0.3 --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
 
 
### Sample Code:
 
from bat_client_sdk.datasets import client as ds
print(ds.load("Forecast"))
 
from bat_client_sdk.recorder import client as rec
rec.version()
 
from bat_client_sdk.s3_client import client as s3
s3.version()

### Code repo:
https://bitbucket.org/noodleai/platform_libraries
 
### Upload package to Nexus:
https://noodleai.atlassian.net/wiki/spaces/SE/pages/166297683/Noodle+internal+artifactory+to+host+Python+R+Node+package
 
### Nexus Repository Manager:
http://192.168.2.129:8081/#browse/browse/components:python-local