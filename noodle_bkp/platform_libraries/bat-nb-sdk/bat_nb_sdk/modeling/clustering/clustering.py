import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import multiprocessing
from scipy import stats
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture
from sklearn.metrics import silhouette_score
from scipy.spatial import distance
from sklearn.datasets.samples_generator import make_blobs


def Synthesize_Data(n_samples, centers, n_features, column_names):
    """ Generate synthetic dataset with 500 samples and 5 clusters to simulate supplier data.
    Variables include Median Delivery Delay, Ontime Delivery Percentage, and Mean Lead Time.

    Parameters
    ----------
    n_samples : int
        The total number of points equally divided among clusters.
    centers : int
        The number of centers to generate, or the fixed center locations.
    n_features : int
        The number of features for each sample.
    column_names : list of str
        list of column names.

    Returns
    -------
    pandas dataFrame

    """

    # Use sklearn's make_blobs function with manually tuned parameters to mimic supplier data
    blobs, cluster_labels = make_blobs(n_samples=n_samples, centers=centers, n_features=n_features,
                                       random_state=9, cluster_std=4, center_box=(0, 100))

    # Rename features to real world supplier metrics
    df = pd.DataFrame(blobs, columns=column_names)

    # Scale Ontime Delivery Percentage down to a 0-1.0 range and adjust Mean Lead Time
    df.Ontime_Delivery_Pct = (df.Ontime_Delivery_Pct + 5) / 75

    return df

def Normality_Metrics(df):
    """
    Get summary statistics to determine if the input variables are normally distributed.
    Statistics include: Shapiro-Wilk score, skew, kurtosis, mean, median, standard deviation.

    Parameters
    -----------
    df : dataFrame
        dataframe of input features (unscaled)

    Returns
    -----------
    Dataframe with features as rows and statistics as columns, sorted by Shaprio_Wilk socres.
    """
    # Generate dataframe using dictionary of summary statistics
    norm_metrics = pd.DataFrame({'Skew': df.skew(),
                                 'Kurtosis': df.kurtosis(),
                                 'Shapiro': [x[0] for x in df.apply(stats.shapiro, axis=0)],
                                 'Mean': df.apply(np.mean, axis=0),
                                 'Median': df.apply(np.median, axis=0),
                                 'Standard_Deviation': df.std()})

    # Sort values by Shapiro-Wilk score
    norm_metrics.sort_values(by='Shapiro', axis=0, ascending=False)

    return norm_metrics


def Pairplot(df, download=True, directory='~/Downloads', folder_name='kmeans_plots', file_name='pairplot'):
    """
    Generate a grid with scatterplots for each feature pair and histograms for each feature.

    Parameters
    -----------
    df: dataframe
        dataframe of input features (unscaled)
    download: bool
        True if the plot should be saved as a .png file
    directory: str
        file path for the parent directory where the plot has to be saved
    folder_name: str
        name of the folder where the plot has to be saved
    file_name: str
        file name of the plot

    Returns
    -----------
    Displays a seaborn visualization.
    """

    sns.pairplot(data=df, markers='.', diag_kws={'bins': 50})

    # Download plot to the specified directory
    if download is True:
        user_dir = os.path.expanduser(directory)
        save_dir = user_dir + '/' + folder_name
        if os.path.exists(save_dir):
            pass
        else:
            os.mkdir(user_dir + '/' + folder_name)
        plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
        print('Downloaded!')

    plt.show()


def Correlation_Heatmap(df, download=True, directory='~/Downloads', folder_name='kmeans_plots', file_name='correlation_heatmap'):
    """
    Generate a correlation heatmap for all features in the input dataframe, colored by correlation strength and direction.

    Parameters
    -----------
    df: datafarme,
        dataframe of input features (unscaled)
    download: bool
        True if the plot should be saved as a .png file
    directory: str
        file path for the parent directory where the plot has to be saved
    folder_name: str
        name of the folder where the plot has to be saved
    file_name: str
        file name of the plot

    Returns
    -----------
    Automatically shows a seaborn heatmap with redundant cells blanked out.
    """
    plt.figure(figsize=(len(df.columns),len(df.columns)))
    mask = np.zeros_like(df.corr())
    mask[np.triu_indices_from(mask)] = True
    with sns.axes_style("white"):
        ax = sns.heatmap(df.corr(), mask=mask, annot=True)

    # Download plot to the specified directory
    if download is True:
        user_dir = os.path.expanduser(directory)
        save_dir = user_dir + '/' + folder_name
        if os.path.exists(save_dir):
            pass
        else:
            os.mkdir(user_dir + '/' + folder_name)
        plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
        print('Downloaded!')

    plt.show()


def Compute_BIC(kmeans, X):
    """
    Compute the Bayesian Information Criterion for a given KMeans object from sklearn.

    Parameters
    -----------
    kmeans: `KMeans`
        sklearn KMeans object which has been fit with input matrix X
    X: `numpy.ndarray`
        input matrix used to fit KMeans (scaled)

    Returns
    -----------
    Float with the BIC score for the given clusters.
    """

    # Assign centers and labels
    centers = [kmeans.cluster_centers_]
    labels  = kmeans.labels_

    # Number of clusters
    m = kmeans.n_clusters

    # Size of the clusters
    n = np.bincount(labels)

    # Size of data set
    N, d = X.shape

    # Compute variance for all clusters beforehand
    cl_var = (1.0 / (N - m) / d) * sum([sum(distance.cdist(X[np.where(labels == i)], [centers[0][i]],
             'euclidean')**2) for i in range(m)])

    # Define constant term
    const_term = 0.5 * m * np.log(N) * (d+1)

    # Caluclate final BIC
    BIC = np.sum([n[i] * np.log(n[i]) - \
                  n[i] * np.log(N) - \
                ((n[i] * d) / 2) * np.log(2*np.pi*cl_var) - \
                ((n[i] - 1) * d/ 2) for i in range(m)]) - \
                  const_term

    return(BIC)


class KMeans_Clustering(object):
    """ The main KMeans_Clustering object.

    The KMeans_Clustering is an python Class which provides methods
    for Kmeans Clustering .
    The Algorithm K-means is one of the simplest unsupervised learning algorithms that solve the well known clustering problem.


    Usage::
            >>> kmeans = KMeans_Clustering(df, X, min_clusters, max_clusters, x_variable, y_variable, worker_cpus, **modelargs)

    Returns
    -----------
    :class:`KMeans_Clustering` object

    """

    def __init__(self, df, X, min_clusters, max_clusters, x_variable, y_variable, worker_cpus, **modelargs):
        """

        Parameters
        ----------
        df : dataframe
            DataFrame with original data (unscaled)
        X : `numpy.ndarray`
            input matrix (scaled)
        min_clusters : int
            minimum value to test for n_clusters argument
        max_clusters : int
            maximum value to test for n_clusters argument
        x_variable : str
            string contianing the name of the variable to be visualized on the x axis of the scatterplot.
        y_variable : str
            string contianing the name of the variable to be visualized on the y axis of the scatterplot.
        worker_cpus : int
            Number of available CPUs to use for multiprocessing, defaults to half of total available CPUs.
        modelargs : dict
            Additional keyword arguments for sklearn's KMeans algorithm.
        """
        self.df = df
        self.X = X
        self.min_clusters = min_clusters
        self.max_clusters = max_clusters
        self.x_variable = x_variable
        self.y_variable = y_variable
        self.modelargs = modelargs
        self.worker_cpus = worker_cpus
        self.optimal_ = None

    def KMeans_Silhouette(self, i):
        """
        Fit a KMeans model with the given value of n_clusters and return the silhouette score.

        Parameters
        -----------
        i: int
            integer to be passed as n_clusters

        Returns
        -----------
        Returns a tuple in the form (n_clusters, silhouette score)
        """
        # Instantiate KMeans object for each possible value of n_clusters and fit with input matrix
        km = KMeans(n_clusters=i, **self.modelargs)
        km.fit_predict(self.X)

        # Compute clustering metric
        sil = silhouette_score(self.X, km.labels_)

        return i, sil

    def KMeans_Inertia(self, i):
        """
        Fit a KMeans model with the given value of n_clusters and return the inertia (SSE).

        Parameters
        -----------
        i: int
            integer to be passed as n_clusters

        Returns
        -----------
        Returns a tuple in the form (n_clusters, inertia)
        """
        # Instantiate KMeans object for each possible value of n_clusters and fit with input matrix
        km = KMeans(n_clusters=i, **self.modelargs)
        km.fit_predict(self.X)

        # Compute clustering metric
        inertia = km.inertia_

        return i, inertia

    def KMeans_BIC(self, i):
        """
        Fit a KMeans model with the given value of n_clusters and return the Bayesian Information Criterion.

        Parameters
        -----------
        i: int
            integer to be passed as n_clusters

        Returns
        -----------
        Returns a tuple in the form (n_clusters, BIC)
        """
        # Instantiate KMeans object for each possible value of n_clusters and fit with input matrix
        km = KMeans(n_clusters=i, **self.modelargs)
        km.fit_predict(self.X)

        # Compute clustering metric
        bic = Compute_BIC(km, self.X)

        return i, bic

    def iterate_silhouette(self, download=True, directory='~/Downloads', folder_name='kmeans_plots',
                           file_name='silhouette_elbow_plot'):
        """
        Run KMeans clustering across a user specified range of n_clusters and display elbow plot
        to determine optimal number of clusters.

        Parameters
        ----------
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -------
        Elbow plot containing silhouette scores, shown in notebook and saved to filepath.
        """
        # Instantiate pool of parallel workers and run iterations
        pool = multiprocessing.Pool(self.worker_cpus)
        results = pool.map(self.KMeans_Silhouette, range(self.min_clusters, (self.max_clusters + 1)))

        # Convert multiprocessing results into a dictionary of n_clusters and silhouette scores
        kmeans_metrics = {i: [i, x] for i, x in results}

        # Convert metrics dictionary into a dataframe and rename columns
        kmetrics_df = pd.DataFrame(kmeans_metrics).transpose()
        kmetrics_df.columns = ['clusters', 'Silhouette']

        # Show silhouette plot
        kmetrics_df.loc[:,'Silhouette'].plot(figsize=(15, 5))
        plt.title('KMeans Silhouette Scores', size=20)
        plt.xlabel('n_clusters')
        plt.ylabel('Silhouette Score')

        # Download silhouette plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

    def iterate_inertia(self, download=True, directory='~/Downloads', folder_name='kmeans_plots',
                        file_name='inertia_elbow_plot'):
        """
        Run KMeans clustering across a user specified range of n_clusters and display elbow plot
        to determine optimal number of clusters.

        Parameters
        ----------
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -------
        Elbow plot containing inertia (SSE), shown in notebook and saved to filepath.
        """

        # Instantiate pool of parallel workers and run iterations
        pool = multiprocessing.Pool(self.worker_cpus)
        results = pool.map(self.KMeans_Inertia, range(self.min_clusters, (self.max_clusters + 1)))

        # Convert multiprocessing results into a dictionary of n_clusters and inertia
        kmeans_metrics = {i: [i, x] for i, x in results}

        # Convert metrics dictionary into a dataframe and rename columns
        kmetrics_df = pd.DataFrame(kmeans_metrics).transpose()
        kmetrics_df.columns = ['clusters', 'Inertia (SSE)']

        # Show inertia plot
        kmetrics_df.loc[:,'Inertia (SSE)'].plot(figsize=(15, 5))
        plt.title('KMeans Inertia (SSE)', size=20)
        plt.xlabel('n_clusters')
        plt.ylabel('Inertia (SSE)')

        # Download plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

        plt.show()

    def iterate_bic(self, download=True, directory='~/Downloads', folder_name='kmeans_plots',
                    file_name='BIC_elbow_plot'):
        """
        Run KMeans clustering across a user specified range of n_clusters and display elbow plot
        to determine optimal number of clusters.

        Parameters
        ----------
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -------
        Elbow plot containing BIC, shown in notebook and saved to filepath.
        """

        # Instantiate pool of parallel workers and run iterations
        pool = multiprocessing.Pool(self.worker_cpus)
        results = pool.map(self.KMeans_BIC, range(self.min_clusters, (self.max_clusters + 1)))

        # Convert multiprocessing results into a dictionary of n_clusters and BIC
        kmeans_metrics = {i: [i, x] for i, x in results}

        # Convert metrics dictionary into a dataframe and rename columns
        kmetrics_df = pd.DataFrame(kmeans_metrics).transpose()
        kmetrics_df.columns = ['clusters', 'BIC']

        # Show BIC plot
        kmetrics_df.loc[:,'BIC'].plot(figsize=(15, 5))
        plt.title('KMeans BIC', size=20)
        plt.xlabel('n_clusters')
        plt.ylabel('BIC')

        # Download plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

        plt.show()

    def optimal(self, optimal_clusters, download=True, directory='~/Downloads', folder_name='kmeans_plots',
                file_name='optimal_scatterplot'):
        """
        Train KMeans algorithm based on optimal number of clusters as determined by plots from iterate_[metric] functions.
        Display scatterplot with differently colored clusters along two user specified dimensions.

        Parameters
        ----------
        optimal_clusters : integer
            indicating the n_clusters value corresponding to the "elbow" of Iterate_KMeans plots
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -------
        Displays scatterplot with a hue corresponding to clusters.
        Returns the sklearn KMeans object fit with the scaled training data at the specified n_clusters.
        """

        # Instantiate KMeans object and fit with input matrix
        km = KMeans(optimal_clusters, **self.modelargs).fit(self.X)

        # Create a dataframe with cluster labels and concatenate to unscaled input data
        km_labels = pd.DataFrame({'Cluster Label': km.labels_})
        km_df = pd.concat([self.df, km_labels], axis=1)

        # Visualize clusters by color on original unscaled feature axes
        sns.lmplot(data=km_df, x=self.x_variable, y=self.y_variable, hue='Cluster Label', fit_reg=False, size=10)
        plt.title('Optimal KMeans', size=20)

        # Download plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

        plt.show()

        # Assign optimal model to a class attribute
        self.optimal_ = km


class Agglomerative_Clustering(object):
    """ The main Agglomerative_Clustering object.

    The Agglomerative_Clustering is an python Class which provides methods
    for agglomerative Clustering .
    Agglomerative hierarchical clustering is a bottom-up clustering method where clusters have sub-clusters, which in turn have sub-clusters, etc.

    Usage::
            >>> agglomerative = Agglomerative_Clustering(df, X, min_clusters, max_clusters, x_variable, y_variable, worker_cpus, **modelargs)


    Returns
    -----------
    :class:`Agglomerative_Clustering` object


    """

    def __init__(self, df, X, min_clusters, max_clusters, x_variable, y_variable, worker_cpus, **modelargs):
        """

        Parameters
        ----------
        df : dataframe
            DataFrame with original data (unscaled)
        X : `numpy.ndarray`
            input matrix (scaled)
        min_clusters : int
            minimum value to test for n_clusters argument
        max_clusters : int
            maximum value to test for n_clusters argument
        x_variable : str
            string containing the name of the variable to be visualized on the x axis of the scatterplot.
        y_variable : str
            string containing the name of the variable to be visualized on the y axis of the scatterplot.
        worker_cpus : int
            Number of available CPUs to use for multiprocessing, defaults to half of total available CPUs.
        modelargs : dict
            Additional keyword arguments for sklearn's AgglomerativeClustering algorithm.
        """

        self.df = df
        self.X = X
        self.min_clusters = min_clusters
        self.max_clusters = max_clusters
        self.x_variable = x_variable
        self.y_variable = y_variable
        self.modelargs = modelargs
        self.worker_cpus = worker_cpus
        self.optimal_ = None

    def Agglomerative_Silhouette(self, i):
        """
        Fit a Agglomerative model with the given value of n_clusters and return the silhouette score.

        Parameters
        -----------
        i: int
            integer to be passed as n_clusters

        Returns
        -----------
        Returns a tuple in the form (n_clusters, silhouette score)
        """
        # Instantiate Agglomerative object for each possible value of n_clusters and fit with input matrix
        ac = AgglomerativeClustering(n_clusters=i, **self.modelargs)
        ac.fit_predict(self.X)

        # Compute clustering metric
        sil = silhouette_score(self.X, ac.labels_)

        return i, sil

    def iterate_silhouette(self, download=True, directory='~/Downloads',
                           folder_name='agglomerative_plots', file_name='silhouette_elbow_plot'):
        """
        Run Agglomerative clustering across a user specified range of n_clusters and display elbow plot
        to determine optimal number of clusters.

        Parameters
        -----------
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -----------
        Elbow plot containing silhouette scores, shown in notebook and saved to filepath.
        """
        # Instantiate pool of parallel workers and run iterations
        pool = multiprocessing.Pool(self.worker_cpus)
        results = pool.map(self.Agglomerative_Silhouette, range(self.min_clusters, (self.max_clusters + 1)))

        # Convert multiprocessing results into a dictionary of n_clusters and silhouette scores
        ac_metrics = {i: [i, x] for i, x in results}

        # Convert metrics dictionary into a dataframe and rename columns
        acmetrics_df = pd.DataFrame(ac_metrics).transpose()
        acmetrics_df.columns = ['clusters', 'Silhouette']

        # Show silhouette plot
        acmetrics_df.loc[:,'Silhouette'].plot(figsize=(15, 5))
        plt.title('Agglomerative Silhouette Scores', size=20)
        plt.xlabel('n_clusters')
        plt.ylabel('Silhouette Score')

        # Download silhouette plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

    def optimal(self, optimal_clusters, download=True, directory='~/Downloads', folder_name='agglomerative_plots',
                file_name='optimal_scatterplot'):
        """
        Train Agglomerative Clustering algorithm based on optimal number of clusters as determined by plots from
        iterate_silhouette function.
        Display scatterplot with differently colored clusters along two user specified dimensions.

        Parameters
        -----------
        optimal_clusters : integer
            indicating the n_clusters value corresponding to the "elbow" of Iterate_KMeans plots
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -----------
        Displays scatterplot with a hue corresponding to clusters.
        Returns the sklearn AgglomerativeClustering object fit with the scaled training data at the specified n_clusters.
        """
        # Instantiate Agglomerative Clustering object and fit with input matrix
        ac = AgglomerativeClustering(optimal_clusters, **self.modelargs).fit(self.X)

        # Create a dataframe with cluster labels and concatenate to unscaled input data
        ac_labels = pd.DataFrame({'Cluster Label': ac.labels_})
        ac_df = pd.concat([self.df, ac_labels], axis=1)

        # Visualize clusters by color on original unscaled feature axes
        sns.lmplot(data=ac_df, x=self.x_variable, y=self.y_variable, hue='Cluster Label', fit_reg=False, size=10)
        plt.title('Optimal Agglomerative', size=20)

        # Download plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

        plt.show()

        # Assign optimal model to a class attribute
        self.optimal_ = ac


class Mixture_Model(object):
    """ The main Mixture_Model object.

    The Mixture_Model is an python Class which provides methods
    for Gaussian Mixture Clustering .
    A Gaussian mixture model is a probabilistic model that assumes all the data points are generated from a mixture of a finite number of Gaussian distributions with unknown parameters.
    One can think of mixture models as generalizing k-means clustering to incorporate information about the covariance structure of the data as well as the centers of the latent Gaussians.

    Usage::
            >>> gmm = Mixture_Model(df, X, min_clusters, max_clusters, x_variable, y_variable, worker_cpus, **modelargs)


    Returns
    -----------
    :class:`Mixture_Model` object


    """

    def __init__(self, df, X, min_clusters, max_clusters, x_variable, y_variable, worker_cpus, **modelargs):
        """

        Parameters
        -----------
        df : dataframe
            DataFrame with original data (unscaled)
        X : `numpy.ndarray`
            input matrix (scaled)
        min_clusters : int
            minimum value to test for n_clusters argument
        max_clusters : int
            maximum value to test for n_clusters argument
        x_variable : str
            string containing the name of the variable to be visualized on the x axis of the scatterplot.
        y_variable : str
            string containing the name of the variable to be visualized on the y axis of the scatterplot.
        worker_cpus : int
            Number of available CPUs to use for multiprocessing, defaults to half of total available CPUs.
        modelargs : dict
            Additional keyword arguments for sklearn's GaussianMixture algorithm.
        """

        self.df = df
        self.X = X
        self.min_clusters = min_clusters
        self.max_clusters = max_clusters
        self.x_variable = x_variable
        self.y_variable = y_variable
        self.modelargs = modelargs
        self.worker_cpus = worker_cpus
        self.optimal_ = None

    def GMM_Silhouette(self, i):
        """
        Fit a Gaussian mixture model with the given value of n_clusters and return the silhouette score.

        Parameters
        -----------
        i: int
            integer to be passed as n_clusters

        Returns
        -----------
        Returns a tuple in the form (n_clusters, silhouette score)
        """
        # Instantiate Gaussian Mixture object for each possible value of n_clusters and fit with input matrix
        gmm = GaussianMixture(n_components=i, **self.modelargs)
        gmm.fit(self.X)

        # Compute clustering metric
        sil = silhouette_score(self.X, gmm.predict(self.X))

        return i, sil

    def iterate_silhouette(self, download=True, directory='~/Downloads',
                           folder_name='gmm_plots', file_name='silhouette_elbow_plot'):
        """
        Run Gaussiam mixture clustering across a user specified range of n_clusters and display elbow plot
        to determine optimal number of clusters.

        Parameters
        -----------
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -----------
        Elbow plot containing silhouette scores, shown in notebook and saved to filepath.
        """
        # Instantiate pool of parallel workers and run iterations
        pool = multiprocessing.Pool(self.worker_cpus)
        results = pool.map(self.GMM_Silhouette, range(self.min_clusters, (self.max_clusters + 1)))

        # Convert multiprocessing results into a dictionary of n_clusters and silhouette scores
        ac_metrics = {i: [i, x] for i, x in results}

        # Convert metrics dictionary into a dataframe and rename columns
        acmetrics_df = pd.DataFrame(ac_metrics).transpose()
        acmetrics_df.columns = ['clusters', 'Silhouette']

        # Show silhouette plot
        acmetrics_df.loc[:,'Silhouette'].plot(figsize=(15, 5))
        plt.title('Gaussian Mixture Silhouette Scores', size=20)
        plt.xlabel('n_clusters')
        plt.ylabel('Silhouette Score')

        # Download silhouette plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

    def optimal(self, optimal_clusters, download=True, directory='~/Downloads', folder_name='gmm_plots',
                file_name='optimal_scatterplot'):
        """
        Train Gaussian mixture moedl algorithm based on optimal number of clusters as determined by plots from
        iterate_silhouette function.
        Display scatterplot with differently colored clusters along two user specified dimensions.

        Parameters
        -----------
        optimal_clusters : integer
            indicating the n_clusters value corresponding to the "elbow" of Iterate_KMeans plots
        download: bool
            True if the plot should be saved as a .png file
        directory: str
            file path for the parent directory where the plot has to be saved
        folder_name: str
            name of the folder where the plot has to be saved
        file_name: str
            file name of the plot

        Returns
        -----------
        Displays scatterplot with a hue corresponding to clusters.
        Returns the sklearn GaussianMixture object fit with the scaled training data at the specified n_clusters.
        """
        # Instantiate GMM Clustering object and fit with input matrix
        gmm = GaussianMixture(optimal_clusters, **self.modelargs)
        gmm.fit(self.X)

        # Create a dataframe with cluster labels and concatenate to unscaled input data
        gmm_labels = pd.DataFrame({'Cluster Label': gmm.predict(self.X)})
        gmm_df = pd.concat([self.df, gmm_labels], axis=1)

        # Visualize clusters by color on original unscaled feature axes
        sns.lmplot(data=gmm_df, x=self.x_variable, y=self.y_variable, hue='Cluster Label', fit_reg=False, size=10)
        plt.title('Optimal Gaussian Mixture', size=20)

        # Download plot to the specified directory
        if download is True:
            user_dir = os.path.expanduser(directory)
            save_dir = user_dir + '/' + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir(user_dir + '/' + folder_name)
            plt.savefig(user_dir + '/' + folder_name + '/' + file_name + '.png')
            print('Downloaded!')

        plt.show()

        # Assign optimal model to a class attribute
        self.optimal_ = gmm
