#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import inspect
import os
import numpy as np
import matplotlib.pyplot as plt


class WarningE(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class FatalError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class dqa(object):

    def __init__(self, data):
        self.data = data
    
    def getfileinfo(self):
        '''
        This function returns the basic information of the file

        :return: * An array of column names
                 * Size in tuple (row, column)
                 * Dictionay with columnname : dtype
        '''

        columns = list(self.data)
        shape = self.data.shape
        dtypes = dict(self.data.dtypes)
        return columns, shape, dtypes

    def check_columns_exists(self, column_names):
        '''
        This function checks if the input columns are present in the dataframe or not

        :param column_names: list of columns(if multiple else string)
        :return: Nothing if the test passes
        :raises: FatalError if columns do not match
        '''

        remain_cols = set([column_names] if type(column_names) == str else column_names) - set(self.data.columns)
        if remain_cols == set():
            pass
        else:
            error = inspect.stack()[0][3] + "(): "+str(list(remain_cols))+" not in dataframe"
            raise FatalError(error)
    
    def check_shape(self, Shape):
        '''
        This function checks the shape of the dataframe

        :param Shape: Size in tuple (row, column)
        :return: Nothing if the test passes
        :raises: FatalError if shape doesn't match
        '''

        if self.data.shape == Shape:
            pass
        else:
            error = inspect.stack()[0][3] + "(): Shape of the dataframe"+ str(self.data.shape)+" doesn't match with "+ str(Shape)
            raise FatalError(error)
            
    def check_none_limits_col(self, *limits):
        '''
        This function checks the percentage of NULL's present in a given column

        :param limits: dictionary with columnname as key and percentage(of null's alllowed) as value
        :return: Nothing if the test passes. If no arguments are given, by default, it returns a dictionary with numeric column names as key and
        their NULL percentages as value
        :raises: Throws Warning if the percentage of NULL values exceed limit
        '''
        
        null_values = self.data.isnull().sum()
        single_limit = len(self.data)
        if len(limits) != 0:
            for item in limits[0]:
                if (null_values[item]*100/single_limit > limits[0][item]):
                    error = inspect.stack()[0][3] + "(): Exceeded for "+ repr(item)
                    raise WarningE(error)
        else:
            Numeric_cols_Null_percents = {}
            Numeric_columns = self.check_numeric()
            for col in Numeric_columns:
                Numeric_cols_Null_percents[col] = null_values[col]*100/single_limit
            return Numeric_cols_Null_percents

    def check_size(self,filepath, sizeLimits ):
        """
        This function checks the size of the file

        :param filepath: Filepath of the test data file
        :param sizeLimits: tuple, the lowerbound and upperbound on the size in kB
        :return: Nothing if the test passes
        :raises: Throws warning if size lies outside bounds
        """

        size = os.path.getsize(filepath)
        low = sizeLimits[0]
        high = sizeLimits[1]
        if((size < low*1e3) or (size > high*1e3)):
            error = inspect.stack()[0][3]+("(): Out of size limits: size is " + str(size/1e3) + " whereas limits are " + str(low) +" and " + str(high))
            raise WarningE(error)

        
    def check_column_range(self, *column_range_limits):
        """
        This function checks the range of the given column

        :param column_range_limits: dictionary with columnname as key and tuple of (lower bound, upper bound) as value
        :return: Nothing if the test passes. If no arguments are passed, by default, it returns a dictionary with column name as key and their range (lower bound, upper bound) in tuple as value
        :raises: Throws warning if any value lies outside the range
        """

        data = self.data[~self.data.isin([np.nan, np.inf, -np.inf]).any(1)]
        if len(column_range_limits) != 0:
            for col in column_range_limits[0]:
                low = data[col].min()
                high = data[col].max()
                if ((low < column_range_limits[0][col][0]) or (high > column_range_limits[0][col][1])):
                    error = inspect.stack()[0][3]+"(): Out of range limits"
                    raise WarningE(error)
        else:
            Numeric_columns_range = {}
            Numeric_columns = self.check_numeric()
            for col in Numeric_columns:
                Numeric_columns_range[col] = (data[col].min(), data[col].max())
            return Numeric_columns_range
    def check_primarykey_duplicates(self, column):
        """
        This function returns the duplicates in a column

        :param column: str, name of the column
        :return: Duplicates in the given column
        """

        return self.data.set_index(column).index.get_duplicates()
    
    
    def check_distinct_values(self, *column):
        """
        This function returns the distinct values in a column

        :param column: str, name of the column
        :return: Distinct values in the given column. If no input is given, it returns the distinct values for all the columns in a dictionary.
        """
        cols_unique_values = {}
        if len(column)!=0:
            cols_unique_values[column[0]] =  self.data[column[0]].unique()
        else:
            for col in list(self.data):
                cols_unique_values[col]=self.data[col].unique()
            return cols_unique_values
                

            
    
    def check_categorical_levels(self, column, levels):
        """
        This function verifies the unique values of the categorical variable

        :param column: column name as string
        :param levels: list of unique values in the categorical feature
        :return: Nothing if the test passes
        :raises: FatalError if the levels given donot match with the column's levels
        """

        if not set(self.data[column])== set(levels):
            error = inspect.stack()[0][3] + "(): "+column+" : Mismatch in the levels"
            raise FatalError(error)
        
    def check_numeric(self, *column):
        """
        This function checks if the given column in dataframe is numeric

        :param column: str, name of the column
        :return: Nothing if the test passes. If no arguments are passed, by default, it returns the list of all numeric columns
        :raises: FatalError if column data type is not numeric
        """
        
        if len(column)!=0:
            for col in column:
                if not np.issubdtype(self.data[col], np.number):
                    error = inspect.stack()[0][3] + "(): "+col+" is not numeric"
                    raise FatalError(error)
        else:
            Numeric_columns=[col for col in list(self.data) if np.issubdtype(self.data[col], np.number) ]
            return Numeric_columns
            
              
    
    def outliers_z_score(self, column, threshold):
        """
        This function gives the outliers using the z-score method

        :param column: str, name of the column
        :param threshold: float, threshold of std dev away from mean to be considered as outliers
        :return: numpy.ndarray, Outliers in that particular dataset
        :raises: FatalError if column does not exist in the dataframe
        """

        self.check_columns_exists(column)
        self.check_numeric(column)
        
        
        mean_y = np.mean(self.data[column])
        stdev_y = np.std(self.data[column])
        z_scores = [(y - mean_y) / stdev_y for y in self.data[column]]
        return np.where(np.abs(z_scores) > threshold)[0]
    
    def outliers_modified_z_score(self, column, threshold):
        """
        This function gives the outliers using the modified z-score method

        :param column: str, name of the column
        :param threshold: float, threshold of MAD(mean absolute deviation) away from median to be considered as outliers
        :return: numpy.ndarray, Outliers in that particular dataset
        :raises: FatalError if column does not exist in dataframe
        """

        self.check_columns_exists( column)
        self.check_numeric( column)
        
        median_y = np.nanmedian(self.data[column])
        median_absolute_deviation_y = np.nanmedian([np.abs(y - median_y) for y in self.data[column]])
        modified_z_scores = [0.6745 * (y - median_y) / median_absolute_deviation_y for y in self.data[column]]
        return np.where(np.abs(modified_z_scores) > threshold)[0]
    
    def outliers_iqr(self, column, lower_cutoff, iqr_factor=1.5):
        """
        This function gives the outliers using the IQR(inter quartile range) method

        :param column: str, name of the column
        :param lower_cutoff: float, lower percentile to be considered outliers (example=25)
        :param iqr_factor: Inter quartile range, default is 1.5
        :return: numpy.ndarray, Outliers in that particular dataset
        :raises: FatalError if column does not exist in dataframe
        """

        self.check_columns_exists( column)
        self.check_numeric( column)
        
        quartile_1, quartile_3 = np.nanpercentile(self.data[column], [lower_cutoff, 100-lower_cutoff])
        iqr = quartile_3 - quartile_1
        lower_bound = quartile_1 - (iqr * iqr_factor)
        upper_bound = quartile_3 + (iqr * iqr_factor)
        return np.where((self.data[column] > upper_bound) | (self.data[column] < lower_bound))[0]
    
    def check_periodicity(self, column, show_plot=False):
        """
        This function returns the difference between ith timestamp and (i-1)th timestamp. Return an array of length n-1 for n observations in the datetime column

        :param column: str, name of the column
        :return: time_diff_array: Array with n-1 values (timestamp differences between i and (i-1)) and frequency as string (example: 'weeks')
        """

        array= self.data[column].unique()
        time_diff_array=[(np.datetime64(array[i+1])-np.datetime64(array[i])) for i in range(len(array)-1) ]
        max_days=max([time_diff_array[i] / np.timedelta64(1, 'D') for i in range(len(time_diff_array))])
        max_secs= max([time_diff_array[i] / np.timedelta64(1, 's') for i in range(len(time_diff_array))])
        if max_days>=7:
            time_diff_output=[(time_diff_array[i]/ np.timedelta64(1, 'D'))/7 for i in range(len(time_diff_array))]
            freq='weeks'
        elif 1<=max_days<7:
            time_diff_output=[time_diff_array[i]/ np.timedelta64(1, 'D') for i in range(len(time_diff_array))]
            freq='days'
        else:
            if max_secs/3600>=1:
                time_diff_output=[(time_diff_array[i]/ np.timedelta64(1, 's'))/3600 for i in range(len(time_diff_array))]
                freq='hours'
                
            elif 60<=max_secs<3600:
                time_diff_output=[(time_diff_array[i]/ np.timedelta64(1, 's'))/60 for i in range(len(time_diff_array))]
                freq='minutes'
            else:
                time_diff_output=[(time_diff_array[i]/ np.timedelta64(1, 's')) for i in range(len(time_diff_array))]
                freq='seconds'
        if show_plot:
            self.plot_hist(time_diff_output, freq)

        return time_diff_array, freq

    def plot_hist(self, time_diff_output, freq):
        plt.hist(time_diff_output)
        plt.title("Histogram of Periodicity")
        plt.xlabel(freq)
        plt.ylabel("Frequency")
        plt.show()







                
            
            
            
            
        
        
        
 