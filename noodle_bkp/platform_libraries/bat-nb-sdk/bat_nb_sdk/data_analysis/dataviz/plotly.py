import numpy as np
import pandas as pd
from plotly.graph_objs import *
from sklearn.preprocessing import scale
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import summary_table
import plotly.figure_factory as FF
import plotly
import seaborn as sns

sns.set(color_codes=True)
from statsmodels.tsa.seasonal import seasonal_decompose


class Plotly_library(object):

    def __init__(self, data):
        self.data = data
        self.numerical_columns = [col for col, s in data.iteritems() if self.get_vartype(s) == 'NUM']
        self.categorical_columns = [col for col, s in data.iteritems() if self.get_vartype(s) == 'CAT']

    @staticmethod
    def _fmt_percent(v):
        return "{:2.1f}%".format(v * 100)

    def get_vartype(self, data):
        """Returns the variable type for the given data series

        :param data: pandas series
        :return: returns the variable type
        :rtype: basestring
        """
        distinct_count = data.nunique(dropna=False)
        leng = len(data)
        if distinct_count <= 1:
            return 'CONST'
        elif pd.api.types.is_numeric_dtype(data):
            return 'NUM'
        elif pd.api.types.is_datetime64_dtype(data):
            return 'DATE'
        elif distinct_count == leng:
            return 'UNIQUE'
        else:
            return 'CAT'

    def plot_histogram_plotly(self, column_name=None):
        """Plots histogram given a column name. If no column name is given, it will randomly pick a column name

        It is useful for univariate analysis on numerical variables

        :param column_name:
        :return: None
        """
        if (column_name == None):
            column_name = np.random.choice(self.numerical_columns, 1)[0]

        if not isinstance(column_name, list):
            column_name = [column_name]

        return_list = []
        for each_column in column_name:
            series = self.data[each_column]
            trace = Histogram(
                x=series.values,
                marker=dict(color='#fd95b8'),
                opacity=0.75)

            data = Data([trace])

            layout = Layout(
                title=series.name,
                titlefont=dict(size=20),
                xaxis=XAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title=series.name
                ),
                yaxis=YAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title='Frequency',
                ),
                bargap=0.15
            )
            fig = Figure(data=data, layout=layout)
            return_list.append(fig)
        return return_list

    def plot_frequency_plotly(self, column_name=None, top_k=10):
        """
        Plots frequency given a column name. If no column name is given, it will randomly pick a column name

        It is useful for univariate analysis on categorical variables

        :param column_name:
        :param top_k:
        :return:
        """
        return_list = []
        if (column_name == None):
            column_name = np.random.choice(self.categorical_columns, 1)[0]

        if not isinstance(column_name, list):
            column_name = [column_name]

        for each_column in column_name:
            series = self.data[each_column]

            objcounts = series.value_counts()
            top_k_objcounts = objcounts.head(top_k)
            x = top_k_objcounts.values.tolist()[::-1]
            y = top_k_objcounts.index.tolist()[::-1]
            sum_x = sum(x)
            tick_val_y = []

            len_series = len(series)
            x_text = [self._fmt_percent(each_x / len_series) for each_x in x]

            n_missing = len(series) - series.count()
            all_traces = []

            if n_missing != 0:
                missing_y = '(Missing)'
                missing_x = n_missing

                trace3 = Bar(
                    x=[missing_x],
                    y=[missing_y],
                    text=[self._fmt_percent(n_missing / len_series)],
                    orientation='h',
                    marker=dict(color='#7D022C'),
                    opacity=0.75,
                    showlegend=False,
                    hoverinfo="x+text"
                )
                all_traces.append(trace3)
                tick_val_y.append(missing_y)

            if objcounts.size > top_k:
                other_y = 'Other values(' + str(objcounts.size - top_k) + ')'
                other_x = (series.count() - sum_x)

                trace2 = Bar(
                    x=[other_x],
                    y=[other_y],
                    text=[self._fmt_percent(other_x / len_series)],
                    orientation='h',
                    marker=dict(color='#98a8a5'),
                    opacity=0.75,
                    showlegend=False,
                    hoverinfo="x+text"
                )
                all_traces.append(trace2)
                tick_val_y.append(other_y)

            trace1 = Bar(
                x=x,
                y=y,
                text=x_text,
                orientation='h',
                marker=dict(color='#2164A5'),
                opacity=0.75,
                showlegend=False,
                hoverinfo="x+text"
            )
            all_traces.append(trace1)
            tick_val_y += y

            data = Data(all_traces)

            layout = Layout(
                title=each_column,
                titlefont=dict(
                    size=20
                ),
                xaxis=XAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title=series.name
                ),

                yaxis=YAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    tickvals=tick_val_y,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    tickangle='-35',
                    title='Value'
                ),
                margin=dict(l=180),
                bargap=0.15
            )
            fig = Figure(data=data, layout=layout)
            return_list.append(fig)
        return return_list

    def plot_distribution_plotly(self, numerical_columns=None):
        """ Generates distribution plot given a numerical column.

        If no column is given, it will pick a column randomly
        :param numerical_columns:
        :return:
        """
        if numerical_columns == None:
            numerical_columns = np.random.choice(self.numerical_columns, 1)

        if len(numerical_columns) == 0:
            print('Not enough columns')
            return

        fig = FF.create_distplot([self.data[numerical_column].dropna() for numerical_column in numerical_columns],
                                 [numerical_column for numerical_column in numerical_columns],
                                 show_hist=False,
                                 show_rug=False)
        for d in fig['data']:
            d.update({'fill': 'tozeroy'})

        fig.layout.xaxis.title = ' vs '.join(numerical_columns)
        return fig

    def plot_stacked_histogram(self, numerical_column=None, categorical_column=None, max_bins=100, top_k=10):
        """Plots stacked histogram between numerical column and categorical column

        If no numerical column/ categorical columns are given, it will randomly generate column

        :param numerical_column:
        :param categorical_column:
        :param max_bins:
        :param top_k:
        :return:
        """
        return_list = []
        if (numerical_column == None):
            numerical_column = np.random.choice(self.numerical_columns, 1)[0]

        if (categorical_column == None):
            categorical_column = np.random.choice(self.categorical_columns, 1)[0]

        if not isinstance(numerical_column, list):
            numerical_column = [numerical_column]

        if not isinstance(categorical_column, list):
            categorical_column = [categorical_column]

        for each_numerical in numerical_column:
            for each_categorical in categorical_column:

                df_subset = self.data[[each_numerical, each_categorical]].copy(deep=True)
                df_subset = df_subset.dropna()
                numerical_value_counts_list = self.data[each_numerical].value_counts().index.tolist()
                categorical_value_counts_list = self.data[each_categorical].value_counts().index.tolist()

                if len(categorical_value_counts_list) < top_k:
                    top_k = len(categorical_value_counts_list)

                categorical_value_counts_list = categorical_value_counts_list[:top_k]

                all_traces = []
                if len(numerical_value_counts_list) < max_bins:
                    max_bins = len(numerical_value_counts_list)

                for i in range(len(categorical_value_counts_list)):
                    trace = Histogram(
                        x=df_subset[df_subset[each_categorical] == categorical_value_counts_list[i]][
                            each_numerical].values,
                        nbinsx=max_bins,
                        name=categorical_value_counts_list[i],
                        opacity=0.75
                    )
                    all_traces.append(trace)
                data = Data(all_traces)
                layout = Layout(
                    title=each_numerical + ' vs ' + each_categorical,
                    titlefont=dict(size=20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_numerical
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title='Frequency',
                    ),
                    bargap=0.15,
                    barmode='stack'
                )
                fig = Figure(data=data, layout=layout)
                return_list.append(fig)
        return return_list

    def plotly_heatmap(self, categorical_column_1, categorical_column_2):
        """
        Plots heatmap between two categorical columns. If not given, it will randomly pick 2 categorical columns

        :param categorical_column_1:
        :param categorical_column_2:
        :return:
        """
        return_list = []
        if (categorical_column_1 == None):
            categorical_column_1 = np.random.choice(self.categorical_columns, 1)[0]

        if (categorical_column_2 == None):
            categorical_column_2 = np.random.choice(self.categorical_columns, 1)[0]
            while (categorical_column_2 == categorical_column_1):  # If both are the same column
                categorical_column_2 = np.random.choice(self.categorical_columns, 1)[0]

        if not isinstance(categorical_column_1, list):
            categorical_column_1 = [categorical_column_1]

        if not isinstance(categorical_column_2, list):
            categorical_column_2 = [categorical_column_2]

        for each_categorical_1 in categorical_column_1:
            for each_categorical_2 in categorical_column_2:
                cross_df = pd.crosstab(self.data[each_categorical_1], self.data[each_categorical_2])
                cross_list = cross_df.values.tolist()

                trace = Heatmap(
                    z=cross_list,
                    x=cross_df.columns.values.tolist(),
                    y=cross_df.index.values.tolist(),
                    colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"],
                                [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"],
                                [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"],
                                [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
                )

                data = Data([trace])
                layout = Layout(
                    autosize=True,
                    margin=dict(r=240, t=30, b=180, l=180, pad=2),
                    showlegend=False,
                    title=each_categorical_1 + ' vs ' + each_categorical_2,
                    titlefont=dict(size=20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_categorical_2
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_categorical_1
                    )
                )
                fig = Figure(data=data, layout=layout)
                return_list.append(fig)
        return return_list

    def plot_correlation(self, zmin=-1, zmax=1):
        """
        Plots correlation heatmap across all numerical variables

        :param zmin: -1
        :param zmax: +1
        :return: None
        """
        corr_df = self.data.corr()
        corr_df.dropna(how="all", axis=0, inplace=True)
        corr_df.dropna(how="all", axis=1, inplace=True)
        corr_df.fillna(value=0, inplace=True)

        # Replace all 1s with 0s along the diagonal
        for row in range(len(corr_df)):
            corr_df.iat[row, row] = 0

        corr_list = corr_df.values.tolist()
        if len(corr_list) == 0:
            print('Not enough values')
            return

        trace = Heatmap(
            z=corr_list,
            x=corr_df.index.values.tolist(),
            y=corr_df.index.values.tolist(),
            colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"],
                        [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"],
                        [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"],
                        [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
            zmax=zmax,
            zmin=zmin
        )

        data = Data([trace])
        layout = Layout(
            autosize=True,
            margin=dict(r=240, t=30, b=180, l=180, pad=2),
            showlegend=False,
            xaxis=XAxis(
                autorange=True,
                linecolor='rgb(207, 226, 243)',
                linewidth=8,
                mirror=True,
                nticks=corr_df.shape[0],
                range=[0, corr_df.shape[0] + 1],
                showline=True,
                title="",
                tickangle='40'
            ),
            yaxis=YAxis(
                autorange=True,
                linecolor='rgb(207, 226, 243)',
                linewidth=8,
                mirror=True,
                nticks=corr_df.shape[0],
                range=[0, corr_df.shape[0] + 1],
                showline=True,
                title='',
                tickangle='-30'
            )
        )
        fig = Figure(data=data, layout=layout)
        return fig

    def plot_box(self):
        """
        Generates box-plot for all numerical columns

        :return:
        """
        if len(self.numerical_columns) == 0:
            print('Not enough numerical columns')
            return

        c = ['hsl(' + str(h) + ',50%' + ',50%)' for h in np.linspace(0, 360, len(self.numerical_columns))]
        all_traces = [
            Box(
                y=scale(self.data[self.numerical_columns[i]].dropna(), axis=0, with_mean=True, with_std=True),
                boxmean='sd',
                marker=dict(color=c[i]),
                name=self.numerical_columns[i])
            for i in range(len(self.numerical_columns))]

        data = Data(all_traces)
        layout = Layout(
            title='Box Plot',
            titlefont=dict(
                size=20
            ),
            xaxis=XAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=False,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False
            ),

            yaxis=YAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=True,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False
            )
        )
        fig = Figure(data=data, layout=layout)
        return fig
    
    def plotly_all_histogram_plot(self, ncols=4):
        """
        If you're lazy generating one histogram at once, this will generate all histograms at once!

        :param ncols: Minimum number of numerical columns to use this function.
        :return:

        """
        if not self.numerical_columns:
            raise ValueError('No numerical columns available')
            
        numerical_cols = np.array(self.numerical_columns)

        n_temp = np.ceil(len(numerical_cols)/ncols)
        numerical_cols = np.append(numerical_cols,([None] * ((int(n_temp)*ncols) - len(numerical_cols))))

        c = np.array(['hsl(' + str(h) + ',50%' + ',50%)' for h in np.linspace(0, 360, len(numerical_cols))])
        numerical_cols = numerical_cols.reshape(-1, ncols)
        c = c.reshape(-1, ncols)
        nrows = len(numerical_cols)
        count = 1
        fig = plotly.tools.make_subplots(rows=nrows, cols=ncols,print_grid=False)
        for i in range(nrows):
            for j in range(ncols):
                if numerical_cols[i][j] is not None:
                    x = self.data[numerical_cols[i][j]].values
                else:
                    x = []
                trace = Histogram(
                    x=x,
                    name=numerical_cols[i][j],
                    marker=dict(color=c[i][j])
                )
                fig.append_trace(trace, i + 1, j + 1)
        
                if numerical_cols[i][j] is None:
                    str_temp = 'yaxis' + str(count)                       
                    fig['layout'][str_temp].update(showticklabels=False,showgrid=False,showline=False,zeroline=False)
                    str_temp = 'xaxis' + str(count)
                    fig['layout'][str_temp].update(showticklabels=False,showgrid=False,showline=False,zeroline=False)
                count += 1 
        
        fig['layout'].update(height=nrows * 300, width=ncols * 300, title='Histogram')
        return fig

    def plotly_all_frequency_plot(self, ncols=4):
        """
        If you're lazy generating one frequency at once, this will generate all frequency plots at once!

        :param ncols: Minimum number of numerical columns to use this function.
        :return:
        """
        if not self.categorical_columns:
            raise ValueError('No categorical columns available')
            
        categorical_cols = np.array(self.categorical_columns)
        
        n_temp = np.ceil(len(categorical_cols)/ncols)
        categorical_cols = np.append(categorical_cols,([None] * ((int(n_temp)*ncols) - len(categorical_cols))))
    
        c = np.array(['hsl(' + str(h) + ',50%' + ',50%)' for h in np.linspace(0, 360, len(categorical_cols))])
        categorical_cols = categorical_cols.reshape(-1, ncols)
        c = c.reshape(-1, ncols)

        nrows = len(categorical_cols)

        fig = plotly.tools.make_subplots(rows=nrows, cols=ncols,print_grid=False)
        count = 1

        for i in range(nrows):
            for j in range(ncols):
                if categorical_cols[i][j] is not None:
                    series = self.data[categorical_cols[i][j]]
                    objcounts = series.value_counts()
                    x = objcounts.values.tolist()[::-1]
                    y = objcounts.index.tolist()[::-1]
                else :
                    x = []
                    y = []
                trace = Bar(
                    x=x,
                    y=y,
                    orientation='h',
                    opacity=0.75,
                    name=categorical_cols[i][j],
                    marker=dict(color=c[i][j])
                )
                fig.append_trace(trace, i + 1, j + 1)
                str_temp = 'yaxis' + str(count)
                fig['layout'][str_temp].update(showticklabels=False)
                
                if categorical_cols[i][j] is None:
                    fig['layout'][str_temp].update(showgrid=False,showline=False,zeroline=False)
                    str_temp = 'xaxis' + str(count)
                    fig['layout'][str_temp].update(showticklabels=False,showgrid=False,showline=False,zeroline=False)
                count += 1   
            
        fig['layout'].update(height=nrows * 300, width=ncols * 300, title='Frequency')
        return fig

    def plotly_scatter(self, numerical_column_1=None, numerical_column_2=None):
        """
        This generates a scatter plot between two numerical columns. If not given, it will randomly pick columns

        :param numerical_column_1:
        :param numerical_column_2:
        :return:
        """
        return_plots = []
        if (numerical_column_1 == None):
            numerical_column_1 = np.random.choice(self.numerical_columns, 1)[0]

        if (numerical_column_2 == None):
            numerical_column_2 = np.random.choice(self.numerical_columns, 1)[0]
            while (numerical_column_2 == numerical_column_1):  # If both are the same column
                numerical_column_2 = np.random.choice(self.numerical_columns, 1)[0]

        if not isinstance(numerical_column_1, list):
            numerical_column_1 = [numerical_column_1]

        if not isinstance(numerical_column_2, list):
            numerical_column_2 = [numerical_column_2]

        for each_numerical_1 in numerical_column_1:
            for each_numerical_2 in numerical_column_2:
                X = sm.add_constant(self.data[each_numerical_1])
                res = sm.OLS(self.data[each_numerical_2], X).fit()

                st, data, ss2 = summary_table(res, alpha=0.05)
                preds = pd.DataFrame.from_records(data, columns=[s.replace('\n', ' ') for s in ss2])
                preds['displ'] = self.data[each_numerical_1]
                preds = preds.sort_values(by='displ')

                trace0 = Scattergl(
                    x=self.data[each_numerical_1].dropna(),
                    y=self.data[each_numerical_2].dropna(),
                    mode='markers',
                    name='Points'
                )

                trace1 = Scattergl(
                    x=preds['displ'],
                    y=preds['Predicted Value'],
                    mode='line',
                    name='Regression'
                )

                trace2 = Scattergl(
                    x=preds['displ'],
                    y=preds['Mean ci 95% low'],
                    name='Lower 95% CI',
                    showlegend=False,
                    line=dict(color='transparent'),
                    mode='line'
                )
                trace3 = Scattergl(
                    x=preds['displ'],
                    y=preds['Mean ci 95% upp'],
                    name='95 % CI',
                    fill='tonexty',
                    mode='line',
                    line=dict(color='transparent'),
                    fillcolor='rgba(255, 127, 14, 0.3)'
                )

                data = Data([trace0, trace1, trace2, trace3])
                layout = Layout(
                    title=each_numerical_1 + ' vs ' + each_numerical_2,
                    titlefont=dict(size=20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_numerical_1
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_numerical_2
                    )
                )
                fig = Figure(data=data, layout=layout)
                return_plots.append(fig)
        return return_plots

    def plotly_ts(self, time_column, target_column, sort=False):
        """
        Plots a time series, with time column on x-axis and target column on y-axis

        :param time_column:
        :param target_column:
        :param sort: If target column is not sorted, send false (default). If it needs to be sorted, send true
        :return:
        """
        if sort:
            df = self.data.sort_values(by=time_column)
        else:
            df = self.data

        trace = Scatter(
            x=df[time_column],
            y=df[target_column],
            line=Line(color='rgb(19,48,84)', width=3),
            mode='lines'
        )
        data = Data([trace])
        layout = Layout(
            title='Timeseries',
            titlefont=dict(
                size=25
            ),
            xaxis=XAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=True,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False,
                title=time_column,
                titlefont=dict(size=19),
                tickangle='35'
            ),

            yaxis=YAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=True,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False,
                title=target_column,
                titlefont=dict(size=19),
                tickfont=dict(
                    size=15
                )
            ),
            legend=dict(
                font=dict(
                    size=13,
                )
            )
        )
        fig = Figure(data=data, layout=layout)
        return fig

    def plot_decomposition(self, time_column, target_column, freq=7):
        """Decomposes a time series into Trend, Seasonality and Residual (STL)

        :param time_column:
        :param target_column:
        :param freq: Number of days the data is repeating
        :return:
        """
        temp_df = pd.DataFrame(data=self.data[target_column].values, index=pd.to_datetime(self.data[time_column]),
                               columns=[target_column])
        result = seasonal_decompose(temp_df[target_column], freq=freq)
        trace1 = Scatter(
            x=result.observed.index.strftime('%Y/%m/%d'),
            y=result.observed.values,
            name='Observed',
            mode='lines',
            xaxis='x1',
            yaxis='y1'
        )
        trace2 = Scatter(
            x=result.trend.index.strftime('%Y/%m/%d'),
            y=result.trend.values,
            name='Trend',
            mode='lines',
            xaxis='x2',
            yaxis='y2'
        )
        trace3 = Scatter(
            x=result.seasonal.index.strftime('%Y/%m/%d'),
            y=result.seasonal.values,
            name='Seasonal',
            mode='lines',
            xaxis='x3',
            yaxis='y3'
        )
        trace4 = Scatter(
            x=result.resid.index.strftime('%Y/%m/%d'),
            y=result.resid.values,
            name='Residual',
            mode='lines',
            xaxis='x4',
            yaxis='y4'
        )
        data = Data([trace1, trace2, trace3, trace4])
        layout = {
            "height": 1100,
            "title": "Decomposed Timeseries",
            "width": 1000,
            "xaxis1": {
                "anchor": "y1",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "xaxis2": {
                "anchor": "y2",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "xaxis3": {
                "anchor": "y3",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "xaxis4": {
                "anchor": "y4",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "yaxis1": {
                "anchor": "x1",
                "domain": [0.85, 1.0]
            },
            "yaxis2": {
                "anchor": "x2",
                "domain": [0.6, 0.75]
            },
            "yaxis3": {
                "anchor": "x3",
                "domain": [0.30, 0.45]
            },
            "yaxis4": {
                "anchor": "x4",
                "domain": [0.05, 0.20]
            }
        }

        fig = Figure(data=data, layout=layout)
        return fig
