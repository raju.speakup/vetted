import numpy as np
import pandas as pd
from plotly.graph_objs import *
import plotly
from sklearn.preprocessing import scale
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import summary_table
import plotly.figure_factory as FF
import networkx as nx
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose
import scipy.stats as stats
from pandas.plotting import autocorrelation_plot
from statsmodels.graphics.tsaplots import plot_pacf
import ipywidgets
import itertools
from tabulate import tabulate
from IPython.core.display import display
from prettytable import PrettyTable
import collections


class DataAnalysis(object):
    """The main data analysis object

    The DataAnalysed object is formed from the pandas data frame
    and we perform various EDA by invoking methods. The results
    are stored in object variables after a method finishes.
    The architecture is similar to sklearn.

    :param data: The pandas data frame
    :return: :class:`DataAnalysis` object

    Usage::

        >>> data_file = "edw_cdr.csv"
        >>> data = pd.read_csv(data_file)
        >>> data_analysed = DataAnalysis(data)

    """

    def __init__(self, data):
        self.data = data
        self.numerical_columns = [col for col, s in data.iteritems() if self.get_vartype(s) == 'NUM']
        self.categorical_columns = [col for col, s in data.iteritems() if self.get_vartype(s) == 'CAT']

    @staticmethod
    def _fmt_percent(v):
        return "{:2.1f}%".format(v * 100)

    @staticmethod
    def _fmt_float(v):
        return str(float('{:.5g}'.format(v))).rstrip('0').rstrip('.')

    @staticmethod
    def fmt_bytesize(num, suffix='B'):
        for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
            if abs(num) < 1024.0:
                return "%3.1f %s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f %s%s" % (num, 'Yi', suffix)

    def get_vartype(self, data):
        """Returns the variable type for the given data series

        :param data: pandas series
        :return: returns the variable type
        :rtype: basestring
        """
        distinct_count = data.nunique(dropna=False)
        leng = len(data)
        if distinct_count <= 1:
            return 'CONST'
        elif pd.api.types.is_numeric_dtype(data):
            return 'NUM'
        elif pd.api.types.is_datetime64_dtype(data):
            return 'DATE'
        elif distinct_count == leng:
            return 'UNIQUE'
        else:
            return 'CAT'

    def plot_histogram(self,column_name = None):
        """Plots histogram given a column name. If no column name is given, it will randomly pick a column name

        It is useful for univariate analysis on numerical variables

        :param column_name:
        :return: None
        """
        if (column_name == None):
            column_name = np.random.choice(self.numerical_columns, 1)[0]

        if not isinstance(column_name, list):
            column_name = [column_name]

        for each_column in column_name:

            series = self.data[each_column]
            trace = Histogram(
                x = series.values,
                marker=dict(color='#fd95b8'),
                opacity=0.75)

            data = Data([trace])

            layout = Layout(
                title = 'Histogram',
                titlefont = dict(size = 20),
                xaxis=XAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth= 0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title = series.name
                ),
                yaxis=YAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth= 0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title='Frequency',
                ),
                bargap=0.15
            )
            fig = Figure(data=data, layout=layout)
            plotly.offline.iplot(fig)

    def plot_frequency(self, column_name = None, top_k=10):
        """
        Plots frequency given a column name. If no column name is given, it will randomly pick a column name

        It is useful for univariate analysis on categorical variables

        :param column_name:
        :param top_k:
        :return:
        """
        if (column_name == None):
            column_name = np.random.choice(self.categorical_columns, 1)[0]

        if not isinstance(column_name, list):
            column_name = [column_name]

        for each_column in column_name:
            series = self.data[each_column]

            objcounts = series.value_counts()
            top_k_objcounts = objcounts.head(top_k)
            x = top_k_objcounts.values.tolist()[::-1]
            y = top_k_objcounts.index.tolist()[::-1]
            sum_x = sum(x)
            tick_val_y = []

            len_series = len(series)
            x_text = [self._fmt_percent(each_x / len_series) for each_x in x]

            n_missing = len(series) - series.count()
            all_traces = []

            if n_missing != 0:
                missing_y = '(Missing)'
                missing_x = n_missing

                trace3 = Bar(
                    x=[missing_x],
                    y=[missing_y],
                    text=[self._fmt_percent(n_missing / len_series)],
                    orientation='h',
                    marker=dict(color='#7D022C'),
                    opacity=0.75,
                    showlegend=False,
                    hoverinfo="x+text"
                )
                all_traces.append(trace3)
                tick_val_y.append(missing_y)

            if objcounts.size > top_k:
                other_y = 'Other values(' + str(objcounts.size - top_k) + ')'
                other_x = (series.count() - sum_x)

                trace2 = Bar(
                    x=[other_x],
                    y=[other_y],
                    text=[self._fmt_percent(other_x / len_series)],
                    orientation='h',
                    marker=dict(color='#98a8a5'),
                    opacity=0.75,
                    showlegend=False,
                    hoverinfo="x+text"
                )
                all_traces.append(trace2)
                tick_val_y.append(other_y)

            trace1 = Bar(
                x=x,
                y=y,
                text=x_text,
                orientation='h',
                marker=dict(color='#2164A5'),
                opacity=0.75,
                showlegend=False,
                hoverinfo="x+text"
            )
            all_traces.append(trace1)
            tick_val_y += y

            data = Data(all_traces)

            layout = Layout(
                title= each_column,
                titlefont=dict(
                    size=20
                ),
                xaxis=XAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title='Count'
                ),

                yaxis=YAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    tickvals=tick_val_y,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    tickangle='-35',
                    title='Value'
                ),
                margin=dict(l=180),
                bargap=0.15
            )
            fig = Figure(data=data, layout=layout)
            plotly.offline.iplot(fig)

    def plot_correlation(self, zmin=-1, zmax=1):
        """
        Plots correlation heatmap across all numerical variables

        :param zmin: -1
        :param zmax: +1
        :return: None
        """
        corr_df = self.data.corr()
        corr_df.dropna(how = "all",axis = 0,inplace = True)
        corr_df.dropna(how = "all",axis = 1,inplace = True)
        corr_df.fillna(value = 0,inplace = True)

        # Replace all 1s with 0s along the diagonal
        for row in range(len(corr_df)):
            corr_df.iat[row, row] = 0

        corr_list = corr_df.values.tolist()
        if len(corr_list) == 0:
            print('Not enough values')
            return

        trace = Heatmap(
            z = corr_list,
            x = corr_df.index.values.tolist(),
            y = corr_df.index.values.tolist(),
            colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"], [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"], [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"], [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
            zmax = zmax,
            zmin = zmin
        )

        data = Data([trace])
        layout = Layout(
            autosize = True,
            margin = dict(r=240,t=30,b=180,l=180,pad=2),
            showlegend = False,
            xaxis = XAxis(
                autorange=True,
                linecolor='rgb(207, 226, 243)',
                linewidth= 8,
                mirror= True,
                nticks = corr_df.shape[0],
                range = [0, corr_df.shape[0]+1],
                showline= True,
                title= "",
                tickangle = '40'
            ),
            yaxis = YAxis(
                autorange= True,
                linecolor='rgb(207, 226, 243)',
                linewidth= 8,
                mirror = True,
                nticks = corr_df.shape[0],
                range = [0, corr_df.shape[0]+1],
                showline= True,
                title = '',
                tickangle = '-30'
            )
        )
        fig = Figure(data=data, layout=layout)
        plotly.offline.iplot(fig)

    def plot_stacked_histogram(self, numerical_column = None, categorical_column = None, max_bins=100, top_k=10):
        """Plots stacked histogram between numerical column and categorical column

        If no numerical column/ categorical columns are given, it will randomly generate column

        :param numerical_column:
        :param categorical_column:
        :param max_bins:
        :param top_k:
        :return:
        """
        if(numerical_column == None):
            numerical_column = np.random.choice(self.numerical_columns, 1)[0]

        if(categorical_column == None):
            categorical_column = np.random.choice(self.categorical_columns, 1)[0]

        if not isinstance(numerical_column, list):
            numerical_column = [numerical_column]

        if not isinstance(categorical_column, list):
            categorical_column = [categorical_column]

        for each_numerical in numerical_column:
            for each_categorical in categorical_column:

                df_subset = self.data[[each_numerical, each_categorical]].copy(deep=True)
                df_subset = df_subset.dropna()
                numerical_value_counts_list = self.data[each_numerical].value_counts().index.tolist()
                categorical_value_counts_list = self.data[each_categorical].value_counts().index.tolist()

                if len(categorical_value_counts_list) < top_k:
                    top_k = len(categorical_value_counts_list)

                categorical_value_counts_list = categorical_value_counts_list[:top_k]

                all_traces = []
                if len(numerical_value_counts_list) < max_bins:
                    max_bins = len(numerical_value_counts_list)

                for i in range(len(categorical_value_counts_list)):
                    trace = Histogram(
                        x=df_subset[df_subset[each_categorical] == categorical_value_counts_list[i]][each_numerical].values,
                        nbinsx=max_bins,
                        name=categorical_value_counts_list[i],
                        opacity=0.75
                    )
                    all_traces.append(trace)
                data = Data(all_traces)
                layout = Layout(
                    title=each_numerical + ' vs ' + each_categorical,
                    titlefont=dict(size=20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_numerical
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth=0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title='Frequency',
                    ),
                    bargap=0.15,
                    barmode='stack'
                )
                fig = Figure(data=data, layout=layout)
                plotly.offline.iplot(fig)

    def plot_heatmap(self,categorical_column_1 = None,categorical_column_2 = None):
        """
        Plots heatmap between two categorical columns. If not given, it will randomly pick 2 categorical columns

        :param categorical_column_1:
        :param categorical_column_2:
        :return:
        """
        if(categorical_column_1 == None):
            categorical_column_1 = np.random.choice(self.categorical_columns, 1)[0]

        if(categorical_column_2 == None):
            categorical_column_2 = np.random.choice(self.categorical_columns, 1)[0]
            while (categorical_column_2 == categorical_column_1): # If both are the same column
                categorical_column_2 = np.random.choice(self.categorical_columns, 1)[0]

        if not isinstance(categorical_column_1, list):
            categorical_column_1 = [categorical_column_1]

        if not isinstance(categorical_column_2, list):
            categorical_column_2 = [categorical_column_2]

        for each_categorical_1 in categorical_column_1:
            for each_categorical_2 in categorical_column_2:

                cross_df = pd.crosstab(self.data[each_categorical_1],self.data[each_categorical_2])
                cross_list = cross_df.values.tolist()

                trace = Heatmap(
                    z = cross_list,
                    x = cross_df.columns.values.tolist(),
                    y = cross_df.index.values.tolist(),
                    colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"], [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"], [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"], [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
                )

                data = Data([trace])
                layout = Layout(
                    autosize = True,
                    margin = dict(r=240,t=30,b=180,l=180,pad=2),
                    showlegend = False,
                    title = each_categorical_1 + ' vs ' + each_categorical_2,
                    titlefont = dict(size = 20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth= 0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title = each_categorical_2
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth= 0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_categorical_1
                    )
                )
                fig = Figure(data=data, layout=layout)
                plotly.offline.iplot(fig)

    def plot_scatter(self,numerical_column_1 = None,numerical_column_2 = None):
        """
        This generates a scatter plot between two numerical columns. If not given, it will randomly pick columns

        :param numerical_column_1:
        :param numerical_column_2:
        :return:
        """
        if (numerical_column_1 == None):
            numerical_column_1 = np.random.choice(self.numerical_columns, 1)[0]

        if(numerical_column_2 == None):
            numerical_column_2 = np.random.choice(self.numerical_columns, 1)[0]
            while (numerical_column_2 == numerical_column_1): # If both are the same column
                numerical_column_2 = np.random.choice(self.numerical_columns, 1)[0]

        if not isinstance(numerical_column_1, list):
            numerical_column_1 = [numerical_column_1]

        if not isinstance(numerical_column_2, list):
            numerical_column_2 = [numerical_column_2]

        for each_numerical_1 in numerical_column_1:
            for each_numerical_2 in numerical_column_2:

                X = sm.add_constant(self.data[each_numerical_1])
                res = sm.OLS(self.data[each_numerical_2], X).fit()

                st, data, ss2 = summary_table(res, alpha=0.05)
                preds = pd.DataFrame.from_records(data, columns=[s.replace('\n', ' ') for s in ss2])
                preds['displ'] = self.data[each_numerical_1]
                preds = preds.sort_values(by = 'displ')

                trace0 = Scattergl(
                    x = self.data[each_numerical_1].dropna(),
                    y = self.data[each_numerical_2].dropna(),
                    mode = 'markers',
                    name = 'Points'
                )

                trace1 = Scattergl(
                    x = preds['displ'],
                    y = preds['Predicted Value'],
                    mode = 'line',
                    name = 'Regression'
                )

                trace2 = Scattergl(
                    x = preds['displ'],
                    y = preds['Mean ci 95% low'],
                    name= 'Lower 95% CI',
                    showlegend= False,
                    line =  dict(color = 'transparent'),
                    mode = 'line'
                )
                trace3 = Scattergl(
                    x = preds['displ'],
                    y = preds['Mean ci 95% upp'],
                    name = '95 % CI',
                    fill = 'tonexty',
                    mode = 'line',
                    line = dict(color='transparent'),
                    fillcolor = 'rgba(255, 127, 14, 0.3)'
                )

                data = Data([trace0, trace1, trace2, trace3])
                layout = Layout(
                    title = each_numerical_1 + ' vs ' + each_numerical_2,
                    titlefont = dict(size = 20),
                    xaxis=XAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth= 0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title = each_numerical_1
                    ),
                    yaxis=YAxis(
                        gridcolor='rgba(229,229,229,0.5)',
                        gridwidth= 0.7,
                        showgrid=True,
                        showline=False,
                        showticklabels=True,
                        tickcolor='rgb(127,127,127)',
                        ticks='outside',
                        zeroline=False,
                        title=each_numerical_2
                    )
                )
                fig = Figure(data=data, layout=layout)
                plotly.offline.iplot(fig)

    def plot_ts(self, time_column, target_column, sort = False):
        """
        Plots a time series, with time column on x-axis and target column on y-axis

        :param time_column:
        :param target_column:
        :param sort: If target column is not sorted, send false (default). If it needs to be sorted, send true
        :return:
        """
        if sort:
            df = self.data.sort_values(by=time_column)
        else:
            df = self.data

        trace = Scatter(
            x = df[time_column],
            y = df[target_column],
            line=Line(color='rgb(19,48,84)',width = 3),
            mode='lines'
        )
        data = Data([trace])
        layout = Layout(
            title='Timeseries',
            titlefont=dict(
                size=25
            ),
            xaxis=XAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=True,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False,
                title=time_column,
                titlefont=dict(size=19),
                tickangle='35'
            ),

            yaxis=YAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=True,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False,
                title=target_column,
                titlefont=dict(size=19),
                tickfont=dict(
                    size=15
                )
            ),
            legend=dict(
                font=dict(
                    size=13,
                )
            )
        )
        fig = Figure(data=data, layout=layout)
        plotly.offline.iplot(fig)

    def plot_box(self):
        """
        Generates box-plot for all numerical columns

        :return:
        """
        if len(self.numerical_columns) == 0:
            print('Not enough numerical columns')
            return

        c = ['hsl(' + str(h) + ',50%' + ',50%)' for h in np.linspace(0, 360, len(self.numerical_columns))]
        all_traces = [
            Box(
            y = scale(self.data[self.numerical_columns[i]].dropna(), axis = 0, with_mean = True, with_std = True),
            boxmean = 'sd',
            marker = dict(color = c[i]),
            name = self.numerical_columns[i])
            for i in range(len(self.numerical_columns))]

        data = Data(all_traces)
        layout = Layout(
            title='Box Plot',
            titlefont=dict(
                size=20
            ),
            xaxis=XAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=False,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False
            ),

            yaxis=YAxis(
                gridcolor='rgba(229,229,229,0.5)',
                gridwidth=0.7,
                showgrid=True,
                showline=False,
                showticklabels=True,
                tickcolor='rgb(127,127,127)',
                ticks='outside',
                zeroline=False
            )
        )
        fig = Figure(data = data, layout = layout)
        plotly.offline.iplot(fig)

    def plot_all_histogram(self, ncols = 4):
        """
        If you're lazy generating one histogram at once, this will generate all histograms at once!

        :param ncols: Minimum number of numerical columns to use this function.
        :return:
        """
        numerical_cols = np.array(self.numerical_columns)

        if len(numerical_cols) < ncols:
            print('To plot all histograms, the number of numerical columns should be greater than ',ncols)
            return

        numerical_cols = numerical_cols[:(len(numerical_cols) - (len(numerical_cols) % ncols))]
        c = np.array(['hsl(' + str(h) + ',50%' + ',50%)' for h in np.linspace(0, 360, len(numerical_cols))])
        numerical_cols = numerical_cols.reshape(-1, ncols)
        c = c.reshape(-1, ncols)

        nrows = len(numerical_cols)

        fig = plotly.tools.make_subplots(rows=nrows, cols=ncols)
        for i in range(nrows):
            for j in range(ncols):
                trace = Histogram(
                    x = self.data[numerical_cols[i][j]].values,
                    name = numerical_cols[i][j],
                    marker = dict(color= c[i][j])
                )
                fig.append_trace(trace, i+1, j+1)

        fig['layout'].update(height=nrows * 300, width=ncols* 300, title='Histogram')
        plotly.offline.iplot(fig)

    def plot_all_frequency(self, ncols = 4):
        """
        If you're lazy generating one frequency at once, this will generate all frequency plots at once!

        :param ncols: Minimum number of numerical columns to use this function.
        :return:
        """
        categorical_cols = np.array(self.categorical_columns)

        if len(categorical_cols) < ncols:
            print('To plot all frequency, the number of categorical columns should be greater than ',ncols)
            return

        categorical_cols = categorical_cols[:(len(categorical_cols) - (len(categorical_cols) % ncols))]
        c = np.array(['hsl(' + str(h) + ',50%' + ',50%)' for h in np.linspace(0, 360, len(categorical_cols))])
        categorical_cols = categorical_cols.reshape(-1, ncols)
        c = c.reshape(-1, ncols)

        nrows = len(categorical_cols)

        fig = plotly.tools.make_subplots(rows=nrows, cols=ncols)
        count = 1
        for i in range(nrows):
            for j in range(ncols):
                series = self.data[categorical_cols[i][j]]
                objcounts = series.value_counts()
                x = objcounts.values.tolist()[::-1]
                y = objcounts.index.tolist()[::-1]

                trace = Bar(
                    x = x,
                    y = y,
                    orientation = 'h',
                    opacity = 0.75,
                    name = categorical_cols[i][j],
                    marker = dict(color= c[i][j])
                )
                fig.append_trace(trace, i+1, j+1)
                str_temp = 'yaxis'+str(count)
                fig['layout'][str_temp].update(showticklabels = False)
                count+=1

        fig['layout'].update(height=nrows * 300, width=ncols* 300, title='Frequency')
        plotly.offline.iplot(fig)

    def plot_distplot(self,numerical_columns = None):
        """ Generates distribution plot given a numerical column.

        If no column is given, it will pick a column randomly
        :param numerical_columns:
        :return:
        """
        if numerical_columns == None:
            numerical_columns = np.random.choice(self.numerical_columns, 1)

        if len(numerical_columns) == 0:
            print('Not enough columns')
            return

        fig = FF.create_distplot([self.data[numerical_column].dropna() for numerical_column in numerical_columns],
                                 [numerical_column for numerical_column in numerical_columns],
                                 show_hist=False,
                                 show_rug= False)
        for d in fig['data']:
            d.update({'fill': 'tozeroy'})

        fig.layout.xaxis.title = ' vs '.join(numerical_columns)
        plotly.offline.iplot(fig)

    def plot_correlation_network(self, THRESH_CORR = 0.9):
        """
        Generates beautiful correlation network plot between all numerical columns

        :param THRESH_CORR: The correlation threshold above which variables are to be plotted
        :return:
        """
        corr_df = self.data.corr()
        links = corr_df.stack().reset_index()
        links.columns = ['var1', 'var2', 'value']
        links_filtered = links.loc[(links['value'] > THRESH_CORR) & (links['var1'] != links['var2'])]
        if len(links_filtered) == 0:
            print('No correlation found. Change THRESH_CORR parameter to lower values')
            return

        G = nx.from_pandas_edgelist(links_filtered, 'var1', 'var2')
        pos = nx.spring_layout(G)

        # Building edge_trace
        edge_trace = Scatter(
            x=[],
            y=[],
            line=Line(width=0.5, color='#888'),
            hoverinfo='none',
            mode='lines')

        for edge in G.edges():
            x0, y0 = pos[edge[0]]
            x1, y1 = pos[edge[1]]
            edge_trace['x'] += [x0, x1, None]
            edge_trace['y'] += [y0, y1, None]

        # Building node_trace
        node_trace = Scatter(
            x=[],
            y=[],
            text=[],
            mode='markers',
            hoverinfo='text',
            marker=Marker(
                showscale=True,
                # colorscale options
                # 'Greys' | 'Greens' | 'Bluered' | 'Hot' | 'Picnic' | 'Portland' |
                # Jet' | 'RdBu' | 'Blackbody' | 'Earth' | 'Electric' | 'YIOrRd' | 'YIGnBu'
                colorscale='YIGnBu',
                reversescale=True,
                color=[],
                size=10,
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line=dict(width=2)))

        for node in G.nodes():
            x, y = pos[node]
            node_trace['x'].append(x)
            node_trace['y'].append(y)

        for node, adjacencies in enumerate(G.adjacency()):
            node_trace['marker']['color'].append(len(adjacencies[1]))
            node_info = '# of connections: ' + str(len(adjacencies[1]))
            node_trace['text'].append(adjacencies[0] + ': ' + node_info)

        # Building a layout
        fig = Figure(data=Data([edge_trace, node_trace]),
                     layout=Layout(
                         title='Correlation Network',
                         titlefont=dict(size=16),
                         showlegend=False,
                         hovermode='closest',
                         annotations=[dict(
                             text="",
                             showarrow=False,
                             xref="paper", yref="paper",
                             x=0.005, y=-0.002)],
                         xaxis=XAxis(showgrid=False, zeroline=False, showticklabels=False),
                         yaxis=YAxis(showgrid=False, zeroline=False, showticklabels=False)))

        plotly.offline.iplot(fig)


    def plot_decomposition(self, time_column, target_column, freq = 7):
        """Decomposes a time series into Trend, Seasonality and Residual (STL)

        :param time_column:
        :param target_column:
        :param freq: Number of days the data is repeating
        :return:
        """
        temp_df = pd.DataFrame(data=self.data[target_column].values, index=pd.to_datetime(self.data[time_column]), columns=[target_column])
        result = seasonal_decompose(temp_df[target_column], freq= freq)
        trace1 = Scatter(
            x=result.observed.index.strftime('%Y/%m/%d'),
            y=result.observed.values,
            name='Observed',
            mode='lines',
            xaxis='x1',
            yaxis='y1'
        )
        trace2 = Scatter(
            x=result.trend.index.strftime('%Y/%m/%d'),
            y=result.trend.values,
            name='Trend',
            mode='lines',
            xaxis='x2',
            yaxis='y2'
        )
        trace3 = Scatter(
            x=result.seasonal.index.strftime('%Y/%m/%d'),
            y=result.seasonal.values,
            name='Seasonal',
            mode='lines',
            xaxis='x3',
            yaxis='y3'
        )
        trace4 = Scatter(
            x=result.resid.index.strftime('%Y/%m/%d'),
            y=result.resid.values,
            name='Residual',
            mode='lines',
            xaxis='x4',
            yaxis='y4'
        )
        data = Data([trace1, trace2, trace3, trace4])
        layout = {
            "height": 1100,
            "title": "Decomposed Timeseries",
            "width": 1000,
            "xaxis1": {
                "anchor": "y1",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "xaxis2": {
                "anchor": "y2",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "xaxis3": {
                "anchor": "y3",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "xaxis4": {
                "anchor": "y4",
                "domain": [0.0, 1.0],
                'tickangle': 40,
                'tickfont': {'size': 12}
            },
            "yaxis1": {
                "anchor": "x1",
                "domain": [0.85, 1.0]
            },
            "yaxis2": {
                "anchor": "x2",
                "domain": [0.6, 0.75]
            },
            "yaxis3": {
                "anchor": "x3",
                "domain": [0.30, 0.45]
            },
            "yaxis4": {
                "anchor": "x4",
                "domain": [0.05, 0.20]
            }
        }

        fig = Figure(data=data, layout=layout)
        plotly.offline.iplot(fig)

    def plot_q_q(self, numerical_column = None):
        """This may take some time to generate

        :param numerical_column:
        :return:
        """

        if numerical_column == None:
            numerical_column = np.random.choice(self.numerical_columns, 1)[0]

        if not isinstance(numerical_column, list):
            numerical_column = [numerical_column]

        for each_numerical in numerical_column:
            fig, ax = plt.subplots()
            stats.probplot(self.data[each_numerical], dist="norm", plot=ax)
            ax.set_title("Q-Q plot")
            print('Showing q-q plot for', each_numerical)
            plt.show()

    def plot_autocorrelation(self, numerical_column):
        """
        Generates autocorrelation plot given numerical column

        :param numerical_column:
        :return:
        """

        if not isinstance(numerical_column, list):
            numerical_column = [numerical_column]

        for each_numerical in numerical_column:
            fig, ax = plt.subplots()
            autocorrelation_plot(self.data[each_numerical], ax=ax)
            ax.set_title('Auto-correlation plot')
            plotly.offline.iplot_mpl(fig)

    def plot_lag(self, numerical_column):
        """
        Lag plots are used to check if a data set or time series is random.
        Random data should not exhibit any structure in the lag plot.
        Non-random structure implies that the underlying data are not random.

        :param numerical_column:
        :return:
        """

        if not isinstance(numerical_column, list):
            numerical_column = [numerical_column]

        for each_numerical in numerical_column:
            fig, ax = plt.subplots()
            pd.plotting.lag_plot(self.data[each_numerical], ax=ax)
            ax.set_title('Lag plot')
            plotly.offline.iplot_mpl(fig)

    def plot_pacf(self, numerical_column, lags = 50):
        """
        Generates partial auto-correlation plot

        :param numerical_column:
        :param lags: The number of lags
        :return:
        """

        if not isinstance(numerical_column, list):
            numerical_column = [numerical_column]

        for each_numerical in numerical_column:
            fig, ax = plt.subplots()
            plot_pacf(self.data[each_numerical], lags = lags, ax= ax)
            ax.set_title('PACF plot')
            plt.show()

    def browse_all(self, combs, function_to_call):
        """
        Helper function to generate an interactive slider

        :param combs:
        :param function_to_call:
        :return:
        """
        n = len(combs)
        if n==0:
            print('Number of combinations should be greater than 1')
            return

        print(tabulate([(i, *c) for i, c in enumerate(combs)], headers=['key', *['col'+ str(i+1) for i in range(len(combs[0]))]]))
        def view_image(x):
            function_to_call(*combs[x])
        ipywidgets.widgets.interact(view_image, x=(0,n-1))

    def generate_numerical_combinations(self, order):
        numerical_combs = list(itertools.combinations(self.numerical_columns, order))
        return numerical_combs

    def generate_numerical_categorical_combinations(self):
        return np.array(np.meshgrid(self.numerical_columns, self.categorical_columns)).T.reshape(-1, 2)

    def generate_categorical_combinations(self, order):
        categorical_combs = list(itertools.combinations(self.categorical_columns, order))
        return categorical_combs

    def print_df(self, nrows = 10):
        """
        A small snapshot of the dataframe.

        :param nrows: The number of columns to be printed
        :return:
        """
        with pd.option_context('display.max_rows', None, 'display.max_columns', self.data.shape[1]):
            return display(self.data.head(nrows))

    def overview(self):
        """
        Overview section at the top of a notebook
        Contains basic overview like number of columns, etc.

        :return:
        """
        n_columns = len(self.data.columns)
        n_rows = len(self.data)
        memsize = self.fmt_bytesize(self.data.memory_usage(index=True).sum())

        n_numerical = len(self.numerical_columns)
        n_categorical = len(self.categorical_columns)

        d = {
            'Number of columns': n_columns,
            'Number of rows': n_rows,
            'Memory size': memsize,
            'Number of numerical columns': n_numerical,
            'Number of categorical columns': n_categorical}

        tbl = PrettyTable(["Stats", "Value"])
        for k in d:
            tbl.add_row([k, d[k]])
        print(tbl)

    def select_run(self, dict):
        dict = collections.OrderedDict(dict)
        for i in dict.keys():
            method = getattr(self, i)
            if type(dict[i]) is tuple:
                a= method(*dict[i])
            else:
                a = method(dict[i])
