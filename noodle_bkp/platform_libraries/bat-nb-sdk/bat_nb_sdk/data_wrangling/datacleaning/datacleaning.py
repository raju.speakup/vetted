import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import warnings
import collections

warnings.filterwarnings('ignore')


class DataCleaning(object):
    """ The main DataCleaning object.

    The DataCleaning is a python class which provides methods
    for data cleaning .
    It is formed from the pandas dataframe.

    :param data: pandas dataFrame
    :return: :class:`DataCleaning` object

    Usage::
            >>> data_file = "datafile.csv"
            >>> df = pd.read_csv(data_file)
            >>> data_cleaning = DataCleaning(df)

    """
    def __init__(self, data):
        """ Initializing the class variables.
        :param data: pandas dataFrame

        """
        self.data = data

    def imputation(self, column, value, name):
        """ Imputation of missing values

        :param column: list of column names.
        :param  value: int or float,
                       mean of the column or median of the column or any value.
        :param   name: str,
                       action performed, e.g. 'impute'.

        :return: pandas dataFrame
        """

        self.data[[s + "_" + name for s in column]] = self.data[column].fillna(value)

    def scaling(self, column, name):
        """ Scaling of data

        :param column: list of column names.
        :param   name: str,
                       action performed, e.g. 'scaling'.

        :return: pandas dataFrame
        """

        scaler = MinMaxScaler()
        df_scaled = scaler.fit_transform(self.data[column])
        self.data[[s + "_" + name for s in column]] = pd.DataFrame(data=df_scaled[0:, 0:])

    def replace(self, column, replace_from, replace_to, name):
        """ Replace a string or a part of a string for a specific set of columns

        :param column: list of column names.
        :param replace_from: str,
                             string matching to this will be replaced.
        :param   replace_to: str ,
                             matched strings are replaced to this value.
        :param         name: str,
                             action performed, e.g. 'replaced'.
        :return: pandas dataFrame
        """

        self.data[[s + "_" + name for s in column]] = self.data[column].replace(to_replace=replace_from,
                                                                                value=replace_to)

    def select_run(self, dict):
        """ This is an utility function to invoke multiple data cleaning methods.

        :param dict: dict,
                     it specifies a dictionary where method names are keys and the corresponding parameter list are its  values.
        """
        dict = collections.OrderedDict(dict)
        for i in dict.keys():
            print("\n" + i)
            method = getattr(self, i)
            if type(dict[i]) is tuple:
                a = method(*dict[i])
            else:
                a = method(dict[i])
