{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Anomaly Detection - Train\n",
    "\n",
    "Anomaly detection (also outlier detection) is the identification of items, events or observations which do not conform to an expected pattern or other items in a dataset. In this notebook, anomaly detection is done on timeseries data, by converting timeseries into a smaller latent vector and finding reconstruction loss between the reconstructed output and the input. The higher the reconstruction loss, the more chances of it being an anomaly.\n",
    "\n",
    "**Methodology followed:**\n",
    "* Use Variational Recurrent AutoEncoder (VRAE) for dimensionality reduction of the timeseries\n",
    "* Reconstruct output for test_dataset and compute reconstruction loss\n",
    "* Higher the score, higher the chances of it being an anomaly\n",
    "* This is a classical example of Unsupervised Learning\n",
    "\n",
    "\n",
    "**In this notebook:**\n",
    "* Train the VRAE model and dump it\n",
    "\n",
    "**Paper:**\n",
    "\n",
    "* [Variational Inference for On-line Anomaly Detection in High-Dimensional Time Series](https://arxiv.org/pdf/1412.6581.pdf)\n",
    "* [Variational Recurrent Auto-encoders](https://arxiv.org/pdf/1602.07109.pdf)\n",
    "\n",
    "**Author: Tejas Lodaya <br>\n",
    "Version: v1 <br>**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Contents\n",
    "\n",
    "0. [Load data and preprocess](#Load-data-and-preprocess)\n",
    "1. [Initialize VRAE object](#Initialize-VRAE-object)\n",
    "2. [Fit the model onto dataset](#Fit-the-model-onto-dataset)\n",
    "3. [Save the model to be fetched later](#Save-the-model-to-be-fetched-later)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import HTML\n",
    "HTML('''<script>\n",
    "code_show=true; \n",
    "function code_toggle() {\n",
    "if (code_show){\n",
    "$('div.input').hide();\n",
    "} else {\n",
    "$('div.input').show();\n",
    "}\n",
    "code_show = !code_show\n",
    "} \n",
    "$( document ).ready(code_toggle);\n",
    "</script>\n",
    "<form action=\"javascript:code_toggle()\"><input type=\"submit\" value=\"Click here to toggle on/off the raw code.\"></form>''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Add python path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.path.append('../../')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import required modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from clustering.vrae import VRAE\n",
    "from clustering.utils import *\n",
    "import numpy as np\n",
    "from sklearn.manifold import TSNE\n",
    "from sklearn.decomposition import TruncatedSVD\n",
    "from random import randint\n",
    "import pandas as pd\n",
    "import torch\n",
    "\n",
    "from plotly.graph_objs import *\n",
    "import plotly\n",
    "import matplotlib\n",
    "from numpy import genfromtxt\n",
    "from torch.utils.data import DataLoader, TensorDataset\n",
    "plotly.offline.init_notebook_mode()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dload = './model_dir'\n",
    "train_data_file = '../../data/chfdb_chf13_45590_TRAIN.pkl'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hyper parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hidden_size = 90\n",
    "hidden_layer_depth = 1\n",
    "latent_length = 20\n",
    "batch_size = 8\n",
    "learning_rate = 0.005\n",
    "n_epochs = 40\n",
    "dropout_rate = 0.0\n",
    "optimizer = 'Adam' # options: ADAM, SGD\n",
    "cuda = True\n",
    "print_every = 1\n",
    "clip = True\n",
    "max_grad_norm = 5\n",
    "loss = 'MSELoss' # options: SmoothL1Loss, MSELoss, ReconLoss\n",
    "block = 'LSTM' # options: LSTM, GRU"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load data and preprocess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train = pd.DataFrame(pd.read_pickle(train_data_file))\n",
    "X_train = X_train.iloc[:, :2].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Fetch `sequence_length` and `number_of_features` from dataset**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sequence_length = 150\n",
    "number_of_features = X_train.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize VRAE object\n",
    "\n",
    "VRAE inherits from `sklearn.base.BaseEstimator` and overrides `fit`, `transform` and `fit_transform` functions, similar to sklearn modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vrae = VRAE(sequence_length=sequence_length,\n",
    "            number_of_features = number_of_features,\n",
    "            hidden_size = hidden_size, \n",
    "            hidden_layer_depth = hidden_layer_depth,\n",
    "            latent_length = latent_length,\n",
    "            batch_size = batch_size,\n",
    "            learning_rate = learning_rate,\n",
    "            n_epochs = n_epochs,\n",
    "            dropout_rate = dropout_rate,\n",
    "            optimizer = optimizer, \n",
    "            cuda = cuda,\n",
    "            print_every=print_every, \n",
    "            clip=clip, \n",
    "            max_grad_norm=max_grad_norm,\n",
    "            loss = loss,\n",
    "            block = block,\n",
    "            dload = dload)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create train dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#train_dataset = create_train_dataset(X_train, vrae, batch_given = True)\n",
    "\n",
    "# If batch is not given, pass `batch_given` as false. Also pass the shift_size (gap between each cut)\n",
    "train_dataset = create_train_dataset(X_train, vrae, batch_given = False, shift_size = sequence_length, sequence_length=sequence_length)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit the model onto dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "vrae.fit(train_dataset)\n",
    "\n",
    "#If the model has to be saved, with the learnt parameters use:\n",
    "# vrae.fit(dataset, save = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save the model to be fetched later"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vrae.save('chfdb_chf13_45590.pth')\n",
    "\n",
    "# To load a presaved model, execute:\n",
    "# vrae.load('vrae.pth')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
