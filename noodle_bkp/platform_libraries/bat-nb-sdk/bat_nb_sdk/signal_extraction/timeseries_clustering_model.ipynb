{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Timeseries clustering\n",
    "\n",
    "Time series clustering is to partition time series data into groups based on similarity or distance, so that time series in the same cluster are similar.\n",
    "\n",
    "Methodology followed:\n",
    "* Use Variational Recurrent AutoEncoder (VRAE) for dimensionality reduction of the timeseries\n",
    "* To visualize the clusters, PCA and t-sne are used\n",
    "\n",
    "Bitbucket link: https://bitbucket.org/noodleai/noodleai\n",
    "\n",
    "**Author: Tejas Lodaya <br>\n",
    "Version: v1 <br>**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Contents\n",
    "\n",
    "0. [Load data](#Load-data)\n",
    "1. [Initialize VRAE object](#Initialize-VRAE-object)\n",
    "2. [Fit the model onto dataset](#Fit-the-model-onto-dataset)\n",
    "3. [Transform the input timeseries to encoded latent vectors](#Transform-the-input-timeseries-to-encoded-latent-vectors)\n",
    "4. [Save the model to be fetched later](#Save-the-model-to-be-fetched-later)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import HTML\n",
    "HTML('''<script>\n",
    "code_show=true; \n",
    "function code_toggle() {\n",
    "if (code_show){\n",
    "$('div.input').hide();\n",
    "} else {\n",
    "$('div.input').show();\n",
    "}\n",
    "code_show = !code_show\n",
    "} \n",
    "$( document ).ready(code_toggle);\n",
    "</script>\n",
    "<form action=\"javascript:code_toggle()\"><input type=\"submit\" value=\"Click here to toggle on/off the raw code.\"></form>''')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Add python path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.path.append('../../')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import required modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from clustering.vrae import VRAE\n",
    "from clustering.utils import *\n",
    "import numpy as np\n",
    "from sklearn.manifold import TSNE\n",
    "from sklearn.decomposition import TruncatedSVD\n",
    "from sklearn.cluster import KMeans\n",
    "from random import randint\n",
    "import pandas as pd\n",
    "\n",
    "from plotly.graph_objs import *\n",
    "import plotly\n",
    "import matplotlib\n",
    "\n",
    "\n",
    "plotly.offline.init_notebook_mode()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dload = './model_dir'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hyper parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hidden_size = 90\n",
    "hidden_layer_depth = 1\n",
    "latent_length = 20\n",
    "batch_size = 32\n",
    "learning_rate = 0.0005\n",
    "n_epochs = 2\n",
    "dropout_rate = 0.2\n",
    "optimizer = 'Adam' # options: ADAM, SGD\n",
    "cuda = False\n",
    "print_every=30\n",
    "clip = True\n",
    "max_grad_norm=5\n",
    "loss = 'MSELoss' # options: SmoothL1Loss, MSELoss, ReconLoss\n",
    "block = 'LSTM' # options: LSTM, GRU"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_val, y_train, y_val = open_data('../../data', ratio_train=0.9)\n",
    "\n",
    "num_classes = len(np.unique(y_train))\n",
    "base = np.min(y_train)  # Check if data is 0-based\n",
    "if base != 0:\n",
    "    y_train -= base\n",
    "y_val -= base"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = TensorDataset(torch.from_numpy(X_train))\n",
    "test_dataset = TensorDataset(torch.from_numpy(X_val))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "** Fetch `sequence_length` from dataset**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sequence_length = X_train.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize VRAE object\n",
    "\n",
    "VRAE inherits from `sklearn.base.BaseEstimator` and overrides `fit`, `transform` and `fit_transform` functions, similar to sklearn modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vrae = VRAE(sequence_length=sequence_length,\n",
    "            hidden_size = hidden_size, \n",
    "            hidden_layer_depth = hidden_layer_depth,\n",
    "            latent_length = latent_length,\n",
    "            batch_size = batch_size,\n",
    "            learning_rate = learning_rate,\n",
    "            n_epochs = n_epochs,\n",
    "            dropout_rate = dropout_rate,\n",
    "            optimizer = optimizer, \n",
    "            cuda = cuda,\n",
    "            print_every=print_every, \n",
    "            clip=clip, \n",
    "            max_grad_norm=max_grad_norm,\n",
    "            loss = loss,\n",
    "            block = block,\n",
    "            dload = dload)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit the model onto dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "vrae.fit(train_dataset)\n",
    "\n",
    "#If the model has to be saved, with the learnt parameters use:\n",
    "# vrae.fit(dataset, save = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transform the input timeseries to encoded latent vectors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z_run = vrae.transform(test_dataset)\n",
    "\n",
    "#If the latent vectors have to be saved, pass the parameter `save`\n",
    "# z_run = vrae.transform(dataset, save = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save the model to be fetched later"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vrae.save('vrae.pth')\n",
    "\n",
    "# To load a presaved model, execute:\n",
    "# vrae.load('vrae.pth')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize using PCA and tSNE"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_clustering(z_run, y_val, engine='matplotlib', download = False)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
