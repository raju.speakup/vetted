import csv
import time
import psutil
from datetime import datetime
from anomaly_detection import module_runner
from celery import group

# model_dir = '/Users/c_rajkumar.t/noodle/noodleai/Pynotebooks/signal_extraction/model_dir/'
model_dir = '/home/bat-user/code/noodleai/Pynotebooks/signal_extraction/model_dir/'
model_name = 'chfdb_chf13_45590_{}.pth'


# no_of_jobs = [2, 4, 6, 8, 10]
no_of_jobs = [2, 4, 6, 8, 10]

def create_csv(val_list, name):
    with open("{}.txt".format(name), "wb") as f:
        for row in val_list:
            f.write("{}\n".format(str(row)).encode('utf-8'))


for idx, no in enumerate(no_of_jobs):

    job_list = list()
    cpu_usage = list()
    memory = list()
    task_time = list()
    cpu_usage.append(psutil.cpu_percent())
    print("Initial Memory: ".format((psutil.virtual_memory().percent / 100) * 32))
    memory.append((psutil.virtual_memory().percent / 100) * 32)
    for i in range(0, no):
        job_list.append(
            module_runner.s(
                model_dir=model_dir,
                model_name=model_name.format(str(i)),
                train_file='/home/bat-user/code/noodleai/data/chfdb_chf13_45590_TRAIN.pkl',
                test_file='/home/bat-user/code/noodleai/data/chfdb_chf13_45590_TEST.pkl',
                end_slice=(None, None, 2)

            )
        )
    job = group(job_list)
    j_no = str(len(job_list))
    print("No of tasks : {} ".format(j_no))
    result = job.apply_async()
    row = 0
    start_time = datetime.now()
    while True:
        cpu_usage.append(psutil.cpu_percent())
        memory.append((psutil.virtual_memory().percent / 100) * 32)
        if result.successful():
            print(result.successful())
            break
        time.sleep(1)
    end_time = datetime.now()
    time_taken = (end_time - start_time).seconds
    print("Memory after run: ".format((psutil.virtual_memory().percent / 100) * 32))
    print("Time taken to run {} tasks : {} ".format(j_no, time_taken))
    create_csv(cpu_usage, '{}_cpu'.format(str(no)))
    create_csv(memory, '{}_mem'.format(str(no)))



