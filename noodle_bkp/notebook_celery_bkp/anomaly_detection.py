import os
from celery import Celery

app = Celery('anamoly', broker="amqp://admin:pass@192.168.10.185:5672/pam", backend="amqp://admin:pass@192.168.10.185:5672/pam")


def init(model_dir, pickle_file, start_slice, end_slice):

    import sys
    sys.path.append('.')
    from clustering.vrae import VRAE

    import pandas as pd
    # from plotly.graph_objs import *
    hidden_size = 90
    hidden_layer_depth = 1
    latent_length = 20
    batch_size = 8
    learning_rate = 0.005
    n_epochs = 40
    dropout_rate = 0.0
    optimizer = 'Adam'  # options: ADAM, SGD
    cuda = False
    print_every = 1
    clip = True
    max_grad_norm = 5
    loss = 'MSELoss'  # options: SmoothL1Loss, MSELoss, ReconLoss
    block = 'LSTM'  # options: LSTM, GRU
    X_train = pd.DataFrame(pd.read_pickle(pickle_file))
    print(X_train)
    X_train = X_train.iloc[slice(*start_slice), slice(*end_slice)].values
    print(X_train)
    sequence_length = 150
    number_of_features = X_train.shape[1]

    vrae = VRAE(sequence_length=sequence_length,
                number_of_features=number_of_features,
                hidden_size=hidden_size,
                hidden_layer_depth=hidden_layer_depth,
                latent_length=latent_length,
                batch_size=batch_size,
                learning_rate=learning_rate,
                n_epochs=n_epochs,
                dropout_rate=dropout_rate,
                optimizer=optimizer,
                cuda=cuda,
                print_every=print_every,
                clip=clip,
                max_grad_norm=max_grad_norm,
                loss=loss,
                block=block,
                dload=model_dir)

    return vrae, X_train, sequence_length


def anamoly_detection_train(vrae, X_train, sequence_length, model_path):
    from clustering.utils import create_train_dataset
    # If batch is not given, pass `batch_given` as false. Also pass the shift_size (gap between each cut)
    train_dataset = create_train_dataset(X_train, vrae, batch_given=False, shift_size=sequence_length,
                                         sequence_length=sequence_length)

    vrae.fit(train_dataset)
    vrae.save(model_path)


def anamoly_detection_test(vrae, X_test, sequence_length, model_path):
    from clustering.utils import cut_and_score
    print("Inside test")
    vrae.load(model_path)
    score_df = cut_and_score(X_test, vrae, batch_given=False, shift_size=sequence_length,
                             sequence_length=sequence_length)
    return score_df
    # plot_anomaly(X_test, score_df)



@app.task
def module_runner(model_dir, model_name, train_file, test_file, start_slice=(None, None, None), end_slice=(None, None, None)):
    vrae, X_train, sequence_length = init(model_dir, train_file, start_slice, end_slice)
    # anamoly_detection_train(
    #     vrae=vrae,
    #     X_train=X_train,
    #     sequence_length=sequence_length,
    #     model_path=model_name
    # )
    # vrae, X_test, sequence_length = init(model_dir, test_file, start_slice, end_slice)
    # score = anamoly_detection_test(
    #     vrae=vrae,
    #     X_test=X_test,
    #     sequence_length=sequence_length,
    #     model_path=os.path.join(model_dir, model_name)
    # )
    #
    # print(score)


if __name__ == "__main__":
    model_dir = '/home/bat-user/code/noodleai/Pynotebooks/signal_extraction/model_dir/'
    model_name = 'chfdb_chf13_45590_{}.pth'
    train_file = '/home/bat-user/code/noodleai/data/chfdb_chf13_45590_TRAIN.pkl'
    test_file = '/home/bat-user/code/noodleai/data/chfdb_chf13_45590_TEST.pkl'
    start_slice = (None, None, None)
    end_slice = (None, None, 2)
    vrae, X_train, sequence_length = init(model_dir, train_file, start_slice, end_slice)
    anamoly_detection_train(
        vrae=vrae,
        X_train=X_train,
        sequence_length=sequence_length,
        model_path=model_name
    )
    vrae, X_test, sequence_length = init(model_dir, test_file, start_slice, end_slice)
    score = anamoly_detection_test(
        vrae=vrae,
        X_test=X_test,
        sequence_length=sequence_length,
        model_path=os.path.join(model_dir, model_name)
    )

    print(score )
    # model_dir = '/Users/c_rajkumar.t/noodle/noodleai/Pynotebooks/signal_extraction/model_dir/'
    # model_name = 'chfdb_chf13_45590.pth'
    #
    # score = module_runner(
    #     model_dir=model_dir,
    #     model_name=model_name,
    #     train_file='/Users/c_rajkumar.t/noodle/noodleai/data/chfdb_chf13_45590_TRAIN.pkl',
    #     test_file='/Users/c_rajkumar.t/noodle/noodleai/data/chfdb_chf13_45590_TEST.pkl',
    #     end_slice=slice(None, 10)
    #
    # )