import pandas as pd
import warnings
warnings.filterwarnings('ignore')


class MtsSubset(object):
    """
    The main %Revenue/unit sales object.
    The MtsSubset is a python class which allows user to get the total revenue/unit sales for different product
    groups and also their percentage revenue/unit sales distribution across data.
    """
    def __init__(self, data, time_column, id_cols, value_column):
        """
        Initializing the class variable.
        :param data: input pandas DataFrame
        :param time_column: date/time column in input data
        :param id_cols: list if unique ids in the data. each unique id represents a unique time series.
        :param value_column: target/value column in data.
        """
        self.data = data
        self.time_column = time_column
        self.id_cols = id_cols
        self.value_column = value_column
        self.result = self.data[self.id_cols]
        
    def subset_var(self, start_date, end_date):
        """
        Function that generates the total revenue and percentage distribution across data for a desired period.
        :param start_date: starting date/time stamp for the time period
        :param end_date: last date/time stamp of the time period.
        :return: pandas DataFrame
        """
        self.result.drop_duplicates(keep='first', inplace=True)
        self.result.index = pd.RangeIndex(len(self.result.index))
        self.data = self.data.groupby(self.id_cols).apply(lambda group: group.loc[(group[self.time_column] >= start_date) & (group[self.time_column] <= end_date)]).reset_index(drop=True)
        self.data = self.data.fillna(0)
        total_sum = self.data[self.value_column].sum()
        self.result['sum_'+''+str(self.value_column)] = self.data.groupby(self.id_cols).apply(lambda group: group[self.value_column].sum()).reset_index(drop=True)
        self.result['percentage_'+''+str(self.value_column)] = ((self.result['sum_'+''+str(self.value_column)]) / total_sum) * 100
        return self.result
    
    def subset_input(self, input_type, n):
        """
        This function allows user to get better understanding of the data based on the total revenue/sales unit and
        the percentage distribution across data for different id groups. It allows user to know best performing 'n ids'
        and also get subset of data with ids which covers 'n%' of distribution across data.
        :param input_type: str,type of subset
        :param n: int, input value for type of subset
        :return: pandas DataFrame
        """
        self.result.sort_values(by=['sum_'+''+str(self.value_column)], ascending=False, inplace=True)
        self.result.index = pd.RangeIndex(len(self.result.index))
        if input_type == 'top_n':
            return self.result.head(n)
        elif input_type == '%n':
            self.result['cum_sum'] = self.result['percentage_'+''+str(self.value_column)].cumsum()
            self.result = self.result.loc[self.result['cum_sum'] <= n]
            self.result.drop(['sum_'+''+str(self.value_column), 'cum_sum'], axis=1, inplace=True)
            return self.result
