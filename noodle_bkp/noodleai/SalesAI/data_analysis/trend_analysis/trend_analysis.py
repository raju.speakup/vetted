# import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')

class TrendAnalysis(object):

    def __init__(self, data, id_cols, time_col, target_col, level, d_folder, download):
        self.data = data
        self.id_cols = id_cols
        self.time_col = time_col
        self.target_col = target_col
        self.level = level
        self.d_folder = d_folder
        self.download = download

    def trend_boxplot(self, level, group, id_cols):
        if level == "Yearly":
            fig, ax = plt.subplots(figsize=(15, 4))
            group.boxplot(column=self.target_col, by=group[self.time_col].dt.year, ax=ax, grid=False).set_title(level + ' ' + str(self.id_cols) + ' ' + str(id_cols))
            plt.suptitle("")
            plt.xlabel(level)
            plt.ylabel(self.target_col)
            if self.download:
                file_name = level + "_"+str(id_cols)
                if os.path.exists(self.d_folder + "/"):
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
                else:
                    os.mkdir(self.d_folder + "/")
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
        elif level == "Quarterly":
            fig, ax = plt.subplots(figsize=(15, 4))
            group.boxplot(column=self.target_col, by=[group[self.time_col].dt.year, group[self.time_col].dt.quarter], ax=ax, grid=False).set_title(level + ' ' + str(self.id_cols) + ' ' + str(id_cols))
            plt.suptitle("")
            plt.xlabel(level)
            plt.ylabel(self.target_col)
            if self.download:
                file_name = level + "_"+str(id_cols)
                if os.path.exists(self.d_folder + "/"):
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
                else:
                    os.mkdir(self.d_folder + "/")
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
        elif level == "Monthly":
            fig, ax = plt.subplots(figsize=(50, 15))
            group.boxplot(column=self.target_col, by=[group[self.time_col].dt.year, group[self.time_col].dt.month], ax=ax, grid=False).set_title("monthly trend -" + " " + str(id_cols))
            plt.suptitle("")
            plt.xlabel(level)
            plt.ylabel(self.target_col)
            if self.download:
                file_name = level + "_"+str(id_cols)
                if os.path.exists(self.d_folder + "/"):
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
                else:
                    os.mkdir(self.d_folder + "/")
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
        elif level == "Weekly":
            fig, ax = plt.subplots(figsize=(200, 20))
            group.boxplot(column=self.target_col, by=[group[self.time_col].dt.year, group[self.time_col].dt.weekofyear], ax=ax, grid=False).set_title("weekly trend -" + " " + str(id_cols))
            plt.suptitle("")
            plt.xlabel(level)
            plt.ylabel(self.target_col)
            if self.download:
                file_name = level + "_"+str(id_cols)
                if os.path.exists(self.d_folder + "/"):
                    plt.savefig(self.d_folder + "/" + file_name + '.png')
                else:
                    os.mkdir(self.d_folder + "/")
                    plt.savefig(self.d_folder + "/" + file_name + '.png')

    def trend_analysis(self):
        if self.id_cols==[]:
            id_cols = 'complete data'
            self.id_cols = " "
            self.trend_boxplot(self.level, self.data, id_cols)
        else:
            for id_cols, group in self.data.groupby(self.id_cols):
                self.trend_boxplot(self.level, group, id_cols)

