import pandas as pd
import numpy as np
from wave_transformation import wavetest
import os
import math
import matplotlib.pyplot as plt 
import datetime as dt
import warnings
warnings.filterwarnings('ignore')


class PeriodicityAnalysis(object):
    """
    Periodicity analysis of the time series data.

    :return: : class: `PeriodicityAnalysis` object
    """

    def __init__(self,data,dload,folder_name,target_column,date_column):
        """

        :param data: pandas `DataFrame`.
        :param dload: str,
                       download path.
        :param folder_name: str,
                             name of folder to save the plots.
        :param target_column: list of str,
                                target column.
        :param date_column: str,
                            date column.
        """
        self.data=data
        self.dload=dload
        self.folder=folder_name
        self.target_column=target_column
        self.dload=dload
        self.power=None
                
    def ts_annual_plot(self,download,group,col_x,col_y,title,xlab,ylab):
        """
        Plot Multiple Boxplot.

        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param group: list,
                         unique group id which is grouped by year.
        :param col_x: list of str,
                         xaxis data.
        :param col_y: list of str,
                         yaxis data.
        :param title: str,
                        title for the plot.
        :param xlab: str,
                       xaxis label.
        :param ylab: str,
                       yaxis label.
        :return: Multiple boxplot
        """
        save_dir = self.dload+"/" +self.folder
        file_name=title
        if os.path.exists(save_dir):
            pass
        else:
            os.mkdir(save_dir)
        fig, ax = plt.subplots(figsize=(15,4))
        group.boxplot(column=col_y,by=col_x,ax=ax,grid=False).set_title(title)
        plt.suptitle("")
        plt.xlabel(xlab)
        plt.ylabel(ylab)
        
        if download==True:
            plt.savefig(save_dir + "/" +file_name+ '.png')
        
   
    def ts_periodicity(self,target_column):
        """
        Periodicity Analysis using Power spectrum.

        :param target_column: list of str,
                                target column.
        :return: Power spectrum
        """
        self.data=self.data.dropna()
        temp= self.data[target_column]
        temp=temp.values.squeeze()
        variance = np.var(temp)
        data_norm = (temp- np.mean(temp)) / (np.sqrt(variance))
        result = wavetest.cwt(data_norm,1, 1, 0.01,2, 7/0.01, 0.72, 6,'Morlet','plot')
        x=np.linspace(0,300,len(data_norm))
        wavetest.wavelet_plot('Power_Spectrum', x, data_norm, 0.03125, result)
        self.power=result['power']  
        
    def ts_annual_periodicity(self,date_column,freq,year,download):
        """
        Periodicity Analysis on Annual Basis.

        :param date_column: str,
                            date column.
        :param target_column: list of str,
                                target column.
        :param freq: str,
                      frequency to based on which the periodicity is analysed.
        :param year: list,
                      year for which the periodicity plot needs to be plotted.
                      :year: empty list,
                              plots for all the years.
                      :year: list of unique years,
                              plots only for the specified year or list of years.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :return: periodicity plot
        """
        self.data[date_column]=pd.to_datetime(self.data[date_column])
        self.data=self.data.sort_values(date_column)
        power = self.power.transpose()
        l=[]
        for i in power:
            l.append(i.mean())
        temp=self.data.dropna()
        temp['power'] = pd.Series(l)
        temp["minmax"]=abs((temp['power'] -temp['power'].max()) / (temp['power'].max() -temp['power'].min()))
        temp["year"]=temp[date_column].dt.year
        print(temp['year'].unique())
        if freq=='quater':
                temp["freq"] = temp[date_column].dt.quarter
        elif freq=='month':
            temp["freq"]=temp[date_column].dt.month
        elif freq=='week':
            temp["freq"] = temp[date_column].dt.week
        else:
            raise ValueError("Invalid value")
        year_group=temp.groupby('year')
        xlab=str(freq)
        ylab="Normalized_data"
        if year==[]:
            for i, group in year_group:
                title='Periodicity Analysis for the year'+' '+str(i)
                self.ts_annual_plot(download=download,group=group,col_x=['freq'],col_y='minmax',title=title,xlab=xlab,ylab=ylab)                 
        elif year:
            for i in year:
                title='Periodicity Analysis for the year'+' '+str(i)
                group=year_group.get_group(i)
                self.ts_annual_plot(download=download,group=group,col_x=['freq'],col_y='minmax',title=title,xlab=xlab,ylab=ylab)

                
           
        
        
            
        
