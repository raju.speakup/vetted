import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from numpy import array
from Pynotebooks.feature_engineering.featureengineering.featureengineering import FeatureEngineering


class timeseries_type(object):
   
        def __init__(self, data, time_column, value_column, id_cols):
            self.data = data
            self.value_column = value_column
            self.time_column = time_column
            self.id_cols = id_cols
            self.feature_engineering = FeatureEngineering(pd.DataFrame())
                        

        def transform(self, time_column, value_column, id_cols):
            result = pd.DataFrame()
            df_groups = self.data.groupby(id_cols)
            for id_field,group in df_groups:
                group.insert(loc=1,column='periodList',value=list(range(1,(len(group)+1),1)))
                self.feature_engineering.data = group
                #self.feature_engineering.data.insert(loc=1,column='periodList',value=list(range(1,(len(self.feature_engineering.data)+1),1)))
                self.feature_engineering.ts_classify('periodList',value_column)
                result = result.append(self.feature_engineering.data)
            Finalresult = result.groupby(id_cols).first()
            return Finalresult
        

    