import pandas as pd
import warnings

warnings.filterwarnings('ignore')


class MtsCalendarVar(object):
    """
    The main date-time feature object.
    The MtsCalendarVar is a python class which generates date-time features for the data based on date/time column.
    """
    def __init__(self, data, time_column):
        """
        Initializing the class variables.
        :param data: input pandas DataFrame
        :param time_column: date/time column in input data
        """
        self.data = data
        self.time_column = time_column

    def calendar_var(self, features_list):
        """
        The main function used to get date-time features for the data.
        :param features_list: list supplied by the user containing features that have to be generated.
        :return: pandas DataFrame containing new data-time features along with the data.
        """
        features = ['year', 'week', 'month', 'quarter', 'dayofweek', 'day_of_year', 'weekdayname']
        if not features_list:
            self.data['year'] = pd.DatetimeIndex(self.data[self.time_column]).year
            self.data['week'] = pd.DatetimeIndex(self.data[self.time_column]).week
            self.data['month'] = pd.DatetimeIndex(self.data[self.time_column]).month
            self.data['quarter'] = pd.DatetimeIndex(self.data[self.time_column]).quarter
            self.data['dayofweek'] = pd.DatetimeIndex(self.data[self.time_column]).dayofweek
            self.data['day_of_year'] = pd.DatetimeIndex(self.data[self.time_column]).dayofyear
            self.data['weekdayname'] = pd.DatetimeIndex(self.data[self.time_column]).weekday_name
            return self.data
        elif features_list:
            for i in features_list:
                if i in features:
                    if i == 'year':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).year
                    elif i == 'week':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).week
                    elif i == 'month':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).month
                    elif i == 'quarter':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).quarter
                    elif i == 'dayofweek':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).dayofweek
                    elif i == 'day_of_year':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).dayofyear
                    elif i == 'weekdayname':
                        self.data[i] = pd.DatetimeIndex(self.data[self.time_column]).weekday_name
                else:
                    raise Exception('input format for list is incorrect')
            return self.data
