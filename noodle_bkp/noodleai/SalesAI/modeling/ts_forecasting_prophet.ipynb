{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Univariate time series forecasting with Prophet\n",
    "The term \"univariate time series\" refers to a time series that consists of single (scalar) observations recorded sequentially over equal time increments.\n",
    "Although a univariate time series data set is usually given as a single column of numbers, time is in fact an implicit variable in the time series. If the data are equi-spaced, the time variable, or index, does not need to be explicitly given. The time variable may sometimes be explicitly used for plotting the series. \n",
    "We will cover following things in this notebook\n",
    "1. [Univariate time series plotting](#Data-Assessment)\n",
    "2. [Plots to examine stationarity](#Stationarity-examination)\n",
    "3. [Augmented Dickey Fuller test](#Augmented-Dickey-Fuller-test)\n",
    "4. [Forecasting using prophet](#Initial-model-train)\n",
    "5. [Examination of trend, seasonality components](#Trend,-seasonality)\n",
    "6. [Error examination](#Model-validation)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Include required libraries\n",
    "from fbprophet import Prophet\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.mlab as mlab\n",
    "import math\n",
    "import plotly as py\n",
    "import plotly.graph_objs as go\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Data import\n",
    "\n",
    "ds        |    y\n",
    "----------|------\n",
    "2013-01-01|   324\n",
    "2013-01-08|   534\n",
    "2017-12-24|   31\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transformation / Aggregation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Read the file as pandas data frame. We expect two columns, ds and y.\n",
    "df = pd.read_csv('../../data/sample_pg_dc.csv', usecols=['ds', 'y'])\n",
    "df['ds']=pd.to_datetime(df['ds'])\n",
    "\n",
    "#Sort on \"ds\" column\n",
    "df = df.sort_values(by=['ds'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train / Test Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Set train end date. Data after train end date will be used for testing\n",
    "train_end_date='2017-01-01'\n",
    "\n",
    "no_steps_to_predict=51\n",
    "\n",
    "#Divide data into train and test set. \n",
    "train=df[df['ds']<train_end_date]\n",
    "test=df[df['ds']>=train_end_date]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Assessment\n",
    "#### Univariate time series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Visualize raw training data\n",
    "from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot\n",
    "init_notebook_mode(connected=True)\n",
    "trace0 = go.Scatter(\n",
    "    x=train.ds,\n",
    "    y=train.y,\n",
    "    mode='lines+markers',\n",
    "    name='Train'\n",
    ")\n",
    "\n",
    "trace1 = go.Scatter(\n",
    "    x=test.ds,\n",
    "    y=test.y,\n",
    "    mode='lines+markers',\n",
    "    name='Test'\n",
    ")\n",
    "\n",
    "data=[trace0, trace1]\n",
    "py.offline.iplot(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stationarity examination\n",
    "We are looking at the mean and variance, we are assuming that the data conforms to a Gaussian (also called the bell ####curve or normal) distribution. We can also quickly check this by eyeballing a histogram of our observations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train.hist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we can split the time series into two contiguous sequences. We can then calculate the mean and variance of each group of numbers and compare the values.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X=pd.Series(train.y)\n",
    "split = int(len(X) / 2)\n",
    "X1, X2 = X[0:split], X[split:]\n",
    "mean1, mean2 = X1.mean(), X2.mean()\n",
    "var1, var2 = X1.var(), X2.var()\n",
    "print('mean1=%f, mean2=%f' % (mean1, mean2))\n",
    "print('variance1=%f, variance2=%f' % (var1, var2))\n",
    "mpl_fig = plt.figure()\n",
    "ax = mpl_fig.add_subplot(111)\n",
    "ax.boxplot([X1, X2])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Augmented Dickey-Fuller test\n",
    "Statistical tests make strong assumptions about your data. They can only be used to inform the degree to which a null hypothesis can be accepted or rejected. The result must be interpreted for a given problem to be meaningful.\n",
    "\n",
    "Nevertheless, they can provide a quick check and confirmatory evidence that your time series is stationary or non-stationary.\n",
    "\n",
    "The Augmented Dickey-Fuller test is a type of statistical test called a unit root test.\n",
    "\n",
    "The intuition behind a unit root test is that it determines how strongly a time series is defined by a trend.\n",
    "\n",
    "There are a number of unit root tests and the Augmented Dickey-Fuller may be one of the more widely used. It uses an autoregressive model and optimizes an information criterion across multiple different lag values.\n",
    "\n",
    "The null hypothesis of the test is that the time series can be represented by a unit root, that it is not stationary (has some time-dependent structure). The alternate hypothesis (rejecting the null hypothesis) is that the time series is stationary.\n",
    "\n",
    "Null Hypothesis (H0): If accepted, it suggests the time series has a unit root, meaning it is non-stationary. It has some time dependent structure.\n",
    "Alternate Hypothesis (H1): The null hypothesis is rejected; it suggests the time series does not have a unit root, meaning it is stationary. It does not have time-dependent structure.\n",
    "We interpret this result using the p-value from the test. A p-value below a threshold (such as 5% or 1%) suggests we reject the null hypothesis (stationary), otherwise a p-value above the threshold suggests we accept the null hypothesis (non-stationary).\n",
    "\n",
    "p-value > 0.05: Accept the null hypothesis (H0), the data has a unit root and is non-stationary.\n",
    "p-value <= 0.05: Reject the null hypothesis (H0), the data does not have a unit root and is stationary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from statsmodels.tsa.stattools import adfuller\n",
    "result = adfuller(train.y)\n",
    "print('ADF Statistic: %f' % result[0])\n",
    "print('p-value: %f' % result[1])\n",
    "print('Critical Values:')\n",
    "for key, value in result[4].items():\n",
    "\tprint('\\t%s: %.3f' % (key, value))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initial model train\n",
    "Prophet follows the sklearn model API. We create an instance of the Prophet class and then call its fit and predict methods. We fit the model by instantiating a new Prophet object. Any settings to the forecasting procedure are passed into the constructor. Then you call its fit method and pass in the historical dataframe. Fitting should take 1-5 seconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m=Prophet(changepoint_prior_scale = 0.001)\n",
    "m.fit(train)\n",
    "\n",
    "#forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initial Model testing\n",
    "Plot the forecast by calling the Prophet.plot method and passing in your forecast dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "future = m.make_future_dataframe(periods=no_steps_to_predict, freq='w')\n",
    "forecast=m.predict(future)\n",
    "m.plot(forecast)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Distribution of forecasted series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mu = np.nanmean(forecast[forecast['ds']>train_end_date]['yhat'])\n",
    "sigma = np.nanmean(m.params['sigma_obs'])*m.y_scale\n",
    "x = np.linspace(mu - 5*sigma, mu + 5*sigma, 100)\n",
    "plt.plot(x,mlab.normpdf(x, mu, sigma))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Trend, seasonality\n",
    "If you want to see the forecast components, you can use the Prophet.plot_components method. By default you’ll see the trend, yearly seasonality, and weekly seasonality of the time series. If you include holidays, you’ll see those here, too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.plot_components(forecast);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### First we examine the plots of actual and forecasted"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Visualize raw training data\n",
    "from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot\n",
    "init_notebook_mode(connected=True)\n",
    "trace0 = go.Scatter(\n",
    "    x=train.ds,\n",
    "    y=train.y,\n",
    "    mode='lines+markers',\n",
    "    name='Train'\n",
    ")\n",
    "\n",
    "trace1 = go.Scatter(\n",
    "    x=test.ds,\n",
    "    y=test.y,\n",
    "    mode='markers',\n",
    "    name='Test'\n",
    ")\n",
    "\n",
    "trace2 = go.Scatter(\n",
    "    x=forecast[forecast['ds']>train_end_date].ds,\n",
    "    y=forecast[forecast['ds']>train_end_date].yhat,\n",
    "    mode='lines',\n",
    "    name='Predicted'\n",
    ")\n",
    "\n",
    "data=[trace0, trace1, trace2]\n",
    "py.offline.iplot(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model experimentation\n",
    "Placeholder for now.\n",
    "\n",
    "## Model validation\n",
    "##### Following errors are calculated\n",
    "1. MAPE\n",
    "2. SMAPE\n",
    "3. RMSE"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def smape(y_true, y_pred):\n",
    "    denominator = (np.abs(y_true) + np.abs(y_pred))\n",
    "    diff = np.abs(y_true - y_pred) / denominator\n",
    "    diff[denominator == 0] = 0.0\n",
    "    return 200 * np.mean(diff)\n",
    "\n",
    "def mape(y_true, y_pred):\n",
    "    return (np.abs(y_true-y_pred)/np.abs(y_true)).mean()*100\n",
    "\n",
    "def rmse(y_true, y_pred):\n",
    "    return np.sqrt(((y_pred - y_true) ** 2).mean())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pred=pd.Series(forecast[forecast['ds']>train_end_date].yhat).reset_index(drop=True)\n",
    "act=pd.Series(test.y).reset_index(drop=True)\n",
    "print(\"MAPE - %f\" % mape(act, pred))\n",
    "print(\"SMAPE - %f\" % smape(act, pred))\n",
    "print(\"RMSE - %f\" % rmse(act, pred))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Long term and short accuracies compared\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Keeping fixed proportion of observation as short term\n",
    "prop_div=.2\n",
    "short_pred=pred[:int(len(pred)*prop_div)]\n",
    "short_act=act[:int(len(pred)*prop_div)]\n",
    "print(\"Short term accuracy based on %f data points\" % int(len(pred)*(prop_div)))\n",
    "print(\"MAPE - %f\" % mape(short_act, short_pred))\n",
    "print(\"SMAPE - %f\" % smape(short_act, short_pred))\n",
    "print(\"RMSE - %f\" % rmse(short_act, short_pred))\n",
    "\n",
    "long_pred=pred[int(len(pred)*(1-prop_div)):]\n",
    "long_act=act[int(len(pred)*(1-prop_div)):]\n",
    "print(\"\\n\\nLong term accuracy based on %f data points\" % int(len(pred)*(1-prop_div)))\n",
    "print(\"MAPE - %f\" % mape(long_act, long_pred))\n",
    "print(\"SMAPE - %f\" % smape(long_act, long_pred))\n",
    "print(\"RMSE - %f\" % rmse(long_act, long_pred))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
