import pandas as pd
import numpy as np
from fastai.structured import *
from fastai.column_data import *
from SalesAI.modeling.deep_forecast.mvts_forecast import ts_forecast

class deep_learning(ts_forecast):
    def __init__(self, ts_forecast):
        '''

        :param ts_forecast: ts forecast class
        '''
        self.train = ts_forecast.train
        self.dep = ts_forecast.dep
        self.date_col = ts_forecast.date_col
        self.id_cols = ts_forecast.id_cols
        self.lag_features_dep = ts_forecast.lag_features_dep
        self.lag_features_ext = ts_forecast.lag_features_ext
        self.train_end_date = ts_forecast.train_end_date
        self.lag_range = ts_forecast.lag_range
        self.time_diff = ts_forecast.time_diff
        self.rolling_features_name_dep = ts_forecast.rolling_features_name_dep
        self.rolling_features_name_ext = ts_forecast.rolling_features_name_ext
        self.LAG_dep = ts_forecast.LAG_dep
        self.LAG_ext = ts_forecast.LAG_ext
        self.rolling_features_dep = ts_forecast.rolling_features_dep
        self.rolling_features_ext = ts_forecast.rolling_features_ext
        self.ROLL_FEAT_DEP = ts_forecast.ROLL_FEAT_DEP
        self.ROLL_FEAT_EXT = ts_forecast.ROLL_FEAT_EXT
        self.output_col = ts_forecast.output_col
        self.cat_vars = ts_forecast.cat_vars
        self.contin_vars = ts_forecast.contin_vars
        self.mapper = None
        self.nas = None
        self.metrics = metrics
        self.trained_model = None
        self.ext = ts_forecast.ext
        self.external_data = ts_forecast.external_data
        self.non_rolling_feature_ext = ts_forecast.non_rolling_feature_ext
        self.pred_ext = ts_forecast.pred_ext
        
    
    def convert_to_model_data(self,in_data, mapper, nas,batch_size):
        '''

        :param in_data: input data
        :param mapper: mapper argument
        :param nas: nas argument
        :param batch_size: batch size
        :return:
        '''
        in_data = in_data[self.cat_vars+self.contin_vars+[self.dep ,self.date_col]+self.ext]
        for v in self.cat_vars: 
            in_data[v] = in_data[v].astype('category').cat.as_ordered()
        for v in self.contin_vars:
            in_data[v] = in_data[v].astype('float32')
        in_data = in_data.set_index(self.date_col)

        df_test, _, nas, mapper = proc_df(in_data, self.dep, do_scale=True, mapper=mapper, na_dict=nas)
        test_dl =  DataLoader(ColumnarDataset.from_data_frame(df_test, self.cat_vars), batch_size)
        return test_dl  
    
    def train_model(self ,batch_size,val_set_bias,validation_start,validation_end,lr, number_epochs,hidden_layers, dropout_rate):
        '''

        :param batch_size: batch size
        :param val_set_bias: validation set bias
        :param validation_start: validation_start date
        :param validation_end: validation_end date
        :param lr: Learning Rate
        :param number_epochs: Number of epochs
        :param hidden_layers: Number of hidden layers
        :param dropout_rate: Dropout rate
        :return: trained model
        '''
        joined = self.train.set_index(self.date_col)
        df, yl, self.nas, self.mapper = proc_df(joined, self.dep, do_scale=True)
        #yl = np.log(y)
        #yl = yl.fillna(-.2)
        #yl=y

        #Find indexes which we want to set as validation set
        val_idx = np.flatnonzero(
            (df.index<=validation_end) & (df.index>=validation_start))

       

        #Set range for the predicted values
        #y_range = (-10, max_log_y*1.2)
        y_range = (np.min(yl), np.max(yl)*1.2)

        #Build internal datastructure of fastai library. 
        md = ColumnarModelData.from_data_frame('./', val_idx, df, yl.astype(np.float32), cat_flds=self.cat_vars, bs=batch_size, test_df=df.tail())

        #Build embeddings for the categorical variables
        #For each categorical variable create maximum of 50 or categories/2 number of embeddings
        cat_sz = [(c, len(joined[c].cat.categories)+1) for c in self.cat_vars]
        emb_szs = [(c, min(50, (c+1)//2)) for _,c in cat_sz]

        ### Train model
        m = md.get_learner(emb_szs, len(df.columns)-len(self.cat_vars),
             0.4, 1, hidden_layers, dropout_rate, y_range=y_range)
        self.trained_model = m 
        
    
    def fit_model(self,val_set_bias,lr,number_epochs, cycle_len , cycle_mult):
        '''

        :param val_set_bias:validation set bias
        :param lr: learning rate
        :param number_epochs: number of epochs
        :param cycle_len: cycle length
        :param cycle_mult: cycle multiplier
        :return: fitted model
        '''
        def inv_y(a): return np.exp(a)

        def exp_rmspe(y_pred, targ):
            targ = inv_y(targ)
            pct_var = (targ - inv_y(y_pred))/targ
            return math.sqrt((pct_var**2).mean())

        def umape_cuda(y_pred, targ):
            score = (val_set_bias + sum(abs((y_pred-targ).cpu().numpy())))/(val_set_bias + (targ.cpu().numpy()).sum())
            return score
        self.trained_model.fit(lr, number_epochs,metrics=[umape_cuda],cycle_len=cycle_len,cycle_mult=cycle_mult)
        
    def predict_deep_learning(self,train,prediction_start_date,forecast_step,predict_horizon,batch_size,alpha_ew):
        '''

        :param forecast_step: number of steps ahead to forecast
        :param predict_horizon: number of steps to predict for
        :param batch_size: bacth size
        :param alpha_ew: smoothing factor
        :return: predicted values
        '''
        out_df = pd.DataFrame()
        self.prepare_test(train,prediction_start_date)
        self.last_train_data.head()
        for step in range(predict_horizon):
            test_data = self.build_features(forecast_step)
            new_test_data = test_data[self.train.columns.values.tolist()]
            test_dl = self.convert_to_model_data(new_test_data, self.mapper, self.nas,batch_size)
            pred = self.trained_model.predict_dl(test_dl)
            result_df = self.get_next(pred, alpha_ew=alpha_ew)
            out_df = pd.concat([out_df, result_df])
        return out_df


