import pandas as pd
import numpy as np
from xgboost import XGBRegressor
from sklearn.ensemble import RandomForestRegressor
from SalesAI.modeling.deep_forecast.mvts_forecast import ts_forecast
class generalized_model(ts_forecast):
    
    def __init__(self, ts_forecast):
        '''

        :param ts_forecast: Accessing  ts_forecast class
        '''
        self.train = ts_forecast.train
        self.dep = ts_forecast.dep
        self.date_col = ts_forecast.date_col
        self.id_cols = ts_forecast.id_cols
        self.train_end_date = ts_forecast.train_end_date
        self.time_diff = ts_forecast.time_diff
        self.rolling_features_name_dep = ts_forecast.rolling_features_name_dep
        self.rolling_features_name_ext = ts_forecast.rolling_features_name_ext
        self.output_col = ts_forecast.output_col
        self.lag_features_dep = ts_forecast.lag_features_dep
        self.lag_features_ext = ts_forecast.lag_features_ext
        self.lag_range = ts_forecast.lag_range
        self.LAG_dep = ts_forecast.LAG_dep
        self.LAG_ext = ts_forecast.LAG_ext
        self.rolling_features_dep = ts_forecast.rolling_features_dep
        self.rolling_features_ext = ts_forecast.rolling_features_ext
        self.ROLL_FEAT_DEP = ts_forecast.ROLL_FEAT_DEP
        self.ROLL_FEAT_EXT = ts_forecast.ROLL_FEAT_EXT
        self.ext = ts_forecast.ext
        self.data = None
        self.test_data = None
        self.external_data = ts_forecast.external_data
        self.non_rolling_feature_ext = ts_forecast.non_rolling_feature_ext
        self.pred_ext = ts_forecast.pred_ext
        self.train_start_date = ts_forecast.train_start_date
        self.contin_vars = ts_forecast.contin_vars
        self.cat_vars = ts_forecast.cat_vars

        

    def train_model(self,args,model_name):
        '''
        Train the model

        :param args: The arguments that are inputted into the model
        :param model_name: Name of the model such as XGBOOST or RandomForest
        :return: Returns the model
        '''
        self.pred = self.train[self.dep]
        self.data = self.train.select_dtypes(include=['float32'])
        model_name = model_name+"(**args)"
        model = eval(model_name)
        model.fit(self.data,self.pred)
        return model

        
    def predict_model(self,train_model,train,prediction_start_date,predict_horizon,alpha_ew,forecast_step):
        '''
        Forecast values using the model

        :param train_model:The model from the train_model function
        :param predict_horizon:Predict for x future dates
        :param alpha_ew: Smoothing factor
        :param forecast_step: Number of shifts
        :return: Returns the forecast
        '''
        self.prepare_test(train,prediction_start_date)
        out_df = pd.DataFrame()
        for step in range(predict_horizon):
            self.test_data = self.build_features(forecast_step)
            new_test_data = self.test_data[self.data.columns.values.tolist()]
            filtered_test_data = new_test_data.select_dtypes(include=['float32','float64'])
            pred = train_model.predict(filtered_test_data)
            result_df = self.get_next(pred, alpha_ew=alpha_ew)
            out_df = pd.concat([out_df, result_df])
        return out_df
    
