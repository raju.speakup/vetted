from fastai.structured import add_datepart
import pandas as pd
import numpy as np
from fastai.structured import *
from fastai.column_data import *
from datetime import timedelta


class ts_forecast():

    def __init__(self,date_col, id_cols, dep,ext, output_col='yhat', train_end_date=None):
        '''
        Preprocesses and make feature ready data to train and predict for a time series forecasting model.
        :param date_col: column name that contains date field
        :param id_cols: list of column which forms time series
        :param dep: dependent column name
        :param ext: external column name
        :param output_col: output field name
        :param train_end_date: training end date. Validation end date <= training end data
        '''

        #### Column information
        self.date_col = date_col
        self.id_cols = id_cols.copy()
        self.dep = dep
        self.ext = ext
        self.output_col = output_col


        #Lag features
        self.LAG_dep = 'lag_dep'
        self.lag_features_dep = []
        self.lag_range = None
        self.LAG_ext = 'lag_ext'
        self.lag_features_ext = []

        #Rolling features
        self.rolling_features_dep = None
        self.ROLL_FEAT_DEP = 'roll_dep'
        self.rolling_features_ext = None
        self.ROLL_FEAT_EXT = 'roll_ext'

        self.train_end_date = train_end_date
        self.time_diff = None
        self.train_start_date = None

        #Continous features
        self.contin_vars = []

        #Categorical features
        self.cat_vars = id_cols.copy()

        #Calender features
        self.calender_features = None
        
        self.main_train = None
        self.rolling_features_name_dep = []
        self.rolling_features_name_ext = []
        self.non_rolling_feature_ext = []
        self.pred_ext = []
          
        
   
    def set_train_data(self,data):
        '''
        Preprocesses and sets input data.
        Converts date column to datetime format.
        Sorts time series.
        Calculates time between two data points

        :param data: DataFrame containing time series
        :return: prepared training data
        '''

        # Set training data
        self.train = data
         # Convert datetime column to correct format
        self.train[self.date_col] = pd.to_datetime(self.train[self.date_col], format = '%d/%m/%y')
        # Keep data which is before training end date
        self.train = self.train[[self.date_col, self.dep]+self.ext + self.id_cols]
        self.train = self.train.sort_values(self.id_cols+[self.date_col]).reset_index()

        # Calculate difference between two consecutive time
        self.time_diff = self.train.loc[1, self.date_col] - self.train.loc[0, self.date_col]
        #self.time_diff = self.time_diff+timedelta(days=77)

    def create_lag_feature(self, lags,forecast_step):
        '''
        Create lag features. Updates train variable with lag features

        :param lags: int, number of lag features to build
        :param forecast_step: Number of shifts
        :return: Returns lag features
        '''
        self.lag_range = [i for i in range(1, lags + 1)]
        for lag in self.lag_range:
            self.train[self.LAG_dep + str(lag)] = self.train[self.dep].shift(lag+forecast_step)
            self.lag_features_dep.append(self.LAG_dep + str(lag))

        self.contin_vars += self.lag_features_dep
        
        
    def create_external_lag_feature(self, lags,forecast_step):
        '''
        Create lag features. Updates train variable with lag features

        :param lags: int, number of lag features to build
        :param forecast_step: Number of shifts
        :return: Returns external lag features
        '''
        self.lag_range = [i for i in range(1, lags + 1)]
        ext_var = [j for j in range(len(self.ext))]
        for lag in self.lag_range:
            for ext in ext_var:
                self.train[self.LAG_ext +self.ext[ext]+"_" +str(lag)] = self.train[self.ext[ext]].shift(lag+forecast_step)
                self.lag_features_ext.append(self.LAG_ext +self.ext[ext]+"_" +str(lag))

        self.contin_vars += self.lag_features_ext

    def create_rolling_feature(self, feature_list,forecast_step):
        '''
        Create rolling features

        :param feature_list: dict, rolling features to build. Dict key should type of rolling features and value is
        list of rolling windows. Example, {'max': [2,4,8], 'sum': [2,4,8]}.
        :param forecast_step: Number of shifts
        :return: Returns rolling features
        '''
        self.rolling_features_dep = feature_list
        self.rolling_features_name_dep = []
        for key, val in self.rolling_features_dep.items():
            for rolls in val:
                out_col = self.ROLL_FEAT_DEP + key + '_' + str(rolls)
                self.train[out_col] = getattr(pd.DataFrame.rolling(self.train[self.dep].shift(max(forecast_step,1)), window=rolls), key)()
                self.contin_vars += [out_col]
                self.rolling_features_name_dep += [out_col]

    def create_external_rolling_feature(self, feature_list,forecast_step):
        '''
        Create rolling features

        :param feature_list: dict, rolling features to build. Dict key should type of rolling features and value is
        list of rolling windows. Example, {'max': [2,4,8], 'sum': [2,4,8]}.
        :param forecast_step:
        :return: Returns external rolling features
        '''
        self.rolling_features_ext = feature_list
        self.rolling_features_name_ext = []
        ext_var = [j for j in range(len(self.ext))]
        for key, val in self.rolling_features_ext.items():
            for rolls in val:
                for ext in ext_var:
                    out_col = self.ROLL_FEAT_EXT + key + self.ext[ext]+'_' + str(rolls)
                    self.train[out_col] = getattr(pd.DataFrame.rolling(self.train[self.ext[ext]].shift(max(forecast_step,1)), window=rolls), key)()
                    self.contin_vars += [out_col]
                    self.rolling_features_name_ext += [out_col]

    def prepare_training_data(self,lags,roll_features,calender_features,forecast_step):
        '''
        Converts categorical and continous variable to right format.

        :param lags: Number of lags
        :param roll_features:  Number of rolling features with function to be applied
        :param calender_features: What type of calender features
        :param forecast_step: Number of shifts
        :return:prepared training data
        '''
        self.create_lag_feature(lags,forecast_step)
        self.create_rolling_feature(roll_features,forecast_step)
        self.add_calender_features(calender_features)
        if len(self.ext)!=0:
            self.create_external_lag_feature(lags,forecast_step)
            self.create_external_rolling_feature(roll_features,forecast_step)
        # Remove dates before this date. They have NaN values in lag features
        
        self.train_start_date = self.train[self.date_col].min() + self.time_diff * (max(self.lag_range)+forecast_step)
        self.train = self.train[self.train[self.date_col] >= self.train_start_date]
        self.external_data = self.train
        self.train = self.train.loc[self.train[self.date_col] <= self.train_end_date]
        self.train = self.train[self.cat_vars + self.contin_vars + [self.dep, self.date_col]+self.ext]
        # Convert categorical variables to category type
        for v in self.cat_vars: self.train[v] = self.train[v].astype('category').cat.as_ordered()

        # Convert continous variables to float
        for v in self.contin_vars:
            self.train[v] = self.train[v].astype('float32')
    

    def store_copy_train(self):
        self.train_copy = self.train.copy()

    def add_calender_features(self, calender_features):
        '''
        Add calender features. Uses fastai add_datepart function.

        :param calender_features: list, list of calender features. Should be a subset of
        ['Month', 'Is_month_end', 'Is_month_start', 'Is_quarter_end', 'Is_quarter_start', 'Is_year_end', 'Is_year_start'
        , 'Year', Week', 'Dayofyear']
        :return: calender features
        '''
        add_datepart(self.train, self.date_col, drop=False)
        self.calender_features = [self.date_col + itr_string for itr_string in calender_features]
        self.cat_vars += self.calender_features

    def build_lag(self,forecast_step, ds, lags, pred_dep,pred_ext, step_in):
        '''
        Build lag features for test data

        :param forecast_step: Number of shifts
        :param ds: dataframe that contains the last train data
        :param lags: number of lags
        :param pred_dep: dependent column values
        :param pred_ext: external column values
        :param step_in: time difference
        :return: Returns lag features
        '''
        ext_var = [j for j in range(len(self.ext))]
        for lag in range(1, lags + 1):
            if len(self.ext)!=0:
                for ext in ext_var:
                    pred_ext, ds[self.LAG_ext +self.ext[ext]+"_" +str(lag)] = np.array(ds[self.LAG_ext +self.ext[ext]+"_" +str(lag)]).astype('float32'), pred_ext.astype('float32')
            pred_dep, ds[self.LAG_dep + str(lag)] = np.array(ds[self.LAG_dep + str(lag)]).astype('float32'), pred_dep.astype('float32')
        self.some_data = self.external_data[self.external_data[self.date_col] == (max(ds[self.date_col])-timedelta(days=max(0,(forecast_step-1)*self.time_diff.days)))]
        ds[self.date_col] += step_in
        if len(self.ext)!=0:
            ds[self.ext] = self.some_data[self.ext].values
        add_datepart(ds, self.date_col, drop=False)
        return ds
    
    def build_roll_sum(self, ds):
        '''
        Build rolling features for test data

        :param ds: dataframe of last train data
        :return: build rolling data
        '''
        ext_var = [j for j in range(len(self.ext))]
        for key, val in self.rolling_features_dep.items():
            for rolls in val:
                out_col1= self.ROLL_FEAT_DEP + key + '_' + str(rolls)
                self.hist[out_col1] = getattr(pd.DataFrame.rolling(self.hist[self.dep], window=rolls), key)()
                if len(self.ext)!=0:
                    for ext in ext_var:
                        out_col2 = self.ROLL_FEAT_EXT + key + self.ext[ext]+'_' + str(rolls)
                        self.hist[out_col2] = getattr(pd.DataFrame.rolling(self.hist[self.ext[ext]], window=rolls), key)()

        roll_data = self.hist.loc[self.hist[self.date_col] == (ds.iloc[0][self.date_col] - (self.time_diff)),
                                  self.id_cols + self.rolling_features_name_dep + self.rolling_features_name_ext]
        ds = ds.merge(roll_data)
        self.hist = self.hist.loc[self.hist[self.date_col] > min(self.hist[self.date_col]),
                                self.id_cols + [self.dep, self.date_col]+self.ext]
        return ds
 
    def prepare_test(self,train,prediction_start_date):
        '''

        :return: prepares test data
        '''
        self.hist = None
        self.last_train_data = None 
        self.external_data = self.external_data[self.train.columns.values.tolist()]
        self.max_train_date = max(self.train[self.date_col])
        train[self.date_col] = pd.to_datetime(train[self.date_col], format = '%d/%m/%y')
        train = train[train[self.date_col] < prediction_start_date]
        self.max_train_date = (max(train[self.date_col]))
        self.last_train_data = self.external_data.loc[self.external_data[self.date_col] == self.max_train_date]
        self.last_train_data = self.last_train_data[self.id_cols + [self.dep, self.date_col] +self.ext+ self.lag_features_dep + self.lag_features_ext]
        self.hist = train.loc[train[self.date_col] >
                              (self.max_train_date - max(self.lag_range, default=0) * (self.time_diff)),
                              self.id_cols + [self.dep,  self.date_col]+self.ext]
        self.pred_dep = self.last_train_data[self.dep]
        if len(self.ext)!=0:
            self.pred_ext = self.last_train_data[self.ext]
        self.non_rolling_feature_dep = [e for e in list(self.last_train_data.columns) if e not in self.rolling_features_name_dep]
        if len(self.ext)!=0:
            self.non_rolling_feature_ext = [e for e in list(self.last_train_data.columns) if e not in self.rolling_features_name_ext]
        self.non_rolling_feature = list(set(self.non_rolling_feature_ext+self.non_rolling_feature_dep))

        return self.last_train_data
    
    def build_features(self,forecast_step):
        '''
        Build lag and rolling features

        :param forecast_step: Number of shifts
        :return: test data
        '''
        self.last_train_data = self.build_lag(forecast_step,self.last_train_data[self.non_rolling_feature],
                                              (max(self.lag_range)),self.pred_dep,self.pred_ext, step_in=self.time_diff)
        test_data1 = self.build_roll_sum(self.last_train_data.copy())
        return test_data1

    def get_next(self, pred, alpha_ew=1):
        '''
        Get the next set of data

        :param pred: prediction column values
        :param alpha_ew: smoothing factor
        :return: next data to be
        '''
        pred = alpha_ew * pred.flatten() + (1 - alpha_ew) * self.last_train_data[self.LAG_dep + '1']
        result_df = self.last_train_data.loc[:, self.id_cols + [self.date_col]].copy()
        result_df[self.output_col] = pred
        self.hist = pd.concat([self.hist, result_df])
        self.hist[self.dep] = self.hist[self.dep].fillna(self.hist[self.output_col])
        if len(self.ext)!=0:
            ext_var = [j for j in range(len(self.ext))]
            for ext in ext_var:
                self.hist[self.ext[ext]] = self.hist[self.ext[ext]].fillna(self.hist[self.output_col])
        self.hist = self.hist.sort_values(self.id_cols + [self.date_col])[self.id_cols + [self.date_col, self.dep]+self.ext]
        return result_df
    
    def check_input(self,validation_end,validation_start,train_end_date,hidden_layers,dropout_rate):
        '''
        Input checks

        :param validation_end: validation_end data
        :param validation_start: validation_start data
        :param train_end_date: train_end_date
        :param hidden_layers: number of hidden_layers
        :param dropout_rate: dropout_rate
        :return: value error
        '''
        #Check if validation start date is less than validation end date
        if validation_start >= validation_end:
            raise ValueError("Validation end date should be greater than validation start date")

        #Check if validation date is less than training date
        if validation_end > train_end_date:
            raise ValueError("Validation date should be less than training end data")

        #Number of dropout layer should be same as hidden layer
        if not(len(dropout_rate) == len(hidden_layers)):
            raise ValueError("Number of elements in dropout_rate param should be same as in hidden_layers")

        #Dropout rate values should be less than 1
        if max(dropout_rate) > 1:
            raise ValueError("Values in droout_rate cannot be greater than 1")
      