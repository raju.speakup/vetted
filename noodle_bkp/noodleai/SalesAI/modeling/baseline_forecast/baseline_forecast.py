import pandas as pd
import numpy as np
from datetime import timedelta
from statsmodels.tsa.api import ExponentialSmoothing, SimpleExpSmoothing, Holt
from statsmodels.tsa.statespace.sarimax import SARIMAX

class BaselineForecast(object):
    """
    A baseline forecast is an estimate of future demand that is based on historical demand.
    Why is a baseline important? Because it elevates your forecast above the status of a guess. When you use a baseline, you recognize that your best guide to what happens next is often what happened before.

    :return: :class:`BaselineForecast` object

    Usage::
            >>> bf = BaselineForecast(df = df, id_field = id_field, date_column = date_column, value_column = value_column, pred_column = pred_column, model=model,args =args,train_start_date = train_start_date, train_end_date = train_end_date,predict_horizon = predict_horizon)

    """

    def __init__(self, df, id_field, date_column, value_column, pred_column, model, train_start_date,train_end_date, predict_horizon):
        """
        :param df: pandas `dataframe`,
                   input data.
        :param id_field: list of str,
                         id columns.
        :param date_column: str,
                            date column
        :param value_column: str,
                             actual column.
        :param pred_column: str,
                            predicted column.
        :param model: str,
                      model name.
        :param args: dict,
                     arguments to the model.
        :param train_start_date: str,
                                 train start date.
        :param train_end_date: str,
                               train end date.
        :param predict_horizon: int,
                                forecast horizon.
        """
        self.data = df
        self.id_field = id_field
        self.date_column = date_column
        self.value_column = value_column
        self.pred_column = pred_column
        self.model = model
        self.train_start_date = pd.to_datetime(train_start_date, format='%d/%m/%Y')
        self.train_end_date = pd.to_datetime(train_end_date, format='%d/%m/%Y')
        self.train_end_date = min(df[date_column], key=lambda x: abs(x - self.train_end_date))
        self.train_start_date = min(df[date_column], key=lambda x: abs(x - self.train_start_date))
        self.predict_horizon = predict_horizon
        self.time_diff = (self.data[self.date_column][1] - self.data[self.date_column][0])
        self.out_df = pd.DataFrame()

    def moving_average(self, data, period=2, weights=None):
        """
        Forecasts using moving average.

        Moving average is Mean of time series data (observations equally spaced in time) from several consecutive periods.

        :param data: pandas `DataFrame`,
                     input data.
        :param period: int,
                       window size.
        :param weights: list,
                        weight for each step.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        data = data[data[self.date_column] <= self.train_end_date].tail(period)
        list_temp = pd.Series(data[self.value_column])
        # for simple moving average with equal weights
        if weights is None:
            for i in range(self.predict_horizon):
                list_temp = list_temp.append(list_temp.rolling(window=period).mean().tail(1))
        # for weighted moving average. Weights are specified
        else:
            period = len(weights)
            for i in range(self.predict_horizon):
                # predict and append to the list of observed values.
                list_temp = list_temp.append(pd.Series(sum([a * b for a, b in zip(list_temp.tail(period), weights)])))
        # forecast data
        forecast.loc[:, self.pred_column] = list_temp.tail(self.predict_horizon).reset_index(drop=True)
        return forecast

    def naive(self, data):
        """
        Forecast using naive approach.

        Naive forecast is simply set all forecasts to be the value of the last observation.

        :param data: pandas `DataFrame`,
                     input data.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        forecast.loc[:, self.pred_column] = data[data[self.date_column] == self.train_end_date][self.value_column].reset_index(drop=True)[0]
        return forecast

    def historical_average(self, data):
        """
        Forecast using historical average.

        The forecasts of all future values are equal to the average of the historical data.
        
        :param data: pandas `DataFrame`,
                     input data.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        data = data[(data[self.date_column] <= self.train_end_date)]
        forecast.loc[:, self.pred_column] = data[self.value_column].mean()
        return forecast

    def seasonal_naive(self, data, seasonal_period=0):
        """
        Forecast using seasonal naive approach.

        In this case, we set each forecast to be equal to the last observed value from the same season of the year (e.g., the same month of the previous year).

        :param data: pandas `DataFrame`,
                     input data.
        :param seasonal_period: int,
                                use previous nth  year's data.
        :return: pandas `DataFrame`,
                 forecast data
        """
        if seasonal_period == 0:
            raise ValueError('Seasonal period cannot be 0')
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        train = data[(data[self.date_column] >= self.train_start_date) & (data[self.date_column] <= self.train_end_date)]
        forecast.loc[:, self.pred_column] = train.tail(round(365*seasonal_period/(self.time_diff/ timedelta (days=1)))).head(self.predict_horizon)[self.value_column].reset_index(drop=True)
        return forecast

    def drift(self, data):
        """
        Forecast using drift approach.

        A variation on the naïve method is to allow the forecasts to increase or decrease over time, where the amount of change over time is set to be the average change seen in the historical data.

        :param data: pandas `DataFrame`,
                     input data.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        list_temp = pd.Series(data[self.value_column])
        start_val = data[data[self.date_column] == self.train_start_date][self.value_column].reset_index(drop=True)[0]
        end_val = data[data[self.date_column] == self.train_end_date][self.value_column].reset_index(drop=True)[0]
        train = data[(data[self.date_column] >= self.train_start_date) & (data[self.date_column] <= self.train_end_date)]
        for i in range(self.predict_horizon):
            # similar to drawing line from start to end and extrapolating to future.
            list_temp = list_temp.append(pd.Series(end_val + ((i + 1) * (end_val - start_val) / (train.shape[0] - 1))))
        forecast.loc[:, self.pred_column] = list_temp.tail(self.predict_horizon).reset_index(drop=True)
        return forecast

    def ses(self, data, alpha=0.5):
        """
        Forecast using simple exponential smoothing approach.

        The Simple Exponential Smoothing method is used for forecasting a time series when there is no trend
        or seasonal pattern, but the mean (or level) of the time series y_t is slowly changing over time.

        :param data: pandas `DataFrame`,
                     input data.
        :param alpha: float,
                       smoothing factor.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        train = data[
            (data[self.date_column] >= self.train_start_date) & (data[self.date_column] <= self.train_end_date)]
        fit = SimpleExpSmoothing(np.asarray(train[self.value_column])).fit(smoothing_level=alpha, optimized=False)
        forecast.loc[:, self.pred_column] = pd.Series(fit.forecast(self.predict_horizon))
        return forecast

    def holt_linear(self, data, smoothing_level=0.3, smoothing_slope=0.10):
        """
        Forecast using Holt's linear trend approach.

        If a time series is increasing or decreasing approximately at a fixed rate, then it may be described by the LINEAR TREND model

        :param data: pandas `DataFrame`,
                     input data.
        :param smoothing_level: float,
                                The alpha value of the simple exponential smoothing, if the value is set then this value will be used as the value.
        :param smoothing_slope: float,
                                The beta value of the holts trend method, if the value is set then this value will be used as the value.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        train = data[
            (data[self.date_column] >= self.train_start_date) & (data[self.date_column] <= self.train_end_date)]
        fit = Holt(np.asarray(train[self.value_column])).fit(smoothing_level=smoothing_level,
                                                             smoothing_slope=smoothing_slope)
        forecast.loc[:, self.pred_column] = pd.Series(fit.forecast(self.predict_horizon))
        return forecast

    def holt_winter(self, data, seasonal_periods=7):
        """
        Holt-Winters is a way to model three aspects of the time series: a typical value (average), a slope (trend) over time, and a cyclical repeating pattern (seasonality).

        :param data:  pandas `DataFrame`,
                     input data.
        :param seasonal_periods: int,
                                 The number of seasons to consider for the holt winters.
        :return: pandas `DataFrame`,
                 forecast data
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        train = data[
            (data[self.date_column] >= self.train_start_date) & (data[self.date_column] <= self.train_end_date)]
        fit = ExponentialSmoothing(np.asarray(train[self.value_column]), seasonal_periods=seasonal_periods, trend='add',
                                   seasonal='add').fit()
        forecast.loc[:, self.pred_column] = pd.Series(fit.forecast(self.predict_horizon))
        return forecast

    def arima(self, data, order=(1, 0, 0)):
        """
        ARIMA is a forecasting technique that projects the future values of a series based entirely on its own inertia.

        :param data: pandas `DataFrame`,
                     input data.
        :param order: iterable,
                      (p,d,q).
                            - p is the number of autoregressive terms.
                            - d is the number of nonseasonal differences needed for stationarity.
                            - q is the number of lagged forecast errors in the prediction equation.
        :return:
        """
        forecast = pd.DataFrame()
        dates = [self.train_end_date+self.time_diff]
        for i in range(self.predict_horizon-1):
            dates.append(dates[-1]+self.time_diff)
        forecast.loc[:, self.date_column] = dates
        train = data[
            (data[self.date_column] >= self.train_start_date) & (data[self.date_column] <= self.train_end_date)]
        fit = SARIMAX(train[self.value_column], order=order).fit()
        forecast.loc[:, self.pred_column] = pd.Series(fit.forecast(self.predict_horizon).values)
        return forecast

    def check_periodicity(self):
        """ Returns the periodicity of time series.

        :return: Timedelta
        """
        return self.time_diff

    def run_model(self,args):
        """
        Utility function to run the forecasting model.
        :return:
        """
        method = getattr(self, self.model)
        self.out_df = (self.data.groupby(self.id_field).apply(method, **args)).reset_index()[ self.id_field + [self.date_column, self.pred_column]]
