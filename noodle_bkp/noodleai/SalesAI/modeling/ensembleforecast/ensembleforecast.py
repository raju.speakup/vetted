"""Noodle's forecast module."""
# Packages for generic use
import pandas as pd
import numpy as np
import multiprocessing
from sklearn import metrics
import copy

import logging
logger = logging.getLogger(__name__)

# Model specific packages
# Prophet
from fbprophet import Prophet

# STL decompose
# !pip install stldecompose
from Pynotebooks.modeling.n_stldecompose.stl import decompose, forecast
from Pynotebooks.modeling.n_stldecompose.forecast_funcs import (naive, drift, mean, seasonal_naive)

# Auto ARIMA
# !pip install pyramid-arima
from pyramid.arima import auto_arima
import matplotlib.pyplot as plt

import plotly as py
import os
import plotly.graph_objs as go


class NoodleProphet(object):
    """Noodle's wrapper on FbProphet.

    Parameters
    ----------
    data_cls: class
              Object of Preprocess class with data

    name: string, default is 'prophet'
          Custom name of object while instantiating

    modelargs: dict, optional keyword arguments
          Additional parameters to be fed to the Prophet class
          Example:
            dict({
                growth: 'linear',
                changepoints: None,
                n_changepoints = 25,
                yearly_seasonality: 'auto',
                weekly_seasonality: 'auto',
                daily_seasonality: 'auto',
                holidays: None,
                seasonality_prior_scale: 10.0,
                holidays_prior_scale: 10.0,
                changepoint_prior_scale: 0.05,
                mcmc_samples: 0,
                interval_width: 0.80,
                uncertainty_samples: 1000
            })
          Visit the links below for more information:
          https://facebook.github.io/prophet/docs/saturating_forecasts.html
          https://github.com/facebook/prophet/blob/master/python/fbprophet/forecaster.py
    Note:
    -----
    Altered/ unaltered functions from the source code of FbProphet
    (https://github.com/facebook/prophet/tree/master/python) are used below
    Visit https://github.com/facebook/prophet/blob/master/LICENSE to view
    license and copyright notice

    Methods:
    --------
    train: Train Prophet model
    predict: Make predictions using the trained Prophet model
    copy: Copy the instantiated NoodleProphet object by slicing data
          (only along index)

    References:
    -----------
    https://github.com/facebook/prophet/blob/master/python/fbprophet/forecaster.py
    """

    def __init__(self, data_cls, name='prophet', **modelargs):
        """Instantiate the NoodleProphet object."""
        self.data_cls = data_cls
        self.data = data_cls.data
        self.name = name
        self.frequency = data_cls.frequency()
        self.modelargs = modelargs
        self.m = Prophet(**modelargs)

    def train(self):
        """Instantiate a Prophet object and call the fit method."""
        dat = self.data.reset_index()
        dat.columns = ['ds', 'y']
        self.m.fit(dat)
        return self

    def predict(self, horizon=16, as_dataframe=False):
        """Call the predict method of the fitted Prophet object.

        Parameters:
        -----------
        horizon: int, defualt is 16.
                 Required number of future predictions

        as_dataframe: bool, default is False
                      If True, returns predictions as a dataframe with datetime
                      index else as a list

        Returns:
        --------
        Predicted values for the specified horizon
        """
        pred = self.m.predict(self.m.make_future_dataframe(periods=horizon,
                                                           include_history=False,
                                                           freq=self.frequency))
        if(as_dataframe):
            horizon_dates = pd.date_range(start=self.data.index[-1],
                                          freq=self.data_cls.frequency(),
                                          periods=horizon + 1)[1:]
            pred_df = pd.DataFrame(pred['yhat'].values, index=horizon_dates,
                                   columns=['yhat_' + self.name])
            return pred_df
        else:
            return pred['yhat'].values

    def copy(self, cutoff=None):
        """Copy the NoodleProphet object.

        Parameters:
        -----------
        cutoff: pd.Timestamp, default is None.
                cuttoff Timestamp for slicing data (along axis).

        Returns:
        --------
        NoodleProphet class object with same parameters.
        """
        cutoff_cls = copy.deepcopy(self.data_cls)
        if not cutoff:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= self.data.index[-1]]
        else:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= cutoff]
        cutoff_args = self.modelargs
        if self.m.specified_changepoints:
            cutoff_args['changepoints'] = self.m.changepoints
            if cutoff is not None:
                # Filter change points '<= cutoff'
                cutoff_args['changepoints'] = cutoff_args['changepoints'][cutoff_args['changepoints'] <= cutoff]
        else:
            cutoff_args['changepoints'] = None
        model_cls = NoodleProphet(data_cls=cutoff_cls, **cutoff_args)
        return(model_cls)


class NoodleAutoArima(object):
    """Noodle's wrapper on pyramid's Auto Arima.

    Parameters:
    -----------
    data_cls: class
              Object of Preprocess class instantiated with data

    name: string, default is 'arima'
          Custom name of object while instantiating the class

    modelargs: dict optional keyword arguments
          Additional parameters to be fed to the Prophet class
          Example: Run ?auto_arima
          dict({
              'exogenous': None,
              'start_p': 2,
              'd': None,
              'start_q': 2,
              'max_p': 5,
              'max_d': 2,
              'max_q': 5,
              'start_P': 1,
              'D': None,
              'start_Q': 1,
              'max_P': 2,
              'max_D': 1,
              'max_Q': 2,
              'max_order': 10,
              'm':1,
              'seasonal': True,
              'stationary': False,
              'information_criterion': 'aic',
              'alpha': 0.05,
              'test': 'kpss',
              'seasonal_test': 'ch',
              'stepwise': True,
              'n_jobs': 1,
              'start_params': None,
              'trend': 'c',
              'method': None,
              'transparams': True,
              'solver'='lbfgs',
              'maxiter': 50,
              'disp': 0,
              'callback': None,
              'offset_test_args': None,
              'seasonal_test_args': None,
              'suppress_warnings': False,
              'error_action': 'warn',
              'trace': False,
              'random': False,
              'random_state': None,
              'n_fits': 10,
              'return_valid_fits': False,
              'out_of_sample_size': 0,
              'scoring': 'mse',
              'scoring_args': None,
            })
            Visit the links below for more information:
            https://github.com/tgsmith61591/pyramid/blob/master/pyramid/arima/auto.py

            Section containing usage of auto arima function in
            https://github.com/tgsmith61591/pyramid/blob/master/examples/quick_start_example.ipynb

    Methods:
    --------
    train: Train Arima model with optimal parameters
    predict: Make predictions using the trained Arima model
    copy: Copy the instantiated NoodleAutoArima object by slicing data
          (only along index)

    References:
    -----------
    https://github.com/tgsmith61591/pyramid/blob/master/pyramid/arima/auto.py
    """

    def __init__(self, data_cls, name='arima', **modelargs):
        """Instantiate the NoodleAutoArima object."""
        self.data_cls = data_cls
        self.data = data_cls.data
        self.name = name
        self.frequency = data_cls.frequency()
        self.stepwise_fit = None
        self.mod_func = auto_arima
        self.modelargs = modelargs

    def train(self):
        """Instantiate the autoarima class to find optimal parameters."""
        self.modelargs['y'] = self.data.values
        self.stepwise_fit = auto_arima(**self.modelargs)
        return(self)

    def predict(self, horizon=16, as_dataframe=False):
        """Call the predict method of instantiated autoarima object.

        Parameters:
        -----------
        horizon: int, defualt is 16.
                 Required number of future predictions

        as_dataframe: bool, default is False
                      If True, returns predictions as a dataframe with datetime
                      index else as a list

        Returns:
        --------
        Predicted values for the specified horizon
        """
        pred = self.stepwise_fit.predict(n_periods=horizon)
        if(as_dataframe):
            horizon_dates = pd.date_range(start=self.data.index[-1],
                                          freq=self.data_cls.frequency(),
                                          periods=horizon + 1)[1:]
            pred_df = pd.DataFrame(pred, index=horizon_dates,
                                   columns=['yhat_' + self.name])
            return pred_df
        else:
            return pred

    def copy(self, cutoff=None):
        """Copy the NoodleAutoArima object.

        Parameters:
        -----------
        cutoff: pd.Timestamp, default is None.
                cuttoff Timestamp for slicing data (along axis).

        Returns:
        --------
        NoodleAutoArima class object with same parameters.
        """
        cutoff_cls = copy.deepcopy(self.data_cls)
        if not cutoff:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= self.data.index[-1]]
        else:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= cutoff]
        model_cls = NoodleAutoArima(data_cls=cutoff_cls, name=self.name,
                                    **self.modelargs)
        return(model_cls)


class NoodleSES(object):
    """Noodle's wrapper on simple exponential smoothing.

    Parameters:
    -----------
    data_cls: class
              Preprocess class object
    name: string, default is 'ses'
          Custom name of object
    alpha: float
           Value of alpha

    Methods:
    --------
    train: Train Arima model with optimal parameters
    predict: Make predictions using the trained Arima model
    copy: Copy the instantiated NoodleAutoArima object by slicing data
          (only along index)
    """

    def __init__(self, data_cls, alpha, name='ses'):
        """Instantiate the NoodleSES class."""
        self.data_cls = data_cls
        self.data = data_cls.data
        self.name = name
        self.frequency = data_cls.frequency()
        self.result = []
        self.alpha = alpha

    def train(self):
        """Train the SES model."""
        self.result = [self.data.values[0]]
        for n in range(1, len(self.data.values)):
            self.result.append(self.alpha * self.data.values[n] + (1 - self.alpha) * self.result[n - 1])

        self.result = list(np.array(self.result).flat)
        return self

    def predict(self, horizon=16, as_dataframe=False):
        """Make predictions using the trained SES object.

        Parameters:
        -----------
        horizon: int, defualt is 16.
                 Required number of future predictions

        as_dataframe: bool, default is False
                      If True, returns predictions as a dataframe with datetime
                      index else as a list

        Returns:
        --------
        Predicted values for the specified horizon
        """
        y = self.result
        for i in range(0, horizon):
            y.append(self.alpha * y[-1] + (1 - self.alpha) * y[-2])

        y = list(np.array(y).flat)
        pred = y[-horizon:]
        if(as_dataframe):
            horizon_dates = pd.date_range(start=self.data.index[-1],
                                          freq=self.data_cls.frequency(),
                                          periods=horizon + 1)[1:]
            pred_df = pd.DataFrame(pred, index=horizon_dates,
                                   columns=['yhat_' + self.name])
            return pred_df
        else:
            return pred

    def copy(self, cutoff=None):
        """Copy the NoodleSES object.

        Parameters:
        -----------
        cutoff: pd.Timestamp, default is None.
                cuttoff Timestamp for slicing data (along axis).

        Returns:
        --------
        NoodleSES class object with same parameters.
        """
        cutoff_cls = copy.deepcopy(self.data_cls)
        if not cutoff:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= self.data.index[-1]]
        else:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= cutoff]
        model_cls = NoodleSES(data_cls=cutoff_cls, alpha=self.alpha,
                              name=self.name)
        return(model_cls)


class NoodleSTLF(object):
    """Noodle's wrapper on STL decomposition.

    Parameters:
    -----------
    data_cls: class
              Preprocess class object instantiated with data
    name: string, default is 'stlf'.
          Custom name of object
    period: int
            Most significant periodicity in the observed time series, in units
            of 1 observation. Ex: to accomodate strong annual periodicity
            within years of daily observations, ``period=365``.
    lo_frac: float, optional
             Fraction of data to use in fitting Lowess regression.
    lo_delta: float, optional
              Fractional distance within which to use linear-interpolation
              instead of weighted regression. Using non-zero ``lo_delta``
              significantly decreases computation time.
    fc_func: function
             Function which takes an array of observations and returns a single
             valued forecast for the next point
    seasonal: bool, default is True
              Include seasonal component in forecast

    Methods:
    --------
    train: Train Arima model with optimal parameters
    predict: Make predictions using the trained Arima model
    copy: Copy the instantiated NoodleAutoArima object by slicing data
          (only along index)

    References:
    -----------
    https://github.com/jrmontag/STLDecompose/blob/master/stldecompose/stl.py
    """

    def __init__(self, data_cls, period = None, name='stlf', lo_frac=0.6,
                 lo_delta=0.01, fc_func=drift, seasonal=True,
                 fc_func_kwargs={}):
        """Instantiate the NoodleSTLF class."""
        self.data_cls = data_cls
        self.data = data_cls.data
        self.name = name
        self.fc_func = fc_func
        self.seasonal = seasonal
        self.period = period
        self.lo_frac = lo_frac
        self.lo_delta = lo_delta
        self.fc_func_kwargs = fc_func_kwargs
        self.df_stlf = None

    def train(self):
        """Train the stlf model."""
        if self.period is None:
            freq_string = self.data_cls.periodicity(show_plot = False)[1]
            period_dict = dict({
                'weeks': 52,
                'days': 365,
                'hours': 8760,
                'minutes': 525600,
                'seconds': 31536000
            })
            self.period = period_dict[freq_string]
        self.df_stlf = decompose(self.data, period=self.period)
        return(self)

    def predict(self, horizon=16, as_dataframe=False):
        """Make predictions using the trained STLF object.

        Parameters:
        -----------
        horizon: int, defualt is 16.
                 Required number of future predictions

        as_dataframe: bool, default is False
                      If True, returns predictions as a dataframe with datetime
                      index else as a list

        Returns:
        --------
        Predicted values for the specified horizon
        """
        
        pred_stlf = forecast(self.df_stlf, steps=horizon, fc_func=self.fc_func,
                             seasonal=self.seasonal, **self.fc_func_kwargs)
        if(as_dataframe):
            horizon_dates = pd.date_range(start=self.data.index[-1],
                                          freq=self.data_cls.frequency(),
                                          periods=horizon + 1)[1:]
            pred_df = pd.DataFrame(pred_stlf.iloc[:, 0].values, index=horizon_dates,
                                   columns=['yhat_' + self.name])
            return pred_df
        else:
            return(pred_stlf.iloc[:, 0].values)

    def copy(self, cutoff=None):
        """Copy the NoodleSTLF object.

        Parameters:
        -----------
        cutoff: pd.Timestamp, default is None.
                cuttoff Timestamp for slicing data (along axis).

        Returns:
        --------
        NoodleSES class object with same parameters.
        """
        cutoff_cls = copy.deepcopy(self.data_cls)
        if not cutoff:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= self.data.index[-1]]
        else:
            cutoff_cls.data = cutoff_cls.data[cutoff_cls.data.index <= cutoff]
        model_cls = NoodleSTLF(data_cls=cutoff_cls, period=self.period,
                               name=self.name, fc_func=self.fc_func,
                               seasonal=self.seasonal)
        return(model_cls)


class Validation(object):
    """Compute validation scores of a model.

    Parameters:
    -----------
    model_cls: class
               A model class object
    k: int, default is 2
       Number of times validation has to be done.

    horizon: int, default is 4
             Size of validation set in each iteration

    period: int, default is 2
            Size of data to be added to training set in successive validation
            iterations

    Note:
    -----
    Altered/ unaltered functions from the source code of FbProphet
    (https://github.com/facebook/prophet/tree/master/python) are used below
    Visit https://github.com/facebook/prophet/blob/master/LICENSE to view
    license and copyright notice

    Methods:
    --------
    _cutoffs: Computes the value of time until which data is considered for
              training
    simulated_historical_forecasts: Generates predictions at each cutoff date
    cv_scores: Computes Unit MAPE, mean absolute error and RMSE of cv
               predictions
    """

    def __init__(self, model_cls, horizon=4, period=2,
                 k=2):
        """Instantiate the Validation class."""
        self.model_cls = model_cls
        self.data = model_cls.data
        self.horizon = horizon
        self.period = period
        self.k = k

    def _cutoffs(self, df, initial, horizon, k, period):
        """Generate cutoff dates.

        Parameters:
        -----------
        df: pd.DataFrame with historical data
        horizon: pd.Timedelta.
            Forecast horizon
        k: Int number.
            The number of forecasts point.
        period: pd.Timedelta.
            Simulated Forecast will be done at every this period.

        Returns:
        --------
        list of pd.Timestamp
        """
        # Last cutoff is 'latest date in data - horizon' date
        cutoff = initial
        if cutoff < df.index.min():
            raise ValueError('Less data than horizon.')
        result = [cutoff]

        for i in range(1, k):
            cutoff += period
            # If data does not exist in data range (cutoff, cutoff + horizon]
            if not (((df.index > cutoff) & (df.index <= cutoff + horizon)).any()):
                # Next cutoff point is 'last date before cutoff in data - horizon'
                closest_date = df[df.index <= cutoff].index.max()
                cutoff = closest_date - horizon
            if cutoff < df.index.min():
                logger.warning(
                    'Not enough data for requested number of cutoffs! '
                    'Using {}.'.format(i))
                break
            result.append(cutoff)

        # Sort lines in ascending order
        return result

    def simulated_historical_forecasts(self):
        """Simulated Historical Forecasts.

        Make forecasts from k historical cutoff points, working backwards from
        (end - horizon) with a spacing of period between each cutoff.
        Parameters:
        -----------

        horizon: string with pd.Timedelta compatible style, e.g., '5 days',
            '3 hours', '10 seconds'.
        k: Int number of forecasts point.
        period: Optional string with pd.Timedelta compatible style. Simulated
            forecast will be done at every this period. If not provided,
            0.5 * horizon is used.

        Returns:
        --------
        A pd.DataFrame with the forecast, actual value and cutoff.
        """
        df = self.data
        horizon = self.timedelta(self.horizon)
        period = self.timedelta(self.period)
        k = self.k
        initial = df.index.max() - (horizon + (k - 1) * period)
        cutoffs = self._cutoffs(df=df, initial=initial, horizon=horizon, k=k,
                                period=period)
        predicts = []
        for cutoff in cutoffs:
            # Generate new object with copying fitting options
            m = self.model_cls.copy(cutoff=cutoff)
            index_predicted = df[(df.index > cutoff) & (df.index <= cutoff + horizon)].index
            pred = pd.DataFrame(index=index_predicted)
            pred['cutoff'] = cutoff
            pred['yhat_' + self.model_cls.name] = m.train().predict(horizon=(self.horizon))
            pred['y'] = df[df.columns[0]][index_predicted]
            # pred['category'] = str(self.data.columns[0])
            pred = pred.reset_index().rename(columns={'index': 'ds'})
            predicts.append(pred)

        return pd.concat(predicts, ignore_index=True)

    def timedelta(self, num):
        """Convert int to timedelta."""
        n = 1#int((self.model_cls.data_cls.periodicity()[0]/np.timedelta64(1, 'w')).astype(int))
        p = self.model_cls.data_cls.periodicity()[1]
        freq_dict = dict({
            'weeks': '7 days',
            'days': '1 days',
            'hours': '1 hours',
            'minutes': '1 minutes',
            'seconds': '1 seconds'
        })
        p = freq_dict[p]
        return num * n * pd.Timedelta(p)

    def cv_scores(self):
        """Compute error metrics of cv predictions."""
        cv_pred = self.simulated_historical_forecasts()
        names = [self.model_cls.name]
        cv_score = pd.DataFrame(index=['Unit MAPE', 'Mean ABS', 'RMSE'], columns=names)
        for j in cv_score.columns:
            if sum(cv_pred['y']) == 0:
                cv_score[j]['Unit MAPE'] = np.inf
            else:
                cv_score[j]['Unit MAPE'] = 100 * sum(abs(cv_pred['y']-cv_pred['yhat_' + j])) / sum(cv_pred['y'])
            cv_score[j]['Mean ABS'] = metrics.mean_absolute_error(cv_pred['y'], cv_pred['yhat_' + j])
            cv_score[j]['RMSE'] = np.sqrt(metrics.mean_squared_error(cv_pred['y'],cv_pred['yhat_' + j]))
        return cv_score


class ParallelEnsemble(object):

    def __init__(self, data_cls, horizon=4, period=2, k=2, n_threads = 2,log_transform = False):
        self.data_cls = data_cls
        self.n_threads = n_threads
        self.horizon = horizon
        self.period = period
        self.k = k
        self.log_transform = log_transform

    def initialize(self, models_ensemble):
        self.models_manager = multiprocessing.Manager().dict()
        self.models_ensemble = models_ensemble
        for category in self.data_cls.data.columns :
            self.parallel_initialize(category)
        '''
        with multiprocessing.Pool(self.n_threads) as pool:
            _ = pool.map_async(self.parallel_initialize, self.data_cls.data.columns)
            pool.close()
            pool.join()
        '''
        self.models = dict(self.models_manager)
        return self

    def parallel_initialize(self, category):
        cat_cls = copy.deepcopy(self.data_cls)
        if self.log_transform:
            cat_cls.data = pd.DataFrame(cat_cls.data[category].apply(np.log))
        else :
            cat_cls.data = pd.DataFrame(cat_cls.data[category])

        train_models = []
        model_dict = dict({
            'prophet': NoodleProphet,
            'ses': NoodleSES,
            'autoarima': NoodleAutoArima,
            'stlf': NoodleSTLF
        })

        for each_model in self.models_ensemble:
            params = self.models_ensemble[each_model]
            params['data_cls'] = cat_cls

            train_models.append(model_dict[each_model](**params))

        self.models_manager[category] = WeightedEnsemble(models=train_models,
                                                 horizon=self.horizon,
                                                 period = self.period,
                                                 k = self.k, log_transform=self.log_transform)

    def cv_scores(self):
        self.cv_list = multiprocessing.Manager().list()

        with multiprocessing.Pool(self.n_threads) as pool:
            _ = pool.map_async(self.parallel_cv_scores, self.data_cls.data.columns)
            pool.close()
            pool.join()

        self.cv_df = pd.concat(self.cv_list, ignore_index=True)
        return self.cv_df

    def parallel_cv_scores(self, category):
        cvs = self.models[category].cv_scores().reset_index().rename(columns={'index':'metric'})
        cvs['category'] = str(category)
        self.cv_list.append(cvs)

    def train(self):
        for category in self.data_cls.data.columns:
            self.models[category].train()

    def get_weights(self):
        self.weight_df = pd.DataFrame()
        for category in self.data_cls.data.columns:
            self.weight_df = self.weight_df.append(pd.Series(self.models[category].weights, name=category))

        self.weight_df = self.weight_df.transpose()

        #self.weight_df.columns = pd.MultiIndex.from_tuples(self.weight_df.columns)

        return self.weight_df

    def predict(self, pred_horizon):
        self.predict_df = pd.DataFrame()
        for category in self.data_cls.data.columns:
            preds = self.models[category].predict(pred_horizon = pred_horizon, as_dataframe=True)
         #   preds.columns = pd.MultiIndex.from_tuples([category])
            preds.columns = [category]
            self.predict_df = pd.concat([self.predict_df, preds], axis=1)

        return self.predict_df

    def calculate_metrics(self, predictions, actuals):
        self.metrics_df = pd.DataFrame()

        for category in self.data_cls.data.columns:
            metrics_dict = {}
            y_hat = predictions[category].values
            y_act = actuals[category].values

            metrics_dict['Unit MAPE'] = 100 * sum(abs(y_act - y_hat)) / sum(y_act)
            metrics_dict['Mean ABS'] = metrics.mean_absolute_error(y_act, y_hat)
            metrics_dict['RMSE'] = np.sqrt(metrics.mean_squared_error(y_act, y_hat))

            self.metrics_df = self.metrics_df.append(pd.Series(metrics_dict, name=category))
        self.metrics_df = self.metrics_df.transpose()
        #self.metrics_df.columns = pd.MultiIndex.from_tuples(self.metrics_df.columns)

        return self.metrics_df

    def visualize(self, predictions, actual = None, download = False, folder_name = 'output'):

        if not download:
            for category in self.data_cls.data.columns:
                layout = go.Layout(
                    title='Predictions for ' + str(category),
                    xaxis=dict(title='Measure of time'),
                    yaxis=dict(title='Value')
                )

                all_traces = []

                if not actual is None:
                    all_traces.append(
                        go.Scatter(
                            x=actual.index,
                            y=actual[category].values,
                            name=str(category) + ' (Actual)'
                        )
                    )

                all_traces.append(
                    go.Scatter(
                    x = predictions.index,
                    y = predictions[category].values,
                    name = 'Predicted'))

                py.offline.iplot(go.Figure(data=all_traces, layout=layout))
        else:
            #self.dload = os.path.expanduser(directory)
            save_dir =  "/" + folder_name
            if os.path.exists(save_dir):
                pass
            else:
                os.mkdir( '/' + folder_name)

            for category in self.data_cls.data.columns:
                plt.figure(figsize=(20, 12))
                if not actual is None:
                    plt.plot(actual.index, actual[category].values, label = str(category) + ' (Actual)')
                plt.plot(predictions.index, predictions[category].values, label = 'Predicted')
                plt.title('Predictions for ' + str(category))
                plt.xlabel('Measure of time')
                plt.ylabel('Value')

                plt.savefig("/" + folder_name + "/" + str(category) + '.png')
                plt.close()


class WeightedEnsemble(object):
    """Formulated at Noodle.

    Description:
    ------------
    * This ensemble method can have any number of models
    * Each model is given a weight which is a function of its validation
      error
    * Currently, the weight of a model is the inverse of its mean absolute
      validation error
    * There can be upgrades/ modifications to the weighing scheme in future
    * Validation methods are integrated in order to compute weights

    Parameters:
    -----------
    models: class (array of model classes)
            Array of model class objects
    name: string, default is 'weighted_ensemble'
          Custom name of object
    k: int, default is 2
       Number of times validation has to be done.

    horizon: int, default is 4
             Size of validation set in each iteration

    period: int, default is 2
            Size of data to be added to training set in successive validation
            iterations

    Note:
    -----
    Weights are the functions of average validation errors

    Methods:
    --------
    train: Compute weights of models in ensemble
    predict: Make predictions using trained model objects and their weights
    cv_scores: Compute cv scores of models in ensemble
    """

    def __init__(self, models, name='weighted_ensemble',
                 horizon=4, period=2, k=2, log_transform=False):
        """Instantiate the model class."""
        self.models = models
        self.horizon = horizon
        self.period = period
        self.k = k
        self.name = name
        self.weights = dict()
        self.log_transform = log_transform

    def train(self):
        """Training the models in ensemble."""
        mabs = self.cv_scores()
        for col in mabs.columns:
            self.weights[col] = 1 / mabs[col]['Mean ABS']
        for m in self.models:
            m.train()
        return self

    def predict(self, pred_horizon, as_dataframe=True):
        """Make future predictions using the trained model objects.

        Parameters:
        -----------
        pred_horizon: int
                      Required number of future predictions

        as_dataframe: bool, default is True
                      If True, returns predictions as a dataframe with datetime
                      index else as a list
        Returns:
        --------
        Predictions of all models and ensemble model for the speicified horizon
        """
        names = []
        for m in self.models:
            names.append(m.name)
        final_pred = np.zeros(pred_horizon)
        #models_pred = pd.DataFrame(index=range(pred_horizon), columns = names)
        for index, nam in enumerate(names):
            #print('Weight of ' + nam + ':', self.weights[nam])
            #models_pred[nam] = self.models[index].predict(horizon=pred_horizon)
            final_pred += np.array(self.models[index].predict(horizon=pred_horizon)) * self.weights[nam]
            #final_pred += models_pred[nam] * self.weights[nam]

        final_pred = final_pred / sum(self.weights.values())
        if self.log_transform:
            final_pred = np.apply_along_axis(np.exp,0,final_pred)
        if(as_dataframe):
            horizon_dates = pd.date_range(start=self.models[0].data.index[-1],
                                          freq=self.models[0].data_cls.frequency(),
                                          periods=pred_horizon + 1)[1:]
            pred_df = pd.DataFrame(final_pred, index=horizon_dates,
                                   columns=['yhat_' + self.name])
#             for nam in names:
#                 pred_df[nam] = models_pred[nam]
            return pred_df
        else:
            return final_pred

    def cv_scores(self):
        """Compute cv scores of all models in ensemble."""
        cv_pred = Validation(model_cls=self.models[0], horizon=self.horizon,
                             k=self.k,
                             period=self.period).simulated_historical_forecasts()
        names = [self.models[0].name]
        if(len(self.models) > 1):
            for m in self.models[1:]:
                v = Validation(model_cls=m, horizon=self.horizon, k=self.k,
                               period=self.period)
                cv_pred = cv_pred.merge(v.simulated_historical_forecasts(),
                                        on=['ds', 'cutoff', 'y'], how='left')
                names.append(m.name)

        cv_score = pd.DataFrame(index=['Unit MAPE', 'Mean ABS', 'RMSE'], columns=names)
        for j in cv_score.columns:
            cv_score[j]['Unit MAPE'] = 100 * sum(abs(cv_pred['y']-cv_pred['yhat_' + j])) / sum(cv_pred['y'])
            cv_score[j]['Mean ABS'] = metrics.mean_absolute_error(cv_pred['y'], cv_pred['yhat_' + j])
            cv_score[j]['RMSE'] = np.sqrt(metrics.mean_squared_error(cv_pred['y'],cv_pred['yhat_' + j]))
        return cv_score


class ParallelModelling(object):
    """Parallelize the modelling process.

    Parameters:
    -----------
    data_cls: class
              Preprocess class object
    model: string
           'prophet': NoodleProphet
           'ses': NoodleSES
           'autoarima': NoodleAutoArima
           'stlf': NoodleSTLF

    params: dict
            input parameters for the chosen model

    n_threads: int
            Number of cores

    horizon: int
            Prediction horizon

    Methods:
    --------
    train: Train model objects of all categories in parallel
    predict: Make predictions using trained objects of all categories
             in parallel
    """

    def __init__(self, data_cls, model, params, n_threads=2, horizon=16):
        """Unleash the multiprocessing capability."""
        self.objects = multiprocessing.Manager().list()
        self.predictions = multiprocessing.Manager().list()
        self.n_threads = n_threads
        self.data_cls = data_cls
        self.model = model
        self.params = params
        self.horizon = horizon

    def train(self):
        """Train the model objects of all categories."""
        with multiprocessing.Pool(self.n_threads) as pool:
            pool.map_async(self.parallel_train, self.data_cls.data.columns)
            pool.close()
            pool.join()
        self.objects = list(self.objects)
        return self

    def parallel_train(self, category):
        """Call the basic train method of model class for all categories."""
        cat_cls = copy.deepcopy(self.data_cls)
        cat_cls.data = cat_cls.data[category]
        self.params['data_cls'] = cat_cls
        self.params['name'] = str(category)
        model_dict = dict({
            'prophet': NoodleProphet,
            'ses': NoodleSES,
            'autoarima': NoodleAutoArima,
            'stlf': NoodleSTLF
        })
        a = model_dict[self.model](**self.params)
        print(a.name)
        self.objects.append(a.train())
        self.objects = list(self.objects)

    def predict(self):
        """Make predictions for all categories in parallel."""
        with multiprocessing.Pool(self.n_threads) as pool:
            pool.map_async(self.parallel_predict, self.objects)
            pool.close()
            pool.join()
        final_pred = pd.concat(self.predictions, axis=1)
        return final_pred

    def parallel_predict(self, obj):
        """Call the basic predict method of model class for all categories."""
        self.predictions.append(obj.predict(horizon=self.horizon,
                                            as_dataframe=True))


class ParallelValidation(object):
    """Parallelize the validation process for all categories.

    Parameters:
    -----------
    data_cls: class
              Preprocess class object
    model: string
           'prophet': NoodleProphet
           'ses': NoodleSES
           'autoarima': NoodleAutoArima
           'stlf': NoodleSTLF

    v_params: dict
            input parameters for the Validation class
            (horizon, period and k)

    m_params: dict
              input parameters for the model class

    n_threads: int
            Number of cores

    Methods:
    --------
    cv_scores: Returns Unit MAPE, Mean Absoulte Error and RMSE for all
               categories as a dataframe
    """

    def __init__(self, data_cls, model, v_params, m_params, n_threads=2):
        """Unleash the multiprocessing capability for validation."""
        self.model = model
        self.n_threads = n_threads
        self.v_params = v_params
        self.m_params = m_params
        self.data_cls = data_cls
        self.validation_dfs = multiprocessing.Manager().list()
        self.model = model

    def cv_scores(self):
        """Compute cv_scores for each category and append the results."""
        with multiprocessing.Pool(self.n_threads) as pool:
            pool.map_async(self.parallel_validation, self.data_cls.data.columns)
            pool.close()
            pool.join()
        return(pd.concat(self.validation_dfs, ignore_index=True))

    def parallel_validation(self, category):
        """Call the cv_scores method for each category."""
        cat_cls = copy.deepcopy(self.data_cls)
        cat_cls.data = cat_cls.data[category]
        model_dict = dict({
            'prophet': NoodleProphet,
            'ses': NoodleSES,
            'autoarima': NoodleAutoArima,
            'stlf': NoodleSTLF
        })
        self.m_params['data_cls'] = cat_cls
        m = model_dict[self.model](**self.m_params)
        self.v_params['model_cls'] = m
        v = Validation(**self.v_params)
        cvs = v.cv_scores().reset_index().rename(columns={'index': 'metric'})
        cvs['category'] = str(category)
        self.validation_dfs.append(cvs)
