import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools

class shapley():
    def __init__(self):
        self.shap_data = pd.DataFrame()
        
    def generate_shap_data(self,train, filter_by):
        '''
        Generate the shape data which mainly consists of shapley values

        :param train: Training data
        :param filter_by: filterby a dictionary of id columns and their values
        :return:
        '''
        self.shap_data = train
        filter_data = pd.DataFrame()
        for k,v in filter_by.items():
            if all(isinstance(item, tuple) for item in v):
                for each_tuple in v:
                    date_data = self.shap_data
                    new_date_data = date_data[(self.shap_data[k] >= each_tuple[0]) & (self.shap_data[k] <= each_tuple[1])]
                    filter_data = filter_data.append(new_date_data)
            else:
                filter_data = train
                filter_data = filter_data[train[k].isin(v)]
        return filter_data

    def generate_features(self, train):
        '''
        Generate  the same list of training features

        :param train: Training data
        :return:
        '''
        new_train = train.select_dtypes(include=['float32'])
        return new_train
    
    def plot_by_id(self, train,filter_by,shapley_data,explainer,shap,tracker,summary_plot_name):
        '''
        Plot for ever filtered option the top most affecting variables

        :param train: Training data
        :param filter_by: filtered by dictionary
        :param shapley_data: dataset of teh shapely values
        :param explainer: shap explainer object
        :param shap: shap object
        :param tracker: experiment tracker
        :param summary_plot_name: summary plot name
        :return:
        '''
        filter_list = list(itertools.product(*list(filter_by.values())))
        filter_list = [list(elem) for elem in filter_list]
        filter_list_keys = list(filter_by.keys())
        print(filter_list_keys)
        for i in range(len(filter_list)):
            filtered_shap_data = shapley_data[shapley_data[filter_list_keys]
                                              .apply(lambda x: list(x.values)==filter_list[i],axis=1)]
            if filtered_shap_data.empty:
                print(str(i) + " :  There are no such values in the ID cols")
                pass
            else:
                filtered_shap_data = filtered_shap_data.select_dtypes(include=['float32'])
                shap_values = explainer.shap_values(filtered_shap_data)
                shap.summary_plot(shap_values, filtered_shap_data, plot_type="bar",show = False)
                plt.savefig(tracker.plot_folder+"/"+summary_plot_name+"_"+str(i)+'.png')
                
                
                
                
                