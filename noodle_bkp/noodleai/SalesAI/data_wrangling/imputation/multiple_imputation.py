import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
from numpy import array
import math
from statistics import mode, StatisticsError
import matplotlib.pyplot as plt
from pandas import DataFrame, merge


class mts_imputation(object):
    """ The main imputation object

    The mts_imputation is an python Class which provides methods for multiple time series imputation.
    
    'mts_imputation' allows its user to identify the dominant frequency of an individual time series or for the whole
    dataset, presence of any missing values in date/time column and impute them.

    """
    def __init__(self, data, time_column, value_column, id_cols):
        """ Initializing the class variables.

        :param data: pandas dataFrame
        :param time_column: date/time column in data
        :param value_column: target/value column in data
        :param id_cols: list of unique ids in the data. each unique id represents a unique time series.

        """
        self.data = data
        self.time_column = time_column
        self.value_column = value_column
        self.id_cols = id_cols
            
            
    def sampling_frequency(self, ts_group, show_plot=True):
        """ 
        This function is used to get the dominant sampling frequency for the whole data and also for individual time
        series. In a data with multiple time series some timeseries might differ from other in sampling frequency. Thus
        by printing histogram for the whole data the user can understand the sampling frequency across data and get the
        most dominant. In case of no dominant frequency a fatal error will be raised.
            
        :param ts_group: time series groups for plotting sampling frequency histogram.
            
        :return: int,dominant frequency across data
            
        """
        output = pd.DataFrame()
        self.groups = self.data.groupby(self.id_cols)
        for i, group in self.groups:
            array= group[self.time_column].unique()
            time_diff_array=[(np.datetime64(array[i+1])-np.datetime64(array[i])) for i in range(len(array)-1)]
            self.max_days = mode([time_diff_array[i] / np.timedelta64(1, 'D') for i in range(len(time_diff_array))])
            max_secs = mode([time_diff_array[i] / np.timedelta64(1, 's') for i in range(len(time_diff_array))])
            if self.max_days >= 1:
                time_diff_output = [time_diff_array[i]/ np.timedelta64(1, 'D') for i in range(len(time_diff_array))]
                freq='days'
                value = int(self.max_days)
            else:
                time_diff_output=[(time_diff_array[i]/ np.timedelta64(1, 's')) for i in range(len(time_diff_array))]
                freq='seconds'
                value = int(max_secs)
               
            result = pd.Series(value)
            output = output.append(result,ignore_index=True)
            if show_plot:
                if ts_group:
                    for j in ts_group:
                        if len(str(j[0]))>1:
                            j=tuple(eval(str(j)))
                        else:
                            j=j[0]
                        if i==j:
                            ts_title = 'sampling frequency for'+' '+str(i)
                            df = time_diff_output
                            self.plot_hist(df, freq, ts_title)
                            
                
        output.columns = ['sampling_frequency']
        if show_plot:
            if ts_group==[]:
                ts_group = tuple(eval(str(ts_group)))
                ts_title = "Histogram of Sampling Frequency for all time series combined"
                df = output['sampling_frequency']
                self.plot_hist(df, freq,ts_title)

        return(int(max_secs))
        
        
    def plot_hist(self,df, freq, ts_title):
        """
        This function is used to plot histogram amd is being called into 'sampling_frequency' function to plot sampling
        frequency histogram. The function uses mode to identify the dominant frequency and if unique dominant frequency
        is not identified an error is raised.
        """
        plt.hist(df)
        plt.title(ts_title)
        plt.xlabel(freq)
        plt.ylabel("Frequency")
        plt.show()
        try:
            if self.max_days >= 1:
                if mode(df) == 1:
                    print("Dominant Frequency is %d day." % (mode(df)))
                else:
                    print("Dominant Frequency is %d days." % (mode(df)))
            else:
                print("Dominant Frquency is %d seconds." % (mode(df)))
        except StatisticsError:
            print("There is no Dominant Frequency!")
                
                
    def generating_date(self,time_column,dominant_frequency):
        """
        This function is responsible for printing all the dates/times between a given time period based on the dominant
        frequency. This function is called into the main function 'missing_value'.
        
        :param time_column: date/time column containing all the dates for the whole data
        :param dominant_frequency: dominant frequency in seconds
        
        :return: pandas dataframe containing time_column with all the dates including missing as well and a key column.
        """
        data = self.data[[time_column]]
        unique_dates = data[time_column].unique()
        start_date = min(unique_dates)
        end_date = max(unique_dates)
        delta = np.timedelta64(dominant_frequency,'s')
        result= []
        for n in self.generating_date_fun(start_date, end_date, delta):
            result.append(n)    
        output= pd.DataFrame(columns=[time_column])
        output[time_column] = result
        count = output.count()
        for i in count:
            output.insert(loc=1,column='key',value=1)
        output.drop_duplicates(keep='first',inplace=True)
        return(output)
        
    def generating_date_fun(self, start, end, delta):
        """
        The function responsible for printing all the dates and is called into 'generating_date' function.

        :param start: starting date/time stamp
        :param end: ending date/time stamp
        :param delta: numpy timedelta object. dominant frequency in seconds
        """
        curr = start
        while curr <= end:
            yield curr
            curr += delta
        
    def data_idcols(self,id_cols):
        """
        This function is used to generate a pandas dataframe with all the unique ids. Also a key column is attached to
        the dataframe. The function is further called into 'missing_value'.

        :param id_cols: list of unique ids in the data. each unique id represents a unique time series.

        :return: pandas dataframe.
        """
        self.result = self.data[id_cols]
        self.result.drop_duplicates(keep='first',inplace=True)
        self.result['key']=np.nan
        self.result = self.result.fillna(int(1))
        return(self.result)
        
        
    def missing_value(self, dominant_frequency):
        """
        The main function that is used to find the missing dates/time stamps in the data. The flow is like this:
        1. the pandas dataframe from 'generating_date' containing all the dates cross merged with pandas dataframe from
           'data_idcols' to get an output containing all the datapoints. Each date/time in time_column is cross merged
           with all the unique ids.
        2. Now this dataframe is left merged with the raw data and the dates which are not present in the raw data will
           get NaN in all the columns corresponding to it except for id_cols column.
           
        :param dominant_frequency: dominant frequency in seconds
        
        :return: pandas dataframe
        """
        output = merge(self.generating_date(self.time_column, dominant_frequency),self.data_idcols(self.id_cols),on='key')
        output = output.drop(['key'],axis=1)
        column = output.columns.values.tolist()
        self.final = pd.merge(output,self.data,on=column,how='left')
        return(self.final)
        
        
    def imputation_method(self, parameter, arg):
        """
        This function is used for imputation. the user have a choice to choose the method for imputation. the imputation
        is done for each time series group individually. In case of 'forward fill', 'backward fill' and
        'linear interpolation' it is possible that the the function might not be able to impute every missing value
        because of the location of the missing value.
        
        :param parameter: str,type of imputation technique. default is mean.
        :param arg: id_cols (for mean,median,mode,ffill,bfill,interpolation
                    int, a value e.g. arg = 0 (for parameter = 'value')
        
        :return: imputed pandas dataframe
        """
        data = self.final
        column = self.value_column
        if parameter == 'mean':  # arg=id_cols
            data = data.groupby(arg).apply(lambda group: group.fillna(group[column].mean())).reset_index(drop=True)
            return(data)
        elif parameter == 'median':  # arg=id_cols
            data = data.groupby(arg).apply(lambda group: group.fillna(group[column].median())).reset_index(drop=True)
            return(data)
        elif parameter == 'mode': # arg=id_cols
            data = data.groupby(arg).apply(lambda group: group.fillna(group[column].mode()[0]))
            return(data)
        elif parameter == 'value': # arg=value
            data[column] = data[column].fillna(arg)
            return(data)
        elif parameter =='ffill': # arg=id_cols
            data = data.groupby(arg).apply(lambda group: group.fillna(group.ffill())).reset_index(drop=True)
            if data[column].isnull().sum() != 0:
                raise Exception('Forward Fill needs a non missing value before current missing value to calculate it.') 
            return(data)
        elif parameter == 'bfill': # arg=id_cols
            data = data.groupby(arg).apply(lambda group: group.fillna(group.bfill())).reset_index(drop=True)
            if data[column].isnull().sum() != 0:
                raise Exception('Backward Fill needs a non missing value after current missing value to calculate it.')
            return(data)
        elif parameter == 'interpolation': # arg=id_cols
            data = self.Interpolation(data, arg, column)
            if data[column].isnull().sum() != 0:
                raise Exception('Interpolation requires non misising values both before and after a missing value to calculate it.') 
            return(data)
    

    def Interpolation(self, data, arg, column):
            output = pd.DataFrame()
            for i, group in data.groupby(arg):
                group[column].interpolate(method='linear', axis=0, inplace =True)
                output = output.append(group)
            return(output)
