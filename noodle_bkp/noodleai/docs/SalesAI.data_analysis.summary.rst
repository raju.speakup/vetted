SalesAI.data\_analysis.summary package
======================================

Submodules
----------

SalesAI.data\_analysis.summary.summary module
---------------------------------------------

.. automodule:: SalesAI.data_analysis.summary.summary
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: SalesAI.data_analysis.summary
    :members:
    :undoc-members:
    :show-inheritance:
