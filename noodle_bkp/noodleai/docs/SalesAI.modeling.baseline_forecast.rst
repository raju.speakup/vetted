SalesAI.modeling.baseline\_forecast package
===========================================

Submodules
----------

SalesAI.modeling.baseline\_forecast.baseline\_forecast module
-------------------------------------------------------------

.. automodule:: SalesAI.modeling.baseline_forecast.baseline_forecast
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: SalesAI.modeling.baseline_forecast
    :members:
    :undoc-members:
    :show-inheritance:
