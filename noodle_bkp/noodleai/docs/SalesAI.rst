SalesAI package
===============

Subpackages
-----------

.. toctree::

    SalesAI.data_analysis
    SalesAI.data_wrangling
    SalesAI.feature_engineering
    SalesAI.modeling

Module contents
---------------

.. automodule:: SalesAI
    :members:
    :undoc-members:
    :show-inheritance:
