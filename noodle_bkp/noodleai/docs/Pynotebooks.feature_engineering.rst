Pynotebooks.feature\_engineering package
========================================

Subpackages
-----------

.. toctree::

    Pynotebooks.feature_engineering.dimensionalityreduction
    Pynotebooks.feature_engineering.featureengineering

Module contents
---------------

.. automodule:: Pynotebooks.feature_engineering
    :members:
    :undoc-members:
    :show-inheritance:
