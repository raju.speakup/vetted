SalesAI.modeling.performance\_eval package
==========================================

Submodules
----------

SalesAI.modeling.performance\_eval.evaluation module
----------------------------------------------------

.. automodule:: SalesAI.modeling.performance_eval.evaluation
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: SalesAI.modeling.performance_eval
    :members:
    :undoc-members:
    :show-inheritance:
