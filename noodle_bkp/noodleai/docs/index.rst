.. Pynotebooks documentation master file, created by
   sphinx-quickstart on Tue Apr 10 14:55:55 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Noodle.ai Notebook's documentation!
==============================================

.. toctree::
   :maxdepth: 10
   :caption: Contents:

   Pynotebooks
   SalesAI
   html


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
