---
title: "Univariate Analysis"
output: html_notebook
---


Source Utility modules and Base Plotting modules
```{r}


source('~/noodleai/Rnotebooks/utilities/utils.R')
source('~/noodleai/Rnotebooks/dataexploration/base_plots.R')

```
Read Sample Data and Convvert to data.table
```{r}
data(mtcars)
mtcars<-as.data.table(mtcars)

data(HairEyeColor)
HairEyeColor<-as.data.table(HairEyeColor)
```


Histogram
```{r}
plot_histogram(mtcars,"mpg")
```

Boxplot
```{r}
plot_boxplot(mtcars,"mpg")
```

Frequency/Bar plot
```{r}
mtcars[,cyl:=as.factor(cyl)]
plot_frequency(mtcars,"cyl")
```




