from .vrae import VRAE
from .utils import *
import argparse


parser = argparse.ArgumentParser(description='Variational Recurrent AutoEncoder')
parser.add_argument('-hs', '--hidden-size', help = 'Hidden size of RNN', default = 90, type = int, required = False)
parser.add_argument('-hd', '--hidden-layer-depth', help = 'Hidden layer depth of RNN', default = 2, type = int, required = False)
parser.add_argument('-ll', '--latent-length', help = 'Length of the latent vector', default = 20, type = int, required = False)
parser.add_argument('-bs', '--batch-size', help = 'Batch size per epoch', default = 32, type = int, required = False)
parser.add_argument('-lr', '--learning-rate', help = 'Learning rate', default = 0.005, type = float, required = False)
parser.add_argument('-n', '--n-epochs', help = 'Number of epochs', default = 5, type = int, required = False)
parser.add_argument('-d', '--dropout-rate', help = 'Encoder dropout rate', type = float, default = 0.2,required = False)
parser.add_argument('-o', '--optimizer', help = 'Optimizer to be used', default = 'Adam',required = False)
parser.add_argument('-g', '--cuda', help = 'Boolean, GPU to be used or not', default = False, type = bool, required = False)
parser.add_argument('-p', '--print-every', help = 'Print output every p iterations', default = 100,type = int, required = False)
parser.add_argument('-c', '--clip', help = 'Clips the gradients, specified by max_grad_norm', type = int, default = True,required = False)
parser.add_argument('-m', '--max-grad-norm', help = 'Amount to be clipped by', default = 5, type = int, required = False)
parser.add_argument('-lo', '--loss', help = 'Loss function to be used', default = 'ReconLoss',required = False)
parser.add_argument('-b', '--block', help = 'Basic building block of encoder/decoder', default = 'LSTM',required = False)
parser.add_argument('-dl', '--dload', help = 'Download directory', default = '.', required = False)

args = parser.parse_args()
args_dict = vars(args)
print(args_dict)

dataset = NoodleDataset(csv_file='../../../data/pg_dc.csv',
                        time_column='week_start',
                        value_column='quantity',
                        id_cols=['pg','dc'],
                        transform=QuantileTransformer(),
                        sort_dates=True)

sequence_length = dataset.ts_class.data.shape[0]

vrae = VRAE(sequence_length=sequence_length, **args_dict)

vrae.fit_transform(dataset, save = True)
