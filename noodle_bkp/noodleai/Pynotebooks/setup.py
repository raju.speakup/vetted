from distutils.core import setup
from setuptools import setup, find_packages

setup(
    # Application name:
    name="pynotebooks",

    # Version number (initial):
    version="1.0.0",

    # Application author details:
    author="Pooja Balusani",
    author_email="pooja.balusani@noodle.ai",

    # Packages
    packages=find_packages(),

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://192.168.2.129:8081/repository/python-local/",

    #
    # license="LICENSE.txt",
    description="Library for Data Science Notebook Mobules on BAT platform",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "numpy==1.14.5",
        "pandas==0.21.0",
        "plotly==2.2.3",
        "scikit-learn==0.19.1",
        "statsmodels==0.9.0",
        "networkx==2.0",
        "matplotlib==2.1.0",
        "scipy==1.0.0",
        "tabulate==0.7.7",
        "prettytable==0.7.2",
        "pyts==0.5",
        "librosa==0.6.0",
        "stldecompose==0.0.3",
        "fitter==1.0.8",
        "numpydoc==0.7.0",
        "gitpython==2.1.9",
        "seaborn==0.8",
        "qgrid==1.0.5",
        "PyWavelets==0.5.2"
    ],
)
