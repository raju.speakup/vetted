import pandas as pd
import numpy as np
# from pyspark.sql.functions import lag, col
# from pyspark.sql.window import Window
import matplotlib.pyplot as plt
from plotly.graph_objs import *
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import seaborn as sns
import os

plt.rcParams["figure.figsize"] = (20, 14)
init_notebook_mode(connected=True)
sns.set(color_codes=True)


class CrossCorrelation(object):
    """ The main CrossCorrelation object.

    The CrossCorrelation is an python Class which provides methods
    for cross-correlation  .
    Cross-correlation is the degree of similarity between two time series
    at different times where lag can be considered.

    :param data: DataFrame,
                 data.
    :param lower_lag: int or None,
                      lower limit for the lag value.
    :param upper_lag: int or None,
                      upper limit for the lag value.
    :param topK: int or None,
                 K value to retrieve top K highly correlated column pairs.
    :param time_column: str or None,
                        time dependent column name.
    :param columns: list of column names.
    :param dload: str,
                  download location path.
    :param using_spark: bool,
                        True if using sparknotebook else False.

    :return: :class:`CrossCorrelation` object

    Usage::
            For Sparknotebooks :

            >>> sqlContext = SQLContext(sc)
            >>> df = sqlContext.read.csv(path=file_name, header="true",inferSchema="true")
            >>> cc = CrossCorrelation(df,lower_lag,upper_lag,topK,time_column,columns,dload,using_spark)

            For Non-spark version :

            >>> df = pd.read_csv(file_name, parse_dates=[time_column])
            >>> cc = CrossCorrelation(df, lower_lag, upper_lag, topK , time_column , columns , dload)

    """

    def __init__(self, data=None, lower_lag=None, upper_lag=None, topK=None, time_column=None, columns=None, dload=None,
                 using_spark=False):
        self.df = data
        self.lower_lag = lower_lag
        self.upper_lag = upper_lag
        self.topK = topK
        self.time_column = time_column
        self.columns = columns
        self.dload = dload
        self.using_spark = using_spark
        '''
        if using_spark:
            pyspark = __import__('pyspark.sql.functions',globals=globals())
            self.lag = pyspark.lag
            self.col = pyspark.col
            pyspark = __import__('pyspark.sql.window',globals=globals())
            self.Window = pyspark.Window
        '''

    def plot_correlation(self, df, zmin=-1, zmax=1, engine='plotly', download=False, folder_name='correlation'):
        """ Plots correlation heatmap.

        :param df: pandas DataFrame,
                   dataframe of correlation values matrix.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.

        """

        def __plot_correlation_matplotlib(corr_df, download, folder_name):
            """ Generate plots using matplotlib library.

            """
            save_dir = self.dload + "/" + folder_name
            sns.heatmap(corr_df.astype(float), xticklabels=corr_df.columns.values, yticklabels=corr_df.columns.values)
            if download:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" + "correlation" + '.png')
            else:
                plt.show()

        def __plot_correlation_plotly(corr_df, zmin=-1, zmax=1):
            """ Generate plots using plotly library.

            :param zmin: int,
                         Sets the lower bound of color domain..
            :param zmax: int,
                         Sets the upper bound of color domain..
            """

            corr_list = corr_df.values.tolist()
            trace = Heatmap(
                z=corr_list,
                x=corr_df.index.values.tolist(),
                y=corr_df.index.values.tolist(),
                colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"],
                            [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"],
                            [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"],
                            [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
                zmax=zmax,
                zmin=zmin
            )

            data = Data([trace])
            layout = Layout(
                autosize=True,
                margin=dict(r=240, t=30, b=180, l=180, pad=2),
                showlegend=False,
                xaxis=XAxis(
                    autorange=True,
                    linecolor='rgb(207, 226, 243)',
                    linewidth=8,
                    mirror=True,
                    nticks=corr_df.shape[0],
                    range=[0, corr_df.shape[0] + 1],
                    showline=True,
                    title="",
                    tickangle='40'
                ),
                yaxis=YAxis(
                    autorange=True,
                    linecolor='rgb(207, 226, 243)',
                    linewidth=8,
                    mirror=True,
                    nticks=corr_df.shape[0],
                    range=[0, corr_df.shape[0] + 1],
                    showline=True,
                    title='',
                    tickangle='-30'
                )
            )
            fig = Figure(data=data, layout=layout)
            return fig

        if (download == False) & (engine == 'plotly'):
            fig = __plot_correlation_plotly(df, zmin, zmax)
            iplot(fig)
        if (download) & (engine == 'plotly'):
            print("Can't download plotly plots")
        if engine == 'matplotlib':
            __plot_correlation_matplotlib(df, self.download, folder_name)

    def plot_ts_specific(self, one, other, lagK, order_by=None, engine='plotly', download=False,
                         folder_name='timeseries'):
        """ Generates plot to visualize two time series data with the lag specified.

        :param one: str,
                    column name.
        :param other: str,
                      column name.
        :param lagK: int,
                     lag value.
        :param order_by: list of str or str,
                         order by this column.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.

        """
        if order_by is None:
            order_by = [self.time_column]

        def __plot_ts_specific_matplotlib(df, one, other, order_by, time_column, lagK, download, folder_name):
            """ Generate plots using matplotlib library.

            """
            save_dir = self.dload + "/" + folder_name
            plt.close('all')
            f, axarr = plt.subplots(2, sharex=True)

            _ = self.shortlist(df, [time_column, one]).plot(x=time_column, y=one, ax=axarr[0])
            df_lag_other = self.lag_shift(self.df, other, order_by, lagK, lagK)
            corr = "{0:.4f}".format(self.cross_correlation_1d(df_lag_other, one, str(lagK)))

            _ = self.shortlist(df_lag_other, [time_column, str(lagK)]).plot(x=time_column, y=str(lagK), ax=axarr[1])
            f.suptitle('Correlation: ' + corr)
            axarr[1].legend([other + '\n lag(' + str(lagK) + ')'])

            if download:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" + one + " vs " + other + '(lag = ' + str(lagK) + ').png')
            else:
                plt.show()

        def __plot_ts_specific_plotly(df, one, other, order_by, time_column, lagK):
            """ Generate plots using plotly library.

            """
            if not isinstance(order_by, list):
                order_by = [order_by]

            if self.using_spark:
                df = df.sort(order_by)
                df_one = df.select(time_column, one).toPandas()
            else:
                df = df.sort_values(order_by).reset_index(drop=True)
                df_one = df.loc[:, [time_column, one]].copy(deep=True)

            df_lag_other = self.lag_shift(df, other, order_by, lagK, lagK)
            corr = "{0:.4f}".format(self.cross_correlation_1d(df_lag_other, one, str(lagK)))
            '''
            if self.using_spark:
                df_lag_other = df_lag_other.select(time_column, str(lagK)).toPandas()
            else:
                df_lag_other = df_lag_other.loc[:,[time_column, str(lagK)]].copy(deep = True)
            '''
            df_lag_other = self.shortlist(df_lag_other, [time_column, str(lagK)])  # .copy(deep = True)
            ## Trace of one.
            trace1 = Scattergl(
                x=df_one[time_column],
                y=df_one[one],
                name=one,
                line=Line(color='rgb(19,48,84)', width=3),
                mode='markers',
                marker=dict(size=3),
                yaxis='y1'
            )

            ## Trace of other
            trace2 = Scattergl(
                x=df_lag_other[time_column],
                y=df_lag_other[str(lagK)],
                name=other,
                line=Line(color='rgb(19,48,84)', width=3),
                mode='markers',
                marker=dict(size=3),
                yaxis='y2',
            )

            data = Data([trace1, trace2])
            layout = Layout(
                title='Correlation: ' + str(corr),
                xaxis=XAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title=time_column,
                    tickangle='35'
                ),
                yaxis1={
                    "domain": [0, 0.5],
                    "title": one
                },
                yaxis2={
                    "domain": [0.5, 1],
                    "title": other + '<br> lag(' + str(lagK) + ')'
                },
                legend=dict(
                    font=dict(
                        size=13,
                    )
                )
            )
            fig = Figure(data=data, layout=layout)
            return fig

        if (download == False) & (engine == 'plotly'):
            fig = __plot_ts_specific_plotly(self.df, one, other, order_by, self.time_column, lagK)
            iplot(fig)
        if (download) & (engine == 'plotly'):
            print("Can't download plotly plots")
        if engine == 'matplotlib':
            __plot_ts_specific_matplotlib(self.df, one, other, order_by, self.time_column, lagK, download, folder_name)

    def plot_ts_topk(self, result_df, order_by=None, engine='matplotlib', download=True, folder_name='timeseries'):
        """  Plot time series data of top K highly correlated column pairs considering lag.

        :param result_df: pandas DataFrame,
                          resultant dataframe after computing correlation value.
        :param order_by: list of str or str,
                         order by this column.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.

        """
        if order_by is None:
            order_by = [self.time_column]
        max_idx = result_df.groupby(['Column_1', 'Column_2'])['Correlation'].idxmax()
        max_per_column = result_df.loc[max_idx, ['Column_1', 'Column_2', 'Correlation', 'Lag']].reset_index(drop=True)
        max_per_column = (max_per_column[max_per_column['Column_1'] != max_per_column['Column_2']]
                              .sort_values(by='Correlation', ascending=False)
                              .reset_index(drop=True)
                              .iloc[:self.topK, :])

        for row in max_per_column.iterrows():
            each_series = row[1]
            self.plot_ts_specific(each_series['Column_1'],
                                  each_series['Column_2'],
                                  each_series['Lag'],
                                  order_by,
                                  engine=engine,
                                  download=download,
                                  folder_name=folder_name)

    def lag_shift(self, df, column, order_by=None, lower_lag=None, upper_lag=None):
        """ This method is used to shift the time series by the specified lag value.

        :param df: pandas DataFrame,
                   time series data.
        :param column: str,
                       column name.
        :param order_by: list of str or str,
                         order by this column.
        :param lower_lag: int,
                          lower limit for the lag value.
        :param upper_lag: int,
                          upper limit for the lag value.

        :return: pandas DataFrame

        """
        if order_by is None:
            order_by = [self.time_column]
        if lower_lag is None:
            lower_lag = self.lower_lag
        if upper_lag is None:
            upper_lag = self.upper_lag
        if self.using_spark:
            from pyspark.sql.functions import lag, col
            from pyspark.sql.window import Window
            order_by = map(col, order_by)
            w = Window().partitionBy().orderBy(order_by)

            for each_lag in range(lower_lag, upper_lag + 1):
                df = df.withColumn(str(each_lag), lag(column, count=each_lag).over(w))
        else:
            df = df.sort_values(order_by).reset_index(drop=True)
            for each_lag in range(lower_lag, upper_lag + 1):
                df[str(each_lag)] = df[column].shift(each_lag)

        return df

    def cross_correlation_1d(self, df, one, other):
        """ Compute correlation of columns, excluding NA/null values

        :param df: pandas DataFrame,
                   data.
        :param one: str,
                    column name.
        :param other: str,
                      column name.

        :return: pandas DataFrame
        """
        if self.using_spark:
            return df.select(one, other).na.drop().corr(one, other)
        else:
            corr_df = df[[one, other]].dropna()
            return abs(np.corrcoef(corr_df[one].astype(float), corr_df[other].astype(float))[0][1])

    def cross_correlation_2d(self, df, one, other, order_by, lower_lag=None, upper_lag=None):
        """ Compute correlation of columns, excluding NA/null values for different lag values

        :param df: pandas DataFrame,
                   data.
        :param one: str,
                    column name.
        :param other: str,
                      column name.
        :param order_by: list of str or str,
                        order by this column.

        :return: pandas DataFrame
        """
        if not lower_lag:
            lower_lag = self.lower_lag
        if not upper_lag:
            upper_lag = self.upper_lag
        corr_lag_result = []
        df_lag = self.lag_shift(df, other, order_by, lower_lag, upper_lag)

        for lag_other in range(lower_lag, upper_lag + 1):
            corr_lag_result.append(self.cross_correlation_1d(df_lag, one, str(lag_other)))

        return pd.DataFrame({
            'Column_1': one,
            'Column_2': other,
            'Lag': range(lower_lag, upper_lag + 1),
            'Correlation': corr_lag_result
        })

    def cross_correlation_3d(self, order_by=None):
        """ Compute pairwise correlation of all columns, excluding NA/null values for different lag values

        :param order_by: list of str or str,
                        order by this column.

        :return: pandas DataFrames
        """
        if order_by is None:
            order_by = [self.time_column]

        if not isinstance(order_by, list):  # convert if not list
            order_by = [order_by]

        full_result_df = pd.DataFrame(columns=['Column_1', 'Column_2', 'Lag', 'Correlation'])
        max_result_df = pd.DataFrame(index=self.columns, columns=self.columns)

        for one in self.columns:
            for other in self.columns:
                if self.using_spark:
                    intermediate_df = self.cross_correlation_2d(self.df.select(one, other, *order_by), one, other,
                                                                order_by)
                else:
                    intermediate_df = self.cross_correlation_2d(self.df, one, other, order_by)
                max_result_df.at[one, other] = intermediate_df.loc[intermediate_df['Correlation'].abs().idxmax()][
                    'Correlation']
                full_result_df = full_result_df.append(intermediate_df, ignore_index=True)

        return full_result_df, max_result_df

    def correlation_against_all(self, column_correlation_againist_all,columns = None,lower_lag=None, upper_lag=None, order_by=None):
        """ To compute correlation against the target column for different lag values.

        :param column_y: str,
                         target column name.
        :param columns: list of column names.
        :param lower_lag: int,
                          lower limit for the lag value.
        :param upper_lag: int,
                          upper limit for the lag value.
        :param order_by: str,
                         order by this column.

        :return: pandas DataFrame
        """
        if lower_lag is None:
            lower_lag = self.lower_lag
        if upper_lag is None:
            upper_lag = self.upper_lag
        if order_by is None:
            order_by = [self.time_column]
            
        if not columns:
            columns = [ c for c in self.df.columns if c not in [self.time_column,column_correlation_againist_all] ]
        if not isinstance(order_by, list):  # convert if not list
            order_by = [order_by]

        full_result_df = pd.DataFrame(columns=['Column_1', 'Column_2', 'Lag', 'Correlation'])

        for other in columns:
            if self.using_spark:
                intermediate_df = self.cross_correlation_2d(self.df.select(column_correlation_againist_all, other, *order_by), column_correlation_againist_all, other,
                                                            order_by, lower_lag, upper_lag)
            else:
                intermediate_df = self.cross_correlation_2d(self.df, column_correlation_againist_all, other, order_by, lower_lag, upper_lag)

            full_result_df = full_result_df.append(intermediate_df, ignore_index=True)
            
        full_result_df.columns = [ 'Target column' , 'Column_Name' , 'Lag' , 'Correlation' ]
        return full_result_df

    def shortlist(self, dframe, columns):
        if self.using_spark:
            return dframe.select(columns).toPandas()
        else:
            return dframe.loc[:, columns]
