"""Noodle's forecast module."""
# Packages for generic use
import pandas as pd
from Pynotebooks.data_analysis.dataquality.dataquality import dqa
import calendar
import plotly as py
import plotly.graph_objs as go

import logging
import os
import matplotlib.pyplot as plt
logger = logging.getLogger(__name__)


class ts(object):
    """ts class prepares and stores data in a format required for models.

    Parameters:
    -----------
    data: Raw data as pandas dataframe.

    Methods:
    --------
    transform: Convert raw data from long format into date stream
    periodicity: Determine the periodicity of time series
    frequency: Find the frequency of time series
    visualize: Visualize time series of many categories
    """

    def __init__(self, data, time_col, value_col, *id_cols):
        """Instantiate the class and store data."""
        self.data = data
        self.data.index.name = None
        self.value_col_name = value_col
        self.id_cols = id_cols
        self.transform(time_col, value_col)

    def transform(self, time_col, value_col, *id_cols):
        """Transform data into date streams.

        Parameters:
        -----------
        time_col: string
                  Column containing datetime type values in str/datetime format
        value_col: string
                  Column containing values of entity
        args : string (as a list)
               Column names containing unique IDs.

        Note:
        -----
        If args is None, all columns except the time_col and the
        value_col will be considered to derive unique categories.

        Returns:
        --------
        pandas dataframe with time_col as index, id_cols as columns and
        value_col as values.
        """
        if not id_cols:
            col = [c for c in self.data.columns if c not in {time_col,
                                                             value_col}]
            self.data = self.data.pivot_table(columns=col, index=time_col,
                                              values=value_col)
        else:
            cols = [time_col, value_col]
            for i in id_cols:
                cols.append(i)
            self.data = self.data[cols].pivot_table(columns=id_cols,
                                                    index=time_col,
                                                    values=value_col)
        self.data.index.name = None
        self.data.index = pd.DatetimeIndex(self.data.index)

    def periodicity(self, show_plot=False):
        """Find the periodicty of time series.

        Parameters:
        -----------
        show_plot: bool, default is False.
                   If True, will also return histogram of timedeltas

        Returns:
        ---------
        periodicity as tuple ('value', 'units') Ex:('1.0', 'weeks')
        """
        tsq = dqa(self.data.reset_index())
        a = tsq.check_periodicity('index', show_plot=show_plot)

        count = pd.DataFrame(pd.DataFrame(a[0])[0].value_counts().sort_values(ascending=False))
        freq = count.index[0]
        if(count.shape[0] > 1):
            count = count.reset_index()
            count['prop'] = count[0] / sum(count[0])
            if(count['prop'][0] >= count['prop'][1:].sum() and count['prop'][0] >= 3 * count['prop'][1]):
                freq = count['index'][0]
            else:
                freq = 'Uncertain'
        return freq, a[1]

    def frequency(self, show_plot=False):
        """Determine frequency of time series.

        Parameters:
        -----------
        show_plot: bool, default is False.
                   If True, will also return histogram of timedeltas

        Returns:
        --------
        Frequency of timeseries as a string
        """
        freq_string = self.periodicity(show_plot=show_plot)[1]
        freq_dict = dict({
            'weeks': 'W-' + calendar.day_abbr[self.data.index[0].weekday()].upper(),
            'days': 'D',
            'hours': 'H',
            'minutes': 'M',
            'seconds': 'S'
        })
        return freq_dict[freq_string]

    def visualize(self, category, download = False, directory = '~/Downloads', folder_name = 'visualize'):
        """Visualize time series of a list of categories

        Parameters:
        -----------
        category: string | list
                  Column name of category, when single pass a string, when multiple
                  pass a list of categories
        Note:
        -----
        plotly's plot can be seen only if it is activated to be seen in the
        notebook

        Returns:
        --------
        plotly's lineplot of category values ~ time
        """

        if not isinstance(category, list):
            category = [category]

        if not download: # plotly
            for c in category:
                layout = go.Layout(
                    title='Time series of ' + str(c),
                    xaxis=dict(title='Measure of time (in ' + self.periodicity()[1] + ')'),
                    yaxis=dict(title='Value')
                )

                trace = go.Scatter(
                    x=self.data.index,
                    y=self.data[c],
                    name=str(c)
                )
                py.offline.iplot(go.Figure(data=[trace], layout=layout))
        else: # matplotlib

#             self.dload = os.path.expanduser(directory)
#             save_dir = self.dload + "/" + folder_name
#             if os.path.exists(save_dir):
#                 pass
#             else:
#                 os.mkdir(self.dload + '/' + folder_name)

            for c in category:
                plt.figure(figsize=(20,12))
                plt.plot(self.data.index, self.data[c])
                plt.title('Time series of ' + str(c))
                plt.xlabel('Measure of time (in ' + self.periodicity()[1] + ')')
                plt.ylabel('Value')

                plt.savefig(folder_name + "/" + str(c) + '.png')
                plt.close()

            print('Downloaded!')
