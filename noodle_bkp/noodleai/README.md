## About 

  

The core of this repository is the Python Notebooks. The Python Notebooks are created in order to make data scientist's work easier, more efficient and faster. This notebook is designed for Python 3.5 and onwards. The core libraries use essential packages required for working with data in Python such as IPython Notebook, NumPy, Pandas, Matplotlib, Scikit-Learn, and related packages. Familiarity with Python as a language is assumed.  

Keep updated on the Slack channel - `#noodle_notebooks`


## Installation 


Python Version : 3.5 + 

Install all the packages through `pip install -r requirements_master.txt`

How to install python package ?

pip3 install pynotebooks --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple

## How to work through the code 

The notebooks come with sample data which is present in the `DATASETS` folder. 
In order to run a particular dataset, just put the name in `df = pd.read_csv("filename")` and change column names as you run the code.
The Libraries have the underlying code for each function.


### Pynotebooks


|Sl.No|Notebook|Folder Name |Notebook Name(ipynb) |Library Name | Author
|---|---|---|---|---|---|---|---
|1.1|EDA |data_analysis/interactive|univariate_analysis | dataviz/dataviz.py | Tejas Lodaya
|1.2|EDA|data_analysis/interactive|bivariate_analysis |dataviz/dataviz.py | Tejas Lodaya
|1.3|EDA|data_analysis/interactive|multivariate_analysis |dataviz/dataviz.py | Tejas Lodaya
|1.4|EDA|data_analysis/interactive|timeseries_analysis |dataviz/dataviz.py | Tejas Lodaya
|1.5|EDA|data_analysis|data_quality_assessment |dataquality/dataquality.py|Bhavana Martha 
|1.6|EDA|data_analysis|eda_download_plots|dataviz/downalod_plots.py|Tejas Lodaya
|2.1|Data Wrangling|data_wrangling/interactive|data_wrangling |datawrangling/datawrangling.py| Pooja Balusani
|2.2|Data Wrangling|data_wrangling/interactive|data_cleaning_and_treatment | datacleaning/datacleaning.py|Pooja Balusani
|3.1|Feature Engineering|feature_engineering/interactive|feature_engineering|featureengineering/featureengineering.py|Pooja Balusani
|3.2|Feature Engineering|feature_engineering/interactive|date_transform|featureengineering/featureengineering.py| Pooja Balusani
|3.3|Feature Engineering|feature_engineering/interactive|data_transformation|featureengineering/featureengineering.py|Pooja Balusani
|3.4|Feature Engineering|feature_engineering/interactive|feature_transform|featureengineering/featureengineering.py| Pooja Balusani
|3.5|Feature Engineering|feature_engineering/interactive|geometric_tranformation|featureengineering/featureengineering.py| Pooja Balusani
|3.6|Feature Engineering|feature_engineering/interactive|time_series|featureengineering/featureengineering.py| Pooja Balusani and Jose Mathew
|3.7|Feature Engineering|feature_engineering/interactive|signal_transform|featureengineering/featureengineering.py|Jose Mathew
|3.8|Feature Engineering|feature_engineering|dimensionality_reduction|dimensionalityreduction/dimensionalityreduction.py|Tejas Lodaya
|4.1|Modeling |modeling/clustering|agglomerative_clustering|clustering/clustering.py| Sunishchal Dev
|4.2|Modeling |modeling/clustering|gaussian_mixture_clustering|clustering/clustering.py| Sunishchal Dev
|4.3|Modeling |modeling/clustering|kmeans_clustering|clustering/clustering.py| Sunishchal Dev
|5.1|Signal Detection|signal_extraction|anamoly_detection|signal_extraction/timeseries_signal_extractor.py|Suvrat Hiran
|5.2|Signal Detection|signal_extraction|causal_signals|signal_extraction/timeseries_signal_extractor.py|Suvrat Hiran
|5.3|Signal Detection|signal_extraction|demand_risks_and_clusters|signal_extraction/timeseries_signal_extractor.py|Suvrat Hiran
|5.4|Signal Detection|signal_extraction|patterns_and_segments|signal_extraction/timeseries_signal_extractor.py|Suvrat Hiran
|5.5|Signal Detection|signal_extraction|sales_demand_sensing|signal_extraction/timeseries_signal_extractor.py|Suvrat Hiran
|5.6|Signal Detection|signal_extraction|pandas_cross-correlation|crosscorrelation/crosscorrelation.py|Tejas Lodaya
|5.7|Signal Detection|signal_extraction|timeseries_clustering_eda|clustering/utils.py| Tejas Lodaya
|5.8|Signal Detection|signal_extraction|timeseries_clustering_model|clustering/vrae.py| Tejas Lodaya

***P.S.*** The [interactive folder under data analysis](Pynotebooks/data_analysis/interactive) contains the sample notebooks for interactive  plotting/feature building/etc. This shall help to explore how to use underlying libaries.


### Sparknotebooks


|Sl.No|Notebook|Folder Name |Notebook Name(ipynb) |Library Name | Author
|---|---|---|---|---|---|---|---
|1.1|EDA|data_analysis|spark visualization - interactive|sparkvisualization/sparkvisualization.py|Tejas Lodaya
|2.1|Feature Engineering|feature_engineering|DS_FE Spark|-|Pooja Balusani
|2.2|Feature Engineering|feature_engineering|NLP Spark|-|Pooja Balusani
|3.1|Signal Detection|signal_extraction|Spark cross-correlation|crosscorrelation/crosscorrelation.py|Tejas Lodaya

### SalesAI

|Sl.No|Notebook|Folder Name |Notebook Name(ipynb) |Library Name | Author
|---|---|---|---|---|---|---|---
|1.1|Data Analysis|data_analysis|summary_statistics|summary/summary.py|Soumya H
|1.2|Data Analysis|data_analysis|coverage_exploration|coverage_exploration/coverage_exploration.py|Abhishek Sinha
|1.3|Data Analysis|data_analysis|trend_analysis|trend_analysis/trend_analysis.py|Soumya Palem
|2.1|Data Wrangling|data_wrangling|multiple_timeseries_imputation|imputation/multiple_imputation.py|Abhishek Sinha
|3.1|Feature Engineering|feature_engineering|timeseries_transformations|timeseries_transformation/timeseries_transformations.py|Soumya Palem
|3.2|Feature Engineering|feature_engineering|ts_classify|ts_classify/ts_classify.py|Abhishek Sinha, Soumya H and Soumya Palem
|3.3|Feature Engineering|feature_engineering|Date_Time_Features|calender_variables/calendar_variables.py|Abhishek Sinha
|4.1|Modeling |modeling|ts_forecasting_prophet|dataviz/dataviz.py |Suvrat Hiran
|4.2|Modeling |modeling|forecasting_ensemble|ensembleforecast/ensembleforecast.py|Kiran Sripada
|4.3|Modeling |modeling|mvts_deep_neural_nets|deep_forecast/mvts_forecast.py| Suvrat Hiran and Pooja Balusani
|4.4|Modeling |modeling|mvts_deep_neural_nets|deep_forecast/deep_learning_neural_nets.py| Suvrat Hiran
|4.5|Modeling |modeling|mvts_deep_neural_nets|deep_forecast/generalized_model.py| Pooja Balusani
|4.6|Modeling |modeling|evaluation|performance_eval/evaluation.py|Shruthi Kumar
|4.7|Modeling |modeling|baseline_forecast|baseline_forecast/baseline_forecast.py|Shruthi Kumar
|4.8|Modeling |modeling|Feature Attribution|feature_attribution/feature_attribution.py|Pooja Balusani 
|5.1|Signal Detection|signal_detection|time_series_periodicity_analysis|time_series_periodicity_analysis/periodicity_analysis.py|Soumya H

Link to API documentation of SalesAI - http://192.168.10.156/SalesAI.html

Link to Information about modules in SalesAI - https://noodleai.atlassian.net/wiki/spaces/PP/pages/641204268/Sales+AI+App


## How to report bugs  

In case you come across any bugs, please create an `ISSUE` in the bitbucket Repository

https://bitbucket.org/noodleai/noodleai/issues?status=new&status=open

Click on create issue, and log in the issue

Please follow the template for the summary : `TypeofNotebook_NotebookName_Bug`

Also mention the following details in the description : 

* **Names of the files the bug was present in** : Eg. datawrangling.py 

* **Description of the bug** : Eg. The code doesn't work 
    
(Optional) Add screenshots of errors or any details you would like to highlight
## How to suggest improvements 


In case you have some suggestions for improvement , please raise it to the slack channel - `@noodle_notebooks` 

Please follow the following template so that it makes it easier for the author to reference.

**Type of notebook** : Eg. Data Analysis, Data Quality, Data wrangling, Feature engineering etc 

**Notebook name** : Eg. Time Series Analysis 

**Names of the files the enhancement can be made in**  : Eg. datawrangling.py 

**Description of the enhancement** : Eg. Add a feature 

Based on the slack discussion, the enhancement can be carried over to an ISSUE on bitbucket.
 
## How to contribute code 
 
* Every Bitbucket project has a `fork link` under "+" menu on left of the page. By clicking this link, you can create a separate Bitbucket project repository that is an exact copy of the repository you wish to clone. 

* As a contributor, you can then clone your Bitbucket repository locally, make changes, commit them locally, and push them to your Bitbucket project just as if you were a lone developer.  

* The next step is to `pull` from the original project and, if there are any changes that were made there since you created your clone.

* `merge` the changes with your local repository. 

* In order contribute code, Click on the "+" button on the left menu panel followed by selecting "Create a pull request". Select the repository you need to contribute to, the title, description and reviewer. Once done with entering all the details click on "Create a pull request".

* The maintainers of the original project will review your changes and revert if there is any issue. If the changes are fine the maintainers will merge and push the changes to the Repository.
 
## Guidelines to maintain while writing your code 
 
 * Have a requirements file with the name `requirements_name.txt` with all the packages used along with their respective version in the format `pandas==0.21.0`
 * Must have some basic **description** of the notebook at the top of the notebook using markdown followed by a **Table of Contents** linked to each respective section. 
 * Each section must have a **back** linked back to the table of contents
 * Each section must have proper commenting about the function, what inputs does it take and what output would it produce. 
 * Also specify the **author** and the **date of creation** of the notebook
 * Import all libraries below the table of contents in every notebook
 * Always print a snapshot of the dataframe
 * Please use meaningful variable names and provide description is needed
 * Mark each section as **1,1.1** etc
 * Apply the common theme 
 * Make sure all the plots are labeled properly and colors dont clash with the background.
 * Add comments to the library file following the Sphinx documentation format.
 * Refer https://noodleai.atlassian.net/wiki/spaces/BAT/pages/547356710/Notebook+Design+Principles for the design principles.
 
 
### To run a jupyter notebook with a higher data rate limit
 * Run the following command `jupyter notebook --NotebookApp.iopub_data_rate_limit=1.0e7`
 * Refer https://bitbucket.org/noodleai/noodleai/issues/1/difficult-to-run-heavy-visualisation
 
#### Note : The FastAI package needs to be installed separately using pip install fastai. By doing so, the version of pytorch gets downgraded. The required version of Pytorch is 0.4.0 to run the timeseries clustering notebooks.

## FAQ 
 * How to discover what is available?
     - `Readme.md` file contain all necessary details
     - Check out the documentation available for all the modules. http://192.168.10.156/index.html(within noodle network)  
 
 * Where do I log issues?
     - Refer [How to report bugs](#How-to-report-bugs)
     
 * Instead of notebooks can I use libraries directly?
     - Yes, you can directly use underneath libraries. Notebooks can serve as an example. You can further use documentation to figure out how to use modules.
     - Documentation is available in http://192.168.10.156/index.html (within noodle network)
     - e.g,
           - `from Pynotebooks.core.time_series.time_series import ts as ts_class` 
           - `ts = ts_class(data=df, time_col=date_column, value_col=value_column)`     
 
 * How to run the notebooks?
     - Create a new conda environment `conda create --name myenv python=3.6`
     - Activate the new conda environment 
           - for Linux/MacOS : `source activate myenv`
           - for Windows : `activate myenv`
       (For more conda related commands refer https://conda.io/docs/_downloads/conda-cheatsheet.pdf)
     - Clone the noodleai repository from https://bitbucket.org/noodleai/noodleai/ 
         using `git clone https://YourName@bitbucket.org/noodleai/noodleai.git` 
     - Navigate into the `noodleai` directory on your system.
     - Install dependencies using `pip install -r requirements_master.txt`
     - Run `jupyter-notebook` in the terminal.
     
  * How to install python package ?
    - pip3 install pynotebooks --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
     
  * How to install dependencies?
     - Navigate into the `noodleai` directory on your system.
     - Install dependencies using `pip install -r requirements_master.txt`
    
#### Note : When you fork the repository to make changes to the notebook and create a pull request then please give "Write" access to the product team so that they can view the changes, suggest improvements and then merge it to the repository. This is done because the changes made are not visible in bitbucket because of the format of the notebook.
      