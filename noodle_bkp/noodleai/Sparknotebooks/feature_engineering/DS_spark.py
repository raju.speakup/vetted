from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.sql.types import DoubleType
from pyspark.ml.feature import Binarizer
from pyspark.ml.feature import PolynomialExpansion
from pyspark.mllib.linalg import Vectors, VectorUDT
from pyspark.sql.functions import udf
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import PCA
from pyspark.sql.types import Row
from pyspark.ml.feature import OneHotEncoder, StringIndexer
from pyspark.ml.feature import Normalizer
from pyspark.ml.feature import StandardScaler
from pyspark.ml.feature import MinMaxScaler
from pyspark.ml.feature import DCT
from pyspark.ml.feature import Bucketizer
from pyspark.ml.feature import SQLTransformer
from pyspark.mllib.stat import Statistics
import numpy as np

class ds_spark(object):
    def __init__(self, data):
        self.data = data

    def binarizer(self,thresh, inputCol):
        '''
        Binarization is the process of thresholding numerical features to binary (0/1) features.
        Binarizer takes the common parameters inputCol and outputCol, as well as the threshold for binarization. Feature values greater than the threshold are binarized to 1.0; values equal to or less than the threshold are binarized to 0.0. Both Vector and Double types are supported for inputCol.:param thresh:
        :param inputCol:
        :return dataframe:
        '''
        new_df = self.data.withColumn(inputCol, self.data[inputCol].cast(DoubleType()))
        binarizer = Binarizer(threshold=thresh, inputCol=inputCol, outputCol="binarized_feature")
        binarizedDataFrame = binarizer.transform(new_df)
        print("Binarizer output with Threshold = %f" % binarizer.getThreshold())
        return binarizedDataFrame.show(50, False)

    def polyExpansion(self,degree):
        '''
        Polynomial expansion is the process of expanding your features into a polynomial space, which is formulated by an n-degree combination of original dimensions. A PolynomialExpansion class provides this functionality. The example below shows how to expand your features into a 3-degree polynomial space.
        :param degree:
        :return dataframe:
        '''
        polyExpansion = PolynomialExpansion(degree=degree, inputCol="features", outputCol="polyFeatures")
        polyDF = polyExpansion.transform(vector_df)
        return polyDF.show(truncate=False)

    def pca(self,k):
        '''
        PCA is a statistical procedure that uses an orthogonal transformation to convert a set of observations of possibly correlated variables into a set of values of linearly uncorrelated variables called principal components. A PCA class trains a model to project vectors to a low-dimensional space using PCA. The example below shows how to project 5-dimensional feature vectors into 3-dimensional principal components.
        :param k:
        :return dataframe:
        '''
        pca = PCA(k=k, inputCol="features", outputCol="pcaFeatures")
        model = pca.fit(vector_df)
        result = model.transform(vector_df).select("pcaFeatures")
        return result.show(truncate=False)

    def onehot(self,inputCol):
        '''
        One-hot encoding maps a column of label indices to a column of binary vectors, with at most a single one-value. This encoding allows algorithms which expect continuous features, such as Logistic Regression, to use categorical features.
        :param inputCol:
        :return dataframe:
        '''
        stringIndexer = StringIndexer(inputCol=inputCol, outputCol="categoryIndex")
        model = stringIndexer.fit(self.data)
        indexed = model.transform(self.data)
        encoder = OneHotEncoder(inputCol="categoryIndex", outputCol="categoryVector")
        encoded = encoder.transform(indexed)
        return encoded.show()

    def L_one(self,inputCol, p):
        '''
        Normalizer is a Transformer which transforms a dataset of Vector rows, normalizing each Vector to have unit norm. It takes parameter p, which specifies the p-norm used for normalization. (p=2 by default.) This normalization can help standardize your input data and improve the behavior of learning algorithms.
        :param inputCol:
        :param p:
        :return dataframe:
        '''
        # Normalize each Vector using $L^1$ norm.
        normalizer = Normalizer(inputCol=inputCol, outputCol="normFeatures", p=p)
        l1NormData = normalizer.transform(vector_df)
        print("Normalized using L^1 norm")
        return l1NormData.show()

    def L_infinity(self,inputCol, p):
        '''
        Normalizer is a Transformer which transforms a dataset of Vector rows, normalizing each Vector to have unit norm. It takes parameter p, which specifies the p-norm used for normalization. (p=2 by default.) This normalization can help standardize your input data and improve the behavior of learning algorithms.
        :param inputCol:
        :param p:
        :return dataframe:
        '''
        # Normalize each Vector using $L^\infty$ norm.
        normalizer = Normalizer(inputCol=inputCol, outputCol="normFeatures", p=p)
        lInfNormData = normalizer.transform(vector_df, {normalizer.p: float("inf")})
        print("Normalized using L^inf norm")
        return lInfNormData.show()

    def scaler(self,inputCol, withStd=True, withMean=False):
        '''
        StandardScaler transforms a dataset of Vector rows, normalizing each feature to have unit standard deviation and/or zero mean. It takes parameters:
        withStd: True by default. Scales the data to unit standard deviation.
        withMean: False by default. Centers the data with mean before scaling. It will build a dense output, so take care when applying to sparse input.
        :param inputCol:
        :param withStd:
        :param withMean:
        :return dataframe:
        '''
        scaler = StandardScaler(inputCol=inputCol, outputCol="scaledFeatures", withStd=True, withMean=False)
        # Compute summary statistics by fitting the StandardScaler
        scalerModel = scaler.fit(vector_df)
        # Normalize each feature to have unit standard deviation.
        scaledData = scalerModel.transform(vector_df)
        return scaledData.show()

    def minmax_scaler(self,inputCol):
        '''
        MinMaxScaler transforms a dataset of Vector rows, rescaling each feature to a specific range (often [0, 1]). It takes parameters:
        min: 0.0 by default. Lower bound after transformation, shared by all features.
        max: 1.0 by default. Upper bound after transformation, shared by all features.
        :param inputCol:
        :return dataframe:
        '''
        scaler = MinMaxScaler(inputCol=inputCol, outputCol="scaledFeatures")
        scalerModel = scaler.fit(vector_df)
        scaledData = scalerModel.transform(vector_df)
        print("Features scaled to range: [%f, %f]" % (scaler.getMin(), scaler.getMax()))
        return scaledData.select(inputCol, "scaledFeatures").show()

    def dct(self,inverse):
        '''
        The Discrete Cosine Transform transforms a length NN real-valued sequence in the time domain into another length NN real-valued sequence in the frequency domain.
        :param inverse:
        :return dataframe:
        '''
        dct = DCT(inverse=False, inputCol="features", outputCol="featuresDCT")
        dctDf = dct.transform(vector_df)
        return dctDf.select("featuresDCT").show(truncate=False)

    def bucketizer(self,splits, column):
        '''
        Bucketizer transforms a column of continuous features to a column of feature buckets, where the buckets are specified by users.
        :param splits:
        :param column:
        :return dataframe:
        '''
        new_df = self.data.withColumn(column, self.data[column].cast(DoubleType()))
        bucketizer = Bucketizer(splits=splits, inputCol=column, outputCol="bucketedFeatures")
        bucketedData = bucketizer.transform(new_df)
        print("Bucketizer output with %d buckets" % (len(bucketizer.getSplits()) - 1))
        return bucketedData.show()

    def string_indexer(self,inputCol):
        '''
        StringIndexer encodes a string column of labels to a column of label indices.
        :param inputCol:
        :return dataframe:
        '''
        indexer = StringIndexer(inputCol=inputCol, outputCol="categoryIndex")
        indexed = indexer.fit(self.data).transform(self.data)
        return indexed.show()



































