from pyspark.ml.feature import HashingTF, IDF, Tokenizer
from pyspark.ml.feature import Word2Vec
from pyspark.ml.feature import CountVectorizer
from pyspark.ml.feature import NGram
from pyspark.ml.feature import StopWordsRemover
from pyspark.sql.types import *
import pyspark.sql.functions as func
from pyspark.ml.feature import Tokenizer, RegexTokenizer
from pyspark.sql.functions import col, udf
from pyspark.sql.types import IntegerType

class nlp_spark(object):
    def __init__(self, data):
        self.data = data

    def tfidf(self,inputCol):
        '''
        Term frequency-inverse document frequency (TF-IDF) is a feature vectorization method widely used in text mining to reflect the importance of a term to a document in the corpus.
        :param inputCol:
        :return dataframe:
        '''
        tokenizer = Tokenizer(inputCol=inputCol, outputCol="words")
        wordsData = tokenizer.transform(self.data)
        hashingTF = HashingTF(inputCol="words", outputCol="rawFeatures", numFeatures=20)
        featurizedData = hashingTF.transform(wordsData)
        idf = IDF(inputCol="rawFeatures", outputCol="features")
        idfModel = idf.fit(featurizedData)
        rescaledData = idfModel.transform(featurizedData)
        return rescaledData.select("name", "features").show()

    def tokenizer(self,inputCol):
        '''
        Tokenization is the process of taking text (such as a sentence) and breaking it into individual terms (usually words).
        :param inputCol:
        :return dataframe:
        '''
        tokenizer = Tokenizer(inputCol=inputCol, outputCol="words")
        regexTokenizer = RegexTokenizer(inputCol=inputCol, outputCol="words", pattern="\\W")
        countTokens = udf(lambda words: len(words), IntegerType())
        tokenized = tokenizer.transform(self.data)
        tokenized.select(inputCol, "words").withColumn("tokens", countTokens(col("words"))).show(truncate=False)
        regexTokenized = regexTokenizer.transform(self.data)
        return regexTokenized.select(inputCol, "words").withColumn("tokens", countTokens(col("words"))).show(
            truncate=False)

    def word_to_vector(self,vectorSize, minCount, inputCol):
        '''
        Word2Vec is an Estimator which takes sequences of words representing documents and trains a Word2VecModel. The model maps each word to a unique fixed-size vector. The Word2VecModel transforms each document into a vector using the average of all words in the document
        :param vectorSize:
        :param minCount:
        :param inputCol:
        :return dataframe:
        '''
        word2Vec = Word2Vec(vectorSize=vectorSize, minCount=minCount, inputCol=inputCol, outputCol="result")
        model = word2Vec.fit(self.data)
        result = model.transform(self.data)
        return result.show()

    def count_vectorizer(self,inputCol, vocabSize, minDF, truncate):
        '''
        CountVectorizer and CountVectorizerModel aim to help convert a collection of text documents to vectors of token counts.
        :param inputCol:
        :param vocabSize:
        :param minDF:
        :param truncate:
        :return dataframe:
        '''
        cv = CountVectorizer(inputCol=inputCol, outputCol="features", vocabSize=vocabSize, minDF=minDF)
        model = cv.fit(self.data)
        result = model.transform(self.data)
        return result.show(truncate=truncate)

    def remove_stop_words(self,inputCol, truncate):
        '''
        Stop words are words which should be excluded from the input, typically because the words appear frequently and don’t carry as much meaning.
        :param inputCol:
        :param truncate:
        :return dataframe:
        '''
        remover = StopWordsRemover(inputCol=inputCol, outputCol="filtered")
        return remover.transform(self.data).show(truncate=truncate)

    def ngram(self,n):
        '''
        An n-gram is a sequence of nn tokens (typically words) for some integer nn. The NGram class can be used to transform input features into nn-grams.
        :param n:
        :return dataframe:
        '''
        ngram = NGram(n=n, inputCol="new_name", outputCol="ngrams")
        ngramDataFrame = ngram.transform(self.data)
        return ngramDataFrame.select("ngrams").show(truncate=False)






























