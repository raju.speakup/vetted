import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import collections
from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.sql.types import DoubleType
from pyspark.ml.feature import Binarizer
from pyspark.ml.feature import PolynomialExpansion
from pyspark.ml.linalg import Vectors, VectorUDT
from pyspark.sql.functions import udf
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import PCA
from pyspark.sql.functions import udf
from pyspark.sql.types import Row
from pyspark.ml.feature import OneHotEncoder, StringIndexer
from pyspark.ml.feature import Normalizer
from pyspark.ml.feature import StandardScaler
from pyspark.ml.feature import MinMaxScaler
from pyspark.ml.feature import DCT
from pyspark.ml.feature import Bucketizer
from pyspark.ml.feature import SQLTransformer
from pyspark.mllib.stat import Statistics
from pyspark.ml.feature import StringIndexer
import numpy as np
import pyspark.sql.functions as func
from pyspark.mllib.linalg import Vectors as MLLibVectors
from pyspark.ml.linalg import VectorUDT as VectorUDTML
from pyspark.sql.functions import udf
import math
from pyspark.sql import Window
from pyspark.sql import functions as F

class FE_Spark(object):
    def __init__(self, data):
        self.data = data

    def log_transform(self,column_name):
        '''
        Perform log transformation on a set of columns

        :param column_name: Column name
        :return: log transformed column
        '''
        new_df = self.data.select('*', *(log2(target).alias(target + "log") for target in column_name))
        new_df.show()

    def sqrt_transform(self,column_name):
        '''
        Perform square root transformation on a set of columns


        :param column_name:Column name
        :return: transformed column
        '''
        new_df = self.data.select('*', *(sqrt(target).alias(target + "sqrt") for target in column_name))
        new_df.show()

    def tanh_transform(self,column_name):
        '''
        Perform tanH transformation on a set of columns

        :param column_name: Column name
        :return: transformed column
        '''
        new_df = self.data.select('*', *(tanh(target).alias(target + "tanh") for target in column_name))
        new_df.show()

    def square_transform(self,column_name):
        '''
        Perform square transformation on a set of columns

        :param column_name: column name
        :return: transformed column
        '''
        def square_float(x):
            return float(x ** 2)

        square_udf = udf(lambda z: square_float(z), FloatType())
        new_df = self.data.select('*', *(square_udf(target).alias(target + "_square") for target in column_name))
        new_df.show()

    def sigmoid_transform(self,column_name):
        '''
        Perform Sigmoid  transformation on a set of columns

        :param column_name: Column name
        :return: transformed column
        '''
        def sigmoid(x):
            return 1 / (1 + math.exp(-x))

        sigmoid_udf = udf(lambda z: sigmoid(z), FloatType())
        new_df = self.data.select('*', *(sigmoid_udf(target).alias(target + "_sigmoid") for target in column_name))
        new_df.show()

    def cumsum(self,partitionby, orderby, sumof):
        '''
        Performs cumulative sumation of a column
        :param partitionby: Partition by which column
        :param orderby: Orer by column name
        :param sumof: Aggregation of column name
        :return: Cumsum of a column
        '''
        windowval = (Window.partitionBy(partitionby).orderBy(orderby)
                     .rangeBetween(Window.unboundedPreceding, 0))
        df_w_cumsum = self.data.withColumn('cum_sum', F.sum(sumof).over(windowval))
        df_w_cumsum.show()

    def get_week_of_year(self,date):
        '''
        Get week number of the year

        :param date: Time column
        :return: Week number of the year
        '''
        return self.data.select(weekofyear(date).alias('week')).collect()

    def get_day_of_month(self,date):
        '''
        Get day number of month

        :param date: Time column
        :return: day number of month
        '''
        return self.data.select(dayofmonth(date).alias('day')).collect()

    def get_day_of_year(self,date):
        '''
        Get Day number of year

        :param date: Time column
        :return: day number of year
        '''
        return self.data.select(dayofyear(date).alias('day')).collect()

    def kmeans(self,k):
        '''
        Peforms K means clustering

        :param k: number of clusters
        :return: centroids of clusters
        '''
        kmeans = KMeans().setK(k).setSeed(1)
        model = kmeans.fit(self.data)
        wssse = model.computeCost(some_df)
        print("Within Set Sum of Squared Errors = " + str(wssse))

        centers = model.clusterCenters()
        print("Cluster Centers: ")
        for center in centers:
            print(center)

    def lag_shift(self,lag_column, orderby_column, new_column_name):
        '''
        Perform lag shift on a column

        :param lag_column: column to be shifted
        :param orderby_column: orderby which column
        :param new_column_name: new shifted column name
        :return:
        '''
        w = Window().partitionBy().orderBy(col(orderby_column))
        df.select("*", lag(lag_column).over(w).alias(new_column_name)).na.drop().show()

    def rolling_average(self,days, time_column, avg_column):
        '''
        Performs rolling average on a column

        :param days: number of days to roll over
        :param time_column: time column
        :param avg_column: aggregation column
        :return:
        '''
        new_df = df.withColumn(time_column, df.Date.cast(time_column))
        w = (Window.orderBy(F.col(time_column).cast('long')).rangeBetween(-days(7), 0))
        new_df = new_df.withColumn('rolling_average', F.avg(avg_column).over(w))
        new_df.show()

    def binarizer(self,thresh, inputCol):
        '''
        Binarization is the process of thresholding numerical features to binary (0/1) features.
        Binarizer takes the common parameters inputCol and outputCol, as well as the threshold for binarization. Feature values greater than the threshold are binarized to 1.0; values equal to or less than the threshold are binarized to 0.0. Both Vector and Double types are supported for inputCol.:param thresh:

        :param inputCol:input column
        :return dataframe: new dataframe with binarizer column
        '''
        new_df = df.withColumn(inputCol, df[inputCol].cast(DoubleType()))
        binarizer = Binarizer(threshold=thresh, inputCol=inputCol, outputCol="binarized_feature")
        binarizedDataFrame = binarizer.transform(new_df)
        print("Binarizer output with Threshold = %f" % binarizer.getThreshold())
        return binarizedDataFrame.show()

    def polyExpansion(self,degree):
        '''
        Polynomial expansion is the process of expanding your features into a polynomial space, which is formulated by an n-degree combination of original dimensions. A PolynomialExpansion class provides this functionality. The example below shows how to expand your features into a 3-degree polynomial space.

        :param degree:
        :return dataframe:
        '''
        polyExpansion = PolynomialExpansion(degree=degree, inputCol="features", outputCol="polyFeatures")
        polyDF = polyExpansion.transform(some_df)
        return polyDF.show(truncate=False)

    def pca(self,k):
        '''
        PCA is a statistical procedure that uses an orthogonal transformation to convert a set of observations of possibly correlated variables into a set of values of linearly uncorrelated variables called principal components. A PCA class trains a model to project vectors to a low-dimensional space using PCA. The example below shows how to project 5-dimensional feature vectors into 3-dimensional principal components.

        :param k:
        :return dataframe:
        '''
        pca = PCA(k=k, inputCol="features", outputCol="pcaFeatures")
        model = pca.fit(some_df)
        result = model.transform(some_df).select("pcaFeatures")
        return result.show(truncate=False)

    def onehot(self,inputCol):
        '''
        One-hot encoding maps a column of label indices to a column of binary vectors, with at most a single one-value. This encoding allows algorithms which expect continuous features, such as Logistic Regression, to use categorical features.

        :param inputCol:
        :return dataframe:
        '''
        stringIndexer = StringIndexer(inputCol=inputCol, outputCol="categoryIndex")
        model = stringIndexer.fit(df)
        indexed = model.transform(df)
        encoder = OneHotEncoder(inputCol="categoryIndex", outputCol="categoryVector")
        encoded = encoder.transform(indexed)
        return encoded.show()

    def L_one(self,inputCol, p):
        '''
        Normalizer is a Transformer which transforms a dataset of Vector rows, normalizing each Vector to have unit norm. It takes parameter p, which specifies the p-norm used for normalization. (p=2 by default.) This normalization can help standardize your input data and improve the behavior of learning algorithms.

        :param inputCol:
        :param p:
        :return dataframe:
        '''
        # Normalize each Vector using $L^1$ norm.
        normalizer = Normalizer(inputCol=inputCol, outputCol="normFeatures", p=p)
        l1NormData = normalizer.transform(some_df)
        print("Normalized using L^1 norm")
        return l1NormData.show()

    def L_infinity(self,inputCol, p):
        '''
        Normalizer is a Transformer which transforms a dataset of Vector rows, normalizing each Vector to have unit norm. It takes parameter p, which specifies the p-norm used for normalization. (p=2 by default.) This normalization can help standardize your input data and improve the behavior of learning algorithms.

        :param inputCol:
        :param p:
        :return dataframe:
        '''
        # Normalize each Vector using $L^\infty$ norm.
        normalizer = Normalizer(inputCol=inputCol, outputCol="normFeatures", p=p)
        lInfNormData = normalizer.transform(some_df, {normalizer.p: float("inf")})
        print("Normalized using L^inf norm")
        return lInfNormData.show()

    def scaler(self,inputCol, withStd=True, withMean=False):
        '''
        StandardScaler transforms a dataset of Vector rows, normalizing each feature to have unit standard deviation and/or zero mean. It takes parameters:
        withStd: True by default. Scales the data to unit standard deviation.
        withMean: False by default. Centers the data with mean before scaling. It will build a dense output, so take care when applying to sparse input.

        :param inputCol:
        :param withStd:
        :param withMean:
        :return dataframe:
        '''
        scaler = StandardScaler(inputCol=inputCol, outputCol="scaledFeatures", withStd=True, withMean=False)
        # Compute summary statistics by fitting the StandardScaler
        scalerModel = scaler.fit(some_df)
        # Normalize each feature to have unit standard deviation.
        scaledData = scalerModel.transform(some_df)
        return scaledData.show()

    def minmax_scaler(self,inputCol):
        '''
        MinMaxScaler transforms a dataset of Vector rows, rescaling each feature to a specific range (often [0, 1]). It takes parameters:
        min: 0.0 by default. Lower bound after transformation, shared by all features.
        max: 1.0 by default. Upper bound after transformation, shared by all features.

        :param inputCol:
        :return dataframe:
        '''
        scaler = MinMaxScaler(inputCol=inputCol, outputCol="scaledFeatures")
        scalerModel = scaler.fit(some_df)
        scaledData = scalerModel.transform(some_df)
        print("Features scaled to range: [%f, %f]" % (scaler.getMin(), scaler.getMax()))
        return scaledData.select(inputCol, "scaledFeatures").show()

    def dct(self,inverse):
        '''
        The Discrete Cosine Transform transforms a length NN real-valued sequence in the time domain into another length NN real-valued sequence in the frequency domain.

        :param inverse:
        :return dataframe:
        '''
        dct = DCT(inverse=False, inputCol="features", outputCol="featuresDCT")
        dctDf = dct.transform(some_df)
        return dctDf.select("featuresDCT").show(truncate=False)

    def bucketizer(self,splits, column):
        '''
        Bucketizer transforms a column of continuous features to a column of feature buckets, where the buckets are specified by users.

        :param splits:
        :param column:
        :return dataframe:
        '''
        new_df = df.withColumn(column, df[column].cast(DoubleType()))
        bucketizer = Bucketizer(splits=splits, inputCol=column, outputCol="bucketedFeatures")
        bucketedData = bucketizer.transform(new_df)
        print("Bucketizer output with %d buckets" % (len(bucketizer.getSplits()) - 1))
        return bucketedData.show()

    def string_indexer(self,inputCol):
        '''
        StringIndexer encodes a string column of labels to a column of label indices.

        :param inputCol:
        :return dataframe:
        '''
        indexer = StringIndexer(inputCol=inputCol, outputCol="categoryIndex")
        indexed = indexer.fit(df).transform(df)
        return indexed.show()

































