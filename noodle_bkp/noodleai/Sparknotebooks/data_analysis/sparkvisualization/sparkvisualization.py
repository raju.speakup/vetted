from pyspark.ml.feature import VectorAssembler
from pyspark.ml.stat import Correlation
import pandas as pd
import numpy as np
try:
    # Python 3
    from itertools import zip_longest as izip_longest
except ImportError:
    # Python 2
    from itertools import izip_longest
import matplotlib.pyplot as plt
from plotly.graph_objs import *
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import seaborn as sns
import collections
import os

plt.rcParams["figure.figsize"] = (20, 14)
init_notebook_mode(connected=True)
sns.set(color_codes=True)


class SparkVisualization(object):
    """ The main SparkVisualization object

    The SparkVisualization is an python Class which provides methods
    for data visualizations using spark  .
    SparkVisualization has methods for different kind of plots
    and downloading it.

    :param data: DataFrame,
                 data.
    :param dload: str,
                  download location path.
    """

    def __init__(self, data=None, dload=None):
        self.df = data
        self.dload = dload
        self.numerical_columns = [col for col, s in data.toPandas().iteritems() if self.get_vartype(s) == 'NUM']
        self.categorical_columns = [col for col, s in data.toPandas().iteritems() if self.get_vartype(s) == 'CAT']

    def get_vartype(self, data):
        """Returns the variable type for the given data series

        :param data: pandas series
        :return: returns the variable type
        :rtype: basestring
        """
        distinct_count = data.nunique(dropna=False)
        leng = len(data)
        if distinct_count <= 1:
            return 'CONST'
        elif pd.api.types.is_numeric_dtype(data):
            return 'NUM'
        elif pd.api.types.is_datetime64_dtype(data):
            return 'DATE'
        elif distinct_count == leng:
            return 'UNIQUE'
        else:
            return 'CAT'

    def plot_heatmap(self, column_1 = None, column_2 = None, engine='plotly', download=False, folder_name='heatmap'):
        """ Plots heatmap between columns specified .

        :param column_1: str,
                         column name.
        :param column_2: str,
                         column name.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.

        """
        if not column_1:
            column_1 = np.random.choice(self.categorical_columns, 1)[0]
        if not column_2:
            column_2 = np.random.choice(self.categorical_columns, 1)[0]
            while column_2 == column_1: 
                column_2 = np.random.choice(self.categorical_columns, 1)[0]

        def __plot_heatmap_matplotlib(df, column_1, column_2, download, folder_name):
            """  Plots using matplotlib library

            """
            save_dir = self.dload + "/" + folder_name
            cross_df = df.crosstab(column_1, column_2).toPandas().set_index(column_1 + '_' + column_2)
            cross_list = cross_df.values.tolist()

            x = cross_df.columns.values.tolist()
            y = cross_df.index.values.tolist()
            z = cross_list
            _ = plt.rcParams['ytick.labelsize']
            fig, ax = plt.subplots()
            _ = sns.heatmap(z, xticklabels=x, yticklabels=y, ax=ax)
            _ = plt.title(column_1 + " vs " + column_2)
            
            if download:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" + (column_1 + column_2) + '.png')
                plt.close()
            else:
                plt.show()

        def __plot_heatmap_plotly(df, column_1, column_2):
            """  Plots using plotly library

            """
            cross_df = df.crosstab(column_1, column_2).toPandas().set_index(column_1 + '_' + column_2)
            cross_list = cross_df.values.tolist()

            trace = Heatmap(
                z=cross_list,
                x=cross_df.columns.values.tolist(),
                y=cross_df.index.values.tolist(),
                colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"],
                            [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"],
                            [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"],
                            [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
            )

            data = Data([trace])
            layout = Layout(
                autosize=True,
                margin=dict(r=240, t=30, b=180, l=180, pad=2),
                showlegend=False,
                title=column_1 + ' vs ' + column_2,
                titlefont=dict(size=20),
                xaxis=XAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title=column_2
                ),
                yaxis=YAxis(
                    gridcolor='rgba(229,229,229,0.5)',
                    gridwidth=0.7,
                    showgrid=True,
                    showline=False,
                    showticklabels=True,
                    tickcolor='rgb(127,127,127)',
                    ticks='outside',
                    zeroline=False,
                    title=column_1
                )
            )
            fig = Figure(data=data, layout=layout)
            return fig

        if (download == False) & (engine == 'plotly'):
            fig = __plot_heatmap_plotly(self.df, column_1, column_2)
            iplot(fig)
        if (download) & (engine == 'plotly'):
            print("Can't download plotly plots")
        if engine == 'matplotlib':
            __plot_heatmap_matplotlib(self.df, column_1, column_2, download, folder_name)

    def plot_histogram(self, column_names=None, num_bins=80, engine='matplotlib', download=False, folder_name='histogram'):
        """ Plots histogram for the list of column names.

        :param column_names: list of column names.
        :param num_bins: int,
                         number of bins.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.
        """
        if column_names is None:
            column_names = self.numerical_columns

        def __plot_histogram_matplotlib(df, column_names, num_bins, download, folder_name):
            """  Generates plot using matplotlib library

            """
            save_dir = self.dload + "/" + folder_name

            for column_name in column_names:
                rdd = df.select(column_name).rdd.flatMap(lambda x: x)
                rdd_histogram_data = rdd.histogram(num_bins)
                heights = np.array(rdd_histogram_data[1])
                full_bins = rdd_histogram_data[0]
                mid_point_bins = full_bins[:-1]
                widths = [abs(i - j) for i, j in zip(full_bins[:-1], full_bins[1:])]
                _ = plt.bar(mid_point_bins, heights, width=widths, color='b')
                _ = plt.title(column_name)

                if download:
                    if os.path.exists(save_dir):
                        pass
                    else:
                        os.mkdir(save_dir)
                    plt.savefig(save_dir + "/" + (column_name) + '.png')
                    plt.close()
                else:
                    plt.show()

        if (engine == 'plotly'):
            print("No histogram in plotly")
        if engine == 'matplotlib':
            __plot_histogram_matplotlib(self.df, column_names, num_bins, download, folder_name)

    def describe(self, column_names = None):
        """ Data Summary

        :param column_names: list of column  names.

        """
        if not column_names:
            column_names = self.numerical_columns + self.categorical_columns

        self.df.select(column_names).describe().show()

    def plot_stacked_histogram(self, numerical_column = None, categorical_column = None, num_bins=20, engine='matplotlib',
                               download=False, folder_name='stacked_histogram'):
        """ Plots stacked histogram between numerical and categorical column.

        :param numerical_column: str,
                                 numerical column name.
        :param categorical_column: str,
                                   categorical column name.
        :param num_bins: int,
                         number of bins.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.

        """
        if not numerical_column:
            numerical_column = np.random.choice(self.numerical_columns,1)[0]
        if not categorical_column:
            categorical_column = np.random.choice(self.categorical_columns,1)[0]

        def __plot_stacked_histogram_matplotlib(df, numerical_column, categorical_column, num_bins, download,folder_name):
            """ Generates plot using matplotlib library

            """
            save_dir = self.dload + "/" + folder_name

            df_subset = self.df.select(numerical_column, categorical_column)
            categorical_value_count_list = df_subset.select(categorical_column).distinct().toPandas().iloc[:,
                                           0].values.tolist()
            previous_height = np.array([])

            for each_i, each_category in enumerate(categorical_value_count_list):
                df_filter = df_subset.filter(df_subset[categorical_column] == each_category)
                rdd = df_filter.select(numerical_column).rdd.flatMap(lambda x: x)
                rdd_histogram_data = rdd.histogram(num_bins)
                heights = np.array(rdd_histogram_data[1])
                full_bins = rdd_histogram_data[0]
                mid_point_bins = full_bins[:-1]
                widths = [abs(i - j) for i, j in zip(full_bins[:-1], full_bins[1:])]

                if each_i == 0:
                    _ = plt.bar(mid_point_bins, heights, width=widths, color=np.random.rand(3, ))
                else:
                    _ = plt.bar(mid_point_bins, heights, width=widths, bottom=previous_height,
                                  color=np.random.rand(3, ))  
                previous_height = [sum(x) for x in izip_longest(previous_height, heights, fillvalue=0)]
                
            plt.title(numerical_column + " - " + categorical_column)
            plt.legend(categorical_value_count_list)
            if download:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" + (numerical_column + categorical_column) + '.png')
                plt.close()
            else:
                plt.show()

        if (engine == 'plotly'):
            print("No histogram in plotly")
        if engine == 'matplotlib':
            __plot_stacked_histogram_matplotlib(self.df, numerical_column, categorical_column, num_bins, download,
                                              folder_name)

    def plot_correlation(self, zmin=-1, zmax=1, method='pearson', engine='plotly', download=False,
                         folder_name='correlation'):
        """ Plots correlation heatmap.

        :param method: str,
                       method to compute correlation value.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.

        """

        def __plot_correlation_matplotlib(df, method, download, folder_name):
            """ Generates plot using matplotlib library.

            """
            save_dir = self.dload + "/" + folder_name
            numerical_columns = [tuple_types[0] for tuple_types in df.dtypes if tuple_types[1] != 'string']
            temp_df = VectorAssembler(inputCols=numerical_columns, outputCol="numerical_features").transform(df)
            corr = pd.DataFrame(Correlation.corr(temp_df, 'numerical_features', method).collect()[0][0].toArray(),
                                columns=numerical_columns, index=numerical_columns)

            sns.heatmap(corr, xticklabels=corr.columns.values, yticklabels=corr.columns.values)
            if download:
                if os.path.exists(save_dir):
                    pass
                else:
                    os.mkdir(save_dir)
                plt.savefig(save_dir + "/" + "Correlation" + '.png')
            else:
                plt.show()

        def __plot_correlation_plotly(df, method, zmin=-1, zmax=1):
            """ Generates plot using plotly library.

            :param zmin: int,
                         Sets the lower bound of color domain.
            :param zmax: int,
                         Sets the upper bound of color domain.
            """
            numerical_columns = [tuple_types[0] for tuple_types in df.dtypes if tuple_types[1] != 'string']
            temp_df = VectorAssembler(inputCols=numerical_columns, outputCol="numerical_features").transform(df)
            corr_df = pd.DataFrame(Correlation.corr(temp_df, 'numerical_features', method).collect()[0][0].toArray(),
                                   columns=numerical_columns, index=numerical_columns)

            corr_df.dropna(how="all", axis=0, inplace=True)
            corr_df.dropna(how="all", axis=1, inplace=True)
            corr_df.fillna(value=0, inplace=True)

            # Replace all 1s with 0s along the diagonal
            for row in range(len(corr_df)):
                corr_df.iat[row, row] = 0

            corr_list = corr_df.values.tolist()
            trace = Heatmap(
                z=corr_list,
                x=corr_df.index.values.tolist(),
                y=corr_df.index.values.tolist(),
                colorscale=[[0, "rgb(0,0,255)"], [0.1, "rgb(51,153,255)"], [0.2, "rgb(102,204,255)"],
                            [0.3, "rgb(153,204,255)"], [0.4, "rgb(204,204,255)"], [0.5, "rgb(255,255,255)"],
                            [0.6, "rgb(255,204,255)"], [0.7, "rgb(255,153,255)"], [0.8, "rgb(255,102,204)"],
                            [0.9, "rgb(255,102,102)"], [1, "rgb(255,0,0)]"]],
                zmax=zmax,
                zmin=zmin
            )

            data = Data([trace])
            layout = Layout(
                autosize=True,
                margin=dict(r=240, t=30, b=180, l=180, pad=2),
                showlegend=False,
                xaxis=XAxis(
                    autorange=True,
                    linecolor='rgb(207, 226, 243)',
                    linewidth=8,
                    mirror=True,
                    nticks=corr_df.shape[0],
                    range=[0, corr_df.shape[0] + 1],
                    showline=True,
                    title="",
                    tickangle='40'
                ),
                yaxis=YAxis(
                    autorange=True,
                    linecolor='rgb(207, 226, 243)',
                    linewidth=8,
                    mirror=True,
                    nticks=corr_df.shape[0],
                    range=[0, corr_df.shape[0] + 1],
                    showline=True,
                    title='',
                    tickangle='-30'
                )
            )
            fig = Figure(data=data, layout=layout)
            return fig

        if (download == False) & (engine == 'plotly'):
            fig = __plot_correlation_plotly(self.df, method, zmin, zmax)
            iplot(fig)
        if (download) & (engine == 'plotly'):
            print("Can't download plotly plots")
        if engine == 'matplotlib':
            __plot_correlation_matplotlib(self.df, method, download, folder_name)

    def plot_frequency(self, column_names = None, engine='matplotlib', download=False, folder_name='frequency'):
        """ Generates frequency plot for the list of column names

        :param column_names: list of column names.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot has to be downloaded else False.
        :param folder_name: str,
                            directory name where the plot are to be saved.
        """
        if not column_names:
            column_names = self.categorical_columns

        def __plot_frequency_matplotlib(df, column_names, download, folder_name):
            """ Generates plot using matplotlib library

            """
            save_dir = self.dload + "/" + folder_name

            for each_column in column_names:
                a = df.groupBy(each_column).count().toPandas()
                plt.xlabel(each_column)
                plt.ylabel('Count')
                plt.bar(a[each_column], a['count'])

                if download:
                    if os.path.exists(save_dir):
                        pass
                    else:
                        os.mkdir(save_dir)
                    plt.savefig(save_dir + "/" + (each_column) + '.png')
                    plt.close()
                else:
                    plt.show()

        if engine == 'plotly':
            print("No frequency in plotly")
        if engine == 'matplotlib':
            __plot_frequency_matplotlib(self.df, column_names, download, folder_name)

    def select_run(self, input_dict, engine, download):
        """ Helper function.

        :param input_dict: dict,
                           methods as key and its corresponding argument list as value.
        :param engine: 'plotly' or 'matplotlib',
                       tool to generate the plots.
        :param download: bool,
                         True if the plot as to be downloaded else False

        """
        input_dict = collections.OrderedDict(input_dict)
        for i in input_dict.keys():
            getattr(self, i)(engine=engine, download=download, **input_dict[i])
