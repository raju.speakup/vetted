#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE
rm -r api-dag-schedule*.tar.gz*
a2enmod wsgi
echo "**********STARTING New api-dag-schedule API DEPLOYMENT****************"
download_filename=$(curl -k -s -g http://jenkins.ad.noodle.ai/job/bat-dev-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep api-dag-schedule)
echo "download_filename : $download_filename"
wget --no-check-certificate http://jenkins.ad.noodle.ai/job/bat-dev-api-build/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "api-dag-schedule*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/api-dag-schedule/src
pwd
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
#cd /var/www/html/api-dag-schedule/src
#pwd
pip3 install -r ../requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
export CONF_STORE=10.0.1.95:8811
export CONF_ENV=dev
mkdir static
a2dissite 000-default
a2ensite apidagschedule
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the api-dag-schedule  service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
