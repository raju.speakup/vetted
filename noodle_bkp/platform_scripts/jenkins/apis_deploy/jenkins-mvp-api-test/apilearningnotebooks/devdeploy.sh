#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE
rm -r api-learning-notebooks*.tar.gz*
a2enmod wsgi
echo "**********STARTING New featurebook API DEPLOYMENT****************"
download_filename=$(curl -k -s -g https://jenkins.ad.noodle.ai/job/bat-aws-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep api-learning-notebooks)
wget --no-check-certificate https://jenkins.ad.noodle.ai/job/bat-aws-api-build/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "api-learning-notebooks*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/api-learning-notebooks/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
pip3 install awscli
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
pip3 install -r ../requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
#service specific settings start
export CONF_STORE=192.168.2.142:8811
export CONF_ENV=play
mkdir /var/tmp/notebooks
chmod -R 777 /var/tmp/notebooks/
chown -R www-data /var/tmp/notebooks/
pip3 install docker-compose
pip3 install jupyter
chmod 666 /var/run/docker.sock
# service specific settings end
mkdir static
a2dissite 000-default
a2ensite featurebookapis
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the api-learning-notebooks service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
