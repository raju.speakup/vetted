#!/bin/bash
mkdir -p logs/accesslogs || :
docker-compose exec datapipeline cat /var/log/apache2/access.log > ./logs/accesslogs/datapipeline_access.log
docker-compose exec clientapi cat /var/log/apache2/access.log > ./logs/accesslogs/clientapi_access.log
docker-compose exec datacatalog cat /var/log/apache2/access.log > ./logs/accesslogs/datacatalog_access.log
docker-compose exec dataexplorer cat /var/log/apache2/access.log > ./logs/accesslogs/dataexplorer_access.log
docker-compose exec modelservice cat /var/log/apache2/access.log > ./logs/accesslogs/modelservice_access.log
