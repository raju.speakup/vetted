#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
cd $WORKSPACE
rm -r inframonitoringapis*.tar.gz*
a2enmod wsgi
echo "**********STARTING NEW inframonitoringapis API DEPLOYMENT****************")
wget http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/beast_monitoring_apis.tar.gz
filepath=$(find . -type f -name "inframonitoringapis*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/inframonitoringapis/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
mkdir static
a2dissite 000-default
a2ensite inframonitoringapis
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the inframonitoringapis service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
