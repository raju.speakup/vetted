#!bin/bash
WORKSPACE="$1"
JOBDEPLOYID="35"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
cd $WORKSPACE
rm -r *.tar.gz*
a2enmod wsgi
echo "**********STARTING NEW CLIENT API DEPLOYMENT****************"
#download_filename=$(curl -s -g http://jenkins.ad.noodle.ai/job/mvp-dev-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep mvp_engagements_api)
echo "**********DOWNLOADING TAR FILE****************"
echo "**********FILE URL: http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/mvp_engagements_api.tar.gz ****************"
wget http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/mvp_engagements_api.tar.gz
filepath=$(find . -type f -name "mvp_engagements_api*tar.gz")
CHECKFILE=$(echo $filepath| grep "mvp_engagements_api" | wc -l)
if [ "$CHECKFILE" -eq "1" ]
then
  echo "**********DOWNLOADED TAR FILE CHECK PASSED****************"
  echo "**********************************"
  echo "**********EXTRACTING TAR FILE NOW************************"
else
  echo "**********************************"
  echo "*******FAILED to FIND DOWNLOADED TARFILE. FILEPATH variable is set to: $filepath *******"
  echo "*******BAILING OUT!!!***************************"
  exit -40
fi
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/mvp_engagements_api/source
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
mkdir static
a2dissite 000-default
a2ensite mvpengagementsapi
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the mvpengagementsapi service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
