#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE
rm -r ai-pipeline-apis*.tar.gz*
a2enmod wsgi
echo "**********STARTING New aipipeline API DEPLOYMENT****************"
echo "*******Path http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/ai-pipeline-apis.tar.gz********"
#download_filename=$(curl -k -s -g https://jenkins.ad.noodle.ai/job/mvp-dev-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep ai-pipeline-apis)
wget http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/ai-pipeline-apis.tar.gz
filepath=$(find . -type f -name "ai-pipeline-apis*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/ai-pipeline-apis/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
mkdir static
a2dissite 000-default
a2ensite aipipelineapis
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the ai-pipeline-apis service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
