#!/bin/bash
mkdir -p logs/errorlogs || :
docker-compose exec datapipeline cat /var/log/apache2/error.log > ./logs/errorlogs/datapipeline_error.log
docker-compose exec clientapi cat /var/log/apache2/error.log > ./logs/errorlogs/clientapi_error.log
docker-compose exec datacatalog cat /var/log/apache2/error.log > ./logs/errorlogs/datacatalog_error.log
docker-compose exec dataexplorer cat /var/log/apache2/error.log > ./logs/errorlogs/dataexplorer_error.log
docker-compose exec modelservice cat /var/log/apache2/error.log > ./logs/errorlogs/modelservice_error.log
