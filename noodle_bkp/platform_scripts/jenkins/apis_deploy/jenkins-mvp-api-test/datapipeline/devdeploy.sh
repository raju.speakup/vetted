#!bin/bash
WORKSPACE="$1"
JOBDEPLOYID="6"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
cd $WORKSPACE
rm -r data-pipeline*.tar.gz*
a2enmod wsgi
echo "**********STARTING NEW datapipeline API DEPLOYMENT****************"
#download_filename=$(curl -s -g http://jenkins.ad.noodle.ai/job/mvp-dev-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep data-pipeline)
echo "**********DOWNLOADING TAR FILE****************"
echo "**********FILE URL: http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/data-pipeline.tar.gz ****************"
wget http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/data-pipeline.tar.gz
filepath=$(find . -type f -name "data-pipeline*tar.gz")
CHECKFILE=$(echo $filepath| grep "data-pipeline" | wc -l)
if [ "$CHECKFILE" -eq "1" ]
then
  echo "**********DOWNLOADED TAR FILE CHECK PASSED****************"
  echo "**********************************"
  echo "**********EXTRACTING TAR FILE NOW************************"
else
  echo "**********************************"
  echo "*******FAILED to FIND DOWNLOADED TARFILE. FILEPATH variable is set to: $filepath *******"
  echo "*******BAILING OUT!!!***************************"
  exit -40
fi
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/data-pipeline/API_Python3_v3/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
mkdir static
a2dissite 000-default
a2ensite datapipeline
service apache2 reload
if [ -f /flask/envfiles/datapipeline/.env ]; then
  echo "Environment file found in /flask/ directory in the container! Using that one!"
  cp /flask/envfiles/datapipeline/.env /var/www/html/data-pipeline/API_Python3_v3/src/.env
fi
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the datapipeline service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
