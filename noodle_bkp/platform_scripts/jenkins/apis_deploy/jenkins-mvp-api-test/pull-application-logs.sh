#!/bin/bash
mkdir -p logs/applicationlogs || :
docker-compose exec datapipeline cat /applicationlogs/datapipeline.log > ./logs/applicationlogs/datapipeline_application.log
docker-compose exec clientapi cat /applicationlogs/clientapi.log > ./logs/applicationlogs/clientapi_application.log
docker-compose exec datacatalog cat /applicationlogs/datacatalog.log > ./logs/applicationlogs/datacatalog_application.log
docker-compose exec dataexplorer cat /applicationlogs/dataexplorer.log > ./logs/applicationlogs/dataexplorer_application.log
docker-compose exec modelservice cat /applicationlogs/modelservice.log > ./logs/applicationlogs/modelservice_application.log
