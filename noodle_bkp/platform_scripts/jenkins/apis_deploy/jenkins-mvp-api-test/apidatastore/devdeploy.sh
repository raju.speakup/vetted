#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE
rm -r api-datastore*.tar.gz*
a2enmod wsgi
echo "**********STARTING New api-datastore API DEPLOYMENT****************"
download_filename=$(curl -k -s -g http://jenkins.ad.noodle.ai/job/bat-aws-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep api-datastore)
wget --no-check-certificate http://jenkins.ad.noodle.ai/job/bat-aws-api-build/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "api-datastore*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/api-datastore/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
pip3 install -r ../requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
export CONF_STORE=192.168.2.142:8811
export CONF_ENV=play
mkdir static
a2dissite 000-default
a2ensite apidatastore
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the api-datastore service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
