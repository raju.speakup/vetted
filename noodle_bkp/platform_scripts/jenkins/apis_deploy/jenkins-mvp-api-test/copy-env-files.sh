#!/bin/bash
docker-compose exec datapipeline sh -c '[ -d /flask/envfiles/datapipeline ] || mkdir -p /flask/envfiles/datapipeline ; cp /var/www/html/data-pipeline/API_Python3_v3/src/.env /flask/envfiles/datapipeline/.env'
docker-compose exec clientapi sh -c '[ -d /flask/envfiles/clientapi ] || mkdir -p /flask/envfiles/clientapi ; cp /var/www/html/client_api/src/.env /flask/envfiles/clientapi/.env'
docker-compose exec datacatalog sh -c '[ -d /flask/envfiles/datacatalog ] || mkdir -p /flask/envfiles/datacatalog ; cp /var/www/html/data_catalog/src/config/config.py /flask/envfiles/datacatalog/config.py'
docker-compose exec dataexplorer sh -c '[ -d /flask/envfiles/dataexplorer ] || mkdir -p /flask/envfiles/dataexplorer ; cp /var/www/html/data-explorer/src/.env /flask/envfiles/dataexplorer/.env'
docker-compose exec dataiq sh -c '[ -d /flask/envfiles/dataiq ] || mkdir -p /flask/envfiles/dataiq ; cp /var/www/html/dataiq-mongo/api_mongo_code/src/.env /flask/envfiles/dataiq/.env'
docker-compose exec datacartridge sh -c '[ -d /flask/envfiles/datacartridge ] || mkdir -p /flask/envfiles/datacartridge ; cp /var/www/html/data_cartridge/src/.env /flask/envfiles/datacartridge/.env'
