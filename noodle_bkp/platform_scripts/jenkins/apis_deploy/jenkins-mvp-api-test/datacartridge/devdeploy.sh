#!bin/bash
WORKSPACE="$1"
JOBDEPLOYID="25"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
cd $WORKSPACE
rm -r data_cartridge*.tar.gz*
a2enmod wsgi
echo "**********STARTING NEW datacartridge API DEPLOYMENT****************"
#download_filename=$(curl -s -g http://jenkins.ad.noodle.ai/job/mvp-dev-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep data_cartridge)
echo "**********DOWNLOADING TAR FILE****************"
echo "**********FILE URL: http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/data_cartridge.tar.gz ****************"
wget http://jenkins.ad.noodle.ai/job/mvp-aws-api-build/lastSuccessfulBuild/artifact/output/data_cartridge.tar.gz
filepath=$(find . -type f -name "data_cartridge*tar.gz")
CHECKFILE=$(echo $filepath| grep "data_cartridge" | wc -l)
if [ "$CHECKFILE" -eq "1" ]
then
  echo "**********DOWNLOADED TAR FILE CHECK PASSED****************"
  echo "**********************************"
  echo "**********EXTRACTING TAR FILE NOW************************"
else
  echo "**********************************"
  echo "*******FAILED to FIND DOWNLOADED TARFILE. FILEPATH variable is set to: $filepath *******"
  echo "*******BAILING OUT!!!***************************"
  exit -40
fi
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/data_cartridge/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
pip3 install -r ../requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
export CONF_STORE=192.168.2.142:8811
export CONF_ENV=play
mkdir static
a2dissite 000-default
a2ensite datacartridge
if [ -f /flask/envfiles/datacartridge/.env ]; then
  echo "Environment file found in /flask/ directory in the container! Using that one!"
  cp /flask/envfiles/datacartridge/.env /var/www/html/data_cartridge/src/.env
fi
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the datacartridge service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
