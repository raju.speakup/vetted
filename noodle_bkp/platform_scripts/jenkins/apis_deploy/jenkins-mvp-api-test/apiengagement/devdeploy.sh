#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
cd $WORKSPACE
rm -r *.tar.gz*
a2enmod wsgi
echo "**********STARTING NEW CLIENT API DEPLOYMENT****************"
download_filename=$(curl -s -g http://jenkins.ad.noodle.ai/job/bat-aws-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep api-engagement)
wget http://jenkins.ad.noodle.ai/job/bat-aws-api-build/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "api-engagement*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/api-engagement/source
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
mkdir static
a2dissite 000-default
a2ensite mvpengagementsapi
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the mvpengagementsapi service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
