#!/usr/bin/python3
import sys
import logging
logging.basicConfig(stream=sys.stderr)
activate_this = '/var/www/html/api-engagement/source/venv/bin/activate_this.py'

with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))
sys.path.insert(0,"/var/www/html/api-engagement/")

from apps import app as application
application.secret_key = 'noodleon'
