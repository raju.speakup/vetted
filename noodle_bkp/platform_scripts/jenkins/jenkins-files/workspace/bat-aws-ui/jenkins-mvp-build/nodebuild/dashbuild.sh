#!bin/sh
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
pwd
ls -l ~
cd $WORKSPACE/product_ui/bat_ui
npm install
npm run build
#npm install uglify-es -g
#appjspath=$(find dist/ -type f -name "app*js")
#uglifyjs $appjspath -c -o dist/app.js
cd $WORKSPACE
name="bat_ui-$(date '+%d%m%y-%H%M')";export name;tar -zcvf "$name.tar.gz" -C product_ui/bat_ui/build/ .;mv $name.tar.gz output/
