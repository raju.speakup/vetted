#!/bin/bash
check_container_status()
{
  service_name=$1
  service_path=$2
  shift; shift;
  SERVICE_OUT=$(docker-compose ps $service_name | grep Up | wc -l)
  if [ "$SERVICE_OUT" -eq "1" ]
  then     # If not set, default to ...
    echo "$service_name container is running. Will now pull git logs with latest changeset"
    echo "**********************************"
    echo "*******$service_name is RUNNING*******"
    echo "**********************************"
    echo "********GIT-LOG for $service_name*************"
    docker-compose exec $service_name sh -c " cd $service_path;git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s""
    echo "********GIT-LOG for $service_name*************"
  else
    echo "Could not verify the status of $service_name. Please ensure it is running"
    echo "**********************************"
    echo "*******$service_name is STOPPED*******"
    echo "**********************************"
  fi
}

echo "Checking status of clientapi service"
check_container_status clientapi /var/www/html/client_api/

echo "Checking status of datacatalog service"
check_container_status datacatalog /var/www/html/data_catalog/

echo "Checking status of dataexplorer service"
check_container_status dataexplorer /var/www/html/data-explorer/

echo "Checking status of datapipeline service"
check_container_status datapipeline /var/www/html/data-pipeline/

echo "Checking status of model service"
check_container_status modelservice /var/www/html/model_service/
