#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE
rm -r conf-store-apis*.tar.gz*
a2enmod wsgi
echo "**********STARTING NEW Config Manager API DEPLOYMENT****************"
download_filename=$(curl -k -s -g https://jenkins.ad.noodle.ai/job/conf-store-apis-ui-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep conf-store-apis)
wget --no-check-certificate https://jenkins.ad.noodle.ai/job/conf-store-apis-ui-build/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "conf-store-apis*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/conf-store/config_store_apis/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
mkdir static
a2dissite 000-default
a2ensite confapi
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the config-manager API service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
