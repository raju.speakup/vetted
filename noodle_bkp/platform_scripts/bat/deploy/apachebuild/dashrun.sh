#!/bin/sh
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE

appuitarpath=$(find output/ -type f -name "bat_ui*tar.gz")
echo $appuitarpath

tar -xvzf $appuitarpath -C /usr/local/apache2/htdocs/
exec /usr/local/apache2/bin/apachectl -D FOREGROUND