#!/bin/sh
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE

tar -xvzf $appuitarpath -C /usr/local/apache2/htdocs/
exec /usr/local/apache2/bin/apachectl -D FOREGROUND