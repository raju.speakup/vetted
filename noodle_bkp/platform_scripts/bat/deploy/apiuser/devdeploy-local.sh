#!bin/bash
WORKSPACE="$1"
JOBDEPLOYID="lastSuccessfulBuild"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
chown 33:33 -R /var/www/html/api-user
cd $WORKSPACE
rm -r api-user*.tar.gz*
a2enmod wsgi

cd /var/www/html/code/product_api/api-user/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
pip3 install -r ../requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
export CONF_STORE=10.0.1.95:8811
export CONF_ENV=qa
mkdir static
a2dissite 000-default
a2ensite user
service apache2 reload
#if [ -f /flask/envfiles/dataiq/.env ]; then
#  echo "Environment file found in /flask/ directory in the container! Using that one!"
#  cp /flask/envfiles/dataiq/.env /var/www/html/api-data-pipeline/src/.env
#fi
echo "CONF_ENV_START"
echo "$CONF_ENV"
echo "CONF_ENV_END"
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the dataiq service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND