#!bin/bash
WORKSPACE="$1"
JOBDEPLOYID="lastSuccessfulBuild"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
chown 33:33 -R /applicationlogs
chown 33:33 -R /var/www/html/api-communication
cd $WORKSPACE
mkdir /applicationlogs
rm -r api-communication*.tar.gz*
a2enmod wsgi

echo "**********STARTING API DEPLOYMENT api-communication ****************"
download_filename=$(curl -s -g http://jenkins.ad.noodle.ai/job/bat-dev-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep api-communication)
echo "**********DOWNLOADING TAR FILE****************"
echo "**********FILE URL: http://jenkins.ad.noodle.ai/job/bat-dev-api-build/$JOBDEPLOYID/artifact/output/api-communication.tar.gz"
url="http://jenkins.ad.noodle.ai/job/bat-dev-api-build/$JOBDEPLOYID/artifact/$download_filename"
echo "Download url : $url"
wget "$url"

filepath=$(find . -type f -name "api-communication*tar.gz")
echo "filepath is =$filepath"
#filepath=echo "$filepath" | sed s/[.]//g
#echo "filepath is =$filepath"
CHECKFILE=$(echo $filepath| grep "api-communication.tar.gz" | wc -l)
echo "CHECKFILE=$CHECKFILE"
CHECKFILE=1
if [ "$CHECKFILE" -eq "1" ]
then
  echo "**********DOWNLOADED TAR FILE CHECK PASSED****************"
  echo "**********************************"
  echo "**********EXTRACTING TAR FILE NOW************************"
else
  echo "**********************************"
  echo "*******FAILED to FIND DOWNLOADED TARFILE. FILEPATH variable is set to: $filepath *******"
  echo "*******BAILING OUT!!!***************************"
  exit -40
fi


echo "filepath is =$filepath"


tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/api-communication/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
#pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
pip3 install -r ../requirements.txt --trusted-host=192.168.2.129 --extra-index-url http://192.168.2.129:8081/repository/python-group/simple
export CONF_STORE=10.0.1.95:8811
export CONF_ENV=dev
mkdir static
a2dissite 000-default
a2ensite communication
service apache2 reload
#if [ -f /flask/envfiles/dataiq/.env ]; then
#  echo "Environment file found in /flask/ directory in the container! Using that one!"
#  cp /flask/envfiles/dataiq/.env /var/www/html/api-data-pipeline/src/.env
#fi
echo "CONF_ENV_START"
echo "$CONF_ENV"
echo "CONF_ENV_END"
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the dataiq service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND