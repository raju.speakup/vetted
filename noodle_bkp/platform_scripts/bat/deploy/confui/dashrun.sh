#!/bin/sh
WORKSPACE="DUMMYVARIABLE"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
#download_filename=$(curl -k -s -g https://jenkins.ad.noodle.ai/job/conf-store-apis-ui-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep conf-store-ui)
#wget --no-check-certificate https://jenkins.ad.noodle.ai/job/conf-store-apis-ui-build/lastSuccessfulBuild/artifact/$download_filename
#filepath=$(find . -type f -name "conf-store-ui*tar.gz")
#tar -xvzf $filepath -C /usr/local/apache2/htdocs/

cp -rf /var/www/html/code/conf-store/confi-store-ui/docs/* /usr/local/apache2/htdocs/
exec /usr/local/apache2/bin/apachectl -D FOREGROUND