#!bin/sh
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
pwd
ls -l ~


echo "**********STARTING New BAT UI DEPLOYMENT****************"
#download_filename=$(curl -k -s -g http://192.168.2.129/job/bat-dev-ui/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xmlstarlet sel -t -v '/workflowRun/artifact/relativePath'|grep 'bat_ui')

download_filename=$(curl -k -s -g http://192.168.2.129/job/bat-dev-ui/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()'|grep 'bat_ui')

wget --no-check-certificate http://192.168.2.129/job/bat-dev-ui/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "bat_ui*tar.gz")
tar -xvzf $filepath -C $WORKSPACE/product_ui/bat_ui

#npm install
#npm run build
apachectl -D FOREGROUND