#!bin/sh
WORKSPACE="$1"
export WORKSPACE

if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"

cd ./../code/product_ui/bat_ui
npm install
npm run build

cd $WORKSPACE
name="bat_ui-$(date '+%d%m%y-%H%M')";export name;tar -zcvf "$name.tar.gz" -C product_ui/bat_ui/build/ .;mv $name.tar.gz output/