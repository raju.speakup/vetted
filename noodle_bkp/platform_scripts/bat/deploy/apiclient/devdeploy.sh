#!bin/bash
WORKSPACE="$1"
export WORKSPACE
if [ -z "$WORKSPACE" ]
then
  echo "Couldn't find workspace, quitting..."
  exit -5
fi
echo "Current workspace $WORKSPACE"
cd $WORKSPACE
rm -r api-client*.tar.gz*
a2enmod wsgi
echo "**********STARTING New api-client API DEPLOYMENT****************"
download_filename=$(curl -k -s -g http://192.168.2.129/job/bat-aws-api-build/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath] | xpath -q -e '//relativePath/text()' | grep api-client)
wget --no-check-certificate http://192.168.2.129/job/bat-aws-api-build/lastSuccessfulBuild/artifact/$download_filename
filepath=$(find . -type f -name "api-client*tar.gz")
tar -xvzf $filepath -C /var/www/html/
cd /var/www/html/api-client/src
echo "**********GIT-LOG*****************"
git log -n5 --pretty=format:"%h%x09%an%x09%ad%x09%s"
echo "**********GIT-LOG*****************"
pip3 install mod_wsgi
pip3 install virtualenv
virtualenv --no-site-packages venv
source venv/bin/activate
pip3 install -r ../requirements.txt --trusted-host nexus.ad.noodle.ai
export CONF_STORE=10.0.1.95:8811
export CONF_ENV=dev
mkdir static
a2dissite 000-default
a2ensite apiclient
service apache2 reload
echo "-------------------------Achtung!-------------------------------------------"
echo "**********Starting the api-client service at current time in UTC $(date) ***************"
apachectl -D FOREGROUND
