if [ "$1" == "ui" ]; then
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose-ui.yml up -d --build --force-recreate nodebuild
    echo "run again if UI shows It works @todo: fix bug"
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose-ui.yml up -d --build --force-recreate runapache
fi

if [ "$1" == "backend" ]; then
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apiuser
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apiauthentication
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apiauthorization
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apiclient
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apiaipipeline
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apidatastore
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apidatacartridge
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apidatapipeline
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apilearningnotebooks
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate apicommunication
fi

if [ "$1" == "tools" ]; then
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate confuibuild
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate confui
    docker-compose -f /opt/dev/code/platform_scripts/bat/deploy/docker-compose.yml up -d --build --force-recreate confapi
fi

if [[ "$1" != "ui" && "$1" != "backend" && "$1" != "tools" ]]; then
   echo "Usage: deploy-all <option>"
   echo "option is one of ui, backend or tools"
fi